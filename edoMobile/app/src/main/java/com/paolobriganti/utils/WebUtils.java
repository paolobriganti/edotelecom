package com.paolobriganti.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.edo.utilities.Constants;

public class WebUtils {


	//Pull all links from the body for easy retrieval
	public static ArrayList<String> getLinks(String text) {
		ArrayList<String> links = new ArrayList<String>();

		String regex = "\\(?\\b(http://|https://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(text);
		while(m.find()) {
			String urlStr = m.group();
			if (urlStr.startsWith("(") && urlStr.endsWith(")"))
			{
				urlStr = urlStr.substring(1, urlStr.length() - 1);
			}
			links.add(urlStr);
		}
		
		return links;
	}



	public static InputStream getInputStreamFromUrl(String urlString) throws IOException{
		URL url = new URL (urlString);
		return url.openStream();
	}


	public static String getPageContent(String url){
		String html = "";
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);
		try {
			HttpResponse response = client.execute(request);
			InputStream in = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder str = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null)
			{
				str.append(line);
			}
			in.close();
			html = str.toString();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return html;
	}


	private static final Pattern TITLE_TAG =
			Pattern.compile("\\<title>(.*)\\</title>", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);



	public static String getPageTitleFromHTML(String html){
		if(JavaUtils.isNotEmpty(html)){
			Matcher matcher = TITLE_TAG.matcher(html);
			if (matcher.find()) {
				return matcher.group(1).replaceAll("[\\s\\<>]+", " ").trim();
			}
		}
		return null;
	}

	/**
	 * @param url the HTML page
	 * @return title text (null if document isn't HTML or lacks a title tag)
	 * @throws java.io.IOException
	 */
	public static String getPageTitle(String url) {
		try {
			URL u = new URL(url);

			URLConnection conn = u.openConnection();

			// ContentType is an inner class defined below
			ContentType contentType = getContentTypeHeader(conn);
			if (contentType==null || contentType.contentType==null || !contentType.contentType.equals("text/html"))
				return null; // don't continue if not HTML
			else {
				HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet(url);
				HttpResponse response = client.execute(request);

				InputStream in = response.getEntity().getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				int n = 0, totalRead = 0;
				char[] buf = new char[1024];
				StringBuilder content = new StringBuilder();

				// read until EOF or first 8192 characters
				while (totalRead < 8192 && (n = reader.read(buf, 0, buf.length)) != -1) {
					content.append(buf, 0, n);
					totalRead += n;
				}
				reader.close();

				// extract the title
				Matcher matcher = TITLE_TAG.matcher(content);
				if (matcher.find()) {
					/* replace any occurrences of whitespace (which may
					 * include line feeds and other uglies) as well
					 * as HTML brackets with a space */
					return matcher.group(1).replaceAll("[\\s\\<>]+", " ").trim();
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}



	/**
	 * Loops through response headers until Content-Type is found.
	 * @param conn
	 * @return ContentType object representing the value of
	 * the Content-Type header
	 */
	private static ContentType getContentTypeHeader(URLConnection conn) {
		int i = 0;
		boolean moreHeaders = true;
		do {
			String headerName = conn.getHeaderFieldKey(i);
			String headerValue = conn.getHeaderField(i);
			if (headerName != null && headerName.equals("Content-Type"))
				return new ContentType(headerValue);

			i++;
			moreHeaders = headerName != null || headerValue != null;
		}
		while (moreHeaders);

		return null;
	}

	private static Charset getCharset(ContentType contentType) {
		if (contentType != null && contentType.charsetName != null && Charset.isSupported(contentType.charsetName))
			return Charset.forName(contentType.charsetName);
		else
			return null;
	}

	/**
	 * Class holds the content type and charset (if present)
	 */
	private static final class ContentType {
		private static final Pattern CHARSET_HEADER = Pattern.compile("charset=([-_a-zA-Z0-9]+)", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);

		private String contentType;
		private String charsetName;
		private ContentType(String headerValue) {
			if (headerValue == null)
				throw new IllegalArgumentException("ContentType must be constructed with a not-null headerValue");
			int n = headerValue.indexOf(";");
			if (n != -1) {
				contentType = headerValue.substring(0, n);
				Matcher matcher = CHARSET_HEADER.matcher(headerValue);
				if (matcher.find())
					charsetName = matcher.group(1);
			}
			else
				contentType = headerValue;
		}
	}















	//*********************************************************************
	//*		YOUTUBE
	//*********************************************************************

	public static boolean isYoutubeUrl(String url){
		if(JavaUtils.isWebUrl(url)){
			return url.contains("youtube")||url.contains(Constants.HOST_NAME_YOUTUBE_APP);
		}
		return false;
	}

	public static String getYoutubeVideoThumbnailUrl(String url){
		if(isYoutubeUrl(url)){
			String videoId = getYoutubeVideoId(url);
			if(JavaUtils.isNotEmpty(videoId)){
				return getYoutubeVideoThumbnailUrlByYoutubeId(videoId);
			}
		}
		return null;
	}

	public static String getYoutubeVideoThumbnailUrlByYoutubeId(String videoId){
		if(JavaUtils.isNotEmpty(videoId)){
			return "http://img.youtube.com/vi/VIDEO_ID/default.jpg".replace("VIDEO_ID", videoId);
		}
		return null;
	}

	public static String getYoutubeVideoId(String youtubeUrl)
	{
		String video_id="";
		if (youtubeUrl != null && youtubeUrl.trim().length() > 0 && youtubeUrl.startsWith("http"))
		{

			String expression = "^.*((youtu.be"+ "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
			CharSequence input = youtubeUrl;
			Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(input);
			if (matcher.matches())
			{
				String groupIndex1 = matcher.group(7);
				if(groupIndex1!=null && groupIndex1.length()==11)
					video_id = groupIndex1;
			}
		}
		return video_id;
	}



	//*********************************************************************
	//*		MAPS
	//*********************************************************************

	public static boolean containsGoogleMapsUrl(String url){
		if(JavaUtils.isNotEmpty(url)){
			if(url.contains("http")){
				String[] u = url.split("http");
				return u[1]!=null && u[1].contains("maps");
			}
		}
		return false;
	}
}
