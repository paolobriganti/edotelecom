/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paolobriganti.utils.imageLoader;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * The main fragment that powers the ImageGridActivity screen. Fairly straight forward GridView
 * implementation with the key addition being the ImageWorker class w/ImageCache to load children
 * asynchronously, keeping the UI nice and smooth and caching thumbnails for quick retrieval. The
 * cache is retained over configuration changes like orientation change so the images are populated
 * quickly if, for example, the user rotates the device.
 */
public abstract class ImageListFragment extends Fragment {

    private ImageLoader mImageLoader;
    private RecyclerView mRecyclerView;

    /**
     * Empty constructor as per the Fragment documentation
     */
    public ImageListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListParams();
    }

    public abstract RecyclerView getRecyclerView();

    public abstract ImageLoader getImageLoader();



    public void setListParams(){
        mImageLoader = getImageLoader();
        mRecyclerView = getRecyclerView();

        mRecyclerView.addOnScrollListener(new MyScrollListener());

    }


    private class MyScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {

            // Pause fetcher to ensure smoother scrolling when flinging
            if (scrollState != RecyclerView.SCROLL_STATE_IDLE) {
                getImageLoader().setPauseWork(true);
            } else {
                getImageLoader().setPauseWork(false);
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
//        if(mImageLoader!=null)
//            mImageLoader.setExitTasksEarly(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mImageLoader!=null) {
//            mImageLoader.setPauseWork(false);
//            mImageLoader.setExitTasksEarly(true);
            mImageLoader.flushCache();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if(mImageLoader!=null)
//            mImageLoader.closeCache();
    }



}
