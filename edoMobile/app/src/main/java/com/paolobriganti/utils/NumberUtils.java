package com.paolobriganti.utils;

public class NumberUtils {

	public static int getPercent(final int current, final int total){
		float progress = ((float)current/(float)total)*100;
		return (int) progress;
	}
	
	public static int getValueFromPercent(final int percent, final int totalValue){
		float progress = ((float)totalValue/(float)100)*percent;
		return (int) progress;
	}

	public static int getPercent(final int current, final int total, final int from, final int to){
		int max = to-from;
		float progress = ((float)current/(float)total)*max;
		return (int) progress + from;
	}


	
	public static float getNewSide2(float oldSide1, float oldSide2, float newSide1){
		return ((newSide1*oldSide2)/oldSide1);
	}
}
