package com.paolobriganti.utils;

import java.io.InputStream;
import java.io.OutputStream;


public interface UniversalProgressListener {
	
	void onPreStart(InputStream is, OutputStream os);
	
	void onStart(InputStream is, OutputStream os);
	
	void onProgressChanged(InputStream is, OutputStream os, int progress);
	
	void onStopByUser();
	
	void onError(Integer errorCode);
	
	void onFinish();
	
	void onPostFinish();
	
	void onInterrupt();
}
