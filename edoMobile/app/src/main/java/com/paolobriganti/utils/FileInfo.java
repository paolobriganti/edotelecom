package com.paolobriganti.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.provider.OpenableColumns;
import android.text.format.Time;
import android.webkit.MimeTypeMap;

import org.apache.commons.io.FilenameUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;


public class FileInfo {
	public static final long GB_IN_BYTES = 1073741824;
	public static final long MB_IN_BYTES = 1048576;
	public static final long KB_IN_BYTES = 1024;




	public static final String EXT_3D = "<3d><3dm><3ds><max><obj>";
	public static final String EXT_AUDIO = "<3ga><aac><ac3><aiff><ape><amr><bwf><cda><flac><it><iff><mid><mka><mod><mpa><mp1><mp2><mp3><m4a><mt9><riff><quicktime><pca><ra><rm><s3m><sfa><w64><ogg><wav><wma><xm>";
	public static final String EXT_BACKUP = "<bak><tmp>";
	public static final String EXT_CAD = "<dwg><dxf>";
	public static final String EXT_CONTACT = "<vcf>";
	public static final String EXT_COMPRESSED = "<bz2><gz><tar><zip><rar><7z><ace><arj><arc><cab><><hqx><csolha><sit><zoo><sitx><zim>";
	public static final String EXT_DATABASE = "<dat><accdb><db><dbf><mdb><pdb><sql><sdf>";
	public static final String EXT_DEVELOPMENT = "<c><class><cpp><cs><dtd><fla><h><java><lua><m><pl><py><sh><sln><vcxproj><xcodeproj>";
	public static final String EXT_DISK = "<bin><cue><dmg><iso><mdf><toast><vcd>";
	public static final String EXT_ENCODED = "<hqx><mim><uue>";
	public static final String EXT_EXE = "<a><out><bat><coff><com><dll><elf><exe><gambas><jar><le><mach-o><mz><ne><pe><script><asp><sh><pl><php><py><sql><js><vbs><apk><app><cgi><gadget><pif><vb><wsf>";
	public static final String EXT_FONT = "<fnt><fon><otf><ttf>";
	public static final String EXT_GAME = "<dem><gam><nes><rom><sav>";
	public static final String EXT_GIS = "<gpx><kml><kmz>";
	public static final String EXT_RASTER = "<bmp><gif><jpeg><jpg><pict><png><tga><tiff><djvu><pcx><cpd><jpd><gpd><webp><cdr><drv><dgn><dwg><dxf><dwf><edrw><flt><fla><igs><lfp><par><prt><sat><stl><pln><pla><shp>";
	public static final String EXT_LAYOUT = "<ibooks><indd><pct><pdf>";
	public static final String EXT_MISC = "<tax2012><keychain><ged><gbr><crdownload><ics><msi><part><torrent>";
	public static final String EXT_PLUGIN = "<crx><plugin>";
	public static final String EXT_PRESENTATION = "<key><pps><ppt><pptx><odp><fodp>";
	public static final String EXT_SPREADSHEET = "<csv><xlr><xls><xlsx><ods><fods>";
	public static final String EXT_SYSTEM = "<cab><cpl><cur><deskthemepack><dll><dmp><drv><icns><ico><lnk><sys><cfg><ini><prf><conf>";
	public static final String EXT_TEXT = "<doc><docx><log><msg><odt><pages><rtf><tex><txt><wpd><ott><fodt>";
	public static final String EXT_VECTOR = "<ai><eps><ps><svg><odg><fodg>";
	public static final String EXT_VIDEO = "<3g2><3gp><asf><animgif><avi><divx><flv><swf><mpeg><m4v><mp4><mp4v><ogm><ogg><wmv><mov><mkv><nbr><rm><vob><svi><dlx><mxf><sfd><webm>";
	public static final String EXT_WEB = "<asp><aspx><cer><cfm><csr><css><htm><html><js><jsp><php><rss><xhtml><xml>";

	public static final String TYPE_3D = "3d";
	public static final String TYPE_AUDIO = "audio";
	public static final String TYPE_BACKUP = "backup";
	public static final String TYPE_CAD = "cad";
	public static final String TYPE_COMPRESSED = "compressed";
	public static final String TYPE_CONTACT = "contact";
	public static final String TYPE_DATABASE = "database";
	public static final String TYPE_DEVELOPMENT = "development";
	public static final String TYPE_DISK = "disk";
	public static final String TYPE_ENCODED = "encoded";
	public static final String TYPE_EXE = "exe";
	public static final String TYPE_FONT = "font";
	public static final String TYPE_FOLDER = "folder";
	public static final String TYPE_GAME = "game";
	public static final String TYPE_GDOC = "gdoc";
	public static final String TYPE_GIS = "gis";
	public static final String TYPE_LAYOUT = "layout";
	public static final String TYPE_MISC = "misc";
	public static final String TYPE_PLUGIN = "plugin";
	public static final String TYPE_PRESENTATION = "presentation";
	public static final String TYPE_RASTER = "raster";
	public static final String TYPE_SPREADSHEET = "spreadsheet";
	public static final String TYPE_SYSTEM = "system";
	public static final String TYPE_TEXT = "text";
	public static final String TYPE_VECTOR = "vector";
	public static final String TYPE_VIDEO = "video";
	public static final String TYPE_WEB = "web";
	public static final String TYPE_UNKNOWN = "unknown";


	public static final String MIME_TEXT = "text/plain";

	public static String getTypeByPath(String filePath){
		String fileExtension = FilenameUtils.getExtension(filePath);
		String typeByExt = getTypeByExt(fileExtension);
		if(JavaUtils.isNotEmpty(typeByExt) && !typeByExt.equals(TYPE_UNKNOWN) && !typeByExt.equals(TYPE_VIDEO) )
			return typeByExt;
		else if(JavaUtils.isNotEmpty(typeByExt) && typeByExt.equals(TYPE_VIDEO)){
			try{
				if(new File(filePath).exists()){
					MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
					metaRetriver.setDataSource(filePath);
					if(JavaUtils.isEmpty(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO)))
						return FileInfo.TYPE_AUDIO;
				}
			}catch(Exception e){}
			return FileInfo.TYPE_VIDEO;			
		}

		String mimeType = getTypeByMime(FileInfo.getMime(filePath));
		if(JavaUtils.isNotEmpty(mimeType))
			return mimeType;

		return TYPE_UNKNOWN;
	}

	public static String getTypeByMime(String mime){
		if(JavaUtils.isNotEmpty(mime)){
			if(mime.contains("audio"))
				return FileInfo.TYPE_AUDIO;
			else if(mime.contains("image"))
				return FileInfo.TYPE_RASTER;
			else if(mime.contains("video"))
				return FileInfo.TYPE_VIDEO;
			else if(mime.contains("text"))
				return FileInfo.TYPE_TEXT;
			else if(mime.contains("document"))
				return FileInfo.TYPE_TEXT;
			else if(mime.contains("drawing"))
				return FileInfo.TYPE_RASTER;
			else if(mime.contains("form"))
				return FileInfo.TYPE_VECTOR;
			else if(mime.contains("fusiontable"))
				return FileInfo.TYPE_SPREADSHEET;
			else if(mime.contains("photo"))
				return FileInfo.TYPE_RASTER;
			else if(mime.contains("presentation"))
				return FileInfo.TYPE_PRESENTATION;
			else if(mime.contains("script"))
				return FileInfo.TYPE_DEVELOPMENT;
			else if(mime.contains("sites"))
				return FileInfo.TYPE_WEB;
			else if(mime.contains("spreadsheet"))
				return FileInfo.TYPE_SPREADSHEET;
			else if(mime.contains("video"))
				return FileInfo.TYPE_VIDEO;
		}
		return TYPE_UNKNOWN;
	}

	public static String getTypeByExt(String fileExtension){
		if(JavaUtils.isNotEmpty(fileExtension)){
			String ext = "<"+fileExtension.toLowerCase()+">";
			if(FileInfo.EXT_3D.contains(ext))
				return FileInfo.TYPE_3D;
			else if(FileInfo.EXT_AUDIO.contains(ext))
				return FileInfo.TYPE_AUDIO;
			else if(FileInfo.EXT_BACKUP.contains(ext))
				return FileInfo.TYPE_BACKUP;
			else if(FileInfo.EXT_CAD.contains(ext))
				return FileInfo.TYPE_CAD;
			else if(FileInfo.EXT_CONTACT.contains(ext))
				return FileInfo.TYPE_CONTACT;
			else if(FileInfo.EXT_COMPRESSED.contains(ext))
				return FileInfo.TYPE_COMPRESSED;
			else if(FileInfo.EXT_DATABASE.contains(ext))
				return FileInfo.TYPE_DATABASE;
			else if(FileInfo.EXT_DEVELOPMENT.contains(ext))
				return FileInfo.TYPE_DEVELOPMENT;
			else if(FileInfo.EXT_DISK.contains(ext))
				return FileInfo.TYPE_DISK;
			else if(FileInfo.EXT_ENCODED.contains(ext))
				return FileInfo.TYPE_ENCODED;
			else if(FileInfo.EXT_EXE.contains(ext))
				return FileInfo.TYPE_EXE;
			else if(FileInfo.EXT_FONT.contains(ext))
				return FileInfo.TYPE_FONT;
			else if(FileInfo.EXT_GAME.contains(ext))
				return FileInfo.TYPE_GAME;
			else if(FileInfo.EXT_GIS.contains(ext))
				return FileInfo.TYPE_GIS;
			else if(FileInfo.EXT_RASTER.contains(ext))
				return FileInfo.TYPE_RASTER;
			else if(FileInfo.EXT_LAYOUT.contains(ext))
				return FileInfo.TYPE_LAYOUT;
			else if(FileInfo.EXT_MISC.contains(ext))
				return FileInfo.TYPE_MISC;
			else if(FileInfo.EXT_PLUGIN.contains(ext))
				return FileInfo.TYPE_PLUGIN;
			else if(FileInfo.EXT_PRESENTATION.contains(ext))
				return FileInfo.TYPE_PRESENTATION;
			else if(FileInfo.EXT_SPREADSHEET.contains(ext))
				return FileInfo.TYPE_SPREADSHEET;
			else if(FileInfo.EXT_SYSTEM.contains(ext))
				return FileInfo.TYPE_SYSTEM;
			else if(FileInfo.EXT_TEXT.contains(ext))
				return FileInfo.TYPE_TEXT;
			else if(FileInfo.EXT_VECTOR.contains(ext))
				return FileInfo.TYPE_VECTOR;
			else if(FileInfo.EXT_VIDEO.contains(ext))
				return FileInfo.TYPE_VIDEO;
			else if(FileInfo.EXT_WEB.contains(ext))
				return FileInfo.TYPE_WEB;
		}
		return TYPE_UNKNOWN;
	}

	public static String getMime(String filePath){
		String unknown = "content/unknown";
		boolean fileExists = true;
		try{
			File file = new File(filePath);
			fileExists = file!=null && file.exists();
		}catch (NullPointerException e){

		}
		if(fileExists){
			try{
				//Method1
				FileNameMap fileNameMap = URLConnection.getFileNameMap();
				String mime = fileNameMap.getContentTypeFor(filePath);


				if(mime!=null && !mime.contains(unknown))
					return mime;
			}catch (Exception e){}


			try{
				//Method2
				String mime = new File(filePath).toURL().openConnection().getContentType();


				if(mime!=null && !mime.contains(unknown))
					return mime;

			}catch (Exception e) {}


			try{
				//Method3
				InputStream is = new BufferedInputStream(new FileInputStream(new File(filePath)));
				String mime = URLConnection.guessContentTypeFromStream(is);
				is.close();


				if(mime!=null && !mime.contains(unknown))
					return mime;

			}catch (Exception e) {}


//			try{
//				//Method4
//				MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
//				String mime = mimeTypesMap.getContentType(filePath);
//
//
//				if(mime!=null && !mime.contains(unknown))
//					return mime;
//
//			}catch (Exception e){}

			try{
				//Method5

				String extension = FilenameUtils.getExtension(filePath);
				String mime =  MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);


				if(mime!=null && !mime.contains(unknown))
					return mime;
			}catch (Exception e){}

		}




		return unknown;
	}

	public static String getRealPathFromURIFile(Context c, Uri contentUri) {
		try{
			Cursor cur = c.getContentResolver().query(contentUri, null, null, null, null);
			if (cur == null) {
				return contentUri.getPath();
			}else{
				String [] proj      = {MediaStore.Audio.Media.DATA, MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
				Cursor cursor       = c.getContentResolver().query(contentUri, proj,null,null,null);
				if (cursor == null) return null;
				int column_index    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				return cursor.getString(column_index);
			}
		}catch(NullPointerException e){}
		return null;
	}

	public static String getRealPathFromURI(Context c, Uri contentUri) {

		String filePath = null;

		try{
			filePath = FileInfo.getRealPathFromURIFile(c,contentUri);
		}catch (Exception e){}

		try{
			filePath = PathUtils.getKitKatRealPath(c, contentUri);
		}catch (Exception e){}

		if(JavaUtils.isEmpty(filePath) && contentUri!=null && contentUri.toString().startsWith("file:/")){
			filePath = (new File(contentUri.toString())).getAbsolutePath();
		}
		return filePath;
	}

	public static String createFileFromUri(Context c, Intent intent, Uri data, String directory, String suffix){
		
		String fileName = "temp";
		
		try 
		{
			fileName = getContentNameFromURI(c.getContentResolver(), data);

			if(JavaUtils.isNotEmpty(fileName)){
				String name = FilenameUtils.getBaseName(fileName);
				String ext = FilenameUtils.getExtension(fileName);
				
				fileName = name+suffix+"."+ext;
				
			}else{
				
				fileName = "file_"+suffix;
			
			}
			
			if(directory!=null){
				File dir = new File(directory);
				dir.mkdirs();
				if(!dir.exists()){
					return null;
				}
			}else
				return null;

			String filePath = directory+File.separator+fileName;

			String mime = getMimeFromURI(c.getContentResolver(), data);
			if(mime!=null) {
				if (mime.contains("image")) {

					Bitmap bmp = getBitmapFromUri(c, data);

					CompressFormat compressFormat = CompressFormat.JPEG;

					if (filePath.endsWith(".png")) {
						compressFormat = CompressFormat.PNG;
					} else if (!filePath.endsWith(".jpg") && !filePath.endsWith(".jpeg")) {
						filePath += ".jpg";
					}
					if (!ImageUtils.saveBitmapToFile(bmp, compressFormat, directory, fileName))
						filePath = null;

				} else if (mime.contains(MIME_TEXT)) {

					String textOfFile = readTextFromUri(c, data);
					if (!StringFile.writeToPath(textOfFile, filePath))
						filePath = null;
				} else {

					InputStream attachment = c.getContentResolver().openInputStream(data);
					if (attachment != null) {
						FileOutputStream tmp = new FileOutputStream(filePath);
						byte[] buffer = new byte[1024];
						while (attachment.read(buffer) > 0)
							tmp.write(buffer);

						tmp.close();
						attachment.close();
					}
				}
			}
			return filePath;
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String getContentNameFromURI(ContentResolver resolver, Uri uri){
		try{
			Cursor cursor = resolver.query(uri, null, null, null, null);
			if(cursor!=null){
				cursor.moveToFirst();
				int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
				if (nameIndex >= 0) {
					return cursor.getString(nameIndex);
				}else{
					nameIndex = cursor.getColumnIndex(MediaColumns.DISPLAY_NAME);
					if (nameIndex >= 0) {
						return cursor.getString(nameIndex);
					}

				}
			}
		}catch (Exception e){

		}
		return null;
	}

	public static String getMimeFromURI(ContentResolver resolver, Uri uri){
		return resolver.getType(uri);
	}

	public static String getContentSizeFromURI(ContentResolver resolver, Uri uri){
		try{
			Cursor cursor = resolver.query(uri, null, null, null, null);
			if(cursor!=null){
				cursor.moveToFirst();
				int index = cursor.getColumnIndex(OpenableColumns.SIZE);
				if (index >= 0) {
					return cursor.getString(index);
				}else{
					index = cursor.getColumnIndex(MediaColumns.SIZE);
					if (index >= 0) {
						return cursor.getString(index);
					}

				}
			}
		}catch (Exception e){

		}
		return null;
	}



	public static Bitmap getBitmapFromUri(Context c, Uri uri) throws IOException {
		ParcelFileDescriptor parcelFileDescriptor =
				c.getContentResolver().openFileDescriptor(uri, "r");
		FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
		Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
		parcelFileDescriptor.close();
		return image;
	}

	public static String readTextFromUri(Context c, Uri uri) throws IOException {
		InputStream inputStream = c.getContentResolver().openInputStream(uri);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));
		StringBuilder stringBuilder = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
		}
		return stringBuilder.toString();
	}








	public static Long getFileByteSize(String filePath){
		File file = new File(filePath);
		if(file.exists())
			return file.length();
		return null;
	}

	public static Long getFileKByteSize(String filePath){
		File file = new File(filePath);
		if(file.exists())
			return file.length()/KB_IN_BYTES;
		return null;
	}

	public static Long getFileMByteSize(String filePath){
		File file = new File(filePath);
		if(file.exists())
			return file.length()/MB_IN_BYTES;
		return null;
	}

	public static Long getFileGByteSize(String filePath){
		File file = new File(filePath);
		if(file.exists())
			return file.length()/GB_IN_BYTES;
		return null;
	}

	public static long byteToKB(long bytes){
		return bytes/KB_IN_BYTES;
	}

	public static long byteToMB(long bytes){
		return bytes/MB_IN_BYTES;
	}
	public static long byteToGB(long bytes){
		return bytes/GB_IN_BYTES;
	}




	public static String getFileNameWithTime(String prefix){
		Time time = new Time();
		time.setToNow();
		return prefix+time.format("%Y%m%d%H%M%S");
	}












	public static ArrayList<File> getAllMediaFiless(Activity activity) {
		ArrayList<File> mediaList = new ArrayList<File>();
		String nameOfFile = null;
		Uri uri;
		Cursor cursor;
		int column_index;
		int column_displayname;
		int lastIndex;


		String[] projection = { MediaColumns.DATA, MediaColumns.DISPLAY_NAME };

		uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

		cursor = activity.managedQuery(uri, projection, null, null, null);
		column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);

		column_displayname = cursor.getColumnIndexOrThrow(MediaColumns.DISPLAY_NAME);



		while (cursor.moveToNext()) {

			String absolutePathOfImage = cursor.getString(column_index);
			nameOfFile = cursor.getString(column_displayname);

			if(absolutePathOfImage!=null && nameOfFile!=null){
				lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);

				lastIndex = lastIndex >= 0 ? lastIndex
						: nameOfFile.length() - 1;


				if (absolutePathOfImage != null) {
					mediaList.add(new File(absolutePathOfImage));
				}
			}
		}


		uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

		cursor = activity.managedQuery(uri, projection, null, null, null);
		column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);

		column_displayname = cursor.getColumnIndexOrThrow(MediaColumns.DISPLAY_NAME);

		while (cursor.moveToNext()) {

			String absolutePathOfVideo = cursor.getString(column_index);
			if(absolutePathOfVideo==null)
				continue;
			
			nameOfFile = cursor.getString(column_displayname);

			lastIndex = absolutePathOfVideo.lastIndexOf(nameOfFile);

			lastIndex = lastIndex >= 0 ? lastIndex
					: nameOfFile.length() - 1;

			if (absolutePathOfVideo != null) {
				mediaList.add(new File(absolutePathOfVideo));
			}

		}



		uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

		cursor = activity.managedQuery(uri, projection, null, null, null);
		column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);

		column_displayname = cursor.getColumnIndexOrThrow(MediaColumns.DISPLAY_NAME);

		while (cursor.moveToNext()) {

			try{
				String absolutePathOfAudio = cursor.getString(column_index);
				nameOfFile = cursor.getString(column_displayname);

				lastIndex = absolutePathOfAudio.lastIndexOf(nameOfFile);

				lastIndex = lastIndex >= 0 ? lastIndex
						: nameOfFile.length() - 1;

				if (absolutePathOfAudio != null) {
					mediaList.add(new File(absolutePathOfAudio));
				}
			}catch(Exception e){}

		}


		return mediaList;
	}













	public static String getTypeByGDocsMime(String gMime){
		if(gMime.contains("application/vnd.google-apps.audio"))
			return TYPE_AUDIO;	
		else if(gMime.contains("application/vnd.google-apps.document"))
			return TYPE_TEXT;	
		else if(gMime.contains("application/vnd.google-apps.drawing"))
			return TYPE_RASTER;	
		else if(gMime.contains("application/vnd.google-apps.file"))
			return TYPE_MISC;	 	
		else if(gMime.contains("application/vnd.google-apps.folder"))
			return TYPE_FOLDER;		
		else if(gMime.contains("application/vnd.google-apps.form"))
			return TYPE_VECTOR;	
		else if(gMime.contains("application/vnd.google-apps.fusiontable"))
			return TYPE_VECTOR;	
		else if(gMime.contains("application/vnd.google-apps.photo"))
			return TYPE_RASTER;		
		else if(gMime.contains("application/vnd.google-apps.presentation"))
			return TYPE_PRESENTATION;	
		else if(gMime.contains("application/vnd.google-apps.script"))	
			return TYPE_DEVELOPMENT;	
		else if(gMime.contains("application/vnd.google-apps.sites"))
			return TYPE_WEB;	
		else if(gMime.contains("application/vnd.google-apps.spreadsheet"))
			return TYPE_SPREADSHEET;		
		else if(gMime.contains("application/vnd.google-apps.unknown"))
			return TYPE_UNKNOWN;	
		else if(gMime.contains("application/vnd.google-apps.video"))
			return TYPE_VIDEO;	
		return TYPE_UNKNOWN;
	}
}
