package com.paolobriganti.utils;

import android.util.Patterns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaUtils {

	public JavaUtils (){

	}

	public static boolean isWebUrl(String inputUrl){
		if(isNotEmpty(inputUrl)){
			return Patterns.WEB_URL.matcher(inputUrl).matches();
		}
		return false;
	}

	public static ArrayList<Object> arrayToArrayList (Object[] array){
		ArrayList<Object> arrayList = new ArrayList<Object>(Arrays.asList(array));
		return arrayList;

	}

	public static Object[] arrayListToArray (ArrayList<Object> arrayList){
		Object[] array = new String[arrayList.size()];
		arrayList.toArray(array);
		return array;

	}


	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static boolean isEmailAddress(final String emailAddress){
		if(isNotEmpty(emailAddress)){

			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(emailAddress);
			return matcher.matches();
		}
		return false;
	}

	public static boolean isNotEmpty(final String string){  
		return string != null && !string.equals("") && !string.trim().equals("") && string.length()>0 && !isBlank(string);  
	}  
	public static boolean isEmpty(final String string){  
		return !isNotEmpty(string);  
	}  
	private static boolean isBlank(String str) {
		Integer strLen;
		if (str == null || (strLen = str.length()) == 0) {
			return true;
		}
		for (Integer i = 0; i < strLen; i++) {
			if ((Character.isWhitespace(str.charAt(i)) == false)) {
				return false;
			}
		}
		return true;
	}

	public static Integer getIntegerValueOfDouble(double doubleValue, Integer defaultValue){
		try{
			if(doubleValue>=Integer.MIN_VALUE && doubleValue<=Integer.MAX_VALUE)
				return (int) (double) doubleValue;
			else
				return defaultValue;
		}catch (NumberFormatException e){
			return defaultValue;
		}
	}

	public static Integer getIntegerValue(String string, Integer defaultValue){
		try{
			return Integer.valueOf(string);
		}catch (NumberFormatException e){
			return defaultValue;
		}
	}

	public static Integer getIntegerValue(Long longValue, Integer defaultValue){
		try{
			if(longValue>=Integer.MIN_VALUE && longValue<=Integer.MAX_VALUE)
				return (int) (long) longValue;
			else
				return defaultValue;
		}catch (NumberFormatException e){
			return defaultValue;
		}
	}

	public static float getFloatValue(String string, float defaultValue){
		try{
			return Float.valueOf(string);
		}catch (NumberFormatException e){
			return defaultValue;
		}
	}
	public static double getDoubleValue(String string, double defaultValue){
		try{
			return Double.valueOf(string);
		}catch (NumberFormatException e){
			return defaultValue;
		}
	}
	public static long getLongValue(String string, long defaultValue){
		try{
			return Long.valueOf(string);
		}catch (NumberFormatException e){
			return defaultValue;
		}
	}


	public static String convertStreamToString(InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	public static boolean getBooleanValue(String string, String stringTrue){
		return string.equals(stringTrue);
	}




	public static String getNodeValue(Node node){
		try{
			return node.getNodeValue().toString();
		}catch (NullPointerException e){
			return " ";
		}
	}


	public static InputStream cloneInputStream(InputStream input){
		if(input!=null){
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len;
			try {
				while ((len = input.read(buffer)) > -1 ) {
					baos.write(buffer, 0, len);
				}
				baos.flush();
				return new ByteArrayInputStream(baos.toByteArray()); 
			} catch (IOException e) {
				return null;
			}
		}
		return null;
	}



	public static boolean isJSONValid(String test) {
		if(JavaUtils.isEmpty(test))
			return false;

		try {
			new JSONObject(test);
		} catch (JSONException ex) {
			// edited, to include @Arthur's comment
			// e.g. in case JSONArray is valid as well...
			try {
				new JSONArray(test);
			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}


	public static Object[] arrayConcat(Object[] a, Object[] b) {
		if(a.getClass().equals(b.getClass())){
			int aLen = a.length;
			int bLen = b.length;
			Object[] c= new Object[aLen+bLen];
			System.arraycopy(a, 0, c, 0, aLen);
			System.arraycopy(b, 0, c, aLen, bLen);
			return c;
		}
		return null;
	}











}
