package com.paolobriganti.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

public class StringFile {

	public StringFile(){}

	//Metodi per scrivere e leggere un file String
	public static void write (Context context, String FILENAME, String string){
		FileOutputStream fos;
		try {
			fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
			fos.write(string.getBytes());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static String read (Context context, String FILENAME){
		FileInputStream fis = null;
		String txt = null;
		try {
			fis = context.openFileInput(FILENAME);
			int size = fis.available();

			byte[] content = new byte[size];
			fis.read(content);
			fis.close();

			txt = new String(content);

		} catch (FileNotFoundException e) {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return txt;
	}


	public static String read (Context context, InputStream fis){
		if(fis!=null){
			String txt = null;
			try {

				int size = fis.available();

				byte[] content = new byte[size];
				fis.read(content);
				fis.close();

				txt = new String(content);

			} catch (FileNotFoundException e) {

			} catch (Exception e) {
				e.printStackTrace();
			}
			return txt;
		}
		return null;
	}

	public static FileInputStream getFileInputStream (Context context, String FILENAME){
		FileInputStream fis = null;
		try {
			fis = context.openFileInput(FILENAME);

		} catch (FileNotFoundException e) {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fis;
	}




	public static boolean fileExists(Context c,String FILE){

		File file = c.getFileStreamPath(FILE);
		return file.exists();

	}

	//Eliminare un file
	public static boolean deleteFile(Context c,String FILE){

		File file = c.getFileStreamPath(FILE);
		if(file.exists()){
			return file.delete();
		}
		return false;
	}


	//Leggere e scrivere testo nella sdcard
	public static void writeToPath(String text, String path, String fileName){
		File file = new File(path, fileName);
		FileOutputStream fos;
		byte[] data = new String(text).getBytes();
		try {
			fos = new FileOutputStream(file);
			fos.write(data);
			fos.flush();
			fos.close();
		} catch (FileNotFoundException e) {
			// handle exception
		} catch (IOException e) {
			// handle exception
		}
	}
	public static boolean writeToPath(String text, String path){
		File file = new File(path);
		FileOutputStream fos;
		byte[] data = new String(text).getBytes();
		try {
			fos = new FileOutputStream(file);
			fos.write(data);
			fos.flush();
			fos.close();
			
			return true;
		} catch (FileNotFoundException e) {
			// handle exception
		} catch (IOException e) {
			// handle exception
		}
		return false;
	}

	public static String readFromPath (String filePath){
		return readFromPath(filePath, null);
	}
	
	public static String readFromPath (String filePath, Integer maxCharacters){


		//Get the text file
		File file = new File(filePath);

		if(file.exists()){

			//Read text from file
			StringBuilder text = new StringBuilder();

			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;

				while ((line = br.readLine()) != null) {
					text.append(line);
					text.append('\n');
					if(maxCharacters!=null && text.length()>maxCharacters){
						break;
					}
				}
			}
			catch (IOException e) {
				//You'll need to add proper error handling here
			}

			return text.toString();
		}
		return null;
	}


	public static String readFromAssets (Context c, String filePath){


		//Get the text file
		InputStream fis = null;
		try {
			fis = c.getAssets().open(filePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return read(c, fis);
	}
}
