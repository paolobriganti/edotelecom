package com.paolobriganti.utils.imageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;

/**
 * Created by PaoloBriganti on 11/06/15.
 */
public class ImageLoader extends ImageFetcher{
    public static final String CACHE_DIR_NAME_IMAGE = "images";
    public static final String CACHE_DIR_NAME_THUMB = "thumb";

    private int mImageThumbSpacing;

    public ImageLoader(FragmentActivity activity, String cacheDirName, int imageWidth, int imageHeight) {
        super(activity, imageWidth, imageHeight);
        initLoader(activity.getSupportFragmentManager(), cacheDirName);
    }

    public ImageLoader(FragmentActivity activity, String cacheDirName, int imageSize) {
        super(activity, -1, imageSize);
        initLoader(activity.getSupportFragmentManager(), cacheDirName);
    }

    public ImageLoader(FragmentManager fragmentManager, Context context, String cacheDirName, int imageWidth, int imageHeight) {
        super(context, imageWidth, imageHeight);
        initLoader(fragmentManager, cacheDirName);
    }

    public ImageLoader(FragmentManager fragmentManager, Context context, String cacheDirName, int imageSize) {
        super(context, -1, imageSize);
        initLoader(fragmentManager, cacheDirName);
    }



    public void initLoader(FragmentManager fragmentManager, String cacheDirName){

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(mContext, cacheDirName);
        cacheParams.setMemCacheSizePercent(0.25f);
        addImageCache(fragmentManager, cacheParams);
    }


    public int getLongestSidePixel(Activity activity){
        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;
        int longest = (height > width ? height : width) / 2;
        return longest;
    }

    public int getImageHeight(){
        return mImageHeight;
    }

    public int getWidthHeight(){
        return mImageWidth;
    }


    public Bitmap decodeBitmapFromHttpUrl(String url){
       return processBitmapFromHttpUrl(url);
    }

}
