package com.paolobriganti.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;


public class ImageUtils {


	//*****************************************************************
	//		FILES
	//*****************************************************************	

	public static boolean saveBitmapToFile(Bitmap bmp, CompressFormat format, String filePath, String fileName){
		try {
			if(filePath==null)
				filePath = android.os.Environment.DIRECTORY_DCIM+File.separator+"temp";

			File file = new File(filePath);
			file.mkdirs();

			String path = filePath+File.separator+fileName;

			FileOutputStream out = new FileOutputStream(path);
			bmp.compress(format, 100, out);
			out.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Bitmap loadFrom(Context context, InputStream is) {
		try {
			try {
				return BitmapFactory.decodeStream(is);
			} finally {
				is.close();                    
			}
		} catch (Exception e) {
			return null;
		}
	}







	//*****************************************************************
	//		COMBINATIONS
	//*****************************************************************	

	public static Bitmap collage(ArrayList<Bitmap> parts){
		Bitmap result = Bitmap.createBitmap(parts.get(0).getWidth() * 2, parts.get(0).getHeight() * 2, Config.ARGB_8888);
		Canvas canvas = new Canvas(result);
		Paint paint = new Paint();
		for (int i = 0; i < parts.size(); i++) {
			canvas.drawBitmap(parts.get(i), parts.get(i).getWidth() * (i % 2), parts.get(i).getHeight() * (i / 2), paint);
		}
		return result;
	}

	public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
		Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(bmp1, new Matrix(), null);
		int x = (bmp1.getWidth()/2)-((bmp2.getWidth()/2));
		int y = (bmp1.getHeight()/2)-((bmp2.getHeight()/2));
		canvas.drawBitmap(bmp2, x, y, null);
		return bmOverlay;
	}

	public static Bitmap overlayFillTheSecond(Bitmap bmp1, Bitmap bmp2) {
		Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
		bmp2 = Bitmap.createScaledBitmap(bmp2, bmp1.getWidth(), bmp1.getHeight(), true);
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(bmp1, new Matrix(), null);
		int x = (bmp1.getWidth()/2)-((bmp2.getWidth()/2));
		int y = (bmp1.getHeight()/2)-((bmp2.getHeight()/2));
		canvas.drawBitmap(bmp2, x, y, null);
		return bmOverlay;
	}















	//*****************************************************************
	//		TEXT TO BITMAP
	//*****************************************************************


	public static Bitmap textUCToBitmap(Context c, String text, String typeface, float size, int color){

		final float density = c.getResources().getDisplayMetrics().density;
		final float textSizeDips = size;
		final float textSizePixels = Math.round(textSizeDips * density);



		Paint paint = new Paint();
		paint.setTextSize(textSizePixels);
		paint.setTextAlign(Paint.Align.LEFT);
		paint.setAntiAlias(true);
		paint.setSubpixelText(true);
		paint.setColor(color);
		if(typeface!=null) {
			Typeface tf = Typeface.createFromAsset(c.getAssets(), typeface);
			paint.setTypeface(tf);
		}
		if(JavaUtils.isEmpty(text)){
			text=" ";
		}
		int textwidth = (int) (paint.measureText(text)); 
		int textheight = - (int)(paint.ascent()/1.1);

		int width = (int) (textwidth + 0.5f); 
		int height = (int) (textheight + 0.5f); 
		if(width<1)width=1;
		if(height<1)height=1;
		Bitmap image = Bitmap.createBitmap(width, height, Config.ARGB_8888);

		float baseline = paint.ascent();
		int y = (int) (baseline*(-1)-(int) (paint.descent()/1.5));
		Canvas canvas = new Canvas(image);
		canvas.drawText(text, 0, y, paint);




		return image;

	}


	public static Bitmap textUCCToBitmap(Context c, String text, String typeface, float size, int color){

		final float density = c.getResources().getDisplayMetrics().density;
		final float textSizeDips = size;
		final float textSizePixels = Math.round(textSizeDips * density);



		Paint paint = new Paint();
		paint.setTextSize(textSizePixels);
		paint.setTextAlign(Paint.Align.LEFT);
		paint.setAntiAlias(true);
		paint.setSubpixelText(true);
		paint.setColor(color);
		if(typeface!=null) {
			Typeface tf = Typeface.createFromAsset(c.getAssets(), typeface);
			paint.setTypeface(tf);
		}
		if(JavaUtils.isEmpty(text)){
			text=" ";
		}
		int textwidth = (int) (paint.measureText(text)); 
		int textheight = (int) paint.descent() - (int)paint.ascent();
		int width = (int) (textwidth + 0.5f); 
		int height = (int) (textheight + 0.5f); 
		Bitmap image = Bitmap.createBitmap(width, height, Config.ARGB_8888);

		float baseline = paint.ascent();
		int y = (int) (baseline*(-1)-(int) (paint.descent()/3));
		Canvas canvas = new Canvas(image);
		canvas.drawText(text, 0, y, paint);


		return image;

	}


	public static Bitmap textToBitmapRect(Context c, String text, String typeface, int size, int color){

		final float density = c.getResources().getDisplayMetrics().density;
		final float textSizeDips = (float) size;
		final float textSizePixels = Math.round(textSizeDips * density);



		Paint paint = new Paint();
		paint.setTextSize(textSizePixels);
		paint.setTextAlign(Paint.Align.LEFT);
		paint.setAntiAlias(true);
		paint.setSubpixelText(true);
		paint.setColor(color);
		if(typeface!=null) {
			Typeface tf = Typeface.createFromAsset(c.getAssets(), typeface);
			paint.setTypeface(tf);
		}else{
			paint.setTypeface(Typeface.DEFAULT);
		}
		if(JavaUtils.isEmpty(text)){
			text=" ";
		}
		int textwidth = (int) (paint.measureText(text)); 
		int textheight = (int) paint.descent() - (int)paint.ascent();
		int width = (int) (textwidth + 0.5f); 
		int height = (int) (textheight + 0.5f); 
		Bitmap image = Bitmap.createBitmap(width, height, Config.ARGB_8888);

		float baseline = paint.ascent();
		int y = (int) (baseline*(-1)-(int) (paint.descent()/3));
		Canvas canvas = new Canvas(image);
		canvas.drawText(text, 0, y, paint);



		paint.setAntiAlias(true);
		paint.setColor(color);
		paint.setStyle(Style.STROKE);
		canvas.drawRect(0, 0, textwidth, textheight, paint);


		return image;

	}





	//*****************************************************************
	//		BITMAP GENERATION
	//*****************************************************************

	public static Bitmap getCircleFill(Context c, int radius, int color){

		final float density = c.getResources().getDisplayMetrics().density;
		final int rectWidth = Math.round(radius * density);
		final int rectHeight = Math.round(radius * density);

		Bitmap image = Bitmap.createBitmap(rectWidth, rectHeight, Config.ARGB_8888);

		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(color);
		paint.setStyle(Style.FILL);

		Canvas canvas = new Canvas(image);
		int cx = rectWidth/2;
		int cy = rectHeight/2;
		canvas.drawCircle(cx, cy, radius, paint);
		return image;

	}






	//*****************************************************************
	//		CUT AND CROP
	//*****************************************************************

	public static Bitmap getCroppedSquareCenterBitmap(Bitmap srcBmp){

		if (srcBmp.getWidth() >= srcBmp.getHeight()){
			return Bitmap.createBitmap(
					srcBmp, 
					srcBmp.getWidth()/2 - srcBmp.getHeight()/2,
					0,
					srcBmp.getHeight(), 
					srcBmp.getHeight()
					);
		}else{
			return Bitmap.createBitmap(
					srcBmp,
					0, 
					srcBmp.getHeight()/2 - srcBmp.getWidth()/2,
					srcBmp.getWidth(),
					srcBmp.getWidth() 
					);
		}

	}

	public static Bitmap getCroppedCircleCenterBitmap(Bitmap sourceBitmap, Integer sideSize) {
		// TODO Auto-generated method stub

		Paint paint = new Paint();
		paint.setAntiAlias(true);

		if(sourceBitmap.getWidth()!=sourceBitmap.getHeight()){
			sourceBitmap = getCroppedSquareCenterBitmap(sourceBitmap);
		}
		
		if(sideSize==null){
			if (sourceBitmap.getWidth() <= sourceBitmap.getHeight()){
				sideSize = sourceBitmap.getWidth();
			}else{
				sideSize = sourceBitmap.getHeight();
			}
		}

		int targetWidth = sideSize;
		int targetHeight = sideSize;

		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight, Config.ARGB_8888);
		
		
		Canvas canvas = new Canvas(targetBitmap);
	    Path path = new Path();
	    path.addCircle(((float) targetWidth - 1) / 2,
	            ((float) targetHeight - 1) / 2,
	            (Math.min(((float) targetWidth), 
	                    ((float) targetHeight)) / 2),
	                    Path.Direction.CCW);

	    canvas.clipPath(path);
	    canvas.drawBitmap(sourceBitmap, 
	            new Rect(0, 0, sourceBitmap.getWidth(),
	                    sourceBitmap.getHeight()), 
	                    new Rect(0, 0, targetWidth, targetHeight), new Paint(Paint.FILTER_BITMAP_FLAG));
		return targetBitmap;
	}

	public static Bitmap getCroppedSquareCenterScaleBitmap(Bitmap srcBmp, int sideSize){

		if (srcBmp.getWidth() >= srcBmp.getHeight()){
			int width = (int)((float)(srcBmp.getWidth()*sideSize)/srcBmp.getHeight());
			srcBmp = Bitmap.createScaledBitmap(srcBmp, width, sideSize, true);
		}else{
			int height = (int)((float)(srcBmp.getHeight()*sideSize)/srcBmp.getWidth());
			srcBmp = Bitmap.createScaledBitmap(srcBmp, sideSize, height, true);
		}
		srcBmp = getCroppedSquareCenterBitmap(srcBmp);
		return srcBmp;
	}








	//*****************************************************************
	//		OBJECTS
	//*****************************************************************




	public static Bitmap resourceToBitmap(Context c, int res){
		Bitmap bitmap = BitmapFactory.decodeResource(c.getResources(), res);
		return bitmap;
	}

	public static Drawable resourceToDrawable(Context c, int res){
		Drawable d = c.getResources().getDrawable(res);
		return d;
	}

	public static Bitmap drawableToBitmap(Context c, Drawable drawable){
		Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
		return bitmap;
	}

	public static Drawable bitmapToDrawable(Context c, Bitmap bitmap){
		Drawable d = new BitmapDrawable(c.getResources(), bitmap);
		return d;

	}


	public static Bitmap noBitmap(){
		return Bitmap.createBitmap(1, 1, Config.RGB_565);
	}

	public static Drawable noDrawable(Context c){
		return new BitmapDrawable(c.getResources(),noBitmap());
	}








	
	
	
	
	
	
	
	
	
	
	//*****************************************************************
	//		TRANSFORMATIONS
	//*****************************************************************
	public static Bitmap fastblur(Bitmap sentBitmap, int radius) {

		// Stack Blur v1.0 from
		// http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
		//
		// Java Author: Mario Klingemann <mario at quasimondo.com>
		// http://incubator.quasimondo.com
		// created Feburary 29, 2004
		// Android port : Yahel Bouaziz <yahel at kayenko.com>
		// http://www.kayenko.com
		// ported april 5th, 2012

		// This is a compromise between Gaussian Blur and Box blur
		// It creates much better looking blurs than Box Blur, but is
		// 7x faster than my Gaussian Blur implementation.
		//
		// I called it Stack Blur because this describes best how this
		// filter works internally: it creates a kind of moving stack
		// of colors whilst scanning through the image. Thereby it
		// just has to add one new block of color to the right side
		// of the stack and remove the leftmost color. The remaining
		// colors on the topmost layer of the stack are either added on
		// or reduced by one, depending on if they are on the right or
		// on the left side of the stack.
		//
		// If you are using this algorithm in your code please add
		// the following line:
		//
		// Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

		Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

		if (radius < 1) {
			return (null);
		}

		int w = bitmap.getWidth();
		int h = bitmap.getHeight();

		int[] pix = new int[w * h];
		bitmap.getPixels(pix, 0, w, 0, 0, w, h);

		int wm = w - 1;
		int hm = h - 1;
		int wh = w * h;
		int div = radius + radius + 1;

		int r[] = new int[wh];
		int g[] = new int[wh];
		int b[] = new int[wh];
		int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
		int vmin[] = new int[Math.max(w, h)];

		int divsum = (div + 1) >> 1;
		divsum *= divsum;
		int dv[] = new int[256 * divsum];
		for (i = 0; i < 256 * divsum; i++) {
			dv[i] = (i / divsum);
		}

		yw = yi = 0;

		int[][] stack = new int[div][3];
		int stackpointer;
		int stackstart;
		int[] sir;
		int rbs;
		int r1 = radius + 1;
		int routsum, goutsum, boutsum;
		int rinsum, ginsum, binsum;

		for (y = 0; y < h; y++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			for (i = -radius; i <= radius; i++) {
				p = pix[yi + Math.min(wm, Math.max(i, 0))];
				sir = stack[i + radius];
				sir[0] = (p & 0xff0000) >> 16;
			sir[1] = (p & 0x00ff00) >> 8;
		sir[2] = (p & 0x0000ff);
		rbs = r1 - Math.abs(i);
		rsum += sir[0] * rbs;
		gsum += sir[1] * rbs;
		bsum += sir[2] * rbs;
		if (i > 0) {
			rinsum += sir[0];
			ginsum += sir[1];
			binsum += sir[2];
		} else {
			routsum += sir[0];
			goutsum += sir[1];
			boutsum += sir[2];
		}
			}
			stackpointer = radius;

			for (x = 0; x < w; x++) {

				r[yi] = dv[rsum];
				g[yi] = dv[gsum];
				b[yi] = dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (y == 0) {
					vmin[x] = Math.min(x + radius + 1, wm);
				}
				p = pix[yw + vmin[x]];

				sir[0] = (p & 0xff0000) >> 16;
			sir[1] = (p & 0x00ff00) >> 8;
			sir[2] = (p & 0x0000ff);

			rinsum += sir[0];
			ginsum += sir[1];
			binsum += sir[2];

			rsum += rinsum;
			gsum += ginsum;
			bsum += binsum;

			stackpointer = (stackpointer + 1) % div;
			sir = stack[(stackpointer) % div];

			routsum += sir[0];
			goutsum += sir[1];
			boutsum += sir[2];

			rinsum -= sir[0];
			ginsum -= sir[1];
			binsum -= sir[2];

			yi++;
			}
			yw += w;
		}
		for (x = 0; x < w; x++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			yp = -radius * w;
			for (i = -radius; i <= radius; i++) {
				yi = Math.max(0, yp) + x;

				sir = stack[i + radius];

				sir[0] = r[yi];
				sir[1] = g[yi];
				sir[2] = b[yi];

				rbs = r1 - Math.abs(i);

				rsum += r[yi] * rbs;
				gsum += g[yi] * rbs;
				bsum += b[yi] * rbs;

				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}

				if (i < hm) {
					yp += w;
				}
			}
			yi = x;
			stackpointer = radius;
			for (y = 0; y < h; y++) {
				// Preserve alpha channel: ( 0xff000000 & pix[yi] )
				pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

			rsum -= routsum;
			gsum -= goutsum;
			bsum -= boutsum;

			stackstart = stackpointer - radius + div;
			sir = stack[stackstart % div];

			routsum -= sir[0];
			goutsum -= sir[1];
			boutsum -= sir[2];

			if (x == 0) {
				vmin[y] = Math.min(y + r1, hm) * w;
			}
			p = x + vmin[y];

			sir[0] = r[p];
			sir[1] = g[p];
			sir[2] = b[p];

			rinsum += sir[0];
			ginsum += sir[1];
			binsum += sir[2];

			rsum += rinsum;
			gsum += ginsum;
			bsum += binsum;

			stackpointer = (stackpointer + 1) % div;
			sir = stack[stackpointer];

			routsum += sir[0];
			goutsum += sir[1];
			boutsum += sir[2];

			rinsum -= sir[0];
			ginsum -= sir[1];
			binsum -= sir[2];

			yi += w;
			}
		}

		bitmap.setPixels(pix, 0, w, 0, 0, w, h);

		return (bitmap);
	}




}
