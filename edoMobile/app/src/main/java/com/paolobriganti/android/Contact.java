package com.paolobriganti.android;


import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.paolobriganti.utils.JavaUtils;

import java.io.InputStream;
import java.util.ArrayList;

public class Contact {



	private Long ID = null;
	private String name = null;
	private String defaultPhoneNumber = null;
	private ArrayList<String> phoneNumbers = null;
	private String defaultEmail = null;
	private ArrayList<String> emails = null;
	private InputStream photo = null;

	public Contact(){}

	public Contact(Long ID, String name, ArrayList<String> phoneNumbers, String defaultPhoneNumber,
			ArrayList<String> emails, String defaultEmail) {
		super();
		this.ID = ID;
		this.name = name;
		this.defaultPhoneNumber = defaultPhoneNumber;
		this.phoneNumbers = phoneNumbers;
		this.defaultEmail = defaultEmail;
		this.emails = emails;
	}

	public Contact(Context c, String name, ArrayList<String> phoneNumbers, String defaultPhoneNumber,
			ArrayList<String> emails, String defaultEmail) {
		super();

		if(JavaUtils.isEmpty(defaultPhoneNumber) && phoneNumbers!=null && phoneNumbers.size()>0)
			defaultPhoneNumber=phoneNumbers.get(0);

		if(JavaUtils.isEmpty(defaultEmail) && emails!=null && emails.size()>0)
			defaultEmail=emails.get(0);

		if(JavaUtils.isNotEmpty(defaultPhoneNumber)){
			this.ID = ContactsManager.getContactIDFromPhoneNumber(c, defaultPhoneNumber);
		}else if(JavaUtils.isNotEmpty(defaultPhoneNumber)){
			this.ID = ContactsManager.getContactIDFromEmail(c, defaultEmail);
		}

		this.name = name;
		this.defaultPhoneNumber = defaultPhoneNumber;
		this.phoneNumbers = phoneNumbers;
		this.defaultEmail = defaultEmail;
		this.emails = emails;

	}
	
	
	public Contact(Context c, Cursor contactCursor){
		Long id = contactCursor.getLong(contactCursor
				.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

		String name = contactCursor.getString(contactCursor
				.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));

		//Tel numbers
		ArrayList<String> telephone_numbers = new ArrayList<String>();
		try{
			if (Integer.parseInt(contactCursor.getString(
					contactCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
				Cursor phoneCur = c.getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null, 
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?", 
						new String[]{String.valueOf(id)}, null);
				while (phoneCur.moveToNext()) {

					String tel = phoneCur.getString(phoneCur.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
					telephone_numbers.add(tel);

				} 
				phoneCur.close();
			}
		}catch(NullPointerException e){}

		//Email
		ArrayList<String> emails = new ArrayList<String>();
		Cursor emailCur = c.getContentResolver().query( 
				ContactsContract.CommonDataKinds.Email.CONTENT_URI, 
				null,
				ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", 
				new String[]{String.valueOf(id)}, null); 
		try{
			while (emailCur.moveToNext()) { 
				// This would allow you get several email addresses
				// if the email addresses were stored in an array
				String email = emailCur.getString(
						emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				emailCur.getString(
						emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
				emails.add(email);
			} 
			emailCur.close();
		}catch(NullPointerException e){
		}

		this.ID = id;
		this.name = name;
		this.phoneNumbers = telephone_numbers;
		this.emails = emails;
	}
	
	

	public Contact(Context c, String name, String defaultPhoneNumber, String defaultEmail) {
		super();



		if(JavaUtils.isNotEmpty(defaultPhoneNumber)){
			this.ID = ContactsManager.getContactIDFromPhoneNumber(c, defaultPhoneNumber);
		}else if(JavaUtils.isNotEmpty(defaultPhoneNumber)){
			this.ID = ContactsManager.getContactIDFromEmail(c, defaultEmail);
		}
		this.name = name;
		this.defaultPhoneNumber = defaultPhoneNumber;
		this.defaultEmail = defaultEmail;

		if(JavaUtils.isNotEmpty(defaultPhoneNumber)){
			phoneNumbers = new ArrayList <String>();
			phoneNumbers.add(defaultPhoneNumber);
		}

		if(JavaUtils.isNotEmpty(defaultEmail)){
			emails = new ArrayList <String>();
			emails.add(defaultEmail);
		}
	}



	public Contact(Context c, String name, ArrayList<String> phoneNumbers, ArrayList<String> emails) {
		super();

		if(JavaUtils.isNotEmpty(defaultPhoneNumber)){
			this.ID = ContactsManager.getContactIDFromPhoneNumber(c, defaultPhoneNumber);
		}else if(JavaUtils.isNotEmpty(defaultPhoneNumber)){
			this.ID = ContactsManager.getContactIDFromEmail(c, defaultEmail);
		}

		this.name = name;
		if(phoneNumbers!=null && phoneNumbers.size()>0)
			this.defaultPhoneNumber=phoneNumbers.get(0);
		this.phoneNumbers = phoneNumbers;
		if(emails!=null && emails.size()>0)
			this.defaultEmail=emails.get(0);
		this.emails = emails;

	}


	public Contact(Context c, Long ID) {
		super();
		this.ID = ID;
		this.name = ContactsManager.getContactName(c, ID);
		this.phoneNumbers = ContactsManager.getContactPhoneNumbers(c, ID);
		if(phoneNumbers!=null && phoneNumbers.size()>0)
			this.defaultPhoneNumber=phoneNumbers.get(0);
		this.emails = ContactsManager.getContactEmails(c, ID);
		if(emails!=null && emails.size()>0)
			this.defaultEmail=emails.get(0);

	}

	public Long getID() {
		return ID;
	}


	public void setID(Long ID) {
		this.ID = ID;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public ArrayList<String> getPhoneNumbers() {
		return phoneNumbers;
	}


	public String getDefaultPhoneNumber() {
		return defaultPhoneNumber;
	}


	public void setDefaultPhoneNumber(String defaultPhoneNumber) {
		this.defaultPhoneNumber = defaultPhoneNumber;
	}

	public void setPhoneNumbers(ArrayList<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}



	public String getDefaultEmail() {
		return defaultEmail;
	}


	public void setDefaultEmail(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}

	public ArrayList<String> getEmails() {
		return emails;
	}


	public void setEmails(ArrayList<String> emails) {
		this.emails = emails;
	}






	public InputStream getPhoto(Context c) {
		if(c!=null && photo==null)
			return ContactsManager.getContactPhotoInputStream(c, ID);
		else if(photo!=null)
			return photo;
		return null;
	}

	public void setPhoto(InputStream photo) {
		this.photo = photo;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (defaultEmail != null && other.defaultEmail != null && !defaultEmail.equals(other.defaultEmail))
			if (defaultPhoneNumber != null && other.defaultPhoneNumber != null && !defaultPhoneNumber.equals(other.defaultPhoneNumber))
				return false;
		return true;
	}








}
