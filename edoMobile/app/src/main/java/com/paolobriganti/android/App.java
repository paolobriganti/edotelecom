package com.paolobriganti.android;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;




public class App {

	private String name = null;
	private String packageName = null;
	private String firstActivity = null;
	private Drawable icon = null;

	public App (String name, String packageName, String firstActivity){
		this.name = name;
		this.packageName = packageName;
		this.firstActivity = firstActivity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getFirstActivity() {
		return firstActivity;
	}

	public void setFirstActivity(String firstActivity) {
		this.firstActivity = firstActivity;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		App other = (App) obj;
		if (packageName == null) {
			if (other.packageName != null)
				return false;
		} else if (!packageName.equals(other.packageName))
			return false;
		return true;
	}

	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

	public void setIcon(Context c, Bitmap icon) {
		this.icon = new BitmapDrawable(c.getResources(), icon);
	}

	public Drawable getIcon(Context c){
		if(icon!=null){
			return icon;
		}else{
			return AppManager.getAppIconDrawable(c, packageName, firstActivity);
		}
	}

	public Bitmap getIconBitmap(Context c){
		Bitmap bitmap = null;
		if(icon==null){
			bitmap = ((BitmapDrawable)getIcon(c)).getBitmap();
		}else{
			bitmap = ((BitmapDrawable)icon).getBitmap();
		}
		return bitmap;
	}

	@Override
	public String toString() {
		return "App [name=" + name + ", packageName=" + packageName
				+ ", firstActivity=" + firstActivity + "]";
	}






	public void run(Context c){
		Intent i = new Intent();

		ComponentName distantActivity = new ComponentName(packageName, firstActivity);
		i.setComponent(distantActivity);
		i.setAction(Intent.ACTION_MAIN);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		c.startActivity(i);
	}

	public Intent getIntent(Context c){
		Intent i = new Intent();

		ComponentName distantActivity = new ComponentName(packageName, firstActivity);
		i.setComponent(distantActivity);
		i.setAction(Intent.ACTION_MAIN);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return i;
	}

	public PendingIntent getPendingIntent(Context c){
		Intent i = new Intent();
		ComponentName distantActivity = new ComponentName(packageName, firstActivity);
		i.setComponent(distantActivity);
		i.setAction(Intent.ACTION_MAIN);
		return PendingIntent.getActivity(c, 0, i, 0);
	}


	public boolean isInstalled(Context c){
		PackageManager pm = c.getPackageManager();
		boolean installed = false;
		try {
			pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
			installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			installed = false;
		}
		return installed;

	}


	public Intent getPlayStoreIntent(){
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)); 
		goToMarket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return goToMarket;
	}





}
