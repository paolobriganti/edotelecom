package com.paolobriganti.android;

import android.R;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class AUtils {


	//*********************************************************************
	//*		ASYNCTASK
	//*********************************************************************


	public static boolean executeAsyncTask(AsyncTask<String, ?, ?> asyncTask, String... params){
		//		BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(ASYNC_TASK_MAXIMUM_POOL_SIZE);
		//		Executor threadPoolExecutor = new ThreadPoolExecutor(ASYNC_TASK_CORE_POOL_SIZE, ASYNC_TASK_MAXIMUM_POOL_SIZE, ASYNC_TASK_KEEP_ALIVE_TIME, TimeUnit.SECONDS, workQueue);
		try{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
				asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
			else
				asyncTask.execute(params);
			return true;
		}catch(java.util.concurrent.RejectedExecutionException e){
			Log.e("ASYNCTASK", "A LOT OF TASKS");
		}catch(OutOfMemoryError e){
			Log.e("ASYNCTASK", "OUT OF MEMORY");
		}catch(Exception e){
			Log.e("ASYNCTASK", "EXCEPTION");
		}
		return false;
	}



	//*********************************************************************
	//*		SDCARD
	//*********************************************************************


	public static void copyAssetsToSdCard(Context c, String fileName, String SDCardDir) {
		File dir = new File(Environment.getExternalStorageDirectory().getPath()+ SDCardDir);
		dir.mkdirs();
		AssetManager assetManager = c.getAssets();
		InputStream in = null;
		OutputStream out = null;
		try {
			in = assetManager.open(fileName);
			out = new FileOutputStream(Environment.getExternalStorageDirectory().getPath()+ SDCardDir + fileName);
			copyFile(in, out);
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
		} catch(IOException e) {

		}
	}
	private static void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while((read = in.read(buffer)) != -1){
			out.write(buffer, 0, read);
		}
	}


	public static void forceMediaUpdate(Context c, String filePath){
		c.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + filePath)));
	}
	
	

	//*********************************************************************
	//*		BROADCASTING
	//*********************************************************************

	public void sendBroadcastAction(Context c, String BROADCAST_ACTION){
		Intent intent = new Intent(BROADCAST_ACTION);
		c.sendBroadcast(intent);
	}


	//*********************************************************************
	//*		DISPLAY
	//*********************************************************************

	public static int getPixelsFromDP(Context c, int dp){
		final float density = c.getResources().getDisplayMetrics().density;
		return (int) (dp * density + 0.5f);
	}


	//*********************************************************************
	//*		CLIPBOARD
	//*********************************************************************

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static boolean copyToClipboard(Context context, String text) {
		try {
			int sdk = Build.VERSION.SDK_INT;
			if (sdk < Build.VERSION_CODES.HONEYCOMB) {
				android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
						.getSystemService(Context.CLIPBOARD_SERVICE);
				clipboard.setText(text);
			} else {
				ClipboardManager clipboard = (ClipboardManager) context
						.getSystemService(Context.CLIPBOARD_SERVICE);
				ClipData clip = ClipData
						.newPlainText(
								context.getResources().getString(
										R.string.copy), text);
				clipboard.setPrimaryClip(clip);
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@SuppressLint("NewApi")
	public static String readFromClipboard(Context context) {
		int sdk = Build.VERSION.SDK_INT;
		if (sdk < Build.VERSION_CODES.HONEYCOMB) {
			android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
					.getSystemService(Context.CLIPBOARD_SERVICE);
			return clipboard.getText().toString();
		} else {
			ClipboardManager clipboard = (ClipboardManager) context
					.getSystemService(Context.CLIPBOARD_SERVICE);

			// Gets a content resolver instance
			ContentResolver cr = context.getContentResolver();

			// Gets the clipboard data from the clipboard
			ClipData clip = clipboard.getPrimaryClip();
			if (clip != null) {

				String text = null;
				String title = null;

				// Gets the first item from the clipboard data
				ClipData.Item item = clip.getItemAt(0);

				// Tries to get the item's contents as a URI pointing to a note
				Uri uri = item.getUri();

				// If the contents of the clipboard wasn't a reference to a
				// note, then
				// this converts whatever it is to text.
				if (text == null) {
					text = coerceToText(context, item).toString();
				}

				return text;
			}
		}
		return "";
	}

	@SuppressLint("NewApi")
	public static CharSequence coerceToText(Context context, ClipData.Item item) {
		// If this Item has an explicit textual value, simply return that.
		CharSequence text = item.getText();
		if (text != null) {
			return text;
		}

		// If this Item has a URI value, try using that.
		Uri uri = item.getUri();
		if (uri != null) {

			// First see if the URI can be opened as a plain text stream
			// (of any sub-type). If so, this is the best textual
			// representation for it.
			FileInputStream stream = null;
			try {
				// Ask for a stream of the desired type.
				AssetFileDescriptor descr = context.getContentResolver()
						.openTypedAssetFileDescriptor(uri, "text/*", null);
				stream = descr.createInputStream();
				InputStreamReader reader = new InputStreamReader(stream,
						"UTF-8");

				// Got it... copy the stream into a local string and return it.
				StringBuilder builder = new StringBuilder(128);
				char[] buffer = new char[8192];
				int len;
				while ((len = reader.read(buffer)) > 0) {
					builder.append(buffer, 0, len);
				}
				return builder.toString();

			} catch (FileNotFoundException e) {
				// Unable to open content URI as text... not really an
				// error, just something to ignore.

			} catch (IOException e) {
				// Something bad has happened.
				return e.toString();

			} finally {
				if (stream != null) {
					try {
						stream.close();
					} catch (IOException e) {
					}
				}
			}

			// If we couldn't open the URI as a stream, then the URI itself
			// probably serves fairly well as a textual representation.
			return uri.toString();
		}

		// Finally, if all we have is an Intent, then we can just turn that
		// into text. Not the most user-friendly thing, but it's something.
		Intent intent = item.getIntent();
		if (intent != null) {
			return intent.toUri(Intent.URI_INTENT_SCHEME);
		}

		// Shouldn't get here, but just in case...
		return "";
	}



	public static void sendPushNotificationHeartBeat(Context c){
		c.sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
		c.sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));
	}

}
