package com.paolobriganti.android;


import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.PhoneLookup;

import com.paolobriganti.utils.ImageUtils;
import com.paolobriganti.utils.JavaUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import io.edo.utilities.eLog;


public class ContactsManager {

	public static final int ACTION_CALL = 1;
	public static final int ACTION_SEND_SMS = 2;
	public static final int ACTION_SEND_EMAIL = 3;
	public static final int ACTION_SHOW_INFO = 4;


	public static Long getContactIDFromPhoneNumber(Context c, String phoneNumber) {
		Long contactId = null;
		try{
			if(JavaUtils.isNotEmpty(phoneNumber)){
				Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
						Uri.encode(phoneNumber));
				if(uri!=null){
					Cursor cursor = c.getContentResolver().query(uri,
							new String[] { PhoneLookup.DISPLAY_NAME, PhoneLookup._ID },
							null, null, null);
					if (cursor!=null && cursor.moveToFirst()) {
						do {
							contactId = cursor.getLong(cursor
									.getColumnIndex(PhoneLookup._ID));
						} while (cursor.moveToNext());
					}
				}
			}
		}catch (NullPointerException e){
			contactId = null;
		}

		return contactId;
	}


	public static Long getContactIDFromEmail(Context c, String email){
		Long contactId = null;
		ContentResolver cr = c.getContentResolver();
		String[] PROJECTION = new String[] { CommonDataKinds.Email.CONTACT_ID };
		Cursor cur = cr.query(CommonDataKinds.Email.CONTENT_URI, PROJECTION,
				CommonDataKinds.Email.DATA +" = ?",
				new String[]{email}, null);

		if(cur!=null){
			if (cur.moveToFirst()) {
				contactId = cur.getLong(0);
			}

			cur.close();
		}

		return contactId;

	}

	public static String getContactName(Context c, Long contactID){
		ContentResolver cr = c.getContentResolver();
		Cursor cursor = cr.query(
				ContactsContract.Contacts.CONTENT_URI, 
				null, 
				ContactsContract.Contacts._ID +" = ?", 
				new String[]{String.valueOf(contactID)}, null);

		if (cursor!=null) {
			String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
			cursor.close();
			return name;
		}else{
			return null;
		}
	}

	public static ArrayList<String> getContactPhoneNumbers(Context c, Long contactID){
		ArrayList<String> phones = new ArrayList<String>();
		ContentResolver cr = c.getContentResolver();
		Cursor cursor = cr.query(
				CommonDataKinds.Phone.CONTENT_URI,
				null, 
				CommonDataKinds.Phone.CONTACT_ID +" = ?",
				new String[]{String.valueOf(contactID)}, null);

		if (cursor!=null && Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
			while (cursor.moveToNext()) 
			{
				phones.add(cursor.getString(cursor.getColumnIndex(CommonDataKinds.Phone.NUMBER)));
			} 
		}

		cursor.close();
		return(phones);
	}

	public static ArrayList<String> getContactEmails(Context c, Long contactID){
		ArrayList<String> emails = new ArrayList<String>();
		ContentResolver cr = c.getContentResolver();
		Cursor cursor = cr.query(
				CommonDataKinds.Email.CONTENT_URI,
				null,
				CommonDataKinds.Email.CONTACT_ID+" = ?",
				new String[]{String.valueOf(contactID)}, null);

		if (cursor!=null) {
			while (cursor.moveToNext()) 
			{
				emails.add(cursor.getString(cursor.getColumnIndex(CommonDataKinds.Email.DATA)));
			} 
		}

		cursor.close();
		return(emails);
	}

	public static InputStream getContactPhotoInputStream(Context c, Long contactID) {
		if(contactID!=null){
			ContentResolver cr = c.getContentResolver();
			Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactID);
			InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
			return input;
		}
		return null;
	}

	public static Bitmap getContactPhoto(Context c, Long contactID) {

		if(contactID!=null){
			Uri photoUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactID);
			try{
				InputStream is = ContactsContract.Contacts.openContactPhotoInputStream(c.getContentResolver(), photoUri);
				if(is!=null){
					Bitmap cPhoto = ImageUtils.loadFrom(c, is);
					if(cPhoto!=null){
						return cPhoto;
					}
				}
			}catch (IllegalArgumentException e){}
		}
		return null;
	}

	public static String getContactNameFromPhoneNumber(Context context, final String phoneNumber) {
		Uri uri;
		String[] projection;
		Uri mBaseUri = Contacts.Phones.CONTENT_FILTER_URL;
		projection = new String[] { Contacts.People.NAME };
		try {
			Class<?> c = Class
					.forName("android.provider.ContactsContract$PhoneLookup");
			mBaseUri = (Uri) c.getField("CONTENT_FILTER_URI").get(mBaseUri);
			projection = new String[] { "display_name" };
		} catch (Exception e) {
		}
		uri = Uri.withAppendedPath(mBaseUri, Uri.encode(phoneNumber));
		Cursor cursor = context.getContentResolver().query(uri, projection, null,
				null, null);

		String contactName = null;

		if (cursor.moveToFirst()) {
			contactName = cursor.getString(0);
		}
		cursor.close();
		cursor = null;
		return contactName;
	}

	public static Intent getContactInfoIntent(Context context, Long contactID) {
		if(contactID!=null){
			Intent i = new Intent(Intent.ACTION_VIEW);
			Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, 
					String.valueOf(contactID));
			i.setData(uri);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			return i;
		}
		return null;
	}

	public static Intent sendSMSIntent(Context context, String phoneNumber) {
		if(JavaUtils.isNotEmpty(phoneNumber)){
			String tel = "tel:"+phoneNumber;
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
					+ tel));
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			return i;
		}
		return null;
	}

	public static Intent callIntent(Context context, String phoneNumber) {
		if(JavaUtils.isNotEmpty(phoneNumber)){
			String tel = "tel:"+phoneNumber;
			Intent i = new Intent(Intent.ACTION_CALL, Uri.parse(tel));
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			return i;
		}
		return null;
	}

	public static Intent sendEmailIntent(Context context, String emailAddress) {
		if(JavaUtils.isNotEmpty(emailAddress)){
			Uri uri = Uri.parse("mailto:"+emailAddress);
			Intent i = new Intent(Intent.ACTION_SENDTO, uri);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			return i;
		}
		return null;
	}


	public static ArrayList<Contact> getAllContactsWithSingleEmail(Context context) {
		ArrayList<Contact> emlContacts = new ArrayList<Contact>();

		try {
			String nameAndEmailOrder = "lower(" + ContactsContract.Data.DISPLAY_NAME + ") ASC";
			String[] nameAndEmailProjection = new String[]{
					ContactsContract.Data.DISPLAY_NAME,
					ContactsContract.CommonDataKinds.Email.DATA};

			Cursor cur = context.getContentResolver().query(
					ContactsContract.CommonDataKinds.Email.CONTENT_URI,
					nameAndEmailProjection, null, null, nameAndEmailOrder);

			eLog.w("CONTACTS", "cur: " + cur.getCount());

			cur.moveToFirst();
			int nameColumn = cur.getColumnIndex(
					ContactsContract.Data.DISPLAY_NAME);
			int emailColumn = cur.getColumnIndex(
					ContactsContract.CommonDataKinds.Email.DATA);
			do {
				String name = cur.getString(nameColumn);
				String emlAddr = cur.getString(emailColumn);
				Contact contact = new Contact(context, name, null, new ArrayList<String>(Arrays.asList(new String[]{emlAddr})));
				emlContacts.add(contact);
				eLog.d("test", String.format("%s (%s)", cur.getString(nameColumn), cur.getString(emailColumn)));
			} while (cur.moveToNext());

			cur.close();
		}catch (Exception e){}

		if(emlContacts==null || emlContacts.size()<0){
			try{


				ArrayList<Contact> contacts = getContacts(context);

				if(contacts!=null && contacts.size()>0){
					for(Contact contact: contacts){
						ArrayList<String> emailAddresses = contact.getEmails();
						if(emailAddresses!=null && emailAddresses.size()>0){
							emlContacts.add(contact);
						}
					}
				}


			}catch (Exception e){

			}
		}



		return emlContacts;
	}




	public static ArrayList<Contact> getContacts(Context c){
		ArrayList<Contact> contacts = new ArrayList<Contact>();
		ContentResolver cr = c.getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);

		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				Contact contact = new Contact();

				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

					contact.setID(JavaUtils.getLongValue(id, 0));
					contact.setName(name);


					// get the phone number
					ArrayList <String> phoneNumbers = new ArrayList<>();
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
							new String[]{id}, null);
					while (pCur.moveToNext()) {
						String phone = pCur.getString(
								pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						phoneNumbers.add(phone);
					}
					pCur.close();
					contact.setPhoneNumbers(phoneNumbers);



					// get email and type
					ArrayList <String> emailAddresses = new ArrayList<>();
					Cursor emailCur = cr.query(
							ContactsContract.CommonDataKinds.Email.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
							new String[]{id}, null);
					while (emailCur.moveToNext()) {
						// This would allow you get several email addresses
						// if the email addresses were stored in an array
						String email = emailCur.getString(
								emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
						String emailType = emailCur.getString(
								emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));

						emailAddresses.add(email);
					}
					emailCur.close();
					contact.setEmails(emailAddresses);

					// Get note.......
//					String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
//					String[] noteWhereParams = new String[]{id,
//							ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE};
//					Cursor noteCur = cr.query(ContactsContract.Data.CONTENT_URI, null, noteWhere, noteWhereParams, null);
//					if (noteCur.moveToFirst()) {
//						String note = noteCur.getString(noteCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
//						System.out.println("Note " + note);
//					}
//					noteCur.close();

					//Get Postal Address....

//					String addrWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
//					String[] addrWhereParams = new String[]{id,
//							ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE};
//					Cursor addrCur = cr.query(ContactsContract.Data.CONTENT_URI,
//							null, null, null, null);
//					while(addrCur.moveToNext()) {
//						String poBox = addrCur.getString(
//								addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POBOX));
//						String street = addrCur.getString(
//								addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
//						String city = addrCur.getString(
//								addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
//						String state = addrCur.getString(
//								addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
//						String postalCode = addrCur.getString(
//								addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
//						String country = addrCur.getString(
//								addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
//						String type = addrCur.getString(
//								addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));
//
//						// Do something with these....
//
//					}
//					addrCur.close();

					// Get Instant Messenger.........
//					String imWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
//					String[] imWhereParams = new String[]{id,
//							ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE};
//					Cursor imCur = cr.query(ContactsContract.Data.CONTENT_URI,
//							null, imWhere, imWhereParams, null);
//					if (imCur.moveToFirst()) {
//						String imName = imCur.getString(
//								imCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA));
//						String imType;
//						imType = imCur.getString(
//								imCur.getColumnIndex(ContactsContract.CommonDataKinds.Im.TYPE));
//					}
//					imCur.close();

					// Get Organizations.........

//					String orgWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
//					String[] orgWhereParams = new String[]{id,
//							ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE};
//					Cursor orgCur = cr.query(ContactsContract.Data.CONTENT_URI,
//							null, orgWhere, orgWhereParams, null);
//					if (orgCur.moveToFirst()) {
//						String orgName = orgCur.getString(orgCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DATA));
//						String title = orgCur.getString(orgCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
//					}
//					orgCur.close();


					contacts.add(contact);
				}
			}
		return contacts;
	}

}
