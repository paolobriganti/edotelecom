package com.paolobriganti.android;

import android.animation.AnimatorInflater;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils.TruncateAt;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class VU {



	//*********************************************************************
	//*		ANIMATIONS
	//*********************************************************************

	public static void setVisibilityWithAnimationToView(Context c, final View v, final int visibility, int animationResource, Long duration){
		Animation animation = AnimationUtils.loadAnimation(c, animationResource);
		animation.reset();
		v.clearAnimation();

		AnimationListener a_to_b = new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				if(visibility==View.VISIBLE)
					v.setVisibility(visibility);      
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				if(visibility==View.GONE || visibility==View.INVISIBLE)
					v.setVisibility(visibility);  
			}
		};
		animation.setAnimationListener(a_to_b);
		if(duration!=null)
			animation.setDuration(duration);
		v.startAnimation(animation);
	}
	
	
	public static ViewPropertyAnimator zoomInAnimation(View v, Long duration){
		v.setAlpha(0);
		v.setScaleX(0);
		v.setScaleY(0);
		v.setVisibility(View.VISIBLE);
		return v.animate().setDuration(duration).
		scaleX(1).scaleY(1).
		translationX(0).translationY(0).alpha(1).
		setInterpolator(new DecelerateInterpolator());
	}
	
	public static ViewPropertyAnimator zoomOutAnimation(View v, Long duration){
		v.setAlpha(1);
		v.setScaleX(1);
		v.setScaleY(1);
		v.setVisibility(View.VISIBLE);
		return v.animate().setDuration(duration).
		scaleX(0).scaleY(0).
		translationX(0).translationY(0).alpha(0).
		setInterpolator(new AccelerateInterpolator());
	}
	
	
	public static ViewPropertyAnimator inFromBottom(View v, Long duration){
		v.setTranslationY(v.getHeight());
		v.setAlpha(0);
		v.setVisibility(View.VISIBLE);
		v.setTranslationY(v.getHeight());
		return v.animate().setDuration(duration).
		translationY(0).alpha(1).
		setInterpolator(new DecelerateInterpolator());
	}
	
	public static ViewPropertyAnimator outToBottom(View v, Long duration){
		v.setVisibility(View.VISIBLE);
		return v.animate().
		translationY(v.getHeight()).
		alpha(0).
		setDuration(duration).
		setInterpolator(new AccelerateInterpolator());
	}
	
	public static ViewPropertyAnimator inFromTop(View v, Long duration){
		v.setAlpha(0);
		v.setVisibility(View.VISIBLE);
		v.setTranslationY(-v.getHeight());
		return v.animate().setDuration(duration).
		translationY(0).alpha(1).
		setInterpolator(new DecelerateInterpolator());
	}
	
	public static ViewPropertyAnimator outToTop(View v, Long duration){
		v.setVisibility(View.VISIBLE);
		return v.animate().
		translationY(-v.getHeight()).
		alpha(0).
		setDuration(duration).
		setInterpolator(new AccelerateInterpolator());
	}
	
	
	public static ViewPropertyAnimator appear(View v, Long duration){
		v.setAlpha(0);
		v.setVisibility(View.VISIBLE);
		return v.animate().setDuration(duration).alpha(1).
		setInterpolator(new DecelerateInterpolator());
	}
	
	public static ViewPropertyAnimator disappear(View v, Long duration){
		v.setVisibility(View.VISIBLE);
		return v.animate().setDuration(duration).alpha(0).
		setInterpolator(new AccelerateInterpolator());
	}
	

//	public static void showView(View myView){
//		// get the center for the clipping circle
//		int cx = (myView.getLeft() + myView.getRight()) / 2;
//		int cy = (myView.getTop() + myView.getBottom()) / 2;
//
//		// get the final radius for the clipping circle
//		int finalRadius = myView.getWidth();
//
//		// create and start the animator for this view
//		// (the start radius is zero)
//		ValueAnimator anim =
//		    ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
//		anim.start();
//	}
//
//	public static void hideView(final View myView){
//		// get the center for the clipping circle
//		int cx = (myView.getLeft() + myView.getRight()) / 2;
//		int cy = (myView.getTop() + myView.getBottom()) / 2;
//
//		// get the initial radius for the clipping circle
//		int initialRadius = myView.getWidth();
//
//		// create the animation (the final radius is zero)
//		ValueAnimator anim =
//		    ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);
//
//		// make the view invisible when the animation is done
//		anim.addListener(new AnimatorListenerAdapter() {
//		    @Override
//		    public void onAnimationEnd(Animator animation) {
//		        super.onAnimationEnd(animation);
//		        myView.setVisibility(View.INVISIBLE);
//		    }
//		});
//
//		// start the animation
//		anim.start();
//	}






	//*********************************************************************
	//*		TEXT VIEW
	//*********************************************************************

	public static void setTextViewScrollable(TextView tv){
		tv.setSelected(true); 
		tv.setEllipsize(TruncateAt.MARQUEE); 
		tv.setSingleLine(true);
	}

	public static void setTypefaceToTextView(Context c, TextView textView, String typeface){
		Typeface tf = Typeface.createFromAsset(c.getAssets(), typeface);
		textView.setTypeface(tf);
	}

	/**
	 * Using reflection to override default typeface
	 * NOTICE: DO NOT FORGET TO SET TYPEFACE FOR APP THEME AS DEFAULT TYPEFACE WHICH WILL BE OVERRIDDEN
	 * @param context to work with assets
	 * @param defaultFontNameToOverride for example "monospace"
	 * @param customFontFileNameInAssets file name of the font from assets
	 */
	public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
		try {
			final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

			final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
			defaultFontTypefaceField.setAccessible(true);
			defaultFontTypefaceField.set(null, customFontTypeface);
		} catch (Exception e) {
		}
	}


	public static String getColoredTextString(Context context, int colorResId, String text){
		String colorStr=context.getResources().getString(colorResId);
		if(colorStr.length()>7){
			char[] c = colorStr.toCharArray();
			colorStr = ""+c[0]+c[3]+c[4]+c[5]+c[7]+c[7]+c[8];
		}

		text = text.replace("/(?:\r\n|\r|\n)/g", "<br />");
		return ("<font color=\""+colorStr+"\">" + text + "</font>");
	}

	public static String getColoredTextString(Context context, int colorResId, int textResId){
		String text=context.getResources().getString(textResId);
		return getColoredTextString(context, colorResId, text);
	}
	
	
	public static String getBoldTextString(Context context, int textResId){
		String text=context.getResources().getString(textResId);
		return getBoldTextString(text);
	}
	
	public static String getBoldTextString(String text){
		text = text.replace("/(?:\r\n|\r|\n)/g", "<br />");
		return ("<b>" + text + "</b>");
	}
	
	public static String getItalicTextString(String text){
		text = text.replace("/(?:\r\n|\r|\n)/g", "<br />");
		return ("<i>" + text + "</i>");
	}
	

	public static String getColoredText(Context context, int colorResId, String text){
		return (getColoredTextString(context, colorResId, text));
	}

	public static String getColoredText(Context context, int colorResId, int textResId){
		return (getColoredTextString(context, colorResId, textResId));
	}

	public static SpannableString highlightText(String text, String textToHighlight, int highlightColor) {
		SpannableString spannable = SpannableString.valueOf(text);
		final String spannableString = spannable.toString();
		final int start = spannableString.toLowerCase().indexOf(textToHighlight.toLowerCase());
		final int end = start + textToHighlight.length();

		if(start>=0 && end>=0) {
			spannable.setSpan(new ForegroundColorSpan(highlightColor), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		return spannable;
	}



	public static ArrayList<View> getAllChildren(View v) {

		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup vg = (ViewGroup) v;
		for (int i = 0; i < vg.getChildCount(); i++) {

			View child = vg.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(getAllChildren(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	public static String replaceBlankSpaces(String text){
		if(text!=null)
			return text.replace(" ", "\u00A0");
		return null;
	}



	//*********************************************************************
	//*		LIST VIEW
	//*********************************************************************

	public static void expandListView(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	public static void expandGridView(GridView myGridView) {
		int verticalSpacing = 0;
		try {
			Field field = myGridView.getClass().getDeclaredField("mVerticalSpacing");
			field.setAccessible(true);
			verticalSpacing = field.getInt(myGridView);
		} catch (Exception e) {
			// TODO: handle exception
		}

		ListAdapter listAdapter = myGridView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(myGridView.getWidth(), MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, myGridView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = myGridView.getLayoutParams();
		params.height = totalHeight - (verticalSpacing * (listAdapter.getCount() - 1));
		myGridView.setLayoutParams(params);
		myGridView.requestLayout();
	}



	
	public static int[] getCoordinatesOfTopAndBottom(View view){
		int[] coords = {0,0};
		view.getLocationOnScreen(coords);
		int absoluteTop = coords[1];
		int absoluteBottom = coords[1] + view.getHeight();
		
		coords = new int[]{absoluteTop, absoluteBottom};
		
		return coords;
	}



	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public static void setStateListAnimator(Context context, View view, int id){
		view.setStateListAnimator(AnimatorInflater.loadStateListAnimator(context, id));
	}

	//*********************************************************************
	//*		SEARCH VIEW
	//*********************************************************************

	public static EditText getSearchViewEditText(View searchView){
		int editTextId = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
		return (EditText) searchView.findViewById(editTextId);
	}

	public static ImageView getSearchViewCloseButton(View searchView){
		int id = searchView.getContext().getResources().getIdentifier("android:id/search_close_btn", null, null);
		return (ImageView) searchView.findViewById(id);
	}

	public static ImageView getSearchViewSearchHintIcon(View searchView){
		int searchHintIconId = searchView.getContext().getResources().getIdentifier("android:id/search_mag_icon", null, null);
		return (ImageView) searchView.findViewById(searchHintIconId);
	}



}
