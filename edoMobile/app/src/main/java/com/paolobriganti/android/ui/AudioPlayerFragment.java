package com.paolobriganti.android.ui;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.paolobriganti.android.ui.views.VisualizerView;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.NumberUtils;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;


public abstract class AudioPlayerFragment extends Fragment implements OnCompletionListener, SeekBar.OnSeekBarChangeListener {

	private static final long POST_DELAYED_TIME = 100;
	
	Activity c;
	// Media Player
	private  MediaPlayer mp;
	// Handler to update UI timer, progress bar etc,.
	private Handler mHandler;
	private AudioUtilities utils;
	private int seekForwardTime = 5000; // 5000 milliseconds
	private int seekBackwardTime = 5000; // 5000 milliseconds
	private int currentSongIndex = 0;
	private boolean isShuffle = false;
	private boolean isRepeat = false;

	boolean isFirstPlaying = true;

	private Visualizer mVisualizer;

	public abstract ImageButton getBtnPlay();
	public abstract ImageButton getBtnForward();
	public abstract ImageButton getBtnBackward();
	public abstract TextView getTvTitle();
	public abstract TextView getTvArtist();
	public abstract TextView getTvYear();
	public abstract TextView getTvAlbum();
	public abstract TextView getTvCurrentLabel();
	public abstract TextView getTotalDurationLabel();
	public abstract SeekBar getSongProgressBar();
	public abstract ImageView getImageArt();
	public abstract VisualizerView getVisualizer();

	public abstract int getPlayButtonDrawableResource();
	public abstract int getPauseButtonDrawableResource();
//	public abstract int getStopButtonDrawableResource();

	public abstract String getFilePath();

	public String getDefaultTitle() {
		return null;
	}
	public String getArtist() {
		return null;
	}
	public String getFileYear(String filePath){
		File file = new File(filePath);
		if(file!=null && file.exists()){
			long mdate = file.lastModified();
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(mdate);
			return ""+cal.get(Calendar.YEAR);
		}
		return null;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		c = getActivity();
		mHandler = new Handler();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		new Thread(new Runnable() {
			@Override
			public void run() {
				setPlayer();
			}
		}).start();
	}

	public void setPlayer(){

		// Mediaplayer
		mp = new MediaPlayer();
		utils = new AudioUtilities();
		isFirstPlaying = true;

		mp.reset();

		try {
			mp.setDataSource(getFilePath());

			c.setVolumeControlStream(AudioManager.STREAM_MUSIC);

			setupVisualizerFxAndUI();
		}catch (Exception e){}

		// Listeners
		getSongProgressBar().setOnSeekBarChangeListener(this); // Important
		mp.setOnCompletionListener(this); // Important

		// Getting all songs list

		String titleString = null;
		String artistString = null;
		String yearString = null;
		String albumString = null;
		long durationLong = 0;

		String url = getFilePath();
		if(url==null)
			return;
		File file = new File(url);
		Uri uri = null;
		if(file.exists()){
			uri = (Uri) Uri.fromFile(file);
		}else{
			uri = (Uri) Uri.parse(url);
		}

		MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
		try{
			mediaMetadataRetriever.setDataSource(c, uri);
		}catch(RuntimeException e){
			
		}
		

		if(mediaMetadataRetriever!=null){
			titleString = (String) mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
			if(JavaUtils.isEmpty(titleString) && JavaUtils.isNotEmpty(getDefaultTitle())){
				titleString = getDefaultTitle();
			}else{
				titleString = file.getName();
			}
			artistString = (String) mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
			yearString = (String) mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR);
			if(JavaUtils.getIntegerValue(yearString, 0)==0){
				yearString = null;
			}
			albumString = (String) mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
			durationLong = JavaUtils.getLongValue((String) mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION), 0);
		}

		final String title = titleString;
		final String artist = artistString==null?getArtist():artistString;
		final String year = yearString==null?getFileYear(getFilePath()):yearString;
		final String album = albumString;
		final long duration = durationLong;

		c.runOnUiThread(new Runnable() {public void run() {

			getTvTitle().setText(title);
			getTvArtist().setText(artist);
			getTvYear().setText(year);
			getTvAlbum().setText(album);
			getTvCurrentLabel().setText("" + utils.milliSecondsToTimer(0));
			getTotalDurationLabel().setText("" + utils.milliSecondsToTimer(duration));
			seekBackwardTime = (int) duration/10;
			seekForwardTime = (int) duration/10;


			/**
			 * Play button click event
			 * plays a song and changes button to pause image
			 * pauses a song and changes button to play image
			 * */
			getBtnPlay().setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if (isFirstPlaying) {
						playSong();
						return;
					}

					// check for already playing
					if (mp.isPlaying()) {
						pausePlay();
					} else {
						resumePlay();
					}

				}
			});

			/**
			 * Forward button click event
			 * Forwards song specified seconds
			 * */
			getBtnForward().setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if(mp.isPlaying()) {
						// get current song position
						int currentPosition = mp.getCurrentPosition();
						// check if seekForward time is lesser than song duration
						if (currentPosition + seekForwardTime <= mp.getDuration()) {
							// forward song
							mp.seekTo(currentPosition + seekForwardTime);
						} else {
							// forward to end position
							mp.seekTo(mp.getDuration());
						}
					}
				}
			});

			/**
			 * Backward button click event
			 * Backward song to specified seconds
			 * */
			getBtnBackward().setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if(mp.isPlaying()) {
						// get current song position
						int currentPosition = mp.getCurrentPosition();
						// check if seekBackward time is greater than 0 sec
						if (currentPosition - seekBackwardTime >= 0) {
							// forward song
							mp.seekTo(currentPosition - seekBackwardTime);
						} else {
							// backward to starting position
							mp.seekTo(0);
						}
					}
				}
			});

		}});
	}

	//    /**
	//     * Receiving song index from playlist view
	//     * and play the song
	//     * */
	//    @Override
	//    protected void onActivityResult(int requestCode,
	//                                     int resultCode, Intent data) {
	//        super.onActivityResult(requestCode, resultCode, data);
	//        if(resultCode == 100){
	//             currentSongIndex = data.getExtras().getInt("songIndex");
	//             // play selected song
	//             playSong(currentSongIndex);
	//        }
	// 
	//    }

	/**
	 * Function to play a song
	 * */
	public void  playSong(){
		isFirstPlaying = false;
		
		// Play song
		try {
			if(mVisualizer!=null)
				mVisualizer.setEnabled(true);

			mp.prepare();
			mp.start();

			// Changing Button Image to pause image
			getBtnPlay().setImageResource(getPauseButtonDrawableResource());

			// set Progress bar values
			getSongProgressBar().setProgress(0);
			getSongProgressBar().setMax(100);

			// Updating progress bar
			updateProgressBar();




		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void pausePlay(){
		if (mp != null) {
			AudioPlayerFragment.this.mHandler.removeCallbacks(mUpdateTimeTask);
			mUpdateTimeTask.killRunnable();
			mp.pause();
			if(mVisualizer!=null)
				mVisualizer.setEnabled(false);
			// Changing button image to play button
			getBtnPlay().setImageResource(getPlayButtonDrawableResource());
		}
	}

	public void resumePlay(){
		if (mp != null) {
			updateProgressBar();
			mp.start();
			if(mVisualizer!=null)
				mVisualizer.setEnabled(true);
			// Changing button image to pause button
			getBtnPlay().setImageResource(getPauseButtonDrawableResource());
		}
	}


	/**
	 * Update timer on seekbar
	 * */
	public void updateProgressBar() {
		mHandler.postDelayed(mUpdateTimeTask, POST_DELAYED_TIME);
	}  

	/**
	 * Background Runnable thread
	 * */
	private MyRunnable mUpdateTimeTask = new MyRunnable() {
		@Override
		public void run() {
			super.run();
			
			long totalDuration = mp.getDuration();
			long currentDuration = mp.getCurrentPosition();

			// Displaying Total Duration time
			getTotalDurationLabel().setText("" + utils.milliSecondsToTimer(totalDuration));
			// Displaying time completed playing
			getTvCurrentLabel().setText("" + utils.milliSecondsToTimer(currentDuration));

			// Updating progress bar
			int progress = (int)(utils.getProgressPercentage(currentDuration, totalDuration));
			getSongProgressBar().setProgress(progress);

			// Running this thread after 100 milliseconds
			
			if(!mp.isPlaying() || currentDuration>=totalDuration){
				getSongProgressBar().setProgress(100);
				onCompletion(mp);
			}else{
				mHandler.postDelayed(this, POST_DELAYED_TIME);
			}
		}
	};
	public class MyRunnable implements Runnable
	{
	   private boolean killMe = false;

	   public void run()
	   {
	      if(killMe)
	         return;

	      /* do your work */
	   }

	   public void killRunnable()
	   {
	      killMe = true;
	   }
	}

	/**
	 *
	 * */
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

	}

	/**
	 * When user starts moving the progress handler
	 * */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// remove message Handler from updating progress bar
		mHandler.removeCallbacks(mUpdateTimeTask);
		mUpdateTimeTask.killRunnable();
	}

	/**
	 * When user stops moving the progress hanlder
	 * */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		mHandler.removeCallbacks(mUpdateTimeTask);
		mUpdateTimeTask.killRunnable();
		
		int totalDuration = mp.getDuration();
		int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

		// forward or backward to certain seconds
		mp.seekTo(currentPosition);

		// update timer progress again
		updateProgressBar();
	}

	/**
	 * On Song Playing completed
	 * if repeat is ON play same song again
	 * if shuffle is ON play random song
	 * */
	@Override
	public void onCompletion(MediaPlayer arg0) {
		//        // check for repeat is ON or OFF
		//        if(isRepeat){
		//            // repeat is on play same song again
		//            playSong(currentSongIndex);
		//        } else if(isShuffle){
		//            // shuffle is on - play a random song
		//            Random rand = new Random();
		//            currentSongIndex = rand.nextInt((songsList.size() - 1) - 0 + 1) + 0;
		//            playSong(currentSongIndex);
		//        } else{
		//            // no repeat or shuffle ON - play next song
		//            if(currentSongIndex < (songsList.size() - 1)){
		//                playSong(currentSongIndex + 1);
		//                currentSongIndex = currentSongIndex + 1;
		//            }else{
		//                // play first song
		//                playSong(0);
		//                currentSongIndex = 0;
		//            }
		//        }
		mHandler.removeCallbacks(mUpdateTimeTask);
		mUpdateTimeTask.killRunnable();
		getBtnPlay().setImageResource(getPlayButtonDrawableResource());
		mVisualizer.setEnabled(false);
	}



	private void setupVisualizerFxAndUI() {

		// Create the Visualizer object and attach it to our media player.
		mVisualizer = new Visualizer(mp.getAudioSessionId());
		mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		mVisualizer.setDataCaptureListener(
				new Visualizer.OnDataCaptureListener() {
					public void onWaveFormDataCapture(Visualizer visualizer,
													  byte[] bytes, int samplingRate) {
						getVisualizer().updateVisualizer(bytes);
					}

					public void onFftDataCapture(Visualizer visualizer,
												 byte[] bytes, int samplingRate) {
					}
				}, Visualizer.getMaxCaptureRate() / 2, true, false);
	}



	@Override
	public void onResume() {
		super.onResume();
		if (mVisualizer != null && mp!=null && mp.isPlaying()) {
			mVisualizer.setEnabled(true);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mVisualizer != null) {
			mVisualizer.setEnabled(false);
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
			if (isVisibleToUser) {
				if (mVisualizer != null && mp!=null && mp.isPlaying()) {
					mVisualizer.setEnabled(true);
				}
			} else {
				if (mVisualizer != null) {
					mVisualizer.setEnabled(false);
				}
			}
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
		if(mVisualizer!=null)
			mVisualizer.release();

		if(mp!=null)
			mp.release();
		AudioPlayerFragment.this.mHandler.removeCallbacks(mUpdateTimeTask);
		mUpdateTimeTask.killRunnable();
	}


	class AudioUtilities {

		/**
		 * Function to convert milliseconds time to
		 * Timer Format
		 * Hours:Minutes:Seconds
		 * */
		public String milliSecondsToTimer(long milliseconds){
			String finalTimerString = "";
			String secondsString = "";

			// Convert total duration into time
			int hours = (int)( milliseconds / (1000*60*60));
			int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
			int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
			// Add hours if there
			if(hours > 0){
				finalTimerString = hours + ":";
			}

			// Prepending 0 to seconds if it is one digit
			if(seconds < 10){
				secondsString = "0" + seconds;
			}else{
				secondsString = "" + seconds;}

			finalTimerString = finalTimerString + minutes + ":" + secondsString;

			// return timer string
			return finalTimerString;
		}

		/**
		 * Function to get Progress percentage
		 * @param currentDuration
		 * @param totalDuration
		 * */
		public int getProgressPercentage(long currentDuration, long totalDuration){
			int current = JavaUtils.getIntegerValue(currentDuration, 0);
			int total = JavaUtils.getIntegerValue(totalDuration, 0);
			return NumberUtils.getPercent(current, total);
		}

		/**
		 * Function to change progress to timer
		 * @param progress -
		 * @param totalDuration
		 * returns current duration in milliseconds
		 * */
		public int progressToTimer(int progress, int totalDuration) {
			int currentDuration = 0;
			totalDuration = (int) (totalDuration / 1000);
			currentDuration = (int) ((((double)progress) / 100) * totalDuration);

			// return current duration in milliseconds
			return currentDuration * 1000;
		}
	}
}
