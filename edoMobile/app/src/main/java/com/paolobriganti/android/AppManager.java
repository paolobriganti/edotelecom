package com.paolobriganti.android;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.edo.edomobile.R;



public class AppManager {


	public static Drawable getAppIconDrawable(Context c, String packageName, String activityName){
		final PackageManager pm = c.getPackageManager();
		Drawable d = null;

		if(activityName!=null){
			ComponentName cn = new ComponentName(packageName, activityName);
			try {
				d = pm.getActivityIcon(cn);
			} catch (NameNotFoundException e) {

			}
		}

		if(d==null)
			try {
				d = pm.getApplicationIcon(packageName);
			} catch (NameNotFoundException e) {
				d = c.getResources().getDrawable(android.R.drawable.sym_def_app_icon);
			}

		return d;
	}


	public static Bitmap getAppIconBitmap(Context c, String packageName, String activityName){
		Drawable icon = getAppIconDrawable(c, packageName, activityName);
		return ((BitmapDrawable)icon).getBitmap();
	}



	public static Intent getAppPlayStoreIntent(String packageName){
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)); 
		goToMarket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return goToMarket;
	}

	public static boolean isAppInstalled(Context c, String packageName){
		PackageManager pm = c.getPackageManager();
		boolean installed = false;
		try {
			pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
			installed = true;
		} catch (NameNotFoundException e) {
			installed = false;
		}
		return installed;

	}

	public static String findFirstActivity(Context c, String packageName){
		String firstActivity = null;
		List<ResolveInfo> appList;
		final PackageManager pm = c.getPackageManager();
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER); 
		appList = pm.queryIntentActivities(mainIntent, 0);
		Collections.sort(appList, new ResolveInfo.DisplayNameComparator(pm));

		for(int i=0; i<appList.size(); i++){
			if(appList.get(i).activityInfo.applicationInfo.packageName.toString().equals(packageName)){
				firstActivity=appList.get(i).activityInfo.name;
				break;
			}
		}

		return firstActivity;
	}

	public static ArrayList <App> getAppsArray(Context c){
		PackageManager pm = c.getPackageManager();
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null); 
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER); 
		List<ResolveInfo> appList = pm.queryIntentActivities(mainIntent, PackageManager.PERMISSION_GRANTED); 
		Collections.sort(appList, new ResolveInfo.DisplayNameComparator(pm));

		ArrayList <App> apps = new ArrayList<App>();

		for(int i=0; i<appList.size(); i++){
			if (!appList.get(i).activityInfo.applicationInfo.processName.equals("system")){
				String name = pm.getApplicationLabel(appList.get(i).activityInfo.applicationInfo).toString();
				String packageName=appList.get(i).activityInfo.applicationInfo.packageName;
				String firstActivity=appList.get(i).activityInfo.name;
				Drawable d = pm.getApplicationIcon(appList.get(i).activityInfo.applicationInfo);
				try {
					name = pm.getResourcesForApplication(appList.get(i).activityInfo.applicationInfo).getString(appList.get(i).activityInfo.labelRes);
					d = pm.getResourcesForApplication(appList.get(i).activityInfo.applicationInfo).getDrawable(appList.get(i).activityInfo.icon);
				} catch (NotFoundException e) {
				} catch (NameNotFoundException e) {
				}
				App app = new App(name, packageName, firstActivity);
				app.setIcon(d);
				apps.add(app);
			}
		}



		return apps;
	}





	public static class AppsAdapter extends ArrayAdapter<App> {
		private final Context context;
		private final ArrayList <App> apps;

		public AppsAdapter(Context context, ArrayList <App> apps) {
			super(context, R.layout.row_image_text_subtext, apps);
			this.context = context;
			this.apps = apps;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.row_image_text_subtext, parent, false);
			TextView tv_title = (TextView) rowView.findViewById(R.id.tv_title);
			tv_title.setText(apps.get(position).getName());
			TextView tv_subTitle = (TextView) rowView.findViewById(R.id.tv_sub_title);
			tv_subTitle.setText(apps.get(position).getPackageName());
			ImageView iv_icon = (ImageView) rowView.findViewById(R.id.iv_icon);
			if(apps.get(position).getIcon(context)!=null){
				iv_icon.setVisibility(ImageView.VISIBLE);
				iv_icon.setImageDrawable(apps.get(position).getIcon(context));
			}else{
				iv_icon.setVisibility(ImageView.GONE);
			}
			return rowView;
		}
	}
}
