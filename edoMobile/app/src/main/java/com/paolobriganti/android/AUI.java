package com.paolobriganti.android;


import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.paolobriganti.utils.JavaUtils;

import java.io.File;
import java.util.Calendar;


public class AUI {
	Toast shortToast, longToast;

	public static final long VIBRATION_LENGTH_SHORT = 125;
	public static final long VIBRATION_LENGTH_MID = 250;
	public static final long VIBRATION_LENGTH_LONG = 500;


	public static final int ASYNC_TASK_CORE_POOL_SIZE = 60;
	public static final int ASYNC_TASK_MAXIMUM_POOL_SIZE = 80;
	public static final int ASYNC_TASK_KEEP_ALIVE_TIME = 10;

	public AUI (Context c){

		shortToast = Toast.makeText(c, "", Toast.LENGTH_SHORT);
		longToast = Toast.makeText(c, "", Toast.LENGTH_LONG);
	}



	public static String getAppVersion(Context c){
		try {
			PackageInfo pInfo = c.getPackageManager().getPackageInfo(c.getPackageName(), 0);
			return pInfo.versionName;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}



	/**
	 * This method converts dp unit to equivalent pixels, depending on device density. 
	 * 
	 * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent px equivalent to dp depending on device density
	 */
	public static float convertDpToPixel(Context context, float dp){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	/**
	 * This method converts device specific pixels to density independent pixels.
	 * 
	 * @param px A value in px (pixels) unit. Which we need to convert into db
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent dp equivalent to px value
	 */
	public static float convertPixelsToDp(Context context, float px){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

	
	
	
	
	
	
	//*********************************************************************
	//*		ACTIVITIES
	//*********************************************************************

	
	public static void setStatusBarColor(Activity activity, int color){
		if(ASI.get_OS_SDK_Version()>=ASI.ANDROID_5_0_SDK_VERSION){
			setStatusBarColorLollipop(activity, color);
		}
	}
	
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private static void setStatusBarColorLollipop(Activity activity, int color){
		Window window = activity.getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		window.setStatusBarColor(color);
	}
	
	
	public static void hideStatusBar(Activity activity){
		if(ASI.get_OS_SDK_Version()>=ASI.ANDROID_4_1_SDK_VERSION){
			hideStatusBarJellyBean(activity);
		}else{
			activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private static void hideStatusBarJellyBean(Activity activity){
		View decorView = activity.getWindow().getDecorView();
		// Hide the status bar.
		int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
		decorView.setSystemUiVisibility(uiOptions);
		// Remember that you should never show the action bar if the
		// status bar is hidden, so hide that too if necessary.
		ActionBar actionBar = activity.getActionBar();
		if(actionBar!=null)
			actionBar.hide();
	}
	
	
	
	
	public static void hideNavigationBar(Activity activity){
		if(ASI.get_OS_SDK_Version()>=ASI.ANDROID_4_0_SDK_VERSION){
			hideNavigationBarICS(activity);
		}
	}
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private static void hideNavigationBarICS(Activity activity){
		View decorView = activity.getWindow().getDecorView();
		// Hide both the navigation bar and the status bar.
		// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
		// a general rule, you should design your app to hide the status bar whenever you
		// hide the navigation bar.
		int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
		              | View.SYSTEM_UI_FLAG_FULLSCREEN;
		decorView.setSystemUiVisibility(uiOptions);
	}
	
	
	
	
	
	
	
	
	
	


	//*********************************************************************
	//*		TOAST AND DIALOGS
	//*********************************************************************

	public void toastShort(String message){
		shortToast.setText(message); 
		shortToast.show();
	}

	public void toastLong(String message){
		longToast.setText(message); 
		longToast.show();
	}

	public static void oneButtonDialog(Context c,String title, String message){
		AlertDialog.Builder builder = new AlertDialog.Builder(c);
		builder.setMessage(message)
		.setCancelable(false)
		.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		if(JavaUtils.isNotEmpty(title)){
			builder.setTitle(title);
		}
		AlertDialog alert = builder.create();
		alert.show();
	}

	public static int getActioBarHeight(Context c){
		TypedValue tv = new TypedValue();
		if (c.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)){
			return TypedValue.complexToDimensionPixelSize(tv.data, c.getResources().getDisplayMetrics());
		}
		return 144;
	}


	public static int getStatusBarHeight(Context c) { 
	      int result = 0;
	      int resourceId = c.getResources().getIdentifier("status_bar_height", "dimen", "android");
	      if (resourceId > 0) {
	          result = c.getResources().getDimensionPixelSize(resourceId);
	      } 
	      return result;
	} 

	//*********************************************************************
	//*		VIBRATION
	//*********************************************************************

	public static void vibrate (Context c, long duration){
		Vibrator v = (Vibrator) c.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(duration);
	}





	//*********************************************************************
	//*		KEYBOARD
	//*********************************************************************


	public static void keyboardHide(Context c){
		if(isKeyboardShowed(c)) {
			InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		}
	}

	public static void keyboardShow(Context c, EditText editText){
		editText.setFocusableInTouchMode(true);
		editText.requestFocus();

		final InputMethodManager inputMethodManager = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
	}

	public static boolean isKeyboardShowed(Context c){
		InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
		return imm.isAcceptingText();
	}











	//*********************************************************************
	//*		WIDGETS
	//*********************************************************************


	public static void updateWidget(Context c, Class <?> cls){
		ComponentName thisAppWidget = new ComponentName(c.getPackageName(), cls.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(c);
		int ids[] = appWidgetManager.getAppWidgetIds(thisAppWidget);


		Intent updateWidget = new Intent(c.getApplicationContext(),
				cls);

		updateWidget.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		updateWidget.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,
				ids);

		c.sendBroadcast(updateWidget);		
	}







	//*********************************************************************
	//*		NOTIFICATIONS
	//*********************************************************************




	public static void showNotification(Context c, Class <?> cls, int notifyId, 
			int statusIcon, CharSequence title, CharSequence text, 
			boolean noClear, Integer lightColor, boolean sound, boolean vibration){	


		NotificationManager nm =
				(NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent startAnActivity = new Intent(c, cls);
		PendingIntent contentIntent = PendingIntent.getActivity(c, 0,startAnActivity, 0);

		NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(c)
		.setContentTitle(title)
		.setContentText(text)
		.setSmallIcon(statusIcon)
		.setContentIntent(contentIntent)
		;

		if(lightColor!=null){
			mNotifyBuilder.setLights(lightColor, 500, 1000);
		}

		if(vibration){
			long[] pattern = {150,150,150,150};
			mNotifyBuilder.setVibrate(pattern);
		}

		if(sound){
			Uri ringtone=RingtoneManager.getActualDefaultRingtoneUri(c, RingtoneManager.TYPE_NOTIFICATION);
			mNotifyBuilder.setSound(ringtone);
		}

		Notification notification = mNotifyBuilder.build();

		if(noClear){
			notification.flags = notification.flags | Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
		}

		nm.notify(notifyId, notification);

	}

	public static void cancelNotification(Context c, int notifyId){	
		NotificationManager nm =
				(NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(notifyId);
	}









































	//*********************************************************************
	//*		THIRD PARTY APPS
	//*********************************************************************

	public static boolean openBrowser(Context c, String url){
		try{
			Intent browser; 
			browser = new Intent(Intent.ACTION_VIEW, Uri.parse(url)); 
			c.startActivity(browser); 
			return true;
		}catch(IllegalStateException e){}
		catch(NullPointerException e){}
		catch(android.content.ActivityNotFoundException e){}
		return false;
	}


	public static void shareWith(Context c, String message){
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, message);
		String shareWith ="shareWith";
		c.startActivity(Intent.createChooser(intent, shareWith));
	}

	public static boolean shareWith(Context c, String filePath, String fileMimeType){
		try{
			String path = c.getFilesDir() + filePath;
			File file = new File(Uri.parse(path).toString());
			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			shareIntent.setType(fileMimeType);

			// For a file in shared storage.  For data in private storage, use a ContentProvider.
			Uri uri = Uri.fromFile(file);
			shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
			c.startActivity(shareIntent);
			return true;
		}catch(android.content.ActivityNotFoundException e){
			return false;
		}
	}

	public static boolean sendEmail(Context c, String dialogTitle, String[] emails, String subject, String text, String filePath, String fileMimeType){
		try{
			Intent emailIntent = new Intent(Intent.ACTION_SEND);
			if(JavaUtils.isEmpty(fileMimeType))
				fileMimeType = "*/*";
			emailIntent.setType(fileMimeType);
			if(emails!=null)
				emailIntent.putExtra(Intent.EXTRA_EMAIL, emails);
			if(subject!=null)
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			if(text!=null)
				emailIntent.putExtra(Intent.EXTRA_TEXT, text);
			if(filePath!=null)
				emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filePath)));
			c.startActivity(Intent.createChooser(emailIntent, dialogTitle));

			return true;
		}catch(android.content.ActivityNotFoundException e){
			return false;
		}
	}



	public static App clockApplication(Context c){
		// Verify clock implementation
		String clockImpls[][] = {
				{"HTC Alarm Clock", "com.htc.android.worldclock", "com.htc.android.worldclock.WorldClockTabControl" },
				{"Standar Alarm Clock", "com.android.deskclock", "com.android.deskclock.AlarmClock"},
				{"Froyo Nexus Alarm Clock", "com.google.android.deskclock", "com.android.deskclock.DeskClock"},
				{"Moto Blur Alarm Clock", "com.motorola.blur.alarmclock",  "com.motorola.blur.alarmclock.AlarmClock"},
				{"Samsung Galaxy Clock", "com.sec.android.app.clockpackage","com.sec.android.app.clockpackage.ClockPackage"},
				{"Alarm-Clock", "com.lge.clock", "com.lge.clock.Clock"}

		};


		for(int i=0; i<clockImpls.length; i++) {
			String name = clockImpls[i][0];
			String packageName = clockImpls[i][1];
			String firstActivity = clockImpls[i][2];

			firstActivity = AppManager.findFirstActivity(c, packageName);
			if(firstActivity!=null)
				return new App(name, packageName, firstActivity);
		}

		return null;
	}


	public static Intent getCurrentCalendarAppIntent(){
		Calendar cal = Calendar.getInstance();
		int d = cal.get(Calendar.DAY_OF_MONTH);
		int m = cal.get(Calendar.MONTH);
		int y = cal.get(Calendar.YEAR);

		long dayMillis = 1000*60*60*24;
		long startTime = 0,endTime = 0;

		Calendar beginCal = Calendar.getInstance();
		beginCal.set(y, m, d, 0, 0);
		startTime = beginCal.getTimeInMillis();
		endTime = startTime+dayMillis;

		Intent intent = new Intent(Intent.ACTION_INSERT);
		intent.setType("vnd.android.cursor.item/event");
		intent.putExtra("beginTime", startTime);
		intent.putExtra("allDay", true);
		intent.putExtra("endTime", endTime);


		return intent;
	}






	public static void navigateUpTo(Activity c, Intent intent){
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
			navigateUpToApi16(c, intent);
		}else{
			c.finish();
			c.startActivity(intent);
		}
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static void navigateUpToApi16(Activity c, Intent intent){
		c.navigateUpTo(intent);
	}


}
