package com.paolobriganti.android;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import java.io.File;
import java.util.List;


public class ASI {
	public static final int SCREEN_ORIENTATION_PORTRAIT = Configuration.ORIENTATION_PORTRAIT;
	public static final int SCREEN_ORIENTATION_LANDSCAPE = Configuration.ORIENTATION_LANDSCAPE;

	public static final int ANDROID_5_0_SDK_VERSION = Build.VERSION_CODES.LOLLIPOP;
	public static final int ANDROID_4_4_SDK_VERSION = Build.VERSION_CODES.KITKAT;
	public static final int ANDROID_4_3_SDK_VERSION = Build.VERSION_CODES.JELLY_BEAN_MR2;
	public static final int ANDROID_4_2_SDK_VERSION = Build.VERSION_CODES.JELLY_BEAN_MR1;
	public static final int ANDROID_4_1_SDK_VERSION = Build.VERSION_CODES.JELLY_BEAN;
	public static final int ANDROID_4_0_3_SDK_VERSION = Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1;
	public static final int ANDROID_4_0_SDK_VERSION = Build.VERSION_CODES.ICE_CREAM_SANDWICH;
	public static final int ANDROID_3_2_SDK_VERSION = Build.VERSION_CODES.HONEYCOMB_MR2;
	public static final int ANDROID_3_1_SDK_VERSION = Build.VERSION_CODES.HONEYCOMB_MR1;
	public static final int ANDROID_3_0_SDK_VERSION = Build.VERSION_CODES.GINGERBREAD_MR1;
	public static final int ANDROID_2_3_3_SDK_VERSION = Build.VERSION_CODES.GINGERBREAD;
	public static final int ANDROID_2_2_SDK_VERSION = Build.VERSION_CODES.FROYO;
	public static final int ANDROID_2_1_SDK_VERSION = Build.VERSION_CODES.ECLAIR;
	public static final int ANDROID_1_6_SDK_VERSION = Build.VERSION_CODES.DONUT;
	public static final int ANDROID_1_5_SDK_VERSION = Build.VERSION_CODES.CUPCAKE;


	public ASI (){
	}

	public static int get_OS_SDK_Version(){
		return android.os.Build.VERSION.SDK_INT;
	}

	public static String get_OS_RELEASE_Version(){
		return android.os.Build.VERSION.RELEASE;
	}

	public static String get_device_model(){
		return android.os.Build.MODEL;
	}

	public static String get_device_imei(Context c){
		TelephonyManager telephonyManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}

	public static String get_device_id(Context c){
		return Secure.getString(c.getContentResolver(),
				Secure.ANDROID_ID); 
	}

	public static Boolean isLocationManagerEnabledDialog(final Context c){
		Boolean enabled = false;
		/*LocationManager locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);

		if(locationManager==null || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
			enabled = false;
			AlertDialog.Builder builder = new AlertDialog.Builder(c);
			builder.setCancelable(true);
			builder.setTitle(R.string.noLocationServices);
			builder.setMessage(R.string.noLocationServicesMessage);
			builder.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					c.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					dialog.cancel();
				}
			});
			builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					if(dialog!=null)dialog.cancel();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		}else{
			enabled = true;
		}*/
		return enabled;
	}


	public static boolean isThisAppForground(Context context) {

		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
		if (appProcesses == null) {
			return false;
		}
		final String packageName = context.getPackageName();
		for (RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isThisAppBackground(final Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				return true;
			}
		}
		return false;
	}



	//Metodo per capire se l'Activity è visualizzata
	public static Boolean isActivityForground(Context c,String packageName, String activity){
		ActivityManager activityManager = (ActivityManager)c.getSystemService (Context.ACTIVITY_SERVICE); 

		List<RunningTaskInfo> services = activityManager.getRunningTasks(Integer.MAX_VALUE); 

		for (int i1 = 0; i1 < services.size(); i1++) { 
			if(services.get(i1).topActivity.toString().equals("ComponentInfo{"+packageName+"/"+packageName+"."+activity+"}"))
				return true;
		} 

		return false;
	}

	public static Boolean isActivityForground(Context context, Class<?> activityClass)
	{
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			if (activityClass.getCanonicalName().equalsIgnoreCase(tasks.get(0).topActivity.getClassName()))
				return true;
		}
		return false;
	}

	public static Boolean isActivityRunning(Context c, Class<?> activityClass)
	{
		ActivityManager activityManager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

		for (RunningTaskInfo task : tasks) {
			if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
				return true;
		}

		return false;
	}


	//Metodo per controllare se un servizio � attivo
	public static boolean isServiceRunning(Context c, String packageName, String serviceClassName) {
		String serviceName = packageName+"."+serviceClassName;
		ActivityManager manager = (ActivityManager)c.getSystemService(Context.ACTIVITY_SERVICE);

		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceName.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}


	//Metodo per controllare se un processo è attivo
	public static boolean isProcessRunning(Context c, String packageName, String processClassName) {
		String procesName = packageName+":"+processClassName;
		ActivityManager am = (ActivityManager)c.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> list2= am.getRunningAppProcesses();
		for (RunningAppProcessInfo ti : list2) {
			if (procesName.equals(ti.processName)) {
				return true;
			}
		}
		return false;
	}

	//Metodo per controllare la connessione ad internet
	public static boolean isInternetOn(Context c) {
		ConnectivityManager cm =  (ConnectivityManager)c.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}

		NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnected();

	}

	public static boolean isScreenOn(Context c){
		PowerManager pm = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
		return pm.isScreenOn();
	}

	public static int getScreenOrientation(Context c){
		int orientation = c.getResources().getConfiguration().orientation;
		return orientation; // return value 1 is portrait and 2 is Landscape Mode
	}

	public static boolean isTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK)
				>= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}



	public static boolean isTelephonyAvaiable(Context c){
		return c.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
	}

	public static float getDisplayDensity(Context c){
		return c.getResources().getDisplayMetrics().density;
	}

	public static float getDisplayMPX(Context c){
		float mpx = (float) c.getResources().getDisplayMetrics().widthPixels * (float) c.getResources().getDisplayMetrics().heightPixels;
		return mpx;
	}

	public static int inchToPixel(Context c, float inch){
		float xdpi = c.getResources().getDisplayMetrics().xdpi;
		return (int) (xdpi*inch);
	}








	public static void clearApplicationData(Context context) {
		File cache = context.getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String s : children) {
				File f = new File(appDir, s);
				if(deleteDir(f)){
					//					eLog.e("edo-data", String.format("**************** DELETED -> (%s) *******************", f.getAbsolutePath()));
				}
			}
		}
	}
	private static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			if(children!=null && children.length>0)
				for (int i = 0; i < children.length; i++) {
					boolean success = deleteDir(new File(dir, children[i]));
					if (!success) {
						return false;
					}
				}
		}
		return dir.delete();
	}
}
