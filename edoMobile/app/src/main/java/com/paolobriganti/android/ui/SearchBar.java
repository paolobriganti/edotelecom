package com.paolobriganti.android.ui;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Field;

/**
 * Created by PaoloBriganti on 25/08/15.
 */
public class SearchBar {

    Context c;
    Toolbar toolbar;

    public LinearLayout searchContainer;
    public EditText toolbarSearchView;
    public ImageView searchClearButton;

    OnQueryTextChangeListener onQueryTextChangeListener;

    public SearchBar(Toolbar toolbar, int textColor, int hintTextColor, String searchHintText, int cursorDrawableResource, int closeDrawableResource) {
        this.toolbar = toolbar;
        this.c = toolbar.getContext();
        setSearchBarViews(textColor, hintTextColor, searchHintText, cursorDrawableResource, closeDrawableResource);
    }

    public SearchBar(Toolbar toolbar, int textColor, int hintTextColor, String searchHintText) {
        this.toolbar = toolbar;
        this.c = toolbar.getContext();
        setSearchBarViews(textColor, hintTextColor, searchHintText, -1, -1);
    }

    private void setSearchBarViews(int textColor, int hintTextColor, String searchHintText, int cursorDrawableResource, int closeDrawableResource){
        // Setup search container view
        searchContainer = new LinearLayout(c);
        Toolbar.LayoutParams containerParams = new Toolbar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        containerParams.gravity = Gravity.CENTER_VERTICAL;
        searchContainer.setLayoutParams(containerParams);

        // Setup search view
        toolbarSearchView = new EditText(c);
        // Set width / height / gravity
        int[] textSizeAttr = new int[]{android.R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = c.obtainStyledAttributes(new TypedValue().data, textSizeAttr);
        int actionBarHeight = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, actionBarHeight);
        params.gravity = Gravity.CENTER_VERTICAL;
        params.weight = 1;
        toolbarSearchView.setLayoutParams(params);

        // Setup display
        toolbarSearchView.setBackgroundColor(Color.TRANSPARENT);
        toolbarSearchView.setPadding(2, 0, 0, 0);
        toolbarSearchView.setTextColor(textColor);
        toolbarSearchView.setGravity(Gravity.CENTER_VERTICAL);
        toolbarSearchView.setSingleLine(true);
        toolbarSearchView.setImeActionLabel(searchHintText, EditorInfo.IME_ACTION_UNSPECIFIED);
        toolbarSearchView.setHint(searchHintText);
        toolbarSearchView.setHintTextColor(hintTextColor);
        toolbarSearchView.setFocusable(true);
        toolbarSearchView.setFocusableInTouchMode(true);
        toolbarSearchView.requestFocus();

        if(cursorDrawableResource!=-1) {
            try {
                // Set cursor colour to white
                // http://stackoverflow.com/a/26544231/1692770
                // https://github.com/android/platform_frameworks_base/blob/kitkat-release/core/java/android/widget/TextView.java#L562-564
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(toolbarSearchView, cursorDrawableResource);
            } catch (Exception ignored) {
            }
        }

        // Search text changed listener
        toolbarSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(onQueryTextChangeListener!=null){
                    onQueryTextChangeListener.onQueryTextChange(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // http://stackoverflow.com/a/6438918/1692770
//                if (s.toString().length() <= 0) {
//                    toolbarSearchView.setHintTextColor(Color.parseColor("#b3ffffff"));
//                }
            }
        });
        ((LinearLayout) searchContainer).addView(toolbarSearchView);

        // Setup the clear button
        searchClearButton = new ImageView(c);
        Resources r = c.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, r.getDisplayMetrics());
        LinearLayout.LayoutParams clearParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        clearParams.gravity = Gravity.CENTER;
        searchClearButton.setLayoutParams(clearParams);
        if(closeDrawableResource!=-1) {
            searchClearButton.setImageResource(closeDrawableResource); // TODO: Get this image from here: https://github.com/google/material-design-icons
        }
        searchClearButton.setPadding(px, 0, px, 0);
        searchClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarSearchView.setText("");
            }
        });
        ((LinearLayout) searchContainer).addView(searchClearButton);

        // Add search view to toolbar and hide it
//        searchContainer.setVisibility(View.GONE);
        toolbar.addView(searchContainer);
    }

    public void setOnClearButtonClickListener(View.OnClickListener onClickListener){
        searchClearButton.setOnClickListener(onClickListener);
    }


    OnQueryTextChangeListener mOnQueryTextChangeListener;

    public void setOnQueryTextChangeListener(OnQueryTextChangeListener listener) {
        onQueryTextChangeListener = listener;
    }

    public interface OnQueryTextChangeListener {
        boolean onQueryTextChange(String text);
    }
}
