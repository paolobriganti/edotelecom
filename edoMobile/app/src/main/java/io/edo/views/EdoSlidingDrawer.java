package io.edo.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SlidingDrawer;


public class EdoSlidingDrawer extends SlidingDrawer {
	private Integer handleWidth = null;
	private Integer mHeight = null;
	private Integer mWidth = null;

	private boolean mVertical;
	private int mTopOffset;

	public void setHeight(int height) {
		this.mHeight = height;
	}

	public int getExtendedHeight() {
		return this.mHeight;
	}

	public void setWidth(int width) {
		this.mWidth = width;
	}

	public EdoSlidingDrawer(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		int orientation = attrs.getAttributeIntValue("android", "orientation", ORIENTATION_VERTICAL);
		mTopOffset = attrs.getAttributeIntValue("android", "topOffset", 0);
		mVertical = (orientation == SlidingDrawer.ORIENTATION_VERTICAL);
	}

	public EdoSlidingDrawer(Context context, AttributeSet attrs) {
		super(context, attrs);

		int orientation = attrs.getAttributeIntValue("android", "orientation", ORIENTATION_VERTICAL);
		mTopOffset = attrs.getAttributeIntValue("android", "topOffset", 0);
		mVertical = (orientation == SlidingDrawer.ORIENTATION_VERTICAL);
	}

	public Integer getHandleWidth() {
		return handleWidth;
	}

	public void setHandleWidth(Integer handleWidth) {
		this.handleWidth = handleWidth;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final View handle = getHandle();
		handle.setX(0);
		
		int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSpecSize =  MeasureSpec.getSize(widthMeasureSpec);

		int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSpecSize =  MeasureSpec.getSize(heightMeasureSpec);

		if (widthSpecMode == MeasureSpec.UNSPECIFIED || heightSpecMode == MeasureSpec.UNSPECIFIED) {
			throw new RuntimeException("SlidingDrawer cannot have UNSPECIFIED dimensions");
		}

		
		final View content = getContent();


		if (mVertical) {
			int height = heightSpecSize - handle.getMeasuredHeight() - mTopOffset;
			content.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, heightSpecMode));
			heightSpecSize = handle.getMeasuredHeight() + mTopOffset + content.getMeasuredHeight();
			widthSpecSize = content.getMeasuredWidth();
			if (handle.getMeasuredWidth() > widthSpecSize) widthSpecSize = handle.getMeasuredWidth();
		}
		else {
			int width = widthSpecSize - handle.getMeasuredWidth() - mTopOffset;
			getContent().measure(MeasureSpec.makeMeasureSpec(width, widthSpecMode), heightMeasureSpec);
			widthSpecSize = handle.getMeasuredWidth() + mTopOffset + content.getMeasuredWidth();
			heightSpecSize = content.getMeasuredHeight();
			if (handle.getMeasuredHeight() > heightSpecSize) heightSpecSize = handle.getMeasuredHeight();
		}

		if(!mVertical && mWidth!=null){
			measureChild(handle, mWidth, heightMeasureSpec);
			setMeasuredDimension(mWidth, heightSpecSize);
			int width = mWidth - handle.getWidth();
			getContent().measure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), heightMeasureSpec);

		}else if(mVertical && mHeight!=null){
			if(handleWidth!=null){
				measureChild(handle, MeasureSpec.makeMeasureSpec(handleWidth, MeasureSpec.EXACTLY), heightMeasureSpec);
			}else
				measureChild(handle, widthMeasureSpec, mHeight);

			setMeasuredDimension(widthSpecSize, mHeight);
			int height = mHeight - handle.getHeight();
			content.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
		}else{
			if(handleWidth!=null){
				measureChild(handle, MeasureSpec.makeMeasureSpec(handleWidth, MeasureSpec.EXACTLY), heightMeasureSpec);
			}else
				measureChild(handle, widthMeasureSpec, heightMeasureSpec);
			
			setMeasuredDimension(widthSpecSize, heightSpecSize);
		}

	}

}
