package io.edo.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by PaoloBriganti on 06/06/15.
 */
public class SquareLinearLayoutOnWidth extends LinearLayout{

    public SquareLinearLayoutOnWidth(Context context) {
        super(context);
    }

    public SquareLinearLayoutOnWidth(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareLinearLayoutOnWidth(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
