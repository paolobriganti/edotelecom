package io.edo.views;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.GridView;

public class GridViewPlus extends GridView{

	GestureDetectorCompat gdt;
	OnScrollToEnd scrollToEndListener;


	public GridViewPlus(Context context) {
		super(context);
		gdt = new GestureDetectorCompat(context, new SimpleGestureListener());
		// TODO Auto-generated constructor stub
	}

	public GridViewPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		gdt = new GestureDetectorCompat(context, new SimpleGestureListener());
		// TODO Auto-generated constructor stub
	}

	public GridViewPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		gdt = new GestureDetectorCompat(context, new SimpleGestureListener());
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		gdt.onTouchEvent(e);
		return super.onTouchEvent(e);
	}

	boolean isScrollEndToTop = false;
	boolean isScrollEndToBottom = false;
	
	
	
	class SimpleGestureListener extends SimpleOnGestureListener {

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			try {
				if(listIsAtTop()){
					if(!isScrollEndToTop && scrollToEndListener!=null)scrollToEndListener.onScrollEndToTop();
					isScrollEndToTop = true;
				}else{
					isScrollEndToTop = false;
				}

				if(listIsAtBottom()){
					if(!isScrollEndToBottom && scrollToEndListener!=null)scrollToEndListener.onScrollEndToBottom();
					isScrollEndToBottom = true;
				}else{
					isScrollEndToBottom = false;
				}
				
			} catch (Exception e) {

			}

			showHideFooter(e1.getRawY()-e2.getRawY());

			return false;
			//			return super.onScroll(e1, e2, distanceX, distanceY);
		}

	}


	public void setOnScrollToEndListener(int preloadItemsCount, OnScrollToEnd listener){
		scrollToEndListener = listener;
	}

	public interface OnScrollToEnd {
		
		void onScrollEndToTop();
		void onScrollEndToBottom();
	}



	private boolean listIsAtTop()   {   
		if(this.getChildCount() == 0) return true;
		return this.getChildAt(0).getTop() == 0;
	}

	private boolean listIsAtBottom()   {   
		return (this.getLastVisiblePosition() == this.getAdapter().getCount() -1 &&
				this.getChildAt(this.getChildCount() - 1).getBottom() <= this.getHeight());
	}





	//*********************************************************************************************************************
	//* For Footer
	//*********************************************************************************************************************
	private View footerView;
	//	private int footerHeight = 0;
	//
	//	private OnGlobalLayoutListener onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
	//		@Override
	//		public void onGlobalLayout() {
	//			if(footerView!=null && footerView.getHeight()>0){
	//				footerHeight = footerView.getHeight();
	///				footerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
	//			}
	//		}
	//	};

	public void setFooter(View footerView){
		this.footerView = footerView;
		//		setScroolListenerForFoother();
	}

	//	private void setScroolListenerForFoother(){
	//		footerView.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
	//	}


	public void showHideFooter(float listTranslationY){
		if(footerView!=null){
			//			float translationY = footerView.getTranslationY() + listTranslationY;
			//			
			//			if(translationY>footerHeight){
			//				translationY = footerHeight+1;
			//			}else if(translationY<0){
			//				translationY = 0;
			//			}
			//			
			//			if(listIsAtTop()){
			//				translationY = 0;
			//			}
			//			
			//			footerView.setTranslationY(translationY);

			if(this.getChildAt(0)!=null){
				int yOfFirstChild = this.getChildAt(0).getTop();

				if(yOfFirstChild == 0){

					footerView.setAlpha(1);
					footerView.setTranslationY(0);

				}else if(yOfFirstChild < 0 && listTranslationY > 0 && footerView.getAlpha() == 1 && footerView.getTranslationY() == 0){
					outToBottom(footerView, 250L);
				}else if(listTranslationY < 0  && footerView.getAlpha() == 0 && footerView.getTranslationY() == footerView.getHeight()){
					inFromBottom(footerView, 250L);
				}
			}
		}
	}



	public static ViewPropertyAnimator inFromBottom(View v, Long duration){
		v.setAlpha(0);
		v.setTranslationY(v.getHeight());
		return v.animate().setDuration(duration).
				translationY(0).alpha(1).
				setInterpolator(new DecelerateInterpolator());
	}

	public static ViewPropertyAnimator outToBottom(View v, Long duration){
		return v.animate().
				translationY(v.getHeight()).
				alpha(0).
				setDuration(duration).
				setInterpolator(new AccelerateInterpolator());
	}

}
