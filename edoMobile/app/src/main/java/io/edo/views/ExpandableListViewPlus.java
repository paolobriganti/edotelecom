package io.edo.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.widget.ExpandableListView;

import com.paolobriganti.utils.NumberUtils;

public class ExpandableListViewPlus extends ExpandableListView{
	private int SWIPE_MIN_DISTANCE = 120;
	private int SWIPE_MAX_OFF_PATH = 250;
	private int SWIPE_THRESHOLD_VELOCITY = 200;
	
	private int PULL_MAX_DISTANCE = 250;
	
	final GestureDetector gdt;
	OnPullListener pullListener;
	OnSwipeListener swipeListener;
	
	public ExpandableListViewPlus(Context context) {
		super(context);
		gdt = new GestureDetector(new SimpleGestureListener());
		
		int side = Math.min(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
		setDistances(side);
	}
	
	public ExpandableListViewPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		gdt = new GestureDetector(new SimpleGestureListener());

		int side = Math.min(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
		setDistances(side);
	}
	
	private void setDistances(int side){
		SWIPE_MIN_DISTANCE = (int)((float)side/3);
		SWIPE_MAX_OFF_PATH = (int)((float)side/2);
		SWIPE_THRESHOLD_VELOCITY = (int)((float)side/2);
		PULL_MAX_DISTANCE = (int)(side/5F *4F);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		gdt.onTouchEvent(e);
		return super.onTouchEvent(e);
	}

	boolean pullUpStarted = false;
	boolean pullDownStarted = false;
	
	class SimpleGestureListener extends SimpleOnGestureListener {
		
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			try {
	            if(listIsAtTop()){
	            	distanceY = e2.getY() - e1.getY();
	            	int percent = NumberUtils.getPercent((int)distanceY, PULL_MAX_DISTANCE);
	            	if(percent>10){
	            		pullDownStarted = true;
	            		if(pullListener!=null)pullListener.onPullDown(percent);
	            	}
	            }else if(listIsAtBottom()){
	            	distanceY = e1.getY() - e2.getY();
	            	int percent = NumberUtils.getPercent((int)distanceY, PULL_MAX_DISTANCE);
	            	if(percent>10){
	            		pullUpStarted = true;
	            		if(pullListener!=null)pullListener.onPullUp(percent);
	            	}
	            }
	        } catch (Exception e) {

	        }
	        return false;
//			return super.onScroll(e1, e2, distanceX, distanceY);
		}
		
	    @Override
	    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
	            float velocityY) {
	        try {
//	            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH){
//	                return false;
//	            }
	            
	        	if(listIsAtTop() && pullDownStarted){
	            	float distanceY = e2.getY() - e1.getY();
	            	int percent = NumberUtils.getPercent((int)distanceY, PULL_MAX_DISTANCE);
	            	if(pullListener!=null && percent>100){
	            		pullListener.onPullDownCompleted();
	            		return false;
	            	}else if(pullDownStarted){
	            		pullListener.onPullDownFailed();
	            	}
	            	pullDownStarted = false;
	        	}else if(listIsAtBottom() && pullUpStarted){
	            	float distanceY = e1.getY() - e2.getY();
	            	int percent = NumberUtils.getPercent((int)distanceY, PULL_MAX_DISTANCE);
	            	if(pullListener!=null && percent>100){
	            		pullListener.onPullUpCompleted();
	            		return false;
	            	}else if(pullUpStarted){
	            		pullListener.onPullUpFailed();
	            	}
	            	pullUpStarted = false;
	            }else if(pullDownStarted){
            		pullListener.onPullDownFailed();
            	}else if(pullUpStarted){
            		pullListener.onPullUpFailed();
            	}else
	            	return false;
	        	
	        	// right to left swipe
	            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
	                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	            	if(swipeListener!=null)swipeListener.onLeftSwipe();
	            } 
	            // left to right swipe
	            else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
	                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	            	if(swipeListener!=null)swipeListener.onRightSwipe();
	            }
	            
	            
	            
	        } catch (Exception e) {

	        }
	        return false;
	      }
	   }
	
	
	
	private boolean listIsAtTop()   {   
	    if(this.getChildCount() == 0) return true;
	    return this.getChildAt(0).getTop() == 0;
	}
    
	private boolean listIsAtBottom()   {   
	    return (this.getLastVisiblePosition() == this.getAdapter().getCount() -1 &&
	    		this.getChildAt(this.getChildCount() - 1).getBottom() <= this.getHeight());
	}
	
	
	
	
	
	
	
	public void setOnPullListener(OnPullListener listener) {
		pullListener = listener;
	}

	public interface OnPullListener {
		void onPullDown(int percent);
		void onPullDownCompleted();
		void onPullDownFailed();
		void onPullUp(int percent);
		void onPullUpCompleted();
		void onPullUpFailed();
	}
	
	
	public void setOnSwipeListener(OnSwipeListener listener) {
		swipeListener = listener;
	}

	public interface OnSwipeListener {
		void onLeftSwipe();
		void onRightSwipe();
	}
}
