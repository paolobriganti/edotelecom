package io.edo.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class LinearLayoutKeyboardListener  extends LinearLayout {

	private KeboardListener listener;

	public LinearLayoutKeyboardListener(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public LinearLayoutKeyboardListener(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public LinearLayoutKeyboardListener(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public LinearLayoutKeyboardListener(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
	}



	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int proposedheight = MeasureSpec.getSize(heightMeasureSpec);
        final int actualHeight = getHeight();

        if (actualHeight > proposedheight){
        	if(listener!=null)listener.onShowKeyboard();
        } else {
        	if(listener!=null)listener.onHideKeyboard();
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        
        if(listener!=null)listener.onKeyboardShowHideEvent();
	}


	public void setKeboardListener(KeboardListener listener){
		this.listener = listener;
	}

	public interface KeboardListener {
		void onShowKeyboard();
		void onHideKeyboard();
		void onKeyboardShowHideEvent();
	}

}
