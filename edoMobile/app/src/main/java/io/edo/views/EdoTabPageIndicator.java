package io.edo.views;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.edo.edomobile.R;
import io.edo.ui.adapters.PagerIconAdapter;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * This widget implements the dynamic action bar tab behavior that can change
 * across different configurations or circumstances.
 */
public class EdoTabPageIndicator extends HorizontalScrollView implements PageIndicator {


	public interface PageInfoProvider{
		String getTitle(int pos);
	}


	public static final CharSequence NO_TITLE = "-1NoTiTle";

	/** Title text used when no title is provided by the adapter. */
	private static final CharSequence EMPTY_TITLE = "";

	/**
	 * Interface for a callback when the selected tab has been reselected.
	 */
	public interface OnTabReselectedListener {
		/**
		 * Callback when the selected tab has been reselected.
		 *
		 * @param position Position of the current center item.
		 */
		void onTabReselected(int position);
	}

	private Runnable mTabSelector;

	private final OnClickListener mTabClickListener = new OnClickListener() {
		public void onClick(View view) {
			TabView tabView = (TabView)view;
			final int oldSelected = mViewPager.getCurrentItem();
			final int newSelected = tabView.getIndex();
			mViewPager.setCurrentItem(newSelected);
			if (oldSelected == newSelected && mTabReselectedListener != null) {
				mTabReselectedListener.onTabReselected(newSelected);
			}
		}
	};

	private final EdoPagerIndicatorLayout mTabLayout;

	private ViewPager mViewPager;
	private OnPageChangeListener mListener;

	private int mMaxTabWidth;
	private int mSelectedTabIndex;

	private OnTabReselectedListener mTabReselectedListener;

	public EdoTabPageIndicator(Context context) {
		this(context, null);
	}

	public EdoTabPageIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		setHorizontalScrollBarEnabled(false);

		mTabLayout = new EdoPagerIndicatorLayout(context, R.attr.vpiTabPageIndicatorStyle);
		addView(mTabLayout, new ViewGroup.LayoutParams(WRAP_CONTENT, MATCH_PARENT));
	}

	public void setOnTabReselectedListener(OnTabReselectedListener listener) {
		mTabReselectedListener = listener;
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		final boolean lockedExpanded = widthMode == MeasureSpec.EXACTLY;
		setFillViewport(lockedExpanded);

		final int childCount = mTabLayout.getChildCount();
		if (childCount > 1 && (widthMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.AT_MOST)) {
			if (childCount > 2) {
				mMaxTabWidth = (int)(MeasureSpec.getSize(widthMeasureSpec) * 0.4f);
			} else {
				mMaxTabWidth = MeasureSpec.getSize(widthMeasureSpec) / 2;
			}
		} else {
			mMaxTabWidth = -1;
		}

		final int oldWidth = getMeasuredWidth();
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int newWidth = getMeasuredWidth();

		if (lockedExpanded && oldWidth != newWidth) {
			// Recenter the tab display if we're at a new (scrollable) size.
			setCurrentItem(mSelectedTabIndex);
		}
	}

	private void animateToTab(final int position) {
		final View tabView = mTabLayout.getChildAt(position);
		if (mTabSelector != null) {
			removeCallbacks(mTabSelector);
		}
		mTabSelector = new Runnable() {
			public void run() {
				final int scrollPos = tabView.getLeft() - (getWidth() - tabView.getWidth()) / 2;
				smoothScrollTo(scrollPos, 0);
				mTabSelector = null;
			}
		};
		post(mTabSelector);
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		if (mTabSelector != null) {
			// Re-post the selector we saved
			post(mTabSelector);
		}
	}

	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mTabSelector != null) {
			removeCallbacks(mTabSelector);
		}
	}

	private void addTab(int index, CharSequence text, int iconResId) {
		if(!text.equals(NO_TITLE)){
			TabView tabView = new TabView(getContext());
			tabView.mIndex = index;
			tabView.setFocusable(true);
			tabView.setOnClickListener(mTabClickListener);
			tabView.setText(text);
			tabView.setEllipsize(TruncateAt.MARQUEE); 
			tabView.setSingleLine(true);
			if (iconResId != 0) {
				tabView.setCompoundDrawablesWithIntrinsicBounds(iconResId, 0, 0, 0);
			}

			mTabLayout.addView(tabView, new LinearLayout.LayoutParams(0, MATCH_PARENT, 1));
		}
	}

	public TabView getTab(int index){
		if(mTabLayout!=null && mTabLayout.getChildCount()>index){
			return (TabView) mTabLayout.getChildAt(index);
		}
		return null;
	}
	
	//    private void addPlusTab(int index) {
		//    	plusTab = new TabView(getContext());
	//    	plusTab.mIndex = index;
	//    	plusTab.setFocusable(true);
	//    	plusTab.setText(null);
	//    	plusTab.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_plus, 0, 0, 0);
	//	    mTabLayout.addView(plusTab, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, MATCH_PARENT));
	//    }

	@Override
	public void onPageScrollStateChanged(int arg0) {
		if (mListener != null) {
			mListener.onPageScrollStateChanged(arg0);
		}
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		if (mListener != null) {
			mListener.onPageScrolled(arg0, arg1, arg2);
		}
	}

	@Override
	public void onPageSelected(int arg0) {
		setCurrentItem(arg0);
		if (mListener != null) {
			mListener.onPageSelected(arg0);
		}
	}

	@Override
	public void setViewPager(ViewPager view) {

		if (mViewPager == view) {
			return;
		}
		if (mViewPager != null) {
			mViewPager.setOnPageChangeListener(null);
		}
		final PagerAdapter adapter = view.getAdapter();
		if (adapter == null) {
			throw new IllegalStateException("ViewPager does not have adapter instance.");
		}
		mViewPager = view;
		view.setOnPageChangeListener(this);
		notifyDataSetChanged();
	}

	public void notifyDataSetChanged() {
		mTabLayout.removeAllViews();
		PagerAdapter adapter = mViewPager.getAdapter();
		PagerIconAdapter iconAdapter = null;
		if (adapter instanceof PagerIconAdapter) {
			iconAdapter = (PagerIconAdapter)adapter;
		}
		final int count = adapter.getCount();
		for (int i = 0; i < count; i++) {
			CharSequence title = adapter.getPageTitle(i);
			if (title == null) {
				title = EMPTY_TITLE;
			}
			int iconResId = 0;
			if (iconAdapter != null) {
				iconResId = iconAdapter.getIconResId(i);
			}
			addTab(i, title, iconResId);
		}
		//        addPlusTab(count);
		if (mSelectedTabIndex > count) {
			mSelectedTabIndex = count;
		}
		setCurrentItem(mSelectedTabIndex);
		requestLayout();
	}

	@Override
	public void setViewPager(ViewPager view, int initialPosition) {
		setViewPager(view);
		setCurrentItem(initialPosition);
	}

	@Override
	public void setCurrentItem(int item) {
		//        if (mViewPager == null) {
		//            throw new IllegalStateException("ViewPager has not been bound.");
		//        }
		mSelectedTabIndex = item;

		if (mViewPager != null)
			mViewPager.setCurrentItem(item);

		final int tabCount = mTabLayout.getChildCount();
		for (int i = 0; i < tabCount; i++) {
			final View child = mTabLayout.getChildAt(i);
			final boolean isSelected = (i == item);
			child.setSelected(isSelected);
			if (isSelected) {
				animateToTab(item);
			}
		}
	}

	@Override
	public void setOnPageChangeListener(OnPageChangeListener listener) {
		mListener = listener;
	}

	public class TabView extends TextView {
		private int mIndex;

		public TabView(Context context) {
			super(context, null, R.attr.vpiTabPageIndicatorStyle);
		}

		@Override
		public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);

			// Re-measure if we went beyond our maximum size.
			if (mMaxTabWidth > 0 && getMeasuredWidth() > mMaxTabWidth) {
				super.onMeasure(MeasureSpec.makeMeasureSpec(mMaxTabWidth, MeasureSpec.EXACTLY),
						heightMeasureSpec);
			}
		}

		public int getIndex() {
			return mIndex;
		}
	}

	private OnTitleLongClickListener mOnLongClickHandler;

	public interface OnTitleLongClickListener{
		void onLongClicked(View v);
	}

	public void setOnTitleLongClickListener(int position, OnTitleLongClickListener onTitleLongClickListener){
		this.mOnLongClickHandler = onTitleLongClickListener;

		for(int i=0; i<mTabLayout.getChildCount()-1; i++)
			mTabLayout.getChildAt(i).setOnLongClickListener(null);
		mTabLayout.getChildAt(position).setOnLongClickListener(new OnTitleLongClickedListener());
	}
	class OnTitleLongClickedListener implements OnLongClickListener{


		@Override
		public boolean onLongClick(View arg0) {
			if(mOnLongClickHandler != null){
				mOnLongClickHandler.onLongClicked(EdoTabPageIndicator.this);
			}
			return false;
		}
	}



	//	private OnPlusClickListener mOnPlusClickHandler;

	//    public interface OnPlusClickListener{
	//	    void onPlusClicked(View v);
	//	}
	//	
	//	public void setOnPlusClickListener(OnPlusClickListener onPlusClickListener){
	//	    this.mOnPlusClickHandler = onPlusClickListener;
	//
	//	    plusTab.setOnClickListener(new OnPlusClickedListener());
	//	}
	//	class OnPlusClickedListener implements android.view.View.OnClickListener{
	//
	//		@Override
	//		public void onClick(View arg0) {
	//			if(mOnPlusClickHandler != null){
	//				mOnPlusClickHandler.onPlusClicked(EdoTabPageIndicator.this);
	//	        }
	//			
	//		}
	//	}
}
