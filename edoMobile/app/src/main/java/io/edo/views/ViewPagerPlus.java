package io.edo.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ViewPagerPlus  extends ViewPager {
	private boolean swipeable = true;
	private float mTouchX;
	private int minSwipeDistance = 0;
	OnSwipeListener mListener;

	public ViewPagerPlus(Context context) {
		super(context);
	}

	public ViewPagerPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	// Call this method in your motion events when you want to disable or enable
	// It should work as desired.
	public void setSwipeable(boolean swipeable) {
		this.swipeable = swipeable;
	}

	public void setMinSwipeDistance(int minSwipeDistance) {
		this.minSwipeDistance = minSwipeDistance;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(mListener!=null)mListener.onTouch(event);
		try{
			return (this.swipeable) ? super.onTouchEvent(event) : false; 
		}catch (IllegalArgumentException e){}
		return false;
	}

	boolean isSwipeOutAtStart = false;
	boolean isSwipeOutAtEnd = false;

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {

		if(this.swipeable){
			boolean response = super.onInterceptTouchEvent(event);

			float x = event.getX();
			switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				setPauseWork(false);
				mTouchX = x;
				break;
			case MotionEvent.ACTION_MOVE:
				setPauseWork(true);
				if (mTouchX < x && getCurrentItem() == 0) {
					if(!isSwipeOutAtStart && mListener!=null)mListener.onSwipeOutAtStart();
					isSwipeOutAtStart = true;
				}else{
					isSwipeOutAtStart = false;
				}

				if (mTouchX > x && getCurrentItem() == getAdapter().getCount() - 1) {
					if(!isSwipeOutAtEnd && mListener!=null)mListener.onSwipeOutAtEnd();
					isSwipeOutAtEnd = true;
				}else{
					isSwipeOutAtEnd = false;
				}

				float dX = Math.abs(x - mTouchX);
				if (dX > minSwipeDistance)
					return response;
				else
					return false;
			default:

				break;
			}
			return response;
		}

		return false;
	}




	@Override
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
		if (v instanceof TouchImageView) {
			//
			// canScrollHorizontally is not supported for Api < 14. To get around this issue,
			// ViewPager is extended and canScrollHorizontallyFroyo, a wrapper around
			// canScrollHorizontally supporting Api >= 8, is called.
			//
			return ((TouchImageView) v).canScrollHorizontallyFroyo(-dx);
			
		} else {
			return super.canScroll(v, checkV, dx, x, y);
		}
	}





	public void setOnSwipeListener(OnSwipeListener listener) {
		mListener = listener;
	}

	public interface OnSwipeListener {
		void onSwipeOutAtStart();
		void onSwipeOutAtEnd();
		void onTouch(MotionEvent event);
	}

	
	
	public boolean mPauseWork = false;
	public final Object mPauseWorkLock = new Object();
	public void setPauseWork(boolean pauseWork) {
		synchronized (mPauseWorkLock) {
			mPauseWork = pauseWork;
			if (!mPauseWork) {
				mPauseWorkLock.notifyAll();
			}
		}
	}
}
