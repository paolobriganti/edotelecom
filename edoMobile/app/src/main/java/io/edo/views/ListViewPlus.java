package io.edo.views;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.paolobriganti.utils.NumberUtils;

public class ListViewPlus extends ListView{
	private int SWIPE_MIN_DISTANCE = 120;
	private int SWIPE_MAX_OFF_PATH = 250;
	private int SWIPE_THRESHOLD_VELOCITY = 200;

	private int PULL_MAX_DISTANCE = 250;

	GestureDetectorCompat gdt;
	OnPullListener pullListener;
	OnSwipeListener swipeListener;
	OnScrollToEnd scrollToEndListener;

	public ListViewPlus(Context context) {
		super(context);
		gdt = new GestureDetectorCompat(context, new SimpleGestureListener());

		int side = Math.min(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
		setDistances(side);
	}

	public ListViewPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		gdt = new GestureDetectorCompat(context, new SimpleGestureListener());

		int side = Math.min(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
		setDistances(side);
	}


	@Override
	public void setAdapter(ListAdapter adapter) {
		// TODO Auto-generated method stub
		super.setAdapter(adapter);
	}


	private void setDistances(int side){
		SWIPE_MIN_DISTANCE = (int)((float)side/3);
		SWIPE_MAX_OFF_PATH = (int)((float)side/2);
		SWIPE_THRESHOLD_VELOCITY = (int)((float)side/2);
		PULL_MAX_DISTANCE = (int)(side/5F *4F);
	}


	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		gdt.onTouchEvent(e);


		return super.onTouchEvent(e);
	}

	boolean pullUpStarted = false;
	boolean pullDownStarted = false;

	boolean isScrollEndToTop = false;
	boolean isScrollEndToBottom = false;
	boolean isScrollToRefreshPoint = false;
	
	
	class SimpleGestureListener extends SimpleOnGestureListener {

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			try {
				if(listIsAtTop()){
					if(!isScrollEndToTop && scrollToEndListener!=null)scrollToEndListener.onScrollEndToTop();
					isScrollEndToTop = true;

					distanceY = e2.getY() - e1.getY();
					int percent = NumberUtils.getPercent((int)distanceY, PULL_MAX_DISTANCE);
					if(percent>10){
						pullDownStarted = true;
						if(pullListener!=null)pullListener.onPullDown(percent);
					}
				}else{
					isScrollEndToTop = false;
				}

				if(listIsAtBottom()){
					if(!isScrollEndToBottom && scrollToEndListener!=null)scrollToEndListener.onScrollEndToBottom();
					isScrollEndToBottom = true;


					distanceY = e1.getY() - e2.getY();
					int percent = NumberUtils.getPercent((int)distanceY, PULL_MAX_DISTANCE);
					if(percent>10){
						pullUpStarted = true;
						if(pullListener!=null)pullListener.onPullUp(percent);
					}
				}else{
					isScrollEndToBottom = false;
				}
				

			} catch (Exception e) {

			}

			showHideFooter(e1.getRawY()-e2.getRawY());

			return false;
			//			return super.onScroll(e1, e2, distanceX, distanceY);
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				//	            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH){
				//	                return false;
				//	            }

				if(listIsAtTop() && pullDownStarted){
					float distanceY = e2.getY() - e1.getY();
					int percent = NumberUtils.getPercent((int)distanceY, PULL_MAX_DISTANCE);
					if(pullListener!=null && percent>100){
						pullListener.onPullDownCompleted();
						return false;
					}else if(pullDownStarted){
						pullListener.onPullDownFailed();
					}
					pullDownStarted = false;
				}else if(listIsAtBottom() && pullUpStarted){
					float distanceY = e1.getY() - e2.getY();
					int percent = NumberUtils.getPercent((int)distanceY, PULL_MAX_DISTANCE);
					if(pullListener!=null && percent>100){
						pullListener.onPullUpCompleted();
						return false;
					}else if(pullUpStarted){
						pullListener.onPullUpFailed();
					}
					pullUpStarted = false;
				}else if(pullDownStarted){
					pullListener.onPullDownFailed();
				}else if(pullUpStarted){
					pullListener.onPullUpFailed();
				}else
					return false;

				// right to left swipe
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					if(swipeListener!=null)swipeListener.onLeftSwipe();
				} 
				// left to right swipe
				else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					if(swipeListener!=null)swipeListener.onRightSwipe();
				}



			} catch (Exception e) {

			}
			return false;
		}
	}



	private boolean listIsAtTop()   {   
		if(this.getChildCount() == 0) return true;
		return this.getChildAt(0).getTop() == 0;
	}

	private boolean listIsAtBottom()   {   
		return (this.getLastVisiblePosition() == this.getAdapter().getCount() -1 &&
				this.getChildAt(this.getChildCount() - 1).getBottom() <= this.getHeight());
	}






	public void setOnPullListener(OnPullListener listener, int itemsOffset){
		pullListener = listener;
	}

	public interface OnPullListener {
		void onPullDown(int percent);
		void onPullDownCompleted();
		void onPullDownFailed();
		void onPullUp(int percent);
		void onPullUpCompleted();
		void onPullUpFailed();
	}


	public void setOnSwipeListener(OnSwipeListener listener) {
		swipeListener = listener;
	}

	public interface OnSwipeListener {
		void onLeftSwipe();
		void onRightSwipe();
	}

	public void setOnScrollToEndListener(int preloadItemsCount, OnScrollToEnd listener){
		scrollToEndListener = listener;
	}

	public interface OnScrollToEnd {
		void onScrollEndToTop();
		void onScrollEndToBottom();
	}











	//*********************************************************************************************************************
	//* For Footer
	//*********************************************************************************************************************
	private View footerView;
	//	private int footerHeight = 0;
	//
	//	private OnGlobalLayoutListener onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
	//		@Override
	//		public void onGlobalLayout() {
	//			if(footerView!=null && footerView.getHeight()>0){
	//				footerHeight = footerView.getHeight();
	///				footerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
	//			}
	//		}
	//	};

	public void setFooter(View footerView){
		this.footerView = footerView;
		//		setScroolListenerForFoother();
	}

	//	private void setScroolListenerForFoother(){
	//		footerView.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
	//	}


	public void showHideFooter(float listTranslationY){
		if(footerView!=null){
			//			float translationY = footerView.getTranslationY() + listTranslationY;
			//			
			//			if(translationY>footerHeight){
			//				translationY = footerHeight+1;
			//			}else if(translationY<0){
			//				translationY = 0;
			//			}
			//			
			//			if(listIsAtTop()){
			//				translationY = 0;
			//			}
			//			
			//			footerView.setTranslationY(translationY);

			if(this.getChildAt(0)!=null){

				int yOfFirstChild = this.getChildAt(0).getTop();

				if(yOfFirstChild == 0){

					footerView.setAlpha(1);
					footerView.setTranslationY(0);

				}else if(yOfFirstChild < 0 && listTranslationY > 0 && footerView.getAlpha() == 1 && footerView.getTranslationY() == 0){
					outToBottom(footerView, 250L);
				}else if(listTranslationY < 0  && footerView.getAlpha() == 0 && footerView.getTranslationY() == footerView.getHeight()){
					inFromBottom(footerView, 250L);
				}
			}
		}
	}



	public static ViewPropertyAnimator inFromBottom(View v, Long duration){
		v.setAlpha(0);
		v.setTranslationY(v.getHeight());
		return v.animate().setDuration(duration).
				translationY(0).alpha(1).
				setInterpolator(new DecelerateInterpolator());
	}

	public static ViewPropertyAnimator outToBottom(View v, Long duration){
		return v.animate().
				translationY(v.getHeight()).
				alpha(0).
				setDuration(duration).
				setInterpolator(new AccelerateInterpolator());
	}


}
