package io.edo.views;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import io.edo.edomobile.R;

public class EdoProgressBar extends ProgressBar{
	private Context context;
	private boolean disappear;
	
	public EdoProgressBar(Context context) {
		super(context);
		this.context = context;
	}
	public EdoProgressBar(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    this.context = context;
	}

	public EdoProgressBar(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	    this.context = context;
	}

	public void setNormalMode(){
		((Activity) context).runOnUiThread(new Runnable() {public void run() {
			EdoProgressBar.this.setProgress(100);
			EdoProgressBar.this.setVisibility(ProgressBar.VISIBLE);
			EdoProgressBar.this.setProgressDrawable(context.getResources().getDrawable(R.drawable.progress_horizontal_white));
		}});
	}
	
	public void setDownloadInProgressMode(){
		((Activity) context).runOnUiThread(new Runnable() {public void run() {
			EdoProgressBar.this.setVisibility(ProgressBar.VISIBLE);
			EdoProgressBar.this.setProgressDrawable(context.getResources().getDrawable(R.drawable.progress_horizontal_white));
		}});
	}
	
	public void setUploadInProgressMode(){
		((Activity) context).runOnUiThread(new Runnable() {public void run() {
			EdoProgressBar.this.setVisibility(ProgressBar.VISIBLE);
			EdoProgressBar.this.setProgressDrawable(context.getResources().getDrawable(R.drawable.progress_horizontal_white));
		}});
	}
	
	public void setCompletedMode(){
		((Activity) context).runOnUiThread(new Runnable() {public void run() {
			EdoProgressBar.this.setVisibility(ProgressBar.VISIBLE);
			EdoProgressBar.this.setProgress(100);
			EdoProgressBar.this.setProgressDrawable(context.getResources().getDrawable(R.drawable.progress_horizontal_white));
		}});
		
		new Thread(){@Override public void run(){Looper.myLooper();
			try {
				sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			((Activity) context).runOnUiThread(new Runnable() {public void run() {
				EdoProgressBar.this.setProgressDrawable(context.getResources().getDrawable(R.drawable.progress_horizontal_white));
				if(disappear)setVisibility(ProgressBar.INVISIBLE);
			}});
		}}.start();
	}
	
	public void setErrorMode(){
		((Activity) context).runOnUiThread(new Runnable() {public void run() {
			EdoProgressBar.this.setVisibility(ProgressBar.VISIBLE);
			EdoProgressBar.this.setProgress(100);
			EdoProgressBar.this.setProgressDrawable(context.getResources().getDrawable(R.drawable.progress_horizontal_white));
		}});
		
		new Thread(){@Override public void run(){Looper.myLooper();
			try {
				sleep(6000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			((Activity) context).runOnUiThread(new Runnable() {public void run() {
				EdoProgressBar.this.setProgressDrawable(context.getResources().getDrawable(R.drawable.progress_horizontal_white));
				if(disappear)setVisibility(ProgressBar.INVISIBLE);
			}});
		}}.start();
	}
	
	public void setVisibile(final Boolean isVisible){
		((Activity) context).runOnUiThread(new Runnable() {public void run() {
			if(isVisible)
				EdoProgressBar.this.setVisibility(ProgressBar.VISIBLE);
			else
				EdoProgressBar.this.setVisibility(ProgressBar.GONE);
		}});
	}
	
	public void setDisappear(boolean disappear){
		this.disappear = disappear;
	}
	
	
}
