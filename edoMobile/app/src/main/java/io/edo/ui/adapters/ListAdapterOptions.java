package io.edo.ui.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class ListAdapterOptions extends BaseAdapter {
	private final Context context;
	private final ArrayList<Integer> titles;


	public ListAdapterOptions(Context context, ArrayList<Integer> titlesResources) {
		this.context = context;
		this.titles = titlesResources;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.row_edo_option, parent, false);
		TextView tv_title = (TextView) rowView.findViewById(R.id.tv_text);
		if(titles!=null && titles.size()>0 && titles.get(position)!=null)
			tv_title.setText(titles.get(position));
		else 
			tv_title.setText(" ");
		
		return rowView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
} 