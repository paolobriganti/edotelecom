package io.edo.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import io.edo.edomobile.R;

public class EdoDialogOne extends Dialog{

	String title;
	int titleRes = 0;
	String text;
	int textRes = 0;
	String btPositiveText;
	int btPositiveTextRes = 0;
	
	TextView tv_title;
	TextView tv_text;
	Button bt_positive;

	OnPositiveClickListener positiveListener;
	
	public EdoDialogOne(Context context) {
		super(context);
		btPositiveTextRes = R.string.ok;
		this.positiveListener = new OnPositiveClickListener() {
			
			@Override
			public void onClick(View v) {
				EdoDialogOne.this.cancel();
			}
		};
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_edo_onebutton);


		EdoUI.setDialogWindowParams(this);
		
		
		tv_title = (TextView) this.findViewById(R.id.tv_title);
		tv_text = (TextView) this.findViewById(R.id.tv_text);
		bt_positive = (Button) this.findViewById(R.id.bt_positive);
		
		setTitle(title);
		setTitle(titleRes);
		setText(text);
		setText(textRes);
		setPositiveButtonText(btPositiveText);
		setPositiveButtonText(btPositiveTextRes);
		setOnPositiveClickListener(positiveListener);
	}
	
	public void setTitle (final String title){
		if(JavaUtils.isNotEmpty(title)){
			this.title = title;
			if(tv_title!=null){
				tv_title.setText(title);
				tv_title.setVisibility(TextView.VISIBLE);
			}
		}else if(tv_title!=null && JavaUtils.isEmpty(tv_title.getText().toString())){
			tv_title.setVisibility(TextView.GONE);
		}
	}

	public void setTitle (final int stringResourceId){
		if(stringResourceId!=0){
			this.titleRes = stringResourceId;
			if(tv_title!=null){
				tv_title.setText(stringResourceId);
				tv_title.setVisibility(TextView.VISIBLE);
			}
		}else if(tv_title!=null && JavaUtils.isEmpty(tv_title.getText().toString())){
			tv_title.setVisibility(TextView.GONE);
		}
	}

	public void setText (final String text){
		if(JavaUtils.isNotEmpty(text)){
			this.text = text;
			if(tv_text!=null){
				tv_text.setText(text);
				tv_text.setVisibility(TextView.VISIBLE);
			}
		}else if(tv_text!=null && JavaUtils.isEmpty(tv_text.getText().toString())){
			tv_text.setVisibility(TextView.GONE);
		}
	}

	public void setText (final int stringResourceId){
		if(stringResourceId!=0){
			this.textRes = stringResourceId;
			if(tv_text!=null){
				tv_text.setText(stringResourceId);
				tv_text.setVisibility(TextView.VISIBLE);
			}
		}else if(tv_text!=null && JavaUtils.isEmpty(tv_text.getText().toString())){
			tv_text.setVisibility(TextView.GONE);
		}
	}
	
	public void setPositiveButtonText (final String text){
		if(JavaUtils.isNotEmpty(text)){
			this.btPositiveText = text;
			if(bt_positive!=null)
				bt_positive.setText(text);
		}
	}

	public void setPositiveButtonText (final int stringResourceId){
		if(stringResourceId!=0){
			this.btPositiveTextRes = stringResourceId;
			if(bt_positive!=null)
				bt_positive.setText(stringResourceId);
		}
	}
	
	public void setOnPositiveClickListener(OnPositiveClickListener listener) {
		if(listener!=null){
			this.positiveListener = listener;
			if(bt_positive!=null)
				bt_positive.setOnClickListener(listener);
		}
    }
	public interface OnPositiveClickListener extends View.OnClickListener{
    }
	

}
