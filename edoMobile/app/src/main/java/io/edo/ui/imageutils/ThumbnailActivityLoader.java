package io.edo.ui.imageutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.paolobriganti.android.AUI;
import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;

public class ThumbnailActivityLoader extends ImageLoader{
    private FragmentActivity c;
    private ThisApp app;
    private Executor executor;

    public static ThumbnailActivityLoader getInstance(FragmentActivity fragmentActivity){
        ThisApp app = ThisApp.getInstance(fragmentActivity);
        if(app.thumbnailActivityLoader==null){
            app.thumbnailActivityLoader = new ThumbnailActivityLoader(fragmentActivity);
        }
        return app.thumbnailActivityLoader;
    }

    public ThumbnailActivityLoader(FragmentActivity fragmentActivity){
        super(fragmentActivity, Constants.THUMB_CACHE_DIR_NAME, getImageViewSideSize(fragmentActivity), getImageViewSideSize(fragmentActivity));
        this.c = fragmentActivity;
        this.app = ThisApp.getInstance(c);
        this.executor = Executors.newFixedThreadPool(3);
    }

    public static int getImageViewSideSize(Context c){
        int w = c.getResources().getDisplayMetrics().widthPixels;
        return (w/ EdoUI.getFilesColumnsNumber(c));
    }

    @Override
    public Executor getExecutor() {
        return executor;
    }


    public void loadImage(LActivity a, ImageView imageView)
    {
        ImageData imageData = new ImageData(a);

        loadDataToImageView(imageData, imageView);
    }

    @Override
    public String getKeyFromData(Object data) {
        ImageData imageData = (ImageData) data;
        return String.valueOf(imageData.a.getId());
    }

    @Override
    public Bitmap getLoadingImageFromData(Object data) {
        ImageData imageData = (ImageData) data;
        return BitmapFactory.decodeResource(mContext.getResources(), R.color.transparent);
    }

    @Override
    protected Bitmap processBitmap(Object data) {
        try {
            ImageData imageData = (ImageData) data;
            LActivity a = imageData.a;

            if(a !=null) {
                if(a.isFileResource()) {
                    LFile file = a.getFileResource(c);

                    if(file!=null){

                        if(file.isFolder() && !file.hasThumbnail())
                            return null;

                        if(file.isSnippet())
                            return ThumbnailManager.getSnippetPreview(c,file,mImageHeight);

                        if(file.isStored()){
                            if(file.isImageRaster()){
                                return processBitmapFromLocalPath(file.getLocalUri());
                            }else if(file.isVideo()){
                                return ThumbnailManager.getVideoPreviewOfLocalFile(c,file.getLocalUri(),mImageHeight);
                            }
                        }

                        String thumbLink = ThumbnailManager.getThumbnailWebLink(mContext,file,mImageHeight);
                        return processBitmapFromHttpUrl(thumbLink);
                    }
                }
            }
        } catch (Throwable ex){

            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError){
                clearCache();
                closeCacheInternal();
            }
        }
        return null;
    }


    @Override
    public void onBitmapProcessed(Object data, ImageView imageView, Drawable value) {
        setImage((ImageData) data, imageView, value);
    }

    public void setImage(ImageData imageData, ImageView imageView, Drawable drawable) {


        if(drawable!=null){

            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            setImageDrawable(imageData, imageView, drawable);


        }else if(imageData!=null){

            LFile file = imageData.a.getFileResource();

            if(file!=null){

                int icon = ThumbnailManager.getNoPreviewRes(c, file);
                imageView.setImageResource(icon);
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                int pad = (int) AUI.convertDpToPixel(c, 10F);
                imageView.setPadding(pad, pad, pad, pad);
                if(file.isFolder() || file.isSpace()) {
                    imageView.setBackgroundColor(file.getThumbnailColor(c,R.color.edo_folder_default));
                }else{
                    String kind = file.getvType();

                    int colorRes = ThumbnailManager.getColorRes(c, kind);
                    imageView.setBackgroundResource(colorRes);
                }


            }



        }
    }




    public class ImageData{
        public LActivity a;

        public ImageData(LActivity a) {
            this.a = a;
        }
    }
}
