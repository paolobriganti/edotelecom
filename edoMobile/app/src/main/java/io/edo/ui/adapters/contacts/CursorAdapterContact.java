package io.edo.ui.adapters.contacts;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.paolobriganti.android.VU;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LContact;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.imageutils.ContactPhotoLoader;

public class CursorAdapterContact extends CursorAdapter {
	private LayoutInflater mLayoutInflater;
	private Context context;
	private ContactPhotoLoader contactPhotoLoader; 
	private Integer rightIcon = null;
	private ThisApp app;

	public CursorAdapterContact(Context context, Cursor c, ContactPhotoLoader contactPhotoLoader, Integer rightIcon) {
		super(context, c, false);
		this.context = context;
		this.app = ThisApp.getInstance(context);
		this.mLayoutInflater = LayoutInflater.from(context); 
		this.contactPhotoLoader = contactPhotoLoader;
		this.rightIcon = rightIcon;

	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View rowView = mLayoutInflater.inflate(R.layout.row_edo_contact_left_image, parent, false);
		return rowView;
	}


	@Override
	public void bindView(View rowView, Context context, Cursor c) {

		LContact contact = new LContact(context, c);

		if(contact!=null){


			TextView tv_title = (TextView) rowView.findViewById(R.id.tv_title);
			String contactName = contact.getNameSurname();

			String emailAddress = contact.getPrincipalEmailAddress();

			if(JavaUtils.isNotEmpty(contactName)){
				contactName = VU.replaceBlankSpaces(contactName);
				tv_title.setText(contactName);
			}else if(JavaUtils.isNotEmpty(emailAddress)){
				tv_title.setText(emailAddress);
			}else{
				tv_title.setText(R.string.contact);
			}

			String subtitle = emailAddress;

			TextView tv_subtitle = (TextView) rowView.findViewById(R.id.tv_sub_title);
//			if(!contact.isGroup() && !contact.isEdoUser()) {
//				tv_subtitle.setVisibility(View.VISIBLE);
//				tv_subtitle.setText(R.string.notOnEdo);
//			}else

			if(!contact.isGroup()){
				tv_subtitle.setVisibility(View.VISIBLE);
				if(JavaUtils.isNotEmpty(contactName)){
					tv_subtitle.setText(subtitle);
				}else{
					tv_subtitle.setText(null);
				}
			}else{
				tv_subtitle.setVisibility(View.GONE);
				tv_subtitle.setText(null);
			}

			ImageView contact_photo = (ImageView) rowView.findViewById(R.id.contact_photo);
			contactPhotoLoader.loadImage(contact, contact_photo);


			//			if(selectedContact!=null && selectedContact!=null && selectedContact.equals(contact)){
			//				rowView.setBackgroundResource(R.drawable.bg_gray_light_gray);
			//			}else{
			//				rowView.setBackgroundResource(R.drawable.bg_transparent_gray_light);
			//			}

//			Button bt_edocontact = (Button) rowView.findViewById(R.id.bt_edocontact);
//
//			Integer count = contact.getUnreadPushCount();
//			if(count!=null && count>0){
//				String countString = count>9? "9+":""+count;
//				bt_edocontact.setText(countString);
//				bt_edocontact.setBackgroundResource(R.drawable.ripple_lime);
//				bt_edocontact.setVisibility(TextView.VISIBLE);
//			}else {
//				if (JavaUtils.isNotEmpty(contact.getEdoUserId())) {
//					bt_edocontact.setVisibility(ImageView.VISIBLE);
//				} else {
//					bt_edocontact.setVisibility(ImageView.GONE);
//				}
//			}

			ImageView iv_rightIcon = (ImageView) rowView.findViewById(R.id.iv_rightIcon);
			if(rightIcon!=null){
				iv_rightIcon.setImageResource(rightIcon);
				iv_rightIcon.setVisibility(View.VISIBLE);
			}


			rowView.setTag(contact);
		}

	}















	
}
