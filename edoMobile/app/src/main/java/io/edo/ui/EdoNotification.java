package io.edo.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.text.Html;

import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.UnreadInfo;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.settings.EdoPreferences;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.ui.imageutils.ThumbnailManager;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class EdoNotification {
	public static final int ID_WELCOME = 0;
	public static final int ID_DOWNLOAD_DATA = 1;
	public static final int ID_DOWNLOAD_DATA_PERSONAL = 2;
	public static final int ID_DOWNLOAD_DATA_CONTACTS = 3;
	public static final int ID_PERSONAL = 4;
	public static final int ID_CONTACTS = 5;
	public static final int ID_GROUP = 6;
	public static final int ID_DOWNLOAD = 7;
	public static final int ID_UPLOAD = 8;
	public static final int ID_TRANSFER = 9;
	public static final int ID_TEST = 20;
	private int imgSideSize=100;

	public static final int INTERVAL_BETWEEN_SOUNDS_NOTIFICATION = 60000;

	Class<?> cls;

	Context c;
	ThisApp app;
	private NotificationManager mNotificationManager;

	private NotificationCompat.Builder mBuilder;
	private int id = 0;

	public EdoNotification(Context c, Class<?> cls) {
		this.c = c;
		this.app = ThisApp.getInstance(c);
		this.cls = cls;
		mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		imgSideSize = (int) ((float)c.getResources().getDisplayMetrics().widthPixels/7F);
	}

	public void showSimpleNotification(String title, String subtitle){

		Intent i = new Intent(c, cls);
		i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

		PendingIntent contentIntent = PendingIntent.getActivity(c, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder =
				new NotificationCompat.Builder(c)
						.setSmallIcon(R.drawable.ic_status_edo)
						.setLargeIcon(BitmapFactory.decodeResource(c.getResources(), R.drawable.ic_noty_edo))
						.setContentTitle(title)
						.setStyle(new NotificationCompat.BigTextStyle()
								.bigText(subtitle))
						.setContentText(subtitle);
		mBuilder.setAutoCancel(true);
		mBuilder.setContentIntent(contentIntent);

		id = ID_WELCOME;

		Notification notification = mBuilder.build();
		notification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
		notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;

		mNotificationManager.notify(id, notification);
	}






	public void buildDataNotification(int id, String title, String subTitle){

		mBuilder =
				new NotificationCompat.Builder(c)
		.setContentTitle(title)
		.setStyle(new NotificationCompat.BigTextStyle().bigText(subTitle))
		.setContentText(subTitle);

		mBuilder.setSmallIcon(R.drawable.ic_status_edo);
		mBuilder.setLargeIcon(BitmapFactory.decodeResource(c.getResources(), R.drawable.ic_noty_edo));
		mBuilder.setAutoCancel(true);
		mBuilder.setProgress(100, 0, true);

		Intent notificationIntent = new Intent(c, cls);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		PendingIntent contentIntent = PendingIntent.getActivity(c, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(contentIntent);

		this.id = id;
	}

	public void buildFileTransferNotification(int id, LFile file, String text){
		Bitmap image = ThumbnailFileLoader.getBitmapFromDiskCache(c,file.getId());

		if(image==null){
			image = BitmapFactory.decodeResource(c.getResources(), ThumbnailManager.getIconRes(c, file));
		}

		mBuilder =
				new NotificationCompat.Builder(c)
		.setLargeIcon(image)
		.setContentTitle(file.getName())
		.setStyle(new NotificationCompat.BigTextStyle().bigText(text))
		.setContentText(text);

		mBuilder.setSmallIcon(R.drawable.ic_status_edo);

		mBuilder.setProgress(100, 0, true);

		mBuilder.setAutoCancel(true);

		Intent intent = EdoIntentManager.getFileIntent(c, file);
		if(intent!=null)
			mBuilder.setContentIntent(PendingIntent.getActivity(c, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));

		this.id = ID_TRANSFER;
	}

	public void showProgress(int progress, boolean indeterminate, String title, String text){
		mBuilder.setProgress(100, progress, indeterminate);

		if(JavaUtils.isNotEmpty(title))
			mBuilder.setContentTitle(title);

		if(JavaUtils.isNotEmpty(text)){
			mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(text))
			.setContentText(text);
		}

		if(progress!=100){
			mBuilder.setOngoing(true);
		}else{
			mBuilder.setAutoCancel(true);
			mBuilder.setOngoing(false);
			mBuilder.setProgress(0,0,false);
		}
		mNotificationManager.notify(id, mBuilder.build());
	}

	public void setFileIntent(LFile file){
		Intent intent = EdoIntentManager.getFileIntent(c, file);
		if(intent!=null)
			mBuilder.setContentIntent(PendingIntent.getActivity(c, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
	}


	public void showContactsNotification(LActivity a){
		if(a.isFromCurrentEdoUser(c))
			return;

		EdoPreferences pref = new EdoPreferences(c);

		LActivityDAO aDAO = new LActivityDAO(c);

		if(a==null){
			LActivity lastPush = aDAO.getLastContactOrGroupActivity();
			if(lastPush!=null){
				a = lastPush;
			}else{
				return;
			}
		}

		eLog.i("EdoActivityMan", "SHOW NOTIFICATION");

		String title = "";
		String message = "";
		String bigText = null;
		Bitmap image = null;
		Intent intent = null;

		boolean isScreenOn = ASI.isScreenOn(c);
		boolean showNotification = true;

		UnreadInfo unreadInfo = aDAO.getUnreadInfos();

		int infosCount = unreadInfo.getTotalCount();

		if(infosCount==1){

			EdoContainer container = a.getContainer();
			if(!container.isContactOrGroup())
				return;
			LContact contact = (LContact) container;
			if(isScreenOn){
//				if(a.isFileResource() && app.currentFile!=null && app.currentFile.getId().equals(a.getResourceId())){
//					showNotification = false;
//				}else if(a.isCommentEvent() && app.currentFile!=null && app.currentFile.getId().equals(a.getResourceId())){
//					showNotification = false;
//				}else if(a.isChatEvent() && app.currentChatContact!=null && app.currentChatContact.equals(contact)){
//					showNotification = false;
//				}
				if(app.getTimelineCurrentContainerId()!=null && a.getContainerId().equals(app.getTimelineCurrentContainerId()))
					showNotification = false;
			}

			if(showNotification){
				title = contact.getNameSurname();
				message = a.getLongDescription();
				bigText = message;
				image = ContactPhotoLoader.getBitmapFromDiskCache(c, contact.getId());
				intent = EdoIntentManager.getContactsIntent(c, contact);
			}
		}else if(infosCount>1){

			boolean thereAreFilesEvents = unreadInfo.getUnreadFilesCount()>0;
			boolean thereAreNotesEvents = unreadInfo.getUnreadNotesCount()>0;
			boolean thereAreChatEvents = unreadInfo.getUnreadChatMessageCount()>0;
			boolean thereAreGroupEvents = unreadInfo.getUnreadGroupCount()>0;

			ArrayList<String> names = aDAO.getContainerNamesOfUnreadActivities();



			if(names!=null && names.size()>0){

				if(names.size()==1){
					EdoContainer container = a.getContainer();
					if(!container.isContactOrGroup())
						return;
					LContact contact = (LContact) container;

					boolean onlyChatEvents = thereAreChatEvents && !thereAreFilesEvents && !thereAreNotesEvents && !thereAreGroupEvents;

					if(isScreenOn){
						if(onlyChatEvents
								&& app.getTimelineCurrentContainerId()!=null && app.getTimelineCurrentContainerId().equals(contact.getContainerId())){
							showNotification = false;
						}
					}

					if(showNotification){

						title = contact.getNameSurname();
						image = ContactPhotoLoader.getBitmapFromDiskCache(c, contact.getId());
//						if(onlyChatEvents){
//							intent = EdoIntentManager.getChatIntent(c, contact);
//						}else{
							intent = EdoIntentManager.getContactsIntent(c, contact);
//						}
					}
				}else if(names.size()>1){

					title = names.get(0);
					for(int i=1; i<names.size(); i++)
						title +=", "+names.get(i);

					image = BitmapFactory.decodeResource(c.getResources(), R.drawable.ic_group_nothumb);
					intent = EdoIntentManager.getContactsIntent(c, null);

				}



				if(showNotification){
					if(unreadInfo.getUnreadChatMessageCount()>0){
						message += c.getResources().getString(R.string.numNewMessages).replace(Constants.PH_NUM, ""+unreadInfo.getUnreadChatMessageCount());
					}

					if(unreadInfo.getUnreadNotesCount()>0){

						if(JavaUtils.isNotEmpty(message)){
							message += "<BR>";
						}

						message += c.getResources().getString(R.string.numNewNotes).replace(Constants.PH_NUM, ""+unreadInfo.getUnreadNotesCount());
					}

					if(unreadInfo.getUnreadFilesCount()>0){

						if(JavaUtils.isNotEmpty(message)){
							message += "<BR>";
						}

						message += c.getResources().getString(R.string.numNewEventOnFiles).replace(Constants.PH_NUM, ""+unreadInfo.getUnreadFilesCount());
					}

					if(unreadInfo.getUnreadGroupCount()>0){

						if(JavaUtils.isNotEmpty(message)){
							message += "<BR>";
						}

						message += c.getResources().getString(R.string.numNewEventOnGroups).replace(Constants.PH_NUM, ""+unreadInfo.getUnreadGroupCount());
					}


					bigText = aDAO.getContactContextUnreadBigTextHTML(10);
				}




			}else{
				return;
			}


		}else{
			return;
		}

		if(message==null)
			return;

		if(showNotification){
			boolean makeSound = System.currentTimeMillis()-app.lastNotificationWithSound > INTERVAL_BETWEEN_SOUNDS_NOTIFICATION;

			if(JavaUtils.isEmpty(bigText)){
				bigText = message;
			}

			mBuilder = new NotificationCompat.Builder(c)
			.setSmallIcon(R.drawable.ic_status_edo)
			.setContentTitle(title)
			.setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml(bigText)))
			.setContentText(Html.fromHtml(message))
			.setAutoCancel(true)
			.setLights(Color.CYAN, 1000, 2000);


			if(image!=null)
				mBuilder.setLargeIcon(image);
			else
				mBuilder.setLargeIcon(BitmapFactory.decodeResource(c.getResources(), R.drawable.ic_group_nothumb));

			if(intent!=null)
				mBuilder.setContentIntent(PendingIntent.getActivity(c, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));


			Notification notification = mBuilder.build();
			notification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
			if(pref.isNotySoundEnabled() && makeSound){
				notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
				app.lastNotificationWithSound = System.currentTimeMillis();
			}

			mNotificationManager.notify(ID_CONTACTS, notification);

		}else{

			EdoUI.vibrateOneShot(c);
		}
	}












	public void showNotification(){
		mNotificationManager.notify(id, mBuilder.build());
	}

	public void cancelNotification(){
		mNotificationManager.cancel(id);
	}


	public static void cancelNotification(Context c, int id){
		NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(id);

		if(id == ID_CONTACTS){
			ThisApp app = ThisApp.getInstance(c);
		}
	}


	public static void showQuickNotification(Context c, int id, String title, String message,
			Integer statusIconResource, Bitmap image, Intent intent){
		NotificationCompat.Builder mBuilder;

		EdoPreferences pref = new EdoPreferences(c);
		ThisApp app = ThisApp.getInstance(c);

		NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);

		if(intent!=null)
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

		mBuilder = new NotificationCompat.Builder(c)
		.setSmallIcon(statusIconResource != null ? statusIconResource : R.drawable.ic_status_edo)
		.setContentTitle(title)
		.setStyle(new NotificationCompat.BigTextStyle().bigText(message))
		.setContentText(message)
		.setAutoCancel(true)
		.setLights(Color.CYAN, 1000, 2000);


		if(image!=null)
			mBuilder.setLargeIcon(image);
		else
			mBuilder.setLargeIcon(BitmapFactory.decodeResource(c.getResources(), R.drawable.ic_noty_edo));

		if(intent!=null)
			mBuilder.setContentIntent(PendingIntent.getActivity(c, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));


		Notification notification = mBuilder.build();
		notification.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
		if(pref.isNotySoundEnabled()){
			notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
		}

		mNotificationManager.notify(id, notification);
	}










	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
