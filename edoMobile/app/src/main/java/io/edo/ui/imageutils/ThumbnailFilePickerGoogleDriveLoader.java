package io.edo.ui.imageutils;

import android.support.v4.app.FragmentActivity;

public class ThumbnailFilePickerGoogleDriveLoader extends ThumbnailFileLoader{

    public ThumbnailFilePickerGoogleDriveLoader(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }


    @Override
    public String getKeyFromData(Object data) {
        ImageData imageData = (ImageData) data;
        return imageData.file.getCloudId();
    }
}
