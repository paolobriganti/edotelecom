package io.edo.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import io.edo.edomobile.R;

public class EdoDialogListView extends EdoDialogOne{

	BaseAdapter adapter;
	ListView lv;

	OnListItemClickListener itemClickListener;
	OnListItemLongClickListener itemLongClickListener;

	public EdoDialogListView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub


	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_edo_listview);

		EdoUI.setDialogWindowParams(this);
		
		tv_title = (TextView) this.findViewById(R.id.tv_title);
		tv_text = (TextView) this.findViewById(R.id.tv_text);
		lv = (ListView)  this.findViewById(R.id.lv);
		bt_positive = (Button) this.findViewById(R.id.bt_positive);

		setTitle(title);
		setTitle(titleRes);
		setText(text);
		setText(textRes);
		setPositiveButtonText(btPositiveText);
		setPositiveButtonText(btPositiveTextRes);
		setOnPositiveClickListener(positiveListener);
		setAdapter (adapter);
		setOnListItemClickListener(itemClickListener);
		setOnListItemLongClickListener(itemLongClickListener);
	}


	public void setAdapter (BaseAdapter adapter){
		if(adapter!=null){ 
			this.adapter = adapter;
			if(lv!=null){
				lv.setAdapter(adapter);
				EdoUI.setListViewLoadItemsAlphaAnimationQuick(lv);
			}
		}
	}

	public ListView getListView (){
		return lv;
	}

	public void setOnListItemClickListener(OnListItemClickListener listener) {
		if(listener!=null){
			this.itemClickListener = listener;
			if(lv!=null)
				lv.setOnItemClickListener(listener);
		}
	}
	public interface OnListItemClickListener extends OnItemClickListener{
	}

	public void setOnListItemLongClickListener(OnListItemLongClickListener listener) {
		if(listener!=null){
			this.itemLongClickListener = listener;
			if(lv!=null)
				lv.setOnItemLongClickListener(listener);
		}
	}
	public interface OnListItemLongClickListener extends OnItemLongClickListener{
	}
}
