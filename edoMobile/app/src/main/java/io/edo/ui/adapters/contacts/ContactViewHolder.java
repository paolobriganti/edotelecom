package io.edo.ui.adapters.contacts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.paolobriganti.android.VU;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LContact;
import io.edo.edomobile.R;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 07/07/15.
 */
public class ContactViewHolder extends RecyclerView.ViewHolder {
    View contactLayout;
    TextView tv_letter;
    TextView tv_title;
    TextView tv_subtitle;
    ImageView contact_photo;
    TextView tv_activities;
//    Button bt_edocontact;
    ImageView iv_rightIcon;

    public ContactViewHolder(View itemView) {
        super(itemView);
        contactLayout = itemView;
        tv_letter = (TextView) itemView.findViewById(R.id.tv_letter);
        tv_title = (TextView) itemView.findViewById(R.id.tv_title);
        tv_subtitle = (TextView) itemView.findViewById(R.id.tv_sub_title);
        contact_photo = (ImageView) itemView.findViewById(R.id.contact_photo);
        tv_activities = (TextView) itemView.findViewById(R.id.tv_activities);
//        bt_edocontact = (Button) itemView.findViewById(R.id.bt_edocontact);
        iv_rightIcon = (ImageView) itemView.findViewById(R.id.iv_rightIcon);


    }


    public void setContactViews(Context c,
                                final LContact contact,
                                final ContactPhotoLoader contactPhotoLoader,
                                final String edoUserId,
                                final String groupOwnerId,
                                final Integer rightIcon,
                                final String letter,
                                String searchText){

        String contactName = contact.getNameSurname();

        String emailAddress = contact.getPrincipalEmailAddress();

        if(JavaUtils.isNotEmpty(contactName)){
            contactName = VU.replaceBlankSpaces(contactName);
            tv_title.setText(contactName);
        }else if(JavaUtils.isNotEmpty(emailAddress)){
            tv_title.setText(emailAddress);
        }else{
            tv_title.setText(R.string.contact);
        }

        if(JavaUtils.isNotEmpty(searchText) && JavaUtils.isNotEmpty(tv_title.getText().toString())){
            String newName = tv_title.getText().toString();
            if(newName.toLowerCase().contains(searchText.toLowerCase())){
                tv_title.setText(VU.highlightText(newName, searchText, c.getResources().getColor(R.color.edo_blue_dark)));
            }
        }

//        if(!contact.isGroup() && !contact.isEdoUser()) {
//            tv_subtitle.setVisibility(View.VISIBLE);
//            tv_subtitle.setText(R.string.notOnEdo);
//        }else

        if(JavaUtils.isNotEmpty(contact.getLastNotificationMessage())){
            tv_subtitle.setText(Html.fromHtml(contact.getLastNotificationMessage()));
        }else if(!contact.isGroup()){
            tv_subtitle.setVisibility(View.VISIBLE);
            if(JavaUtils.isNotEmpty(contactName)){
                tv_subtitle.setText(emailAddress);
            }else{
                tv_subtitle.setText(null);
            }
        }else{
            if(contact.getGroupMembersCount()!=0){
                String ownerString = contact.isUserOwnerOfTheGroup(edoUserId)?
                        (" ("+c.getResources().getString(R.string.owner)+")"):"";
                String subTitle = c.getResources().getString(R.string.groupNUMMembers)
                        .replace(Constants.PH_ISOWNER, ownerString)
                        .replace(Constants.PH_NUM, ""+contact.getGroupMembersCount());

                tv_subtitle.setVisibility(View.VISIBLE);
                tv_subtitle.setText(subTitle);

            }else{
                tv_subtitle.setVisibility(View.GONE);
                tv_subtitle.setText(null);
            }
        }

        if(JavaUtils.isNotEmpty(searchText) && JavaUtils.isNotEmpty(tv_subtitle.getText().toString())){
            String newName = tv_subtitle.getText().toString();
            if(newName.toLowerCase().contains(searchText.toLowerCase())){
                tv_subtitle.setText(VU.highlightText(newName, searchText, c.getResources().getColor(R.color.edo_blue_dark)));
            }
        }

        contactPhotoLoader.loadImage(contact, contact_photo);

        Integer count = contact.getUnreadPushCount();
        if(count!=null && count>0){
            String countString = count>9? "9+":""+count;
            tv_activities.setText(countString);
            tv_activities.setVisibility(TextView.VISIBLE);

            tv_title.setTypeface(Typeface.DEFAULT_BOLD);
        }else {
            tv_title.setTypeface(Typeface.DEFAULT);
            tv_activities.setVisibility(TextView.GONE);
        }

        if(rightIcon!=null){
            iv_rightIcon.setVisibility(View.VISIBLE);
            if(rightIcon==R.drawable.group_owner){
                if(contact.isEdoUser()){
                    if(groupOwnerId!=null && groupOwnerId.equals(contact.getEdoUserId())){
                        iv_rightIcon.setImageResource(rightIcon);
                    }else{
                        iv_rightIcon.setImageResource(R.drawable.ic_action_accept);
                    }
                }else{
                    iv_rightIcon.setImageResource(R.drawable.ic_action_time);
                }
            }else{
                iv_rightIcon.setImageResource(rightIcon);
            }
        }


        if(tv_letter!=null){
            if(contact.isPersonal(c)){
                tv_letter.setText(R.string.you);
            }else{
                tv_letter.setText(letter);
            }

        }


        contactLayout.setTag(contact);



    }


}
