package io.edo.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.edo.edomobile.R;
import io.edo.ui.adapters.ListAdapterEdoRadioButton;

public class EdoDialogRadioButton extends EdoDialog{

	ArrayList<String> rbTitles = new ArrayList<String>();
	ArrayList<Integer> images = new ArrayList<Integer>();
	Integer checkPosition = null;
	ListView lv;

	public EdoDialogRadioButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_edo_listview);


		EdoUI.setDialogWindowParams(this);
		
		
		tv_title = (TextView) this.findViewById(R.id.tv_title);
		tv_text = (TextView) this.findViewById(R.id.tv_text);
		lv = (ListView)  this.findViewById(R.id.lv);
		bt_positive = (Button) this.findViewById(R.id.bt_positive);
		bt_negative = (Button) this.findViewById(R.id.bt_negative);

		setTitle(title);
		setTitle(titleRes);
		setText(text);
		setText(textRes);
		setPositiveButtonText(btPositiveText);
		setPositiveButtonText(btPositiveTextRes);
		setNegativeButtonText(btNegativeText);
		setNegativeButtonText(btNegativeTextRes);
		setOnPositiveClickListener(positiveListener);
		setOnNegativeClickListener(negativeListener);
		setRBTitles (rbTitles, images, checkPosition);
	}


	public void setRBTitles (final ArrayList<String> rbTitles, final ArrayList<Integer> rbImages, Integer checkPosition){
		if(rbTitles!=null && rbTitles.size()>0){
			this.rbTitles = rbTitles;
			this.images = rbImages;
			this.checkPosition = checkPosition;
			if(rbTitles!=null && lv!=null){
				ListAdapterEdoRadioButton adapter = new ListAdapterEdoRadioButton(getContext(), rbTitles, images, checkPosition);
				lv.setAdapter(adapter);
			}
				
		}
	}

	public boolean isChecked(int position){
		return ((ListAdapterEdoRadioButton)lv.getAdapter()).isChecked(position);
	}
	public Integer getCheckedPosition(){
		return ((ListAdapterEdoRadioButton)lv.getAdapter()).getChecked();
	}
	public void setChecked(int position, boolean checked){
		((ListAdapterEdoRadioButton)lv.getAdapter()).setChecked(position);
	}
}
