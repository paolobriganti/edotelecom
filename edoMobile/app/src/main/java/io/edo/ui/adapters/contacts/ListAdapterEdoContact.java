package io.edo.ui.adapters.contacts;


import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.paolobriganti.android.VU;
import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.local.beans.LContact;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.utilities.Constants;

public class ListAdapterEdoContact extends BaseAdapter{
	private Context context;
	private String edoUserId;
	private ContactPhotoLoader contactPhotoLoader;
	private ArrayList<LContact> contacts;
	private Integer rightIcon = null;
	private String groupOwnerId = null;

	private int activedPosition = ListView.INVALID_POSITION;

	public ListAdapterEdoContact(Context context,
								 ArrayList<LContact> contacts,
								 ContactPhotoLoader contactPhotoLoader,
								 int activedPosition,
								 Integer rightIcon,
								 String groupOwnerId) {
		this.context = context;
		this.edoUserId = ThisApp.getInstance(context).getEdoUserId();
		this.contactPhotoLoader = contactPhotoLoader;
		this.contacts = contacts;
		this.activedPosition = activedPosition;
		this.rightIcon = rightIcon;
		this.groupOwnerId = groupOwnerId;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.row_edo_contact_left_image, parent, false);

		rowView.setId(position);

		LContact contact = null;
		if(contacts!=null && contacts.size()>position)
			contact = contacts.get(position);

		if(contact!=null){



			TextView tv_title = (TextView) rowView.findViewById(R.id.tv_title);
			String contactName = contact.getNameSurname();

			String emailAddress = contact.getPrincipalEmailAddress();

			if(JavaUtils.isNotEmpty(contactName)){
				contactName = VU.replaceBlankSpaces(contactName);
				tv_title.setText(contactName);
			}else if(JavaUtils.isNotEmpty(emailAddress)){
				tv_title.setText(emailAddress);
			}else{
				tv_title.setText(R.string.contact);
			}


			TextView tv_subtitle = (TextView) rowView.findViewById(R.id.tv_sub_title);
			if(!contact.isGroup() && !contact.isEdoUser()) {
				tv_subtitle.setVisibility(View.VISIBLE);
				tv_subtitle.setText(R.string.notOnEdo);
			}else if(JavaUtils.isNotEmpty(contact.getLastNotificationMessage())){
				tv_subtitle.setVisibility(View.VISIBLE);
				tv_subtitle.setText(Html.fromHtml(contact.getLastNotificationMessage()));
			}else if(!contact.isGroup()){
				tv_subtitle.setVisibility(View.VISIBLE);
				if(JavaUtils.isNotEmpty(contactName)){
					tv_subtitle.setText(emailAddress);
				}else{
					tv_subtitle.setText(null);
				}
			}else{
				if(contact.getGroupMembersCount()!=0){
					String ownerString = contact.isUserOwnerOfTheGroup(edoUserId)?
							(" ("+context.getResources().getString(R.string.owner)+")"):"";
					String subTitle = context.getResources().getString(R.string.groupNUMMembers)
							.replace(Constants.PH_ISOWNER, ownerString)
							.replace(Constants.PH_NUM, ""+contact.getGroupMembersCount());

					tv_subtitle.setVisibility(View.VISIBLE);
					tv_subtitle.setText(subTitle);

				}else{
					tv_subtitle.setVisibility(View.GONE);
					tv_subtitle.setText(null);
				}
			}

			ImageView contact_photo = (ImageView) rowView.findViewById(R.id.contact_photo);
			contactPhotoLoader.loadImage(contact, contact_photo);



			TextView tv_activities = (TextView) rowView.findViewById(R.id.tv_activities);
			Integer count = contact.getUnreadPushCount();
			if(count!=null && count>0){
				String countString = count>9? "9+":""+count;
				tv_activities.setText(countString);
				tv_activities.setVisibility(TextView.VISIBLE);

				tv_title.setTypeface(Typeface.DEFAULT_BOLD);
			}else {
				tv_title.setTypeface(Typeface.DEFAULT);
				tv_activities.setVisibility(TextView.GONE);
			}

//			Button bt_edocontact = (Button) rowView.findViewById(R.id.bt_edocontact);
//			bt_edocontact.setText(null);
//			if(!contact.isGroup()){
//				if(contact.isEdoUser()){
//					bt_edocontact.setBackgroundResource(R.drawable.ripple_blue);
//					bt_edocontact.setVisibility(ImageView.VISIBLE);
//				}else{
//					bt_edocontact.setVisibility(ImageView.GONE);
//				}
//			}else{
//				bt_edocontact.setBackgroundResource(R.drawable.bg_circle_blue);
//				bt_edocontact.setVisibility(ImageView.VISIBLE);
//			}


			ImageView iv_rightIcon = (ImageView) rowView.findViewById(R.id.iv_rightIcon);
			if(rightIcon!=null){
				iv_rightIcon.setVisibility(View.VISIBLE);
				if(rightIcon==R.drawable.group_owner){
					if(contact.isEdoUser()){
						if(groupOwnerId!=null && groupOwnerId.equals(contact.getEdoUserId())){
							iv_rightIcon.setImageResource(rightIcon);
						}else{
							iv_rightIcon.setImageResource(R.drawable.ic_action_accept);
						}
					}else{
						iv_rightIcon.setImageResource(R.drawable.ic_action_time);
					}
				}else{
					iv_rightIcon.setImageResource(rightIcon);
				}
			}

//			LinearLayout ll_background = (LinearLayout) rowView.findViewById(R.id.lv_contacts_row);
			if(activedPosition == position){
				rowView.setBackgroundResource(R.color.edo_blue);
				tv_title.setTextColor(context.getResources().getColor(R.color.white));
				tv_subtitle.setTextColor(context.getResources().getColor(R.color.edo_gray_very_light));
			}else{
				rowView.setBackgroundResource(R.drawable.bg_card_gray_extra_light);
				tv_title.setTextColor(context.getResources().getColor(R.color.edo_black));
				tv_subtitle.setTextColor(context.getResources().getColor(R.color.edo_gray_dark));
			}


			rowView.setTag(contact);
		}

		return rowView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return contacts.size();
	}

	@Override
	public Object getItem(int position) {
		return contacts.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void notifyDataSetChanged(int activedPosition) {
		this.activedPosition = activedPosition;

		super.notifyDataSetChanged();
	}
}