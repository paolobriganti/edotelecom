package io.edo.ui;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LFile;
import io.edo.edomobile.R;
import io.edo.views.EdoProgressBar;

public class EdoProgressDialog extends Dialog{

	private Activity activity;
	private TextView tv_title;
	private TextView tv_subtitle;
	private EdoProgressBar progressBar;
	private ImageView iv_spinner;
	private ProgressBar pb_spinner;
	private ImageView iv_preview;
	private String title;
	private String subTitle;
	private String originalTitle;
	private String originalSubTitle;

	private boolean isOnPause = false;

	public EdoProgressDialog(Activity activity, String title, String subTitle) {
		super(activity);
		this.activity = activity;
		this.originalTitle = title;
		this.originalSubTitle = subTitle;
		this.title = title;
		this.subTitle = subTitle;

	}

	public EdoProgressDialog(Activity activity, int titleResId, int subTitleResId) {
		super(activity);
		this.activity = activity;
		this.originalTitle = activity.getResources().getString(titleResId);
		this.originalSubTitle = activity.getResources().getString(subTitleResId);
		this.title = activity.getResources().getString(titleResId);
		this.subTitle = activity.getResources().getString(subTitleResId);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setCancelable(false);

		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.edo_progress_dialog);

		WindowManager.LayoutParams params = getWindow().getAttributes();  
		params.width = (int)(Math.min(getContext().getResources().getDisplayMetrics().widthPixels, getContext().getResources().getDisplayMetrics().heightPixels)*0.85F);
		this.getWindow().setAttributes(params); 

		tv_title  = (TextView)findViewById(R.id.title);
		tv_subtitle  = (TextView)findViewById(R.id.subtitle);
		progressBar = (EdoProgressBar) findViewById(R.id.progressBar);
		pb_spinner = (ProgressBar) findViewById(R.id.pb_spinner);
		iv_spinner = (ImageView) findViewById(R.id.iv_spinner);
		iv_preview = (ImageView) findViewById(R.id.iv_thumb);


		if(android.os.Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
			iv_spinner.setVisibility(View.GONE);
		}else{
			pb_spinner.setVisibility(View.GONE);
		}

		final FrameLayout fl_image = (FrameLayout) findViewById(R.id.fl_image);
		
		setTitle(title);
		setSubTitle(subTitle);
		
		iv_preview.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			
			@Override
			public void onGlobalLayout() {
				if(fl_image.getWidth()>0 && fl_image.getHeight()>0){
					FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(fl_image.getWidth() , fl_image.getHeight());
					iv_preview.setLayoutParams(params);
					iv_preview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				}
			}
		});
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		super.show();
		startSpin();
		
	}


	public void pause(){
		boolean wasShowing = isShowing();
		this.dismiss();
		if(wasShowing)
			isOnPause = true;
	}

	@Override
	public void dismiss() {
		try {
			// TODO Auto-generated method stub
			super.dismiss();
			isOnPause = false;
			stopSpin();
			setTitle(originalTitle);
			setSubTitle(originalSubTitle);
		}catch (Exception e){}
	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		super.cancel();
		isOnPause = false;
		stopSpin();
		setTitle(originalTitle);
		setSubTitle(originalSubTitle);
	}


	public void resume(){
		if(isOnPause){
			this.show();
			isOnPause = false;
		}
	}



	public void setTitle(final int resId){
		title = getContext().getResources().getString(resId);
		activity.runOnUiThread(new Runnable() {public void run() {
			if(tv_title!=null && JavaUtils.isNotEmpty(title))
				tv_title.setText(title);
		}});


	}

	public void setTitle(final String text){
		title = text;
		activity.runOnUiThread(new Runnable() {public void run() {
			if(tv_title!=null && JavaUtils.isNotEmpty(text))
				tv_title.setText(text);
		}});

	}

	public void setSubTitle(final int resId){
		subTitle = getContext().getResources().getString(resId);
		activity.runOnUiThread(new Runnable() {public void run() {
			if(tv_subtitle!=null && JavaUtils.isNotEmpty(subTitle))
				tv_subtitle.setText(subTitle);
		}});
	}

	public void setSubTitle(final String text){
		subTitle = text;
		activity.runOnUiThread(new Runnable() {public void run() {
			if(tv_subtitle!=null && JavaUtils.isNotEmpty(text))
				tv_subtitle.setText(text);
		}});

	}

	public void setThumb(final BitmapDrawable drawableThumb){
		if(drawableThumb!=null)
			activity.runOnUiThread(new Runnable() {public void run() {
				if(iv_preview!=null)
					iv_preview.setImageDrawable(drawableThumb);
			}});
	}

	public void setLFile(LFile file, BitmapDrawable thumb){
		setSubTitle(file.getName());
		setThumb(thumb);
	}
	
	
	
	
	
	public void startSpin(){
		if(iv_spinner!=null){
			setThumb(null);
			activity.runOnUiThread(new Runnable() {public void run() {
				Animation anim = AnimationUtils.loadAnimation(activity, R.anim.rotate);
				iv_spinner.setAnimation(anim);
			}});
		}
	}

	public void stopSpin(){
		if(iv_spinner!=null){
			setThumb(null);
			activity.runOnUiThread(new Runnable() {public void run() {
				iv_spinner.clearAnimation();
				iv_spinner.setAnimation(null);
			}});
		}
	}







	// ** PROGRESS PERCENT ***

	public int getPercent(final int current, final int total, final int from, final int to){
		int max = to-from;
		float progress = ((float)current/(float)total)*max;
		return (int) progress + from;
	}


	public int getPercent(final int current, final int total){
		float progress = ((float)current/(float)total)*100;
		return (int) progress;
	}

	// ** PROGRESS BAR ***

	public int setBarProgress(final int current, final int total, final int from, final int to){
		int percent = getPercent(current, total, from, to);
		setBarProgress(percent);
		return percent;
	}

	public int setBarProgress(final int current, final int total){
		int percent = getPercent(current, total);
		setBarProgress(percent);
		return percent;
	}
	public void setBarProgress(final int percent){
		activity.runOnUiThread(new Runnable() {public void run() {
			if(progressBar!=null){
				progressBar.setVisibility(ProgressBar.VISIBLE);
				progressBar.setProgress(percent);
			}
		}});
	}

	public ProgressViews getProgressViews(){
		return new ProgressViews(activity, progressBar, tv_title, tv_subtitle);
	}



}
