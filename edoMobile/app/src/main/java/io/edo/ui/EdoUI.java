package io.edo.ui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Looper;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.paolobriganti.android.ASI;

import java.lang.reflect.Field;
import java.util.ArrayList;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LGroupMemberDAO;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.edomobile.Act_00_SplashScreen;
import io.edo.edomobile.Act_GroupEditor;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoDialog.OnNegativeClickListener;
import io.edo.ui.EdoDialogOne.OnPositiveClickListener;
import io.edo.ui.adapters.contacts.ListAdapterEdoContact;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.utilities.Constants;
import io.edo.views.ListViewPlus;


@SuppressLint("ShowToast")
public class EdoUI {
	Context context;

	Toast shortToast, longToast;

	public static final String FONT_NAME_ARCHITECT_DAUGHTER = "ArchitectsDaughter.ttf";
	public static final String FONT_NAME_QUICKSAND_BOOK = "Quicksand_Book.otf";


	public EdoUI (Context c){
		context = c;
		shortToast = new Toast(c);
		int topOffset = c.getResources().getDisplayMetrics().densityDpi/10;
		shortToast.setGravity(Gravity.BOTTOM|Gravity.LEFT, topOffset, topOffset);
		longToast = new Toast(c);
		longToast.setGravity(Gravity.BOTTOM|Gravity.LEFT, topOffset, topOffset);
	}

	//Toast Message
	public void showToastShort(String message, Bitmap image){
		View layout = getEdoToastLayout(context, message, image);
		shortToast.setDuration(Toast.LENGTH_SHORT);
		shortToast.setView(layout);
		shortToast.show();
	}
	public void showToastLong(String message, Bitmap image){
		View layout = getEdoToastLayout(context, message, image);
		shortToast.setDuration(Toast.LENGTH_LONG);
		shortToast.setView(layout);
		shortToast.show();
	}

	public void showToastShort(int textRes, Bitmap image){
		showToastShort(context.getResources().getString(textRes), image);
	}
	public void showToastLong(int textRes, Bitmap image){
		showToastLong(context.getResources().getString(textRes), image);
	}

	public static void showToast(Context context, CharSequence text, Bitmap image, int duration) {
		View layout = getEdoToastLayout(context, text, image);

		Toast toast = new Toast(context);
		int topOffset = context.getResources().getDisplayMetrics().densityDpi/10;
		toast.setGravity(Gravity.BOTTOM|Gravity.LEFT, topOffset, topOffset);
		toast.setDuration(duration);
		toast.setView(layout);
		toast.show();
	}

	public static void showToast(Context context, int textRes, Bitmap image, int duration) {
		showToast(context, context.getResources().getString(textRes), image, duration);
	}


	public static View getEdoToastLayout(Context context, CharSequence text, Bitmap image){
		LayoutInflater inflater = LayoutInflater.from(context);
		View layout = inflater.inflate(R.layout.toast_layout, null);

		TextView tv = (TextView) layout.findViewById(R.id.text);
		tv.setText(text);

		ImageView iv = (ImageView) layout.findViewById(R.id.image);
		if(image!=null){
			iv.setVisibility(View.VISIBLE);
			iv.setImageBitmap(image);
		}else{
			iv.setVisibility(View.GONE);
		}


		return layout;
	}


	//	public static AnimationDrawable setProgressSpinner(Context c, ImageView iv){
	//		AnimationDrawable progressAnimation;
	//		iv.setBackgroundResource(R.anim.edo_spinner_animation);
	//		progressAnimation = (AnimationDrawable) iv.getBackground();
	//		progressAnimation.start();
	//		return progressAnimation;
	//	}
	//
//	public static AnimationDrawable setProgressSpinnerLarge(Context c, ImageView iv, boolean oneShot){
//		AnimationDrawable progressAnimation;
//		iv.setBackgroundResource(R.anim.progress_animation);
//		progressAnimation = (AnimationDrawable) iv.getBackground();
//		progressAnimation.start();
//		progressAnimation.setOneShot(oneShot);
//		return progressAnimation;
//	}
	//
	//	public static ImageView setEmergeAnimation(Context c, ImageView iv, boolean oneShot){
	//		AnimationDrawable progressAnimation;
	//		iv.setBackgroundResource(R.anim.edo_emerge_animation);
	//		progressAnimation = (AnimationDrawable) iv.getBackground();
	//		progressAnimation.start();
	//		progressAnimation.setOneShot(oneShot);
	//		return iv;
	//	}

	public static void setListViewLoadItemsAlphaAnimation(final ListView lv){
		setListViewLoadItemsAlphaAnimation(lv,450);
	}

	public static void setListViewLoadItemsAlphaAnimation(final ListView lv, long duration){
		AnimationSet set = new AnimationSet(true);

		Animation animation = new AlphaAnimation(0.0f, 1.0f);
		animation.setDuration(duration);
		set.addAnimation(animation);

		animation = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, -1.0f,Animation.RELATIVE_TO_SELF, 0.0f
				);
		animation.setDuration(duration/2);
		set.addAnimation(animation);

		LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
		lv.setLayoutAnimation(controller);
		lv.setVisibility(ListView.VISIBLE);
	}

	public static void setListViewLoadItemsAlphaAnimationQuick(final ListView lv){
		setListViewLoadItemsAlphaAnimation(lv, 250);
	}


	public static void setGridViewLoadItemsAlphaAnimation(final GridView gv){
		AnimationSet set = new AnimationSet(true);

		Animation animation = new AlphaAnimation(0.0f, 1.0f);
		animation.setDuration(350);
		set.addAnimation(animation);

		animation = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
				Animation.RELATIVE_TO_SELF, -1.0f,Animation.RELATIVE_TO_SELF, 0.0f
				);
		animation.setDuration(100);
		set.addAnimation(animation);


		LayoutAnimationController controller = new LayoutAnimationController(set,0.2f);
		gv.setLayoutAnimation(controller);
		gv.setVisibility(ListView.VISIBLE);
	}


	public static AnimationSet setGridViewLoadItemsZoomInAnimation(final GridView gv){
		AnimationSet set = new AnimationSet(true);

		Animation animation = new AlphaAnimation(0.0f, 1.0f);
		animation.setDuration(250);
		set.addAnimation(animation);
		animation.setDuration(100);
		set.addAnimation(animation);

		LayoutAnimationController controller = new LayoutAnimationController(set,0.2f);
		gv.setLayoutAnimation(controller);
		gv.setVisibility(ListView.VISIBLE);

		return set;
	}


	public static void oneButtonDialog (Context c, String title, String text){
		final EdoDialogOne dialog = new EdoDialogOne(c);
		dialog.setTitle(title);
		dialog.setText(text);
		dialog.setOnPositiveClickListener(new OnPositiveClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	public static void oneButtonDialog (Activity c, int titleRes, int textRes){
		final EdoDialogOne dialog = new EdoDialogOne(c);
		dialog.setTitle(titleRes);
		dialog.setText(textRes);
		dialog.setOnPositiveClickListener(new OnPositiveClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		c.runOnUiThread(new Runnable() {
			public void run() {
				dialog.show();
			}
		});

	}





	public static void getGroupInfoDialog(final FragmentActivity c, final ContactPhotoLoader contactPhotoLoader, final LContact cgroup){
		if(cgroup!=null){
			final ThisApp app = ThisApp.getInstance(c);

			final Dialog dialog = new Dialog(c);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.layout_group_info);

			TextView text = (TextView) dialog.findViewById(R.id.tv_name);
			text.setText(cgroup.getName());

			ImageView image = (ImageView) dialog.findViewById(R.id.iv_group_icon);
			ContactPhotoLoader.getInstance(c).loadImage(cgroup, image);

			Button bt_cancel = (Button) dialog.findViewById(R.id.bt_cancel);
			bt_cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			Button bt_modifyGroup = (Button) dialog.findViewById(R.id.bt_modifyGroup);
			bt_modifyGroup.setVisibility(cgroup.isUserOwnerOfTheGroup(app.getEdoUserId())?View.VISIBLE:View.GONE);
			bt_modifyGroup.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Runnable modifyGroup = new Runnable() {

						@Override
						public void run() {
							Intent editGroup = new Intent(c, Act_GroupEditor.class);
							editGroup.putExtra(DBConstants.CONTAINER, cgroup);
							c.startActivityForResult(editGroup, Constants.CONTACT_EDITOR_RESULT);
							dialog.dismiss();
						}
					};

					if (!EdoUI.dialogNoInternet(c, modifyGroup, null)) {
						modifyGroup.run();
					}
				}
			});

			final ListViewPlus lv_members = (ListViewPlus) dialog.findViewById(R.id.lv);
			new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
			final ArrayList<LContact> members = new LGroupMemberDAO(c).getContactsOfGroup(cgroup.getId());
			if(members!=null){
				c.runOnUiThread(new Runnable() {public void run() {
					ListAdapterEdoContact adapter = new ListAdapterEdoContact(c, members, contactPhotoLoader, ListView.INVALID_POSITION, R.drawable.group_owner, cgroup.getGroupOwnerId());
					lv_members.setAdapter(adapter);
				}});
			}
			}}.start();



			dialog.show();
		}
	}







	public static void setFontToTextView(Context c, TextView textView, String fontName){
		Typeface tf = Typeface.createFromAsset(c.getAssets(), "fonts/"+fontName);
		textView.setTypeface(tf);
	}



	public static void closeAllActivities(Context c){
		Intent intent = new Intent(Constants.ACTION_ACTIVITY_FINISH);
		c.sendBroadcast(intent);
	}

	public static void resetApplicationDataAndRestart(Context c){
		ThisApp app = ThisApp.getInstance(c);
		
		app.setUser(null);
		
		ASI.clearApplicationData(c);
		
		if(app.getDB()!=null){
			app.getDB().close();
		}
		
		app.startDB();
		
		closeAllActivities(c);

		if(ASI.isThisAppForground(c)) {
			Intent splash = new Intent(c, Act_00_SplashScreen.class);
			splash.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(splash);
		}
	}







	public static void vibrateOneShot (Context c){
		Vibrator v = (Vibrator) c.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(50);
	}







	public static boolean dialogNoInternet(final Activity c, final Runnable positiveAction, final Runnable negativeAction){
		if(!ASI.isInternetOn(c)){
			c.runOnUiThread(new Runnable() {public void run() {
				final EdoDialog dialog = new EdoDialog(c);
				dialog.setCancelable(false);
				dialog.setTitle(R.string.noInternetConnection);
				dialog.setText(R.string.unableToCompleteTheOperationCheckInternetRetry);
				dialog.setPositiveButtonText(R.string.retry);
				dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

					@Override
					public void onClick(View arg0) {
						c.runOnUiThread(new Runnable() {public void run() {
							dialog.dismiss();
						}});

						if(ASI.isInternetOn(c)){
							if(positiveAction!=null)
								c.runOnUiThread(positiveAction);
						}else{
							dialogNoInternet(c, positiveAction, negativeAction);
						}
					}
				});
				dialog.setOnNegativeClickListener(new OnNegativeClickListener() {

					@Override
					public void onClick(View v) {
						c.runOnUiThread(new Runnable() {public void run() {
							dialog.dismiss();
						}});

						if(negativeAction!=null)
							c.runOnUiThread(negativeAction);
					}
				});

				dialog.show();
			}});

			return true;
		}
		return false;
	}



	public static void setDialogWindowParams(Dialog dialog){

		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.getWindow().clearFlags(LayoutParams.FLAG_DIM_BEHIND);
		dialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		dialog.getWindow().clearFlags(LayoutParams.FLAG_NOT_FOCUSABLE| LayoutParams.FLAG_ALT_FOCUSABLE_IM);

		LayoutParams params = dialog.getWindow().getAttributes();
		params.width = dialog.getContext().getResources().getDisplayMetrics().widthPixels;
		params.height = dialog.getContext().getResources().getDisplayMetrics().heightPixels;
		dialog.getWindow().setAttributes(params); 
	}


	public static int getDistanceToTrigger(Context c){
		return (int)((float)c.getResources().getDisplayMetrics().heightPixels/3.5F);
	}


	public static boolean setDistanceToTrigger(Context c, SwipeRefreshLayout swipeLayout){
		int mDistanceToTriggerSync = getDistanceToTrigger(c);

		if(ASI.get_OS_SDK_Version()>=ASI.ANDROID_5_0_SDK_VERSION){
			setDistanceToTriggerLollipop(swipeLayout, mDistanceToTriggerSync);
			return true;
		}else{
			try {
				// Set the internal trigger distance using reflection.
				Field field = SwipeRefreshLayout.class.getDeclaredField("mDistanceToTriggerSync");
				field.setAccessible(true);
				field.setFloat(swipeLayout, mDistanceToTriggerSync);
				return true;
			} catch (Exception e) {
			}
		}


		return false;
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public static void setDistanceToTriggerLollipop(SwipeRefreshLayout swipeLayout, int mDistanceToTriggerSync){
		swipeLayout.setDistanceToTriggerSync(mDistanceToTriggerSync);
	}

	public static boolean isTablet(Context c){
		return c.getResources().getBoolean(R.bool.isTablet);
	}

	public static void setOrientationTo(Activity activity){

		if (isTablet(activity)) {
			int currentOrientation = activity.getResources().getConfiguration().orientation;
			if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE)
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
			else
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}else{
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
	}

	
	
	public static void setStateListAnimatorRaise(Context context, View rowView){
		if(ASI.get_OS_SDK_Version()>=ASI.ANDROID_5_0_SDK_VERSION){
//			VU.setStateListAnimator(context, rowView, R.anim.raise);
		}
	}


	public static int getSpacesColumnsNumber(Context c){
		if(isTablet(c) && ASI.getScreenOrientation(c)==ASI.SCREEN_ORIENTATION_LANDSCAPE){
			return 3;
		}
		return 3;
	}

	public static int getSpacesMinColumnsNumber(Context c){
		if(isTablet(c) && ASI.getScreenOrientation(c)==ASI.SCREEN_ORIENTATION_LANDSCAPE){
			return 2;
		}
		return 1;
	}

	public static int getFilesColumnsNumber(Context c){
		if(isTablet(c) && ASI.getScreenOrientation(c)==ASI.SCREEN_ORIENTATION_LANDSCAPE){
			return 5;
		}
		return 3;
	}
	
}
