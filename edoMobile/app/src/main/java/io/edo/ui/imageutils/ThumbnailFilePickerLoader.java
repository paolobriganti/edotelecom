package io.edo.ui.imageutils;

import android.support.v4.app.FragmentActivity;

public class ThumbnailFilePickerLoader extends ThumbnailFileLoader{

    public ThumbnailFilePickerLoader(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }



    @Override
    public String getKeyFromData(Object data) {
        ImageData imageData = (ImageData) data;
        return imageData.file.getLocalUri();
    }
}
