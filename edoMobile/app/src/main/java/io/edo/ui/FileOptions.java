package io.edo.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;

import io.edo.api.ApiFile;
import io.edo.api.ApiResponse;
import io.edo.db.global.EdoActivityManager;
import io.edo.db.global.uibeans.ContactFileUIDAO;
import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LocalFiles;
import io.edo.edomobile.Act_Forward_Organize;
import io.edo.edomobile.Act_Forward_Share;
import io.edo.edomobile.Act_SpaceEditor;
import io.edo.edomobile.R;
import io.edo.edomobile.Service_DwnUpl;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoDialog.OnNegativeClickListener;
import io.edo.ui.EdoDialogListView.OnListItemClickListener;
import io.edo.ui.EdoDialogOne.OnPositiveClickListener;
import io.edo.ui.adapters.ListAdapterOptions;
import io.edo.utilities.Constants;
import io.edo.utilities.DateManager;

public class FileOptions {


    public Activity c;
    public EdoUI edoUI;
    public EdoContainer container;
    public LSpace space;

    public FileUIDAO fileDAO;


    public EdoProgressDialog loading;

    public FileOptions(Activity c, EdoContainer container) {
        super();
        this.c = c;
        this.edoUI = new EdoUI(c);
        this.container = container;

        fileDAO = new FileUIDAO(c);

        this.loading = new EdoProgressDialog(c, R.string.loading, R.string.loading);
    }

    //*************	OPTIONS	********************
    public void infoDialog(final LSpace space, final LFile file){
        if(file!=null){
            final EdoDialogOne dialog = new EdoDialogOne(c);
            EdoUI.setDialogWindowParams(dialog);

            dialog.setTitle(file.getNameWithExt());

            new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
                final String info = getFileInfo(space, file);
                c.runOnUiThread(new Runnable() {public void run() {
                    dialog.setText(info);
                }});
            }}.start();

            dialog.setOnPositiveClickListener(new OnPositiveClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });



            dialog.show();
        }

    }
















    public void openWith(EdoContainer container, LSpace space, LFile file){
        FileApps.openFileWithApp(c, this, container, space, file);
    }






    public String getFileInfo(LSpace space, LFile file){

        String createdBy = null;

        if(file!=null && JavaUtils.isNotEmpty(file.getCreatedBy())){

            String edoUserId = ThisApp.getInstance(c).getEdoUserId();
            if(edoUserId!=null && edoUserId.equals(file.getCreatedBy())){
                createdBy = c.getResources().getString(R.string.createdyBy).replace(Constants.PH_CONTACTNAME, c.getResources().getString(R.string.you));
            }else{
                LContact contact = new LContactDAO(c).get(file.getCreatedBy());
                if(contact!=null){
                    createdBy = c.getResources().getString(R.string.createdyBy).replace(Constants.PH_CONTACTNAME, contact.getNameSurname());
                }
            }



        }


        if(file.isFolder()){
            String info = c.getResources().getString(R.string.folder);
            info = createdBy!=null? (info+"\n"+createdBy):info;
            return info;
        }else{
            boolean isStored = file.isStored();
            boolean isInCloud = file.isInCloud();
            if(isInCloud||isStored){


                String info = "";

                if(isStored && isInCloud){
                    LService service = space.getService(c,false);
                    if(service!=null) {
                        String store = c.getResources().getString(R.string.fileNameIsInCloudAndInLocal).
                                replaceFirst(Constants.PH_FILENAME, file.getName()).
                                replaceFirst(Constants.PH_SERVICENAME, service.getCommercialCloudName(c));
                        info+=(store+"\n\n");
                    }
                }else if(isInCloud){
                    LService service = space.getService(c, false);
                    if(service!=null) {
                        String store = c.getResources().getString(R.string.fileNameIsInCloudOnly).
                                replaceFirst(Constants.PH_FILENAME, file.getName()).
                                replaceFirst(Constants.PH_SERVICENAME, service.getCommercialCloudName(c));
                        info += (store + "\n\n");
                    }
                }else if(isStored){

                    String store = c.getResources().getString(R.string.fileNameIsInLocalOnly).
                            replaceFirst(Constants.PH_FILENAME, file.getName());
                    info+=(store+"\n\n");
                }

                if(isStored){

                    String size = c.getResources().getString(R.string.size)+": "+FilesUI.getFileSizePrint(c, file.getLocalUri());
                    info+=(size+"\n");

                }else if(isInCloud){
                    LService service = space.getService(c, false);
                    ApiResponse response = ApiFile.getFileSizeLong(c, service, file);
                    Long fileSize = (Long) (response!=null?response.result:null);

                    String size = c.getResources().getString(R.string.size)+": "+FilesUI.getFileSizePrint(c, fileSize);
                    info+=(size+"\n");
                }

                info+=(c.getResources().getString(R.string.extension)+": "+file.getExtension()+"\n");
                info+=(c.getResources().getString(R.string.creationDate)+": "+DateManager.getDateTimeDisplayFromMillis(c, file.getCDate())+"\n");
                info+=(c.getResources().getString(R.string.modificationDate)+": "+DateManager.getDateTimeDisplayFromMillis(c, file.getCDate()));

                info = createdBy!=null? (info+"\n"+createdBy):info;

                return info;
            }else if(file.isWebLink()){
                String info =
                        c.getResources().getString(R.string.type)+": "+FilesUI.getFileTypePrintName(c, file)+"\n"+
                                c.getResources().getString(R.string.creationDate)+": "+DateManager.getDateTimeDisplayFromMillis(c, file.getCDate())+"\n";

                info = createdBy!=null? (info+"\n"+createdBy):info;

                return info;
            }else{
                String info = c.getResources().getString(R.string.fileStoredInAnotherDevice);
                info = createdBy!=null? (info+"\n"+createdBy):info;
                return info;
            }
        }

    }

    public void renameFileDialog(final LSpace space, final LFile file){
        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        dialog.setContentView(R.layout.dialog_edit);

        EdoUI.setDialogWindowParams(dialog);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText(R.string.rename);

        final EditText et_name = (EditText) dialog.findViewById(R.id.et_text);
        Button bt_positive = (Button) dialog.findViewById(R.id.bt_positive);
        Button bt_negative = (Button) dialog.findViewById(R.id.bt_negative);



        bt_positive.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String newName = et_name.getText().toString();
                renameFile(space, file, newName);
                dialog.dismiss();
            }
        });

        bt_negative.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        et_name.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String newName = et_name.getText().toString();
                    renameFile(space, file, newName);
                    dialog.dismiss();
                }
                return false;
            }
        });

        if(file!=null && JavaUtils.isNotEmpty(file.getName())){
            String baseName = FilenameUtils.getBaseName(file.getName());
            et_name.setText(baseName);
        }

        dialog.show();
    }

    public void renameFile(final LSpace space, final LFile file, final String newName){


        boolean isInternetOn = !EdoUI.dialogNoInternet(c,
                new Runnable() {public void run() {
                    renameFile(space, file, newName);
                }},
                null);

        if(isInternetOn){
            new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();

                c.runOnUiThread(new Runnable() {public void run() {
                    loading.setTitle(file.getName());
                    loading.setSubTitle(R.string.updatingInProgress);
                    loading.show();
                }});

                if(file!=null){
                    file.setName(newName);
                    new FileUIDAO(c).rename(container, space, file);
                }

                c.runOnUiThread(new Runnable() {public void run() {
                    loading.dismiss();
                }});
            }}.start();
        }

    }

    public void star(final LFile file){

        boolean isInternetOn = !EdoUI.dialogNoInternet(c,
                new Runnable() {public void run() {
                    star(file);
                }},
                null);

        if(isInternetOn){
            new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
                c.runOnUiThread(new Runnable() {public void run() {
                    loading.setTitle(file.getName());
                    loading.setSubTitle(R.string.updatingInProgress);
                    loading.show();
                }});
                if(file!=null){
                    file.setIsStarred(!file.isStarred());
                    new FileUIDAO(c).star(container, space, file);
                }
                c.runOnUiThread(new Runnable() {public void run() {
                    loading.dismiss();
                }});
            }}.start();
        }


    }





    public void addSpaceDialog(final EdoContainer container){
        Intent intent = new Intent(c, Act_SpaceEditor.class);
        intent.putExtra(DBConstants.CONTAINER, container);
        c.startActivityForResult(intent, Constants.SPACE_EDITOR_RESULT);
    }

    public void editSpaceDialog(final EdoContainer container, final LSpace space){
        Intent intent = new Intent(c, Act_SpaceEditor.class);
        intent.putExtra(DBConstants.CONTAINER, container);
        intent.putExtra(DBConstants.SPACE, space);
        c.startActivityForResult(intent, Constants.SPACE_EDITOR_RESULT);
    }








    public void addFolderDialog(final EdoProgressDialog loading, final LSpace space){
        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        dialog.setContentView(R.layout.dialog_edit);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText(R.string.addNewFolder);

        EdoUI.setDialogWindowParams(dialog);

        final EditText et_name = (EditText) dialog.findViewById(R.id.et_text);
        Button bt_positive = (Button) dialog.findViewById(R.id.bt_positive);
        Button bt_negative = (Button) dialog.findViewById(R.id.bt_negative);

        bt_positive.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String folderName = JavaUtils.isNotEmpty(et_name.getText().toString())? et_name.getText().toString() : c.getResources().getString(R.string.folder);
                addFolder(loading, space, folderName);
                dialog.dismiss();
            }
        });

        bt_negative.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        et_name.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String folderName = JavaUtils.isNotEmpty(et_name.getText().toString()) ? et_name.getText().toString() : c.getResources().getString(R.string.folder);
                    addFolder(loading, space, folderName);
                    dialog.dismiss();
                }
                return false;
            }
        });


        dialog.show();
    }

    public void addFolder(final EdoProgressDialog loading, final LSpace spaceWithParentFolder, final String folderName){
        boolean isInternetOn = !EdoUI.dialogNoInternet(c,
                new Runnable() {public void run() {
                    addFolder(loading, spaceWithParentFolder, folderName);
                }},
                null);

        if(isInternetOn){
            loading.setTitle(R.string.loading);
            loading.setSubTitle(R.string.creatingFolder);
            loading.show();
            new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
                if(JavaUtils.isNotEmpty(folderName)){
                    LFile parentFile = spaceWithParentFolder.getCurrentFolder();
                    if(spaceWithParentFolder.getCurrentFolder()!=null){
                        parentFile = spaceWithParentFolder.getCurrentFolder();
                    }

                    boolean added = false;

                    LFile folder = LFile.newFolderFile(parentFile, folderName, ThisApp.getInstance(c).getEdoUserId());
                    if(container.isContactOrGroup()){
                        added = new ContactFileUIDAO(c).add(((LContact)container), spaceWithParentFolder, folder)!=null;
                    }

                    if(!added){
                        c.runOnUiThread(new Runnable() {public void run() {
                            EdoUI.showToast(c, c.getResources().getString(R.string.errorAddingNewFolderRetry), null, Toast.LENGTH_SHORT);
                        }});
                    }


                    loading.dismiss();
                }else{
                    loading.dismiss();
                    c.runOnUiThread(new Runnable() {public void run() {
                        edoUI.showToastShort(R.string.chooseANameForThisFolder, null);
                    }});
                }

            }}.start();
        }
    }


    public void editFolderDialog(final LSpace space, final LFile folder, final Runnable filePage){
        if(folder!=null){
            final EdoDialogListView dialog = new EdoDialogListView(c);
            dialog.setTitle(folder.getName());

            final ArrayList<Integer> titles = new ArrayList<Integer>();

            titles.add(R.string.Notes);
            titles.add(R.string.rename);
            titles.add(R.string.delete);

            ListAdapterOptions adapter = new ListAdapterOptions(c, titles);
            dialog.setAdapter(adapter);
            dialog.setPositiveButtonText(R.string.cancel);
            dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

                @Override
                public void onClick(View arg0) {
                    dialog.cancel();
                }
            });
            dialog.setOnListItemClickListener(new OnListItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    switch (titles.get(position)) {

                        case R.string.Notes:
                            c.runOnUiThread(filePage);
                            break;

                        case R.string.rename:
                            renameFileDialog(space, folder);
                            break;

                        case R.string.delete:
                            ArrayList<LFile> files = new ArrayList<LFile>();
                            files.add(folder);
                            deleteFilesDialog(space, files);
                            break;

                        default:
                            break;
                    }
                    dialog.cancel();
                }
            });
            dialog.show();
        }
    }





    public void deleteFileDialog(final LSpace space, LFile file){
        ArrayList<LFile> files = new ArrayList<LFile>();
        files.add(file);
        deleteFilesDialog(space,files);
    }

    public void deleteFilesDialog(final LSpace space, final ArrayList<LFile> files){
        String message = null;
        if(files.size()==1){
            message = c.getResources().getString(R.string.doYouWantToDelete)+" "+files.get(0).getName()+"?";
        }else{
            message = c.getResources().getString(R.string.doYouWantToDeleteFiles).replace(Constants.PH_NUM, ""+files.size());
        }

        final Dialog dialog = new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        dialog.setContentView(R.layout.dialog_file_delete);

        EdoUI.setDialogWindowParams(dialog);

        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        TextView tv_question = (TextView) dialog.findViewById(R.id.tv_text);
        LinearLayout bt_removeFromEdo = (LinearLayout) dialog.findViewById(R.id.ll_removeFromEdo);
        final CheckBox cb_removeFromEdo = (CheckBox) dialog.findViewById(R.id.cb_removeFromEdo);
//        LinearLayout bt_removeFromCloud = (LinearLayout) dialog.findViewById(R.id.ll_removeFromCloud);
//        final CheckBox cb_removeFromCloud = (CheckBox) dialog.findViewById(R.id.cb_removeFromCloud);
        LinearLayout bt_removeFromLocal = (LinearLayout) dialog.findViewById(R.id.ll_removeFromLocal);
        final CheckBox cb_removeFromLocal = (CheckBox) dialog.findViewById(R.id.cb_removeFromLocal);
        Button bt_close = (Button) dialog.findViewById(R.id.bt_negative);
        Button bt_apply = (Button) dialog.findViewById(R.id.bt_positive);

        tv_title.setText(R.string.delete);
        tv_question.setText(message);

        bt_removeFromEdo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                cb_removeFromEdo.setChecked(!cb_removeFromEdo.isChecked());
            }
        });


        if(files.size()==1){
            LFile file = files.get(0);
//            if(file.isInCloud()){
//                bt_removeFromCloud.setVisibility(Button.VISIBLE);
//                cb_removeFromCloud.setVisibility(CheckBox.VISIBLE);
//                bt_removeFromCloud.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        cb_removeFromCloud.setChecked(!cb_removeFromCloud.isChecked());
//                    }
//                });
//            }else{
//                cb_removeFromCloud.setChecked(false);
//                bt_removeFromCloud.setVisibility(Button.GONE);
//                cb_removeFromCloud.setVisibility(CheckBox.GONE);
//            }
            if(file.isStored()){
                bt_removeFromLocal.setVisibility(Button.VISIBLE);
                cb_removeFromLocal.setVisibility(CheckBox.VISIBLE);
                bt_removeFromLocal.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        cb_removeFromLocal.setChecked(!cb_removeFromLocal.isChecked());
                    }
                });
            }else{
                cb_removeFromLocal.setChecked(false);
                bt_removeFromLocal.setVisibility(Button.GONE);
                cb_removeFromLocal.setVisibility(CheckBox.GONE);
            }
        }

        final ArrayList<LFile> filesToDelete = new ArrayList<>();
        filesToDelete.addAll(files);

        bt_apply.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                final boolean removeFromEdo = cb_removeFromEdo.isChecked();
                final boolean removeFromLocal = cb_removeFromLocal.isChecked();
//                final boolean removeFromCloud = cb_removeFromCloud.isChecked();

                loading.setTitle(R.string.loading);
                loading.setSubTitle(R.string.removingFile);
                loading.show();


                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        boolean done = true;
                        for(LFile file:filesToDelete){
                            if(!deleteFile(c, container, space, file, removeFromEdo, removeFromLocal))
                                done = false;
                        }

                        if(!done){
                            c.runOnUiThread(new Runnable() {public void run() {
                                EdoUI.showToast(c, c.getResources().getString(R.string.errorRetry), null, Toast.LENGTH_SHORT);
                            }});
                        }

                        loading.dismiss();
                    }
                }).start();



                dialog.dismiss();
            }
        });

        bt_close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();



    }



    public boolean deleteFile(Activity c, EdoContainer container, LSpace space, LFile file, boolean removeFromEdo, boolean removeFromLocal){

        ThisApp app = ThisApp.getInstance(c);
        if(app.failedOperations!=null){
            app.failedOperations.remove(file.getId());
        }
        if(app.asyncTaskMng!=null && app.asyncTaskMng.getSize()>0){
            app.asyncTaskMng.stop(file.getId(),false);
        }
        return new FileUIDAO(c).deleteFile(container, space, file, removeFromEdo, removeFromLocal)!=null;
    }


    public void downloadFile(final EdoContainer container, final LSpace space, final LFile edoFile){

        if(edoFile.isStored() || !edoFile.isDownloadable())
            return;


        if(Constants.getExternalCacheDir(c)==null){
            EdoUI.showToast(c, R.string.noExternalMemoryfound, null, Toast.LENGTH_LONG);
            return;
        }


        LService service = space.getService(c,false);
        if(service!=null){

            String folderPath = space.getPhysicalPath(c, container);

            if(folderPath!=null && !isFileAlreadyStored(folderPath, edoFile)){
                c.runOnUiThread(new Runnable() {public void run() {
                    edoUI.showToastShort(R.string.downloadInProgress, null);
                }});

                new Service_DwnUpl().download(c, container, space, folderPath, edoFile);
            }
        }
    }

    public void downloadFileAndCheckInternet(final EdoContainer container, final LSpace space, final LFile edoFile){
        if(edoFile.isStored() || !edoFile.isInCloud() || edoFile.isOnlyMetaFile())
            return;


        boolean isInternetOn = !EdoUI.dialogNoInternet(c,
                new Runnable() {public void run() {
                    downloadFileAndCheckInternet(container, space, edoFile);
                }},
                null);

        if(isInternetOn){
            downloadFile(container, space, edoFile);
        }
    }


    public boolean isFileAlreadyStored(String folderPath, LFile edoFile){

        String filePath = folderPath+File.separator+edoFile.getName()+"."+edoFile.getExtension();
        File fileContent = new File(filePath);

        if(fileContent!=null && fileContent.exists()){
            edoFile.setLocalUri(filePath);

            return fileDAO.getLDAO().update(edoFile);
        }

        return false;
    }


    public void uploadFileAndCheckInternet(final EdoContainer container, final LSpace space, final LFile edoFile, final LActivity a){
        if(!edoFile.isStored() || !edoFile.isUploadable())
            return;


        boolean isInternetOn = !EdoUI.dialogNoInternet(c,
                new Runnable() {public void run() {
                    uploadFileAndCheckInternet(container, space, edoFile, a);
                }},
                null);

        if(isInternetOn){
            uploadFile(container, space, edoFile, a);
        }
    }


    public void uploadFile(final EdoContainer container, final LSpace space, final LFile edoFile, LActivity a){
        if(!edoFile.isStored() || !edoFile.isUploadable())
            return;



        if(a==null){
            LActivity oldActivity = new LActivityDAO(c).getByResourceId(edoFile.getId());

            a = new LActivity(LActivity.getContext(c, container), LActivity.EVENT_INSERT, LActivity.RESOURCE_FILE);
            a.setExtra(ThisApp.getInstance(c).getUserContact(), (LContact) container, space, edoFile);
            a.setStatus(LActivity.STATUS_STARTED);

            a.setOldActivityId(oldActivity.getId());

            new EdoActivityManager(c).processActivity(a);
        }else{
            a.setEvent(LActivity.EVENT_UPLOAD_TO_CLOUD);
            a.setStatus(LActivity.STATUS_STARTED);
        }

        LService service = space.getService(c,false);
        if(service!=null){

            c.runOnUiThread(new Runnable() {
                public void run() {
                    edoUI.showToastShort(R.string.uploadInProgress, null);
                }
            });
            if(container.isPersonal(c)){
                new Service_DwnUpl().upload(c, space, edoFile, a);
            }else{
                new Service_DwnUpl().uploadAndShare(c, (LContact) container, space, edoFile, a);
            }

        }
    }


    public void stopUpload(LFile file){
        Service_DwnUpl.stop(c, file.getId());
        EdoNotification.cancelNotification(c, EdoNotification.ID_UPLOAD);
//		EdoUIUpdateManager.removeUpdate(c, file.getId());
//		EdoUIUpdate uiUpdate = new EdoUIUpdate(null,EdoUIUpdate.EVENT_UPLOAD_TO_CLOUD, EdoUIUpdate.PROGRESS_STATUS_STOP, 0, null, null);
//		EdoUIUpdateManager.sendUpdate(c, file.getId(), uiUpdate);
        c.runOnUiThread(new Runnable() {public void run() {
            edoUI.showToastShort(R.string.uploadStopped, null);
        }});
    }

    public void stopDownload(LFile file){
        Service_DwnUpl.stop(c, file.getId());
        EdoNotification.cancelNotification(c, EdoNotification.ID_DOWNLOAD);
//		EdoUIUpdateManager.removeUpdate(c, file.getId());
//		EdoUIUpdate uiUpdate = new EdoUIUpdate(null,EdoUIUpdate.EVENT_DOWNLOAD_FROM_CLOUD, EdoUIUpdate.PROGRESS_STATUS_STOP, 0, null, null);
//		EdoUIUpdateManager.sendUpdate(c, file.getId(), uiUpdate);
        c.runOnUiThread(new Runnable() {public void run() {
            edoUI.showToastShort(R.string.downloadStopped, null);
        }});
    }

    public void removeFileFromLocal(final LFile file, final LocalFileRemovedListener listener){
        if(file!=null){
            new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
                c.runOnUiThread(new Runnable() {public void run() {
                    loading.setTitle(file.getName());
                    loading.setSubTitle(R.string.removeFromLocal);
                    loading.show();
                }});

                fileDAO.removeLocalFile(file);
                if(listener!=null){
                    listener.onFileRemoved();
                }

                c.runOnUiThread(new Runnable() {public void run() {
                    loading.dismiss();
                    edoUI.showToastShort(R.string.fileRemovedFromSdCard, null);
                }});
            }}.start();
        }
    }

    public interface LocalFileRemovedListener{
        void onFileRemoved();
    }



    public void shareWith(final LFile file){
        ArrayList<LFile> files = new ArrayList<LFile>();
        files.add(file);
        shareWith(files);
    }
    public void shareWith(ArrayList<LFile> localFiles){
        final ArrayList<Uri> uris = new ArrayList<Uri>();
        final ArrayList<String> mimes = new ArrayList<String>();
        final StringBuffer text = new StringBuffer();

        final ArrayList<LFile> filesNotReady = new ArrayList<LFile>();


        for(LFile file: localFiles){
            if(file.isStored()){
                uris.add(Uri.fromFile(new File(file.getLocalUri())));
                String mime = file.getType();
                if(JavaUtils.isEmpty(mime)){
                    mime = FileInfo.getMime(file.getLocalUri());
                    file.setType(mime);
                }
                mimes.add(mime);
            }else if(file.isWebLink()){
                text.append(file.getWebUri()+"\n\n");

            }else if(file.isSnippet()){
                text.append(file.getContent()+"\n\n");

            }else{
                filesNotReady.add(file);
            }
        }
        if(filesNotReady.size()>0){
            final EdoDialog dialog = new EdoDialog(c);
            String sFilesNotReady = "";
            for(LFile file: filesNotReady){
                sFilesNotReady += file.getNameWithExt()+"\n";
            }
            dialog.setTitle(null);
            String message = filesNotReady.size()==1? c.getResources().getString(R.string.thisFileCantBeShared) :
                    (c.getResources().getString(R.string.theFollowingFilesCantBeShared)+": \n\n"+sFilesNotReady);
            dialog.setText(message);
            if(uris.size()>0){
                dialog.setPositiveButtonText(R.string.continue_);
            }else{
                dialog.setPositiveButtonText(R.string.ok);
            }
            dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    if(uris.size()<=0 && JavaUtils.isNotEmpty(text.toString())) {
                        singleShare(c, null, text.toString(), FileInfo.MIME_TEXT);
                    }else if(uris.size()==1){
                        singleShare(c, uris.get(0), text.toString(), mimes.get(0));
                    }else if(uris.size()>0){
                        multipleShare(c,uris, text.toString());
                    }
                    //					else if(filesNotReady.size()>0){
                    //						for(LFile file: filesNotReady){
                    //							downloadFile(space, file);
                    //						}
                    //					}

                }
            });
            dialog.setNegativeButtonText(R.string.cancel);
            dialog.setOnNegativeClickListener(new OnNegativeClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.show();
        }else if(uris.size()<=0 && JavaUtils.isNotEmpty(text.toString())) {
            singleShare(c, null, text.toString(), FileInfo.MIME_TEXT);
        }else if(uris.size()==1){
            singleShare(c, uris.get(0), text.toString(), mimes.get(0));
        }else if(uris.size()>0){
            multipleShare(c,uris, text.toString());
        }

    }

    public static void multipleShare(Context c, ArrayList<Uri> uris, String text){
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        if(uris!=null)
            shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        if(JavaUtils.isNotEmpty(text))
            shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.setType("*/*");
        c.startActivity(Intent.createChooser(shareIntent, c.getResources().getString(R.string.share)));
    }

    public static void singleShare(Context c, Uri uri, String text, String mimeType){
        try{
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            if(JavaUtils.isNotEmpty(mimeType))
                shareIntent.setType(mimeType);
            if(uri!=null)
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            if(JavaUtils.isNotEmpty(text))
                shareIntent.putExtra(Intent.EXTRA_TEXT, text);
            c.startActivity(shareIntent);
        }catch(android.content.ActivityNotFoundException e){
        }
    }

//	public void sendToOther(LFile file){
//		String dialogTitle = c.getResources().getString(R.string.sendFileWith);
//		String subject = c.getResources().getString(R.string.subjectSharedEmail).replace(Constants.PH_FILENAME, file.getNameWithExt());
//		String text = "\n\n\n\n\n\n\n\n\n\n\n\n"+c.getResources().getString(R.string.app_name);
//		String mime = file.getType();
//		if(JavaUtils.isEmpty(mime) && JavaUtils.isNotEmpty(file.getLocalUri()))
//			mime = FileInfo.getMime(file.getLocalUri());
//		AUI.sendEmail(c, dialogTitle, null, subject, text, file.getLocalUri(), file.getType());
//	}


    public void forwardShare(final LFile file){
        ArrayList<LFile> files = new ArrayList<LFile>();
        files.add(file);
        forwardShare(files);
    }

    public void forwardShare(ArrayList<LFile> localFiles){
        Intent fromAppPicker = new Intent(c, Act_Forward_Share.class);
        fromAppPicker.setAction(Constants.ACTION_SEND_MULTIPLE_BY_EDO);
        LocalFiles files = new LocalFiles(localFiles);
        fromAppPicker.putExtra(Constants.EXTRA_FILES, files);
        c.startActivity(fromAppPicker);
    }

    public void forwardOrganize(final LFile file){
        ArrayList<LFile> files = new ArrayList<LFile>();
        files.add(file);
        forwardOrganize(files);
    }

    public void forwardOrganize(ArrayList<LFile> localFiles){
        Intent fromAppPicker = new Intent(c, Act_Forward_Organize.class);
        fromAppPicker.setAction(Constants.ACTION_SEND_MULTIPLE_BY_EDO);
        LocalFiles files = new LocalFiles(localFiles);
        fromAppPicker.putExtra(Constants.EXTRA_FILES, files);
        c.startActivity(fromAppPicker);
    }


    public void showComments(LSpace space, LFile file){
        Intent commentsIntent = EdoIntentManager.getCommentsIntent(c,container,space,file);
        c.startActivity(commentsIntent);
    }
}
