package io.edo.ui.adapters.contacts;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import io.edo.db.local.LocalDBHelper;
import io.edo.edomobile.R;

public class CursorAdapterNewQuickContact extends CursorAdapter {


	public static CursorAdapterNewQuickContact getQuickContactCursorAdapter(Context c, String id, String name, String email){
		// Create new cursor
		String[] columns = {"_id",LocalDBHelper.KEY_NAME, LocalDBHelper.KEY_EMAIL};
		Object[] temp = new Object[] { 0L, name, email };

		MatrixCursor cursor = new MatrixCursor(columns);
		cursor.addRow(temp);

		return new CursorAdapterNewQuickContact(c, cursor);
	}



	private CursorAdapterNewQuickContact(Context context, Cursor cursor) {
		super(context, cursor, false);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.row_edo_contact_left_image, parent, false);
		return rowView;
	}


	@Override
	public void bindView(View rowView, Context context, Cursor cursor) {

		String name = cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_NAME));
		String email = cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_EMAIL));


		TextView tv_title = (TextView) rowView.findViewById(R.id.tv_title);
		tv_title.setText(name);

		TextView tv_subtitle = (TextView) rowView.findViewById(R.id.tv_sub_title);
		tv_subtitle.setText(email);

		ImageView contact_photo = (ImageView) rowView.findViewById(R.id.contact_photo);
		contact_photo.setImageResource(R.drawable.ic_action_new);

//		Button bt_edocontact = (Button) rowView.findViewById(R.id.bt_edocontact);
//		bt_edocontact.setVisibility(ImageView.GONE);

		ImageView iv_rightIcon = (ImageView) rowView.findViewById(R.id.iv_rightIcon);
		iv_rightIcon.setImageResource(R.drawable.ic_action_accept);
		iv_rightIcon.setVisibility(View.VISIBLE);


	}

}
