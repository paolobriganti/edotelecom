package io.edo.ui.adapters.timeline;

/**
 * Created by PaoloBriganti on 01/07/15.
 */
public abstract interface TimelineBaseViewHolder {


    public abstract void setSelected(boolean isSelected);
}
