package io.edo.ui;

import android.app.Activity;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paolobriganti.android.AUI;
import com.paolobriganti.android.AUtils;
import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.global.uibeans.CommentUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.Comment;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoDialogListView.OnListItemClickListener;
import io.edo.ui.EdoDialogOne.OnPositiveClickListener;
import io.edo.ui.adapters.ListAdapterOptions;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.utilities.Constants;
import io.edo.utilities.DateManager;

public class CommentsUIContacts implements CommentsUI {

	private Activity c;
	private ThisApp app;

	private CommentUIDAO commentUIDAO;

	private LContact contact;


	private String me = "me";

	private LFile file;

	//Views shared
	public LinearLayout layout;
	private LinearLayout ll_comments;
	private EditText et_comment;
	private ImageButton bt_addComment;


	private EdoProgressDialog loading;

	//Views contacts
	ContactPhotoLoader contactPhotoLoader;

	private LayoutInflater inflater;

	boolean isTimeline = false;


	public CommentsUIContacts(final Activity c,
							  LContact contact,
							  ContactPhotoLoader contactPhotoLoader,
							  View layout, boolean isTimeline) {
		super();
		this.c = c;
		this.app = ThisApp.getInstance(c);

		this.commentUIDAO = new CommentUIDAO(c);

		this.contact = contact;

		inflater = c.getLayoutInflater();

		this.contactPhotoLoader = contactPhotoLoader;

		me = c.getResources().getString(R.string.me);

		loading = new EdoProgressDialog(c,R.string.loading,R.string.postingComment);

		layout.setVisibility(View.VISIBLE);
		ll_comments = (LinearLayout) layout.findViewById(R.id.ll_comments);
		et_comment = (EditText) layout.findViewById(R.id.et_comment);

		if(et_comment!=null){
			bt_addComment = (ImageButton) layout.findViewById(R.id.bt_addComment);
			bt_addComment.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					postComment();
				}
			});
		}

		this.isTimeline = isTimeline;

	}






	@Override
	public void setComments(final LFile file, final boolean setViews){
		CommentsUIContacts.this.file = file;

		if(setViews)
			setCommentsViews();

	}

	@Override
	public void setCommentsViews() {
		c.runOnUiThread(new Runnable() {public void run() {
			if(file!=null){
				setCommentsViews(file.getCommentsList());
			}
		}});
	}


    @Override
    public void postComment() {
        String message = et_comment.getText().toString();
        addNewComment(file, message);
    }

    public void addNewComment(final LFile file, final String message){
		if(JavaUtils.isNotEmpty(message)){
			AUI.keyboardHide(c);
			loading.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if(!file.hasComments()){
						c.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								ll_comments.removeAllViews();
							}
						});
                    }

                    final LFile wfile = commentUIDAO.addComment(c, contact, file, message);

                    if(wfile!=null && wfile.getComments()!=null && wfile.getComments().length>0){
//                        Comment comment = wfile.getComments()[wfile.getComments().length-1];
//                        final View commentView = getCommentLayout(inflater, comment);

                        c.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                ll_comments.addView(commentView);

                                et_comment.setText("");
                            }
                        });


					}else{
						c.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								EdoUI.showToast(c, c.getResources().getString(R.string.errorRetry), null, Toast.LENGTH_SHORT);
							}
						});

					}


					loading.dismiss();
                }
            }).start();




		}
	}








	private void setCommentsViews(ArrayList<Comment> comments) {
		ll_comments.removeAllViews();

		boolean hasComments = comments!=null && comments.size()>0;

		if(hasComments){
			for(Comment comment : comments) {
				View commentView = getCommentLayout(inflater, comment);
				if(commentView!=null)
					ll_comments.addView(commentView);
				if(isTimeline)
					break;
			}
		}else{
			String text = c.getResources().getString(R.string.noteEmptyContactsTextFile);
			Comment comment = new Comment(Constants.USER_NO, text);

			View commentView = getCommentLayout(inflater, comment);
			ll_comments.addView(commentView);
		}

	}







	public View getCommentLayout(final LayoutInflater inflater, final Comment comment){
		if(comment==null || comment.getMessage()==null)
			return null;

		final View commentView = inflater.inflate(R.layout.layout_comment_detail, ll_comments, false);

		c.runOnUiThread(new Runnable() {public void run() {

			OnClickListener onCommentClickListener = new OnClickListener() {

				@Override
				public void onClick(View v) {
					onCommentClick(comment);
				}
			};

			OnLongClickListener onCommentLongClickListener = new OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					onCommentClick(comment);
					return true;
				}
			};

			commentView.setOnClickListener(onCommentClickListener);
			commentView.setOnLongClickListener(onCommentLongClickListener);

			final ImageView iv_image = (ImageView) commentView.findViewById(R.id.iv_image);
			iv_image.setOnClickListener(onCommentClickListener);
			iv_image.setOnLongClickListener(onCommentLongClickListener);

//			final TextView tv_title = (TextView) commentView.findViewById(R.id.tv_title);
//			tv_title.setOnClickListener(onCommentClickListener);
//			tv_title.setOnLongClickListener(onCommentLongClickListener);

			final TextView tv_comment = (TextView) commentView.findViewById(R.id.tv_comment);
			tv_comment.setOnClickListener(onCommentClickListener);
			tv_comment.setOnLongClickListener(onCommentLongClickListener);
			tv_comment.setMovementMethod(LinkMovementMethod.getInstance());

			final TextView tv_date = (TextView) commentView.findViewById(R.id.tv_date);
			tv_date.setOnClickListener(onCommentClickListener);
			tv_date.setOnLongClickListener(onCommentLongClickListener);

			final Button bt_action = (Button) commentView.findViewById(R.id.bt_action);
			if(comment.getType()!=null && comment.getType().equals(Comment.TYPE_ACTIONABLE)) {
				bt_action.setVisibility(View.VISIBLE);
				bt_action.setText(comment.getButtonLabel()!=null?comment.getButtonLabel():"Paga ora");
				bt_action.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						final EdoDialogOne dialog = new EdoDialogOne(c);
						dialog.setTitle(" ");
						dialog.setText("Pagamento con TIM Pay");

						dialog.setOnPositiveClickListener(new OnPositiveClickListener() {
							@Override
							public void onClick(View v) {
								dialog.dismiss();
							}
						});
						dialog.show();
						bt_action.setVisibility(View.GONE);
						new Thread(new Runnable() {
							@Override
							public void run() {
								String originalMessage = comment.getMessage();
								Comment[] comments = file.getComments();
								for(Comment com:comments){
									if(com.getMessage().contains(originalMessage)){
										com.setType(null);
										com.setcDate(System.currentTimeMillis());
									}
								}
								new LFileDAO(c).update(file);
							}
						}).start();
					}
				});
			}else{
				bt_action.setVisibility(View.GONE);
			}
			if(!comment.getUserId().equals(Constants.USER_NO)){
				iv_image.setVisibility(View.VISIBLE);
				String name = me;
				String edoUserId = comment.getUserId();
				if(!app.getEdoUserId().equals(edoUserId)){
					if(contact!=null){
						if(!contact.isGroup()){
							if(JavaUtils.isNotEmpty(contact.getNameSurname()))
								name = contact.getNameSurname();
							else
								name = contact.getPrincipalEmailAddress();

							contactPhotoLoader.loadImage(contact, iv_image);
						}else{
							LContact member = contact.getGroupMember(edoUserId);
							if(member==null){
								member = new LContactDAO(c).get(edoUserId);
							}
							if(member!=null){
								if(JavaUtils.isNotEmpty(member.getNameSurname()))
									name = member.getNameSurname();
								else
									name = member.getPrincipalEmailAddress();

								contactPhotoLoader.loadImage(member, iv_image);
							}else{
								name = c.getResources().getString(R.string.contact);
								iv_image.setImageResource(R.drawable.ic_contact_nothumb);
							}

						}
					}

				}else{
//					iv_image.setImageResource(R.drawable.no_image);
//					iv_image.setVisibility(View.GONE);
					contactPhotoLoader.loadImage(app.getUserContact(), iv_image);
				}
//				final String userName = name;
//				tv_title.setText(userName);

				long GMTmillis = comment.getcDate();

				tv_date.setText(name+" - "+DateManager.getUserFriendlyDateFromMillis(c, GMTmillis));


			}else{
//				tv_title.setVisibility(View.GONE);
				iv_image.setVisibility(TextView.GONE);
			}

			tv_comment.setText(comment.getMessage());


		}});

		return commentView;
	}





	private void onCommentClick(Comment comment){
		if(isTimeline){
			FileOptions fileOptions = new FileOptions(c,contact);
			fileOptions.showComments(file.getSpace(c),file);
		}else{
			final String oldMessage = comment.getMessage();


			final EdoDialogListView dialog = new EdoDialogListView(c);
			dialog.setTitle(R.string.message);

			final ArrayList<Integer> titles = new ArrayList<Integer>();
			titles.add(R.string.copy);

			ListAdapterOptions adapter = new ListAdapterOptions(c, titles);
			dialog.setAdapter(adapter);
			dialog.setPositiveButtonText(R.string.cancel);
			dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

				@Override
				public void onClick(View arg0) {
					dialog.cancel();
				}
			});
			dialog.setOnListItemClickListener(new OnListItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					switch (titles.get(position)) {

						case R.string.copy:
							AUtils.copyToClipboard(c, oldMessage);
							break;

						default:
							break;
					}
					dialog.cancel();
				}
			});
			dialog.show();
		}
	}







	@Override
	public View getLayout() {
		// TODO Auto-generated method stub
		return layout;
	}




















}
