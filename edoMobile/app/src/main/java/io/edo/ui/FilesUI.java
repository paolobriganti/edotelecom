package io.edo.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;

import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.edomobile.R;
import io.edo.ui.adapters.ListAdapterImageTextSubText;
import io.edo.ui.imageutils.ThumbnailManager;
import io.edo.ui.imageutils.ThumbnailViews;

public class FilesUI {


	public static void setFileProgress(final Activity c, final LActivity a, final LFile file, final ThumbnailViews fv){
//		if(fv!=null){
//			try{
//				c.runOnUiThread(new Runnable() {public void run() {
//					if(updateInfos!=null){
//						setProgress(c, fv, updateInfos, updateInfos.getPercent());
//					}else if(fv.fl_progress!=null){
//						fv.fl_progress.setVisibility(LinearLayout.GONE);
//						fv.iv_center.setVisibility(ImageView.GONE);
//						fv.iv_progress.setVisibility(View.GONE);
//					}
//				}});
//
//			}catch(OutOfMemoryError e){
//
//			}
//		}
	}

	public static void setProgress(final Activity c, final ThumbnailViews fv,
			final LActivity a){

//		if(infos.getProgressStatus()!= LActivity.VALUE_ERROR){
//			if(infos.getProgressStatus() == EdoUIUpdate.PROGRESS_STATUS_PENDING){
//				fv.fl_progress.setVisibility(View.VISIBLE);
//				fv.iv_center.setVisibility(ImageView.VISIBLE);
//				fv.iv_center.setImageResource(R.drawable.ic_file_status_pending);
//				fv.iv_progress.setVisibility(View.GONE);
//				fv.tv_progress.setVisibility(View.GONE);
//			}else if(infos.getProgressStatus() == EdoUIUpdate.PROGRESS_STATUS_IN_PROGRESS){
//				fv.fl_progress.setVisibility(View.VISIBLE);
//				fv.iv_center.setVisibility(ImageView.GONE);
//				if(infos.getEvent() == EdoUIUpdate.EVENT_DOWNLOAD_FROM_CLOUD){
//					fv.iv_progress.setImageResource(R.drawable.ic_file_status_loader_green);
//				}else{
//					fv.iv_progress.setImageResource(R.drawable.ic_file_status_loader);
//				}
//				fv.iv_progress.setVisibility(View.VISIBLE);
//				fv.iv_progress.setRotation(infos.getPercent() * 36 / 10);
//				fv.tv_progress.setVisibility(View.VISIBLE);
//				fv.tv_progress.setText(infos.getPercent() + "%");
//			}else if(infos.getProgressStatus() == EdoUIUpdate.PROGRESS_STATUS_ERROR){
//				fv.fl_progress.setVisibility(View.VISIBLE);
//				fv.iv_center.setVisibility(ImageView.VISIBLE);
//				fv.iv_center.setImageResource(R.drawable.ic_file_status_retry);
//				fv.iv_progress.setVisibility(View.GONE);
//				fv.tv_progress.setVisibility(View.GONE);
//			}
//		}else{
//			fv.fl_progress.setVisibility(View.GONE);
//			fv.iv_center.setVisibility(ImageView.GONE);
//			fv.iv_progress.setVisibility(View.GONE);
//			fv.tv_progress.setVisibility(View.GONE);
//		}
	}


	public static String getFileSizePrint(Context c, String filePath){
		if(JavaUtils.isNotEmpty(filePath)){
			File file = new File(filePath);
			if(file.exists())
				if(file.length()>FileInfo.GB_IN_BYTES)
					return FileInfo.getFileGByteSize(filePath)+" GB";
				else if(file.length()>FileInfo.MB_IN_BYTES)
					return FileInfo.getFileMByteSize(filePath)+" MB";
				else if(file.length()>FileInfo.KB_IN_BYTES)
					return FileInfo.getFileKByteSize(filePath)+" KB";
				else
					return FileInfo.getFileByteSize(filePath)+" Bytes";
		}
		return c.getResources().getString(R.string.unknown);
	}


	public static String getFileSizePrint(Context c, Long fileSize){
		if(fileSize!=null){
			if(fileSize>FileInfo.GB_IN_BYTES)
				return FileInfo.byteToGB(fileSize)+" GB";
			else if(fileSize>FileInfo.MB_IN_BYTES)
				return FileInfo.byteToMB(fileSize)+" MB";
			else if(fileSize>FileInfo.KB_IN_BYTES)
				return FileInfo.byteToKB(fileSize)+" KB";
			else
				return fileSize+" Bytes";
		}
		return c.getResources().getString(R.string.unknown);
	}

	public static String getFileTypePrintName(Context c, LFile file){
		if(file.isSpace())
			return c.getResources().getString(R.string.desk);
		if(file.isWebLink())
			if(file.isYoutubeLink()){
				return c.getResources().getString(R.string.youtubeVideo);
			}else{
				return c.getResources().getString(R.string.webPage);
			}
		String type = file.getvType().toUpperCase();
		if(JavaUtils.isNotEmpty(type)){
			int id = c.getResources().getIdentifier("FILE_TYPE_"+type, "string", c.getPackageName());
			return c.getResources().getString(id);
		}else{
			return c.getResources().getString(R.string.unknown);
		}
	}



	public static ListAdapterImageTextSubText getFilesListAdapter(Activity c, ArrayList<File> files){
		ArrayList<String> titles = new ArrayList<String>();
		ArrayList<String> subTitles = new ArrayList<String>();
		ArrayList<Drawable> images = new ArrayList<Drawable>();
		for(File file:files){
			titles.add(FilenameUtils.getBaseName(file.toString()));
			subTitles.add(FilenameUtils.getPath(file.toString()));
			images.add(c.getResources().getDrawable(ThumbnailManager.getIconResFromPath(c, file.getName())));
		}
		return new ListAdapterImageTextSubText(c, R.color.transparent,
				titles, c.getResources().getColor(R.color.white), 
				subTitles, c.getResources().getColor(R.color.grey), 
				images, c.getResources().getColor(R.color.transparent));
	}




	public static String getExtforUI(LFile efile){
		if(efile.isGdoc())
			return "Google Doc";
//		else if(efile.isGMap())
//			return "Google Map";
		else if(efile.isWebLink())
			return "WWW";
		else
			return (efile.getExtension()!=null) ? efile.getExtension().toUpperCase() : null;
	}

















}