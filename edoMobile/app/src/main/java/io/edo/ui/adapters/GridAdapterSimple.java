package io.edo.ui.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class GridAdapterSimple extends BaseAdapter {
	private final Context context;
	private final Integer bgColor;
	private final ArrayList<String> titles;
	private final ArrayList<Integer> images;
	private final Integer titlesColor;
	private final Integer imageBgColor;

	public GridAdapterSimple(Context context, Integer bgColor, ArrayList<String> titles, Integer titlesColor, ArrayList<Integer> images, Integer imageBgColor) {
		this.context = context;
		this.bgColor = bgColor;
		this.titles = titles;
		this.titlesColor = titlesColor;
		this.images = images;
		this.imageBgColor = imageBgColor;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.grid_item, parent, false);
		
		
		LinearLayout bg = (LinearLayout) rowView.findViewById(R.id.grid_item);
		if(bgColor!=null)
			bg.setBackgroundColor(bgColor);
		
		
		TextView label = (TextView) rowView.findViewById(R.id.label);
		if(titles!=null && titles.size()>0 && JavaUtils.isNotEmpty(titles.get(position)))
			label.setText(titles.get(position));
		else 
			label.setText(" ");
		
		if(titlesColor!=null)
			label.setTextColor(titlesColor);
		
		
		ImageView thumb = (ImageView) rowView.findViewById(R.id.thumb);
		if(images!=null && images.size()>0 && images.get(position)!=null){
			thumb.setVisibility(ImageView.VISIBLE);
			thumb.setImageResource(images.get(position));
			if(imageBgColor!=null)
				thumb.setBackgroundColor(imageBgColor);
		}else{
			thumb.setVisibility(ImageView.GONE);
			thumb.setBackgroundColor(Color.TRANSPARENT);
		}
		return rowView;
	}

	
} 