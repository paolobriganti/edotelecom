package io.edo.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.paolobriganti.android.ui.AudioPlayerFragment;
import com.paolobriganti.android.ui.views.VisualizerView;

import java.io.Serializable;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.edomobile.R;

/**
 * Created by PaoloBriganti on 31/07/15.
 */
public class Frag_AudioPlayer extends AudioPlayerFragment{


    LFile currentFile;

    private ImageButton btnPlay;
    private ImageButton btnForward;
    private ImageButton btnBackward;
    private TextView tv_title;
    private TextView tv_artist;
    private TextView tv_year;
    private TextView tv_album;
    private SeekBar songProgressBar;
    private TextView songCurrentDurationLabel;
    private TextView songTotalDurationLabel;
    private ImageView iv_art;
    private VisualizerView visualizer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null && getArguments().containsKey(DBConstants.FILE)) {
            Serializable fileSerial = getArguments().getSerializable(DBConstants.FILE);
            if(fileSerial!=null) {
                currentFile = (LFile) fileSerial;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.frag_audioplayer, container, false);

        btnPlay = (ImageButton) layout.findViewById(R.id.ib_play);
        btnForward = (ImageButton) layout.findViewById(R.id.ib_forward);
        btnBackward = (ImageButton) layout.findViewById(R.id.ib_rewind);
        tv_title = (TextView) layout.findViewById(R.id.tv_title);
        tv_artist = (TextView) layout.findViewById(R.id.tv_artist);
        tv_year = (TextView) layout.findViewById(R.id.tv_year);
        tv_album = (TextView) layout.findViewById(R.id.tv_album);
        songProgressBar = (SeekBar) layout.findViewById(R.id.seekBar);
        songCurrentDurationLabel = (TextView) layout.findViewById(R.id.tv_currentDuration);
        songTotalDurationLabel = (TextView) layout.findViewById(R.id.tv_totalDuration);
        iv_art = (ImageView) layout.findViewById(R.id.iv_art);
        visualizer = (VisualizerView) layout.findViewById(R.id.visualizer);
        visualizer.setColor(getResources().getColor(R.color.white_50_transparent));
        visualizer.setWidth(10f);
        return layout;
    }

    @Override
    public ImageButton getBtnPlay() {
        return btnPlay;
    }

    @Override
    public ImageButton getBtnForward() {
        return btnForward;
    }

    @Override
    public ImageButton getBtnBackward() {
        return btnBackward;
    }

    @Override
    public TextView getTvTitle() {
        return tv_title;
    }

    @Override
    public TextView getTvArtist() {
        return tv_artist;
    }

    @Override
    public TextView getTvYear() {
        return tv_year;
    }

    @Override
    public TextView getTvAlbum() {
        return tv_album;
    }

    @Override
    public TextView getTvCurrentLabel() {
        return songCurrentDurationLabel;
    }

    @Override
    public TextView getTotalDurationLabel() {
        return songTotalDurationLabel;
    }

    @Override
    public SeekBar getSongProgressBar() {
        return songProgressBar;
    }

    @Override
    public ImageView getImageArt() {
        return iv_art;
    }

    @Override
    public VisualizerView getVisualizer() {
        return visualizer;
    }

    @Override
    public int getPlayButtonDrawableResource() {
        return R.drawable.ic_action_play_w;
    }

    @Override
    public int getPauseButtonDrawableResource() {
        return R.drawable.ic_action_pause_w;
    }

    @Override
    public String getFilePath() {
        return currentFile.getLocalUri();
    }

    @Override
    public String getArtist() {
        if(currentFile.getCreatedBy()!=null){
            LContact contact = new LContactDAO(getActivity()).get(currentFile.getCreatedBy());
            if(contact!=null)
                return contact.getNameSurname();
        }
        return super.getArtist();
    }

    @Override
    public String getDefaultTitle() {
        return currentFile.getName();
    }


}
