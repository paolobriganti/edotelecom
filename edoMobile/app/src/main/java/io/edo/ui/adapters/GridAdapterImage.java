package io.edo.ui.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class GridAdapterImage extends BaseAdapter {
	private final Context context;
	private final ArrayList<Integer> images;
	private final Integer imageBgColor;

	public GridAdapterImage(Context context, ArrayList<Integer> images, Integer imageBgColor) {
		this.context = context;
		this.images = images;
		this.imageBgColor = imageBgColor;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return images.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return images.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.item_image, parent, false);
		
		
		ImageView thumb = (ImageView) rowView.findViewById(R.id.thumb);
		if(images!=null && images.size()>0 && images.get(position)!=null){
			thumb.setVisibility(ImageView.VISIBLE);
			thumb.setImageResource(images.get(position));
			if(imageBgColor!=null)
				thumb.setBackgroundColor(imageBgColor);
		}else{
			thumb.setVisibility(ImageView.GONE);
			thumb.setBackgroundColor(Color.TRANSPARENT);
		}
		return rowView;
	}

	
} 