package io.edo.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import io.edo.edomobile.R;

public class EdoDialog extends EdoDialogOne{

	String btNegativeText;
	int btNegativeTextRes = 0;
	Button bt_negative;

	OnNegativeClickListener negativeListener;

	public EdoDialog(Context context) {
		super(context);
		btNegativeTextRes = R.string.cancel;
		this.negativeListener = new OnNegativeClickListener() {
			
			@Override
			public void onClick(View v) {
				EdoDialog.this.cancel();
			}
		};
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_edo);


		EdoUI.setDialogWindowParams(this);
		
		tv_title = (TextView) this.findViewById(R.id.tv_title);
		tv_text = (TextView) this.findViewById(R.id.tv_text);
		bt_positive = (Button) this.findViewById(R.id.bt_positive);
		bt_negative = (Button) this.findViewById(R.id.bt_negative);

		setTitle(title);
		setTitle(titleRes);
		setText(text);
		setText(textRes);
		setPositiveButtonText(btPositiveText);
		setPositiveButtonText(btPositiveTextRes);
		setNegativeButtonText(btNegativeText);
		setNegativeButtonText(btNegativeTextRes);
		setOnPositiveClickListener(positiveListener);
		setOnNegativeClickListener(negativeListener);
	}

	

	public void setNegativeButtonText (final String text){
		if(JavaUtils.isNotEmpty(text)){
			this.btNegativeText = text;
			if(bt_negative!=null)
				bt_negative.setText(text);
		}
	}

	public void setNegativeButtonText (final int stringResourceId){
		if(stringResourceId!=0){
			this.btNegativeTextRes = stringResourceId;
			if(bt_negative!=null)
				bt_negative.setText(stringResourceId);
		}
	}
	
	public void setOnNegativeClickListener(OnNegativeClickListener listener) {
		if(listener!=null){
			this.negativeListener = listener;
			if(bt_negative!=null)
				bt_negative.setOnClickListener(listener);
		}
	}
	public interface OnNegativeClickListener extends View.OnClickListener{
	}

}
