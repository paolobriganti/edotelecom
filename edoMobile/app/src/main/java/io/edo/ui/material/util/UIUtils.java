/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.edo.ui.material.util;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import java.util.regex.Pattern;

/**
 * An assortment of UI helpers.
 */
public class UIUtils {
	private static final Pattern REGEX_HTML_ESCAPE = Pattern.compile(".*&\\S;.*");
	public static final float BG_COLOR_SCALE_FACTOR = 0.65f;
	private static final int[] RES_IDS_ACTION_BAR_SIZE = { android.R.attr.actionBarSize };

	/** Calculates the Action Bar height in pixels. */
	public static int calculateActionBarSize(Context context) {
		if (context == null) {
			return 0;
		}

		Resources.Theme curTheme = context.getTheme();
		if (curTheme == null) {
			return 0;
		}

		TypedArray att = curTheme.obtainStyledAttributes(RES_IDS_ACTION_BAR_SIZE);
		if (att == null) {
			return 0;
		}

		float size = att.getDimension(0, 0);
		att.recycle();
		return (int) size;
	}

	public static float getProgress(int value, int min, int max) {
		if (min == max) {
			throw new IllegalArgumentException("Max (" + max + ") cannot equal min (" + min + ")");
		}

		return (value - min) / (float) (max - min);
	}


	public static int setColorAlpha(int color, float alpha) {
		int alpha_int = Math.min(Math.max((int)(alpha * 255.0f), 0), 255);
		return Color.argb(alpha_int, Color.red(color), Color.green(color), Color.blue(color));
	}

	public static int scaleColor(int color, float factor, boolean scaleAlpha) {
		return Color.argb(scaleAlpha ? (Math.round(Color.alpha(color) * factor)) : Color.alpha(color),
				Math.round(Color.red(color) * factor), Math.round(Color.green(color) * factor),
				Math.round(Color.blue(color) * factor));
	}

	public static int scaleColorToDefaultBG(int color) {
		return scaleColor(color, BG_COLOR_SCALE_FACTOR, false);
	}
	
	
	public static void setTextMaybeHtml(TextView view, String text) {
		if (TextUtils.isEmpty(text)) {
			view.setText("");
			return;
		}
		if ((text.contains("<") && text.contains(">")) || REGEX_HTML_ESCAPE.matcher(text).find()) {
			view.setText(Html.fromHtml(text));
			view.setMovementMethod(LinkMovementMethod.getInstance());
		} else {
			view.setText(text);
		}
	}

}
