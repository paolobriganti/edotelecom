package io.edo.ui.imageutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.edo.db.local.beans.LContact;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.Constants;

public class ContactPhotoLoader extends ImageLoader{
	private FragmentActivity c;
	private ThisApp app;
	private Executor executor;

	public static ContactPhotoLoader getInstance(FragmentActivity fragmentActivity){
		ThisApp app = ThisApp.getInstance(fragmentActivity);
		if(app.contactPhotoLoader==null){
			app.contactPhotoLoader = new ContactPhotoLoader(fragmentActivity);
		}
		return app.contactPhotoLoader;
	}

	public ContactPhotoLoader(FragmentActivity fragmentActivity){
		super(fragmentActivity, Constants.CONTACTS_CACHE_DIR_NAME, getImageViewSideSize(fragmentActivity), getImageViewSideSize(fragmentActivity));
		this.c = fragmentActivity;
		this.app = ThisApp.getInstance(c);
		this.executor = Executors.newFixedThreadPool(3);
	}

	public static int getImageViewSideSize(Context c){
		return ((int) c.getResources().getDimension(R.dimen.contact_photo_size)*2);
	}

	@Override
	public Executor getExecutor() {
		return executor;
	}

	public void loadImage(LContact contact, ImageView imageView)
	{
		ImageData imageData = new ImageData(contact);
        loadDataToImageView(imageData, imageView);
	}

	@Override
	public String getKeyFromData(Object data) {
		ImageData imageData = (ImageData) data;
		return imageData.contact.getId();
	}

	@Override
	public Bitmap getLoadingImageFromData(Object data) {
        return BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_contact_nothumb);
	}

	@Override
	protected Bitmap processBitmap(Object data) {
		try {
			ImageData imageData = (ImageData) data;
			LContact contact = imageData.contact;

			if(contact!=null && !contact.hasNoThumbnail()){
				Bitmap bitmap = ContactPhoto.getPhotoFromDevice(c, contact);
				if(bitmap==null) {
					String thumbLink = ContactPhoto.getPhotoWebLink(mContext, contact);
					if (thumbLink != null)
						bitmap = processBitmapFromHttpUrl(thumbLink);
				}
				if(bitmap!=null) {
					bitmap = ContactPhoto.getCircularContactPhoto(c, bitmap, mImageHeight);
				}else{
					contact.setThumbnail(LContact.NO_THUMB);
//					new LContactDAO(c).update(contact);
				}
				return bitmap;

			}
		} catch (Throwable ex){

			ex.printStackTrace();
			if(ex instanceof OutOfMemoryError){
				clearCache();
				closeCacheInternal();
			}
		}
		return null;
	}


	@Override
	public void onBitmapProcessed(Object data, ImageView imageView, Drawable value) {
        setImage((ImageData) data, imageView, value);
	}

	public void setImage(ImageData imageData, ImageView imageView, Drawable drawable) {

        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

		if(drawable!=null){

			if(ASI.get_OS_SDK_Version()<ASI.ANDROID_5_0_SDK_VERSION){
				try {
					Bitmap bm = ContactPhoto.getCircularContactPhoto(c, ((BitmapDrawable) drawable).getBitmap(), mImageHeight);
					imageView.setImageBitmap(bm);
				}catch (OutOfMemoryError e){
					imageView.setImageDrawable(drawable);
				}
			}else {
				imageView.setImageDrawable(drawable);
			}

		}else{
			if(!imageData.contact.isGroup()){
				imageView.setImageResource(R.drawable.ic_contact_nothumb);
			}else{
				imageView.setImageResource(R.drawable.ic_group_nothumb);
			}
		}

		imageView.setBackgroundResource(android.R.color.transparent);
	}




	public class ImageData{
		public LContact contact;

		public ImageData(LContact contact) {
			this.contact = contact;
		}
	}

	public static Bitmap getBitmapFromDiskCache(Context c, String key){
		return getBitmapFromDiskCache(c,Constants.CONTACTS_CACHE_DIR_NAME,key);
	}
}
