package io.edo.ui.fragments;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.Serializable;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.LSpace;
import io.edo.edomobile.Act_FilePage;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.imageutils.FilePageLoader;

/**
 * Created by PaoloBriganti on 31/07/15.
 */
public class Frag_VideoPlayer extends Fragment{
    Act_FilePage c;
    ThisApp app;
    FilePageLoader filePageLoader;


    LFile currentFile;
    LSpace space;

    private ImageView iv_preview;
    private ImageView iv_play_overlay;
    private VideoView vv_filepage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        c = (Act_FilePage)getActivity();
        app = ThisApp.getInstance(c);

        if(c.filePageLoader==null){
            c.filePageLoader = FilePageLoader.getInstance(c);
        }
        filePageLoader = c.filePageLoader;

        if (getArguments()!=null && getArguments().containsKey(DBConstants.FILE)) {
            Serializable fileSerial = getArguments().getSerializable(DBConstants.FILE);
            if(fileSerial!=null) {
                currentFile = (LFile) fileSerial;
            }
        }

        if (getArguments()!=null && getArguments().containsKey(DBConstants.SPACE)) {
            Serializable spaceSerial = getArguments().getSerializable(DBConstants.SPACE);
            if (spaceSerial != null) {
                space = (LSpace) spaceSerial;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.frag_videoview, container, false);

        iv_preview = (ImageView) layout.findViewById(R.id.iv_preview);
        iv_play_overlay = (ImageView) layout.findViewById(R.id.iv_play_overlay);
        vv_filepage = (VideoView) layout.findViewById(R.id.vv_filepage);

        filePageLoader.loadImage(space, currentFile, iv_preview);

        iv_play_overlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!currentFile.isStored()) {
                    c.download(v);
                    return;
                }
                playPause();
            }
        });

        vv_filepage.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                iv_play_overlay.setVisibility(View.VISIBLE);
                c.showBars(true);
            }
        });

        vv_filepage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c.showHideBars();
            }
        });

        return layout;
    }


    public void playPause(){
        if (!vv_filepage.isPlaying()) {

            c.showBars(false);
            iv_play_overlay.setVisibility(View.GONE);
            iv_preview.setBackgroundColor(Color.BLACK);
            iv_preview.setImageResource(R.color.black);
            vv_filepage.setVideoURI(Uri.parse(currentFile.getLocalUri()));
            vv_filepage.setMediaController(new MediaController(c));
            vv_filepage.requestFocus();
            vv_filepage.setVisibility(View.VISIBLE);
            vv_filepage.start();
        } else {
            iv_play_overlay.setVisibility(View.VISIBLE);
            c.showBars(true);

            vv_filepage.pause();
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {

        } else {
            vv_filepage.pause();
        }
    }

}
