package io.edo.ui;

import android.content.Context;

import java.util.HashMap;

import io.edo.db.global.EdoActivityManager;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.eLog;

/**
 * Created by PaoloBriganti on 31/05/15.
 */
public class UIManager {

    private HashMap<String,UIBaseListener> listeners = new HashMap<>();
    private HashMap<String,LActivity> activities = new HashMap<>();

    public void insertResource(final LActivity a, Class<?> listenerClass){
        processUpdate(a, listenerClass, new ListenerFoundListener() {
            @Override
            public void onListenerWithSameResourceFound(UIBaseListener listener) {
                listener.onInsert(a.getContainer(), a.getResource());
            }
        });
    }

    public void updateResource(final LActivity a, Class<?> listenerClass){
        processUpdate(a, listenerClass, new ListenerFoundListener() {
            @Override
            public void onListenerWithSameResourceFound(UIBaseListener listener) {
                listener.onUpdate(a.getContainer(), a.getResource());
            }
        });
    }

    public void deleteResource(final LActivity a, Class<?> listenerClass){
        processUpdate(a, listenerClass, new ListenerFoundListener() {
            @Override
            public void onListenerWithSameResourceFound(UIBaseListener listener) {

                listener.onDelete(a.getContainer(), a.getResource());
            }
        });
    }

    public void syncResourcesList(final LActivity a, Class<?> listenerClass){
        processUpdate(a, listenerClass, new ListenerFoundListener() {
            @Override
            public void onListenerWithSameResourceFound(UIBaseListener listener) {
                listener.onSyncList(a);
            }
        });
    }

    public void uploadToCloud(final LActivity a){
        processUpdate(a, FilesUpdatesListener.class, new ListenerFoundListener() {
            @Override
            public void onListenerWithSameResourceFound(UIBaseListener listener) {
                ((FilesUpdatesListener)listener).onUploadToCloud(a.getContainer(), (LFile) a.getResource(), a.getStatus(), a.getProgressPercent());
            }
        });
    }

    public void downloadFromCloud(final LActivity a){
        processUpdate(a, FilesUpdatesListener.class, new ListenerFoundListener() {
            @Override
            public void onListenerWithSameResourceFound(UIBaseListener listener) {
                ((FilesUpdatesListener)listener).onDownloadFromCloud(a.getContainer(), (LFile) a.getResource(), a.getStatus(), a.getProgressPercent());
            }
        });
    }

    //*******************************
    //* BASE UPDATES LISTENER
    //*******************************

    UIBaseListener mListener;

    public interface UIBaseListener <Container, Resource>{
        void onInsert(Container container, Resource resource);
        void onUpdate(Container container, Resource resource);
        void onDelete(Container container, Resource resource);
        void onSyncList(LActivity a);
    }

    public void removeListener(String key){
        listeners.remove(key);
    }

    //*******************************
    //* ACTIVITY UPDATES LISTENER
    //*******************************

    public void addActivitiesUpdatesListener(String key, ActivitiesUpdatesListener listener) {
        listeners.put(key,listener);
    }

    public interface ActivitiesUpdatesListener extends UIBaseListener<EdoContainer, LActivity>{
    }


    //*******************************
    //* CONTACTS UPDATES LISTENER
    //*******************************

    public void addContactsUpdatesListener(String key, ContactsUpdatesListener listener) {

        listeners.put(key,listener);
    }

    public interface ContactsUpdatesListener extends UIBaseListener<EdoContainer, LContact>{}



    //*******************************
    //* FILES UPDATES LISTENER
    //*******************************

    public void addFilesUpdatesListener(String key, FilesUpdatesListener listener) {
        listeners.put(key,listener);
    }

    public interface FilesUpdatesListener extends UIBaseListener<EdoContainer, LFile>{
        void onUploadToCloud(EdoContainer container, LFile file,  int progressStatus, int progressPercent);
        void onDownloadFromCloud(EdoContainer container, LFile file,  int progressStatus, int progressPercent);
    }

    //*******************************
    //* SPACES UPDATES LISTENER
    //*******************************

    public void addSpacesUpdatesListener(String key, SpacesUpdatesListener listener) {
        listeners.put(key,listener);
    }

    public interface SpacesUpdatesListener extends UIBaseListener<EdoContainer, LSpace>{}



    //*******************************
    //* PROCESS UPDATE
    //*******************************
    public void processUpdate(LActivity a, Class<?> listenerClass, ListenerFoundListener listenerFound){
        if(listeners.size()>0){
            for(UIBaseListener listener: listeners.values()){

                if (listener instanceof ActivitiesUpdatesListener) {

                    eLog.i("EdoActivityMan", "ACTIVITY LISTENER FOUNDED");
                    if(a.canBeShownOnTimeline()) {
                        eLog.i("EdoActivityMan", "canBeShownOnTimeline");
                        listener.onInsert(a.getContainer(), a);
                    }else{
                        eLog.w("EdoActivityMan", "canBeShownOnTimeline NO");
                        if(a.getEvent() == LActivity.EVENT_SYNC_ALL){
                            listener.onSyncList(a);
                        }
                    }
                }else if (listenerClass.isInstance(listener)) {
                    eLog.i("EdoActivityMan", "LISTENER FOUNDED");
                    listenerFound.onListenerWithSameResourceFound(listener);
                }
            }
        }

        if (a.getResource() != null) {
            activities.put(a.getResourceId(), a);
        } else {
            activities.put(a.getId(), a);
        }

    }

    public interface ListenerFoundListener{
        void onListenerWithSameResourceFound(UIBaseListener listener);
    }


    //*******************************
    //* UPDATES
    //*******************************
    public void resendUIUpdatesFrom(Context c, long startMillis){
        if(activities.size()>0){
            EdoActivityManager manager = new EdoActivityManager(c);
            for(LActivity a:activities.values()){
                if(a.getTimeStamp()>=startMillis){
                    manager.processActivity(a);
                }
            }
        }
    }


    public void clearUpdates(){
        activities.clear();
    }


    //*******************************
    //* GET ACTIVITY
    //*******************************
    public static LActivity getActivityByResourceId(Context c, String resourceId){
        UIManager manager = ThisApp.getInstance(c).uiManager;
        return manager.getActivityByResourceId(resourceId);
    }
    public LActivity getActivityByResourceId(String resourceId){
        return activities.get(resourceId);
    }
    public LActivity removeActivityByResourceId(String resourceId){
        return activities.remove(resourceId);
    }

}
