package io.edo.ui;

import android.view.View;

import io.edo.db.local.beans.LFile;

public interface CommentsUI {

	void setComments(final LFile file, boolean setViews);

	void setCommentsViews();

	void postComment();

	View getLayout();
}
