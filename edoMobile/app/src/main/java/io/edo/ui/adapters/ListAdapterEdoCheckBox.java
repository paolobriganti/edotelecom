package io.edo.ui.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class ListAdapterEdoCheckBox extends BaseAdapter {
	private final Context context;
	private final ArrayList<String> titles;
	private ArrayList<Boolean>  chekeds = new ArrayList<Boolean>(); 
	private ArrayList<CheckBox> checkBoxs = new ArrayList<CheckBox>(); 

	public ListAdapterEdoCheckBox(Context context, ArrayList<String> titles) {
		this.context = context;
		this.titles = titles;
		
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.row_edo_checkbox, parent, false);
		CheckBox cb = (CheckBox) rowView.findViewById(R.id.cb);
		if(titles!=null && titles.size()>0 && JavaUtils.isNotEmpty(titles.get(position)))
			cb.setText(titles.get(position));
		else 
			cb.setText(" ");
		
		chekeds.add(cb.isChecked());
		
		cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				chekeds.set(position, isChecked);
			}
		});
		checkBoxs.add(cb);
		return rowView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public boolean isChecked(int position){
		return chekeds.get(position);
	}
	public void setChecked(int position, boolean checked){
		checkBoxs.get(position).setChecked(checked);
	}
} 