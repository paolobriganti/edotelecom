package io.edo.ui.adapters.timeline;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import io.edo.db.local.beans.LActivity;
import io.edo.edomobile.R;

/**
 * Created by PaoloBriganti on 05/06/15.
 */
public class BlankSheetViewHolder extends RecyclerView.ViewHolder implements TimelineBaseViewHolder {
    public View cardView;
    public View layout_container;
    public TextView tv_message;
//    public Button bt_ok;

    public BlankSheetViewHolder(View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.card_content);
        layout_container = itemView.findViewById(R.id.layout_container);
        tv_message = (TextView) itemView.findViewById(R.id.tv_message);
//        bt_ok = (Button) itemView.findViewById(R.id.bt_ok);
    }

    public View setViews(Context c, LActivity msg) {

        this.tv_message.setText(Html.fromHtml(msg.getMessage()));

        return this.cardView;
    }

    @Override
    public void setSelected(boolean isSelected){
        this.layout_container.setBackgroundResource(!isSelected?R.drawable.bg_card_white:R.drawable.bg_card_blue_light);
    }
}



