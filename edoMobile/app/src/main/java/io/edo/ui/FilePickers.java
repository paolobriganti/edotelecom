package io.edo.ui;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.paolobriganti.android.AUtils;
import com.paolobriganti.utils.FileInfo;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import io.edo.db.global.uibeans.ContactFileUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LSpaceDAO;
import io.edo.db.local.beans.support.LocalFiles;
import io.edo.edomobile.Act_AudioRecorder;
import io.edo.edomobile.Act_FilePicker;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoDialogGridView.OnGridItemClickListener;
import io.edo.ui.EdoDialogOne.OnPositiveClickListener;
import io.edo.ui.adapters.GridAdapterFilePickers;
import io.edo.ui.adapters.GridAdapterOptions;
import io.edo.utilities.Constants;
import io.edo.views.SlidingUpPanelLayout;

public class FilePickers {
	private Activity c;
	private EdoContainer container;
	private LSpace space;
	private String filePath;
	private ThisApp app;
	private EdoProgressDialog loading;

	public int prefixId = 0;

	public static final int CONDITION_NO_FOLDERS = 0;

	public FilePickers(Activity c) {
		super();
		this.c = c;
		this.app = ThisApp.getInstance(c);
		this.loading = new EdoProgressDialog(c,R.string.loading,R.string.loading);

//		if(fragment!=null){
//			prefixId = new Random().nextInt(1000);
//		}

	}



	public void pickersSlider(final EdoContainer container, final LSpace space, final GridView gv_pickers, final SlidingUpPanelLayout slidingPickers, final int condition){
		if(container==null || space==null)
			return;

		this.container = container;
		this.space = space;


		final FileOptions fileOptions = new FileOptions(c, container);
		final LSpaceDAO spaceDAO = new LSpaceDAO(c);

		final LService service = space.getService(c, false);
		final String folderPath = space.getPhysicalPath(c,container);

		final ArrayList<Integer> titles = new ArrayList<Integer>();
		final ArrayList<Integer> images = new ArrayList<Integer>();

		titles.add(R.string.photo);
		images.add(R.drawable.bt_picker_camera);

		titles.add(R.string.gallery);
		images.add(R.drawable.bt_picker_gallery);

		titles.add(R.string.video);
		images.add(R.drawable.bt_picker_video);

		titles.add(R.string.audio);
		images.add(R.drawable.bt_picker_audio);

		if (service != null) {
			titles.add(service.getCommercialCloudNameResource(c));
			images.add(service.getPickerCloudIcon(c));
		}

		titles.add(R.string.files);
		images.add(R.drawable.bt_picker_files);

		titles.add(R.string.Snippet);
		images.add(R.drawable.bt_picker_snippets);


		if(condition != CONDITION_NO_FOLDERS) {
			titles.add(R.string.folder);
			images.add(R.drawable.bt_picker_folder);
		}

		final GridAdapterFilePickers adapter = new GridAdapterFilePickers(c,titles,images);


		c.runOnUiThread(new Runnable() {public void run() {

			gv_pickers.setAdapter(adapter);
			gv_pickers.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
					fileTypePickersSwitcher(titles.get(position), container, space, folderPath, fileOptions);
					slidingPickers.collapsePane();

				}
			});
			gv_pickers.getViewTreeObserver().addOnGlobalLayoutListener(
					new ViewTreeObserver.OnGlobalLayoutListener() {
						@Override
						public void onGlobalLayout() {
							slidingPickers.expandPane();
							gv_pickers.getViewTreeObserver().removeGlobalOnLayoutListener(this);
						}
					});


		}});


		if(!slidingPickers.isExpanded())
			slidingPickers.expandPane();
	}

	public void fileTypePickersSwitcher(int position, final EdoContainer container, LSpace space, String folderPath, final FileOptions fileOptions) {
		if(container==null || space==null)
			return;

		this.container = container;
		this.space = space;

		LService service = space.getService(c, false);

		switch (position) {
			case R.string.photo:
				cameraImagePicker(folderPath);

				break;

			case R.string.gallery:
				edoGallery(service);

				break;

			case R.string.video:
				cameraVideoPicker();

				break;

			case R.string.audio:
				audioPicker(container,folderPath);

				break;

			case R.string.googleDrive:
				if (service != null && service.getName().equals(LService.SERVICE_NAME_GOOGLE)) {
					driveFilesPicker(service);

				}

				break;

			case R.string.files:
				filesPicker(service);

				break;

			case R.string.Snippet:
				snippetEditor(container,space,null);

				break;

			case R.string.folder:
				fileOptions.addFolderDialog(loading, space);

				break;

			default:
				break;
		}

	}





	public String getFilePath() {
		return filePath;
	}

	public void audioPicker(final EdoContainer container, final String folderPath){
		if(container==null)
			return;

		this.container = container;

		final EdoDialogGridView dialog = new EdoDialogGridView(c);
		dialog.setNumColumns(2);
		dialog.setTitle(R.string.completeActionUsing);
		ArrayList<String> titles = new ArrayList<String>();
		titles.add(c.getResources().getString(R.string.recordAudio));
		titles.add(c.getResources().getString(R.string.selectAudioTrack));
		ArrayList<Integer> images = new ArrayList<Integer>();
		images.add(R.drawable.ic_audiorecorder);
		images.add(R.drawable.ic_tracks);
		GridAdapterOptions adapter = new GridAdapterOptions(c, titles, images);
		dialog.setAdapter(adapter);
		dialog.setOnGridItemClickListener(new OnGridItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				dialog.cancel();


				if(pos==0){
					Intent audioRecorder = new Intent(c, Act_AudioRecorder.class);
					audioRecorder.putExtra(Constants.EXTRA_FILE_PATH, folderPath+File.separator+FileInfo.getFileNameWithTime("Record_"));
					startActivityForResult(audioRecorder, Constants.AUDIO_REC_PICKER_RESULT);

				}else if(pos==1){
					try{
						Intent mediaChooser = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
						mediaChooser.setType("audio/*");
						startActivityForResult(mediaChooser, Constants.AUDIO_PICKER_RESULT);

						return;
					}catch(ActivityNotFoundException e){}

					try{
						Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
						mediaChooser.setType("audio/*");
						startActivityForResult(mediaChooser, Constants.AUDIO_PICKER_RESULT);
					}catch(ActivityNotFoundException e){
						EdoUI.showToast(c, R.string.noAppCanOpenThisTypeOfFiles, null, Toast.LENGTH_LONG);
					}
				}

			}
		});
		dialog.setPositiveButtonText(R.string.cancel);
		dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.show();



	}


	public void driveFilesPicker(LService service){
		Intent intent = new Intent(c, Act_FilePicker.class);
		intent.putExtra(DBConstants.SERVICE_ID, service!=null? service.getId():null);
		intent.putExtra(Act_FilePicker.EXTRA_SOURCE, Act_FilePicker.EXTRA_SOURCE_GOOGLE_DRIVE);
		startActivityForResult(intent, Constants.EDO_GOOGLE_DRIVE_FILES_PICKER_RESULT);
	}

	public void filesPicker(LService service){
		// Create a new Intent for the file picker activity
		Intent intent = new Intent(c, Act_FilePicker.class);
		intent.putExtra(DBConstants.SERVICE_ID,  service!=null? service.getId():null);
		intent.putExtra(Act_FilePicker.EXTRA_SOURCE, Act_FilePicker.EXTRA_SOURCE_LOCAL_DISK);

		// Set the initial directory to be the sdcard
		//intent.putExtra(FilePickerActivity.EXTRA_FILE_PATH, Environment.getExternalStorageDirectory());

		// Show hidden files
		//intent.putExtra(FilePickerActivity.EXTRA_SHOW_HIdDEN_FILES, true);

		// Only make .png files visible
		//ArrayList<String> extensions = new ArrayList<String>();
		//extensions.add(".png");
		//intent.putExtra(FilePickerActivity.EXTRA_ACCEPTED_FILE_EXTENSIONS, extensions);

		// Start the activity
		startActivityForResult(intent, Constants.EDO_FILES_PICKER_RESULT);
	}

	public void edoGallery(LService service){
		// Create a new Intent for the file picker activity
		Intent intent = new Intent(c, Act_FilePicker.class);
		intent.putExtra(DBConstants.SERVICE_ID, service != null ? service.getId() : null);
		intent.putExtra(Act_FilePicker.EXTRA_SOURCE, Act_FilePicker.EXTRA_SOURCE_LOCAL_DISK_GALLERY);

		// Set the initial directory to be the sdcard
		//intent.putExtra(FilePickerActivity.EXTRA_FILE_PATH, Environment.getExternalStorageDirectory());

		// Show hidden files
		//intent.putExtra(FilePickerActivity.EXTRA_SHOW_HIdDEN_FILES, true);

		ArrayList<String> types = new ArrayList<String>();
		types.add(FileInfo.TYPE_RASTER);
		types.add(FileInfo.TYPE_VIDEO);
		intent.putExtra(Act_FilePicker.EXTRA_ACCEPTED_FILE_TYPES, types);

		// Start the activity
		startActivityForResult(intent, Constants.EDO_GALLERY_PICKER_RESULT);
	}








	public void cameraImagePicker(String folderPath){
		try{
			File file = new File(folderPath+File.separator+FileInfo.getFileNameWithTime("Photo_")+".jpg");
			Uri imageUri = Uri.fromFile(file);
			filePath = file.toString();
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
			startActivityForResult(intent, Constants.CAMERA_PHOTO_PICKER_RESULT);
			return;
		}catch(ActivityNotFoundException e){
		}

		imagePicker();
	}

	public void imagePicker(){
		try{
			Intent mediaChooser = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			mediaChooser.setType("image/*");
			startActivityForResult(mediaChooser, Constants.IMAGE_PICKER_RESULT);
			return;
		}catch(ActivityNotFoundException e){}

		try{
			Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
			mediaChooser.setType("image/*");
			startActivityForResult(mediaChooser, Constants.IMAGE_PICKER_RESULT);
		}catch(ActivityNotFoundException e){
			EdoUI.showToast(c, R.string.noAppCanOpenThisTypeOfFiles, null, Toast.LENGTH_LONG);
		}

	}

	public void cameraVideoPicker(){

		try{
			Intent cameraIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
			startActivityForResult(cameraIntent, Constants.CAMERA_VIDEO_PICKER_RESULT);
			return;
		}catch(ActivityNotFoundException e){
		}

		videoPicker();
	}


	public void videoPicker(){
		try{
			Intent mediaChooser = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
			mediaChooser.setType("video/*");
			startActivityForResult(mediaChooser, Constants.VIDEO_PICKER_RESULT);
			return;
		}catch(ActivityNotFoundException e){}

		try{
			Intent mediaChooser = new Intent(Intent.ACTION_GET_CONTENT);
			mediaChooser.setType("video/*");
			startActivityForResult(mediaChooser, Constants.VIDEO_PICKER_RESULT);
		}catch(ActivityNotFoundException e){
			EdoUI.showToast(c, R.string.noAppCanOpenThisTypeOfFiles, null, Toast.LENGTH_LONG);
		}
	}


	public void snippetEditor(final EdoContainer container, LSpace space, String text){
		if(container==null || space==null)
			return;

		this.container = container;
		this.space = space;

		startActivityForResult(EdoIntentManager.getSnippetIntent(c,container,space,null,text),Constants.EDO_FILES_PICKER_RESULT);
	}


	public LSpace getSpace() {
		return space;
	}

	public void setSpace(LSpace space) {
		this.space = space;
	}

	public void startActivityForResult(Intent intent, int requestCode){
		requestCode = prefixId+requestCode;
//		if(fragment!=null)
//			fragment.startActivityForResult(intent, requestCode);
//		else
		if(c!=null)
			c.startActivityForResult(intent, requestCode);
	}




	public void onActivityResult(int requestCode, int resultCode, final Intent intent)
	{

		if(container==null || space==null)
			return;

		if(resultCode == Activity.RESULT_OK) {

			requestCode = requestCode-prefixId;

			switch(requestCode) {
				case Constants.EDO_FILES_PICKER_RESULT:
				case Constants.EDO_GALLERY_PICKER_RESULT:
					if(intent.hasExtra(Constants.EXTRA_FILES)) {

						Serializable filesSerial = intent.getSerializableExtra(Constants.EXTRA_FILES);
						if(filesSerial!=null){
							LocalFiles efiles = (LocalFiles) filesSerial;
							addFilesToSpace(container, space, efiles);


						}

					}
					break;

				case Constants.EDO_GOOGLE_DRIVE_FILES_PICKER_RESULT:
					if(intent.hasExtra(Constants.EXTRA_FILES)) {

						Serializable filesSerial = intent.getSerializableExtra(Constants.EXTRA_FILES);
						if(filesSerial!=null){
							LocalFiles efiles = (LocalFiles) filesSerial;
							addFilesToSpace(container, space, efiles);

						}

					}
					break;

				case Constants.IMAGE_PICKER_RESULT:
				case Constants.VIDEO_PICKER_RESULT:
				case Constants.AUDIO_PICKER_RESULT:
					if (intent != null && intent.getData()!=null) {
						Uri uri = intent.getData();
						String filePath = FileInfo.getRealPathFromURI(c, uri);
						addFileToSpace(container, space, filePath);

					}
					break;

				case Constants.AUDIO_REC_PICKER_RESULT:
					if (intent != null && intent.hasExtra(Constants.EXTRA_FILE_PATH)) {
						String filePath = intent.getStringExtra(Constants.EXTRA_FILE_PATH);
						addFileToSpace(container, space, filePath);

					}
					break;

				case Constants.CAMERA_PHOTO_PICKER_RESULT:
					String filePath = getFilePath();
					if(filePath!=null){
						addFileToSpace(container, space, filePath);

					}
					break;

				case Constants.CAMERA_VIDEO_PICKER_RESULT:
					if (intent != null && intent.getData()!=null) {
						Uri videoUri = intent.getData();
						String filePath2 = FileInfo.getRealPathFromURI(c, videoUri);
						if(filePath2!=null){
							addFileToSpace(container, space, filePath2);

						}
					}
					break;



				default:
					break;
			}


		}

	}










	public void addFileToSpace(final EdoContainer container, final LSpace space, final String filePath){

		if(filePath!=null && space!=null && space.getCurrentFolder()!=null){

			LFile efile = LFile.newLocalFile(null, null, filePath, null, app.getEdoUserId());

			LocalFiles efiles = new LocalFiles(efile);

			addFilesToSpace(container, space, efiles);

		}
	}

	public void addFilesToSpaceQuestion(final EdoContainer container, final LSpace space, final LocalFiles efiles){
		if(efiles!=null && space!=null && space.getCurrentFolder()!=null){
			LService service = space.getService(c, false);
			String message = c.getResources().getString(R.string.doYouWantToUpload)+" "+service.getCommercialCloudName(c)+"?\n\n"+
					efiles.getFiles().size()+" "+c.getResources().getString(R.string.filesSelected);
			final EdoDialog dialog = new EdoDialog(c);
			dialog.setTitle(R.string.cloudUpload);
			dialog.setText(message);
			dialog.setPositiveButtonText(R.string.yes);
			dialog.setNegativeButtonText(R.string.no);
			dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

				@Override
				public void onClick(View arg0) {
					dialog.cancel();
					addFilesToSpace(container, space, efiles);
				}
			});
			dialog.setOnNegativeClickListener(new EdoDialog.OnNegativeClickListener() {

				@Override
				public void onClick(View arg0) {
					dialog.cancel();
				}
			});
			dialog.show();
		}
	}

	public void addFilesToSpace(final EdoContainer container, final LSpace space, final LocalFiles efiles){
		AUtils.executeAsyncTask(new AsyncAddingToSpace(container, space, efiles), "");
	}

	class AsyncAddingToSpace extends AsyncTask<String, String, String> {
		LocalFiles efiles;
		LSpace space;
		EdoContainer container;


		public AsyncAddingToSpace(EdoContainer container, LSpace space, LocalFiles efiles) {
			super();
			this.container = container;
			this.space = space;
			this.efiles = efiles;

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... params) {

			c.runOnUiThread(new Runnable() {
				public void run() {
					String spaceName = space.getCurrentFolder().getName();
					loading.setTitle(c.getResources().getString(R.string.loading));
					loading.setSubTitle(c.getResources().getString(R.string.sharingFiles));
					loading.show();
				}
			});



			if(efiles!=null && efiles.getFiles()!=null && efiles.getFiles().size()>0
					&& space!=null && space.getCurrentFolder()!=null){


				ContactFileUIDAO cfDAO = new ContactFileUIDAO(c);

				ArrayList<LFile> files = efiles.getFiles();

				if(files.size()>1){

					OnProgressChangeListener progressChangeListener = new OnProgressChangeListener() {

						@Override
						public void onProgressChange(int current, int total, Object object,
													 String info) {
							loading.setBarProgress(current, total);
						}
					};

					cfDAO.addAll((LContact) container, space, files);

					c.runOnUiThread(new Runnable() {public void run() {
						loading.dismiss();
					}});

				}else if(files.size()==1){
					LFile file = files.get(0);

					cfDAO.add((LContact) container, space, file);

					c.runOnUiThread(new Runnable() {public void run() {
						loading.dismiss();
					}});
				}
			}else{

				c.runOnUiThread(new Runnable() {public void run() {
					loading.dismiss();
				}});
			}


			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			this.cancel(true);

		}
	}
}
