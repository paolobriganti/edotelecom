package io.edo.ui.adapters.files;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import io.edo.db.local.beans.LActivity;
import io.edo.edomobile.R;
import io.edo.views.ArcProgress;

/**
 * Created by PaoloBriganti on 02/07/15.
 */
public class FileOverlayHolder extends RecyclerView.ViewHolder{
    public View overlay;
    public ImageView iv_overlay;
    public ArcProgress arc_overlay;

    public FileOverlayHolder(View itemView) {
        super(itemView);

        overlay = itemView.findViewById(R.id.fl_overlay);
        iv_overlay = (ImageView)itemView.findViewById(R.id.iv_overlay);
        arc_overlay = (ArcProgress) itemView.findViewById(R.id.arc_overlay);
    }

    public void setSelected(boolean selected){
        overlay.setVisibility(selected ? View.VISIBLE : View.GONE);
        iv_overlay.setVisibility(selected ? View.VISIBLE : View.GONE);
        iv_overlay.setImageResource(R.drawable.ic_action_accept_w);
        arc_overlay.setVisibility(View.GONE);
    }

    public void setProgress(int progressStatus, int progressPercent, String text){
        iv_overlay.setVisibility(View.GONE);
        if(progressStatus == LActivity.STATUS_PENDING || progressStatus == LActivity.STATUS_STARTED) {
            overlay.setVisibility(View.VISIBLE);
            iv_overlay.setVisibility(View.VISIBLE);
            arc_overlay.setVisibility(View.GONE);
            iv_overlay.setImageResource(R.drawable.ic_action_time_w);
        }else if(progressStatus == LActivity.STATUS_IN_PROGRESS){
            overlay.setVisibility(View.VISIBLE);
            iv_overlay.setVisibility(View.GONE);
            arc_overlay.setVisibility(View.VISIBLE);
            arc_overlay.setProgress(progressPercent);
            arc_overlay.setBottomText(text);
        }else if(progressStatus == LActivity.STATUS_ERROR){
            overlay.setVisibility(View.VISIBLE);
            iv_overlay.setVisibility(View.VISIBLE);
            arc_overlay.setVisibility(View.GONE);
            iv_overlay.setImageResource(R.drawable.ic_action_sync);
        }else if(progressStatus == LActivity.STATUS_COMPLETE || progressStatus == LActivity.STATUS_STOP){
            overlay.setVisibility(View.GONE);
            arc_overlay.setVisibility(View.GONE);
            iv_overlay.setVisibility(View.GONE);
        }
    }
}
