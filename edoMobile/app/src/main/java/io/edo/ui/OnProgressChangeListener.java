package io.edo.ui;


public interface OnProgressChangeListener {
	void onProgressChange(int current, int total, Object object, String info);
}
