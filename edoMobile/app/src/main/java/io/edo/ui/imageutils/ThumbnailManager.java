package io.edo.ui.imageutils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.webkit.WebView;

import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.WebUtils;
import com.paolobriganti.utils.imageLoader.ImageWorker;

import java.io.File;

import io.edo.api.ApiFile;
import io.edo.api.ApiResponse;
import io.edo.api.linkparser.LinkParser;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LService;
import io.edo.db.web.request.EdoRequest;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.Constants;

public class ThumbnailManager {


	//*********************************************************************
	//*		CREATE THUMB
	//*********************************************************************
//	public static Bitmap getThumbnail(Context c, LSpace tab, LFile efile, int thumbSize){
//		try{
//
//
//
//		}catch (OutOfMemoryError e){
//		}
//
//		return null;
//	}


	public static String getThumbnailWebLink(Context c, LFile efile, int heightSizePx){

		if(JavaUtils.isWebUrl(efile.getThumbnail())){

			return efile.getThumbnail();

		}else if(efile.isSpace()){
			String iconName = efile.getThumbnail()!=null?efile.getThumbnail():"files";
			return EdoRequest.URL_SPACE_THUMBNAIL.replace(Constants.PH_SPACEICONNAME,iconName);

		}else if(efile.isYoutubeLink()){

			if(WebUtils.isYoutubeUrl(efile.getWebUri())){
				return WebUtils.getYoutubeVideoThumbnailUrl(efile.getWebUri());
			}

		}else if(efile.isWebLink()){

			LFile linkFile = LinkParser.getThumbnailAndNameOfWebLink(c, efile.getWebUri());
			if(linkFile!=null){
				efile.setName(linkFile.getName());
				efile.setThumbnail(linkFile.getThumbnail());

				return linkFile.getThumbnail();
			}

//		}else if(efile.isWebLink()){
//
//			LFile linkFile = LinkParser.getThumbnailAndNameOfWebLink(c, efile.getWebUri());
//			if(linkFile!=null){
//				efile.setName(linkFile.getName());
//				efile.setThumbnail(linkFile.getThumbnail());
//
//				return linkFile.getThumbnail();
//			}

		}else if(efile.isInCloud() && efile.canHaveACloudPreview()) {
				LService service = efile.getService(c);

				if(service!=null) {
					ApiResponse response = ApiFile.getThumbnailUrl(c, service, efile, heightSizePx);
					if (response != null) {
						return (String) response.result;
					}
				}
		}

		return null;
	}


	public static Bitmap getSnippetPreview(Context c, LFile snippet, int heightSizePx){
		if(snippet==null)
			return null;

		int size = heightSizePx/10;

		return drawText(snippet.getContent(), heightSizePx, size, snippet.getThumbnailColor(c,R.color.white));
	}

	public static Bitmap drawText(String text, int textWidth, int textSize, int backgroundColor) {

		int pad = textWidth/6;

		// Get text dimensions
		TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG
				| Paint.LINEAR_TEXT_FLAG);
		textPaint.setStyle(Paint.Style.FILL);
		textPaint.setColor(Color.BLACK);
		textPaint.setTextSize(textSize);
		StaticLayout mTextLayout = new StaticLayout(text, textPaint,
				textWidth-pad, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);

		// Create bitmap and canvas to draw to
		Bitmap b = Bitmap.createBitmap(textWidth, textWidth, Bitmap.Config.RGB_565);
		Canvas c = new Canvas(b);

		// Draw background
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG
				| Paint.LINEAR_TEXT_FLAG);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(backgroundColor);
		c.drawPaint(paint);

		// Draw text
		c.save();
		c.translate((pad/2), (pad/2));
		mTextLayout.draw(c);
		c.restore();

		return b;
	}





	//*********************************************************************
	//*		FILES
	//*********************************************************************

	public static void removeThumbanilFromMemory(Context c, String key){
		removeThumbanilFromMemCache(c, key);
		removeThumbanilFromDisk(c, key);
	}

	private static void removeThumbanilFromMemCache(Context c, String key){
		ThisApp app = ThisApp.getInstance(c);
		if(app.thumbnailFileLoader!=null)app.thumbnailFileLoader.removeFromMemCache(c,key);
		if(app.thumbnailActivityLoader!=null)app.thumbnailActivityLoader.removeFromMemCache(c,key);
		if(app.thumbnailSpaceLoader!=null)app.thumbnailSpaceLoader.removeFromMemCache(c,key);
		if(app.contactPhotoLoader!=null)app.contactPhotoLoader.removeFromMemCache(c,key);
		if(app.filePageLoader!=null)app.filePageLoader.removeFromMemCache(c,key);
	}

	private static void removeThumbanilFromDisk(Context c, String key){
		ImageWorker.removeFromDiskCache(c, Constants.CONTACTS_CACHE_DIR_NAME, key);
		ImageWorker.removeFromDiskCache(c, Constants.THUMB_CACHE_DIR_NAME, key);
		ImageWorker.removeFromDiskCache(c, Constants.IMAGE_CACHE_DIR_NAME, key);
	}




	//*********************************************************************
	//*		RESOURCES
	//*********************************************************************

	public static Integer getLoadingRes(Context c, LFile efile){
		return getLoadingRes(c, efile.getvType());
	}
	public static Integer getLoadingRes(Context c, String vType){
		if(JavaUtils.isNotEmpty(vType)){
			if(vType.equals(FileInfo.TYPE_FOLDER))
				return (R.drawable.file_folder_i);

			int id = c.getResources().getIdentifier("file_"+vType.toLowerCase()+"_loading", "drawable", c.getPackageName());
			if(id!=0)
				return id;
		}
		return R.drawable.file_misc_loading;
	}

	public static Integer getNoPreviewRes(Context c, LFile efile){
		return getNoPreviewRes(c, efile.getvType(), efile.isUnread());
	}

	public static Integer getNoPreviewRes(Context c, String vType, boolean isUnread){
		if(JavaUtils.isNotEmpty(vType)){
			if(vType.equals(FileInfo.TYPE_FOLDER))
				return (!isUnread?R.drawable.file_folder_big:R.drawable.file_folder_big_unread);

			if(vType.equals(LFile.FILE_TYPE_SNIPPET))
				return R.drawable.ic_action_notes_w;


			int id = c.getResources().getIdentifier("file_"+vType.toLowerCase()+"_big", "drawable", c.getPackageName());
			if(id!=0)
				return id;
		}
		return R.drawable.file_misc_big;
	}

	public static Integer getColorRes(Context c, String vType){
		if(JavaUtils.isNotEmpty(vType)){
			int id = c.getResources().getIdentifier("file_"+vType.toUpperCase(), "color", c.getPackageName());
			if(id!=0)
				return id;
		}
		return R.color.file_UNKNOWN;
	}



	public static Integer getIconRes(Context c, LFile efile){
		if(efile.isFolder())
			return (!efile.isUnread())?R.drawable.file_folder_i:R.drawable.file_folder_i_unread;

		if(efile.isSnippet())
			return R.drawable.ic_action_notes_w;

		String type = efile.getvType();
		return getIconRes(c, type);
	}

	public static Integer getIconRes(Context c, File file){
		if(file.isDirectory())
			return R.drawable.file_folder_i;



		String type = FileInfo.getTypeByPath(file.getPath());
		return getIconRes(c, type);
	}

	public static Integer getIconRes(Context c, String vType){
		if(JavaUtils.isNotEmpty(vType)){
			int id = c.getResources().getIdentifier("file_"+vType.toLowerCase(), "drawable", c.getPackageName());
			if(id!=0)
				return id;
		}
		return R.drawable.file_misc;
	}


	public static Integer getIconResFromPath(Context c, String filePath){
		String type = FileInfo.getTypeByPath(filePath);
		return getIconRes(c, type);
	}


	public static Integer getFolderIconResFromName(Context c, String folderName){
		if(JavaUtils.isNotEmpty(folderName)){
			int id = c.getResources().getIdentifier("file_folder_"+folderName.toLowerCase(), "drawable", c.getPackageName());
			if(id!=0)
				return id;
		}
		return R.drawable.file_folder;
	}

















	//*********************************************************************
	//*		BITMAP THUMBS
	//*********************************************************************


	public static Bitmap getVideoPreviewOfLocalFile(Context c, String localUri, int previewSize){
		int vType = MediaStore.Images.Thumbnails.FULL_SCREEN_KIND;
		if(previewSize<192){
			vType = MediaStore.Images.Thumbnails.MICRO_KIND;
		}else if(previewSize<512){
			vType = MediaStore.Images.Thumbnails.MINI_KIND;
		}
		return ThumbnailUtils.createVideoThumbnail(localUri, vType);
	}



	public static Bitmap getThumbnailFromWebView(Activity a, WebView wv){
		wv.buildDrawingCache();

		Bitmap bitmap = wv.getDrawingCache();
		if(bitmap!=null){
			int width = (int)(((float)bitmap.getWidth())/3F);
			int height = (int)(((float)bitmap.getHeight())/3F);
			bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
		}
		return bitmap;
	}







}
