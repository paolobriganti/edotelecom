package io.edo.ui.adapters.filepage;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.LSpace;
import io.edo.edomobile.R;
import io.edo.ui.adapters.files.FileOverlayHolder;
import io.edo.ui.fragments.Frag_AudioPlayer;
import io.edo.ui.fragments.Frag_Snippet;
import io.edo.ui.fragments.Frag_VideoPlayer;
import io.edo.views.TouchImageView;

/**
 * Created by PaoloBriganti on 22/07/15.
 */
public class FilePageHolder extends FileOverlayHolder {
    public ImageView imageView;
    public ProgressBar pb_loader;
    public FrameLayout fp_file_fragment;

    public FilePageHolder(View itemView) {
        super(itemView);

        imageView = (TouchImageView) itemView.findViewById(R.id.imageView);
        pb_loader = (ProgressBar) itemView.findViewById(R.id.pb_loader);
        fp_file_fragment = (FrameLayout) itemView.findViewById(R.id.fp_file_fragment);
    }

    public void showLoader(){
        pb_loader.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);
        fp_file_fragment.setVisibility(View.GONE);
    }

    public void showImageView(){
        imageView.setVisibility(View.VISIBLE);
        pb_loader.setVisibility(View.GONE);
        fp_file_fragment.setVisibility(View.GONE);
    }

    public void showSnippetView(LFile snippet, FragmentManager fm){
        imageView.setVisibility(View.GONE);
        pb_loader.setVisibility(View.GONE);
        fp_file_fragment.setVisibility(View.VISIBLE);

        Bundle arguments1 = new Bundle();
        arguments1.putSerializable(DBConstants.FILE, snippet);
        final Frag_Snippet fragment = new Frag_Snippet();
        fragment.setArguments(arguments1);
        fm.beginTransaction()
                .add(R.id.fp_file_fragment, fragment)
                .commit();
    }

    public void showAudioView(LFile file, FragmentManager fm){
        imageView.setVisibility(View.GONE);
        pb_loader.setVisibility(View.GONE);
        fp_file_fragment.setVisibility(View.VISIBLE);

        Bundle arguments1 = new Bundle();
        arguments1.putSerializable(DBConstants.FILE, file);
        final Frag_AudioPlayer fragment = new Frag_AudioPlayer();
        fragment.setArguments(arguments1);
        fm.beginTransaction()
                .add(R.id.fp_file_fragment, fragment)
                .commit();
    }

    public void showVideoView(LSpace space, LFile file, FragmentManager fm){
        imageView.setVisibility(View.GONE);
        pb_loader.setVisibility(View.GONE);
        fp_file_fragment.setVisibility(View.VISIBLE);

        Bundle arguments1 = new Bundle();
        arguments1.putSerializable(DBConstants.SPACE, space);
        arguments1.putSerializable(DBConstants.FILE, file);
        final Frag_VideoPlayer fragment = new Frag_VideoPlayer();
        fragment.setArguments(arguments1);
        fm.beginTransaction()
                .add(R.id.fp_file_fragment, fragment)
                .commit();
    }

}
