package io.edo.ui;

import android.content.Context;
import android.content.Intent;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.edomobile.Act_03_Settings;
import io.edo.edomobile.Act_Comments;
import io.edo.edomobile.Act_ContactActiveList;
import io.edo.edomobile.Act_ContactFileFilter;
import io.edo.edomobile.Act_FilePage;
import io.edo.edomobile.Act_Snippet;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.Constants;

public class EdoIntentManager {

//	public static Intent getPushMsgContactsIntent(Context c, LActivity a){
//
//		a.loadExtraFromDB(c,false);
//
//		EdoContainer container = a.getContainer();
//		if(!container.isContactOrGroup())
//			return null;
//		LContact contactContainer = (LContact) container;
//
//		if(contactContainer!=null){
//
//			int resourceType = a.getResourceType();
//			int event = a.getEvent();
//
//			if(resourceType == LActivity.RESOURCE_FILE){
//
//				LFile file = a.getFileResource();
//
//				if(file!=null){
//
//					LSpace tab = a.getSpace();
//
//					boolean hasNoteEvent = event == LActivity.EVENT_COMMENT_INSERT ||
//							event == LActivity.EVENT_COMMENT_UPDATED;
//
//
//					if(event != LActivity.EVENT_DELETE){
//
//						if(!file.isSpace() && !file.isFolder())
//							return getFilePageIntent(c, contactContainer, tab, file, null);
//						else if(file.isFolder()){
//							tab.setCurrentFolder(file);
//							return getContactsIntent(c, contactContainer);
//						}else{
//							return getContactsIntent(c, contactContainer);
//						}
//
//
//					}else{
//
//						return getContactsIntent(c, contactContainer);
//					}
//				}
//
//			}else if(resourceType == LActivity.RESOURCE_GROUP){
//				return getContactsIntent(c, contactContainer);
//			}else if(resourceType == LActivity.RESOURCE_MEMBER){
//				return getContactsIntent(c, contactContainer);
//			}else if(event == LActivity.EVENT_MESSAGE){
//				return getChatIntent(c, contactContainer);
//			}
//
//		}
//		return null;
//	}




	public static Intent getPersonalIntent(Context c){
		final Intent show = new Intent(c, Act_ContactActiveList.class);
		show.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		show.putExtra(DBConstants.CONTAINER, ThisApp.getInstance(c).getUserContact());
		return show;
	}

	public static Intent getContactsIntent(Context c, LContact contact){
		final Intent show = new Intent(c, Act_ContactActiveList.class);
		show.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				Intent.FLAG_ACTIVITY_SINGLE_TOP);
		show.putExtra(DBConstants.CONTAINER, contact);
		return show;
	}

	public static Intent getContactsIntent(Context c, LContact contact, LSpace spaceWithCurrentFolder){
		final Intent show = new Intent(c, Act_ContactActiveList.class);
		show.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				Intent.FLAG_ACTIVITY_SINGLE_TOP);
		show.putExtra(DBConstants.CONTAINER, contact);
		show.putExtra(DBConstants.SPACE, spaceWithCurrentFolder);
		return show;
	}

//	public static Intent getChatIntent(Context c, LContact contact){
//		final Intent show = new Intent(c, Act_ContactActiveList.class);
//		show.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//		show.putExtra(DBConstants.CONTAINER, contact);
//		return show;
//	}

	public static Intent getSettingsIntent(Context c){
		final Intent show = new Intent(c, Act_03_Settings.class);
		show.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		return show;
	}

	public static Intent getFileIntent(Context c, LFile file){
		if(file.isFolder()){
			EdoContainer container = new LFileDAO(c).getContainerOf(file);
			if(file!=null && container!=null){
				LSpace space = file.getSpace(c);
				if(space!=null){
					space.setCurrentFolder(file);
					return getContactsIntent(c, (LContact)container, space);
				}
			}
		}
		return getFilePageIntent(c, null, null, file, null);
	}


	public static Intent getFilePageIntent(Context c, EdoContainer container, LSpace space, LFile file, TransitionInfos transitionInfo){

		if(transitionInfo == null && file!=null){
			transitionInfo = TransitionInfos.TransitionInfosFromNotificationBar(c, file.getId(), file.getvType());
		}

		Intent fp = new Intent(c, Act_FilePage.class);
		fp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				Intent.FLAG_ACTIVITY_SINGLE_TOP);
		fp.putExtra(DBConstants.CONTAINER, container);
		fp.putExtra(DBConstants.SPACE, space);
		fp.putExtra(DBConstants.FILE, file);
		fp.putExtra(Constants.EXTRA_TRANSITION_INFOS, transitionInfo);

		return fp;
	}


	public static Intent getSnippetIntent(Context c, EdoContainer container, LSpace tab, LFile snippet, String extraText){

		Intent fp = new Intent(c, Act_Snippet.class);
		fp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				Intent.FLAG_ACTIVITY_SINGLE_TOP);
		fp.putExtra(DBConstants.CONTAINER, container);
		fp.putExtra(DBConstants.SPACE, tab);
		fp.putExtra(DBConstants.FILE, snippet);
		fp.putExtra(Constants.EXTRA_TEXT, extraText);

		return fp;
	}

	public static Intent getContactFileFilter(Context c, EdoContainer container){

		Intent fp = new Intent(c, Act_ContactFileFilter.class);
		fp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				Intent.FLAG_ACTIVITY_SINGLE_TOP);
		fp.putExtra(DBConstants.CONTAINER, container);

		return fp;
	}

	public static Intent getCommentsIntent(Context c, EdoContainer container, LSpace tab, LFile file){

		Intent fp = new Intent(c, Act_Comments.class);
		fp.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		fp.putExtra(DBConstants.CONTAINER, container);
		fp.putExtra(DBConstants.SPACE, tab);
		fp.putExtra(DBConstants.FILE, file);

		return fp;
	}


}
