package io.edo.ui.imageutils;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.edo.edomobile.R;
import io.edo.utilities.Constants;
import io.edo.views.EdoProgressBar;

public class GalleryViews {
	public int visualizationMode;
	public FrameLayout fl_preview; 
	public ImageView iv_thumb; 
	public ImageView iv_frame; 
	public LinearLayout ll_label; 
	public TextView tv_label; 
	public TextView tv_title;
	public TextView tv_subTitle;
	public TextView tv_notesCount;
	public FrameLayout fl_progress; 
	public ImageView iv_progress;
	public ImageView iv_center;
	public TextView tv_progress;
	public EdoProgressBar progressBar;


	public GalleryViews(Activity c, View principalLayout, int mImageThumbSize, int visualizationMode) {
		this.visualizationMode = visualizationMode;
		this.fl_preview = (FrameLayout) principalLayout.findViewById(R.id.fl_preview);
		this.ll_label = (LinearLayout) principalLayout.findViewById(R.id.ll_label);
		this.iv_thumb = (ImageView) principalLayout.findViewById(R.id.thumb);
		this.iv_frame = (ImageView) principalLayout.findViewById(R.id.frame);
		this.tv_label = (TextView) principalLayout.findViewById(R.id.label);
		this.tv_title = (TextView) principalLayout.findViewById(R.id.tv_title);
		this.tv_subTitle = (TextView) principalLayout.findViewById(R.id.tv_sub_title);
		this.tv_notesCount = (TextView) principalLayout.findViewById(R.id.tv_notesCount);
		this.fl_progress = (FrameLayout) principalLayout.findViewById(R.id.fl_progress);
		this.iv_progress = (ImageView) fl_progress.findViewById(R.id.iv_progress);
		this.iv_center = (ImageView) fl_progress.findViewById(R.id.iv_center);
		this.tv_progress = (TextView) fl_progress.findViewById(R.id.tv_progress);
		this.progressBar = (EdoProgressBar) principalLayout.findViewById(R.id.progressBar);

		if(visualizationMode == Constants.VISUALIZATION_GRID){
			final AbsListView.LayoutParams params = new AbsListView.LayoutParams(mImageThumbSize, mImageThumbSize);
			c.runOnUiThread(new Runnable() {public void run() {	
				fl_preview.setLayoutParams(params);
				tv_label.setTextSize(12);
				tv_label.setGravity(Gravity.LEFT);
			}});
	
			final FrameLayout.LayoutParams flthumbparams = new FrameLayout.LayoutParams (mImageThumbSize, mImageThumbSize, Gravity.CENTER);
			c.runOnUiThread(new Runnable() {public void run() {	
				GalleryViews.this.fl_progress.setLayoutParams(flthumbparams);
			}});
		}else{
			mImageThumbSize = mImageThumbSize/2;
			final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mImageThumbSize, mImageThumbSize);
			c.runOnUiThread(new Runnable() {public void run() {	
				fl_preview.setLayoutParams(params);
				tv_label.setTextSize(6);
				tv_label.setGravity(Gravity.CENTER);
				tv_label.setPadding(0, 0, 0, 0);
			}});
	
			final FrameLayout.LayoutParams flthumbparams = new FrameLayout.LayoutParams (mImageThumbSize, mImageThumbSize, Gravity.CENTER);
			c.runOnUiThread(new Runnable() {public void run() {	
				GalleryViews.this.fl_progress.setLayoutParams(flthumbparams);
			}});
		}
	}

//	public ThumbnailViews(FrameLayout fl_preview, ImageView iv_thumb,
//			ImageView iv_frame, TextView tv_ext, TextView tv_label,
//			FrameLayout fl_progress, ImageView iv_progress, ImageView iv_center) {
//		super();
//		this.fl_preview = fl_preview;
//		this.iv_thumb = iv_thumb;
//		this.iv_frame = iv_frame;
//		this.tv_ext = tv_ext;
//		this.tv_label = tv_label;
//		this.fl_progress = fl_progress;
//		this.iv_progress = iv_progress;
//		this.iv_center = iv_center;
//	}

	public GalleryViews(EdoProgressBar progressBar) {
		this.progressBar = progressBar;
	}




}
