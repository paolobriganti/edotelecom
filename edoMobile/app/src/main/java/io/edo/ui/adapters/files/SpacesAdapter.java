package io.edo.ui.adapters.files;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.LSpace;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.imageutils.ThumbnailSpaceLoader;
import io.edo.utilities.eLog;

public class SpacesAdapter extends RecyclerView.Adapter<SpacesAdapter.ViewHolder> {

    private ArrayList<LSpace> spaces;
    private Activity c;
    private ThisApp app;

    private ThumbnailSpaceLoader thumbnailSpaceLoader;

    LContact contact;

    public SpacesAdapter(FragmentActivity c, ThumbnailSpaceLoader thumbnailSpaceLoader, LContact contact, ArrayList<LSpace> spaces) {
        this.spaces = spaces;
        this.c = c;
        this.app = ThisApp.getInstance(c);
        this.contact = contact;
        this.thumbnailSpaceLoader = thumbnailSpaceLoader;
    }


    public void clearSpaces() {
        int size = this.spaces.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                spaces.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addSpaces(List<LSpace> spaces) {
        this.spaces.addAll(spaces);
        this.notifyItemRangeInserted(0, spaces.size() - 1);
    }

    public void addSpace(int index, LSpace space) {
        this.spaces.add(index, space);
        this.notifyItemInserted(index);
    }

    public void addSpace(LSpace space) {
        this.spaces.add(0,space);
        this.notifyItemInserted(0);

    }

    public void updateSpace(LSpace space){
        int index = spaces.indexOf(space);

        eLog.w("EdoActivityMan", "index: " + index);
        if(index>=0){
            this.spaces.set(index, space);
            this.notifyDataSetChanged();
        }
    }

    public void updateSpace(int index, LSpace space){
        this.spaces.set(index, space);
        this.notifyDataSetChanged();
    }

    public void removeSpace(LSpace space){
        int index = spaces.indexOf(space);

        eLog.w("EdoActivityMan", "del index: " + index);

        if(index>=0){
            this.spaces.remove(index);
            this.notifyDataSetChanged();
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_space, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder vh, int i) {
        final LSpace space = spaces.get(i);
        vh.setSpaceViews(c,space,thumbnailSpaceLoader,mSpaceClickListener,mSpaceLongClickListener);
       
    }

    @Override
    public int getItemCount() {
        return spaces == null ? 0 : spaces.size();
    }


    public static class ViewHolder extends FileOverlayHolder {
        View spaceLayout;
        ImageView iv_space_image;
        ImageView iv_space_foreground;
        TextView tv_space_title;

        public ViewHolder(View itemView) {
            super(itemView);
            spaceLayout = itemView;
            iv_space_image = (ImageView) itemView.findViewById(R.id.iv_space_image);
            iv_space_foreground = (ImageView) itemView.findViewById(R.id.iv_space_foreground);
            tv_space_title = (TextView) itemView.findViewById(R.id.tv_space_title);
        }


        public void setSpaceViews(Context c,
                                      final LSpace space,
                                      ThumbnailSpaceLoader thumbnailSpaceLoader,
                                      final SpaceClickListener mSpaceClickListener,
                                      final SpaceLongClickListener mSpaceLongClickListener){

            tv_space_title.setText(space.getName());

            thumbnailSpaceLoader.loadImage(space, iv_space_image);

            iv_space_foreground.setBackgroundResource(space.getForegroundColorRes(c));

            spaceLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSpaceClickListener != null) {
                        mSpaceClickListener.onClick(v, space);
                    }
                }
            });
            spaceLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mSpaceLongClickListener != null) {
                        mSpaceLongClickListener.onLongClick(v, space);
                    }
                    return false;
                }
            });

            setSelected(space.isSelected());
        }

    }


    SpaceClickListener mSpaceClickListener;

    public void setSpaceClickListener(SpaceClickListener listener) {
        mSpaceClickListener = listener;
    }

    public interface SpaceClickListener {
        void onClick(View v, LSpace space);
    }

    SpaceLongClickListener mSpaceLongClickListener;

    public void setLongSpaceClickListener(SpaceLongClickListener listener) {
        mSpaceLongClickListener = listener;
    }

    public interface SpaceLongClickListener {
        void onLongClick(View v, LSpace space);
    }
}
