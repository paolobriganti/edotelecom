package io.edo.ui.adapters.timeline;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import io.edo.db.local.beans.LActivity;
import io.edo.edomobile.R;

/**
 * Created by PaoloBriganti on 05/06/15.
 */
public class NewEventsViewHolder extends RecyclerView.ViewHolder {
    public TextView tv_new_events;

    public NewEventsViewHolder(View itemView) {
        super(itemView);

        tv_new_events = (TextView) itemView.findViewById(R.id.tv_new_events);

    }


    public View setNewEventsViews(Context c, LActivity event){
        if(event.getMessage()!=null)
            this.tv_new_events.setText(Html.fromHtml(event.getMessage()));

        return tv_new_events;
    }
}



