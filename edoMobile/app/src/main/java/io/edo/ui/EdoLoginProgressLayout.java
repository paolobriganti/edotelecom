package io.edo.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import io.edo.edomobile.R;
import io.edo.views.ViewPagerPlus;

public class EdoLoginProgressLayout implements ViewPager.OnPageChangeListener{
	FragmentActivity activity;

	boolean isDestroyed = false;

	long INTERVAL_TIME = 4000L;
	long ANIMATION_TIME = 250L;

	boolean isTourRunning = false;
	static boolean isProgressEnded = false;
	private View layout;

	ViewPagerPlus mViewPager;
	AppSectionsPagerAdapter mAppSectionsPagerAdapter;
	public static final int SECTIONS_NUMBER = 6;

	TextView tv_progress;
	ProgressBar progressBar;
	ImageView iv_indicator;

//	boolean sawTheEnd = false;
	public static final int LAST_SECTION = SECTIONS_NUMBER-1;

	public EdoLoginProgressLayout(final FragmentActivity activity, View layout, ContinueButtonClickListener listener) {
		this.activity = activity;

		isDestroyed = false;
		this.layout = layout;

		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(activity.getSupportFragmentManager());
		mAppSectionsPagerAdapter.setContinueButtonClickListener(listener);

		mViewPager = (ViewPagerPlus)layout.findViewById(R.id.viewPager);
		mViewPager.setAdapter(mAppSectionsPagerAdapter);
		mViewPager.addOnPageChangeListener(this);
		tv_progress = (TextView)layout.findViewById(R.id.tv_progress);
		progressBar = (ProgressBar)layout.findViewById(R.id.progressBar);
		iv_indicator = (ImageView)layout.findViewById(R.id.iv_indicator);


		setProgressEnded(isProgressEnded);
	}


	final Runnable tourRunnable = new Runnable() {
		public void run() {
			isTourRunning = true;

			int count = 0;
			while(!isProgressEnded) {
				try {
					Thread.sleep(INTERVAL_TIME);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				count++;
				if(count>=SECTIONS_NUMBER){
					count=0;
				}

				final int i = count;
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mViewPager.setCurrentItem(i);
					}
				});

			}

			isTourRunning = false;
		}

	};





	public void startExample(){
		show();
		Thread s = new Thread(exampleRunnable);
		s.start();
	}
	 final Runnable exampleRunnable = new Runnable() {
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				int progress = 0;
				while(progress<101) {
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					setProgressPercentage(progress);
					progress++;
				}
				setProgressEnded(true);
			}

     };

	public void show(){
		isProgressEnded = false;

		activity.runOnUiThread(new Runnable() {public void run() {

			layout.setVisibility(View.VISIBLE);
//			startTour();
		}});
//		Thread s = new Thread(exampleRunnable);
//		s.start();


	}

	public void startTour(){
		Thread s = new Thread(tourRunnable);
		s.start();
	}


	public void hide(){
		activity.runOnUiThread(new Runnable() {public void run() {
			layout.setVisibility(View.GONE);

		}});
	}



	public void setProgressEnded(final boolean isProgressEnded){
		this.isProgressEnded = isProgressEnded;

		if(isProgressEnded){
			activity.runOnUiThread(new Runnable() {public void run() {
				setProgressMessage(activity.getResources().getString(R.string.loginSuccess));
//				if(sawTheEnd){
					mViewPager.setCurrentItem(LAST_SECTION);
//				}
			}});
		}

		activity.runOnUiThread(new Runnable() {
			public void run() {
				mAppSectionsPagerAdapter.notifyDataSetChanged();
			}
		});
	}


	public void setProgressMessage(final String message){
		activity.runOnUiThread(new Runnable() {public void run() {
			if(tv_progress!=null){
				tv_progress.setText(message);
			}
		}});

	}


	public void setProgressPercentage(final int percent){
		activity.runOnUiThread(new Runnable() {public void run() {
			if(tv_progress!=null){
				progressBar.setProgress(percent);
				setProgressMessage(percent +"%");


//				if(!isTourRunning){
//					startTour();
//				}
			}
		}});
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		int section = position+1;
		int imageRes = activity.getResources().getIdentifier("intro_indicator_"+section, "drawable", activity.getPackageName());
		iv_indicator.setImageResource(imageRes);

		if(position==LAST_SECTION){
//			sawTheEnd = true;
		}
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}


	public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {


		public AppSectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			// The other sections of the app are dummy placeholders.
			DummySectionFragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
			fragment.setArguments(args);
			fragment.setContinueButtonClickListener(mContinueButtonClickListener);
			return fragment;
		}

		@Override
		public int getCount() {
			return SECTIONS_NUMBER;
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "Section " + (position + 1);
		}

		ContinueButtonClickListener mContinueButtonClickListener;
		public void setContinueButtonClickListener(ContinueButtonClickListener listener) {
			mContinueButtonClickListener = listener;
		}

//		boolean isProgressEnded;
//		public void setProgressEnded(boolean isProgressEnded) {
//			this.isProgressEnded = isProgressEnded;
//			eLog.v("EDO", "ADAPTER isProgressEnded: " + isProgressEnded);
//				this.notifyDataSetChanged();
//		}
	}

	public static class DummySectionFragment extends Fragment {

		public static final String ARG_SECTION_NUMBER = "section_number";

		Button bt_intro;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
								 Bundle savedInstanceState) {

			View rootView = inflater.inflate(R.layout.login_intro_screen, container, false);
			Bundle args = getArguments();
			int section = args.getInt(ARG_SECTION_NUMBER);

			if(section!=SECTIONS_NUMBER) {
				TextView tv_intro = (TextView) rootView.findViewById(R.id.tv_intro);
				try {
					int textRes = getActivity().getResources().getIdentifier("intro" + section, "string", getActivity().getPackageName());
					tv_intro.setText(textRes);
				} catch (Exception e) {

				}
				tv_intro.setVisibility(section == SECTIONS_NUMBER ? View.GONE : View.VISIBLE);

				ImageView iv_intro = (ImageView) rootView.findViewById(R.id.iv_intro);
				int imageRes = getActivity().getResources().getIdentifier("intro_" + section, "drawable", getActivity().getPackageName());
				iv_intro.setImageResource(imageRes);
			}else{
				rootView = inflater.inflate(R.layout.login_end_screen, container, false);

				bt_intro = (Button) rootView.findViewById(R.id.bt_intro);
				bt_intro.setText(!isProgressEnded ? R.string.loading : R.string.continue_);
				bt_intro.setVisibility(section == SECTIONS_NUMBER ? View.VISIBLE : View.GONE);
				bt_intro.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mContinueButtonClickListener != null && isProgressEnded) {
							mContinueButtonClickListener.onClick(v);
						}
					}
				});

//				ImageButton bt_facebook = (ImageButton) rootView.findViewById(R.id.bt_facebook);
//				bt_facebook.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View v) {
//
//					}
//				});
//
//				ImageButton bt_twitter = (ImageButton) rootView.findViewById(R.id.bt_twitter);
//				bt_twitter.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View v) {
//
//					}
//				});

			}



			return rootView;
		}

		ContinueButtonClickListener mContinueButtonClickListener;
		public void setContinueButtonClickListener(ContinueButtonClickListener listener) {
			mContinueButtonClickListener = listener;
		}

//		boolean isProgressEnded;
//		public void setProgressEnded(boolean isProgressEnded) {
//			eLog.v("EDO", "DUMMY isProgressEnded: " + isProgressEnded);
//			this.isProgressEnded = isProgressEnded;
//		}
	}







	public interface ContinueButtonClickListener {
		void onClick(View v);
	}





}
