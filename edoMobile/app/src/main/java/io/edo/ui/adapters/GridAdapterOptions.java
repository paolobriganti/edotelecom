package io.edo.ui.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class GridAdapterOptions extends BaseAdapter {
	private Context context;
	private ArrayList<String> titles;
	private ArrayList<Integer> images;

	public GridAdapterOptions(Context context, ArrayList<String> titles, ArrayList<Integer> images) {
		this.context = context;

		this.titles = titles;
		this.images = images;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.grid_opt_item, parent, false);


		TextView label = (TextView) rowView.findViewById(R.id.label);
		if(titles!=null && titles.size()>0 && JavaUtils.isNotEmpty(titles.get(position)))
			label.setText(titles.get(position));
		else 
			label.setText(" ");

//		EdoUI.setFontToTextView(context, label, EdoUI.FONT_NAME_ARCHITECT_DAUGHTER);

		ImageView thumb = (ImageView) rowView.findViewById(R.id.thumb);
		if(images!=null && images.size()>0 && images.get(position)!=null){
			thumb.setVisibility(ImageView.VISIBLE);
			thumb.setImageResource(images.get(position));
		}else{
			thumb.setVisibility(ImageView.GONE);
			thumb.setBackgroundColor(Color.TRANSPARENT);
		}
		rowView.setBackgroundResource(R.drawable.bt_transparent);
		return rowView;
	}


} 