package io.edo.ui.adapters;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class ListAdapterImageSimple extends BaseAdapter {
	private final Context context;
	private final ArrayList<String> titles, subTitles;
	private final ArrayList<Drawable> images;
	private final Integer titlesColor;
	private final Integer subTitlesColor;
	private final Integer imageBgColor;

	public ListAdapterImageSimple(Context context, ArrayList<String> titles, Integer titlesColor, ArrayList<String> subTitles, Integer subTitlesColor, ArrayList<Drawable> images, Integer imageBgColor) {
		this.context = context;
		this.titles = titles;
		this.titlesColor = titlesColor;
		this.subTitles = subTitles;
		this.subTitlesColor = subTitlesColor;
		this.images = images;
		this.imageBgColor = imageBgColor;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.row_image_text_subtext, parent, false);
		TextView tv_title = (TextView) rowView.findViewById(R.id.tv_title);
		if(titles!=null && titles.size()>0 && JavaUtils.isNotEmpty(titles.get(position)))
			tv_title.setText(titles.get(position));
		else 
			tv_title.setText(" ");
		
		if(titlesColor!=null)
			tv_title.setTextColor(titlesColor);
		
		TextView tv_subTitle = (TextView) rowView.findViewById(R.id.tv_sub_title);
		if(subTitles!=null && subTitles.size()>0 && JavaUtils.isNotEmpty(subTitles.get(position)))
			tv_subTitle.setText(subTitles.get(position));
		else 
			tv_subTitle.setText(" ");

		if(subTitlesColor!=null)
			tv_subTitle.setTextColor(subTitlesColor);
		
		ImageView iv_icon = (ImageView) rowView.findViewById(R.id.iv_icon);
		if(images!=null && images.size()>0 && images.get(position)!=null){
			iv_icon.setVisibility(ImageView.VISIBLE);
			iv_icon.setImageDrawable(images.get(position));
			if(imageBgColor!=null)
				iv_icon.setBackgroundColor(imageBgColor);
		}else{
			iv_icon.setVisibility(ImageView.GONE);
			iv_icon.setBackgroundColor(Color.TRANSPARENT);
		}
		return rowView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
} 