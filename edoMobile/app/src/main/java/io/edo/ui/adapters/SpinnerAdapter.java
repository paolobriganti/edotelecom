package io.edo.ui.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import io.edo.edomobile.R;

/**
 * Created by PaoloBriganti on 06/06/15.
 */
public class SpinnerAdapter extends BaseAdapter {

    Activity c;

    public SpinnerAdapter(Activity c, int[] mItems) {
        this.c = c;
        this.mItems = mItems;
    }

    private int[] mItems = new int[0];

    public void clear() {
        mItems = new int[0];
    }

//    public void addItem(YourObject yourObject) {
//        mItems.add(yourObject);
//    }
//
//    public void addItems(List<YourObject> yourObjectList) {
//        mItems.addAll(yourObjectList);
//    }

    @Override
    public int getCount() {
        return mItems.length;
    }

    @Override
    public Object getItem(int position) {
        return mItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
            view = c.getLayoutInflater().inflate(R.layout.spinner_item, parent, false);
            view.setTag("DROPDOWN");
        }

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
            view = c.getLayoutInflater().inflate(R.layout.
                    spinner_item, parent, false);
            view.setTag("NON_DROPDOWN");
        }
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));
        return view;
    }

    private int getTitle(int position) {
        return position >= 0 && position < mItems.length ? mItems[position] : R.string.app_name;
    }
}
