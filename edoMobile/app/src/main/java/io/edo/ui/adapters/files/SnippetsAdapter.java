package io.edo.ui.adapters.files;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;

public class SnippetsAdapter extends RecyclerView.Adapter<SnippetsAdapter.ViewHolder> {

    private ArrayList<LFile> snippets;
    private Activity c;
    private ThisApp app;

    LContact contact;

    public SnippetsAdapter(FragmentActivity c, LContact contact, ArrayList<LFile> snippets) {
        this.snippets = snippets;
        this.c = c;
        this.app = ThisApp.getInstance(c);
        this.contact = contact;
    }


    public void clearSnippets() {
        int size = this.snippets.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                snippets.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addSnippets(List<LFile> snippets) {
        this.snippets.addAll(snippets);
        this.notifyItemRangeInserted(0, snippets.size() - 1);
    }

    public void addSnippet(int index, LFile snippet) {
        this.snippets.add(index, snippet);
        this.notifyItemInserted(index);
    }

    public void addSnippet(LFile snippet) {
        this.snippets.add(snippet);
        this.notifyItemInserted(snippets.size()-1);

    }

    public void updateSnippet(LFile snippet){
        int index = snippets.indexOf(snippet);
        if(index>=0){
            this.snippets.set(index, snippet);
            this.notifyDataSetChanged();
        }
    }

    public void updateSnippet(int index, LFile snippet){
        this.snippets.set(index, snippet);
        this.notifyDataSetChanged();
    }

    public void removeSnippet(LFile snippet){
        int index = snippets.indexOf(snippet);
        if(index>=0){
            this.snippets.remove(index);
            this.notifyDataSetChanged();
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_snippet, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder vh, int i) {
        final LFile snippet = snippets.get(i);
        vh.setSnippetViews(c, snippet, mSnippetClickListener, mSnippetLongClickListener);
       
    }

    @Override
    public int getItemCount() {
        return snippets == null ? 0 : snippets.size();
    }


    public static class ViewHolder extends FileOverlayHolder {
        CardView snippetLayout;
        TextView tv_snippet_title;
        TextView tv_snippet_text;

        public ViewHolder(View itemView) {
            super(itemView);
            snippetLayout = (CardView) itemView;
            tv_snippet_title = (TextView) itemView.findViewById(R.id.tv_snippet_title);
            tv_snippet_text = (TextView) itemView.findViewById(R.id.tv_snippet_text);
        }


        public void setSnippetViews(Context c,
                                      final LFile snippet,
                                      final SnippetClickListener mSnippetClickListener,
                                      final SnippetLongClickListener mSnippetLongClickListener){

            int backgroundColor = snippet.getThumbnailColor(c,R.color.white);

            snippetLayout.setCardBackgroundColor(backgroundColor);

            tv_snippet_title.setText(snippet.getName());
//            tv_snippet_text.setText(DateManager.getUserFriendlyDateFromMillis(c, snippet.getCDate()));


            tv_snippet_text.setText(snippet.getContent());


            setSelected(snippet.isSelected());

            snippetLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSnippetClickListener != null) {
                        mSnippetClickListener.onClick(ViewHolder.this, snippet);
                    }
                }
            });
            snippetLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mSnippetLongClickListener != null) {
                        mSnippetLongClickListener.onLongClick(ViewHolder.this, snippet);
                    }
                    return true;
                }
            });
        }

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    SnippetClickListener mSnippetClickListener;

    public void setSnippetClickListener(SnippetClickListener listener) {
        mSnippetClickListener = listener;
    }

    public interface SnippetClickListener {
        void onClick(SnippetsAdapter.ViewHolder v, LFile snippet);
    }

    SnippetLongClickListener mSnippetLongClickListener;

    public void setLongSnippetClickListener(SnippetLongClickListener listener) {
        mSnippetLongClickListener = listener;
    }

    public interface SnippetLongClickListener {
        void onLongClick(SnippetsAdapter.ViewHolder vh, LFile snippet);
    }
}
