package io.edo.ui.imageutils;

import android.content.Context;
import android.graphics.Bitmap;

import com.paolobriganti.android.ContactsManager;
import com.paolobriganti.utils.ImageUtils;

import io.edo.api.ApiContacts;
import io.edo.api.linkparser.LinkParser;
import io.edo.db.local.beans.LContact;
import io.edo.db.web.beans.WEntity;
import io.edo.edomobile.ThisApp;

public class ContactPhoto {


    public static String getPhotoWebLink(Context c, LContact contact){
        if(contact.getThumbnail()!=null){
            return contact.getThumbnail();
        }

        String link = ApiContacts.getContactPhotoWebLink(c, ThisApp.getInstance(c).getDB(),contact);
        if(link != null){
            return link;
        }

        WEntity entity = LinkParser.getContactInfo(c, contact.getPrincipalEmailAddress());
        if(entity!=null){
            return entity.thumb;
        }
        return null;
    }

    public static Bitmap getPhotoFromDevice(Context c, LContact contact){
        //Get photo from device
        if(contact.isContact() && contact.getPrincipalEmailAddress()!=null) {
            Long contactDeviceId = ContactsManager.getContactIDFromEmail(c, contact.getPrincipalEmailAddress());
            if (contactDeviceId != null) {
                return ContactsManager.getContactPhoto(c, contactDeviceId);
            }
        }
        return null;
    }

    public static Bitmap getCircularContactPhoto(Context c, Bitmap bitmap, Integer sideSize){
        if(bitmap!=null){
            if(bitmap!=null){
                return ImageUtils.getCroppedCircleCenterBitmap(bitmap, sideSize);
            }
        }
        return null;
    }





//
//	public static Bitmap getPhoto(Context c, String contactId){
//		try{
//			if(hasPhoto(c, contactId)){
//				return BitmapFactory.decodeFile(getPhotoPath(c, contactId));
//			}
//		}catch(OutOfMemoryError e){
//			return null;
//		}
//		return null;
//	}


//	public static Bitmap getCircularContactPhoto(Context c, LContact contact, Integer sideSize){
//		if(contact!=null){
//			Bitmap cPhoto = getCircularContactPhoto(c, contact.getId(), sideSize);
//			if(cPhoto==null){
//				cPhoto = BitmapFactory.decodeResource(c.getResources(), !contact.isGroup()?R.drawable.no_photo:R.drawable.ic_group_no_photo);
//			}
//			return cPhoto;
//		}
//		return BitmapFactory.decodeResource(c.getResources(), R.drawable.no_photo);
//	}
//
//
//	public static Bitmap getCircularContactPhoto(Context c, String contactId, Integer sideSize){
//		if(contactId!=null){
//			Bitmap cPhoto = getPhoto(c, contactId);
//			if(cPhoto!=null){
//				return ImageUtils.getCroppedCircleCenterBitmap(cPhoto, sideSize);
//			}
//		}
//		return null;
//	}






//	public static Bitmap getAndSavePhotoFromURL(Context c, String contactId, String thumbnailURL){
//		if(thumbnailURL!=null){
//			Bitmap bitmap = ImageResizer.dec;
//			if(bitmap!=null){
//				savePhoto(c, contactId, bitmap);
//			}
//			return bitmap;
//		}
//		return null;
//	}












    //*********************************************************************
    //*		ANDROID CONTACTS PHOTO
    //*********************************************************************

//	public static Bitmap createPhoto(Context c, Contact contact){
//
//		if(contact!=null){
//			InputStream is = null;
//			if(contact!=null){
//				is = contact.getPhoto(c);
//				return getScaledBitmapByMaxSide(c, is, Constants.THUMB_SIZE);
//			}
//		}
//		return null;
//
//	}
//
//	public static boolean createAndSavePhoto(Context c, String contactId, Contact contact){
//		if(contactId!=null && contact!=null){
//			Bitmap photo = createPhoto(c, contact);
//			if(photo!=null){
//				savePhoto(c, contactId, photo);
//				photo.recycle();
//				return true;
//			}
//		}
//		return false;
//	}


//	//*********************************************************************
//	//*		GOOGLE PLUS PHOTO
//	//*********************************************************************
//
//	public static Bitmap createPhoto(Context c, GooglePlusContactInfo infos){
//
//		if(infos!=null && JavaUtils.isNotEmpty(infos.getPhotoLink())){
//			InputStream is = HTTPUtils.downloadFromUrl(infos.getPhotoLink());
//			if(infos!=null && is!=null){
//				return getScaledBitmapByMaxSide(c, is, Constants.THUMB_SIZE);
//			}
//		}
//		return null;
//
//	}
//
//	public static boolean createAndSavePhoto(Context c, String contactId, GooglePlusContactInfo infos){
//		if(contactId!=null && infos!=null){
//			Bitmap photo = createPhoto(c, infos);
//			if(photo!=null){
//				savePhoto(c, contactId, photo);
//				photo.recycle();
//				return true;
//			}
//		}
//		return false;
//	}










//	public static String savePhoto(Context c, String contactId, Bitmap bitmap){
//		if(bitmap!=null){
//			File dir = getPhotoFolder(c);
//			dir.mkdirs();
//			if(dir.exists()){
//				String filePath = getPhotoPath(c, contactId);
//				ImageUtils.saveBitmapToFile(bitmap, Bitmap.CompressFormat.PNG, filePath);
//				return filePath;
//			}
//		}
//		return null;
//	}





//
//	public static boolean hasPhoto(Context c, LContact contact){
//		return hasPhoto(c, contact.getId());
//	}
//
//	public static boolean hasPhoto(Context c, String contactId){
//		File prevFile = new File(getPhotoPath(c, contactId));
//		return prevFile.exists();
//	}
//
//
//	public static String getPhotoPath(Context c, String contactId){
//		return getPhotoFolder(c)+File.separator+contactId;
//	}
//
//	public static File getPhotoFolder(Context c){
//		//		return new File(ThumbnailManager.getThumbnailDirPath(c)+File.separator+Constants.SECTION_NAME_CONTACTS+File.separator+"Avatars");
//		if(Constants.EDO_EXTERNAL_SD_PATH==null){
//			return null;
//		}
//
//		return new File(Constants.EDO_EXTERNAL_SD_PATH+File.separator+Constants.SECTION_NAME_CONTACTS+File.separator+"Avatars");
//	}












//	public static Bitmap getScaledBitmapByMaxSide(Context c, InputStream is, int maxSide) {
//		if(is!=null){
//			InputStream in = is; 
//			InputStream in2 = JavaUtils.cloneInputStream(is); 
//
//
//			// Decode image size
//			BitmapFactory.Options o = new BitmapFactory.Options();
//			o.inJustDecodeBounds = true;
//			BitmapFactory.decodeStream(in, null, o);
//			try {
//				in.close();
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//
//
//			boolean resize = false;
//			int width = maxSide;
//			int height = maxSide;
//			if(o.outWidth >= o.outHeight){
//				resize = true;
//				height = (int)((float)((float)o.outHeight /(float) o.outWidth)*(float)width);
//			}else if(o.outWidth < o.outHeight){
//				resize = true;
//				width = (int)((float)((float)o.outWidth /(float) o.outHeight)*(float)height);
//			}
//
//			int maxSize = width*height;
//
//			int scale = 1;
//			while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) > maxSize) {
//				scale++;
//			}
//
//
//			Bitmap b = null;
//			in = in2;
//			if (resize && scale > 1) {
//				scale--;
//				// scale to max possible inSampleSize that still yields an image
//				// larger than target
//				o = new BitmapFactory.Options();
//				o.inSampleSize = scale;
//				b = BitmapFactory.decodeStream(in, null, o);
//
//				System.gc();
//			} else {
//				try {
//					b = BitmapFactory.decodeStream(in);
//				} catch (OutOfMemoryError e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			}
//			try {
//				in.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//
//			return b;
//		}
//		return null;
//	}






}
