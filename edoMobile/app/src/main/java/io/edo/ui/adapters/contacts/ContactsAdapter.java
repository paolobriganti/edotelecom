package io.edo.ui.adapters.contacts;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.edo.db.local.beans.LContact;
import io.edo.edomobile.R;
import io.edo.ui.imageutils.ContactPhotoLoader;

public class ContactsAdapter extends RecyclerView.Adapter<ContactViewHolder> {

    private Context c;
    private String edoUserId;
    private ContactPhotoLoader contactPhotoLoader;
    private ArrayList<LContact> contacts;
    private Integer rightIcon = null;
    private String groupOwnerId = null;

    private String lastLetter;

    public ContactsAdapter(FragmentActivity c,
                           ArrayList<LContact> contacts,
                           ContactPhotoLoader contactPhotoLoader,
                           Integer rightIcon,
                           String groupOwnerId) {
        this.contacts = contacts;
        this.c = c;
        this.contactPhotoLoader = contactPhotoLoader;
        this.rightIcon = rightIcon;
        this.groupOwnerId = groupOwnerId;
    }

    String searchText = null;
    public void setSearchText(String searchText){
        this.searchText = searchText;
    }

    public void clearContacts() {
        lastLetter = null;

        int size = this.contacts.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                contacts.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addContacts(List<LContact> contacts) {
        this.contacts.addAll(contacts);
//        this.notifyItemRangeInserted(0, contacts.size() - 1);
        this.notifyDataSetChanged();
    }

    public void addContact(int index, LContact contact) {
        this.contacts.add(index, contact);
//        this.notifyItemInserted(index);
        this.notifyDataSetChanged();
    }

    public void addContact(LContact contact) {
        this.contacts.add(contact);
//        this.notifyItemInserted(contacts.size() - 1);
        this.notifyDataSetChanged();
    }

    public void updateContact(LContact contact){
        int index = contacts.indexOf(contact);
        if(index>=0){
            this.contacts.set(index, contact);
            this.notifyDataSetChanged();
        }
    }

    public void updateContact(int index, LContact contact){
        this.contacts.set(index, contact);
        this.notifyDataSetChanged();
    }

    public void removeContact(LContact contact){
        int index = contacts.indexOf(contact);
        if(index>=0){
            this.contacts.remove(index);
//            this.notifyItemRemoved(index);
            this.notifyDataSetChanged();
        }
    }


    @Override
    public ContactViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_edo_contact_right_image, viewGroup, false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ContactViewHolder vh, int i) {
        final LContact contact = contacts.get(i);
        String currentLetter = contact.getName()!=null && contact.getName().length()>1? contact.getName().substring(0,1):null;

        String letter = null;
        if((currentLetter!=null && lastLetter==null) ||
           (currentLetter!=null && lastLetter!=null && !currentLetter.equalsIgnoreCase(lastLetter))){
            lastLetter = currentLetter.toUpperCase();
            letter = lastLetter;
        }



        vh.setContactViews(c, contact, contactPhotoLoader, edoUserId, groupOwnerId, rightIcon, letter, searchText);
        if(mContactClickListener!=null){
            vh.contactLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContactClickListener.onClick(v,contact);
                }
            });
        }

        if(mContactLongClickListener!=null){
            vh.contactLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    mContactLongClickListener.onLongClick(v, contact);

                    return true;
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return contacts == null ? 0 : contacts.size();
    }

    ContactClickListener mContactClickListener;

    public void setContactClickListener(ContactClickListener listener) {
        mContactClickListener = listener;
    }

    public interface ContactClickListener {
        void onClick(View v, LContact contact);
    }

    ContactLongClickListener mContactLongClickListener;

    public void setLongContactClickListener(ContactLongClickListener listener) {
        mContactLongClickListener = listener;
    }

    public interface ContactLongClickListener {
        void onLongClick(View v, LContact contact);
    }


}
