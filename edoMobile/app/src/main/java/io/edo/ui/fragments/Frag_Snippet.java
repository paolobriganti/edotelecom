package io.edo.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.edomobile.R;

/**
 * Created by PaoloBriganti on 31/07/15.
 */
public class Frag_Snippet extends Fragment{


    LFile currentFile;

    public LinearLayout ll_snippet_background;
    public TextView tv_snippet_title;
    public TextView tv_snippet_content;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()!=null && getArguments().containsKey(DBConstants.FILE)) {
            Serializable fileSerial = getArguments().getSerializable(DBConstants.FILE);
            if(fileSerial!=null) {
                currentFile = (LFile) fileSerial;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.frag_snippet, container, false);

        ll_snippet_background = (LinearLayout) layout.findViewById(R.id.ll_snippet_background);
        tv_snippet_title = (TextView)layout.findViewById(R.id.tv_snippet_title);
        tv_snippet_content = (TextView) layout.findViewById(R.id.tv_snippet_content);


        return layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setSnippet();
    }

    public void setSnippet(){
        int backgroundColor = currentFile.getThumbnailColor(getActivity(), R.color.white);
        ll_snippet_background.setBackgroundColor(backgroundColor);
        tv_snippet_title.setText(currentFile.getName());
        tv_snippet_content.setText(currentFile.getContent());
    }


}
