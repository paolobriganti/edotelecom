package io.edo.ui.imageutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;

import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.edo.db.local.beans.LFile;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoUI;
import io.edo.ui.adapters.files.FilesAdapter;
import io.edo.utilities.Constants;

public class ThumbnailFileLoader extends ImageLoader{
    public FragmentActivity c;
    public ThisApp app;
    private Executor executor;

    public static ThumbnailFileLoader getInstance(FragmentActivity fragmentActivity){
        ThisApp app = ThisApp.getInstance(fragmentActivity);
        if(app.thumbnailFileLoader==null){
            app.thumbnailFileLoader = new ThumbnailFileLoader(fragmentActivity);
        }
        return app.thumbnailFileLoader;
    }

    public ThumbnailFileLoader(FragmentActivity fragmentActivity){
        super(fragmentActivity, Constants.THUMB_CACHE_DIR_NAME, getImageViewSideSize(fragmentActivity), getImageViewSideSize(fragmentActivity));
        this.c = fragmentActivity;
        this.app = ThisApp.getInstance(c);
        this.executor = Executors.newFixedThreadPool(EdoUI.getFilesColumnsNumber(c));
    }

    public static int getImageViewSideSize(Context c){
        int w = c.getResources().getDisplayMetrics().widthPixels;
        return (w/EdoUI.getFilesColumnsNumber(c));
    }

    @Override
    public Executor getExecutor() {
        return executor;
    }


    public void loadImage(LFile file, FilesAdapter.ViewHolder vh)
    {
        ImageData imageData = new ImageData(file, vh);
        loadImage(imageData,vh.iv_file);
    }

    public void loadImage(LFile file, ImageView imageView)
    {
        FilesAdapter.ViewHolder vh = new FilesAdapter.ViewHolder(imageView);
        ImageData imageData = new ImageData(file, vh);
        loadImage(imageData,imageView);
    }

    public void loadImage(ImageData imageData, ImageView imageView)
    {
        boolean canHavePreview = imageData.file.canHaveACloudPreview() || imageData.file.canHaveAManualPreview();
        if(canHavePreview) {
            loadDataToImageView(imageData, imageView);
        }else{
            setImage(imageData,imageView,null);
        }
        setPreProcessExtraViews(imageData);
    }

    @Override
    public String getKeyFromData(Object data) {
        ImageData imageData = (ImageData) data;
        return imageData.file.getId();
    }

    @Override
    public Bitmap getLoadingImageFromData(Object data) {
        ImageData imageData = (ImageData) data;
        return BitmapFactory.decodeResource(mContext.getResources(), ThumbnailManager.getLoadingRes(c, imageData.file));
    }

    @Override
    protected Bitmap processBitmap(Object data) {
        try {
            ImageData imageData = (ImageData) data;
            LFile file = imageData.file;

            if(file!=null){
                if(file.isFolder() && !file.hasThumbnail())
                    return null;

                if(file.isSnippet())
                    return ThumbnailManager.getSnippetPreview(c,file,mImageHeight);

                if(file.isStored()){
                    if(file.isImageRaster()){
                       return processBitmapFromLocalPath(file.getLocalUri());
                    }else if(file.isVideo()){
                        return ThumbnailManager.getVideoPreviewOfLocalFile(c,file.getLocalUri(),mImageHeight);
                    }
                }
                String thumbLink = ThumbnailManager.getThumbnailWebLink(mContext,file,mImageHeight);

                return processBitmapFromHttpUrl(thumbLink);
            }
        } catch (Throwable ex){

            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError){
                clearCache();
                closeCacheInternal();
                return processBitmap(data);
            }
        }
        return null;
    }


    @Override
    public void onBitmapProcessed(Object data, ImageView imageView, Drawable value) {
        setImage((ImageData) data, imageView, value);
    }

    public void setImage(ImageData imageData, ImageView imageView, Drawable drawable) {


        if(drawable!=null){
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

//            setImageDrawable(imageData, imageView, drawable);
            imageView.setImageDrawable(drawable);
            imageView.setBackgroundColor(c.getResources().getColor(R.color.transparent));


        }else{
            LFile file = imageData.file;

            int icon = ThumbnailManager.getIconRes(c, file);
            imageView.setImageResource(icon);

            if(file.isFolder() || file.isSpace()) {

                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setBackgroundColor(file.getThumbnailColor(c, R.color.edo_folder_default));

            }else {
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setBackgroundColor(c.getResources().getColor(R.color.transparent));
            }
        }

        setPostProcessExtraViews(imageData,drawable!=null);

    }


    public void setPreProcessExtraViews(ImageData imageData){

        if(imageData.vh.overlay!=null){
            imageData.vh.setSelected(imageData.file.isSelected());
        }
    }

    public void setPostProcessExtraViews(ImageData imageData, boolean hasPreview){
        if(imageData.vh.label_container!=null){
            imageData.vh.label_container.setBackgroundResource(hasPreview ? R.color.edo_black_40_transparent : android.R.color.transparent);
        }
        if(imageData.vh.video_overlay!=null){
            imageData.vh.video_overlay.setVisibility(hasPreview && imageData.file.isVideo() ? View.VISIBLE : View.GONE);
        }
    }













    public class ImageData{
        public LFile file;
        public FilesAdapter.ViewHolder vh;

        public ImageData(LFile file, FilesAdapter.ViewHolder vh) {
            this.file = file;
            this.vh = vh;
        }
    }

    public static Bitmap getBitmapFromDiskCache(Context c, String key){
        return getBitmapFromDiskCache(c,Constants.THUMB_CACHE_DIR_NAME,key);
    }
}
