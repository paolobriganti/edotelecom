package io.edo.ui.imageutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.edo.db.local.beans.support.LSpace;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;

public class ThumbnailSpaceLoader extends ImageLoader{
    private FragmentActivity c;
    private ThisApp app;
    private Executor executor;

    public static ThumbnailSpaceLoader getInstance(FragmentActivity fragmentActivity){
        ThisApp app = ThisApp.getInstance(fragmentActivity);
        if(app.thumbnailSpaceLoader==null){
            app.thumbnailSpaceLoader = new ThumbnailSpaceLoader(fragmentActivity);
        }
        return app.thumbnailSpaceLoader;
    }

    public ThumbnailSpaceLoader(FragmentActivity fragmentActivity){
        super(fragmentActivity, Constants.THUMB_CACHE_DIR_NAME, getImageViewSideSize(fragmentActivity), getImageViewSideSize(fragmentActivity));
        this.c = fragmentActivity;
        this.app = ThisApp.getInstance(c);
        this.executor = Executors.newFixedThreadPool(EdoUI.getSpacesColumnsNumber(c));
    }

    public static int getImageViewSideSize(Context c){
        int w = c.getResources().getDisplayMetrics().widthPixels;
        return w;
    }

    @Override
    public Executor getExecutor() {
        return executor;
    }


    public void loadImage(LSpace space, ImageView imageView)
    {
        ImageData imageData = new ImageData(space);
        loadDataToImageView(imageData, imageView);
    }

    @Override
    public String getKeyFromData(Object data) {
        ImageData imageData = (ImageData) data;
        return imageData.space.getId();
    }

    @Override
    public Bitmap getLoadingImageFromData(Object data) {
        ImageData imageData = (ImageData) data;
        return BitmapFactory.decodeResource(mContext.getResources(), R.color.transparent);
    }

    @Override
    protected Bitmap processBitmap(Object data) {
        try {
            ImageData imageData = (ImageData) data;
            LSpace space = imageData.space;

            if(space!=null){
                String thumbLink = ThumbnailManager.getThumbnailWebLink(mContext,space,mImageHeight);
                return processBitmapFromHttpUrl(thumbLink);
            }
        } catch (Throwable ex){

            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError){
                clearCache();
                closeCacheInternal();
            }
        }
        return null;
    }


    @Override
    public void onBitmapProcessed(Object data, ImageView imageView, Drawable value) {
        setImage((ImageData) data, imageView, value);
    }

    public void setImage(ImageData imageData, ImageView imageView, Drawable drawable) {


        if(drawable!=null){

            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            setImageDrawable(imageData, imageView, drawable);

            imageView.setBackgroundResource(R.drawable.bg_card_white);

        }else{

            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imageView.setImageResource(R.drawable.file_folder_i);

            imageView.setBackgroundResource(R.color.edo_tab_background);


        }
    }




    public class ImageData{
        public LSpace space;

        public ImageData(LSpace space) {
            this.space = space;
        }
    }
}
