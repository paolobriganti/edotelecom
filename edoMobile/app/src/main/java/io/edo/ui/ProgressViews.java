package io.edo.ui;

import android.app.Activity;
import android.widget.ProgressBar;
import android.widget.TextView;

import io.edo.views.EdoProgressBar;

public class ProgressViews {
	private Activity context;
	private EdoProgressBar bar;
	private TextView title;
	private TextView subTitle;

	public ProgressViews(Activity context, EdoProgressBar bar,
			TextView title, TextView subTitle) {
		super();
		this.context = context;
		this.bar = bar;
		this.title = title;
		this.subTitle = subTitle;
	}
	public EdoProgressBar getBar() {
		return bar;
	}
	public void setBar(EdoProgressBar bar) {
		this.bar = bar;
	}

	public void setTitle(final int resId){
		if(title!=null)
			context.runOnUiThread(new Runnable() {public void run() {
				title.setText(resId);
			}});
	}

	public void setTitle(final String text){
		if(title!=null)
			context.runOnUiThread(new Runnable() {public void run() {
				title.setText(text);
			}});
	}

	public void setSubTitle(final int resId){
		if(subTitle!=null)
			context.runOnUiThread(new Runnable() {public void run() {
				subTitle.setText(resId);
			}});
	}

	public void setSubTitle(final String text){
		if(subTitle!=null)
			context.runOnUiThread(new Runnable() {public void run() {
				subTitle.setText(text);
			}});
	}


	// ** PROGRESS PERCENT ***

	public int getPercent(final int current, final int total, final int from, final int to){
		int max = to-from;
		float progress = ((float)current/(float)total)*max;
		return (int) progress + from;
	}


	public int getPercent(final int current, final int total){
		float progress = ((float)current/(float)total)*100;
		return (int) progress;
	}

	// ** PROGRESS BAR ***

	public int setBarProgress(final int current, final int total, final int from, final int to){
		int percent = getPercent(current, total, from, to);
		setBarProgress(percent);
		return percent;
	}

	public int setBarProgress(final int current, final int total){
		int percent = getPercent(current, total);
		setBarProgress(percent);
		return percent;
	}
	public void setBarProgress(final int percent){
		context.runOnUiThread(new Runnable() {public void run() {
			if(bar!=null){
				bar.setVisibility(ProgressBar.VISIBLE);
				bar.setProgress(percent);
			}
		}});
	}

}
