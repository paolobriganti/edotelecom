package io.edo.ui.adapters.timeline;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.Comment;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.CommentsUI;
import io.edo.ui.CommentsUIContacts;
import io.edo.ui.adapters.files.FileOverlayHolder;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.ui.imageutils.ThumbnailActivityLoader;
import io.edo.utilities.Constants;
import io.edo.utilities.DateManager;

/**
 * Created by PaoloBriganti on 05/06/15.
 */
public class TimelineEventViewHolder extends FileOverlayHolder implements TimelineBaseViewHolder {
    public LContact contact;
    private CommentsUI notesUI = null;

    public CardView cardView;
    public FrameLayout preview_container;
    public ImageView iv_image_left;
    public ImageView iv_image_left_overlay;
    public TextView tv_contactName;
    public TextView tv_date;
    public TextView tv_description;
    public TextView tv_location;
    public RelativeLayout ll_footer;
    public ImageButton bt_comments;
    public ImageButton bt_star;
    public ImageButton bt_share;
    public LinearLayout ll_comments_container;

    public TimelineEventViewHolder(final FragmentActivity c,
                                   LContact contact,
                                   ContactPhotoLoader contactPhotoLoader,
                                   View itemView) {
        super(itemView);
        this.contact = contact;
        cardView = (CardView) itemView.findViewById(R.id.cardView);
        preview_container = (FrameLayout) itemView.findViewById(R.id.preview_container);
        iv_image_left = (ImageView) itemView.findViewById(R.id.iv_image_left);
        iv_image_left_overlay = (ImageView) itemView.findViewById(R.id.iv_image_left_overlay);
        tv_contactName = (TextView) itemView.findViewById(R.id.tv_contactName);
        tv_date = (TextView) itemView.findViewById(R.id.tv_date);
        tv_description = (TextView) itemView.findViewById(R.id.tv_description);
        tv_location = (TextView) itemView.findViewById(R.id.tv_location);
        ll_footer = (RelativeLayout) itemView.findViewById(R.id.ll_footer);
        bt_comments = (ImageButton) itemView.findViewById(R.id.bt_comments);
        bt_star = (ImageButton) itemView.findViewById(R.id.bt_star);
        bt_share = (ImageButton) itemView.findViewById(R.id.bt_share);
        ll_comments_container = (LinearLayout) itemView.findViewById(R.id.ll_comments_container);

//        if(contact.isPersonal(c)){
//            this.notesUI = new CommentsUIPersonal(c, ll_comments_container);
//        }else{
        this.notesUI = new CommentsUIContacts(c, contact, contactPhotoLoader, ll_comments_container,true);
//        }


    }


    public View setEventViews(final Activity c, ThumbnailActivityLoader thumbnailActivityLoader, final LActivity event){
        ThisApp app = ThisApp.getInstance(c);
        tv_contactName.setText(event.getSenderName());

        if(event.isFromCurrentEdoUser(c)) {
            tv_contactName.setTextColor(c.getResources().getColor(R.color.edo_me_chat));
        }else if(contact.isGroup()){
            LContact member = contact.getGroupMember(event.getSenderId());
            if(member!=null)
                tv_contactName.setTextColor(member.getMemberColor(c));
            else
                tv_contactName.setTextColor(c.getResources().getColor(R.color.edo_contact_chat));
        }else{
            tv_contactName.setTextColor(c.getResources().getColor(R.color.edo_contact_chat));
        }


        boolean isRedux = event.isReduxVisualization() || event.isReduxDefVisualization();


        if(isRedux) {
            this.preview_container.setVisibility(View.GONE);
            this.ll_footer.setVisibility(View.GONE);

        }else{
            this.ll_footer.setVisibility(View.VISIBLE);
            thumbnailActivityLoader.loadImage(event, this.iv_image_left);
            this.preview_container.setVisibility(View.VISIBLE);

        }

        if(event.isFileResource()){
            LFile file = event.getFileResource();

//            if(event.getVtype()!=null && event.getVtype().equals(LFile.FILE_TYPE_SNIPPET)){
//                this.preview_container.setVisibility(View.GONE);
//            }

            String location = c.getResources().getString(R.string.fileTypeNameinFolderName);

            String fileType = file.getTypeToShowInAView(c);

            String folderName = event.getParentName()!=null ? event.getParentName():event.getSpaceName();

            if(fileType!=null && folderName!=null) {

                fileType = fileType.toUpperCase();
//                folderName = VU.getColoredTextString(c, R.color.edo_blue_file, folderName.toUpperCase());

                if(!folderName.toLowerCase().contains("default")){
                    location = location.replace(Constants.PH_FILETYPENAME, fileType).replace(Constants.PH_FOLDERNAME, folderName);
                }else{
                    location = fileType;
                }

                tv_location.setText(location);
            }else if(fileType!=null){
                tv_location.setText(Html.fromHtml(fileType.toUpperCase()));
            }
            if(event.getVtype()!=null && event.getVtype().equals(FileInfo.TYPE_VIDEO)){
                iv_image_left_overlay.setVisibility(View.VISIBLE);
                iv_image_left_overlay.setImageResource(R.drawable.overlay_video_icon);
            }else
                iv_image_left_overlay.setVisibility(View.GONE);
        }else{
            this.iv_image_left.setVisibility(View.GONE);
            this.iv_image_left_overlay.setVisibility(View.GONE);
        }

        this.tv_date.setText(event.getTimeStamp() != LActivity.VALUE_ERROR ? DateManager.getUserFriendlyDateFromMillis(c, event.getTimeStamp()) : null);

        if(event.getEvent() == LActivity.EVENT_INSERT || event.isCommentEvent()){
            this.tv_description.setText(Html.fromHtml(event.isCommentEvent()?event.getShortDescription():event.getMessage()));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final LFile commentsFile = event.getFileResource(c);
                    if(commentsFile!=null && commentsFile.getComments()!=null) {
                        Comment[] ori = commentsFile.getComments();
                        if (ori != null && ori.length > 0) {
                            Comment[] comments = new Comment[1];
                            comments[0] = ori[ori.length - 1];
                            commentsFile.setComments(comments);

                            c.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ll_comments_container.setVisibility(View.VISIBLE);
                                    notesUI.setComments(commentsFile, true);
                                }
                            });
                        }
                    }

                }
            }).start();

        }else{
            if(JavaUtils.isNotEmpty(event.getAggregationMessage())){
                this.tv_description.setText(Html.fromHtml(event.getAggregationMessage()));
            }else if(JavaUtils.isNotEmpty(event.getMessage())){
                this.tv_description.setText(Html.fromHtml(event.getMessage()));
            }else if(JavaUtils.isNotEmpty(event.getShortDescription())){
                this.tv_description.setText(Html.fromHtml(event.getShortDescription()));
            }else if(JavaUtils.isNotEmpty(event.getLongDescription())) {
                this.tv_description.setText(Html.fromHtml(event.getLongDescription()));
            }else{
                this.tv_description.setText("EVENT: "+event.getEvent()+",\n Resource Type:"+event.getResourceType());
            }
            this.ll_comments_container.setVisibility(View.GONE);
        }

        setSelected(event.isSelected());

        LActivity a = app.uiManager.getActivityByResourceId(event.getResourceId());
        if(a!=null && (a.isUploadEvent() || a.isDownloadEvent())){
            String text = c.getResources().getString(R.string.upload).toUpperCase();
            if(a.isDownloadEvent()){
                text = c.getResources().getString(R.string.download).toUpperCase();
            }
            setProgress(a.getStatus(), a.getProgressPercent(), a.getMessage(),text);
        }

        return this.cardView;
    }


    public void setProgress(int progressStatus, int progressPercent, String message, String progressType) {
        setProgress(progressStatus, progressPercent,progressType);

        if(JavaUtils.isNotEmpty(message)){
            this.tv_description.setText(Html.fromHtml(message));
        }
    }

    @Override
    public void setSelected(boolean selected){
        super.setSelected(selected);
    }


}



