package io.edo.ui;

import android.app.Activity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.paolobriganti.android.AUI;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.global.uibeans.CommentUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.Comment;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;

public class CommentsUIPersonal implements CommentsUI {
    private Activity c;
    private ThisApp app;


    private String execId = "";

    private LContact userContact;
    private LFile file;

    //Views shared
    LinearLayout layout;
    private LinearLayout notesContentLayout;
    //    private LinearLayout ll_comments;
    private LinearLayout ll_editComments;
    private EditText et_note;
//    private ImageButton bt_post;

    private EdoProgressDialog loading;

    private LayoutInflater inflater;

    CommentUIDAO commentDAO;

    public CommentsUIPersonal(final Activity c,
                              View layout) {
        super();
        this.c = c;
        this.app = ThisApp.getInstance(c);
        this.userContact = app.getUserContact();
        this.commentDAO = new CommentUIDAO(c);

        inflater = c.getLayoutInflater();

        layout.setVisibility(View.VISIBLE);

        notesContentLayout = (LinearLayout) layout;
//        ll_comments = (LinearLayout) layout.findViewById(R.id.ll_comments);
        ll_editComments = (LinearLayout) layout.findViewById(R.id.ll_editComments);

        loading = new EdoProgressDialog(c,R.string.loading,R.string.postingComment);

        et_note = (EditText) layout.findViewById(R.id.et_comment);
        if(et_note!=null)
            et_note.setOnEditorActionListener(new OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        postComment();
                    }
                    return false;
                }
            });


//        bt_post = (ImageButton) layout.findViewById(R.id.bt_post);
//        bt_post.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }





    @Override
    public void setComments(final LFile file, final boolean setViews){


        CommentsUIPersonal.this.file = file;

        if(setViews)
            setCommentsViews();


    }

    @Override
    public void setCommentsViews(){
        c.runOnUiThread(new Runnable() {
            public void run() {
//                ll_comments.setVisibility(View.VISIBLE);
//                ll_editComments.setVisibility(View.GONE);

                Comment comment = null;

                if (!file.hasComments()) {
                    String text = c.getResources().getString(R.string.noteEmptyPersonalTextFile);
                    et_note.setHint(text);
//                    comment = new Comment(Constants.USER_NO, text);

//                    setCommentLayout(inflater, note);
                } else {
                    comment = file.getCommentsList().get(0);
//                    setCommentLayout(inflater, file.getCommentsList().get(0));
                }


//                notesContentLayout.setOnClickListener(onCommentClickListener);
                if (et_note != null) {
//                    et_note.setOnClickListener(onCommentClickListener);

                    if (comment != null) {
                        et_note.setText(comment.getMessage());
                        et_note.setSelection(et_note.getText().length());
                    } else {
                        et_note.setText(null);
                    }

                }
//                ll_comments.setOnClickListener(onCommentClickListener);


            }
        });

    }

//    OnClickListener onCommentClickListener = new OnClickListener() {
//
//        @Override
//        public void onClick(View v) {
//            onCommentClick(file, 0);
//        }
//    };
//
//
//    private void onCommentClick(final LFile file, int noteIndex){
//        String oldMessage = "";
//        if(file.getCommentsList()!=null && file.getCommentsList().size()>noteIndex){
//            Comment note = file.getCommentsList().get(noteIndex);
//            if(!note.getUserId().equals(Constants.USER_NO))
//                oldMessage = note.getMessage();
//        }
//        editComment(file, oldMessage);
//    }
//
//
//    public void editComment(final LFile file, final String oldMessage){
////        ll_comments.setVisibility(View.GONE);
////        ll_editComments.setVisibility(View.VISIBLE);
//
//        if(JavaUtils.isNotEmpty(oldMessage))
//            if(et_note!=null)
//                et_note.setText(oldMessage);
//
//    }

    @Override
    public void postComment() {

        String text = null;
        if(et_note!=null){
            text = et_note.getText().toString();
        }

        addNewComment(file, text);
    }

    public void addNewComment(final LFile file, final String message){
        if(JavaUtils.isNotEmpty(message)) {
            AUI.keyboardHide(c);
            loading.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final LFile wfile = commentDAO.addComment(c, userContact, file, message);

                    c.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(wfile!=null) {
                                setComments(wfile, true);
                                c.finish();
                            }else{
                                EdoUI.showToast(c, c.getResources().getString(R.string.errorRetry), null, Toast.LENGTH_SHORT);
                            }
                            loading.dismiss();
                        }
                    });
                }
            }).start();
        }
    }

    public String getExecId() {
        return execId;
    }

    public void setIdExecId(String execId) {
        this.execId = execId;
    }













//    public void setCommentLayout(final LayoutInflater inflater, final Comment note){
//        if(note==null || note.getMessage()==null)
//            return;
//
//        final ImageView iv_image = (ImageView) ll_comments.findViewById(R.id.iv_image);
//        final TextView tv_title = (TextView) ll_comments.findViewById(R.id.tv_title);
//        final TextView tv_note = (TextView) ll_comments.findViewById(R.id.tv_comment);
//        tv_note.setMovementMethod(LinkMovementMethod.getInstance());
//        final TextView tv_date = (TextView) ll_comments.findViewById(R.id.tv_date);
//        tv_title.setVisibility(View.GONE);
//        iv_image.setVisibility(View.INVISIBLE);
//
//        c.runOnUiThread(new Runnable() {public void run() {
//
//            if(!note.getUserId().equals(Constants.USER_NO)){
//                tv_date.setVisibility(TextView.VISIBLE);
//
//                long GMTmillis = note.getcDate();
//
//                tv_date.setText(DateManager.getUserFriendlyDateFromMillis(c, GMTmillis));
//            }else{
//                tv_date.setVisibility(TextView.GONE);
//            }
//
//            tv_note.setText(note.getMessage());
//
//            iv_image.setOnClickListener(onCommentClickListener);
//            tv_title.setOnClickListener(onCommentClickListener);
//            tv_note.setOnClickListener(onCommentClickListener);
//            tv_date.setOnClickListener(onCommentClickListener);
//
//        }});
//
//    }

    @Override
    public View getLayout() {
        // TODO Auto-generated method stub
        return layout;
    }







}
