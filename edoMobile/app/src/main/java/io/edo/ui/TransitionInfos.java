package io.edo.ui;

import android.content.Context;
import android.view.View;

import com.paolobriganti.android.AUI;

import java.io.Serializable;


public class TransitionInfos implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public String resourceId;
	public String resourceUri;
	public String description;
	public String type;
	public int thumbnailTop;
	public int thumbnailLeft;
	public int thumbnailWidth;
	public int thumbnailHeight;
	public int originalOrientation;

	public boolean enabled;
	
	
	public TransitionInfos() {
		super();
	}
	
	public TransitionInfos(Context c, View v, String id, String description, String type) {
		int[] screenLocation = new int[2];
        v.getLocationOnScreen(screenLocation);
        
        this.resourceId = id;
        this.description = description;
        this.type = type;
        this.thumbnailLeft = screenLocation[0];
        this.thumbnailTop = screenLocation[1];
        this.thumbnailWidth = v.getWidth();
        this.thumbnailHeight = v.getHeight();
        this.originalOrientation = c.getResources().getConfiguration().orientation;
	}

	public TransitionInfos(Context c, String resourceId, String type, int thumbnailTop,
			int thumbnailLeft, int thumbnailWidth, int thumbnailHeight) {
		super();
		this.resourceId = resourceId;
		this.type = type;
		this.thumbnailTop = thumbnailTop;
		this.thumbnailLeft = thumbnailLeft;
		this.thumbnailWidth = thumbnailWidth;
		this.thumbnailHeight = thumbnailHeight;
		this.originalOrientation = c.getResources().getConfiguration().orientation;
	}

	public static TransitionInfos TransitionInfosFromNotificationBar(Context c, String resourceId, String type) {
		return new TransitionInfos(c, resourceId, 
				type, 
				AUI.getStatusBarHeight(c), 
				0, 
				c.getResources().getDisplayMetrics().widthPixels, 
				AUI.getActioBarHeight(c));
	}




}
