package io.edo.ui.imageutils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import io.edo.db.local.beans.LFile;

public class ThumbnailFilePickerGalleryLoader extends ThumbnailFilePickerLoader{

    public ThumbnailFilePickerGalleryLoader(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @Override
    public Bitmap getLoadingImageFromData(Object data) {
        return BitmapFactory.decodeResource(mContext.getResources(), android.R.color.transparent);
    }

    @Override
    protected Bitmap processBitmap(Object data) {
        try {
            ImageData imageData = (ImageData) data;
            LFile file = imageData.file;

            if(file!=null){

                if(file.isFolder()){
                    file = LFile.newLocalFile(null, null, file.getThumbnail(), null, null);
                }

                if(file.isImageRaster()){
                    return processBitmapFromLocalPath(file.getLocalUri());
                }else if(file.isVideo()){
                    return ThumbnailManager.getVideoPreviewOfLocalFile(mContext,file.getLocalUri(),mImageHeight);
                }

                return null;
            }
        } catch (Throwable ex){

            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError){
                clearCache();
                closeCacheInternal();
                return processBitmap(data);
            }
        }
        return null;
    }


    @Override
    public void setPostProcessExtraViews(ImageData imageData, boolean hasPreview) {
        super.setPostProcessExtraViews(imageData, hasPreview);

        if(imageData.vh.label_container!=null){
            imageData.vh.label_container.setVisibility((hasPreview && !imageData.file.isFolder()) ? View.GONE : View.VISIBLE);
        }
    }
}
