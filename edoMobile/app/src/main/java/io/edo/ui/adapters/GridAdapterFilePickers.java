package io.edo.ui.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class GridAdapterFilePickers extends BaseAdapter {
	private Context context;
	private ArrayList<Integer> titles;
	private ArrayList<Integer> images;
	private Integer titlesColor;
	private Integer imageBgColor;

	public GridAdapterFilePickers(Context context,
								  ArrayList<Integer> titles,
								  ArrayList<Integer> images) {

		this.titles = titles;
		this.images = images;

		this.context = context;
		this.titlesColor = context.getResources().getColor(R.color.grey_light);
		this.imageBgColor = Color.TRANSPARENT;





	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.grid_item_pickers, parent, false);


		TextView label = (TextView) rowView.findViewById(R.id.label);
		label.setText(titles.get(position));

		ImageView thumb = (ImageView) rowView.findViewById(R.id.thumb);
		if(images!=null && images.size()>0 && images.get(position)!=null){
			thumb.setVisibility(ImageView.VISIBLE);
			thumb.setImageResource(images.get(position));
			if(imageBgColor!=null)
				thumb.setBackgroundColor(imageBgColor);
		}else{
			thumb.setVisibility(ImageView.GONE);
			thumb.setBackgroundColor(Color.TRANSPARENT);
		}
		rowView.setBackgroundResource(R.drawable.bt_transparent);
		return rowView;
	}


} 