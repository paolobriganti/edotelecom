package io.edo.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import io.edo.edomobile.R;

public class EdoDialogGridView extends EdoDialogOne{

	BaseAdapter adapter;
	GridView gv;
	
	int numColumns = 0;

	OnGridItemClickListener itemClickListener;
	OnGridItemLongClickListener itemLongClickListener;

	public EdoDialogGridView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub


	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_edo_gridview);

		EdoUI.setDialogWindowParams(this);
		
		tv_title = (TextView) this.findViewById(R.id.tv_title);
		tv_text = (TextView) this.findViewById(R.id.tv_text);
		gv = (GridView)  this.findViewById(R.id.gv);
		bt_positive = (Button) this.findViewById(R.id.bt_positive);

		setTitle(title);
		setTitle(titleRes);
		setText(text);
		setText(textRes);
		setPositiveButtonText(btPositiveText);
		setPositiveButtonText(btPositiveTextRes);
		setOnPositiveClickListener(positiveListener);
		setNumColumns(numColumns);
		setAdapter (adapter);
		setOnGridItemClickListener(itemClickListener);
		setOnGridItemLongClickListener(itemLongClickListener);
	}


	public void setAdapter (BaseAdapter adapter){
		if(adapter!=null){ 
			this.adapter = adapter;
			if(gv!=null){
				gv.setAdapter(adapter);
			}
		}
	}

	public GridView getGridView (){
		return gv;
	}
	
	public void setNumColumns (final int numColumns){
		if(numColumns!=0){
			this.numColumns = numColumns;
			if(gv!=null){
				gv.setNumColumns(numColumns);
			}
		}
	}
	
	public void setOnGridItemClickListener(OnGridItemClickListener listener) {
		if(listener!=null){
			this.itemClickListener = listener;
			if(gv!=null)
				gv.setOnItemClickListener(listener);
		}
	}
	public interface OnGridItemClickListener extends OnItemClickListener{
	}

	public void setOnGridItemLongClickListener(OnGridItemLongClickListener listener) {
		if(listener!=null){
			this.itemLongClickListener = listener;
			if(gv!=null)
				gv.setOnItemLongClickListener(listener);
		}
	}
	public interface OnGridItemLongClickListener extends OnItemLongClickListener{
	}
}
