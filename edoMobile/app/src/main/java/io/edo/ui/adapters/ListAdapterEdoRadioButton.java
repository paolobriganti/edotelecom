package io.edo.ui.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class ListAdapterEdoRadioButton extends BaseAdapter {
	private final Context context;
	private final ArrayList<String> titles;
	private final ArrayList<Integer> images;
	private Integer checkPosition = null;


	public ListAdapterEdoRadioButton(Context context, ArrayList<String> titles, ArrayList<Integer> images, Integer checkPosition) {
		this.context = context;
		this.titles = titles;
		this.images = images;
		this.checkPosition = checkPosition;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.row_edo_radio, parent, false);
		RadioButton rb = (RadioButton) rowView.findViewById(R.id.rb);
		if(titles!=null && titles.size()>position && JavaUtils.isNotEmpty(titles.get(position)))
			rb.setText(titles.get(position));
		else 
			rb.setText(" ");
		if(checkPosition!=null && checkPosition==position)
			rb.setChecked(true);
		else
			rb.setChecked(false);
		
		rb.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setChecked(position);
			}
		});
		rb.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				setChecked(position);
				return true;
			}
		});
		
		ImageView iv = (ImageView) rowView.findViewById(R.id.iv_icon);
		if(images!=null && images.size()>position){
			iv.setVisibility(ImageView.VISIBLE);
			iv.setImageResource(images.get(position));
		}else{ 
			iv.setVisibility(ImageView.GONE);
		}
		return rowView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public boolean isChecked(Integer position){
		return checkPosition == position;
	}
	public Integer getChecked(){
		return checkPosition;
	}
	public void setChecked(int position){
		checkPosition = position;
		notifyDataSetChanged();
	}
} 