package io.edo.ui.adapters;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.api.ApiInfo;
import io.edo.db.local.LocalDBManager;
import io.edo.db.local.beans.LService;
import io.edo.edomobile.R;
import io.edo.views.EdoProgressBar;

public class ListAdapterServices extends BaseAdapter {
	private final ArrayList<LService> services;
	private final Context context;
	LocalDBManager db;
	private final Integer bgColor;
	private final Integer titlesColor;
	private final Integer subTitlesColor;
	private final Integer imageBgColor;

	private String freeOf = "free of";
	
	public ListAdapterServices(Context context, LocalDBManager db, ArrayList<LService> services,
			Integer bgColor, 
			Integer titlesColor, 
			Integer subTitlesColor, 
			Integer imageBgColor) {
		this.db = db;
		this.context = context;
		this.services = services;
		this.bgColor = bgColor;
		this.titlesColor = titlesColor;
		this.subTitlesColor = subTitlesColor;
		this.imageBgColor = imageBgColor;
		
		this.freeOf = context.getResources().getString(R.string.freeOf);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return services.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return services.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.row_edo_services, parent, false);


		LinearLayout lv_item_image_ll = (LinearLayout) rowView.findViewById(R.id.lv_item_image_ll);
		if(bgColor!=null)
			lv_item_image_ll.setBackgroundColor(bgColor);

		if(services!=null && services.size()>0){
			final LService service = services.get(position);
			TextView tv_title = (TextView) rowView.findViewById(R.id.tv_title);
			String serviceInfo = service.getCommercialCloudName(context);
			tv_title.setText(serviceInfo);

			if(titlesColor!=null)
				tv_title.setTextColor(titlesColor);

			final TextView tv_subTitle = (TextView) rowView.findViewById(R.id.tv_sub_title1);
			tv_subTitle.setText(service.getEmail());
			
			final TextView tv_subTitle2 = (TextView) rowView.findViewById(R.id.tv_sub_title2);
			final EdoProgressBar progressBar = (EdoProgressBar) rowView.findViewById(R.id.progressBar);
			new Thread(){@Override public void run(){Looper.myLooper();
			try{
				if(ASI.isInternetOn(context)){
					ApiInfo apiInfo = new ApiInfo(context, db, service);
					if(apiInfo!=null){
						Long freeBytes = apiInfo.getFreeBytes();
						Long totalBytes = apiInfo.getTotalBytes();
						String freeSpace = apiInfo.getBytesForView(freeBytes);
						String totalSpace = apiInfo.getBytesForView(totalBytes);
						final String cInfo = freeSpace+" "+context.getResources().getString(R.string.freeOf)+" "+totalSpace;
						final int progress = JavaUtils.getIntegerValueOfDouble(apiInfo.getOccupiedBytes()/1000000, 0);
						final int max = JavaUtils.getIntegerValueOfDouble(totalBytes/1000000, 0);
						((Activity) context).runOnUiThread(new Runnable() {public void run() {
							tv_subTitle2.setText(cInfo);
							progressBar.setVisibility(ProgressBar.VISIBLE);
							progressBar.setMax(max);
							progressBar.setProgress(progress);
						}});
					}
				}
			}catch(NullPointerException e){}
				
			}}.start();
			
			if(subTitlesColor!=null){
				tv_subTitle.setTextColor(subTitlesColor);
				tv_subTitle2.setTextColor(subTitlesColor);
			}
			

			ImageView iv_icon = (ImageView) rowView.findViewById(R.id.iv_icon);
			Integer res = service.getCloudIcon(context);
			if(res!=null){
				iv_icon.setVisibility(ImageView.VISIBLE);
				iv_icon.setBackgroundResource(res);
			}else{
				iv_icon.setVisibility(ImageView.GONE);
				iv_icon.setBackgroundColor(Color.TRANSPARENT);
			}
		}
		return rowView;
	}


} 