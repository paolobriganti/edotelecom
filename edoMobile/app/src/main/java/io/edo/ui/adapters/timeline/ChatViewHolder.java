package io.edo.ui.adapters.timeline;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.DateManager;

/**
 * Created by PaoloBriganti on 05/06/15.
 */
public class ChatViewHolder extends RecyclerView.ViewHolder implements TimelineBaseViewHolder {
    public View cardView;
    public View layout_container;
    public TextView tv_contactName;
    public TextView tv_message;
    public TextView tv_date;

    public ChatViewHolder(View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.card_chatContent);
        layout_container = itemView.findViewById(R.id.layout_container);
        tv_contactName = (TextView) itemView.findViewById(R.id.tv_contactName);
        tv_message = (TextView) itemView.findViewById(R.id.tv_message);
        tv_date = (TextView) itemView.findViewById(R.id.tv_date);
    }

    public View setChatViews(Context c, LContact contact, LActivity msg) {

        ThisApp app = ThisApp.getInstance(c);

        setSelected(msg.isSelected());
        if(JavaUtils.isNotEmpty(msg.getAggregationMessage())){
            this.tv_message.setText(msg.getAggregationMessage());
        }else{
            this.tv_message.setText(msg.getMessage());
        }


        if (msg.isPersonal() || msg.getSenderId().equals(app.getEdoUserId())) {
            if (msg.isInProgress()) {
                this.tv_date.setText(R.string.sendingInProgress);
                this.tv_date.setVisibility(TextView.VISIBLE);
//                this.cardView.setBackgroundResource(R.color.grey_50);
            } else if (msg.hasBeenProcessed()) {
                long mills = msg.getTimeStamp() != LActivity.VALUE_ERROR ? msg.getTimeStamp() : System.currentTimeMillis();

                this.tv_date.setText(DateManager.getUserFriendlyDateFromMillis(c, mills));
                this.tv_date.setVisibility(TextView.VISIBLE);
//                this.cardView.setBackgroundResource(R.color.grey_50);
            } else {
                this.tv_date.setText(R.string.sendingFailed);
                this.tv_date.setVisibility(TextView.VISIBLE);
//                this.cardView.setBackgroundResource(R.color.red_50);
            }
            this.tv_contactName.setText(app.getUserContact().getName());
            this.tv_contactName.setTextColor(c.getResources().getColor(R.color.edo_me_chat));
//            this.tv_date.setGravity(Gravity.LEFT);
        } else {
//            int backgroundColor = c.getResources().getColor(R.color.edo_blue_extra_light);
//            int alpha = 255;

            int nameColor = c.getResources().getColor(R.color.edo_contact_chat);

            String senderName = msg.getSenderName() != null ? msg.getSenderName() : c.getResources().getString(R.string.contact);

            if (contact != null && contact != null) {
                if (!contact.isGroup())
                    senderName = contact.getNameSurname();
                else {
                    LContact member = contact.getGroupMember(msg.getSenderId());
                    if (member != null) {
                        senderName = member.getNameSurname();
                        nameColor = member.getMemberColor(c);
                    } else {
                        nameColor = c.getResources().getColor(R.color.edo_group_chat_deleted_contact);
                    }
//                    backgroundColor = nameColor;
//                    alpha = 50;
                }
            }

            long GMTmillis = msg.getTimeStamp();
            this.tv_date.setText(DateManager.getUserFriendlyDateFromMillis(c, GMTmillis));
            this.tv_date.setVisibility(TextView.VISIBLE);
//            this.tv_date.setTextColor(nameColor);
            this.tv_contactName.setText(senderName);
            this.tv_contactName.setTextColor(nameColor);
//            this.cardView.setBackgroundColor(backgroundColor);
//            this.cardView.getBackground().setAlpha(alpha);
//            this.tv_date.setGravity(Gravity.RIGHT);
        }

        return this.cardView;
    }

    @Override
    public void setSelected(boolean isSelected){
        this.layout_container.setBackgroundResource(!isSelected?R.drawable.bg_card_white:R.drawable.bg_card_blue_light);
    }
}