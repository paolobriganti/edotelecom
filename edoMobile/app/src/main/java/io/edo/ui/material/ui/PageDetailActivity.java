/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.edo.ui.material.ui;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.paolobriganti.android.AUI;

import io.edo.edomobile.R;
import io.edo.ui.material.util.LPreviewUtils;
import io.edo.ui.material.util.LPreviewUtilsBase;
import io.edo.ui.material.util.UIUtils;
import io.edo.ui.material.widget.ObservableScrollView;

public abstract class PageDetailActivity extends FragmentActivity implements ObservableScrollView.Callbacks{
	private static final float PHOTO_ASPECT_RATIO = 1.7777777f;

	LPreviewUtilsBase mLPreviewUtils;

	private int mHeaderTopClearance;
	private int mPhotoHeightPixels;
	private int mHeaderHeightPixels;
	private int fabButtonHeightPixels;
	private int mSessionColor;

	boolean mGapFillShown;

	private static final float GAP_FILL_DISTANCE_MULTIPLIER = 1.5f;


	private float mMaxHeaderElevation;
	private float mFABElevation;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//		if (shouldBeFloatingWindow()) {
		//			setupFloatingWindow();
		//		}
		super.onCreate(savedInstanceState);

		mSessionColor = getResources().getColor(R.color.edo_blue);

		setTitle("");

		mLPreviewUtils = LPreviewUtils.getInstance(PageDetailActivity.this);

		mFABElevation = getResources().getDimensionPixelSize(R.dimen.fab_elevation);
		mMaxHeaderElevation = getResources().getDimensionPixelSize(
				R.dimen.max_header_elevation);

	}


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        setupScrollLayout();
    }


    public abstract View getScrollViewChild();
    public abstract ObservableScrollView getScrollView();
    public abstract View getFabButton();
    public abstract View getHeaderBox();
    public abstract View getHeaderBackgroundBox();
    public abstract View getHeaderShadow();
    public abstract View getDetailsContainer();
    public abstract View getPhotoViewContainer();
    public abstract TextView[] getTitles();
    public abstract boolean isHeaderOverlay();
    public abstract boolean hasPhoto();
	public void onSetupFinished(){}




    public void setupScrollLayout() {
        setupCustomScrolling();
    }


    public void setHeaderColor(int color, boolean doAnimation){
		ColorDrawable previousColorDrawable = new ColorDrawable(mSessionColor);
		mSessionColor = color;

		if (mSessionColor == 0) {
			// no color -- use default
			mSessionColor = getResources().getColor(R.color.edo_blue);
		} else {
			// make sure it's opaque
			mSessionColor = UIUtils.setColorAlpha(mSessionColor, 255);
		}

		ColorDrawable colorDrawable = new ColorDrawable(mSessionColor);
		//		ColorDrawable colorDrawableScaled = new ColorDrawable(UIUtils.scaleColor(mSessionColor, 0.8f, false));
		ColorDrawable colorDrawableScaledDefault = new ColorDrawable(UIUtils.scaleColor(mSessionColor, 0.8f, false));
		
		int colorScaledStatusBar = UIUtils.scaleColor(colorDrawableScaledDefault.getColor(), 0.8f, false);

		AUI.setStatusBarColor(this, colorScaledStatusBar);
		
		if(doAnimation){
			ColorDrawable[] transColor1 = {previousColorDrawable, colorDrawable};
			TransitionDrawable trans = new TransitionDrawable(transColor1);
			if(getHeaderBackgroundBox()!=null)
				getHeaderBackgroundBox().setBackgroundDrawable(trans);
			trans.startTransition(250);
		}else{
			if(getHeaderBackgroundBox()!=null)
				getHeaderBackgroundBox().setBackgroundColor(mSessionColor);
		}
		//	    ColorDrawable[] transColor2 = {previousColorDrawable, colorDrawableScaledDefault};
		//	    TransitionDrawable trans2 = new TransitionDrawable(transColor2);
		//	    getPhotoViewContainer().setBackgroundDrawable(trans2);
		//	    trans.startTransition(250);

		//		getHeaderBackgroundBox().setBackgroundDrawable(colorDrawable);
		//		mLPreviewUtils.setStatusBarColor(colorDrawableScaled);
		
		if(getPhotoViewContainer()!=null)
			getPhotoViewContainer().setBackgroundDrawable(colorDrawableScaledDefault);


		if(getTitles()!=null && getTitles().length>0)
			for(TextView tv_title: getTitles()){
				tv_title.setTextColor(mSessionColor);
			}
	}




	private void setupCustomScrolling() {
		getScrollView().addCallbacks(this);
		ViewTreeObserver vto = getScrollView().getViewTreeObserver();
		if (vto.isAlive()) {
			vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					fabButtonHeightPixels = getFabButton().getHeight();
					recomputePhotoAndScrollingMetrics();
					getScrollView().getViewTreeObserver().removeGlobalOnLayoutListener(this);

					onSetupFinished();
				}
			});
		}else{
			onSetupFinished();
		}
	}
	
	
	//	private void setupFloatingWindow() {
	// configure this Activity as a floating window, dimming the background
	//		WindowManager.LayoutParams params = getWindow().getAttributes();
	//		params.width = getResources().getDimensionPixelSize(R.dimen.session_details_floating_width);
	//		params.height = getResources().getDimensionPixelSize(R.dimen.session_details_floating_height);
	//		params.alpha = 1;
	//		params.dimAmount = 0.7f;
	//		params.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
	//		getWindow().setAttributes(params);
	//	}

	//	private boolean shouldBeFloatingWindow() {
	//		Resources.Theme theme = getTheme();
	//		TypedValue floatingWindowFlag = new TypedValue();
	//		if (theme == null || !theme.resolveAttribute(R.attr.isFloatingWindow, floatingWindowFlag, true)) {
	//			// isFloatingWindow flag is not defined in theme
	//			return false;
	//		}
	//		return (floatingWindowFlag.data != 0);
	//	}

	//	private void setTextSelectable(TextView tv) {
	//		if (tv != null && !tv.isTextSelectable()) {
	//			tv.setTextIsSelectable(true);
	//		}
	//	}

	protected void recomputePhotoAndScrollingMetrics() {
		final int actionBarSize = UIUtils.calculateActionBarSize(PageDetailActivity.this);
		mHeaderTopClearance = actionBarSize - getHeaderBox().getPaddingTop();
		mHeaderHeightPixels = getHeaderBox().getHeight();

		mPhotoHeightPixels = mHeaderTopClearance;

		if (hasPhoto()) {
			mPhotoHeightPixels = (int) (getPhotoViewContainer().getWidth() / PHOTO_ASPECT_RATIO);
			mPhotoHeightPixels = PageDetailActivity.this.getResources().getDisplayMetrics().heightPixels - mHeaderHeightPixels - fabButtonHeightPixels;
		}

		ViewGroup.LayoutParams lp;
		lp = getPhotoViewContainer().getLayoutParams();
		if (lp.height != mPhotoHeightPixels) {
			lp.height = mPhotoHeightPixels;
			getPhotoViewContainer().setLayoutParams(lp);
		}

		lp = getHeaderBackgroundBox().getLayoutParams();
		if (lp.height != mHeaderHeightPixels) {
			lp.height = mHeaderHeightPixels;
			getHeaderBackgroundBox().setLayoutParams(lp);
		}

		ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams)
				getDetailsContainer().getLayoutParams();
		if (mlp.topMargin != mHeaderHeightPixels + mPhotoHeightPixels) {
			mlp.topMargin = mHeaderHeightPixels + mPhotoHeightPixels;
			getDetailsContainer().setLayoutParams(mlp);
		}

		onScrollChanged(0, 0); // trigger scroll handling
	}


	

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (getScrollView() == null) {
			return;
		}

	}






	@Override
	public void onScrollChanged(int deltaX, int deltaY) {

		// Reposition the header bar -- it's normally anchored to the top of the content,
		// but locks to the top of the screen on scroll
		int scrollY = getScrollView().getScrollY();

		float newTop = Math.max(mPhotoHeightPixels, scrollY + mHeaderTopClearance);
		getHeaderBox().setTranslationY(newTop);
		getFabButton().setTranslationY(newTop + mHeaderHeightPixels
				- fabButtonHeightPixels / 2);

		getHeaderBackgroundBox().setPivotY(mHeaderHeightPixels);
		int gapFillDistance = (int) (mHeaderTopClearance * GAP_FILL_DISTANCE_MULTIPLIER);
		boolean showGapFill = !hasPhoto() || (scrollY > (mPhotoHeightPixels - gapFillDistance));
		float desiredHeaderScaleY = showGapFill ? ((mHeaderHeightPixels + gapFillDistance + 1) * 1f / mHeaderHeightPixels) : 1f;
		if (!hasPhoto()) {
			getHeaderBackgroundBox().setScaleY(desiredHeaderScaleY);
		} else if (mGapFillShown != showGapFill) {
			getHeaderBackgroundBox().animate()
			.scaleY(desiredHeaderScaleY)
			.setInterpolator(new DecelerateInterpolator(2f))
			.setDuration(250)
			.start();
		}
		mGapFillShown = showGapFill;

		LPreviewUtilsBase lpu = mLPreviewUtils;

		getHeaderShadow().setVisibility(lpu.hasLPreviewAPIs() ? View.GONE : View.VISIBLE);

		if (mHeaderTopClearance != 0) {
			// Fill the gap between status bar and header bar with color
			float gapFillProgress = Math.min(Math.max(UIUtils.getProgress(scrollY,
					mPhotoHeightPixels - mHeaderTopClearance * 2,
					mPhotoHeightPixels - mHeaderTopClearance), 0), 1);
			lpu.setViewElevation(getHeaderBackgroundBox(), gapFillProgress * mMaxHeaderElevation);
			lpu.setViewElevation(getHeaderBox(), gapFillProgress * mMaxHeaderElevation + 0.1f);
			lpu.setViewElevation(getFabButton(), gapFillProgress * mMaxHeaderElevation
					+ mFABElevation);
			if (!lpu.hasLPreviewAPIs()) {
				getHeaderShadow().setAlpha(gapFillProgress);
			}
		}

		// Move background photo (parallax effect)
		getPhotoViewContainer().setTranslationY(scrollY * 0.5f);
	}






	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}





}
