package io.edo.ui.imageutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.LSpace;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.adapters.filepage.FilePageHolder;
import io.edo.utilities.Constants;

public class FilePageLoader extends ImageLoader{
	private FragmentActivity c;
	private ThisApp app;
	private Executor executor;
    private ThumbnailFileLoader thumbnailFileLoader;

	boolean cropCenterImage = false;

	public static FilePageLoader getInstance(FragmentActivity fragmentActivity){
		ThisApp app = ThisApp.getInstance(fragmentActivity);
		if(app.filePageLoader==null){
			app.filePageLoader = new FilePageLoader(fragmentActivity);
		}
		return app.filePageLoader;
	}

	public FilePageLoader(FragmentActivity fragmentActivity){
		super(fragmentActivity, Constants.IMAGE_CACHE_DIR_NAME, getImageViewSideSize(fragmentActivity), getImageViewSideSize(fragmentActivity));
		this.c = fragmentActivity;
		this.app = ThisApp.getInstance(c);
		this.executor = Executors.newFixedThreadPool(2);
        this.thumbnailFileLoader = ThumbnailFileLoader.getInstance(fragmentActivity);
	}

	public static int getImageViewSideSize(Context c){
		return c.getResources().getDisplayMetrics().widthPixels;
	}

	@Override
	public Executor getExecutor() {
		return executor;
	}

	public void loadImage(LSpace space, LFile file, ImageView imageView)
	{
		ImageData imageData = new ImageData(space, file, null);
		loadDataToImageView(imageData, imageView);
	}

	public void loadImage(LSpace space, LFile file, FilePageHolder vh)
	{
		if(vh==null || vh.imageView==null)
			return;

        vh.showLoader();
        vh.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
		ImageData imageData = new ImageData(space, file, vh);
        loadDataToImageView(imageData, vh.imageView);
	}

	@Override
	public String getKeyFromData(Object data) {
		ImageData imageData = (ImageData) data;
		return imageData.file.getId();
	}

	@Override
	public Bitmap getLoadingImageFromData(Object data) {
		ImageData imageData = (ImageData) data;
        if(thumbnailFileLoader!=null) {
            BitmapDrawable bitmapDrawable = thumbnailFileLoader.getBitmapFromMemCache(imageData.file.getId());
            if (bitmapDrawable != null)
                return bitmapDrawable.getBitmap();

            Bitmap bitmap = thumbnailFileLoader.getBitmapFromDiskCache(imageData.file.getId());
            if(bitmap != null)
                return bitmap;

        }
        return null;
	}

	@Override
	protected Bitmap processBitmap(Object data) {
		try {
			ImageData imageData = (ImageData) data;
//			LSpace space = imageData.space;
			LFile file = imageData.file;

			if(file!=null){
				if(file.isFolder())
					return null;

				if(file.isSnippet())
					return ThumbnailManager.getSnippetPreview(c,file,mImageHeight);

				if(file.isStored()){
					if(file.isImageRaster()){
						return processBitmapFromLocalPath(file.getLocalUri());
					}else if(file.isVideo()){
						return ThumbnailManager.getVideoPreviewOfLocalFile(c,file.getLocalUri(),mImageHeight);
					}
				}

				String thumbLink = ThumbnailManager.getThumbnailWebLink(mContext,file, mImageHeight);
				return processBitmapFromHttpUrl(thumbLink);
			}
		} catch (Throwable ex){

			ex.printStackTrace();
			if(ex instanceof OutOfMemoryError){
				clearCache();
				closeCacheInternal();
			}
		}
		return null;
	}


	@Override
	public void onBitmapProcessed(Object data, ImageView imageView, Drawable value) {
		setImage((ImageData) data, imageView, value);
	}

	public void setImage(ImageData imageData, ImageView imageView, Drawable drawable) {


		if(drawable!=null){

            imageView.setImageDrawable(drawable);

			imageView.setBackgroundResource(R.color.transparent);

			if(cropCenterImage)
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

			if(imageData.vh !=null) {
				imageData.vh.showImageView();
			}else{
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			}

		}else{
            LFile file = imageData.file;


			int icon = ThumbnailManager.getNoPreviewRes(c, file);
			imageView.setImageResource(icon);
			if(file.isFolder() || file.isSpace()) {

				if (file.hasThumbnail()) {
					int color = Color.parseColor(file.getThumbnail());
					imageView.setBackgroundColor(color);
				} else {
					imageView.setBackgroundResource(R.color.edo_folder_default);
				}

			}else{
				String kind = file.getvType();

				int colorRes = ThumbnailManager.getColorRes(c, kind);
				imageView.setBackgroundResource(colorRes);
			}
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			if(imageData.vh !=null)
				imageData.vh.showImageView();
		}


	}


	public void setCropCenterImage(boolean cropCenterImage){
		this.cropCenterImage = cropCenterImage;
	}


	public class ImageData{
		public LSpace space;
		public LFile file;
		public FilePageHolder vh;

		public ImageData(LSpace space, LFile file, FilePageHolder vh) {
			this.space = space;
			this.file = file;
			this.vh = vh;
		}
	}

	public static Bitmap getBitmapFromDiskCache(Context c, String key){
		return getBitmapFromDiskCache(c,Constants.IMAGE_CACHE_DIR_NAME,key);
	}
}
