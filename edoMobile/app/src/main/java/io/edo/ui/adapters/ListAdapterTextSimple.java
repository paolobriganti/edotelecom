package io.edo.ui.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.edomobile.R;

public class ListAdapterTextSimple extends BaseAdapter {
	private final Context context;
	private final ArrayList<String> titles;
	private final Integer titlesColor;
	private final Integer bgColor;


	public ListAdapterTextSimple(Context context,Integer bgColor, ArrayList<String> titles, Integer titlesColor) {
		this.context = context;
		this.titles = titles;
		this.titlesColor = titlesColor;
		this.bgColor = bgColor;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		if(rowView==null)
			rowView = inflater.inflate(R.layout.spinner_item, parent, false);
		TextView tv_title = (TextView) rowView.findViewById(R.id.text);
		if(titles!=null && titles.size()>0 && JavaUtils.isNotEmpty(titles.get(position)))
			tv_title.setText(titles.get(position));
		else 
			tv_title.setText(" ");
		
		if(bgColor!=null)
			tv_title.setBackgroundColor(bgColor);
		
		if(titlesColor!=null)
			tv_title.setTextColor(titlesColor);
		
		
		return rowView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return titles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return titles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
} 