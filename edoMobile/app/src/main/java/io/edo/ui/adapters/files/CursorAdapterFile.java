package io.edo.ui.adapters.files;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Looper;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.paolobriganti.android.VU;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.Comment;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.edomobile.R;
import io.edo.ui.imageutils.ThumbnailFileLoader;

public class CursorAdapterFile extends CursorAdapter {
	private LayoutInflater mLayoutInflater;
	private LFileDAO lfileDAO;
	private Activity activity;
	public ThumbnailFileLoader thumbnailLoader;
	public int mImageThumbSize;

	String searchText;

	public CursorAdapterFile(Activity context, Cursor c, int mImageThumbSize, ThumbnailFileLoader thumbnailLoader, String searchText) {
		super(context, c);
		this.mLayoutInflater = LayoutInflater.from(context);
		this.lfileDAO = new LFileDAO(context);
		this.activity = context;
		this.thumbnailLoader = thumbnailLoader;
		this.mImageThumbSize = mImageThumbSize;
		this.searchText = searchText;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View rowView = mLayoutInflater.inflate(R.layout.row_edo_file, parent, false);
		return rowView;
	}


	@Override
	public void bindView(View rowView, Context context, Cursor c) {

		final LFile file = new LFile(c);

		TextView tv_title = (TextView) rowView.findViewById(R.id.tv_title);
		tv_title.setText(file.getNameWithExt());
		if(JavaUtils.isNotEmpty(searchText) && JavaUtils.isNotEmpty(tv_title.getText().toString())){
			String newName = tv_title.getText().toString();
			if(newName.toLowerCase().contains(searchText.toLowerCase())){
				tv_title.setText(VU.highlightText(newName, searchText, activity.getResources().getColor(R.color.green_light)));
			}
		}



		final TextView tv_subtitle = (TextView) rowView.findViewById(R.id.tv_sub_title);
		if(JavaUtils.isNotEmpty(searchText)){
			if(file.getCommentsJson()!=null && file.getCommentsJson().toLowerCase().contains(searchText.toLowerCase())){
				Comment[] comments = file.getComments();
				if(comments!=null && comments.length>0){
					for(Comment comment:comments){
						if(comment.getMessage().toLowerCase().contains(searchText.toLowerCase())){
							SpannableString spannable = VU.highlightText(comment.getMessage(), searchText, activity.getResources().getColor(R.color.green_light));
							tv_subtitle.setText(spannable);
							break;
						}
					}
				}
			}else{
				new Thread(){@Override public void run(){
					Looper.myLooper();
					EdoContainer container = lfileDAO.getContainerOf(file);
					final String subTitle = container.getNameComplete();
					activity.runOnUiThread(new Runnable() {public void run() {
						tv_subtitle.setText(subTitle);
					}});
				}}.start();
			}
		}

		ImageView iv_thumb = (ImageView) rowView.findViewById(R.id.thumb);
//		ThumbnailViews thumbViews = new ThumbnailViews(activity, rowView, mImageThumbSize, Constants.VISUALIZATION_LIST_SEARCH);
		thumbnailLoader.loadImage(file, iv_thumb);

	}
}
