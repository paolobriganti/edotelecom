package io.edo.ui.adapters.timeline;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.ui.imageutils.ThumbnailActivityLoader;

public class TimelineEventAdapter extends RecyclerView.Adapter<TimelineEventAdapter.ViewHolder> {

    private ArrayList<LActivity> events;
    private FragmentActivity c;
    private ThisApp app;

    private ThumbnailActivityLoader thumbnailActivityLoader;

    private ContactPhotoLoader contactPhotoLoader;

    private HashMap<String, RecyclerView.ViewHolder> activityHolders = new HashMap<>();

    LContact contact;

    public TimelineEventAdapter(FragmentActivity c, LContact contact, ThumbnailActivityLoader thumbnailActivityLoader, ArrayList<LActivity> events) {
        this.events = events;
        this.c = c;
        this.app = ThisApp.getInstance(c);
        this.contact = contact;
        this.contactPhotoLoader = ContactPhotoLoader.getInstance(c);
        this.thumbnailActivityLoader = thumbnailActivityLoader;
    }


    public void clearEvents() {
        int size = this.events.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                events.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void addEvents(List<LActivity> events) {
        this.events.addAll(events);
        this.notifyItemRangeInserted(0, events.size() - 1);
    }

    public void addEvent(int index, LActivity event) {
        this.events.add(index, event);
        this.notifyItemInserted(index);
    }

    public void addEvent(LActivity event) {
        this.events.add(0, event);
        this.notifyItemInserted(0);

    }

    public void updateEvent(LActivity event){
        int index = events.indexOf(event);
        if(index>=0){
            this.events.set(index, event);
            this.notifyDataSetChanged();
        }
    }

    public void updateEvent(int index, LActivity event){
        this.events.set(index, event);
        this.notifyDataSetChanged();
    }

    public void removeEvent(LActivity event){
        int index = events.indexOf(event);
        if(index>=0){
            this.events.remove(index);
            this.notifyItemRemoved(index);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_timeline_event, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder vh, int i) {
        final LActivity event = events.get(i);

        if(event.isChatEvent()) {

            if (event.isPersonal() || event.getSenderId().equals(app.getEdoUserId())) {
                final ChatViewHolder cvh = new ChatViewHolder(vh.getChatMeLayout());
                View view = cvh.setChatViews(c, contact, event);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mEventClickListener!=null)
                            mEventClickListener.onChatMessageClick(cvh,event);
                    }
                });
                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (mEventLongClickListener != null)
                            mEventLongClickListener.onChatMessageLongClick(cvh, event);
                        return true;
                    }
                });
                activityHolders.put(event.getId(), cvh);
            } else {
                final ChatViewHolder cvh = new ChatViewHolder(vh.getChatLayout());
                View view = cvh.setChatViews(c, contact, event);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mEventClickListener!=null)
                            mEventClickListener.onChatMyMessageClick(cvh, event);
                    }
                });
                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (mEventLongClickListener != null)
                            mEventLongClickListener.onChatMyMessageLongClick(cvh, event);
                        return true;
                    }
                });
                activityHolders.put(event.getId(), cvh);
            }

        }else if(event.getResourceType() == LActivity.RESOURCE_BLANC_SHEET) {
            final BlankSheetViewHolder bsvh = new BlankSheetViewHolder(vh.getBlancSheetLayout());
            View view = bsvh.setViews(c, event);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEventClickListener != null)
                        mEventClickListener.onBlancSheetClick(bsvh, event);
                }
            });

        }else if(event.getResourceType()== LActivity.RESOURCE_NEW_EVENTS){
            final NewEventsViewHolder nevh = new NewEventsViewHolder(vh.getNewEventsLayout());
            View view = nevh.setNewEventsViews(c, event);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEventClickListener != null)
                        mEventClickListener.onNewEventsViewClick(nevh);
                }
            });
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mEventLongClickListener != null)
                        mEventLongClickListener.onNewEventsViewLongClick(nevh);
                    return true;
                }
            });



        }else{
            final TimelineEventViewHolder tvh = new TimelineEventViewHolder(c,contact, contactPhotoLoader,vh.getTimelineEventLayout());
            View view = tvh.setEventViews(c, thumbnailActivityLoader, event);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mEventClickListener!=null){
                        if(event.isFileResource()){

                            if (mEventClickListener != null)
                                mEventClickListener.onFileEventViewClick(tvh,event);

                        }else{
                            if (mEventClickListener != null)
                                mEventClickListener.onTimelineEventClick(tvh,event);
                        }
                    }

                }
            });

            tvh.bt_comments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEventClickListener != null)
                        mEventClickListener.onFileEventCommentViewClick(tvh.bt_comments,event);
                }
            });
            tvh.bt_star.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEventClickListener != null)
                        mEventClickListener.onFileEventStarViewClick(tvh.bt_star, event);
                }
            });
            tvh.bt_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEventClickListener != null)
                        mEventClickListener.onFileEventShareViewClick(tvh.bt_share,event);
                }
            });


            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (event.isFileResource()) {
                        if (mEventLongClickListener != null)
                            mEventLongClickListener.onFileEventViewLongClick(tvh, event);
                    } else {
                        if (mEventLongClickListener != null)
                            mEventLongClickListener.onTimelineEventLongClick(tvh, event);
                    }
                    return true;
                }
            });
            activityHolders.put(event.getId(), tvh);
        }
    }

    @Override
    public int getItemCount() {
        return events == null ? 0 : events.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout ll_timeline_event;
        private LinearLayout ll_chat_me;
        private LinearLayout ll_chat;
        private LinearLayout ll_new_events;
        private LinearLayout ll_blancSheet;

        public ViewHolder(View itemView) {
            super(itemView);

            ll_timeline_event = (LinearLayout) itemView.findViewById(R.id.ll_timeline_event);
            ll_chat_me = (LinearLayout) itemView.findViewById(R.id.ll_chat_me);
            ll_chat = (LinearLayout) itemView.findViewById(R.id.ll_chat);
            ll_new_events = (LinearLayout) itemView.findViewById(R.id.ll_new_events);
            ll_blancSheet = (LinearLayout) itemView.findViewById(R.id.ll_blancSheet);
        }

        public View getTimelineEventLayout(){
            ll_timeline_event.setVisibility(View.VISIBLE);
            ll_chat_me.setVisibility(View.GONE);
            ll_chat.setVisibility(View.GONE);
            ll_new_events.setVisibility(View.GONE);
            ll_blancSheet.setVisibility(View.GONE);
            return ll_timeline_event;
        }
        public View getChatMeLayout(){
            ll_timeline_event.setVisibility(View.GONE);
            ll_chat_me.setVisibility(View.VISIBLE);
            ll_chat.setVisibility(View.GONE);
            ll_new_events.setVisibility(View.GONE);
            ll_blancSheet.setVisibility(View.GONE);
            return ll_chat_me;
        }
        public View getChatLayout(){
            ll_timeline_event.setVisibility(View.GONE);
            ll_chat_me.setVisibility(View.GONE);
            ll_chat.setVisibility(View.VISIBLE);
            ll_new_events.setVisibility(View.GONE);
            ll_blancSheet.setVisibility(View.GONE);
            return ll_chat;
        }
        public View getNewEventsLayout(){
            ll_timeline_event.setVisibility(View.GONE);
            ll_chat_me.setVisibility(View.GONE);
            ll_chat.setVisibility(View.GONE);
            ll_new_events.setVisibility(View.VISIBLE);
            ll_blancSheet.setVisibility(View.GONE);
            return ll_new_events;
        }
        public View getBlancSheetLayout(){
            ll_timeline_event.setVisibility(View.GONE);
            ll_chat_me.setVisibility(View.GONE);
            ll_chat.setVisibility(View.GONE);
            ll_new_events.setVisibility(View.GONE);
            ll_blancSheet.setVisibility(View.VISIBLE);
            return ll_blancSheet;
        }

    }

    public RecyclerView.ViewHolder getViewHolder(String actId){
        return activityHolders.get(actId);
    }




    EventClickListener mEventClickListener;

    public void setEventClickListener(EventClickListener listener) {
        mEventClickListener = listener;
    }

    public interface EventClickListener {
        void onTimelineEventClick(TimelineEventViewHolder vh, LActivity a);
        void onFileEventViewClick(TimelineEventViewHolder vh, LActivity a);
        void onFileEventCommentViewClick(View bt_comment, LActivity a);
        void onFileEventStarViewClick(View bt_star, LActivity a);
        void onFileEventShareViewClick(View bt_share, LActivity a);
        void onChatMessageClick(ChatViewHolder vh, LActivity a);
        void onChatMyMessageClick(ChatViewHolder vh, LActivity a);
        void onNewEventsViewClick(NewEventsViewHolder vh);
        void onBlancSheetClick(BlankSheetViewHolder vh, LActivity a);
    }

    EventLongClickListener mEventLongClickListener;

    public void setEventLongClickListener(EventLongClickListener listener) {
        mEventLongClickListener = listener;
    }

    public interface EventLongClickListener {
        void onTimelineEventLongClick(TimelineEventViewHolder vh, LActivity LActivity);
        void onFileEventViewLongClick(TimelineEventViewHolder vh, LActivity LActivity);
        void onChatMessageLongClick(ChatViewHolder vh, LActivity LActivity);
        void onChatMyMessageLongClick(ChatViewHolder vh, LActivity LActivity);
        void onNewEventsViewLongClick(NewEventsViewHolder vh);
    }
}
