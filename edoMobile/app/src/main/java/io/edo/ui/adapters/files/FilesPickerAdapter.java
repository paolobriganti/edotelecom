package io.edo.ui.adapters.files;

import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import io.edo.db.local.beans.LFile;
import io.edo.edomobile.R;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.views.SquareLinearLayoutOnWidth;

public class FilesPickerAdapter extends FilesAdapter {


    public FilesPickerAdapter(FragmentActivity c, ThumbnailFileLoader thumbnailFileLoader, ArrayList<LFile> files) {
        super(c, thumbnailFileLoader, files);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        SquareLinearLayoutOnWidth v = (SquareLinearLayoutOnWidth)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_edofile, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        viewHolder.file_container.setBackgroundResource(R.drawable.bg_card_black);
        return viewHolder;
    }

}
