package io.edo.ui.adapters.files;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.views.SquareLinearLayout;
import io.edo.views.SquareLinearLayoutOnWidth;

public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.ViewHolder> {

    private ArrayList<LFile> files;
    private Activity c;
    private ThisApp app;
    private ThumbnailFileLoader thumbnailFileLoader;

    private HashMap<String, ViewHolder> filesHolders = new HashMap<>();


    public FilesAdapter(FragmentActivity c, ThumbnailFileLoader thumbnailFileLoader, ArrayList<LFile> files) {
        this.c = c;
        this.app = ThisApp.getInstance(c);
        this.thumbnailFileLoader = thumbnailFileLoader;
        this.files = files;
    }


    public void clearFiles() {
        int size = this.files.size();
        if (size > 0) {
            this.files.clear();

            this.notifyDataSetChanged();
        }
    }

    public void addFiles(List<LFile> files) {
        this.files.addAll(files);
        this.notifyItemRangeInserted(0, files.size() - 1);
    }

    public void addFile(int index, LFile file) {
        this.files.add(index, file);
        this.notifyItemInserted(index);
    }

    public void addFile(LFile file) {
        this.files.add(0,file);
        this.notifyItemInserted(0);

    }

    public void updateFile(LFile file){
        int index = files.indexOf(file);
        if(index>=0){
            this.files.set(index, file);
            this.notifyDataSetChanged();
        }
    }

    public void updateFile(int index, LFile file){
        this.files.set(index, file);
        this.notifyDataSetChanged();
    }

    public void removeFile(LFile file){
        int index = files.indexOf(file);
        if(index>=0){
            this.files.remove(index);
            this.notifyDataSetChanged();
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        SquareLinearLayoutOnWidth v = (SquareLinearLayoutOnWidth) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_edofile, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder vh, int i) {
        final LFile file = files.get(i);

        vh.setFileViews(c, file, thumbnailFileLoader, mFileClickListener, mFileLongClickListener);

        filesHolders.put(file.getId(),vh);
    }

    @Override
    public int getItemCount() {
        return files == null ? 0 : files.size();
    }


    public static class ViewHolder extends FileOverlayHolder{
        public CardView card;
        public TextView tv_file;
        public ImageView iv_file;
        public View file_container;
        public View video_overlay;

        public View label_container;
        public SquareLinearLayout comments_container;
        public TextView tv_file_comment_count;

        public ViewHolder(SquareLinearLayoutOnWidth itemView) {
            super(itemView);

            card = (CardView) itemView.findViewById(R.id.card_file);

            file_container = itemView.findViewById(R.id.file_container);
            tv_file = (TextView) itemView.findViewById(R.id.tv_file);
            iv_file = (ImageView) itemView.findViewById(R.id.iv_file);
            video_overlay = itemView.findViewById(R.id.video_overlay);

            label_container = itemView.findViewById(R.id.label_container);
            comments_container = (SquareLinearLayout)itemView.findViewById(R.id.comments_container);
            if(android.os.Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
                comments_container.setBackgroundResource(R.drawable.ripple_white_transparent);
            }else{
                comments_container.setBackgroundResource(R.color.white);
            }

            tv_file_comment_count = (TextView) itemView.findViewById(R.id.tv_file_comment_count);

        }


        public ViewHolder(ImageView imageView) {
            super(imageView);

            iv_file = imageView;

        }


        public void setFileViews(Context c,
                                 final LFile file,
                                 final ThumbnailFileLoader thumbnailFileLoader,
                                 final FileClickListener mFileClickListener,
                                 final FileLongClickListener mFileLongClickListener){

            ThisApp app = ThisApp.getInstance(c);

            tv_file.setText(file.getName());

            if(file.hasComments()){
                comments_container.setVisibility(View.VISIBLE);
                tv_file_comment_count.setText(""+file.getCommentsCount());
            }else{
                comments_container.setVisibility(View.GONE);
                tv_file_comment_count.setText("");
            }

            thumbnailFileLoader.loadImage(file, this);

            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFileClickListener != null) {
                        mFileClickListener.onClick(ViewHolder.this, file);
                    }
                }
            });
            card.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (mFileLongClickListener != null) {
                        mFileLongClickListener.onLongClick(ViewHolder.this, file);
                    }
                    return true;
                }
            });


            setSelected(file.isSelected());

            LActivity a = app.uiManager.getActivityByResourceId(file.getId());
            if(a!=null && (a.isUploadEvent() || a.isDownloadEvent())){
                String text = c.getResources().getString(R.string.upload).toUpperCase();
                if(a.isDownloadEvent()){
                    text = c.getResources().getString(R.string.download).toUpperCase();
                }
                setProgress(a.getStatus(),a.getProgressPercent(),text);
            }
        }



    }


    public ViewHolder getViewHolder(String fileId){
        return filesHolders.get(fileId);
    }




    FileClickListener mFileClickListener;

    public void setFileClickListener(FileClickListener listener) {
        mFileClickListener = listener;
    }

    public interface FileClickListener {
        void onClick(ViewHolder v, LFile file);
    }

    FileLongClickListener mFileLongClickListener;

    public void setLongFileClickListener(FileLongClickListener listener) {
        mFileLongClickListener = listener;
    }

    public interface FileLongClickListener {
        void onLongClick(ViewHolder v, LFile file);
    }
}
