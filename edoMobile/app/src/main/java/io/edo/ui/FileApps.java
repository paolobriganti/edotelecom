package io.edo.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Looper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.paolobriganti.android.AUI;
import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.edomobile.R;
import io.edo.ui.EdoDialogListView.OnListItemClickListener;
import io.edo.ui.EdoDialogOne.OnPositiveClickListener;
import io.edo.ui.adapters.ListAdapterOptionsText;

public class FileApps {
	


	public static void openFileAs(final Context c, final LFile efile){
		final EdoDialogListView dialog = new EdoDialogListView(c);
		dialog.setTitle(R.string.openAs);
		String[] opts = c.getResources().getStringArray(R.array.openAsList);
		final String[] mimeTypes = c.getResources().getStringArray(R.array.openAsListValues);
		ArrayList<String> options = new ArrayList<String>(Arrays.asList(opts));
		ListAdapterOptionsText adapter = new ListAdapterOptionsText(c, options);
		dialog.setAdapter(adapter);
		dialog.setPositiveButtonText(R.string.cancel);
		dialog.setOnPositiveClickListener(new OnPositiveClickListener() {
	
			@Override
			public void onClick(View arg0) {
				dialog.cancel();
			}
		});
		dialog.setOnListItemClickListener(new OnListItemClickListener() {
	
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				Intent newIntent2 = new Intent(Intent.ACTION_VIEW);
				newIntent2.setDataAndType(Uri.fromFile(new File(efile.getLocalUri())),mimeTypes[position]);
				newIntent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				try {
					c.startActivity(newIntent2);
				} catch (android.content.ActivityNotFoundException e) {
				}
				dialog.cancel();
			}
		});
		dialog.show();
	}

	public static void openFileWithApp(final Activity c, final FileOptions fileOptions, EdoContainer container, final LSpace space, final LFile efile){
		if(efile==null)
			return;
		if(efile.isFolder()){
			new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
			LFileDAO fileDAO = new LFileDAO(c);
			LSpace space2 = space;
			space2.setCurrentFolder(efile);
			
			ArrayList<LFile> filesOfFolder = fileDAO.getSubFiles(efile, -1, 0, -1, false);
			space2.setFilesOfCurrentFolder(filesOfFolder);
			
			if(space2.getFilesOfCurrentFolder()!=null && space2.getFilesOfCurrentFolder().size()>0){
				EdoContainer container = fileDAO.getContainerOf(efile);
				c.finish();
				c.startActivity(EdoIntentManager.getFilePageIntent(c, container, space2, space2.getFilesOfCurrentFolder().get(0), null));
			}else{
				c.runOnUiThread(new Runnable() {public void run() {
					EdoUI.showToast(c, R.string.thisFolderIsEmpty, null, Toast.LENGTH_LONG);
				}});
			}
			}}.start();
			
		
		}else if(efile.isWebLink()){
			AUI.openBrowser(c, efile.getWebUri());
		}else if(efile.isGdoc()){
			AUI.openBrowser(c, efile.getWebUri());
//			new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
//			LService service = efile.getService(c, db, null);
//			if(service.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				ApiResponse resp = GoogleDriveEdo.getAlternateLink(c, db, service.getId(), efile);
//				String alternateLink = (String) (resp!=null && resp.result!=null ? resp.result : null);
//				AUI.openBrowser(c, alternateLink);
//			}
//			}}.start();
			
		}else if(efile.isStored()){
			String mimeType = efile.getType();
			if(JavaUtils.isEmpty(mimeType) || mimeType.contains("unknown")){
				mimeType = FileInfo.getMime(efile.getLocalUri());
			}
			File file = new File(efile.getLocalUri());
			if(JavaUtils.isNotEmpty(mimeType) && file.exists() && !mimeType.equals("application/octet-stream")){
				Intent newIntent = new Intent(Intent.ACTION_VIEW);
				newIntent.setDataAndType(Uri.fromFile(file),mimeType);
				newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				try {
					c.startActivity(newIntent);
				} catch (android.content.ActivityNotFoundException e) {
					openFileAs(c, efile);
				}
			}else{
				openFileAs(c, efile);
			}
		}else if(efile.isInCloud()){
			fileOptions.downloadFileAndCheckInternet(container, space, efile);
		}
	
	}

}
