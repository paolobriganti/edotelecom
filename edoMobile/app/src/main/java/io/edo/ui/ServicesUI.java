package io.edo.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;

import io.edo.db.local.LocalDBManager;
import io.edo.db.local.beans.LService;
import io.edo.edomobile.R;
import io.edo.ui.adapters.ListAdapterImageTextSubText;
import io.edo.ui.adapters.ListAdapterServices;

public class ServicesUI {

	public static ListAdapterImageTextSubText getUserServicesAdapter(Context c, ArrayList<LService> services){
		ArrayList<String> titles = new ArrayList<String>();
		ArrayList<String> subTitles = new ArrayList<String>();
		ArrayList<Drawable> images = new ArrayList<Drawable>();

		for(LService service:services){
			titles.add(service.getCommercialName(c));
			subTitles.add(service.getEmail());
			images.add(c.getResources().getDrawable(service.getIcon(c)));
		}

		return new ListAdapterImageTextSubText(c, null,
				titles, c.getResources().getColor(R.color.edo_black), 
				subTitles, c.getResources().getColor(R.color.grey), 
				images, c.getResources().getColor(R.color.transparent));
	}
	
	
	public static ListAdapterServices getCloudUserServicesAdapter(Context c, LocalDBManager db, ArrayList<LService> services){
		return new ListAdapterServices(c, db, services,
				null,
				c.getResources().getColor(R.color.edo_black), 
				c.getResources().getColor(R.color.grey), 
				c.getResources().getColor(R.color.transparent));
	}
	
}
