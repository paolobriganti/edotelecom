package io.edo.utilities;


import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Environment;

import com.paolobriganti.utils.imageLoader.Utils;

import java.io.File;

public class Constants {

	//*********************************************************************
	//*		ACTIONS
	//*********************************************************************
	public static final String ACTIONS_PREFIX = "io.edo.edomobile.";
	public static final String ACTION_ACTIVITY_FINISH = ACTIONS_PREFIX+"activityFinish";
	public static final String ACTION_LOGIN = ACTIONS_PREFIX+"login";
	public static final String ACTION_SEND_BY_EDO = ACTIONS_PREFIX+"sendByEdo";
	public static final String ACTION_SEND_MULTIPLE_BY_EDO = ACTIONS_PREFIX+"sendByEdo";
	public final static int EDIT_FILE_ACTION_COPY = 1;
	public final static int EDIT_FILE_ACTION_MOVE = 2;
	public static final int ACTION_DOWNLOAD = 10;
	public static final int ACTION_STOP_DOWNLOAD = 11;
	public static final int ACTION_UPLOAD = 20;
	public static final int ACTION_STOP_UPLOAD = 21;



	//*********************************************************************
	//*		CLOUD
	//*********************************************************************
	public static final String CLOUD_FOLDER_EDO_STORAGE = "edo storage";
	public static final String CLOUD_FOLDER_PERSONAL = "personal";
	public static final String CLOUD_FOLDER_CONTACT = "contacts";

	//*********************************************************************
	//*		SECTIONS
	//*********************************************************************
//	public static final String DETAIL_SECTION = "detailSection";
//	public static final String SECTIONS_SECTION  = "sectionsSection";


	//*********************************************************************
	//*		EXTRAS
	//*********************************************************************
	public static final String EXTRA_ACTION = "action";
	public static final String EXTRA_EMAIL = "email";
	public static final String EXTRA_EMAIL_MAX_DOWNLOAD_NUMBER = "maxEmailDownloadNumber";
	public static final String EXTRA_AFTER_DATE_MILLIS = "afterDateMillis";
	public static final String EXTRA_BEFORE_DATE_MILLIS = "beforeDateMillis";
	public static final String EXTRA_FILE_PATH = "file_path";
	public static final String EXTRA_PASSWORD = "password";
	public static final String EXTRA_TABS_SIZE = "tabs_size";
	public static final String EXTRA_TABS_INFO = "tabs_info";
	public static final String EXTRA_TABS_INFO_CONTAINER_ID = "tabs_info_container_id";
	public static final String EXTRA_TABS_INFO_TIME = "tabs_info_time";
	public static final String EXTRA_NEW_ISTANCE_STATE = "new_istance_state";
	public static final String EXTRA_SELECT_FOLDER = "selectFolder";
	public static final String EXTRA_CURRENT_SECTION= "currentSection";
	public static final String EXTRA_STATE = "state";
	public static final String STATE_IN_PROGRESS = "in_progress";
	public static final String STATE_ENDED = "ended";
	public static final String EXTRA_SHOW_NOTES = "showNote";
	public static final String EXTRA_SHOW_CHAT = "showChat";
	public static final String EXTRA_NEW_CHAT_MESSAGES = "newChatMessages";
	public static final String EXTRA_SHOW_TIMELINE = "showTimeline";
	public static final String EXTRA_PUSH = "push";
	public static final String EXTRA_PUSH_MSG_ID = "pushMsgId";
	public static final String EXTRA_NEW_FILE_ID = "newFileId";
	public static final String EXTRA_TEXT = "text";
	public static final String EXTRA_FILES = "files";
	public static final String EXTRA_TYPE = "type";
	public static final String EXTRA_TYPES = "types";
	//	public final static String EXTRA_START_PREVIOUS_ACTIVITY_ON_EXIT = "startPreviousActivityOnExit";
	public final static String EXTRA_UPDATE = "update";
	public final static String EXTRA_UPDATE_ALL = "updateAll";
	public final static String EXTRA_TRANSITION_INFOS = "transitionGridActivity";
	public static final String EXTRA_START_DESK_ACTIVITY = "startDeskActivity";

	//*********************************************************************
	//*		ID & INDEXES
	//*********************************************************************
	public static final String CATEGORY_INDEX = "categoryIndex";
	public static final String TAB_INDEX = "tabIndex";
	public static final String FILE_INDEX = "fileIndex";
	public static final String CONTACT_INDEX = "contactIndex";
//	public static final String SECTION_INDEX = "sectionIndex";
	public static final String LAST_TAB = "LASTCATEGORY";
	public static final String PARENT_DIR_ID = "PARENT_DIR_ID";
	public final static String CONTAINER_ID = "containerId";
	public final static String NOTE_INDEX = "note_index";
	public final static String CURRENT_ITEM = "currentItem";


	//*********************************************************************
	//*		FILES
	//*********************************************************************
	public static final String FILE_NAME_EDO_USER_INFO = "USR";
	public static final String FILE_NAME_PREFERENCES = "pref";
	public static final String FILE_NAME_TIME = "TS";


	//*********************************************************************
	//*		PATHS
	//*********************************************************************
//	public static final String EXTERNAL_SD_PATH = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) ?
//			Environment.getExternalStorageDirectory().toString() : null;
	public static final String EDO_FOLDER_NAME = "edo";
//	public static final String EDO_EXTERNAL_SD_PATH = EXTERNAL_SD_PATH!=null? (EXTERNAL_SD_PATH+File.separator+EDO_FOLDER_NAME):null;
	public static final String THUMB_CACHE_DIR_NAME = "thumbs";
	public static final String IMAGE_CACHE_DIR_NAME = "images";
	public static final String CONTACTS_CACHE_DIR_NAME = "avatars";
	public static final String AUDIO_CACHE_RECORDINGS = "AudioRecordings";
	public static final String ATTACHMENTS_CACHE = "attachments";

	public static File getDiskCacheDir(Context context, String uniqueName) {
		// Check if media is mounted or storage is built-in, if so, try and use external cache dir
		// otherwise use internal cache dir
		final String cachePath =
				Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
						!isExternalStorageRemovable() ? getExternalCacheDir(context).getPath() :
						context.getCacheDir().getPath();

		return new File(cachePath+ File.separator+ EDO_FOLDER_NAME + File.separator + uniqueName);
	}

	@TargetApi(Build.VERSION_CODES.FROYO)
	public static File getExternalCacheDir(Context context) {
//		if (Utils.hasFroyo()) {
//			return context.getExternalCacheDir();
//		}

		// Before Froyo we need to construct the external cache dir ourselves
//		final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
		File file = new File(Environment.getExternalStorageDirectory().getPath());
		if(file!=null && !file.exists()){
			file.mkdirs();
		}
		return file;
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public static boolean isExternalStorageRemovable() {
		if (Utils.hasGingerbread()) {
			return Environment.isExternalStorageRemovable();
		}
		return true;
	}



	//*********************************************************************
	//*		PICKERS - ON ACTIVITY RESULT
	//*********************************************************************
	public static final int FILE_PICKERS_SELECTION_LIMIT = 20;
	//	public static final int FILE_PAGE_RESULT = 0;
	public static final int IMAGE_PICKER_RESULT = 1;
	public static final int CAMERA_PHOTO_PICKER_RESULT = 2;
	public static final int CAMERA_VIDEO_PICKER_RESULT = 3;
	public static final int VIDEO_PICKER_RESULT = 4;
	public static final int AUDIO_PICKER_RESULT = 5;
	public static final int AUDIO_REC_PICKER_RESULT = 6;
	public static final int EDO_FILES_PICKER_RESULT = 7;
	public static final int EDO_GALLERY_PICKER_RESULT = 8;
	public final static int SPACE_EDITOR_RESULT = 9;
	public static final int EDO_GOOGLE_DRIVE_FILES_PICKER_RESULT = 13;
	public static final int CONTACT_PICKER_RESULT = 14;
	public static final int CONTACT_EDITOR_RESULT = 15;
	public static final int CONTACT_SELECT_RESULT = 16;
	public static final int CONTACT_SEARCH_RESULT_FOR_SEND = 17;
	public static final int CONTACT_CREATE_GROUP_RESULT = 18;



	//*********************************************************************
	//*		PLACEHOLDERS
	//*********************************************************************

	public static final String PH_CONTACTNAME = "__contactName__";
	public static final String PH_GROUPNAME = "__groupName__";
	public static final String PH_CONTACTID = "__contactId__";
	public static final String PH_CONTAINERNAME = "__containerName__";
	public static final String PH_FILENAME = "__fileName__";
	public static final String PH_FILETYPENAME = "__fileTypeName__";
	public static final String PH_SPACEICONNAME = "__spaceIconName__";
	public static final String PH_NUM = "__NUM__";
	public static final String PH_POS = "__POS__";
	public static final String PH_SERVICENAME = "__serviceName__";
	public static final String PH_SPACENAME = "__spaceName__";
	public static final String PH_FOLDERNAME = "__folderName__";
	public static final String PH_ORIGINALNAME = "__originalName__";
	public static final String PH_TOT = "__TOT__";
	public static final String PH_COLORED_TEXT = "__coloredText__";
	public static final String PH_USERNAME = "__userName__";
	public static final String PH_ISOWNER = "__ISOWNER__";
	public static final String PH_ADMINNAME = "__adminName__";



	//*********************************************************************
	//*		SECTIONS
	//*********************************************************************
	public static final int SECTION_NUMBER_PERSONAL = 0;
	public static final int SECTION_NUMBER_CONTACTS = 1;
	public static final int SECTION_NUMBER_SETTINGS = 2;
	public static final int SECTION_NUMBER_CONTACTS_EMAIL = 0;
	public static final int SECTION_NUMBER_CONTACTS_DESK = 0;
	public static final int SECTION_NUMBER_TIMELINE_EMAIL = 0;
	public static final int SECTION_NUMBER_TIMELINE_LOG = 0;
	public static final String SECTION_NAME_PERSONAL = "Personal";
	public static final String SECTION_NAME_CONTACTS = "Contacts";
	public static final String SECTION_NAME_GROUPS = "Groups";
	public static final String SECTION_NAME_TIMELINE = "Timeline";


	//*********************************************************************
	//*		RESPONSE
	//*********************************************************************
	public static final int RESPONSE_SUCCESS = 200;
	public static final int RESPONSE_FAILED = 400;
	public static final int RESPONSE_ERROR = 500;
	public static final int RESPONSE_NO_INTERNET_CONNECTION = 600;

	//*********************************************************************
	//*		SIZES
	//*********************************************************************
	public static final int THUMB_SIZE = 256;
	public static final float MENU_IMAGE_SIZE = 12.0F;
	public final static float NOTES_SCREEN_PERCENT = 100;
	public final static float ICON_INCH_SIZE = 0.7F;
	public static final int THUMB_PADDING_PERCENT = 6;
//	public static final int THUMB_MINI_PADDING_PERCENT = 5;
	public static final int THUMB_MICRO_PADDING_PERCENT = 2;
	public final static int IMAGE_SCREEN_PERCENT_DEFAULT = 50;
	public final static int IMAGE_SCREEN_PERCENT_MEDIUM = 20;
	public final static int IMAGE_SCREEN_PERCENT_SMALL = 8;
	public static final int EMAIL_MAX_DOWNLOAD_NUMBER = 20;
	public final static float WHEEL_SPEED = 0.01F;
	public final static float TRANSITION_ANIMATION_SCALE = 0.5F;



	//*********************************************************************
	//*		SORT
	//*********************************************************************
	public static final int SORT_BY_CDATE_DESC = 0;
	public static final int SORT_BY_CDATE_ASC = 1;
	public static final int SORT_BY_MDATE_DESC = 2;
	public static final int SORT_BY_MDATE_ASC = 3;
	public static final int SORT_BY_NAME_ASC = 4;
	public static final int SORT_BY_NAME_DESC = 5;
	public static final int SORT_BY_RANK_ASC = 6;
	public static final int SORT_BY_RANK_DESC = 7;



	//*********************************************************************
	//*		TAGS
	//*********************************************************************
	public static final String TAG_JSON_SYNTAX_EXCEPTION = "JsonSyntaxException";



	//*********************************************************************
	//*		TIME
	//*********************************************************************
	public static final int EMAIL_SYNC_PERIOD = 900000;
	public static final long EXPAND_TIME = 500;
	public static final long LOADING_PREVIEW_TIME = 250;
	public static final long LOADING_DESK_TIME = 100;
	public static final long DIALOG_ANIMATIONS_DURATION = 250;


	//*********************************************************************
	//*		VISUALIZATIONS
	//*********************************************************************
	public static final int VISUALIZATION_LIST_SEARCH = -1;
	public static final int VISUALIZATION_GRID = 0;
	public static final int VISUALIZATION_LIST = 1;
	public static final int VISUALIZATION_FILE_PAGE = 10;
	 

	//*********************************************************************
	//*		OTHER
	//*********************************************************************
	public static final String USER_NO = "nouser";




	//*********************************************************************
	//*		URL
	//*********************************************************************
	public static final String HOST_NAME_YOUTUBE_APP = "youtu.be";
}
