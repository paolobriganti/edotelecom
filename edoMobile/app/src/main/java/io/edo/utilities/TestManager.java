package io.edo.utilities;

import android.app.Activity;
import android.os.Looper;

import com.google.gson.Gson;
import com.paolobriganti.android.AUI;

import org.apache.http.HttpResponse;

import io.edo.db.web.request.HttpRequest;

public class TestManager {

	
	public static void timeTest(final Activity c){
		
		new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
		
		HttpResponse resp = HttpRequest.sendGet(ServiceStatus.URL, null);
		if(resp!=null){
			String json = HttpRequest.getStringResponse(resp);
			if(json!=null){
				ServiceStatus status = new TestManager().new ServiceStatus().fromJson(json);
				if(status!=null){
					
					long serverTime = (long) (status.time_stamp * 1000);
					long systemTime = System.currentTimeMillis();
					
					long diffHours = serverTime - systemTime;
					
					final String diffStamp = diffHours + " ms";
					
					String serverTimeStampNotConverted = DateManager.getDateTimeDisplayFromMillis(c, serverTime);
					String currentTimeStamp = DateManager.getDateTimeDisplayFromMillis(c, systemTime);
					
					final String stamp = 
							"Server time not converted: " + serverTimeStampNotConverted +"\n\n" +
							"System time: " + currentTimeStamp +"\n\n"+
							"Diff time: " + diffStamp +"\n\n";
					
					c.runOnUiThread(new Runnable() {public void run() {
						AUI.oneButtonDialog(c, "TIME", stamp);
					}});
					
					
					
				}
			}
		}
		
		}}.start();
	}
	
	
	public class ServiceStatus{
		public static final String URL = "https://service.edo.io/status";
		
		public long tot_usrs;
		public long reg_web;
		public String version;
		public long tot_reg;
		public long online_dsk;
		public float time_stamp;
		public long reg_dsk;
		public long online_web;
		public long reg_mob;
		public String svr_id;
		
		public String toJson(){
			return new Gson().toJson(this);
		}

		public ServiceStatus fromJson(String json){
			try{
				return new Gson().fromJson(json, ServiceStatus.class);
			}catch (com.google.gson.JsonSyntaxException e){
				eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "ServiceStatus:\n"+e.getMessage());
				eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "ServiceStatus JSON:\n"+json);
				return null;
			}

		}
		
	}
}
