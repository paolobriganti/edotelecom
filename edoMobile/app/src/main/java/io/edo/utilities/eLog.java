package io.edo.utilities;

import android.util.Log;

import io.edo.db.web.request.EdoRequest;

public class eLog {

	private static final boolean isTest = EdoRequest.IS_TEST_APP;
	
	public static int d(String tag, String msg) {
		if(isTest)
			return Log.d(tag, msg);
		return Log.DEBUG;
	}
	
	public static int e(String tag, String msg) {
		if(isTest)
			return Log.e(tag, msg);
		return Log.ERROR;
	}
	
	public static int i(String tag, String msg) {
		if(isTest)
			return Log.i(tag, msg);
		return Log.INFO;
	}
	
	public static int v(String tag, String msg) {
		if(isTest)
			return Log.v(tag, msg);
		return Log.VERBOSE;
	}
	
	public static int w(String tag, String msg) {
		if(isTest)
			return Log.w(tag, msg);
		return Log.WARN;
	}
	
	public static int wtf(String tag, String msg) {
		if(isTest)
			return Log.wtf(tag, msg);
		return Log.WARN;
	}
}
