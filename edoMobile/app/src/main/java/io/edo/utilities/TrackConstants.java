package io.edo.utilities;

public class TrackConstants {

	public static final String SCREEN_NAME_LOGIN = "Login Screen";
	public static final String SCREEN_NAME_ACTIVE_CONTACTS_LIST = "Active Contacts List Screen";
	public static final String SCREEN_NAME_PERSONAL = "Personal Screen";
	public static final String SCREEN_NAME_CONTACT = "Contact Screen";
	public static final String SCREEN_NAME_GROUP = "Group Screen";

	public static final String EVENT_LOGIN_WITH_GOOGLE_CLICK = "Login With Google Click";
	public static final String EVENT_LOGIN_SELECT_ACCOUNT_DIALOG = "Select Account Dialog";
	public static final String EVENT_LOGIN_ACCOUNT_SELECTED = "Account Selected";
	public static final String EVENT_LOGIN_NEW = "New User";
	public static final String EVENT_LOGIN_RETURN = "Return User";
	public static final String EVENT_LOGIN_CONTINUE_BUTTON = "Continue Button Click";

	public static final String EVENT_CLICK_ON_ADD_CONTACT = "Add Contact Click";
	public static final String EVENT_START_SHARING = "Start sharing";
	public static final String EVENT_SEARCH_CONTACT = "Search Contact";
	public static final String EVENT_CREATE_CONTACT = "Create Contact";
	public static final String EVENT_CREATE_GROUP = "Create Group";

	public static final String EVENT_TIMELINE = "Timeline";
	public static final String EVENT_SPACES = "Spaces";
	public static final String EVENT_NOTES = "Notes";
	public static final String EVENT_ADD_FILE_ON_TIMELINE_CLICK = "Click add file to Timeline ";
	public static final String EVENT_ADD_SPACE_CLICK = "Click on add Space";
	public static final String EVENT_ADD_FILE_ON_SPACE_CLICK = "Click on add file to Space";
	public static final String EVENT_ADD_NOTE_ON_NOTES_CLICK = "Click add note on Notes";
	public static final String EVENT_ORGANIZE_FROM_EDO = "Organize from edo";
	public static final String EVENT_SHARE_FROM_EDO = "Share from edo";
	public static final String EVENT_ORGANIZE_FROM_TPA = "Organize from tpa";
	public static final String EVENT_SHARE_FROM_TPA = "Organize from tpa";

	public static final String KEY_ACTION = "action";


	public static final String KEY_ACCOUNT_PRESENT_IN_THE_DEVICE_SELECTED = "Account present in the device selected";
	public static final String KEY_CONTEXT = "context";
	public static final String VALUE_CONTEXT_PERSONAL = "Personal";
	public static final String VALUE_CONTEXT_CONTACTS = "Contacts";
	public static final String VALUE_CONTEXT_GROUP = "Groups";

}
