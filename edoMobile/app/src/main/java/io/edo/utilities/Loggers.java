package io.edo.utilities;

import java.util.logging.Logger;

public final class Loggers {
	/**
	 * The prefix for all logger names.
	 */
	public static final String PREFIX = "org.bson";
	/**
	 * Gets a logger with the given suffix appended on to {@code PREFIX}, separated by a '.'.
	 *
	 * @param suffix the suffix for the logger.
	 * @return the logger
	 * @see io.edo.utilities.Loggers#PREFIX
	 */
	public static Logger getLogger(final String suffix) {
		if (suffix == null) {
			throw new IllegalArgumentException("suffix can not be null");
		}
		if (suffix.startsWith(".") || suffix.endsWith(".")) {
			throw new IllegalArgumentException("The suffix can not start or end with a '.'");
		}
		return Logger.getLogger(PREFIX + "." + suffix);
	}
	private Loggers() {
	}
}
