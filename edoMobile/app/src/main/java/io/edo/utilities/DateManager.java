package io.edo.utilities;

import android.content.Context;
import android.text.format.DateFormat;

import java.util.Calendar;

import io.edo.edomobile.R;

public class DateManager {


	public static long SECOND_IN_MILLIS =  1000;
	public static long MINUTE_IN_MILLIS = 60000;
	public static long HOUR_IN_MILLIS = 3600000;
	public static long DAY_IN_MILLIS = 86400000;

	public static String FORMAT_DAY_NAME_LONG = "EEEE";
	public static String FORMAT_DAY_NAME_SHORT = "EEE";
	public static String FORMAT_MONTH_NAME_LONG = "MMMM";
	public static String FORMAT_MONTH_NAME_SHORT = "MMM";




	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//*		TIME FORMATS
	//*****************************************************************************************************************************************************************************************************************************************************************************************************


	public static String getTimeFormat(Context c){
		return c.getResources().getString(R.string.timeFormat);
	}
	public static String getDateFormat(Context c){
		return c.getResources().getString(R.string.dateFormat);
	}
	public static String getWeekDayTimeFormat(Context c){
		return c.getResources().getString(R.string.weekDayTimeFormat);
	}
	public static String getNumberDayWeekDayTimeFormat(Context c){
		return c.getResources().getString(R.string.numberDayWeekDayTimeFormat);
	}
	public static String getMonthTimeFormat(Context c){
		return c.getResources().getString(R.string.monthTimeFormat);
	}
	public static String getDateTimeFormat(Context c){
		return c.getResources().getString(R.string.dateTimeFormat);
	}



	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//*		LOCAL TIME
	//*****************************************************************************************************************************************************************************************************************************************************************************************************

	public static String getCurrentDateDisplay(Context c){
		return DateFormat.format(getDateFormat(c),System.currentTimeMillis()).toString();
	}
	public static String getCurrentDateTimeDisplay(Context c){
		return DateFormat.format(getDateTimeFormat(c),System.currentTimeMillis()).toString();
	}
	public static String getTimeDisplayFromMillis(Context c, long millis){
		return DateFormat.format(getTimeFormat(c),millis).toString();
	}
	public static String getDateDisplayFromMillis(Context c, long millis){
		return DateFormat.format(getDateFormat(c),millis).toString();
	}
	public static String getDateTimeDisplayFromMillis(Context c, long millis){
		return DateFormat.format(getDateTimeFormat(c),millis).toString();
	}
	public static String getNumberDayWeekDayTimeDisplayFromMillis(Context c, long millis){
		return DateFormat.format(getNumberDayWeekDayTimeFormat(c),millis).toString();
	}
	public static String getWeekDayTimeDisplayFromMillis(Context c, long millis){
		return DateFormat.format(getWeekDayTimeFormat(c),millis).toString();
	}
	public static String getMonthTimeDisplayFromMillis(Context c, long millis){
		return DateFormat.format(getMonthTimeFormat(c),millis).toString();
	}



	public static String getUserFriendlyDateFromMillis(Context c, Long millis){
		if(millis!=null){
			Calendar calendar = Calendar.getInstance();
			Calendar fileDate = Calendar.getInstance();
			fileDate.setTimeInMillis(millis);

			long timeDistance = calendar.getTimeInMillis()-fileDate.getTimeInMillis();

			if(fileDate.get(Calendar.DAY_OF_YEAR)==calendar.get(Calendar.DAY_OF_YEAR) && 
					fileDate.get(Calendar.MONTH)==calendar.get(Calendar.MONTH) && 
					fileDate.get(Calendar.YEAR)==calendar.get(Calendar.YEAR)){

				return DateManager.getTimeDisplayFromMillis(c, millis);

			}else if(calendar.get(Calendar.DAY_OF_YEAR)-fileDate.get(Calendar.DAY_OF_YEAR)>0 && timeDistance<(DAY_IN_MILLIS)){

				return c.getResources().getString(R.string.yesterdayAt)+" "+DateManager.getTimeDisplayFromMillis(c, millis);

//			}else if(calendar.get(Calendar.DAY_OF_YEAR)-fileDate.get(Calendar.DAY_OF_YEAR)<3 && timeDistance>(DAY_IN_MILLIS)){
//
//				return c.getResources().getString(R.string.twoDaysAgoAt)+" "+DateManager.getTimeDisplayFromMillis(c, millis);

			}else if(fileDate.get(Calendar.WEEK_OF_YEAR)==calendar.get(Calendar.WEEK_OF_YEAR) && 
					fileDate.get(Calendar.YEAR)==calendar.get(Calendar.YEAR)){

				return DateManager.getWeekDayTimeDisplayFromMillis(c, millis);

			}else if(fileDate.get(Calendar.MONTH)==calendar.get(Calendar.MONTH) && 
					fileDate.get(Calendar.YEAR)==calendar.get(Calendar.YEAR)){

				return DateManager.getNumberDayWeekDayTimeDisplayFromMillis(c, millis);

			}else if(fileDate.get(Calendar.YEAR)==calendar.get(Calendar.YEAR)){

				return DateManager.getMonthTimeDisplayFromMillis(c, millis);

			}else{
				return DateManager.getDateTimeDisplayFromMillis(c, millis);
			}
		}
		return null;
	}






	public static String getCurrentDayName(){
		return getDayName(System.currentTimeMillis());
	}
	public static String getDayName(int year, int month, int day){
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		return getDayName(calendar.getTimeInMillis());
	}
	public static String getDayName(long millis){
		return DateFormat.format(FORMAT_DAY_NAME_LONG,millis).toString();
	}
	public static String getDayNameShort(long millis){
		return DateFormat.format(FORMAT_DAY_NAME_SHORT,millis).toString();
	}

	public static String getCurrentMonthName(){
		return getMonthName(System.currentTimeMillis());
	}
	public static String getMonthName(int month){
		Calendar calendar = Calendar.getInstance();
		calendar.set(1970, month, 1);
		return getMonthName(calendar.getTimeInMillis());
	}
	public static String getMonthName(long millis){
		return DateFormat.format(FORMAT_MONTH_NAME_LONG,millis).toString();
	}
	public static String getMonthNameShort(long millis){
		return DateFormat.format(FORMAT_MONTH_NAME_SHORT,millis).toString();
	}



	public static String convertMillsToHourMinSec(long millis){
		int seconds = (int) (millis / 1000);
		int minutes = seconds / 60;
		seconds = seconds % 60;
		return String.format("%d:%02d", minutes, seconds);
	}


	public static Boolean isNight(){
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		return hour < 6 || hour > 18;
	}







}