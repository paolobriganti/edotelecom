package io.edo.settings;

import android.content.Context;

import com.google.gson.Gson;
import com.paolobriganti.utils.StringFile;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class EdoTimes {

	private Long lastEmailSync;
	
	public EdoTimes() {
		super();
	}

	public EdoTimes(Context context) {
		super();
		load(context);
	}

	public Long getLastEmailSync() {
		return lastEmailSync;
	}

	public void setLastEmailSync(Long lastEmailSync) {
		this.lastEmailSync = lastEmailSync;
	}

	public void save(Context c){
		String json = toJson();
		StringFile.write(c, Constants.FILE_NAME_TIME, json);
	}

	public EdoTimes load(Context c){
		if(StringFile.fileExists(c, Constants.FILE_NAME_TIME)){
			String json = StringFile.read(c, Constants.FILE_NAME_TIME);
			EdoTimes pref = fromJson(json);
			this.lastEmailSync = pref.lastEmailSync;
			return pref;
		}else{
			return null;
		}
	}

	private String toJson(){
		return new Gson().toJson(this);
	}

	private static EdoTimes fromJson(String json){

		try{
			return new Gson().fromJson(json, EdoTimes.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "EdoTimes:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "EdoTimes JSON:\n"+json);
			return null;
		}
	}
}
