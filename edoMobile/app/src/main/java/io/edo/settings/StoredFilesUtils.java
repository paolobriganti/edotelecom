package io.edo.settings;

import java.io.File;

public class StoredFilesUtils {

	public static String getNewFilePath(String filePath, String fileName, String extension){
		
		
		File dir = new File (filePath);
		dir.mkdirs();
		
		filePath = filePath + fileName;
		
		
		File file = new File(filePath+"."+extension);
		if(file.exists()){
			String newPath = filePath;
			int count = 1;
			while(file.exists()){
				newPath = filePath + " ("+count+")";
				file = new File(newPath+"."+extension);
				count++;
			}
			filePath = newPath;
		}
		
		filePath = filePath+"."+extension;
		
		
		return filePath;
	}
	
}
