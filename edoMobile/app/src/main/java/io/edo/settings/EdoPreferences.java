package io.edo.settings;

import android.content.Context;

import com.google.gson.Gson;
import com.paolobriganti.utils.StringFile;

import java.io.File;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class EdoPreferences {
	

	private boolean isNotySoundEnabled = true;
	private int filesView = Constants.VISUALIZATION_GRID;
	
	public EdoPreferences() {
		super();
	}

	public EdoPreferences(Context context) {
		super();
		load(context);
	}
	
	public static long getLastPreferencesUpdate(Context c){
		File file = c.getFileStreamPath(Constants.FILE_NAME_PREFERENCES);
		if(file!=null && file.exists())
			return file.lastModified();
		return 0;
	}


	public boolean isNotySoundEnabled() {
		return isNotySoundEnabled;
	}

	public void setNotySoundEnabled(boolean isNotySoundEnabled) {
		this.isNotySoundEnabled = isNotySoundEnabled;
	}

	public int getFilesView() {
		return filesView;
	}

	public void setFilesView(int filesView) {
		this.filesView = filesView;
	}

	public void save(Context c){
		String json = toJson();
		StringFile.write(c, Constants.FILE_NAME_PREFERENCES, json);
	}

	public EdoPreferences load(Context c){
		if(StringFile.fileExists(c, Constants.FILE_NAME_PREFERENCES)){
			String json = StringFile.read(c, Constants.FILE_NAME_PREFERENCES);
			EdoPreferences pref = fromJson(json);
			this.isNotySoundEnabled = pref.isNotySoundEnabled;
			this.filesView = pref.filesView;
			return pref;
		}else{
			return null;
		}
	}

	private String toJson(){
		return new Gson().toJson(this);
	}

	private static EdoPreferences fromJson(String json){

		try{
			return new Gson().fromJson(json, EdoPreferences.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "EdoPreferences:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "EdoPreferences JSON:\n"+json);
			return null;
		}
	}


}
