package io.edo.edomobile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.paolobriganti.android.AUtils;
import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.ArrayList;

import io.edo.api.linkparser.LinkParser;
import io.edo.db.global.beans.ActivityDAO;
import io.edo.db.global.beans.StatusDAO;
import io.edo.db.global.uibeans.ActivityUIDAO;
import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.global.uibeans.MessageUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LActivities;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LSpaceDAO;
import io.edo.db.local.beans.support.LocalFiles;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoIntentManager;
import io.edo.ui.EdoNotification;
import io.edo.ui.EdoUI;
import io.edo.ui.FileOptions;
import io.edo.ui.FilePickers;
import io.edo.ui.TransitionInfos;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.timeline.BlankSheetViewHolder;
import io.edo.ui.adapters.timeline.ChatViewHolder;
import io.edo.ui.adapters.timeline.NewEventsViewHolder;
import io.edo.ui.adapters.timeline.TimelineBaseViewHolder;
import io.edo.ui.adapters.timeline.TimelineEventAdapter;
import io.edo.ui.adapters.timeline.TimelineEventViewHolder;
import io.edo.ui.imageutils.ThumbnailActivityLoader;
import io.edo.utilities.Constants;
import io.edo.utilities.TrackConstants;

public class Frag_ContactTimeline extends Frag_Base
        implements UIManager.ActivitiesUpdatesListener,
        TimelineEventAdapter.EventClickListener, TimelineEventAdapter.EventLongClickListener{

    private final static String LISTENER_ID = Frag_ContactTimeline.class.getName();

    private ActivityUIDAO activityDAO;
    private MessageUIDAO messageDAO;
    private LActivities activities = new LActivities();
    private TimelineEventAdapter mAdapter;
    private LinearLayoutManager lm;

    private EditText et_chat;

    private int screenHeight = 0;


    private ThumbnailActivityLoader thumbnailActivityLoader;

    boolean canSendMessage = true;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Frag_ContactTimeline() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityDAO = new ActivityUIDAO(c);
        messageDAO = new MessageUIDAO(c);

        thumbnailActivityLoader = ThumbnailActivityLoader.getInstance(getActivity());

        screenHeight = c.getResources().getDisplayMetrics().heightPixels;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater,container,savedInstanceState);

        et_chat = (EditText) rootView.findViewById(R.id.et_chat);

        et_chat.addTextChangedListener(new TextWatcher() {
            int previousCount = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int charCount = s != null ? s.length() : 0;
                if (charCount > 0 && previousCount <= 0) {
                    ((ImageButton)fabButton).setImageResource(R.drawable.ic_action_send_now_w);
                } else if (charCount <= 0 && previousCount > 0) {
                    ((ImageButton)fabButton).setImageResource(R.drawable.ic_action_new_w);
                }

                previousCount = charCount;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lm = new LinearLayoutManager(c);
        lm.setReverseLayout(true);
        mRecyclerView.setLayoutManager(lm);

        mAdapter = new TimelineEventAdapter(getActivity(), contact, thumbnailActivityLoader, activities.getactivities());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setEventClickListener(this);
        mAdapter.setEventLongClickListener(this);

        loadEvents();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        app.uiManager.addActivitiesUpdatesListener(LISTENER_ID, this);
    }

    @Override
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public ImageLoader getImageLoader() {
        return app.thumbnailFileLoader;
    }


    @Override
    public void onPause() {
        super.onPause();

    }



    public void onFabButtonClick(View v){
        String text = et_chat.getText().toString();
        if(JavaUtils.isNotEmpty(text)){
            if(JavaUtils.isWebUrl(text)){
                processLink(text);
            }else
                sendMessage(text);
        }else{

            app.sendEventTrack(TrackConstants.EVENT_ADD_FILE_ON_TIMELINE_CLICK, TrackConstants.KEY_CONTEXT, contact.isPersonal(c)? TrackConstants.VALUE_CONTEXT_PERSONAL : (contact.isGroup() ? TrackConstants.VALUE_CONTEXT_GROUP : TrackConstants.VALUE_CONTEXT_CONTACTS));
            filesPicker.pickersSlider(getCurrentContainer(),getTimelineSpace(), gv_pickers, slidingPickers, FilePickers.CONDITION_NO_FOLDERS);
        }
    }




    void processLink(String plainText){
        new LinkTask(plainText).execute();
    }

    class LinkTask extends AsyncTask<String, String, LFile> {

        String plainText;

        public LinkTask(String plainText){

            this.plainText = plainText;
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            loading.setTitle(R.string.webPage);
            loading.setSubTitle(R.string.loading);
            loading.show();

        }

        @Override
        protected LFile doInBackground(String... params) {

            if(JavaUtils.isWebUrl(plainText)){
                return LinkParser.getThumbnailAndNameOfWebLink(c, plainText);
            }

            return null;
        }

        @Override
        protected void onPostExecute(LFile result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if(result!=null){
                filesPicker.addFilesToSpace(getCurrentContainer(), getTimelineSpace(), new LocalFiles(result));
                et_chat.setText("");
            }else
                sendMessage(plainText);

            loading.dismiss();

        }

    }




    @Override
    public int getContentViewResourceId() {
        return R.layout.fragment_contact_timeline;
    }

    @Override
    public boolean canAddFirstSpace() {
        return true;
    }

    @Override
    public boolean canHideFooterOnScroll() {
        return false;
    }


    //*******************************
    //* Events
    //*******************************
    @Override
    public void onRefresh() {
        if(app.isUpdateInProgress) {
            srlayout.setRefreshing(false);
            return;
        }
        srlayout.setRefreshing(false);
//        AUtils.executeAsyncTask(new EventsTask(true), "");
    }

    public void loadEvents(){
        AUtils.executeAsyncTask(new EventsTask(false), "");
    }


    class EventsTask extends AsyncTask<String, String, LActivities> {

        int offset = -1;

        boolean isRefreshing = false;

        public EventsTask(boolean isRefreshing) {
            this.isRefreshing = isRefreshing;
        }

        @Override
        protected void onPreExecute() {
            app.isUpdateInProgress = true;

            if(isRefreshing){
                srlayout.setRefreshing(true);
            }

            if(mAdapter!=null && mAdapter.getItemCount()>0)
                offset = mAdapter.getItemCount();


            super.onPreExecute();
        }

        @Override
        protected LActivities doInBackground(String... params) {
            LContact contact = ((LContact)getCurrentContainer());
            canSendMessage = contact.isEdoUser();
            if(contact.isEdoUser()){
                if(contact.getGroupMembersCount()>0){
                    canSendMessage = false;
                    for(LContact member:contact.getGroupMembers(c)){
                        if(member.isEdoUser()){
                            canSendMessage = true;
                        }
                    }
                }
            }


            if(isRefreshing){
                new ActivityDAO(c).downloadAllBy(getCurrentContainer(), offset, true, null);

            }

            if(getTimelineSpace() !=null) {
                return activityDAO.getLDAO().getListBy(getCurrentContainer());
            }else{
                onNoTimelineSpaceFound();
            }


            return null;
        }


        @Override
        protected void onPostExecute(final LActivities result) {
            app.isUpdateInProgress = false;
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    mAdapter.clearEvents();

                    if (result != null) {
                        if (mAdapter != null) {
                            mAdapter.addEvents(result.getactivities());

                            if(isRefreshing && mAdapter.getItemCount()>0){
                                lm.scrollToPosition(mAdapter.getItemCount()-1);
                            }else {
                                int firstUnreadIndex = result.getactivities().indexOf(LActivities.getNewEventsMsg(c, 0));
                                if (firstUnreadIndex >= 0) {
                                    lm.scrollToPositionWithOffset(firstUnreadIndex, screenHeight / 2);
                                }
                            }
                        }

                        activities.setLastNotificationUnread(result.getLastNotificationUnread());

                        if(result.getUnreadNumber()>0) {
                            setAllEventsRed();
                            updateWebStatus();
                            EdoNotification.cancelNotification(c,EdoNotification.ID_CONTACTS);
                        }


                    }
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                    if (isRefreshing) {
                        srlayout.setRefreshing(false);
                    }
                }
            });
            this.cancel(true);

        }
    }

    @Override
    public void onPostDefaultSpaceCreation(LSpace defaultSpace) {
        if(defaultSpace!=null){
            loadEvents();
        }
    }

    @Override
    public LSpace getCurrentSpace() {
        return getTimelineSpace();
    }

    public void setAllEventsRed(){
        if(getCurrentContainer()!=null && getCurrentContainer().isContactOrGroup())
            new AllEventsRedTask().execute();
    }

    class AllEventsRedTask extends AsyncTask<String, String, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean done = activityDAO.getLDAO().setAllActivitiesRedBy(getCurrentContainer().getId(), getCurrentContainer().getEdoUserId());
            if(!getCurrentContainer().isPersonal(c)) {
                boolean contactHighlighted = new ContactUIDAO(c).highlightContact((LContact) getCurrentContainer(), null, true);
            }
            return true;
        }
    }

    public void updateWebStatus(){
        if(getCurrentContainer()!=null && !getCurrentContainer().isPersonal(c))
            new UpdateWebStatusTask().execute();
    }

    class UpdateWebStatusTask extends AsyncTask<String, String, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            if(activities.getLastNotificationUnread()!=null) {
                new StatusDAO(c).update(getCurrentContainer().getContainerId(), activities.getLastNotificationUnread());
                activities.setLastNotificationUnread(null);
            }
            return true;
        }
    }


    @Override
    public void onTimelineEventClick(TimelineEventViewHolder v, LActivity a) {

    }

    @Override
    public void onTimelineEventLongClick(TimelineEventViewHolder vh, LActivity a) {
        addSelectedMessage(vh, a);
    }

    @Override
    public void onNewEventsViewLongClick(NewEventsViewHolder vh) {

    }

    @Override
    public void onNewEventsViewClick(NewEventsViewHolder v) {

    }

    @Override
    public void onBlancSheetClick(BlankSheetViewHolder vh, LActivity a) {
        removeEventFromList(a);
    }

    @Override
    public void onFileEventViewClick(TimelineEventViewHolder v, LActivity a) {
        if(!a.isSelected() && !istimelineOptionsActionModeEnabled) {

            if(a.getEvent()==LActivity.EVENT_DELETE)
                return;

            LFile file = a.getFileResource();
            if (file != null) {
                if(a.getVtype()!=null && a.getVtype().equals(LFile.FILE_TYPE_EDOTAB)) {
                    LSpace space = new LSpaceDAO(c).getSpaceById(contact, a.getResourceId());
                    if(space!=null) {
                        c.startActivity(EdoIntentManager.getContactsIntent(c, contact, space));
                    }
                } else if(a.getVtype()!=null && a.getVtype().equals(FileInfo.TYPE_FOLDER)) {
                    LFile folder = fileDAO.getLDAO().get(a.getResourceId());
                    if(folder!=null) {
                        LSpace space = folder.getSpace(c);
                        if (space != null) {
                            space.setCurrentFolder(folder);
                            c.startActivity(EdoIntentManager.getContactsIntent(c, contact, space));
                        }
                    }
                } else if (a.getVtype()!=null && a.getVtype().equals(LFile.FILE_TYPE_SNIPPET)){
                    LFile snippet = fileDAO.getLDAO().get(file.getId());
                    c.startActivity(EdoIntentManager.getSnippetIntent(c, getCurrentContainer(), snippet.getSpace(c), snippet, null));

                }else{
                    TransitionInfos transitionInfo = new TransitionInfos(c, v.cardView, file.getId(), file.getName(), file.getvType());
                    Intent filePage = EdoIntentManager.getFilePageIntent(c, getCurrentContainer(), null, file, transitionInfo);
                    c.startActivity(filePage);
                }

            }
        }else
//        if(a.getVtype()!=null && a.getVtype().equals(LFile.FILE_TYPE_SNIPPET) &&
//                a.getEvent()==LActivity.EVENT_INSERT)
        {
            addSelectedMessage(v,a);
        }

    }

    @Override
    public void onFileEventCommentViewClick(View bt_comment, LActivity LActivity) {
        fileOptions.showComments(null, LActivity.getFileResource());
    }

    @Override
    public void onFileEventStarViewClick(View bt_star, LActivity LActivity) {
        fileOptions.forwardOrganize(LActivity.getFileResource(c));
    }

    @Override
    public void onFileEventShareViewClick(View bt_share, LActivity LActivity) {
        fileOptions.shareWith(LActivity.getFileResource(c));
    }

    @Override
    public void onFileEventViewLongClick(TimelineEventViewHolder vh, LActivity a) {
//        if(a.getVtype()!=null && a.getVtype().equals(LFile.FILE_TYPE_SNIPPET) &&
//                a.getEvent()==LActivity.EVENT_INSERT) {
            addSelectedMessage(vh, a);
//        }
    }





    //*******************************
    //* UPDATES
    //*******************************
    public void addEventToList(LActivity a){
        if(!activities.getactivities().contains(a)) {
            mAdapter.addEvent(a);
            lm.scrollToPosition(0);
        }
    }

    public void updateEventOnList(LActivity a){
        mAdapter.updateEvent(a);
    }

    public void updateEventOnList(int index, LActivity a){
        mAdapter.updateEvent(index, a);
    }

    public void removeEventFromList(LActivity a){
        mAdapter.removeEvent(a);
    }




    @Override
    public synchronized void onInsert(final EdoContainer container, final LActivity a) {
        if(app.isUpdateInProgress)
            return;



        if(a.isStatusResource()){
            loadEvents();
            return;
        }

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (container != null &&
                        container.equals(getCurrentContainer())) {


                    if (a.isUnread()) {
                        activities.setLastNotificationUnread(a.getId());
                        setAllEventsRed();
                    }

                    if (a.getOldActivityId() != null) {

                        int index = activities.getactivities().indexOf(new LActivity(a.getOldActivityId()));
                        if (index >= 0) {
                            updateEventOnList(index, a);
                            return;
                        }
                    }

                    if (!activities.getactivities().contains(a)) {

                        if (activities.getactivities().size() > 0) {
                            LActivity lastActivity = activities.getactivities().get(0);
                            if (activities.updateNewAndRemoveLast(c, lastActivity, a)) {
                                removeEventFromList(lastActivity);
                            }
                        }

                        addEventToList(a);

                    } else {
                        onUpdate(container, a);
                    }

                }

            }
        });
    }

    @Override
    public synchronized void onUpdate(final EdoContainer container, final LActivity a) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (a.getContainerId() != null && a.getContainerId().equals(getCurrentContainer().getContainerId())) {
                    if (a.isUploadEvent() || a.isDownloadEvent()) {

                        RecyclerView.ViewHolder vh = mAdapter.getViewHolder(a.getId());
                        if (vh instanceof TimelineEventViewHolder) {
                            String text = c.getResources().getString(R.string.upload).toUpperCase();
                            if(a.isDownloadEvent()){
                                text = c.getResources().getString(R.string.download).toUpperCase();
                            }
                            ((TimelineEventViewHolder) vh).setProgress(a.getStatus(), a.getProgressPercent(), a.getMessage(),text);
                        }
                    } else
                        updateEventOnList(a);


                }
            }
        });
    }

    @Override
    public synchronized void onDelete(EdoContainer container, final LActivity a) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (a.getContainerId() != null && a.getContainerId().equals(getCurrentContainer().getContainerId()))
                    removeEventFromList(a);
            }
        });
    }

    @Override
    public synchronized void onSyncList(LActivity a) {
        if(a.hasBeenProcessed())
            loadEvents();
    }





    //*******************************
    //* Chat
    //*******************************
    public void sendMessage(final String text){

        if(canSendMessage) {
            final LActivity a;
            if (JavaUtils.isNotEmpty(text) && getCurrentContainer().isContactOrGroup()) {
                a = messageDAO.sendMessage((LContact) getCurrentContainer(), text);
            } else {
                a = null;
            }

            if (a != null) {
                c.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        et_chat.setText("");
                        addEventToList(a);
                    }
                });
            }
        }else{
            String oops = c.getResources().getString(R.string.chatOnlyWithEdoContactName).replace(Constants.PH_CONTACTNAME,getCurrentContainer().getName());
            if(getCurrentContainer().isGroup()){
                oops = c.getResources().getString(R.string.chatOnlyWithEdoGroup);
            }
            EdoUI.oneButtonDialog(c, c.getResources().getString(R.string.oops),oops);
        }
    }


    @Override
    public void onChatMessageClick(ChatViewHolder v, LActivity a) {
        addSelectedMessage(v, a);
    }

    @Override
    public void onChatMyMessageClick(ChatViewHolder v, LActivity a) {
        addSelectedMessage(v, a);
    }


    @Override
    public void onChatMessageLongClick(ChatViewHolder vh, LActivity a) {
        addSelectedMessage(vh, a);
    }

    @Override
    public void onChatMyMessageLongClick(ChatViewHolder vh, LActivity a) {
        addSelectedMessage(vh, a);
    }


    public ArrayList<LActivity> selectedActivities = new ArrayList<LActivity>();
    public void addSelectedMessage(TimelineBaseViewHolder vh, LActivity a){
        starttimelineOptionsActionMode();
        if(!selectedActivities.contains(a)){
            selectedActivities.add(a);
            a.setIsSelected(true);
            vh.setSelected(true);
            settimelineOptionsActionModeTitle("" + selectedActivities.size());
        }else{
            selectedActivities.remove(a);
            a.setIsSelected(false);
            vh.setSelected(false);
            settimelineOptionsActionModeTitle("" + selectedActivities.size());
            if(selectedActivities.size()==0){
                stoptimelineOptionsActionMode();
            }
        }
        checkMenuForselectedActivities();
    }


    ActionMode timelineOptionsActionMode = null;

    public void starttimelineOptionsActionMode(){
        if(!istimelineOptionsActionModeEnabled) {
            selectedActivities.clear();
            timelineOptionsActionMode = c.startActionMode(new timelineOptionsActionMode());
        }
    }

    public void stoptimelineOptionsActionMode(){
        if(istimelineOptionsActionModeEnabled && timelineOptionsActionMode!=null) {
            timelineOptionsActionMode.finish();
        }
    }

    public void settimelineOptionsActionModeTitle(String title){
        if(timelineOptionsActionMode!=null)
            timelineOptionsActionMode.setTitle(title);
    }
    
    public void checkMenuForselectedActivities(){
        if(timelineOptionsActionMode!=null){
            boolean showAll = true;
            for (LActivity message : selectedActivities) {
                if(!message.isChatEvent()){
                    showAll = false;
                    break;
                }
            }
            Menu menu = timelineOptionsActionMode.getMenu();
            menu.findItem(R.id.newNote).setVisible(showAll?true:false);
            menu.findItem(R.id.copy).setVisible(showAll?true:false);
            menu.findItem(R.id.share).setVisible(showAll?true:false);
        }
    }


    boolean istimelineOptionsActionModeEnabled = false;

    public final class timelineOptionsActionMode implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            istimelineOptionsActionModeEnabled = true;

            MenuInflater inflater = c.getMenuInflater();
            inflater.inflate(R.menu.chatoptions, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {


            switch(item.getItemId()) {

                case R.id.newNote:
                    newNoteFromselectedActivities();
                    break;

                case R.id.copy:
                    copyselectedActivities();
                    break;

                case R.id.share:
                    shareselectedActivities();
                    break;

                case R.id.delete:
                    deleteselectedActivities();
                    break;

                default:
                    break;

            }

            mode.finish();

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

            istimelineOptionsActionModeEnabled = false;

            c.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for(LActivity event:activities.getactivities()){
                        event.setIsSelected(false);
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public String getTextFromselectedActivities(){
        String messages = "";
        for(LActivity message:selectedActivities){
            if(JavaUtils.isNotEmpty(message.getAggregationMessage())) {
                if (message.getSenderId().equals(app.getEdoUserId())) {
                    messages += message.getAggregationMessage() + "\n";
                } else {
                    messages += message.getSenderName() + ": " + message.getAggregationMessage() + "\n";
                }
            }
        }
        return messages;
    }


    public void newNoteFromselectedActivities(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String messages = getTextFromselectedActivities();
                c.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        filesPicker.snippetEditor(getCurrentContainer(),getTimelineSpace(),messages);
                    }
                });
            }
        }).start();
    }


    public void copyselectedActivities(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String messages = getTextFromselectedActivities();
                c.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AUtils.copyToClipboard(c, messages);
                    }
                });
            }
        }).start();
    }

    public void shareselectedActivities(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String messages = getTextFromselectedActivities();
                c.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        FileOptions.singleShare(c, null, messages.toString(), FileInfo.MIME_TEXT);
                    }
                });
            }
        }).start();
    }

    public void deleteselectedActivities(){
        final EdoDialog dialog2 = new EdoDialog(c);
        dialog2.setTitle(R.string.delete);
        dialog2.setText(R.string.doYouWantToDeleteThisMessage);
        dialog2.setPositiveButtonText(R.string.ok);
        dialog2.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

            @Override
            public void onClick(View v) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (LActivity message : selectedActivities) {

                            boolean done = activityDAO.delete(message);
                            if (done) {
                                activities.getactivities().remove(message);
                            }
                        }
                        c.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.notifyDataSetChanged();
                                EdoUI.showToast(c, R.string.selectedMessagesHasBeenRemoved, null, Toast.LENGTH_LONG);
                            }
                        });
                    }
                }).start();

                dialog2.cancel();
            }
        });
        dialog2.setNegativeButtonText(R.string.cancel);
        dialog2.setOnNegativeClickListener(new EdoDialog.OnNegativeClickListener() {

            @Override
            public void onClick(View v) {
                dialog2.cancel();
            }
        });
        dialog2.show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        updateWebStatus();
        app.uiManager.removeListener(LISTENER_ID);
    }
}
