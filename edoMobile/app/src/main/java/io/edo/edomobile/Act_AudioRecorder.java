package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.paolobriganti.android.AUI;
import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;
import io.edo.utilities.DateManager;
import io.edo.utilities.eLog;

public class Act_AudioRecorder extends Activity
{
	Activity c;
	private static final String LOG_TAG = "AudioRecordTest";
	private static String mFileName = null;

	public static final int SAMPLE_RATE = 8000;

	private AudioRecord mRecorder;
	private File mRecording;
	private short[] mBuffer;
	private boolean mIsRecording = false;

	private MediaPlayer mPlayer = null;

	private TextView tv_text;
	private ProgressBar mProgressBar;
	private ImageButton bt_record;

	String stopTimeSt = null;
	boolean isPlaying = false;
	long recTime = 0;
	long startTime = 0;

	//runs without a timer by reposting this handler at the end of the runnable
	Handler timerHandler = new Handler();
	Runnable timerRunnable = new Runnable() {

		@Override
		public void run() {
			long current = System.currentTimeMillis();
			long millis = current - startTime;
			String time = DateManager.convertMillsToHourMinSec(millis);
			if(mIsRecording){
				tv_text.setText(time);
				timerHandler.postDelayed(this, 500);
			}else if(isPlaying && millis<recTime){
				tv_text.setText(time+"/"+stopTimeSt);
				mProgressBar.setMax(JavaUtils.getIntegerValue(recTime, 0));
				mProgressBar.setProgress(JavaUtils.getIntegerValue(millis, 0));
				timerHandler.postDelayed(this, 500);
			}else if(isPlaying && millis>=recTime){
				tv_text.setText(stopTimeSt+"/"+stopTimeSt);
				mProgressBar.setMax(JavaUtils.getIntegerValue(recTime, 0));
				mProgressBar.setProgress(JavaUtils.getIntegerValue(recTime, 0));
				timerHandler.removeCallbacks(timerRunnable);
				setPlayButton();
			}

		}
	};

	@Override
	public void onCreate(Bundle icicle) {

		AUI.setStatusBarColor(this, this.getResources().getColor(R.color.edo_blue_dark));
		EdoUI.setOrientationTo(this);


		c = this;
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		overridePendingTransition(0,0);
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.act_audiorecorder);

		if(Constants.getExternalCacheDir(c)==null){
			EdoUI.showToast(c, R.string.noExternalMemoryfound, null, Toast.LENGTH_LONG);
			Act_AudioRecorder.this.finish();
			return;
		}

		registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mFileName = extras.getString(Constants.EXTRA_FILE_PATH, null);
		}
		if(JavaUtils.isEmpty(mFileName) || mFileName.equals("null") && Constants.getExternalCacheDir(c)!=null){
			File dir = new File(Constants.getExternalCacheDir(c).getPath()+File.separator+Constants.AUDIO_CACHE_RECORDINGS);
			dir.mkdirs();
			if(dir.exists()){
				mFileName = dir.toString()+File.separator+FileInfo.getFileNameWithTime("Record_");
			}
		}
		if(JavaUtils.isNotEmpty(mFileName)){
			initRecorder();
			tv_text = (TextView) this.findViewById(R.id.tv_text);
			mProgressBar = (ProgressBar) this.findViewById(R.id.progressBar);
			bt_record = (ImageButton) this.findViewById(R.id.bt_record);
			setRecordButton();
		}else{
			Act_AudioRecorder.this.finish();
		}


	}

	@Override
	public void onPause() {
		super.onPause();
		if (mRecorder != null) {
			mRecorder.release();
			mRecorder = null;
		}

		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
		//		timerHandler.removeCallbacks(timerRunnable);
	}

	public void setRecordButton(){
		mProgressBar.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_horizontal_rec));
//		bt_record.setText(R.string.startRecording);
		bt_record.setImageResource(R.drawable.bt_audiorecord_large);
		bt_record.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startTime = System.currentTimeMillis();
				timerHandler.postDelayed(timerRunnable, 0);
				startRecording();
			}
		});
	}
	public void setStopRecordingButton(){
//		bt_record.setText(R.string.stopRecording);
		bt_record.setImageResource(R.drawable.bt_audiorecord_stop_record_large);
		bt_record.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				timerHandler.removeCallbacks(timerRunnable);
				stopTimeSt = tv_text.getText().toString();
				recTime = System.currentTimeMillis()-startTime;
				stopRecording();

			}
		});
	}
//	public void setSaveButton(){
////		bt_record.setText(R.string.save);
//		bt_record.setBackgroundResource(R.drawable.bt_right_green);
//		bt_record.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				bt_save(v);
//			}
//		});
//	}

	public void setPlayButton(){
		mProgressBar.setProgressDrawable(c.getResources().getDrawable(R.drawable.progress_horizontal_play));
		bt_record.setImageResource(R.drawable.bt_audioplayer_play_large);
		bt_record.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isPlaying = true;
				startTime = System.currentTimeMillis();
				timerHandler.postDelayed(timerRunnable, 0);
				startPlaying();
			}
		});
	}

	public void setStopPlayButton(){
		bt_record.setImageResource(R.drawable.bt_audioplayer_pause_large);
		bt_record.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				timerHandler.removeCallbacks(timerRunnable);
				stopPlaying();
			}
		});
	}


	private void startPlaying() {
		mPlayer = new MediaPlayer();
		try {
			File waveFile = getFile("wav");
			mPlayer.setDataSource(waveFile.toString());
			mPlayer.prepare();
			mPlayer.start();

			setStopPlayButton();
		} catch (IOException e) {
			eLog.e(LOG_TAG, "prepare() failed");
			setPlayButton();
		}
	}

	private void stopPlaying() {
		mPlayer.release();
		mPlayer = null;
		setPlayButton();
	}

	private void startRecording() {
		if (!mIsRecording) {
			mIsRecording = true;
			mRecorder.startRecording();
			mRecording = getFile("raw");
			startBufferedWrite(mRecording);
			EdoUI.showToast(Act_AudioRecorder.this, R.string.recordingInProgress, null,
					Toast.LENGTH_SHORT);
		}
		setStopRecordingButton();
	}

	private void stopRecording() {
		if (mIsRecording) {
			mIsRecording = false;
			if(mRecorder!=null)
				mRecorder.stop();
			File waveFile = getFile("wav");
			try {
				rawToWave(mRecording, waveFile);
			} catch (IOException e) {
				EdoUI.showToast(Act_AudioRecorder.this, e.getMessage(), null, Toast.LENGTH_SHORT);
			}
			EdoUI.showToast(Act_AudioRecorder.this, c.getResources().getString(R.string.recorderedTo) + waveFile.getName(), null,
					Toast.LENGTH_SHORT);
		}

		if(getFile("wav").exists()){
			setPlayButton();
		}else{
			setRecordButton();
		}
	}



	public void bt_cancel(View v){
		new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
			File waveFile = getFile("wav");
			if(waveFile.exists())
				waveFile.delete();
		}}.start();

		Act_AudioRecorder.this.finish();
	}

	public void bt_save(View v){
		stopRecording();
		File waveFile = getFile("wav");
		if(waveFile.exists()){
			String path = waveFile.toString();

//			EdoUserInfo eui = ThisApp.getInstance(c).userInfo;
//			ContentResolver mCr = c.getContentResolver();
//			ContentValues values = new ContentValues();
//			values.put(MediaStore.MediaColumns.DATA, waveFile.getAbsolutePath());
//			values.put(MediaStore.MediaColumns.TITLE, mFileName);
//			values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mpeg");
//			values.put(MediaStore.MediaColumns.SIZE, waveFile.length());
//			values.put(MediaStore.Audio.Media.ARTIST, eui.getUserNameSurname());
//			values.put(MediaStore.Audio.Media.ALBUM, c.getResources().getString(R.string.app_name));
//			values.put(MediaStore.Audio.Media.YEAR, String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
//
//			Uri uri = MediaStore.Audio.Media.getContentUriForPath(waveFile.getAbsolutePath());
//			Uri uri2 = mCr.insert(uri, values);
//
//			path = FileInfo.getRealPathFromURI(c, uri2);

			Intent extra = new Intent();
			extra.putExtra(Constants.EXTRA_FILE_PATH, path);
			setResult(RESULT_OK, extra);
		}
		Act_AudioRecorder.this.finish();
	}

















	private void initRecorder() {
		int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
				AudioFormat.ENCODING_PCM_16BIT);
		mBuffer = new short[bufferSize];
		mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
				AudioFormat.ENCODING_PCM_16BIT, bufferSize);
	}

	private void startBufferedWrite(final File file) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				DataOutputStream output = null;
				try {
					output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
					while (mIsRecording) {
						if(mBuffer!=null){
							double sum = 0;
							int readSize = mRecorder.read(mBuffer, 0, mBuffer.length);
							for (int i = 0; i < readSize; i++) {
								output.writeShort(mBuffer[i]);
								sum += mBuffer[i] * mBuffer[i];
							}
							if (readSize > 0) {
								final double amplitude = sum / readSize;
								mProgressBar.setProgress((int) Math.sqrt(amplitude));
							}
						}
					}
				} catch (NullPointerException e) {
					eLog.e(LOG_TAG, "NullPointerException");
				} catch (IOException e) {
					eLog.e(LOG_TAG, e.getMessage());
				} finally {
					mProgressBar.setProgress(0);
					if (output != null) {
						try {
							output.flush();
						} catch (IOException e) {
							eLog.e(LOG_TAG, e.getMessage());
						} finally {
							try {
								output.close();
							} catch (IOException e) {
								eLog.e(LOG_TAG, e.getMessage());
							}
						}
					}
				}
			}
		}).start();
	}

	private void rawToWave(final File rawFile, final File waveFile) throws IOException {

		byte[] rawData = new byte[(int) rawFile.length()];
		DataInputStream input = null;
		try {
			input = new DataInputStream(new FileInputStream(rawFile));
			input.read(rawData);
		} finally {
			if (input != null) {
				input.close();
			}
		}

		DataOutputStream output = null;
		try {
			output = new DataOutputStream(new FileOutputStream(waveFile));
			// WAVE header
			// see http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
			writeString(output, "RIFF"); // chunk id
			writeInt(output, 36 + rawData.length); // chunk size
			writeString(output, "WAVE"); // format
			writeString(output, "fmt "); // subchunk 1 id
			writeInt(output, 16); // subchunk 1 size
			writeShort(output, (short) 1); // audio format (1 = PCM)
			writeShort(output, (short) 1); // number of channels
			writeInt(output, SAMPLE_RATE); // sample rate
			writeInt(output, SAMPLE_RATE * 2); // byte rate
			writeShort(output, (short) 2); // block align
			writeShort(output, (short) 16); // bits per sample
			writeString(output, "data"); // subchunk 2 id
			writeInt(output, rawData.length); // subchunk 2 size
			// Audio data (conversion big endian -> little endian)
			short[] shorts = new short[rawData.length / 2];
			ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
			ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
			for (short s : shorts) {
				bytes.putShort(s);
			}
			output.write(bytes.array());
		} finally {
			if (output != null) {
				output.close();

				if(waveFile.exists() && rawFile.exists())
					rawFile.delete();
			}
		}
	}

	private File getFile(final String suffix) {
		return new File(mFileName + "." + suffix);
	}

	private void writeInt(final DataOutputStream output, final int value) throws IOException {
		output.write(value >> 0);
		output.write(value >> 8);
		output.write(value >> 16);
		output.write(value >> 24);
	}

	private void writeShort(final DataOutputStream output, final short value) throws IOException {
		output.write(value >> 0);
		output.write(value >> 8);
	}

	private void writeString(final DataOutputStream output, final String value) throws IOException {
		for (int i = 0; i < value.length(); i++) {
			output.write(value.charAt(i));
		}
	}






	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mKillReceiver);
	}
	private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, final Intent intent) {
			finish();
		}
	};
}
