package io.edo.edomobile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.adapters.filepage.FilePageHolder;
import io.edo.ui.imageutils.FilePageLoader;

/**
 * Created by PaoloBriganti on 01/07/15.
 */
public class Frag_FilePage extends Fragment{

    Act_FilePage c;
    ThisApp app;
    FilePageLoader filePageLoader;

    EdoContainer container;
    LSpace space;
    LFile currentFile;
    
    public Frag_FilePage() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        c = (Act_FilePage)getActivity();
        app = ThisApp.getInstance(c);

        if(c.filePageLoader==null){
            c.filePageLoader = FilePageLoader.getInstance(c);
        }
        filePageLoader = c.filePageLoader;

        if (getArguments()!=null && getArguments().containsKey(DBConstants.CONTAINER)) {
            Serializable containerSerial = getArguments().getSerializable(DBConstants.CONTAINER);
            if (containerSerial != null) {
                container = (EdoContainer) containerSerial;
            }
        }
        if (getArguments()!=null && getArguments().containsKey(DBConstants.SPACE)) {
            Serializable spaceSerial = getArguments().getSerializable(DBConstants.SPACE);
            if (spaceSerial != null) {
                space = (LSpace) spaceSerial;
            }
        }
        if (getArguments()!=null && getArguments().containsKey(DBConstants.FILE)) {
            Serializable fileSerial = getArguments().getSerializable(DBConstants.FILE);
            if(fileSerial!=null) {
                currentFile = (LFile) fileSerial;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_filepage, container, false);


        final FilePageHolder vh = new FilePageHolder(rootView);
        if(mFileClickListener!=null){
            vh.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFileClickListener.onClick(vh,currentFile);
                }
            });
        }

        c.filePageHolders.put(currentFile.getId(), vh);

        if(currentFile.isSnippet()){
            vh.showSnippetView(currentFile, getChildFragmentManager());
        }else if(currentFile.isAudio() && currentFile.isStored()){
            vh.showAudioView(currentFile, getChildFragmentManager());
        }else if(currentFile.isVideo()){
            vh.showVideoView(space, currentFile, getChildFragmentManager());
        }else{
            filePageLoader.loadImage(space, currentFile, vh);
        }

        setBackPressed(rootView);

        return rootView;
    }



    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        c.filePageHolders.remove(currentFile.getId());
    }



    public void setBackPressed(View v){
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

                }
                return false;
            }
        });
    }


    FileClickListener mFileClickListener;

    public void setFileClickListener(FileClickListener listener) {
        mFileClickListener = listener;
    }

    public interface FileClickListener {
        void onClick(FilePageHolder v, LFile file);
    }

    FileLongClickListener mFileLongClickListener;

    public void setLongFileClickListener(FileLongClickListener listener) {
        mFileLongClickListener = listener;
    }

    public interface FileLongClickListener {
        void onLongClick(FilePageHolder v, LFile file);
    }

}
