package io.edo.edomobile;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.SignInButton;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.paolobriganti.android.ASI;
import com.paolobriganti.android.AUI;
import com.paolobriganti.android.AUtils;
import com.paolobriganti.android.VU;
import com.paolobriganti.utils.JavaUtils;

import java.io.IOException;
import java.util.ArrayList;

import io.edo.api.EdoAPI;
import io.edo.api.google.gcm.GCMActivity;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LUser;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.web.request.EdoAuthManager;
import io.edo.db.web.request.EdoRequest;
import io.edo.db.web.request.LoginWG1Resp;
import io.edo.db.web.webupdate.EdoGeneralUpdate;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialog.OnNegativeClickListener;
import io.edo.ui.EdoDialogListView;
import io.edo.ui.EdoDialogListView.OnListItemClickListener;
import io.edo.ui.EdoDialogOne.OnPositiveClickListener;
import io.edo.ui.EdoIntentManager;
import io.edo.ui.EdoLoginProgressLayout;
import io.edo.ui.EdoNotification;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.ListAdapterOptionsText;
import io.edo.utilities.Constants;
import io.edo.utilities.TrackConstants;

public class Act_00_SplashScreen extends GCMActivity implements UIManager.ActivitiesUpdatesListener{
	public static final boolean TEST_LOGIN_DATA = false;

	private Activity c;
	private ThisApp app;

	private final static String LISTENER_ID = Act_00_SplashScreen.class.getName();

	private EdoProgressDialog progressDialog;
	private EdoNotification notification;

	private EdoDialogListView accountsDialog;

	private boolean resumeActivity = true;

	private static final long ANI_TIME = 500;

	private LinearLayout ll_splashProgress;
	private SignInButton bt_lwg;
	private FrameLayout fl_firstLayout;
	private ImageView  iv_top, iv_splashLogo, iv_splashSpinner;
	private RelativeLayout fl_progressLogin;
	private TextView tv_splashProgressMessage;
	private WebView wv_login;

	EdoLoginProgressLayout loginProgressLayout;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		AUtils.sendPushNotificationHeartBeat(this);

		AUI.setStatusBarColor(this, this.getResources().getColor(R.color.edo_blue_dark));
		EdoUI.setOrientationTo(this);

		super.onCreate(savedInstanceState);

		app = ThisApp.getInstance(this);
		c = this;

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                LActivity[] activities = new ActivityDAO(c).downloadAll();
//            }
//        }).start();

		if(!TEST_LOGIN_DATA)
			if(app.isLoginComplete() &&
					GCM.hasRegistrationId(c)
					&& EdoAuthManager.hasRefreshToken(c)){
				bt_edo(null);
				resumeActivity = false;

				return;
			}

		if(!app.isLoginComplete()){
			app.sendEventTrack(TrackConstants.SCREEN_NAME_LOGIN);
		}

		setContentView(R.layout.act_00_splash);

		findViews();

		progressDialog = new EdoProgressDialog(c, R.string.login, R.string.authenticationInProgress);
		notification = new EdoNotification(c, Act_00_SplashScreen.class);

		firstScreen(app.isLoginComplete());

		if(!TEST_LOGIN_DATA)
			if(ASI.isInternetOn(c) && app.isLoginComplete()) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						splashSpinner(true);

						if (!GCM.hasRegistrationId(c)) {
							new Thread(new Runnable() {
								@Override
								public void run() {
									register(true);
								}
							}).start();
						}

						if (!EdoAuthManager.hasRefreshToken(c)) {
							EdoAuthManager.refreshToken(c,true);
						}

						bt_edo(null);
					}

				}).start();
			}




		registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));
	}





	protected void findViews() {

		bt_lwg = (SignInButton) findViewById(R.id.sign_in_button);
		fl_firstLayout = (FrameLayout) findViewById(R.id.fl_firstLayout);
		iv_top = (ImageView) findViewById(R.id.iv_top);
		iv_splashLogo = (ImageView) findViewById(R.id.splash_logo);
		fl_progressLogin = (RelativeLayout) findViewById(R.id.fl_progressLogin);
		ll_splashProgress = (LinearLayout) findViewById(R.id.ll_splashProgress);
		iv_splashSpinner = (ImageView) findViewById(R.id.iv_splashSpinner);
		tv_splashProgressMessage = (TextView) findViewById(R.id.tv_splashProgressMessage);
		wv_login = (WebView) findViewById(R.id.wv_login);
		wv_login.getSettings().setJavaScriptEnabled(true);

		loginProgressLayout = new EdoLoginProgressLayout(Act_00_SplashScreen.this, fl_progressLogin, new EdoLoginProgressLayout.ContinueButtonClickListener() {
			@Override
			public void onClick(View v) {
				bt_edo(null);
				app.sendEventTrack(TrackConstants.EVENT_LOGIN_CONTINUE_BUTTON);
			}
		});


		bt_lwg.setSize(SignInButton.SIZE_WIDE);
		bt_lwg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				app.sendEventTrack(TrackConstants.EVENT_LOGIN_WITH_GOOGLE_CLICK);

				bt_loginWithGoogle1(v);
			}
		});


	}

	public void showLoadingDataLayout(){
		runOnUiThread(new Runnable() {public void run() {
			bt_lwg.setVisibility(Button.INVISIBLE);
			bt_lwg.setOnClickListener(null);
			fl_firstLayout.setVisibility(View.GONE);
			wv_login.setVisibility(View.GONE);
			loginProgressLayout.show();
		}});
	}

	public void showLoginButtonLayout(final boolean showButton){
		runOnUiThread(new Runnable() {public void run() {
			bt_lwg.setVisibility(!showButton?Button.INVISIBLE:View.VISIBLE);
			fl_firstLayout.setVisibility(View.VISIBLE);
			fl_progressLogin.setVisibility(View.GONE);
			wv_login.setVisibility(View.GONE);
		}});
	}

	public void showLoginWebViewLayout(){
		runOnUiThread(new Runnable() {public void run() {
			fl_firstLayout.setVisibility(View.GONE);
			wv_login.setVisibility(View.VISIBLE);
			fl_progressLogin.setVisibility(View.GONE);
		}});
	}


	public void splashSpinner(final boolean show){

		runOnUiThread(new Runnable() {
			public void run() {
				if (show) {
					bt_lwg.setVisibility(Button.INVISIBLE);
					ll_splashProgress.setVisibility(View.VISIBLE);
					iv_splashSpinner.setAnimation(AnimationUtils.loadAnimation(c, R.anim.rotate));
					new Thread() {
						@Override
						public void run() {
							Looper.myLooper();//Looper.prepare();
							try {
								sleep(2000L);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							runOnUiThread(new Runnable() {
								public void run() {

									tv_splashProgressMessage.setVisibility(View.VISIBLE);
								}
							});
						}
					}.start();
				} else {
					showLoginButtonLayout(true);
				}
			}
		});
	}






	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AUtils.sendPushNotificationHeartBeat(this);
		app.uiManager.addActivitiesUpdatesListener(LISTENER_ID,this);

		if(!resumeActivity){
			resumeActivity = true;
			return;
		}

		if(!app.isLoginDownloadDataInProgress){

			int loginStatus = app.getLoginStatus();

			switch (loginStatus) {
				case LUser.LOGIN_0_NOT_EXECUTED:

					break;

				case LUser.LOGIN_1_TEMP_AUTHENTICATION_EXECUTED:
					loginWithGoogle2(null);
					break;

				case LUser.LOGIN_2_GOOGLE_AUTHORIZATION_OBTAINED:
					loginWithGoogle3();
					break;

				case LUser.LOGIN_3_AUTHENTICATION_ENDED:
					loginDataDownload(true);
					break;

				case LUser.LOGIN_4_DATA_DOWNLOAD_FINISHED:
//					bt_edo(null);
					break;

				default:
					break;
			}

		}else{

			showLoadingDataLayout();

		}

		progressDialog.resume();
	}


	@Override
	protected void onPause() {
		super.onPause();
		progressDialog.pause();
	}






	//**************************************************************
	//* FIRST SCREEN
	//**************************************************************

	public void firstScreen(final boolean isLogged){
		showLoginButtonLayout(false);


		new Thread(){@Override public void run(){Looper.myLooper();Looper.prepare();

			if(isLogged){
				runOnUiThread(new Runnable() {public void run() {
					bt_lwg.setVisibility(Button.INVISIBLE);
				}});
			}

			runOnUiThread(new Runnable() {public void run() {
				if(!isLogged){
					VU.zoomInAnimation(iv_splashLogo, ANI_TIME/2).setListener(new AnimatorListener() {

						@Override
						public void onAnimationStart(Animator animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationRepeat(Animator animation) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onAnimationEnd(Animator animation) {
							if(!isLogged){
								VU.inFromTop(iv_top, ANI_TIME);
							}
						}

						@Override
						public void onAnimationCancel(Animator animation) {
							// TODO Auto-generated method stub

						}
					});
				}else{
					iv_splashLogo.setVisibility(View.VISIBLE);
				}
			}});


			if(!isLogged) {
				try {
					sleep(ANI_TIME / 2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				runOnUiThread(new Runnable() {
					public void run() {
						VU.zoomInAnimation(bt_lwg, ANI_TIME);
					}
				});

				try {
					sleep(ANI_TIME * 2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(TEST_LOGIN_DATA)
				loginProgressLayout.startExample();
		}}.start();
	}




	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//*		BUTTONS
	//*****************************************************************************************************************************************************************************************************************************************************************************************************

	public void bt_edo(View v){
		if(app.isLoginComplete()){
			c.startActivity(EdoIntentManager.getContactsIntent(c, null));
			Act_00_SplashScreen.this.finish();
		}
	}






















	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//* LOGIN
	//*****************************************************************************************************************************************************************************************************************************************************************************************************








	//**************************************************************
	//* FIRST LOGIN PHASE
	//**************************************************************

	public void bt_loginWithGoogle1(View v){
		loginWithGoogle1();
	}

	public void loginWithGoogle1(){
		boolean isInternetOn = !EdoUI.dialogNoInternet(c,
				new Runnable() {public void run() {
					loginWithGoogle1();
				}},
				new Runnable() {public void run() {
					Act_00_SplashScreen.this.finish();
				}});

		if(isInternetOn){
			progressDialog.show();
			new Thread(){@Override public void run(){Looper.myLooper();Looper.prepare();

				final LoginWG1Resp lwg1Resp = EdoAuthManager.loginWithGoogle1(c, GCM.getRegistrationId());

				runOnUiThread(new Runnable() {public void run() {
					if(lwg1Resp!=null){
						app.saveLoginStatus(LUser.LOGIN_1_TEMP_AUTHENTICATION_EXECUTED);
						loginWithGoogle2(lwg1Resp);
					}else{
						progressDialog.dismiss();
						final EdoDialog dialog = new EdoDialog(c);
						dialog.setCancelable(false);
						dialog.setTitle(R.string.loginError);
						dialog.setText(R.string.unableToCompleteTheOperationCheckInternetRetry);
						dialog.setPositiveButtonText(R.string.retry);
						dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

							@Override
							public void onClick(View arg0) {
								loginWithGoogle1();
								runOnUiThread(new Runnable() {public void run() {
									dialog.dismiss();
								}});
							}
						});
						dialog.setOnNegativeClickListener(new OnNegativeClickListener() {

							@Override
							public void onClick(View v) {
								runOnUiThread(new Runnable() {public void run() {
									dialog.dismiss();
									Act_00_SplashScreen.this.finish();
								}});
							}
						});
						runOnUiThread(new Runnable() {public void run() {
							dialog.show();
						}});
					}
				}});

			}}.start();
		}
	}


	//**************************************************************
	//* SECOND LOGIN PHASE
	//**************************************************************
	public void loginWithGoogle2(final LoginWG1Resp lwg1Resp){
		if(accountsDialog==null || !accountsDialog.isShowing()){
			accountsDialog = new EdoDialogListView(c);
			accountsDialog.setTitle(R.string.selectAccount);

			Account[] accounts = AccountManager.get(c).getAccountsByType("com.google");

			final ArrayList<String> titles = new ArrayList<String>();

			if(accounts!=null && accounts.length>0){
				for(Account account:accounts){
					titles.add(account.name);
				}
			}

			titles.add(c.getString(R.string.otherAccount));



			ListAdapterOptionsText adapter = new ListAdapterOptionsText(c, titles);
			accountsDialog.setAdapter(adapter);
			accountsDialog.setPositiveButtonText(R.string.cancel);
			accountsDialog.setOnPositiveClickListener(new OnPositiveClickListener() {

				@Override
				public void onClick(View arg0) {
					accountsDialog.cancel();
				}
			});
			accountsDialog.setOnListItemClickListener(new OnListItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					if (position == titles.size() - 1) {
						selectOtherAccount(lwg1Resp);
					} else {
						requestAccessToken(titles.get(position));
					}
					accountsDialog.cancel();
				}
			});
			progressDialog.dismiss();
			accountsDialog.show();

			app.sendEventTrack(TrackConstants.EVENT_LOGIN_SELECT_ACCOUNT_DIALOG);
		}
	}

	public void loginWithGoogle2_1(LoginWG1Resp lwg1Response, final String code){

		if(lwg1Response==null)
			lwg1Response = LoginWG1Resp.load(c);

		final LoginWG1Resp lwg1Resp = lwg1Response;

		new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();

			if(lwg1Resp!=null){
				boolean authenticationSuccess = EdoAuthManager.loginWithGoogle2(c, code, lwg1Resp.token);
				if(authenticationSuccess){
					app.saveLoginStatus(LUser.LOGIN_2_GOOGLE_AUTHORIZATION_OBTAINED);

					progressDialog.dismiss();

					loginWithGoogle3();
				}else{
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							EdoUI.showToast(c, R.string.autoLoginError, null, Toast.LENGTH_LONG);
						}
					});

					selectOtherAccount(null);

					progressDialog.dismiss();
				}
			}else{
				progressDialog.dismiss();
				loginWithGoogle1();

			}
		}}.start();


	}

	//**************************************************************
	//*	SIGN IN WITH GOOGLE
	//**************************************************************
	private static final int REQUEST_ACCOUNT_SELECTED = 44;
	private static final int REQUEST_AUTHORIZATION = 123;


	private String accountName = null;

	//	public void selectAccount(){
	//		Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"},
	//				false, null, null, null, null);
	//		startActivityForResult(intent, REQUEST_ACCOUNT_SELECTED);
	//	}

	public void requestAccessToken(final String accountName){
		progressDialog.dismiss();
		progressDialog.show();

		this.accountName = accountName;

		new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
			try {

				String code = GoogleAuthUtil.getToken(c, accountName, EdoAPI.GOOGLE_WEB_SCOPE);

				loginWithGoogle2_1(null, code);


				app.sendEventTrack(TrackConstants.EVENT_LOGIN_ACCOUNT_SELECTED, TrackConstants.KEY_ACCOUNT_PRESENT_IN_THE_DEVICE_SELECTED, true);


			} catch (UserRecoverableAuthIOException e) {
				startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
			} catch (UserRecoverableAuthException e) {
				// TODO Auto-generated catch block
				startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);

				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				progressDialog.dismiss();

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						EdoUI.showToast(c, R.string.autoLoginError, null, Toast.LENGTH_LONG);
					}
				});


				selectOtherAccount(null);

			} catch (GoogleAuthException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				progressDialog.dismiss();

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						EdoUI.showToast(c, R.string.autoLoginError, null, Toast.LENGTH_LONG);
					}
				});

				selectOtherAccount(null);

			}
		}}.start();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		resumeActivity = false;

		//		if (requestCode == REQUEST_ACCOUNT_SELECTED && resultCode == RESULT_OK) {
		//			String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
		//			requestAccessToken(accountName);
		//
		//		}else

		if(requestCode == REQUEST_AUTHORIZATION){
			progressDialog.dismiss();

//			if (requestCode == RESULT_CANCELED) {
//				//                eLog.v("PLUS_LOGIN","getString(R.string.signed_out_status)");
//			} else

			if (resultCode == RESULT_OK) {
				requestAccessToken(this.accountName);
			}else{
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						EdoUI.showToast(c, R.string.autoLoginError, null, Toast.LENGTH_LONG);
					}
				});

				selectOtherAccount(null);
			}
		}
	}

	public void selectOtherAccount(LoginWG1Resp lwg1Resp){
		if(lwg1Resp==null)
			lwg1Resp = LoginWG1Resp.load(c);

		if(lwg1Resp!=null){

			app.sendEventTrack(TrackConstants.EVENT_LOGIN_ACCOUNT_SELECTED, TrackConstants.KEY_ACCOUNT_PRESENT_IN_THE_DEVICE_SELECTED, true);

			final String url = EdoAuthManager.getLoginWithGoogleUrl(c, lwg1Resp.token);

			if(JavaUtils.isWebUrl(url)){
				runOnUiThread(new Runnable() {public void run() {
					showLoginWebViewLayout();



					wv_login.loadUrl(url);

					final String successURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE + EdoRequest.PATH_SUCCESS;

					wv_login.setWebViewClient(new WebViewClient() {

						@Override
						public void onPageStarted(WebView view, String url,
												  Bitmap favicon) {

							if(url.toLowerCase().contains(successURL.toLowerCase())){
								app.saveLoginStatus(LUser.LOGIN_2_GOOGLE_AUTHORIZATION_OBTAINED);
								loginWithGoogle3();
							}
							super.onPageStarted(view, url, favicon);
						}

						@Override
						public void onPageFinished(WebView view, String url) {
							// TODO Auto-generated method stub
							super.onPageFinished(view, url);
							if(progressDialog!=null && progressDialog.isShowing())progressDialog.dismiss();
						}

						@Override
						public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
							handler.proceed();
						}
					});
				}});
			}




		}else{
			loginWithGoogle1();
		}
	}



	//**************************************************************
	//* THIRD LOGIN PHASE
	//**************************************************************


	public void loginWithGoogle3(){
		showLoginButtonLayout(false);

		final LoginWG1Resp lwg1Resp = LoginWG1Resp.load(c);

		boolean isInternetOn = !EdoUI.dialogNoInternet(c,
				new Runnable() {public void run() {
					loginWithGoogle3();
				}},
				new Runnable() {public void run() {
					Act_00_SplashScreen.this.finish();
				}});

		if(isInternetOn){
			if(lwg1Resp!=null){
				new Thread(){@Override public void run(){Looper.myLooper();Looper.prepare();

					runOnUiThread(new Runnable() {public void run() {
						progressDialog.show();
					}});

					final int result = EdoAuthManager.loginWithGoogle3(c, lwg1Resp.token);

					if(result<=EdoRequest.SUCCESS_LOGIN){

						app.saveLoginStatus(LUser.LOGIN_3_AUTHENTICATION_ENDED);


						progressDialog.dismiss();


						loginDataDownload(result==EdoRequest.SUCCESS_REGISTRATION);




					}else{
						progressDialog.dismiss();

						runOnUiThread(new Runnable() {public void run() {
							final EdoDialog dialog = new EdoDialog(c);
							dialog.setCancelable(false);
							dialog.setTitle(R.string.loginError);
							dialog.setText(R.string.unableToCompleteTheOperationCheckInternetRetry);
							dialog.setPositiveButtonText(R.string.retry);
							dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

								@Override
								public void onClick(View arg0) {
									dialog.dismiss();
									loginWithGoogle3();
								}
							});
							dialog.setOnNegativeClickListener(new OnNegativeClickListener() {

								@Override
								public void onClick(View v) {
									dialog.dismiss();
									firstScreen(false);
								}
							});

							dialog.show();
						}});
					}
				}}.start();

			}else{
				runOnUiThread(new Runnable() {public void run() {
					loginWithGoogle1();
				}});
			}
		}
	}


	public void loginDataDownload(final boolean isRegistration){
		if(isRegistration){
			app.sendEventTrack(TrackConstants.EVENT_LOGIN_NEW);
		}else{
			app.sendEventTrack(TrackConstants.EVENT_LOGIN_RETURN);
		}

		app.startTimeOf(TrackConstants.EVENT_LOGIN_CONTINUE_BUTTON);
		if(EdoRequest.IS_TEST_APP){
			c.runOnUiThread(new Runnable() {public void run() {

				String[] options = {"PERSONAL AND CONTACTS","PERSONAL","CONTACTS"};

				AlertDialog.Builder builder = new AlertDialog.Builder(c);
				builder.setTitle("DOWNLOAD DATA OF:")
						.setItems(options, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {

								AUtils.executeAsyncTask(new AsyncDataDownload(isRegistration, which), "");

								dialog.cancel();
								dialog.dismiss();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();

			}});
		}else{
			AUtils.executeAsyncTask(new AsyncDataDownload(isRegistration,EdoGeneralUpdate.DOWNLOAD_ALL), "");
		}
	}

	class AsyncDataDownload extends AsyncTask<String, String, String>{
		boolean isRegistration = false;
		int whatToDownload = 0;

		public AsyncDataDownload(boolean isRegistration, int whatToDownload) {
			super();
			this.isRegistration = isRegistration;
			this.whatToDownload = whatToDownload;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showLoadingDataLayout();

		}

		@Override
		protected String doInBackground(String... params) {

			dataDownload(isRegistration, whatToDownload);

//			new StatusDAO(c).checkAndUpdateLocalStatus();

			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			app.sendEventTrack(TrackConstants.EVENT_LOGIN_CONTINUE_BUTTON);

			c.runOnUiThread(new Runnable() {public void run() {
				EdoNotification.cancelNotification(c, EdoNotification.ID_DOWNLOAD_DATA);
				EdoUI.showToast(c, c.getResources().getString(R.string.loginSuccess), null, Toast.LENGTH_LONG);
				loginProgressLayout.setProgressEnded(true);
//				bt_edo(null);
			}});


		}
	}


	public void dataDownload(boolean isRegistration, int whatToDownload){
		app.isLoginDownloadDataInProgress = true;

		EdoGeneralUpdate.getWebDBToLocalDB(c, isRegistration, whatToDownload, notification);

		app.saveLoginStatus(LUser.LOGIN_4_DATA_DOWNLOAD_FINISHED);

		startRegistration(true);

		app.isLoginDownloadDataInProgress = false;

		app.uiManager.clearUpdates();

		loginProgressLayout.setProgressPercentage(100);
	}

	@Override
	public void onInsert(EdoContainer container, LActivity a) {
	}

	@Override
	public void onUpdate(EdoContainer container, LActivity a) {

	}

	@Override
	public void onDelete(EdoContainer container, LActivity a) {

	}

	@Override
	public void onSyncList(LActivity a) {
		int percent = a.getProgressPercent();
		if(a.getContext()!=null && a.getContext().equals(LActivity.CONTEXT_PERSONAL)){
			int temp = Math.round(((float) percent / 100) * 25);
			percent = 0+temp;
		}else if(a.getContext()!=null && a.getContext().equals(LActivity.CONTEXT_CONTACT)){
			int temp = Math.round(((float) percent / 100) * 70);
			percent = 25+temp;
		}
		loginProgressLayout.setProgressPercentage(percent);
	}




	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//*		CLOSE METHODS
	//*****************************************************************************************************************************************************************************************************************************************************************************************************

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}






	@Override
	protected void onDestroy() {
		super.onDestroy();
		app.uiManager.removeListener(LISTENER_ID);
		try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
	}
	private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, final Intent intent) {
			finish();
		}
	};
}