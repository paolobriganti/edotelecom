package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.paolobriganti.android.ASI;

import java.io.Serializable;

import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LocalFiles;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.UIManager;
import io.edo.utilities.Constants;


public class Act_Snippet extends FragmentActivity implements UIManager.FilesUpdatesListener{
    Activity c;
    ThisApp app;

    private final static String LISTENER_ID = Act_Snippet.class.getName();

    private EdoContainer container;
    private LSpace space;
    private LFile snippet;

    private FileUIDAO fileDAO;

    private EditText et_title;
    private EditText et_text;

    private final static long TIME_INTERVAL = 5000;

    private String originalName = null;

    private long lastModified = 0;
    private long lastEditing = 0;

    public EdoProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_snippet);


        c = this;
        app = ThisApp.getInstance(c);

        fileDAO = new FileUIDAO(c);

        onCreated(savedInstanceState);

        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));

    }




    public void onCreated(Bundle savedInstanceState){

        if (savedInstanceState == null) {

            String extraText = null;

            if (getIntent().getExtras().containsKey(DBConstants.CONTAINER)) {
                Serializable containerSerial = getIntent().getSerializableExtra(DBConstants.CONTAINER);
                if(containerSerial!=null) {
                    container = (EdoContainer) containerSerial;
                }else{
                    container = app.getUserContact();
                }
            }
            if (getIntent().getExtras().containsKey(DBConstants.SPACE)) {
                Serializable tabSerial = getIntent().getSerializableExtra(DBConstants.SPACE);
                if(tabSerial!=null) {
                    space = (LSpace) tabSerial;
                }
            }
            if (getIntent().getExtras().containsKey(DBConstants.FILE)) {
                Serializable fileSerial = getIntent().getSerializableExtra(DBConstants.FILE);
                if(fileSerial!=null) {
                    snippet = (LFile) fileSerial;
                    if(snippet!=null){
                        snippet = fileDAO.getLDAO().get(snippet.getId());
                    }
                }
            }
            if (getIntent().getExtras().containsKey(Constants.EXTRA_TEXT)) {
                extraText = getIntent().getStringExtra(Constants.EXTRA_TEXT);
            }

            if(container!=null && space!=null) {

//                Bundle arguments = new Bundle();
//                arguments.putSerializable(DBConstants.CONTAINER, contact);
//                Frag_ContactDetail fragment = new Frag_ContactDetail();
//                fragment.setArguments(arguments);
//                getSupportFragmentManager().beginTransaction()
//                        .add(R.id.contact_detail_container, fragment)
//                        .commit();

                // Show the Up button in the action bar.
                getActionBar().setDisplayHomeAsUpEnabled(true);
                getActionBar().setTitle(R.string.Snippet);

                loading = new EdoProgressDialog(c,R.string.loading,R.string.loading);

                et_title = (EditText) findViewById(R.id.et_title);
                et_text = (EditText) findViewById(R.id.et_text);

                et_text.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        lastModified = System.currentTimeMillis();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        sendContentChanged();
                    }
                });

                if(snippet!=null){
                    originalName = snippet.getName();
                    et_title.setText(snippet.getName());
                    et_text.setText(snippet.getContent());
                }else if(extraText!=null){
                    et_text.setText(extraText);
                }


            }else{
                finish();
            }

        }



//        if(contact == null)
//            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));


    }

    @Override
    protected void onResume() {
        super.onResume();
        app.uiManager.addFilesUpdatesListener(LISTENER_ID,this);
    }

    public void bt_post(View v){
        if(snippet==null){
            if(space==null){
                finish();
                return;
            }

            snippet = LFile.newSnippetFile(space.getCurrentFolder().getId(),
                    et_title.getText().toString(),
                    et_text.getText().toString(),
                    app.getEdoUserId());

            LocalFiles files = new LocalFiles(snippet);

            Intent extra = new Intent();
            extra.putExtra(Constants.EXTRA_FILES, files);
            c.setResult(Activity.RESULT_OK, extra);
            finish();
        }else{
            sendContentCommitted();
        }
    }


    public void sendContentChanged(){
        if(snippet==null)
            return;

        if(System.currentTimeMillis()-lastEditing>TIME_INTERVAL) {
            lastEditing = System.currentTimeMillis();

            snippet.setName(et_title.getText().toString());
            snippet.setContent(et_text.getText().toString());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    fileDAO.updateContentOnly(container, space, snippet, false);
                }
            }).start();
        }
    }

    public void sendEditing(){
        if(System.currentTimeMillis()-lastEditing>TIME_INTERVAL) {
            lastEditing = System.currentTimeMillis();

            snippet.setName(et_title.getText().toString());
            snippet.setContent(et_text.getText().toString());
            new Thread(new Runnable() {
                @Override
                public void run() {
                    fileDAO.updateContentOnly(container, space, snippet, true);
                }
            }).start();
        }
    }

    public void sendContentCommitted(){
        snippet.setName(et_title.getText().toString());
        snippet.setContent(et_text.getText().toString());

        new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        loading.setTitle(snippet.getName());
                        loading.setSubTitle(R.string.loading);
                        loading.show();
                    }
                });

                if(snippet.isNotSavedToServer()){
                    fileDAO.getLDAO().update(snippet);
                    finish();
                }else if(ASI.isInternetOn(c)){
                    if(originalName!=null && originalName.equals(snippet.getName())){
                        fileDAO.updateContentOnly(container, space, snippet, false);
                    }else{
                        fileDAO.rename(container,space,snippet);
                        fileDAO.updateContentOnly(container, space, snippet, false);
                    }
                    finish();
                }else{
                    runOnUiThread(new Runnable() {
                        public void run() {
                            EdoUI.dialogNoInternet(c, new Runnable() {
                                @Override
                                public void run() {
                                    sendContentCommitted();
                                }
                            },null);
                        }
                    });
                }


                runOnUiThread(new Runnable() {
                    public void run() {
                        loading.dismiss();
                    }
                });
            }
        }).start();
    }


    @Override
    public void onUpdate(EdoContainer container, final LFile file) {
        if(System.currentTimeMillis()-lastEditing>TIME_INTERVAL) {
            c.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (snippet != null && file != null &&
                            file.getId().equals(snippet.getId())) {

                        et_title.setText(file.getName());
                        et_text.setText(file.getContent());

                    }

                }
            });
        }
    }

    @Override
    public void onDelete(EdoContainer container, LFile lFile) {

    }

    @Override
    public void onSyncList(LActivity a) {

    }

    @Override
    public void onUploadToCloud(EdoContainer container, LFile file, int progressStatus, int progressPercent) {

    }

    @Override
    public void onDownloadFromCloud(EdoContainer container, LFile file, int progressStatus, int progressPercent) {

    }


    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.snippet, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.post:
                bt_post(null);

                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        app.uiManager.removeListener(LISTENER_ID);
        try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };

    @Override
    public void onInsert(EdoContainer container, LFile lFile) {

    }


}
