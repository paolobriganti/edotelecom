package io.edo.edomobile;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.paolobriganti.android.AUtils;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.imageLoader.ImageListFragment;
import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.ArrayList;

import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.contacts.ContactsAdapter;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.ui.interfaces.HidingScrollListener;
import io.edo.utilities.TrackConstants;

/**
 * Created by PaoloBriganti on 07/07/15.
 */
public class Frag_ContactList  extends ImageListFragment
        implements ContactsAdapter.ContactClickListener, ContactsAdapter.ContactLongClickListener, UIManager.ContactsUpdatesListener,
        SearchView.OnQueryTextListener, SearchView.OnSuggestionListener{

    public Activity c;
    public ThisApp app;

    private final static String LISTENER_ID = Frag_ContactList.class.getName();

    private ContactUIDAO contactDAO;

    private ArrayList<LContact> contacts = new ArrayList<LContact>();
    private ContactsAdapter mAdapter;
    private LinearLayoutManager lm;

    private ContactPhotoLoader contactPhotoLoader;

    SearchView searchView;

    public RecyclerView mRecyclerView;
    public ProgressBar mProgressBar;
    public LinearLayout ll_blankSheet;

    public LinearLayout ll_footer;
    public View fabButtonContact;
    public View fabButtonGroup;

    public boolean selectForAdd = false;
    public static final String EXTRA_SELECT_FOR_ADD = "selectForAdd";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Frag_ContactList() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        c = getActivity();
        app = ThisApp.getInstance(c);

        contactDAO = new ContactUIDAO(c);

        contactPhotoLoader = ContactPhotoLoader.getInstance(getActivity());

        if (getArguments()!=null && getArguments().containsKey(EXTRA_SELECT_FOR_ADD)) {
            selectForAdd = getArguments().getBoolean(EXTRA_SELECT_FOR_ADD, false);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        app.uiManager.addContactsUpdatesListener(LISTENER_ID, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_list_selection, container, false);
        searchView = (SearchView) rootView.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);
        searchView.setOnSuggestionListener(this);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list);
//        mRecyclerView.setItemAnimator(new CustomItemAnimator());

        lm = new LinearLayoutManager(c);
        mRecyclerView.setLayoutManager(lm);

        mAdapter = new ContactsAdapter(getActivity(), contacts, contactPhotoLoader, null,null);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setContactClickListener(this);
        mAdapter.setLongContactClickListener(this);


        mRecyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideFooter();
            }

            @Override
            public void onShow() {
                showFooter();
            }
        });

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        ll_blankSheet = (LinearLayout) rootView.findViewById(R.id.ll_blankSheet);

        ll_footer = (LinearLayout) rootView.findViewById(R.id.ll_footer);
        fabButtonContact = (ImageButton) rootView.findViewById(R.id.ib_add_contact);
        fabButtonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateContactButtonClick(v);
            }
        });
        fabButtonGroup = (ImageButton) rootView.findViewById(R.id.ib_add_group);
        fabButtonGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateGroupButtonClick(v);
            }
        });
        ll_footer.setVisibility(selectForAdd ? View.GONE : View.VISIBLE);

        ///show progress
        mRecyclerView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        ll_blankSheet.setVisibility(View.GONE);


        loadContacts();

        return rootView;
    }

    @Override
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public ImageLoader getImageLoader() {
        return contactPhotoLoader;
    }


    public void onCreateContactButtonClick(View v){
        app.sendEventTrack(TrackConstants.EVENT_CREATE_CONTACT);
        mCallbacks.onCreateContactButtonClick();
    }

    public void onCreateGroupButtonClick(View v){
        app.sendEventTrack(TrackConstants.EVENT_CREATE_GROUP);
        mCallbacks.onCreateGroupButtonClick();
    }

    public void showList(boolean show){
        ll_blankSheet.setVisibility(show ? View.GONE : View.VISIBLE);
        mRecyclerView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
    public void hideFooter() {
        int fabBottomMargin = 0;

        if (ll_footer.getLayoutParams() instanceof FrameLayout.LayoutParams){
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) ll_footer.getLayoutParams();
            fabBottomMargin = lp.bottomMargin;
        }
        ll_footer.animate().translationY(ll_footer.getHeight()+fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();

    }

    public void showFooter(){
        ll_footer.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }




    //*******************************
    //* Contacts
    //*******************************
    public void loadContacts(){
        AUtils.executeAsyncTask(new ContactsTask(), "");
    }


    class ContactsTask extends AsyncTask<String, String, ArrayList<LContact>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<LContact> doInBackground(String... params) {
            if(selectForAdd){
                ArrayList<LContact> conts = new ArrayList<>();
                conts.add(app.getUserContact());
                ArrayList<LContact> temp = app.getActiveContacts();
                if(temp!=null){
                    conts.addAll(temp);
                }
                return conts;

            }
            return contactDAO.getLDAO().getList();
        }


        @Override
        protected void onPostExecute(final ArrayList<LContact> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    mAdapter.clearContacts();
                    if (result != null) {
                        mAdapter.addContacts(result);

                        showList(true);
                    } else{
                        showList(false);
                    }

                    mProgressBar.setVisibility(View.GONE);
                }
            });
            this.cancel(true);
        }
    }

    public void addContactToList(LContact contact){
        showList(true);
        if(!contacts.contains(contact)) {
            mAdapter.addContact(contact);
        }
    }

    public void updateContactOnList(LContact contact){
        mAdapter.updateContact(contact);
    }

    public void updateContactOnList(int index, LContact contact){
        mAdapter.updateContact(index, contact);
    }

    public void removeContactFromList(LContact contact){
        mAdapter.removeContact(contact);
    }


    @Override
    public void onClick(View v, LContact contact) {
        if(mCallbacks!=null){
            mCallbacks.onItemSelected(contact);
        }
    }

    @Override
    public void onLongClick(View v, LContact contact) {
        if(mCallbacks!=null){
            mCallbacks.onItemLongClick(contact);
        }
    }

    //*******************************
    //* UPDATES
    //*******************************
    @Override
    public void onInsert(final EdoContainer container, final LContact contact) {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!contacts.contains(contact) && (contact.isActive() || contact.isSuggested() || contact.isGroup())) {


                    addContactToList(contact);


                }

            }
        });
    }

    @Override
    public void onUpdate(final EdoContainer container, final LContact contact) {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateContactOnList(contact);
            }
        });
    }

    @Override
    public void onDelete(final EdoContainer container, final LContact contact) {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                removeContactFromList(contact);
            }
        });
    }

    @Override
    public void onSyncList(LActivity a) {
        if(a.hasBeenProcessed())
            loadContacts();
    }




    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        void onItemSelected(LContact contact);
        void onItemLongClick(LContact contact);
        void onCreateContactButtonClick();
        void onCreateGroupButtonClick();
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(LContact contact) {
        }

        @Override
        public void onItemLongClick(LContact contact) {

        }

        @Override
        public void onCreateContactButtonClick() {
        }

        @Override
        public void onCreateGroupButtonClick() {
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }



    //*******************************
    //* SEARCH
    //*******************************
    public void searchContacts(String text){
        new AsyncSearchContacts(text).execute();
    }

    String searchName;
    LContact newContact = null;

    class AsyncSearchContacts extends AsyncTask<String, ArrayList<LContact>, ArrayList<LContact>>{
        String text;

        public AsyncSearchContacts(String text) {
            super();
            this.text = text;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }


        @Override
        protected ArrayList<LContact> doInBackground(String... params) {

            if(JavaUtils.isNotEmpty(text)){
                return contactDAO.getLDAO().getContactByNameLike(text,false);
            }

            return null;
        }


        @Override
        protected void onPostExecute(final ArrayList<LContact> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            this.cancel(true);

            if(JavaUtils.isEmpty(text)){
                newContact = null;
                searchView.setSuggestionsAdapter(null);

                loadContacts();

            }else if(result!=null && result.size()>0 && text.equals(searchName)){
                newContact = null;

                c.runOnUiThread(new Runnable() {
                    public void run() {
                        if (result != null) {
                            mAdapter.clearContacts();
                            mAdapter.setSearchText(text);
                            mAdapter.addContacts(result);
                        }
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });

            }else{
                String email = searchName;
                String name = email.contains("@")? email.split("@")[0] : searchName;

                newContact = new LContact(name, (String)null, (String)null);
                newContact.setPrincipalEmailAddress(email);
                newContact.setStatus(LContact.STATUS_CREATION_IN_PROGRESS);

                c.runOnUiThread(new Runnable() {
                    public void run() {
                        mAdapter.clearContacts();
                        mAdapter.addContact(newContact);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });


            }
        }
    }


    @Override
    public boolean onSuggestionClick(int position) {

        return false;
    }


    @Override
    public boolean onSuggestionSelect(int position) {
        // TODO Auto-generated method stub
        return false;
    }

    boolean trackSended = false;

    @Override
    public boolean onQueryTextChange(String newText) {
        searchName = newText;

        if(JavaUtils.isNotEmpty(newText)){
            searchContacts(newText);

            if(!trackSended){
                trackSended = true;
                app.sendEventTrack(TrackConstants.EVENT_SEARCH_CONTACT);
            }
        }else{
            loadContacts();
        }
        return false;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        app.uiManager.removeListener(LISTENER_ID);
    }
}

