package io.edo.edomobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.paolobriganti.android.AUtils;

import java.io.Serializable;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoMessageBuilder;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.utilities.Constants;
import io.edo.utilities.TrackConstants;


/**
 * An activity representing a list of Contacts. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link Act_ContactDetail} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link Frag_ContactActiveList} and the item details
 * (if present) is a {@link Frag_ContactTimeline}.
 * <p/>
 * This activity also implements the required
 * {@link Frag_ContactActiveList.Callbacks} interface
 * to listen for item selections.
 */
public class Act_ContactActiveList extends Act_ContactBase
        implements Frag_ContactActiveList.Callbacks {


    ContactPhotoLoader contactPhotoLoader;

    //    private View mFabButton;
    private View mHeader;

    private ImageView iv_userPhoto;
    private TextView tv_userName;
    private TextView tv_userEmail;


    public TextView tv_blankSheet;

    private LContact userContact;

    boolean activateOnItemClick;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AUtils.sendPushNotificationHeartBeat(this);
        setContentView(R.layout.activity_contact_list);

        getActionBar().setDisplayHomeAsUpEnabled(false);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        getActionBar().setTitle("");

        userContact = app.getUserContact();

//        mFabButton = findViewById(R.id.fab_button);
        mHeader = findViewById(R.id.activity_transition_header);

        iv_userPhoto = (ImageView) findViewById(R.id.iv_user_photo);
        tv_userName = (TextView) findViewById(R.id.tv_user_name);
        tv_userEmail = (TextView) findViewById(R.id.tv_user_email);

        contactPhotoLoader = ContactPhotoLoader.getInstance(this);
        contactPhotoLoader.loadImage(userContact, iv_userPhoto);
        tv_userName.setText(R.string.you);
        tv_userEmail.setText(R.string.personalFiles);


        tv_blankSheet = (TextView) findViewById(R.id.tv_blankSheet);

        String message = EdoMessageBuilder.getMessageBlancSheetOfActiveContactList(c);
        tv_blankSheet.setText(Html.fromHtml(message));


        mHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemSelected(userContact);
            }
        });

        isContactDetailActivity = false;

        if (findViewById(R.id.contact_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((Frag_ContactActiveList) getSupportFragmentManager()
                    .findFragmentById(R.id.contact_list))
                    .setActivateOnItemClick(true);
            setActivateOnItemClick(true);

            onItemSelected(app.getUserContact());
            getActionBar().setLogo(R.color.transparent);
            getActionBar().setIcon(R.color.transparent);

        }
        invalidateOptionsMenu();

//        processExtras(getIntent());

//        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
//            setExitAnimationLollipop();
//        }

        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));
    }

    @Override
    protected void onResume() {
        super.onResume();
        AUtils.sendPushNotificationHeartBeat(c);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        processExtras(intent);
    }

    public void processExtras(Intent intent){
        if(intent!=null && intent.getExtras()!=null) {

            if (intent.getExtras().containsKey(DBConstants.CONTAINER)) {

                Serializable contactSerial = intent.getSerializableExtra(DBConstants.CONTAINER);
                if (contactSerial != null) {
                    LContact contact = (LContact) contactSerial;
                    onItemSelected(contact);
                }
            }

        }

    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        this.activateOnItemClick = activateOnItemClick;
    }



    @Override
    public void onItemSelected(LContact contact) {
        if(contact==null)
            return;

        if(activateOnItemClick)
            setSelected(contact);

        app.setCurrentTimelineContainerId(contact);

        this.contact = contact;

        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            setContactDetailFragment(Frag_ContactDetail.SECTION_TIMELINE, Frag_ContactOrganizer.SECTION_SPACES);

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, Act_ContactDetail.class);
            detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            detailIntent.putExtra(DBConstants.CONTAINER, contact);

            if (getIntent().getExtras()!=null && getIntent().getExtras().containsKey(DBConstants.SPACE)) {
                detailIntent.putExtra(DBConstants.SPACE, getIntent().getSerializableExtra(DBConstants.SPACE));
            }




//            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
//                startActivityWithTransition(detailIntent);
//            }else{
            startActivity(detailIntent);
//            }

        }
    }




    public void setSelected(LContact contact){
        if(contact.equals(app.getUserContact())){
            mHeader.setBackgroundResource(R.color.edo_blue);
            tv_userName.setTextColor(c.getResources().getColor(R.color.white));
            tv_userEmail.setTextColor(c.getResources().getColor(R.color.edo_gray_very_light));

            if(mContactSelectedListener!=null){
                mContactSelectedListener.onUserSelected(contact);
            }

        }else{
            mHeader.setBackgroundResource(R.drawable.bg_card_blue_light);
            tv_userName.setTextColor(c.getResources().getColor(R.color.edo_black));
            tv_userEmail.setTextColor(c.getResources().getColor(R.color.edo_gray_dark));


            if(mContactSelectedListener!=null){
                mContactSelectedListener.onContactSelected(contact);
            }

        }
    }





    @Override
    public void onOnViewsLoaded() {
        processExtras(getIntent());
    }


    public void onFabPressed(View view) {
        Intent addContact = new Intent(c, Act_ContactSelector.class);

        Frag_ContactActiveList fragContactList = ((Frag_ContactActiveList) getSupportFragmentManager()
                .findFragmentById(R.id.contact_list));

        if(fragContactList!=null){
            fragContactList.startActivityForResult(addContact, Constants.CONTACT_SELECT_RESULT);
        }else{
            c.startActivityForResult(addContact, Constants.CONTACT_SELECT_RESULT);
        }

        app.sendEventTrack(TrackConstants.EVENT_CLICK_ON_ADD_CONTACT);
    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void startActivityWithTransition(Intent intent){
//        ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(
//                this, Pair.create(mFabButton, "fab"));
//        startActivity(intent, transitionActivityOptions.toBundle());
//    }


//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void setExitAnimationLollipop(){
//        Slide slideExitTransition = new Slide(Gravity.BOTTOM);
//        slideExitTransition.excludeTarget(android.R.id.navigationBarBackground, true);
//        slideExitTransition.excludeTarget(android.R.id.statusBarBackground, true);
//        slideExitTransition.excludeTarget(R.id.activity_transition_header, true);
//        slideExitTransition.excludeTarget(R.id.row_container2, true);
//        slideExitTransition.excludeTarget(R.id.contact_list, true);
//        slideExitTransition.excludeTarget(getActionBarView().getId(), true);
//        getWindow().setExitTransition(slideExitTransition);
//    }

    public View getActionBarView() {
        Window window = getWindow();
        View v = window.getDecorView();
        int resId = getResources().getIdentifier("action_bar_container", "id", "android");
        return v.findViewById(resId);
    }




    ContactSelectedListener mContactSelectedListener;

    public void setContactSelectedListener(ContactSelectedListener listener) {
        mContactSelectedListener = listener;
    }

    public interface ContactSelectedListener {
        void onUserSelected(LContact contact);
        void onContactSelected(LContact contact);
    }










    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{unregisterReceiver(mKillReceiver);}catch(Exception e){}


    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };
}
