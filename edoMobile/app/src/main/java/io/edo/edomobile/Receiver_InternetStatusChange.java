package io.edo.edomobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;

import com.paolobriganti.android.ASI;

import java.util.ArrayList;

import io.edo.api.TransferOperation;
import io.edo.api.google.gcm.GCMUtils;
import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.global.uibeans.MessageUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.web.request.EdoAuthManager;


public class Receiver_InternetStatusChange extends BroadcastReceiver {
	public static final String BROADCAST_INTERNET_ON = "io.edo.edomobile.INTERNET_ON";
	public static final String BROADCAST_INTERNET_OFF = "io.edo.edomobile.INTERNET_OFF";
	@Override
	public void onReceive(final Context c, final Intent intent) {
		new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
			doAction(c, intent);
		}}.start();
	}

	
	public synchronized void doAction(final Context c, Intent intent){
		final ThisApp app = ThisApp.getInstance(c);

		if(app.isLoginComplete() && ASI.isInternetOn(c)){

			ArrayList<TransferOperation> failedOperations = new ArrayList<TransferOperation>(app.failedOperations.values());

			if(failedOperations!=null && failedOperations.size()>0){


				for(TransferOperation operation:failedOperations){

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if(ASI.isInternetOn(c)){
						Service_DwnUpl.startOperation(c, operation.action, operation.source);
						app.failedOperations.remove(operation.id);
					}
				}
			}

			GCMUtils GCM = new GCMUtils(c);
			if(!GCM.hasRegistrationId(c)){
				new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
					GCMUtils.updateRegistrationId(c, app.getDB(), true);
				}}.start();
			}

			if (!EdoAuthManager.hasRefreshToken(c)) {
				EdoAuthManager.refreshToken(c, true);
			}


			FileUIDAO fileDAO = new FileUIDAO(c);
			fileDAO.saveToServerFilesWithError();

			try {
				MessageUIDAO messageUIDAO = new MessageUIDAO(c);
				ArrayList<LActivity> activities = new LActivityDAO(c).getAllMessagesWithError();
				if (activities != null && activities.size() > 0) {
					for (LActivity a : activities) {
						messageUIDAO.sendMessage(a);
					}
				}
			}catch (Exception e){}


		}else{
			Intent b = new Intent(BROADCAST_INTERNET_OFF);
			c.sendBroadcast(b);
		}
	}
	
}