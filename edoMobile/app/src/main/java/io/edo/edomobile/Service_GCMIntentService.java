/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.edo.edomobile;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import io.edo.api.google.gcm.GCMUtils;
import io.edo.db.global.EdoActivityManager;
import io.edo.db.web.request.EdoAuthManager;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;


/**
 * IntentService responsible for handling GCM messages.
 */
public class Service_GCMIntentService extends IntentService {
	private Handler handler;
	NotificationCompat.Builder builder;

	public Service_GCMIntentService() {
		super("GcmIntentService");
	}


	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that GCM will be
			 * extended in the future with new message types, just ignore any message types you're
			 * not interested in, or that you don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification("Deleted messages on server: " + extras.toString());
				for (int i = 0; i < 5; i++) {
					eLog.i(GCMUtils.TAG, "Working... " + (i + 1)
							+ "/5 @ " + SystemClock.elapsedRealtime());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}

				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				// Post notification of received message.
				sendNotification(extras.getString("message"));
				eLog.i(GCMUtils.TAG, "Received: " + extras.getString("message"));
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		Receiver_GCMBroadcastReceiver.completeWakefulIntent(intent);
	}

	@Override  
	public int onStartCommand(Intent intent, int flags, int startId) {  
		handler = new Handler();  
		return super.onStartCommand(intent, flags, startId);  
	}  

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private synchronized void sendNotification(String msg) {

		ThisApp app = ThisApp.getInstance(this);

		if(app.isLoginComplete()) {

			if (!EdoAuthManager.hasRefreshToken(app)) {
				int response = EdoAuthManager.refreshToken(this, false);

				if (response == Constants.RESPONSE_FAILED) {
					return;
				}
			}

			try {


				if (!msg.equalsIgnoreCase("hb")) {
					EdoActivityManager.processActivity(this, msg);
				}
//			else{
				//  EdoNotification edoNoty = new EdoNotification(this, Act_00_SplashScreen.class, db);
				//  edoNoty.welcomeNotification();
//			}

			} catch (Exception e) {

				eLog.e("GCM_ERROR", e.getMessage() != null ? e.getMessage() : "");

			}
		}
	}
}

