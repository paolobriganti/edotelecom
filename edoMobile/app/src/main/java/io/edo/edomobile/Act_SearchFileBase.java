package io.edo.edomobile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.ui.EdoIntentManager;
import io.edo.ui.adapters.files.CursorAdapterFile;
import io.edo.ui.imageutils.ThumbnailFileLoader;

/**
 * Created by PaoloBriganti on 05/07/15.
 */
public class Act_SearchFileBase extends FragmentActivity
        implements SearchView.OnQueryTextListener, SearchView.OnSuggestionListener {


    Activity c;
    ThisApp app;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        c = this;
        app = ThisApp.getInstance(this);

    }


    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		SEARCHVIEW
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public ArrayList<LFile> resultFiles;
    public SearchView searchView;
    public MenuItem searchItem;
    ThumbnailFileLoader thumbnailFileLoader;
    public void setupSearchView(SearchView searchView, MenuItem searchItem) {
        this.searchView = searchView;
        this.searchItem = searchItem;

        searchView.setQueryHint(c.getResources().getString(R.string.searchFiles));

        if(searchView!=null){
            if (isAlwaysExpanded()) {
                searchView.setIconifiedByDefault(false);
            } else if(searchItem!=null){
                searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM
                        | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
            }
            searchView.setOnQueryTextListener(this);
            searchView.setOnSuggestionListener(this);
            thumbnailFileLoader = ThumbnailFileLoader.getInstance(this);
        }

//        if(searchItem!=null){
//            searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
//                @Override
//                public boolean onMenuItemActionExpand(MenuItem item) {
//
//                    return true;
//                }
//
//                @Override
//                public boolean onMenuItemActionCollapse(MenuItem item) {
//                    onInboxExit();
//                    return true;
//                }
//            });
//        }

    }

    public boolean onQueryTextChange(String newText) {
        if(JavaUtils.isNotEmpty(newText)){
            Cursor cursor = null;

            cursor = new LFileDAO(c).getFileCursorByLikeName(newText);

            CursorAdapterFile simple = new CursorAdapterFile((Activity) c,cursor, ThumbnailFileLoader.getImageViewSideSize(c), thumbnailFileLoader, newText);

            searchView.setSuggestionsAdapter(simple);

            resultFiles = new LFileDAO(c).getFilesFromCursor(cursor);
            return cursor != null && cursor.getCount() > 0;
        }
        return false;
    }


    @Override
    public boolean onSuggestionClick(int position) {
        if(searchItem!=null)searchItem.collapseActionView();

        Act_SearchFileBase.this.finish();

        LFile file = resultFiles.get(position);
        Intent i = EdoIntentManager.getFileIntent(c, file);
        startActivity(i);
        return true;
    }


    @Override
    public boolean onSearchRequested() {
        return super.onSearchRequested();
    }

    @Override
    public boolean onSuggestionSelect(int arg0) {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    protected boolean isAlwaysExpanded() {
        return false;
    }

    public class SuggestionSimpleCursorAdapter
            extends SimpleCursorAdapter
    {

        public SuggestionSimpleCursorAdapter(Context context, int layout, Cursor c,
                                             String[] from, int[] to) {
            super(context, layout, c, from, to);
        }

        public SuggestionSimpleCursorAdapter(Context context, int layout, Cursor c,
                                             String[] from, int[] to, int flags) {
            super(context, layout, c, from, to, flags);
        }

        @Override
        public CharSequence convertToString(Cursor cursor) {

            int indexColumnSuggestion = cursor.getColumnIndex(LocalDBHelper.KEY_NAME);

            return cursor.getString(indexColumnSuggestion);
        }


    }


}
