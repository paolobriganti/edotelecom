package io.edo.edomobile;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.paolobriganti.utils.imageLoader.ImageListFragment;
import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LocalFiles;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.adapters.files.FilesAdapter;
import io.edo.ui.adapters.files.FilesPickerAdapter;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.ui.itemanimator.CustomItemAnimator;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 12/06/15.
 */
public abstract class Frag_Base_FilePicker extends ImageListFragment {
    public FragmentActivity c;
    public ThisApp app;

    private TextView tv_breadcrumbs;
    public RecyclerView mRecyclerView;
    public ProgressBar mProgressBar;

    public EdoProgressDialog loading;

    public FilesPickerAdapter mAdapter;

    private HashMap<String, LFile> selFiles = new HashMap<String, LFile>();
    private GridLayoutManager lm;

    private LFile currentFolder = null;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Frag_Base_FilePicker() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        c = getActivity();
        app = ThisApp.getInstance(c);

        c.getActionBar().setTitle(c.getResources().getString(R.string.nofilesSelected));

        loading = new EdoProgressDialog(c, R.string.loading, R.string.loading);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(getContentLayoutResourceId(), container, false);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list);
        mRecyclerView.setItemAnimator(new CustomItemAnimator());

        tv_breadcrumbs = (TextView) rootView.findViewById(R.id.tv_breadcrumbs);
        tv_breadcrumbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upLevel();
            }
        });

        ///show progress
        mRecyclerView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        lm = new GridLayoutManager(c, EdoUI.getFilesColumnsNumber(c));
        mRecyclerView.setLayoutManager(lm);


        final Button bt_ok = (Button) rootView.findViewById(R.id.bt_ok);
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt_ok(v);
            }
        });
        final Button bt_cancel = (Button) rootView.findViewById(R.id.bt_cancel);
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt_cancel(v);
            }
        });

        setBackPressed(rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new FilesPickerAdapter(c,getThumbnailFileLoader(), new ArrayList<LFile>());
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setFileClickListener(new FilesAdapter.FileClickListener() {
            @Override
            public void onClick(FilesAdapter.ViewHolder v, LFile file) {
                if (file.isFolder()) {
                    refreshFilesList(file);
                } else {
                    onFileClick(v, file);
                }
            }
        });

        refreshFilesList(currentFolder);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public ImageLoader getImageLoader() {
        return getThumbnailFileLoader();
    }

    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ABSTRACT METHODS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    public abstract int getContentLayoutResourceId();

    public abstract ThumbnailFileLoader getThumbnailFileLoader();

    public abstract LSpace getSpace();

    public abstract ArrayList<LFile> getFiles(LFile folder);

    public abstract LFile getEdoFile(Object file);

    public abstract Object getFile(LFile edoFile);

    public abstract String getUniqueKeyForFile(LFile edoFile);

    public abstract LFile getParentFolder(LFile currentFolder);

    public abstract String getBreadCrumbs(LFile currentFolder);

    public abstract Comparator<LFile> getFileComparator();




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		FILES
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public void refreshFilesList(LFile folder){
        new FilesTask().execute(folder);
    }

    public class FilesTask extends AsyncTask<LFile, LFile, ArrayList<LFile>> {

        String breadCrumbs = "";

        public FilesTask() {
            super();
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            c.runOnUiThread(new Runnable() {
                public void run() {

                    ///show progress
                    mRecyclerView.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);

                }
            });
        }

        @Override
        protected ArrayList<LFile> doInBackground(LFile... params) {
            LFile folder = null;
            if(params!=null && params.length>0){
                folder = params[0];
            }
            setCurrentFolder(folder);
            breadCrumbs = getBreadCrumbs(currentFolder);
            ArrayList<LFile> files = getFiles(folder);
            if(files!=null)
                Collections.sort(files, getFileComparator());
            return files;
        }

        @Override
        protected void onPostExecute(final ArrayList<LFile> result) {
            super.onPostExecute(result);


            c.runOnUiThread(new Runnable() {
                public void run() {

                    tv_breadcrumbs.setText(breadCrumbs);

                    mAdapter.clearFiles();
                    if (result != null && result.size()>0) {
                        mAdapter.addFiles(result);
                    }
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);

                }
            });
        }
    }

    public void upLevel(){
        LFile parent = getParentFolder(currentFolder);
        refreshFilesList(parent);
    }

    private void setCurrentFolder(LFile folder){
        this.currentFolder = folder;
    }

    private LFile getCurrentFolder() {
        return currentFolder;
    }

    public void onFileClick(FilesAdapter.ViewHolder v, LFile file){
        if(!isSelected(file))
            addFileToSelectedFiles(v,file);
        else
            removeFileFromSelectedFiles(v, file);
        if(c.getActionBar()!=null)
            c.getActionBar().setTitle(selFiles.size()+" "+c.getResources().getString(R.string.filesSelected));
    }


    private void addFileToSelectedFiles(FilesAdapter.ViewHolder v, LFile file){

        if(selFiles.size() < Constants.FILE_PICKERS_SELECTION_LIMIT) {
            file.setIsSelected(true);
            selFiles.put(getUniqueKeyForFile(file),file);
            v.setSelected(true);
//            addFileView(newFile);
        } else {
            c.runOnUiThread(new Runnable() {
                public void run() {
                    String selectionLimit = c.getResources().getString(R.string.selectionLimit).replace(Constants.PH_NUM, ""+Constants.FILE_PICKERS_SELECTION_LIMIT);
                    EdoUI.showToast(c, selectionLimit, null, Toast.LENGTH_LONG);
                }});
        }

    }

    private void removeFileFromSelectedFiles(FilesAdapter.ViewHolder v, LFile file){
        file.setIsSelected(false);
        selFiles.remove(getUniqueKeyForFile(file));
        v.setSelected(false);


//        removeFileView(newFile);
    }


    public boolean isSelected(LFile edoFile){
        return selFiles.containsKey(getUniqueKeyForFile(edoFile));
    }










    public class FileFolderFirstComparator implements Comparator<LFile> {
        @Override
        public int compare(LFile f1, LFile f2) {
            if(f1 == f2) {
                return 0;
            }
            if(f1.isFolder() && !f2.isFolder()) {
                // Show directories above files
                return -1;
            }
            if(!f1.isFolder() && f2.isFolder()) {
                // Show files below directories
                return 1;
            }
            // Sort the directories alphabetically
            return f1.getName().compareToIgnoreCase(f2.getName());
        }
    }





    public class FileDateDescComparator implements Comparator<LFile> {
        @Override
        public int compare(LFile f1, LFile f2) {
            if(f1 == f2) {
                return 0;
            }
            if(f1.getmDate()!=null && f2.getmDate()==null) {
                return -1;
            }
            if(f1.getmDate()==null && f2.getmDate()!=null) {
                return 1;
            }
            return new Date(f2.getmDate()).compareTo(new Date(f1.getmDate()));
        }
    }

    public void bt_ok (View v){
        if(selFiles!=null && selFiles.size()>0){
            loading.setSubTitle(null);
            loading.show();

            new Thread(){@Override public void run(){
                Looper.myLooper();//Looper.prepare();

                // Set result
                Intent extra = new Intent();

                ArrayList<LFile> localFiles = new ArrayList<LFile>();
                for(LFile lfile: selFiles.values()){
                    lfile.setmDate(System.currentTimeMillis());
                    lfile.setIsSelected(false);
                    localFiles.add(lfile);
                    loading.setLFile(lfile, null);
                }
                LocalFiles files = new LocalFiles(localFiles);
                extra.putExtra(Constants.EXTRA_FILES, files);
                c.setResult(Activity.RESULT_OK, extra);
                // Finish the activity
                c.finish();

                loading.dismiss();
            }}.start();
        }else{
            c.finish();
        }
    }


    public void bt_cancel (View v){
        c.finish();
    }























    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		RECEIVERS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************


    public void setBackPressed(View v){
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return onBackPressed();
                }
                return false;
            }
        } );
    }


    public boolean onBackPressed(){
        upLevel();
        return true;
    }
}
