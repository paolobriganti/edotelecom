package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;

import com.paolobriganti.android.AUI;
import com.paolobriganti.utils.JavaUtils;

import java.io.Serializable;
import java.util.ArrayList;

import io.edo.db.global.beans.ServiceDAO;
import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.global.uibeans.SpaceUIDAO;
import io.edo.db.local.LocalDBManager;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoDialogListView;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.ServicesUI;
import io.edo.ui.adapters.files.SpacesAdapter;
import io.edo.ui.imageutils.ThumbnailSpaceLoader;
import io.edo.ui.itemanimator.CustomItemAnimator;
import io.edo.utilities.Constants;


public class Act_SpaceEditor extends FragmentActivity
        implements SpacesAdapter.SpaceClickListener{
    public ThisApp app;

    private EdoUI edoUI;
    private FragmentActivity c;
    private LocalDBManager db;

    public EdoProgressDialog loading;

    private SpaceUIDAO spaceDAO;
    private ArrayList<LSpace> spaces = new ArrayList<LSpace>();
    private ThumbnailSpaceLoader thumbnailSpaceLoader;

    private EdoContainer container;

    private boolean isNew = false;
    private LSpace space = null;

    private ServiceDAO serviceDAO;
    private ArrayList<LService> services;

    private GridLayoutManager lm;
    private SpacesAdapter adapter;
    public RecyclerView mRecyclerView;
    private EditText et_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        AUI.setStatusBarColor(this, this.getResources().getColor(R.color.edo_blue_dark));
        EdoUI.setOrientationTo(this);


        super.onCreate(savedInstanceState);
        c = this;
        app = ThisApp.getInstance(this);
        db = app.getDB();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.fragment_spaceeditor);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        edoUI = new EdoUI(c);
        loading = new EdoProgressDialog(this, R.string.loading, R.string.loading);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(DBConstants.CONTAINER)) {
                Serializable contSerial = extras.getSerializable(DBConstants.CONTAINER);
                if (contSerial != null) {
                    container = (EdoContainer) contSerial;
                }
            }

            Serializable spaceSerial = extras.getSerializable(DBConstants.SPACE);
            if(spaceSerial!=null){
                space = (LSpace) spaceSerial;
                isNew = false;
            }
        }



        et_name = (EditText) this.findViewById(R.id.et_name);
        mRecyclerView = (RecyclerView) this.findViewById(R.id.list);
        mRecyclerView.setItemAnimator(new CustomItemAnimator());

        lm = new GridLayoutManager(c, EdoUI.getFilesColumnsNumber(c));
        mRecyclerView.setLayoutManager(lm);

        if(space==null){
            space = new LSpace();
            isNew = true;
            getActionBar().setTitle(R.string.addSpace);
        }else{
            getActionBar().setTitle(R.string.editSpace);
        }

        spaceDAO = new SpaceUIDAO(this);
        serviceDAO = new ServiceDAO(c);
        thumbnailSpaceLoader = ThumbnailSpaceLoader.getInstance(c);

        new Thread(){@Override public void run(){
            Looper.myLooper();//Looper.prepare();
            services = new ArrayList<LService>();
            services.add(serviceDAO.getLDAO().getPrincipalService());
        }}.start();

        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));


        if(!isNew && space!=null){
            et_name.setText(space.getName());
        }

        spaces = spaceDAO.getLDAO().getSpaceSamples();

        adapter = new SpacesAdapter(this,thumbnailSpaceLoader,(LContact)container,spaces);
        adapter.setSpaceClickListener(this);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v, LSpace selectedSpace) {
        space.setThumbnail(selectedSpace.getIconName());


        if (JavaUtils.isEmpty(et_name.getText().toString())) {
            et_name.setText(selectedSpace.getName());
        }

        for(LSpace sspace:spaces){
            sspace.setIsSelected(false);
        }
        selectedSpace.setIsSelected(true);

        adapter.notifyDataSetChanged();
    }


    public void bt_back(View v){
        Act_SpaceEditor.this.finish();
    }




//    class SpaceIconsAdapter extends BaseAdapter {
//        private LayoutInflater layoutInflater;
//
//        public SpaceIconsAdapter(Activity activity) {
//            // TODO Auto-generated constructor stub
//            layoutInflater = (LayoutInflater) activity
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        }
//
//        @Override
//        public int getCount() {
//            return spaces.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            // TODO Auto-generated method stub
//            return position;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            // TODO Auto-generated method stub
//            return position;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//
//            // Inflate the item layout and set the views
//            View listItem = convertView;
//            if (listItem == null) {
//                listItem = layoutInflater.inflate(R.layout.card_space, null);
//            }
//
//
//            final SquareImageView thumb = (SquareImageView) listItem.findViewById(R.id.imageView);
//            thumbnailSpaceLoader.loadImage(spaces.get(position), thumb);
//
//            FrameLayout fl_overlay = (FrameLayout)listItem.findViewById(R.id.fl_overlay);
//
//            ImageView iv_overlay = (ImageView) listItem.findViewById(R.id.iv_overlay);
//            iv_overlay.setImageResource(R.drawable.ic_action_accept_w);
//            iv_overlay.setVisibility(View.VISIBLE);
//
//            if(space!=null && JavaUtils.isNotEmpty(space.getIconName()) &&
//                    spaces.get(position).getIconName().equals(space.getIconName()))
//                fl_overlay.setVisibility(View.VISIBLE);
//            else
//                fl_overlay.setVisibility(View.GONE);
//            return listItem;
//        }
//
//
//    }






    public void bt_post(View v){
        if(space!=null){
            if(JavaUtils.isNotEmpty(et_name.getText().toString())){

                if(JavaUtils.isNotEmpty(space.getServiceId())){

                    editSpace();

                }else if(services!=null && services.size()==1){

                    final LService service = services.get(0);
                    space.setServiceId(service.getId());
                    editSpace();

                }else if(services!=null && services.size()>1){

                    final EdoDialogListView dialog = new EdoDialogListView(c);
                    dialog.setTitle(R.string.cloudService);
                    dialog.setText(R.string.chooseAthirdPartyCloudServer);
                    dialog.setAdapter(ServicesUI.getCloudUserServicesAdapter(c, db, services));
                    dialog.setOnListItemClickListener(new EdoDialogListView.OnListItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                            dialog.cancel();

                            final LService service = services.get(position);
                            space.setServiceId(service.getId());
                            editSpace();
                        }
                    });
                    dialog.setPositiveButtonText(R.string.back);
                    dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });
                    dialog.show();
                }else{
                    c.startActivity(new Intent(c, Act_30_ThirdPartyServices.class));
                }

            }else{
                edoUI.showToastLong(c.getResources().getString(R.string.chooseANameForThisSpace), null);
            }
        }else{
            edoUI.showToastLong(c.getResources().getString(R.string.chooseIcon), null);
        }

    }

    public void editSpace(){

        boolean isInternetOn = !EdoUI.dialogNoInternet((Activity) c,
                new Runnable() {public void run() {
                    editSpace();
                }},
                null);

        if(isInternetOn){

            String name = et_name.getText().toString();
            space.setName(name);



            loading.setTitle(name);
            loading.show();


            new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
                LSpace spaceWithSameName = spaceDAO.getLDAO().getSpaceByName((LContact)container,space.getName());
                if(spaceWithSameName==null || spaceWithSameName.getId().equals(space.getId())){
                    boolean done = false;

                    if(isNew){

                        LSpace newSpace = new LSpace(c, space.getName(), container, space.getServiceId(), null, null);
                        newSpace.setThumbnail(space.getThumbnail());
                        done = spaceDAO.addNewSpaceToContact((LContact)container,newSpace)!=null;
                    }else{
                        FileUIDAO fileUIDAO = new FileUIDAO(c);
                        done = fileUIDAO.update(container,space,space)!=null;
                    }

                    if(done){

                        Intent extra = new Intent();
                        extra.putExtra(DBConstants.SPACE, space);
                        setResult(RESULT_OK, extra);

                        loading.dismiss();
                        Act_SpaceEditor.this.finish();

                    }else{
                        runOnUiThread(new Runnable() {public void run() {
                            loading.dismiss();
                            edoUI.showToastLong(c.getResources().getString(R.string.errorRetry), null);
                        }});
                    }
                }else{
                    loading.dismiss();
                    runOnUiThread(new Runnable() {public void run() {
                        EdoUI.oneButtonDialog(c, space.getName(), c.getResources().getString(R.string.changeTheNameOfSpace));
                    }});
                }
            }}.start();

        }
    }


//*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.spaceseditor, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.post:
                bt_post(null);

                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));

    }







    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mKillReceiver);
    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };



}
