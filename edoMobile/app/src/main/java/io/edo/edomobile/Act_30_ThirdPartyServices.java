package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.paolobriganti.android.ASI;
import com.paolobriganti.android.AUI;

import java.util.ArrayList;
import java.util.Arrays;

import io.edo.api.google.EdoGoogleManager;
import io.edo.db.global.beans.ServiceDAO;
import io.edo.db.local.beans.LService;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.ServicesUI;
import io.edo.ui.adapters.ListAdapterImageTextSubText;
import io.edo.utilities.Constants;

public class Act_30_ThirdPartyServices extends Activity {
	public ThisApp app;

	Activity c;

	ServiceDAO serviceDAO;

//	MenuItem menu_add;

	String userId = null;

	private ListView lv;

	public EdoProgressDialog loading;

	IntentFilter IF = new IntentFilter();


	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		AUI.setStatusBarColor(this, this.getResources().getColor(R.color.edo_blue_dark));
		EdoUI.setOrientationTo(this);


		app = ThisApp.getInstance(this);
		c = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_30_thirdpartyservices);

		registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));

		loading = new EdoProgressDialog(c, R.string.thirdServices, R.string.loading);


		lv = (ListView) this.findViewById(R.id.lv_services);
		getActionBar().setTitle(R.string.thirdServices);
		getActionBar().setLogo(R.drawable.ic_status_cloud);


		userId = app.getEdoUserId();
		serviceDAO = new ServiceDAO(c);

		new Thread(){@Override public void run(){Looper.myLooper();
			ArrayList<LService> services = serviceDAO.getLDAO().getList();
			if(services!=null && services.size()>0){
				final ListAdapterImageTextSubText adapter = ServicesUI.getUserServicesAdapter(c, services);
				runOnUiThread(new Runnable() {public void run() {
					lv.setAdapter(adapter);
				}});
			}
		}}.start();
	}


	@Override
	public void onResume() {
		super.onResume();


		if(loading != null){
			loading.resume();
		}
		updateUserServices(true);

	}


	private void updateUserServices(final boolean metaSync){
		if(ASI.isInternetOn(c)){
			new Thread(){@Override public void run(){Looper.myLooper();
				LService[] services = serviceDAO.downloadAllServices();
				if(services!=null && services.length>0){
					final ListAdapterImageTextSubText adapter = ServicesUI.getUserServicesAdapter(c, new ArrayList<LService>(Arrays.asList(services)));
					runOnUiThread(new Runnable() {public void run() {
//					menu_add.setVisible(false);
						lv.setAdapter(adapter);
					}});
				}
			}}.start();

		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if(loading != null){
			loading.pause();
		}
	}

	public void bt_google(View v){
		EdoGoogleManager.requestAuth(c, userId);
	}

	public void bt_dropbox(View v){
	}






	/* ACTION BAR */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.thirdpartyservices, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

//		menu_add = menu.findItem(R.id.add);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		// Handle action buttons
		switch(item.getItemId()) {

//		case R.id.add:
//			bt_google(null);
//			break;

//		case R.id.logout:
//			bt_logout(null);
//			break;

			default:
				break;

		}
		return super.onOptionsItemSelected(item);
	}







	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mKillReceiver);
	}
	private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, final Intent intent) {
			finish();
		}
	};
}