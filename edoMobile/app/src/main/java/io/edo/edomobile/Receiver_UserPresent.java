package io.edo.edomobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.paolobriganti.android.AUtils;

public class Receiver_UserPresent extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        AUtils.sendPushNotificationHeartBeat(context);
    }
}
