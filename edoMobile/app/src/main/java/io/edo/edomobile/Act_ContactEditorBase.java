package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.paolobriganti.utils.JavaUtils;

import java.io.Serializable;

import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.global.uibeans.SpaceUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;
import io.edo.utilities.TrackConstants;

/**
 * Created by PaoloBriganti on 07/07/15.
 */
public abstract class Act_ContactEditorBase extends FragmentActivity{
    Activity c;
    ThisApp app;

    EdoProgressDialog loading;
    EdoUI edoUI;

    ContactUIDAO contactDAO;

    LContact contactToEdit = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        c = this;
        app = ThisApp.getInstance(c);

        contactDAO = new ContactUIDAO(c);

        loading = new EdoProgressDialog(this, R.string.contact, R.string.loading);
        edoUI = new EdoUI(c);

        Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            if (extras.containsKey(DBConstants.CONTAINER)) {
                Serializable contactSerial = extras.getSerializable(DBConstants.CONTAINER);
                if (contactSerial != null) {
                    contactToEdit = (LContact) contactSerial;
                }
            }
        }


        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));
    }

    public abstract Frag_ContactEditor getFragContactEditor();


    public void saveContact(){
        if(getFragContactEditor()==null)
            return;

        String name = getFragContactEditor().getName();
        String email = getFragContactEditor().getEmailAddress();
        if(JavaUtils.isNotEmpty(email)){
            if(JavaUtils.isEmailAddress(email)){
                if(!email.equalsIgnoreCase(app.getUserEmail())){

                    if(contactToEdit==null) {

                        final LContact oldContact = contactDAO.getLDAO().getByEmail(email);

                        if (oldContact == null) {

                            LContact contact = new LContact(name, null, email);
                            finishWithResult(contact, true);

                        } else {
                            if (oldContact != null) {
                                final EdoDialog dialog = new EdoDialog(c);
                                dialog.setTitle(R.string.alreadyInUse);
                                dialog.setText(c.getResources().getString(R.string.emailAddressAlreadyInUseText).replace(Constants.PH_CONTACTNAME, oldContact.getNameSurname()));
                                dialog.setNegativeButtonText(R.string.edit);
                                dialog.setOnNegativeClickListener(new EdoDialog.OnNegativeClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent editContact = new Intent(c, Act_ContactEditor.class);
                                        editContact.putExtra(DBConstants.CONTAINER, oldContact);
                                        dialog.cancel();
                                        finish();
                                        startActivity(editContact);

                                    }
                                });
                                dialog.setPositiveButtonText(R.string.add);
                                dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                        finishWithResult(oldContact, true);
                                    }
                                });
                                dialog.show();
                            }
                        }

                    }else{

                        contactToEdit.setName(name);
                        contactToEdit.setSurname(null);

                        finishWithResult(contactToEdit, false);

                    }
                    
                }else{
                    edoUI.showToastLong(c.getResources().getString(R.string.youCantAddYourEmailAddress), null);
                }
            }else{
                edoUI.showToastLong(c.getResources().getString(R.string.wrongEmailAddress), null);
            }

        }else{
            edoUI.showToastLong(c.getResources().getString(R.string.pleaseAddAnEmailAddress), null);
        }
        
    }


    public void finishWithResult(final LContact contact, final boolean init){
        boolean isInternetOn = !EdoUI.dialogNoInternet((Activity) c,
                new Runnable() {public void run() {
                    finishWithResult(contact, init);
                }},
                null);

        if(isInternetOn) {

            if (contact != null) {

                if(init){

                    if(!contact.isActive() && !contact.isHidden() && !contact.isGroup()) {
                        final EdoDialog dialog = new EdoDialog(c);

                        dialog.setTitle(contact.getPrincipalEmailAddress());
                        dialog.setText(c.getResources().getString(R.string.startSharingWithContactName).replace(Constants.PH_CONTACTNAME, contact.getNameSurname()));
                        dialog.setPositiveButtonText(R.string.letsStart);
//                        String name = contact.getNameSurname() + "\n" + (contact.getPrincipalEmailAddress() != null ? contact.getPrincipalEmailAddress() : "");
//                        if(contact.isGroup()){
//                            name = contact.getName();
//                        }
//
//                        dialog.setText(name);

                        dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {
                            @Override
                            public void onClick(View v) {
                                initContact(contact);

                                app.sendEventTrack(TrackConstants.EVENT_START_SHARING, TrackConstants.KEY_CONTEXT, contact.isGroup()? TrackConstants.VALUE_CONTEXT_GROUP:TrackConstants.VALUE_CONTEXT_CONTACTS);

                                dialog.dismiss();


                            }
                        });

                        dialog.show();

                    }else{
                        initContact(contact);

                    }

                }else{
                    updateContact(contact);
                }

            }
        }
    }


    public void initContact(final LContact contact){

        new Thread() {
            @Override
            public void run() {
                Looper.myLooper();

                runOnUiThread(new Runnable() {
                    public void run() {
                        loading.setTitle(contact.getNameSurname());
                        loading.setSubTitle(R.string.loading);
                        loading.show();
                    }
                });

                LContact newEdoContact = new SpaceUIDAO(c).initContact(contact);

                if(newEdoContact!=null) {

                    newEdoContact.setmDate(System.currentTimeMillis());

                    Intent extra = new Intent();
                    extra.putExtra(DBConstants.CONTAINER, newEdoContact);
                    setResult(RESULT_OK, extra);
                    Act_ContactEditorBase.this.finish();

                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            edoUI.showToastLong(c.getResources().getString(R.string.errorRetry), null);
                        }
                    });

                }

                loading.dismiss();

            }
        }.start();

    }


    public void updateContact(final LContact contact){
        new Thread() {
            @Override
            public void run() {
                Looper.myLooper();

                runOnUiThread(new Runnable() {
                    public void run() {
                        loading.setTitle(contact.getNameSurname());
                        loading.setSubTitle(R.string.loading);
                        loading.show();
                    }
                });



                if(contactDAO.update(contact)) {

                    contact.setmDate(System.currentTimeMillis());

                    Intent extra = new Intent();
                    extra.putExtra(DBConstants.CONTAINER, contact);
                    setResult(RESULT_OK, extra);
                    Act_ContactEditorBase.this.finish();

                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            edoUI.showToastLong(c.getResources().getString(R.string.errorRetry), null);
                        }
                    });

                }

                loading.dismiss();

            }
        }.start();

    }







    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };
}
