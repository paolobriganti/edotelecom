package io.edo.edomobile;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paolobriganti.android.AUtils;

import java.io.Serializable;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.ui.EdoNotification;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.FilePickers;
import io.edo.utilities.TrackConstants;
import io.edo.views.TabPageIndicator;

/**
 * A fragment representing a single Contact detail screen.
 * This fragment is either contained in a {@link Act_ContactActiveList}
 * in two-pane mode (on tablets) or a {@link Act_ContactDetail}
 * on handsets.
 */
public class Frag_ContactDetail extends Fragment implements ViewPager.OnPageChangeListener{
    Activity c;
    ThisApp app;

    private LContact contact;

    public FilePickers filesPicker;


    public EdoProgressDialog loading;

    ViewPager mViewPager;
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;

    TabPageIndicator pageIndicator;

    public static final String EXTRA_SECTION = "detailExtraSection";
    public static final int SECTION_TIMELINE = 0;
    public static final int SECTION_ORGANIZER = 1;
    public static final int SECTION_NOTES = 2;


    public Frag_ContactDetail() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AUtils.sendPushNotificationHeartBeat(getActivity());

        c = getActivity();
        app = ThisApp.getInstance(c);

        if (getArguments().containsKey(DBConstants.CONTAINER)) {
            Serializable contactSerial = getArguments().getSerializable(DBConstants.CONTAINER);
            if(contactSerial!=null) {
                contact = (LContact) contactSerial;
            }else{
                contact = app.getUserContact();
            }
        }else{
            contact = app.getUserContact();
        }

        loading = new EdoProgressDialog(c, R.string.loading, R.string.loading);

        if(contact.isPersonal(c)){
            c.getActionBar().setTitle(R.string.you);
        }else
            c.getActionBar().setTitle(contact.getName());

        setActionBarTitle(SECTION_TIMELINE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_detail, container, false);

        int sectionsSection = Frag_ContactOrganizer.SECTION_SPACES;
        if (getArguments().containsKey(Frag_ContactOrganizer.EXTRA_SECTION)) {
            sectionsSection = getArguments().getInt(Frag_ContactOrganizer.EXTRA_SECTION, Frag_ContactOrganizer.SECTION_SPACES);
        }

        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(c, getChildFragmentManager(), getArguments(), filesPicker, sectionsSection);

        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
//        mViewPager.addOnPageChangeListener(this);
        mViewPager.setOffscreenPageLimit(2);


        pageIndicator = (TabPageIndicator) rootView.findViewById(R.id.pageIndicator);
        pageIndicator.setViewPager(mViewPager);
        pageIndicator.setOnPageChangeListener(this);

        if (getArguments().containsKey(EXTRA_SECTION)) {
            mViewPager.setCurrentItem(getArguments().getInt(EXTRA_SECTION, SECTION_TIMELINE));
        }

        return rootView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setActionBarTitle(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public void setActionBarTitle(int position){
        switch (position) {
            case SECTION_ORGANIZER:
                app.setCurrentTimelineContainerId(null);

                if(contact.isPersonal(c)){
//                    String title = VU.getColoredTextString(c, R.color.lime, VU.getBoldTextString(c.getResources().getString(R.string.mySpaces)));
//                    c.getActionBar().setTitle(Html.fromHtml(title));

                    if(!app.spacesSeen) {
                        app.spacesSeen = true;
                        app.sendEventTrack(TrackConstants.EVENT_SPACES, TrackConstants.KEY_CONTEXT, TrackConstants.VALUE_CONTEXT_PERSONAL);
                    }
                }else{
//                    String title = contact.getNameComplete() + VU.getColoredTextString(c, R.color.lime, VU.getBoldTextString(" | " + c.getResources().getString(R.string.Spaces)));
//                    c.getActionBar().setTitle(Html.fromHtml(title));

                    if (!app.spacesSeen) {
                        app.spacesSeen = true;
                        app.sendEventTrack(TrackConstants.EVENT_SPACES, TrackConstants.KEY_CONTEXT, contact.isGroup() ? TrackConstants.VALUE_CONTEXT_GROUP : TrackConstants.VALUE_CONTEXT_CONTACTS);
                    }
                }
                break;
            case SECTION_NOTES:
                app.setCurrentTimelineContainerId(null);

                if(contact.isPersonal(c)){
//                    String title = VU.getColoredTextString(c, R.color.lime, VU.getBoldTextString(c.getResources().getString(R.string.myNotes)));
//                    c.getActionBar().setTitle(Html.fromHtml(title));

                    if(!app.notesSeen) {
                        app.notesSeen = true;
                        app.sendEventTrack(TrackConstants.EVENT_NOTES, TrackConstants.KEY_CONTEXT, TrackConstants.VALUE_CONTEXT_PERSONAL);
                    }
                }else{
//                    String title = contact.getNameComplete() + VU.getColoredTextString(c, R.color.lime, VU.getBoldTextString(" | " + c.getResources().getString(R.string.Notes)));
//                    c.getActionBar().setTitle(Html.fromHtml(title));


                    if (!app.notesSeen) {
                        app.notesSeen = true;
                        app.sendEventTrack(TrackConstants.EVENT_NOTES, TrackConstants.KEY_CONTEXT, contact.isGroup() ? TrackConstants.VALUE_CONTEXT_GROUP : TrackConstants.VALUE_CONTEXT_CONTACTS);
                    }
                }
                break;

            default:
                app.setCurrentTimelineContainerId(contact);

                if(contact.isPersonal(c)) {
//                    String title = VU.getColoredTextString(c, R.color.lime, VU.getBoldTextString(c.getResources().getString(R.string.myTimeline)));
//                    c.getActionBar().setTitle(Html.fromHtml(title));

                    if (!app.timelineSeen) {
                        app.timelineSeen = true;
                        app.sendEventTrack(TrackConstants.EVENT_TIMELINE, TrackConstants.KEY_CONTEXT, TrackConstants.VALUE_CONTEXT_PERSONAL);
                    }
                }else{
//                    String title = contact.getNameComplete() + VU.getColoredTextString(c, R.color.lime, VU.getBoldTextString(" | " + c.getResources().getString(R.string.Timeline)));
//                    c.getActionBar().setTitle(Html.fromHtml(title));


                    if (!app.timelineSeen) {
                        app.timelineSeen = true;
                        app.sendEventTrack(TrackConstants.EVENT_TIMELINE, TrackConstants.KEY_CONTEXT, contact.isGroup() ? TrackConstants.VALUE_CONTEXT_GROUP : TrackConstants.VALUE_CONTEXT_CONTACTS);
                    }
                }

                break;
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {
//        public LContact contact;
        Context c;
        int sectionsSection = Frag_ContactOrganizer.SECTION_SPACES;
        FilePickers filesPicker;
        Bundle arguments;

        public AppSectionsPagerAdapter(Context c,FragmentManager fm, Bundle arguments, FilePickers filesPicker, int sectionsSection) {
            super(fm);
//            this.contact = contact;
            this.c = c;
            this.arguments = arguments;
            this.filesPicker = filesPicker;
            this.sectionsSection = sectionsSection;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case SECTION_ORGANIZER:
                    Bundle arguments2 = new Bundle();
                    arguments2.putAll(arguments);
                    arguments2.putInt(Frag_ContactOrganizer.EXTRA_SECTION, sectionsSection);
                    final Frag_ContactOrganizer fragment2 = new Frag_ContactOrganizer();
                    fragment2.setArguments(arguments2);
                    fragment2.setFilesPicker(filesPicker);

                    return fragment2;

                case SECTION_NOTES:
                    Bundle arguments3 = new Bundle();
                    arguments3.putAll(arguments);
                    final Frag_ContactSnippets fragment3 = new Frag_ContactSnippets();
                    fragment3.setArguments(arguments3);
                    fragment3.setFilesPicker(filesPicker);

                    return fragment3;

                default:
                    Bundle arguments1 = new Bundle();
                    arguments1.putAll(arguments);
                    final Frag_ContactTimeline fragment = new Frag_ContactTimeline();
                    fragment.setArguments(arguments1);
                    fragment.setFilesPicker(filesPicker);

                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case SECTION_ORGANIZER:
                    return c.getResources().getString(R.string.Spaces);
                case SECTION_NOTES:
                    return c.getResources().getString(R.string.Notes);
                default:
                    return c.getResources().getString(R.string.Timeline);
            }
        }
    }






    @Override
    public void onResume() {
        super.onResume();
        if(mViewPager!=null && mViewPager.getCurrentItem()==SECTION_TIMELINE)
            app.setCurrentTimelineContainerId(contact);

        EdoNotification.cancelNotification(c, EdoNotification.ID_CONTACTS);

        AUtils.sendPushNotificationHeartBeat(c);
    }

    @Override
    public void onPause() {
        super.onPause();
        app.setCurrentTimelineContainerId(null);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        app.setCurrentTimelineContainerId(null);
    }

    public void setFilesPicker(FilePickers filesPicker){
        this.filesPicker = filesPicker;
    }





    public boolean onBackPressed(){
        if (mViewPager.getCurrentItem() > 0) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
            return true;
        }
        return false;
    }

}
