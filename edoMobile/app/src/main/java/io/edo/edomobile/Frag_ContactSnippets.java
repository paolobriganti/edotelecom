package io.edo.edomobile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.ArrayList;

import io.edo.db.global.beans.ContactFileDAO;
import io.edo.db.global.uibeans.ContactFileUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.EdoMessageBuilder;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoIntentManager;
import io.edo.ui.EdoUI;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.files.SnippetsAdapter;
import io.edo.utilities.TrackConstants;

public class Frag_ContactSnippets extends Frag_Base
        implements SnippetsAdapter.SnippetClickListener, SnippetsAdapter.SnippetLongClickListener, UIManager.FilesUpdatesListener{

    private final static String LISTENER_ID = Frag_ContactSnippets.class.getName();

    private ArrayList<LFile> snippets = new ArrayList<LFile>();
    private SnippetsAdapter mAdapter;
    private StaggeredGridLayoutManager lm;

    private ContactFileUIDAO contactFileUIDAO;



    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Frag_ContactSnippets() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contactFileUIDAO = new ContactFileUIDAO(c);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        String message = EdoMessageBuilder.getMessageBlancSheetOfNotes(c, contact);
        tv_blankSheet.setText(Html.fromHtml(message));
        if(contact.isPersonal(c)){
            iv_blankSheet.setImageResource(R.drawable.blank_personal_notes);
        }else if(contact.isGroup()){
            iv_blankSheet.setImageResource(R.drawable.blank_group_notes);
        }else{
            iv_blankSheet.setImageResource(R.drawable.blank_contact_notes);
        }


        lm = new StaggeredGridLayoutManager(EdoUI.getFilesColumnsNumber(c)-1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(lm);


        mAdapter = new SnippetsAdapter(getActivity(), contact, snippets);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setSnippetClickListener(this);
        mAdapter.setLongSnippetClickListener(this);


        loadSnippets();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        app.uiManager.addFilesUpdatesListener(LISTENER_ID, this);
    }

    @Override
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public ImageLoader getImageLoader() {
        return app.thumbnailFileLoader;
    }


    public void onFabButtonClick(View v) {
        filesPicker.setSpace(getTimelineSpace());
        filesPicker.snippetEditor(getCurrentContainer(), getTimelineSpace(), null);
        app.sendEventTrack(TrackConstants.EVENT_ADD_NOTE_ON_NOTES_CLICK, TrackConstants.KEY_CONTEXT, contact.isPersonal(c) ? TrackConstants.VALUE_CONTEXT_PERSONAL : (contact.isGroup() ? TrackConstants.VALUE_CONTEXT_GROUP : TrackConstants.VALUE_CONTEXT_CONTACTS));
    }
    @Override
    public int getContentViewResourceId() {
        return R.layout.fragment_contact_snippets;
    }

    @Override
    public boolean canAddFirstSpace() {
        return false;
    }

    @Override
    public boolean canHideFooterOnScroll() {
        return true;
    }

    @Override
    public LSpace getCurrentSpace() {
        return getTimelineSpace();
    }


    //*******************************
    //* Snippets
    //*******************************
    public void loadSnippets(){
        new SnippetsTask(false).execute();
    }

    public void refreshSnippets(){
        if(app.isUpdateInProgress) {
            srlayout.setRefreshing(false);
            return;
        }
        new SnippetsTask(true).execute();
    }

    class SnippetsTask extends AsyncTask<String, String, ArrayList<LFile>> {
        boolean isRefreshing = false;

        public SnippetsTask(boolean isRefreshing) {
            this.isRefreshing = isRefreshing;
        }

        @Override
        protected void onPreExecute() {
            app.isUpdateInProgress = true;

            if(isRefreshing){
                srlayout.setRefreshing(true);
            }
            super.onPreExecute();
        }

        @Override
        protected ArrayList<LFile> doInBackground(String... params) {
            EdoContainer container = getCurrentContainer();
            if(container!=null) {

                if(isRefreshing){
                    LFile[] files = new ContactFileDAO(c).downloadAllFileOf((LContact) getCurrentContainer(),null);
                    if(files==null || files.length<=0){
                        return null;
                    }
                }

                if(getTimelineSpace()==null) {
                    onNoTimelineSpaceFound();
                }

                if(container.isContactOrGroup()){
                    return contactFileUIDAO.getLDAO().getListByTipeOf(contact,LFile.FILE_TYPE_SNIPPET);
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(final ArrayList<LFile> result) {
            app.isUpdateInProgress = false;
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    mAdapter.clearSnippets();

                    if (result != null && result.size()>0) {
                        mAdapter.addSnippets(result);
                        showList(true);
                    }else{
                        showList(false);
                    }
                    mProgressBar.setVisibility(View.GONE);

                    if(isRefreshing){
                        srlayout.setRefreshing(false);
                    }
                }
            });
            this.cancel(true);
        }
    }

    public void addSnippetToList(LFile snippet){
        showList(true);

        if(!snippets.contains(snippet)) {
            mAdapter.addSnippet(0,snippet);
        }
    }

    public void updateSnippetOnList(LFile snippet){
        mAdapter.updateSnippet(snippet);
    }

    public void updateSnippetOnList(int index, LFile snippet){
        mAdapter.updateSnippet(index, snippet);
    }

    public void removeSnippetFromList(LFile snippet){
        mAdapter.removeSnippet(snippet);
    }


    @Override
    public void onClick(SnippetsAdapter.ViewHolder v, LFile snippet) {
        if (!snippet.isSelected() && !isSelOptionsActionModeEnabled) {
            c.startActivity(EdoIntentManager.getSnippetIntent(c, getCurrentContainer(), getTimelineSpace(), snippet, null));
        }else{
            addSelectedFile(v,snippet);
        }
    }
    @Override
    public void onLongClick(SnippetsAdapter.ViewHolder v, LFile snippet) {
        addSelectedFile(v,snippet);
    }

    public void addSelectedFile(SnippetsAdapter.ViewHolder vh, LFile file){
        startSelOptionsActionMode();
        if(!selFiles.contains(file)){
            selFiles.add(file);
            file.setIsSelected(true);
            vh.setSelected(true);
            setSelOptionsActionModeTitle("" + selFiles.size());
        }else{
            selFiles.remove(file);
            file.setIsSelected(false);
            vh.setSelected(false);
            setSelOptionsActionModeTitle("" + selFiles.size());
            if(selFiles.size()==0){
                stopSelOptionsActionMode();
            }
        }
    }

    @Override
    public void onSelectionFinished() {
        super.onSelectionFinished();

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (LFile file : snippets) {
                    file.setIsSelected(false);
                }
                mAdapter.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onRefresh() {
        refreshSnippets();
    }










    //*******************************
    //* UPDATES
    //*******************************
    @Override
    public void onInsert(final EdoContainer container, final LFile file) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file.isSnippet() && container != null &&
                        container.equals(getCurrentContainer()) &&
                        !snippets.contains(file)) {


                    addSnippetToList(file);


                }

            }
        });
    }

    @Override
    public void onUpdate(final EdoContainer container, final LFile file) {
        if(file==null)
            return;

        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file.isSnippet() && container != null &&
                        container.equals(getCurrentContainer()))
                    updateSnippetOnList(file);
            }
        });
    }

    @Override
    public void onDelete(final EdoContainer container, final LFile file) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file!=null)
                    removeSnippetFromList(file);
            }
        });
    }

    @Override
    public void onSyncList(LActivity a) {
        if(app.isUpdateInProgress)
            return;

        if(a.hasBeenProcessed())
            loadSnippets();
    }

    @Override
    public void onUploadToCloud(EdoContainer container, LFile file, int progressStatus, int progressPercent) {
        if(app.isUpdateInProgress)
            return;


    }

    @Override
    public void onDownloadFromCloud(EdoContainer container, LFile file, int progressStatus, int progressPercent) {
        if(app.isUpdateInProgress)
            return;


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        app.uiManager.removeListener(LISTENER_ID);
    }
}
