package io.edo.edomobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import io.edo.utilities.eLog;
public class Receiver_OnUpgrade extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {


		if (intent == null)
			return;
		if (context == null)
			return;

		String action = intent.getAction();
		if (action == null)
			return;

//		if (action.equals(Intent.ACTION_PACKAGE_REPLACED)) {
//			Bundle bundle = intent.getExtras();
//			if(bundle!=null && bundle.containsKey(Intent.EXTRA_UID)) {
//				int uid = bundle.getInt(Intent.EXTRA_UID);
//
//				String[] packages = context.getPackageManager().getPackagesForUid(uid);
//				for (String pack : packages) {
//					try { 
//						if(pack.equals(context.getPackageName())){
//							PackageInfo packageInfo = context.getPackageManager()
//									.getPackageInfo(context.getPackageName(), 0);
//							upgradeToVersion(context, packageInfo.versionCode);
//							break;
//						}
//					} catch (NameNotFoundException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//
//				}
//			}
//		}


		if (action.equals(Intent.ACTION_MY_PACKAGE_REPLACED)) {
			try { 
				PackageInfo packageInfo = context.getPackageManager()
						.getPackageInfo(context.getPackageName(), 0);
				upgradeToVersion(context, packageInfo.versionCode);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		

	}


	public void upgradeToVersion(final Context c, final int newVersionCode){
		eLog.i("edoMobile", "edo Mobile has been updated to version "+newVersionCode);
		
	}

}


