package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paolobriganti.android.ASI;
import com.paolobriganti.android.AUI;
import com.paolobriganti.android.VU;
import com.paolobriganti.utils.JavaUtils;

import java.io.Serializable;

import io.edo.api.ApiInfo;
import io.edo.db.local.LocalDBManager;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;
import io.edo.views.EdoProgressBar;

public class Act_Dialog_Service extends Activity {
	public ThisApp app;
	
	Activity c;
	EdoUI edoUI;
	LService service;
	LocalDBManager db;

	ImageView iv_icon;
	TextView tv_cloudName;
	TextView tv_cloudEmail;
	EdoProgressBar progressBar;
	TextView tv_space;
	Button bt_positive;



	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		AUI.setStatusBarColor(this, this.getResources().getColor(R.color.edo_blue_dark));
		EdoUI.setOrientationTo(this);
		
		
		c = this;
		app = ThisApp.getInstance(this);
		db = app.getDB();	
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		overridePendingTransition(0,0);
		setContentView(R.layout.dialog_edo_service);
		
//		WindowManager.LayoutParams params = getWindow().getAttributes();  
//		params.width = this.getResources().getDisplayMetrics().widthPixels;
//		params.height = this.getResources().getDisplayMetrics().heightPixels;
//		this.getWindow().setAttributes(params); 

        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			Serializable serviceSerial = extras.getSerializable(DBConstants.SERVICE);
			if(serviceSerial!=null){
				service = (LService) serviceSerial;
			}else{
				edoUI.showToastLong(c.getResources().getString(R.string.noCloudServiceInfo), null);
				Act_Dialog_Service.this.finish();
			}
		}else{
			edoUI.showToastLong(c.getResources().getString(R.string.noCloudServiceInfo), null);
			Act_Dialog_Service.this.finish();
		}

		if(service!=null){
			
			iv_icon = (ImageView) this.findViewById(R.id.iv_icon);
			tv_cloudName = (TextView) this.findViewById(R.id.tv_cloudName);
			tv_cloudEmail = (TextView) this.findViewById(R.id.tv_cloudEmail);
			progressBar = (EdoProgressBar) this.findViewById(R.id.progressBar);
			tv_space = (TextView) this.findViewById(R.id.tv_space);
			bt_positive = (Button) this.findViewById(R.id.bt_positive);
			
			iv_icon.setImageResource(service.getCloudIcon(c));
			tv_cloudName.setText(service.getCommercialCloudName(c));
			tv_cloudEmail.setText(service.getEmail());
			bt_positive.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Act_Dialog_Service.this.finish();
				}
			});
			new Thread(){@Override public void run(){Looper.myLooper();
			try{
				if(ASI.isInternetOn(c)){
					ApiInfo apiInfo = new ApiInfo(c, db, service);
					if(apiInfo!=null){
						Long freeBytes = apiInfo.getFreeBytes();
						Long totalBytes = apiInfo.getTotalBytes();
						String freeSpace = apiInfo.getBytesForView(freeBytes);
						String totalSpace = apiInfo.getBytesForView(totalBytes);
						final String cInfo = freeSpace+" "+c.getResources().getString(R.string.freeOf)+" "+totalSpace;
						final int progress = JavaUtils.getIntegerValueOfDouble(apiInfo.getOccupiedBytes()/1000000, 0);
						final int max = JavaUtils.getIntegerValueOfDouble(totalBytes/1000000, 0);
						c.runOnUiThread(new Runnable() {public void run() {
							tv_space.setText(cInfo);
							progressBar.setMax(max);
							progressBar.setProgress(progress);
							VU.setVisibilityWithAnimationToView(c, progressBar, ProgressBar.VISIBLE, R.anim.appear, (long)1000);
							VU.setVisibilityWithAnimationToView(c, tv_space, TextView.VISIBLE, R.anim.appear, (long)1000);
						}});
					}
				}
			}catch(NullPointerException e){}
			}}.start();
			
			
			
		}
	}


	@Override
	public void onResume() {
		super.onResume();

	}


	@Override
	public void onPause() {
		super.onPause();
	}

	
	
	
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mKillReceiver);
	}
	private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, final Intent intent) {
			finish();
		}
	};
}