package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.paolobriganti.android.AUI;

import io.edo.db.global.uibeans.ActivityUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.LActivities;
import io.edo.ui.EdoIntentManager;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.FilePickers;
import io.edo.ui.OnProgressChangeListener;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 05/07/15.
 */
public class Act_ContactBase extends Act_SearchFileBase {


    Activity c;
    ThisApp app;

    public LContact contact;


    public FilePickers filesPicker;

    boolean isContactDetailActivity = false;

    Frag_ContactDetail frag_ContactDetail;

    public EdoProgressDialog loading;


    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    public boolean mTwoPane;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        c = this;
        app = ThisApp.getInstance(this);

        loading = new EdoProgressDialog(c,R.string.loading,R.string.loading);

        filesPicker = new FilePickers(c);

        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));
    }

    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		FRAGMENTS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public void setContactDetailFragment(int detailSection, int organizeSection){
        if(contact==null || (!isContactDetailActivity && !mTwoPane))
            return;

        if(frag_ContactDetail!=null){
            getSupportFragmentManager().beginTransaction().remove(frag_ContactDetail).commit();
        }

        Bundle arguments = new Bundle();
        arguments.putSerializable(DBConstants.CONTAINER, contact);

        if(getIntent().getExtras()!=null && getIntent().getExtras().containsKey(DBConstants.SPACE)){
            arguments.putInt(Frag_ContactDetail.EXTRA_SECTION, Frag_ContactDetail.SECTION_ORGANIZER);
            arguments.putInt(Frag_ContactOrganizer.EXTRA_SECTION, Frag_ContactOrganizer.SECTION_SPACES);
            arguments.putSerializable(DBConstants.SPACE, getIntent().getExtras().getSerializable(DBConstants.SPACE));
        }else{
            arguments.putInt(Frag_ContactDetail.EXTRA_SECTION, detailSection);
            arguments.putInt(Frag_ContactOrganizer.EXTRA_SECTION, organizeSection);
        }

        frag_ContactDetail = new Frag_ContactDetail();
        frag_ContactDetail.setArguments(arguments);
        frag_ContactDetail.setFilesPicker(filesPicker);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.contact_detail_container, frag_ContactDetail)
                .commit();
    }

    public void onRefreshMenuItemClick(View v) {
        new ActivitiesTask().execute();
    }

    class ActivitiesTask extends AsyncTask<String, String, LActivities> {


        @Override
        protected void onPreExecute() {
            app.isUpdateInProgress = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loading.setTitle(R.string.syncData);
                    loading.setSubTitle(R.string.loading);
                    loading.show();
                }
            });
        }

        @Override
        protected LActivities doInBackground(String... params) {
                new ActivityUIDAO(c).downloadAll(new OnProgressChangeListener() {
                    @Override
                    public void onProgressChange(final int current, final int total, Object object, String info) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loading.setBarProgress(current, total);
                            }
                        });
                    }
                });
            return null;
        }


        @Override
        protected void onPostExecute(final LActivities result) {
            app.isUpdateInProgress = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loading.dismiss();
                }
            });
        }
    }

    public void onSearchMenuItemClick(View v) {
        c.startActivity(new Intent(c, Act_SearchResults.class));
    }

    public void onTimelineMenuItemClick(View v) {

        setContactDetailFragment(Frag_ContactDetail.SECTION_TIMELINE, Frag_ContactOrganizer.SECTION_SPACES);
    }

    public void onSpacesMenuItemClick(View v) {

        setContactDetailFragment(Frag_ContactDetail.SECTION_ORGANIZER, Frag_ContactOrganizer.SECTION_SPACES);
    }

    public void onNotesMenuItemClick(View v) {

        setContactDetailFragment(Frag_ContactDetail.SECTION_NOTES, Frag_ContactOrganizer.SECTION_NOTES);
    }

    public void onInboxMenuItemClick(View v) {
        c.startActivity(EdoIntentManager.getContactFileFilter(c,contact));
    }

    public void onInboxExit() {
        onTimelineMenuItemClick(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        filesPicker.onActivityResult(requestCode,resultCode,intent);

    }












    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        if (isContactDetailActivity || mTwoPane) {
            menu.findItem(R.id.settings).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            menu.findItem(R.id.settings).setVisible(mTwoPane?true:false);
            menu.findItem(R.id.refresh).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

            if(!mTwoPane) {
                menu.findItem(R.id.search).setVisible(false);
                MenuItem searchItem = menu.findItem(R.id.searchFiles);
                searchItem.setVisible(true);
                SearchView searchView = (SearchView) searchItem.getActionView();
                setupSearchView(searchView, searchItem);
            }else{
                menu.findItem(R.id.search).setVisible(true);
                menu.findItem(R.id.searchFiles).setVisible(false);
            }

        }else{
            menu.findItem(R.id.inbox).setVisible(false);
            menu.findItem(R.id.search).setVisible(true);
            menu.findItem(R.id.searchFiles).setVisible(false);
//            menu.findItem(R.id.timeline).setVisible(false);
//            menu.findItem(R.id.spaces).setVisible(false);
//            menu.findItem(R.id.notes).setVisible(false);
        }



        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.inbox:

                    onInboxMenuItemClick(null);

                break;

            case R.id.refresh:
                onRefreshMenuItemClick(null);

                break;

            case R.id.search:
                if(!isContactDetailActivity)
                    onSearchMenuItemClick(null);

                break;

//            case R.id.timeline:
//                onTimelineMenuItemClick(null);
//
//                break;
//
//            case R.id.spaces:
//                onSpacesMenuItemClick(null);
//
//                break;
//
//            case R.id.notes:
//                onNotesMenuItemClick(null);
//
//                break;

            case R.id.settings:
                settings(null);

                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }



    public void settings(View v){
        c.startActivity(EdoIntentManager.getSettingsIntent(c));
    }










    @Override
    protected void onDestroy() {
        super.onDestroy();

        try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };


    @Override
    public void onBackPressed() {

        if(frag_ContactDetail!=null && frag_ContactDetail.onBackPressed()){
            return;
        }

        super.onBackPressed();
        AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));
    }
}
