package io.edo.edomobile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoUI;
import io.edo.ui.FileOptions;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.filepage.FilePageHolder;
import io.edo.ui.imageutils.FilePageLoader;
import io.edo.views.TouchImageView;
import io.edo.views.ViewPagerPlus;
import io.edo.views.pagetransformers.CardTransformer;

public class Act_FilePage extends FragmentActivity implements
        ViewPager.OnPageChangeListener, UIManager.FilesUpdatesListener{

    FragmentActivity c;
    ThisApp app;

    private final static String LISTENER_ID = Act_FilePage.class.getName();

    private ViewPagerPlus mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;

    private TouchImageView iv_transition;

    private LinearLayout ll_footer;
    private Button bt_comments;
    private Button bt_star;
    private Button bt_openWith;

    private final int ACTION_OPEN_WITH = -1;
    private final int ACTION_DOWNLOAD = 11;
    private final int ACTION_STOP_DOWNLOAD = 12;
    private final int ACTION_UPLOAD = 21;
    private final int ACTION_STOP_UPLOAD = 22;


    private EdoContainer container;
    private LSpace space;

    private LFile currentFile;
    private FileUIDAO fileDAO;

    public FilePageLoader filePageLoader;

    FileOptions fileOptions;


    HashMap<String, FilePageHolder> filePageHolders = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_filepage);

        c = this;
        app = ThisApp.getInstance(c);

        // Show the Up button in the action bar.
        getActionBar().setDisplayHomeAsUpEnabled(true);

        fileDAO = new FileUIDAO(c);
        filePageLoader = FilePageLoader.getInstance(this);
        filePageLoader.setCropCenterImage(false);

        iv_transition = (TouchImageView) findViewById(R.id.imageView);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPagerPlus) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(this);
        mPager.setPageTransformer(true, new CardTransformer());

        ll_footer = (LinearLayout) findViewById(R.id.ll_footer);
        bt_comments = (Button) findViewById(R.id.bt_comments);
        bt_star = (Button) findViewById(R.id.bt_star);
        bt_openWith = (Button) findViewById(R.id.bt_openWith);


        processExtras(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        app.uiManager.addFilesUpdatesListener(LISTENER_ID, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        processExtras(intent);
    }

    public void processExtras(Intent intent){
        if (intent.getExtras()!=null && intent.getExtras().containsKey(DBConstants.CONTAINER)) {
            Serializable containerSerial = intent.getExtras().getSerializable(DBConstants.CONTAINER);
            if (containerSerial != null) {
                container = (EdoContainer) containerSerial;
            }
        }
        if (intent.getExtras()!=null && intent.getExtras().containsKey(DBConstants.SPACE)) {
            Serializable spaceSerial = intent.getExtras().getSerializable(DBConstants.SPACE);
            if (spaceSerial != null) {
                space = (LSpace) spaceSerial;
            }
        }
        if (intent.getExtras()!=null && intent.getExtras().containsKey(DBConstants.FILE)) {
            Serializable fileSerial = intent.getExtras().getSerializable(DBConstants.FILE);
            if(fileSerial!=null) {
                currentFile = (LFile) fileSerial;
            }
        }

        if(currentFile==null){
            this.finish();
            return;
        }else{
            getActionBar().setTitle(currentFile.getNameWithExt());
        }

        loadFiles();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        app.uiManager.removeListener(LISTENER_ID);
    }

    @Override
    public void onPageSelected(int position) {
        // When changing pages, reset the action bar actions since they are dependent
        // on which page is currently active. An alternative approach is to have each
        // fragment expose actions itself (rather than the activity exposing actions),
        // but for simplicity, the activity provides the actions in this sample.
        currentFile = getFile(position);
        setCurrentFileViews(currentFile);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            final Frag_FilePage fragment = new Frag_FilePage();

            LFile file = space.getFile(position);
            if(file!=null){
                Bundle arguments = new Bundle();
                arguments.putSerializable(DBConstants.FILE, file);
                fragment.setArguments(arguments);

                fragment.setFileClickListener(new Frag_FilePage.FileClickListener() {
                    @Override
                    public void onClick(FilePageHolder v, LFile file) {
                        showHideBars();
                    }
                });
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return getFilesCount();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        public void clearFiles() {
            int size = getCount();
            if (size > 0) {
                getFiles().clear();

                this.notifyDataSetChanged();
            }
        }
        public void addFiles(List<LFile> files) {
            getFiles().addAll(files);
            this.notifyDataSetChanged();
            setCurrentFileItem();
        }
        public void addFile(LFile file) {
            getFiles().add(0, file);
            this.notifyDataSetChanged();
            setCurrentFileItem();
        }

        public void updateFile(LFile file){
            int index = getFiles().indexOf(file);
            if(index>=0){
                getFiles().set(index, file);
            }
            if(file.getId().equals(currentFile.getId())){
                currentFile = file;
                setCurrentFileViews(currentFile);
            }
            this.notifyDataSetChanged();
        }

        public void deleteFile(LFile file){
            int index = getFiles().indexOf(file);

            if(index>=0){
                getFiles().remove(index);
                this.notifyDataSetChanged();
                setCurrentFileItem();
            }

            if(getFiles()==null || getFiles().size()==0){
                Act_FilePage.this.finish();
            }
        }


        public void setCurrentFileItem(){
            if(getCount()>0) {
                if (currentFile != null && getFiles().contains(currentFile)) {
                    int index = getFiles().indexOf(currentFile);
                    mPager.setCurrentItem(index);
                } else {
                    currentFile = getFiles().get(0);
                    mPager.setCurrentItem(0);
                }
            }
            setCurrentFileViews(currentFile);
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();

        }
    }




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		CURRENT FILE
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    public void setCurrentFileViews(LFile currentFile){
        if(currentFile!=null){
            getActionBar().setTitle(currentFile.getNameWithExt());





            int event = -1;
            int progressStatus = -1;

            if(currentFile.isStored() && !currentFile.isInCloud()){
                event = LActivity.EVENT_UPLOAD_TO_CLOUD;
                progressStatus = LActivity.STATUS_ERROR;

                FilePageHolder vh = filePageHolders.get(currentFile.getId());
                if (vh != null) {
                    vh.setProgress(progressStatus, 100, null);
                }

            }else if(!currentFile.isStored() && currentFile.isInCloud() && !currentFile.isOnlyMetaFile()){
                event = LActivity.EVENT_DOWNLOAD_FROM_CLOUD;
            }

            setOpenWithView(event, progressStatus);


            LActivity a = app.uiManager.getActivityByResourceId(currentFile.getId());
            if(a!=null) {
                if (a.isUploadEvent()) {
                    onUploadToCloud(container, currentFile, a.getStatus(), a.getProgressPercent());

                    if(a.hasBeenProcessed()){
                        app.uiManager.removeActivityByResourceId(currentFile.getId());
                    }

                } else if (a.isDownloadEvent()) {
                    onDownloadFromCloud(container, currentFile, a.getStatus(), a.getProgressPercent());

                    if(a.hasBeenProcessed()){
                        app.uiManager.removeActivityByResourceId(currentFile.getId());
                    }
                }
            }


        }
        setStarView();
        setCommentsView();
        invalidateOptionsMenu();



    }

    public void setOpenWithView(int event, int progressStatus){

        int iconRes = R.drawable.ic_action_openwith_w;
        int textRes = R.string.openWith;
        int action = ACTION_OPEN_WITH;

        if(event == LActivity.EVENT_UPLOAD_TO_CLOUD &&
                progressStatus!=LActivity.STATUS_COMPLETE){
            iconRes = R.drawable.ic_action_arrow_up_w;
            textRes = R.string.upload;
            action = ACTION_UPLOAD;

        }else if(event == LActivity.EVENT_DOWNLOAD_FROM_CLOUD &&
                progressStatus!=LActivity.STATUS_COMPLETE){
            iconRes = R.drawable.ic_action_download_w;
            textRes = R.string.download;
            action = ACTION_DOWNLOAD;

        }




        if(progressStatus == LActivity.STATUS_PENDING ||
                progressStatus == LActivity.STATUS_STARTED ||
                progressStatus == LActivity.STATUS_IN_PROGRESS) {

            iconRes = R.drawable.ic_action_stop;
            textRes = R.string.stop;

            if(event == LActivity.EVENT_UPLOAD_TO_CLOUD){
                action = ACTION_STOP_UPLOAD;
            }else if(event == LActivity.EVENT_DOWNLOAD_FROM_CLOUD){
                action = ACTION_STOP_DOWNLOAD;
            }

        }else if(progressStatus == LActivity.STATUS_ERROR){

            iconRes = R.drawable.ic_action_sync;
            textRes = R.string.retry;

            if(event == LActivity.EVENT_UPLOAD_TO_CLOUD){
                action = ACTION_UPLOAD;
            }else if(event == LActivity.EVENT_DOWNLOAD_FROM_CLOUD){
                action = ACTION_DOWNLOAD;
            }

        }

        bt_openWith.setCompoundDrawablesWithIntrinsicBounds(c.getResources().getDrawable(iconRes),null,null,null);
        bt_openWith.setText(textRes);
        bt_openWith.setTag(action);
    }

    public void setStarView(){
        int iconRes = currentFile.isStarred()?R.drawable.ic_action_favorite_actived:R.drawable.ic_action_favorite_w;
        bt_star.setCompoundDrawablesWithIntrinsicBounds(c.getResources().getDrawable(iconRes), null, null, null);
    }

    public void setCommentsView(){
        String text = c.getResources().getString(R.string.Comments);

        int commentCount = currentFile.getCommentsCount();
        if(commentCount>0){
            text += " ("+commentCount+")";
        }

        bt_comments.setText(text);
    }





    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		OPTIONS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    public void share(View v){
        fileOptions.shareWith(currentFile);
    }

    public void forward(View v){
        fileOptions.forwardShare(currentFile);
    }

    public void info(View v){
        fileOptions.infoDialog(space, currentFile);
    }

    public void store(View v){
        if(!currentFile.isStored()) {
            download(null);

            invalidateOptionsMenu();
        }else{
            fileOptions.removeFileFromLocal(currentFile, new FileOptions.LocalFileRemovedListener() {
                @Override
                public void onFileRemoved() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            currentFile.setLocalUri(null);
                            onUpdate(container,currentFile);
                        }
                    });
                }
            });
        }

    }

    public void rename(View v){
        fileOptions.renameFileDialog(space, currentFile);
    }

    public void delete(View v){
        fileOptions.deleteFileDialog(space, currentFile);
    }

    public void comments(View v){
        if(fileOptions!=null)
        fileOptions.showComments(space, currentFile);
    }

    public void star(View v){
        fileOptions.star(currentFile);
    }

    public void openWith(View v){
        Object tag = bt_openWith.getTag();

        if(tag!=null && tag instanceof Integer){
            int action = (int) tag;
            switch (action){
                case ACTION_DOWNLOAD:
                    download(v);
                    return;
                case ACTION_STOP_DOWNLOAD:
                    stopDownload(v);
                    return;
                case ACTION_UPLOAD:
                    upload(v);
                    return;
                case ACTION_STOP_UPLOAD:
                    stopUpload(v);
                    return;
            }
        }
        fileOptions.openWith(container, space, currentFile);
    }

    public void download(View v){
        if (currentFile.isDownloadable())
            fileOptions.downloadFileAndCheckInternet(container, space, currentFile);
    }

    public void stopDownload(View v){
        if(currentFile.isDownloading(c)) {
            fileOptions.stopDownload(currentFile);
        }
    }

    public void upload(View v){
        if (currentFile.isUploadable())
            fileOptions.uploadFileAndCheckInternet(container, space, currentFile,null);
    }

    public void stopUpload(View v){
        if(currentFile.isUploading(c)) {
            fileOptions.stopUpload(currentFile);
        }
    }

    public void showHideBars(){
        showBars(ll_footer.getVisibility()!=View.VISIBLE);
    }

    public void showBars(boolean show){
        ll_footer.setVisibility(show ? View.VISIBLE : View.GONE);
        if(!show) {
            // Hide status bar
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getActionBar().hide();

            if(android.os.Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN) {
                View decorView = getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                decorView.setSystemUiVisibility(uiOptions);
            }
        }else {
            // Show status bar
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getActionBar().show();

            if(android.os.Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN) {
                View decorView = getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN  |
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
                decorView.setSystemUiVisibility(uiOptions);
            }
        }
    }

    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		UPDATES
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public void onInsert(EdoContainer container, final LFile file) {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file != null &&
                        file.getParentId().equals(space.getCurrentFolder().getId()) &&
                        !getFiles().contains(file)) {

                    mPagerAdapter.addFile(file);

                }

            }
        });
    }

    @Override
    public void onUpdate(EdoContainer container, final LFile file) {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file != null &&
                        file.getParentId().equals(space.getCurrentFolder().getId())) {

                    mPagerAdapter.updateFile(file);

                }

            }
        });
    }

    @Override
    public void onDelete(final EdoContainer container, final LFile file) {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file != null &&
                        file.getParentId().equals(space.getCurrentFolder().getId())) {

                    mPagerAdapter.deleteFile(file);

                }

            }
        });
    }

    @Override
    public void onSyncList(LActivity a) {
        if(a.hasBeenProcessed())
            loadFiles();
    }

    @Override
    public void onUploadToCloud(final EdoContainer container, final LFile file, final int progressStatus, final int progressPercent) {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file != null && space!=null &&
                        file.getParentId().equals(space.getCurrentFolder().getId())) {

                    FilePageHolder vh = filePageHolders.get(file.getId());
                    if (vh != null) {
                        String text = c.getResources().getString(R.string.upload).toUpperCase();
                        vh.setProgress(progressStatus, progressPercent,text);
                    }

                    if(currentFile!=null && currentFile.equals(file))
                        setOpenWithView(LActivity.EVENT_UPLOAD_TO_CLOUD, progressStatus);


                }
            }
        });
    }

    @Override
    public void onDownloadFromCloud(final EdoContainer container, final LFile file, final int progressStatus, final int progressPercent) {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {


                if (file != null && space!=null &&
                        file.getParentId().equals(space.getCurrentFolder().getId())) {

                    FilePageHolder vh = filePageHolders.get(file.getId());
                    if(vh!=null){
                        String text = c.getResources().getString(R.string.download).toUpperCase();
                        vh.setProgress(progressStatus,progressPercent,text);
                    }
                    if(currentFile!=null && currentFile.equals(file))
                        setOpenWithView(LActivity.EVENT_DOWNLOAD_FROM_CLOUD, progressStatus);

                }
            }
        });
    }


    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		EXTRAS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public void loadFiles()
    {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new FilesTask().execute();
            }
        });
    }



    class FilesTask extends AsyncTask<String, String, ArrayList<LFile>> {

        @Override
        protected void onPreExecute() {
            mPagerAdapter.clearFiles();
            super.onPreExecute();
        }

        @Override
        protected ArrayList<LFile> doInBackground(String... params) {
            if(currentFile==null)
                return null;

            currentFile = fileDAO.getLDAO().get(currentFile.getId());

            if(currentFile==null){
                c.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        EdoUI.showToast(c,R.string.thisFileHasBeenRemovedFromThisSpace,null, Toast.LENGTH_LONG);
                    }
                });
                Act_FilePage.this.finish();
                return null;
            }

            if(getCurrentContainer()==null) {
                container = currentFile.getContainer(c);
            }
            if(container!=null && space==null){
                space = currentFile.getSpace(c);
            }

            if(container!=null && space!=null) {
                return fileDAO.getLDAO().getSubFiles(space.getCurrentFolder(), -1, -1, -1, false);
            }

            return null;
        }


        @Override
        protected void onPostExecute(final ArrayList<LFile> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    if (result != null && result.size()>0) {
                        mPagerAdapter.addFiles(result);
                        mPager.setVisibility(View.VISIBLE);
                        iv_transition.setVisibility(View.GONE);
                        fileOptions = new FileOptions(c,container);
                    }else{
                        finish();
                    }

                }
            });
            this.cancel(true);
        }
    }

    public EdoContainer getCurrentContainer(){
        return container;
    }

    public void setFiles(ArrayList<LFile> files){
        space.setFilesOfCurrentFolder(files);
    }

    public ArrayList<LFile> getFiles(){
        if(space!=null){
            if(space.getFilesOfCurrentFolder()==null) {
                setFiles(new ArrayList<LFile>());
            }
            return space.getFilesOfCurrentFolder();
        }
        return null;
    }

    public LFile getFile(int position){
        if(getFiles()!=null && getFiles().size()>position)
            return getFiles().get(position);

        return null;
    }

    public int getFilesCount(){
        return getFiles()!=null?getFiles().size():0;
    }

    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filepage, menu);

        MenuItem item = menu.findItem(R.id.store);
        if(currentFile!=null){
            item.setChecked(currentFile.isStored()||currentFile.isDownloading(c));
            item.setVisible(currentFile.isDownloadable() && !currentFile.isDownloading(c));
        }




        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.share:
                share(null);
                break;

            case R.id.forward:
                forward(null);
                break;

            case R.id.info:
                info(null);
                break;

            case R.id.store:
                store(null);
                break;

            case R.id.rename:
                rename(null);
                break;

            case R.id.delete:
                delete(null);
                break;

            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }


}

