package io.edo.edomobile;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.paolobriganti.android.ASI;
import com.paolobriganti.android.AUtils;

import java.util.ArrayList;

import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.global.uibeans.GroupUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.web.webupdate.EdoGeneralUpdate;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialogListView;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoNotification;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.ListAdapterOptions;
import io.edo.ui.adapters.contacts.ListAdapterEdoContact;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.ui.interfaces.SwipeRefreshListFragment;
import io.edo.utilities.Constants;
import io.edo.utilities.TrackConstants;


/**
 * A list fragment representing a list of Contacts. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link Frag_ContactTimeline}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class Frag_ContactActiveList extends SwipeRefreshListFragment
        implements UIManager.ContactsUpdatesListener{
    FragmentActivity c;
    ThisApp app;

    private ContactPhotoLoader contactPhotoLoader;

    private EdoProgressDialog loading;

    private ContactUIDAO contactDAO;

    private final static String LISTENER_ID = Frag_ContactActiveList.class.getName();

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    boolean activateOnItemClick;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        void onItemSelected(LContact contact);
        void onOnViewsLoaded();
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(LContact contact) {
        }

        @Override
        public void onOnViewsLoaded() {

        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Frag_ContactActiveList() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        c = getActivity();
        app = ThisApp.getInstance(c);
        contactPhotoLoader = ContactPhotoLoader.getInstance(getActivity());



        loading = new EdoProgressDialog(c, R.string.loading, R.string.loading);

        contactDAO = new ContactUIDAO(c);


        app.sendEventTrack(TrackConstants.SCREEN_NAME_ACTIVE_CONTACTS_LIST, (String)null);
    }

    @Override
    public void onResume() {
        super.onResume();
        app.uiManager.addContactsUpdatesListener(LISTENER_ID, this);

        AUtils.sendPushNotificationHeartBeat(c);
        EdoNotification.cancelNotification(c, EdoNotification.ID_CONTACTS);
        setRefreshing(app.isContactsUpdateInProgress);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            mActivatedPosition = savedInstanceState.getInt(STATE_ACTIVATED_POSITION);
        }

        getListView().setDivider(null);

        getListView().setOnItemLongClickListener(new EdoDialogListView.OnListItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                settingsDialog(contacts.get(position));
                return true;
            }
        });

        setColorScheme(R.color.edo_blue_dark, R.color.edo_blue, R.color.edo_blue_light, R.color.edo_blue_very_light);

        EdoUI.setDistanceToTrigger(c, getSwipeRefreshLayout());

        loadContacts();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(contacts.get(position));


//        if(activateOnItemClick)
//            setActivatedPosition(position);
    }




    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        this.activateOnItemClick = activateOnItemClick;
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);



        if(activateOnItemClick) {
            if (getActivity() != null && getActivity() instanceof Act_ContactActiveList) {
                ((Act_ContactActiveList) getActivity()).setContactSelectedListener(new Act_ContactActiveList.ContactSelectedListener() {
                    @Override
                    public void onUserSelected(LContact user) {

                        setActivatedPosition(ListView.INVALID_POSITION);
                    }

                    @Override
                    public void onContactSelected(LContact contact) {
                        int index = contacts != null ? contacts.indexOf(contact) : -1;

                        if (index >= 0) {
                            setActivatedPosition(index);
                        }
                    }

                });
            }
        }
    }

    private void setActivatedPosition(int position) {
//        if (position == ListView.INVALID_POSITION) {
//            getListView().setItemChecked(mActivatedPosition, false);
//        } else {
//            getListView().setItemChecked(position, true);
//        }
        if(getListAdapter()!=null)
            ((ListAdapterEdoContact) getListAdapter()).notifyDataSetChanged(position);

        mActivatedPosition = position;
    }





    //*******************************
    //* Contacts
    //*******************************

    ArrayList<LContact> contacts = new ArrayList<LContact>();

    public void loadContacts(){
        AUtils.executeAsyncTask(new AsyncContacts(), "");
    }

    class AsyncContacts extends AsyncTask<String, String, ArrayList<LContact>> {


        @Override
        protected ArrayList<LContact> doInBackground(String... params) {
            return app.getActiveContacts();
        }


        @Override
        protected void onPostExecute(final ArrayList<LContact> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    if (result != null) {
                        contacts.clear();
                        contacts.addAll(result);

                        if (getListAdapter() != null) {
                            ((ListAdapterEdoContact) getListAdapter()).notifyDataSetChanged(mActivatedPosition);
                        } else {
                            setListAdapter(new ListAdapterEdoContact(c, contacts, contactPhotoLoader, mActivatedPosition, null, null));
                        }

                        mCallbacks.onOnViewsLoaded();
                    }

                    showBlankSheet(result==null || result.size()<=0);
                }
            });

            this.cancel(true);

        }
    }

    public void showBlankSheet(boolean show){
        if(show){
            if(c instanceof Act_ContactActiveList){
                ((Act_ContactActiveList)c).tv_blankSheet.setVisibility(View.VISIBLE);
            }
        }else{
            if(c instanceof Act_ContactActiveList){
                ((Act_ContactActiveList)c).tv_blankSheet.setVisibility(View.GONE);
            }
        }
    }

    public void settingsDialog(final LContact econtact){
        if(econtact!=null){
            final EdoDialogListView dialog = new EdoDialogListView(c);
            dialog.setTitle(econtact.getNameSurname());

            final ArrayList<Integer> titles = new ArrayList<Integer>();

            if(!econtact.isGroup()){
                titles.add(R.string.edit);
                titles.add(R.string.hideContact);
            }else{
                titles.add(R.string.groupInfo);
                if(econtact.isUserOwnerOfTheGroup(app.getEdoUserId())){
                    titles.add(R.string.modifyGroup);
                    titles.add(R.string.deleteGroup);
                }else{
                    titles.add(R.string.leaveGroup);
                }

            }

            ListAdapterOptions adapter = new ListAdapterOptions(c, titles);
            dialog.setAdapter(adapter);
            dialog.setPositiveButtonText(R.string.cancel);
            dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

                @Override
                public void onClick(View arg0) {
                    dialog.cancel();
                }
            });
            dialog.setOnListItemClickListener(new EdoDialogListView.OnListItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {
                    switch (titles.get(position)) {
                        case R.string.edit:
                            Intent editContact = new Intent(c, Act_ContactEditor.class);
                            editContact.putExtra(DBConstants.CONTAINER, econtact);
                            c.startActivityForResult(editContact, Constants.CONTACT_EDITOR_RESULT);
                            break;

                        case R.string.hideContact:
                            Runnable positiveAction = new Runnable() {

                                @Override
                                public void run() {
                                    hideContact(econtact);
                                }
                            };

                            if(!EdoUI.dialogNoInternet(c, positiveAction, null)){
                                positiveAction.run();
                            }

                            break;


                        case R.string.groupInfo:
                            EdoUI.getGroupInfoDialog(c, contactPhotoLoader, econtact);
                            break;

                        case R.string.modifyGroup:

                            Runnable modifyGroup = new Runnable() {

                                @Override
                                public void run() {
                                    Intent editGroup = new Intent(c, Act_GroupEditor.class);
                                    editGroup.putExtra(DBConstants.CONTAINER, econtact);
                                    c.startActivityForResult(editGroup, Constants.CONTACT_EDITOR_RESULT);
                                }
                            };

                            if(!EdoUI.dialogNoInternet(c, modifyGroup, null)){
                                modifyGroup.run();
                            }

                            break;

                        case R.string.deleteGroup:

                            final EdoDialog dialog = new EdoDialog(c);
                            dialog.setTitle(econtact.getName());
                            dialog.setText(c.getResources().getString(R.string.deleteGroupQuestion).
                                    replace(Constants.PH_CONTACTNAME, econtact.getNameSurname()));
                            dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

                                @Override
                                public void onClick(View v) {

                                    Runnable deleteGroup = new Runnable() {

                                        @Override
                                        public void run() {
                                            c.runOnUiThread(new Runnable() {public void run() {
                                                loading.setTitle(R.string.loading);
                                                loading.setSubTitle(R.string.deletingGroup);
                                                loading.show();
                                            }});
                                            new Thread(){@Override public void run(){
                                                Looper.myLooper();//Looper.prepare();
                                                if(new GroupUIDAO(c).delete(econtact)!=null)
                                                    c.runOnUiThread(new Runnable() {public void run() {
                                                        onDelete(null,econtact);
                                                        loading.dismiss();
                                                        EdoUI.showToast(c, R.string.groupDeleted, null, Toast.LENGTH_LONG);
                                                    }});
                                                else
                                                    c.runOnUiThread(new Runnable() {public void run() {
                                                        loading.dismiss();
                                                        EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_SHORT);
                                                    }});
                                            }}.start();

                                            dialog.dismiss();
                                        }
                                    };

                                    if(!EdoUI.dialogNoInternet(c, deleteGroup, null)){
                                        deleteGroup.run();
                                    }


                                }
                            });
                            dialog.show();



                            break;

                        case R.string.leaveGroup:

                            final EdoDialog dialog2 = new EdoDialog(c);
                            dialog2.setTitle(econtact.getName());
                            dialog2.setText(c.getResources().getString(R.string.leaveGroupQuestion).
                                    replace(Constants.PH_CONTACTNAME, econtact.getNameSurname()));
                            dialog2.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

                                @Override
                                public void onClick(View v) {

                                    Runnable leaveGroup = new Runnable() {

                                        @Override
                                        public void run() {
                                            c.runOnUiThread(new Runnable() {public void run() {
                                                loading.setTitle(R.string.loading);
                                                loading.setSubTitle(R.string.leavingGroup);
                                                loading.show();
                                            }});
                                            new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
                                                if(new GroupUIDAO(c).leaveTheGroup(econtact))
                                                    c.runOnUiThread(new Runnable() {
                                                        public void run() {
                                                            onDelete(null,econtact);
                                                            loading.dismiss();
                                                            EdoUI.showToast(c, R.string.youAreNoLongerAMemberOfTheGroup, null, Toast.LENGTH_LONG);
                                                        }
                                                    });
                                                else
                                                    c.runOnUiThread(new Runnable() {public void run() {
                                                        loading.dismiss();
                                                        EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_SHORT);
                                                    }});
                                            }}.start();

                                            dialog2.dismiss();
                                        }
                                    };

                                    if(!EdoUI.dialogNoInternet(c, leaveGroup, null)){
                                        leaveGroup.run();
                                    }


                                }
                            });
                            dialog2.show();

                            break;

                        default:
                            break;
                    }
                    dialog.cancel();
                }
            });
            dialog.show();
        }
    }

    public void hideContact(final LContact econtact){
        c.runOnUiThread(new Runnable() {public void run() {
            loading.setTitle(R.string.loading);
            loading.setSubTitle(R.string.hidingContact);
            loading.show();
        }});
        new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
            if(contactDAO.hideContact(econtact)){
                c.runOnUiThread(new Runnable() {public void run() {
                    onDelete(null,econtact);
                    loading.dismiss();
                    EdoUI.showToast(c, R.string.contactHidden, null, Toast.LENGTH_SHORT);
                }});
            }else{
                c.runOnUiThread(new Runnable() {public void run() {
                    loading.dismiss();
                    EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_SHORT);
                }});
            }
        }}.start();


    }




    //*******************************
    //* Contacts Updates
    //*******************************
    @Override
    public synchronized void onInsert(final EdoContainer container, final LContact contact) {

        if(contact.isActive() || contact.isSuggested() || contact.isGroup()) {

            if (getListAdapter() == null || app.isContactsUpdateInProgress)
                return;

            c.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int index = contacts.indexOf(contact);

                    if(contacts.size()==0){
                        showBlankSheet(false);
                    }

                    if (index < 0) {
                        contacts.add(0, contact);
                        ((ListAdapterEdoContact) getListAdapter()).notifyDataSetChanged(mActivatedPosition);
                    } else {
                        onUpdate(container, contact);
                    }
                    setActivatedPosition(mActivatedPosition);
                }
            });
        }
    }

    @Override
    public synchronized void onUpdate(EdoContainer container, final LContact contact) {

        if(getListAdapter()==null || app.isContactsUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(contacts.size()==0){
                    showBlankSheet(false);
                }

                int index = contacts.indexOf(contact);

                if(index>=0){
                    contacts.remove(index);
                    if(!contact.isHidden()) {
                        contacts.add(0, contact);
                    }
                    ((ListAdapterEdoContact) getListAdapter()).notifyDataSetChanged(mActivatedPosition);

                }else if(contact.isActive() || contact.isGroup()){
                    contacts.add(0, contact);
                    ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
                }

                setActivatedPosition(mActivatedPosition);
            }
        });
    }

    @Override
    public synchronized void onDelete(EdoContainer container, final LContact contact) {

        if(getListAdapter()==null || app.isContactsUpdateInProgress)
            return;


        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {


                int index = contacts.indexOf(contact);

                if(index>=0){
                    contacts.remove(index);
                    ((ListAdapterEdoContact) getListAdapter()).notifyDataSetChanged(mActivatedPosition);
                }

                setActivatedPosition(mActivatedPosition);
            }
        });
    }

    @Override
    public synchronized void onSyncList(LActivity a) {

        if(a!=null && a.hasBeenProcessed()) {
            loadContacts();
            if (isRefreshing())
                setRefreshing(false);
        }
    }




    @Override
    public void onRefresh() {
        super.onRefresh();

        if(app.getTransferServiceOperationsCount()<=0)
            AUtils.executeAsyncTask(new AsyncWebUpdate(), "");
        else
            setRefreshing(false);
    }

    public class AsyncWebUpdate extends AsyncTask<String, String, String>{
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            setRefreshing(true);
//        }

        @Override
        protected String doInBackground(String... params) {
            if(!app.isContactsUpdateInProgress)
                if(ASI.isInternetOn(c)){
                    c.runOnUiThread(new Runnable() {
                        public void run() {
                            EdoUI.showToast(c, R.string.updatingInProgress, null, Toast.LENGTH_SHORT);
                        }
                    });


                    long startTimeInMillis = System.currentTimeMillis();


                    EdoGeneralUpdate.contactsSection(c, new EdoNotification(c, getActivity().getClass()), false);


                }else{
                    c.runOnUiThread(new Runnable() {public void run() {
                        EdoUI.oneButtonDialog(c,  R.string.update, R.string.noInternetConnection);
                    }});
                }



            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            setRefreshing(false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        app.uiManager.removeListener(LISTENER_ID);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        if(resultCode == Activity.RESULT_OK) {
            switch(requestCode) {


                case Constants.CONTACT_EDITOR_RESULT:
                    LContact contact = (LContact) intent.getSerializableExtra(DBConstants.CONTAINER);
                    if(contact!=null){
                        onUpdate(null,contact);
                    }
                    break;

                case Constants.CONTACT_SELECT_RESULT:
                    LContact newContact = (LContact) intent.getSerializableExtra(DBConstants.CONTAINER);
                    if(newContact!=null){
                        onInsert(null,newContact);
                        mCallbacks.onItemSelected(newContact);
                    }

                    break;

                default:
                    break;
            }


        }

    }
}
