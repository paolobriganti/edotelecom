package io.edo.edomobile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.paolobriganti.android.AUI;
import com.paolobriganti.android.AUtils;
import com.paolobriganti.android.VU;
import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.ImageUtils;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.imageLoader.ImageResizer;

import java.io.File;
import java.io.FileDescriptor;
import java.io.Serializable;
import java.util.ArrayList;

import io.edo.db.global.uibeans.GroupUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.web.beans.WFileDAO;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.adapters.contacts.ContactViewHolder;
import io.edo.ui.adapters.contacts.CursorAdapterContact;
import io.edo.ui.adapters.contacts.CursorAdapterNewQuickContact;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

/**
 * A fragment representing a single Contact detail screen.
 * This fragment is either contained in a {@link Act_ContactActiveList}
 * in two-pane mode (on tablets) or a {@link Act_ContactDetail}
 * on handsets.
 */
public class Frag_GroupEditor extends Fragment implements SearchView.OnQueryTextListener, SearchView.OnSuggestionListener{
    FragmentActivity c;
    ThisApp app;

    private GroupUIDAO groupDAO;

    private LContact oldGroup;
    private ArrayList<LContact> originalMembers = new ArrayList<LContact>();
    private String originalName = null;

    private ArrayList<LContact> members = new ArrayList<LContact>();

    private ContactPhotoLoader contactPhotoLoader;

    private boolean isNew = true;

    private ImageView iv_group_photo;
    private TextView tv_groupName;
    private ImageButton ib_chooseIcon;
    private EditText et_name;


    private SearchView searchView;

    public LinearLayout ll_members_container;
    public ProgressBar mProgressBar;

    private EdoUI edoUI;
    EdoProgressDialog loading;

    private LayoutInflater inflater;

    public Frag_GroupEditor() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        c = getActivity();
        app = ThisApp.getInstance(c);

        groupDAO = new GroupUIDAO(c);

        contactPhotoLoader = ContactPhotoLoader.getInstance(c);

        edoUI = new EdoUI(c);
        loading = new EdoProgressDialog(c, R.string.group, R.string.loading);

        inflater = c.getLayoutInflater();

        if (getArguments()!=null && getArguments().containsKey(DBConstants.CONTAINER)) {
            Serializable contactSerial = getArguments().getSerializable(DBConstants.CONTAINER);
            if (contactSerial != null) {
                oldGroup = (LContact) contactSerial;
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_group_editor, container, false);


        iv_group_photo = (ImageView) rootView.findViewById(R.id.iv_group_photo);
        tv_groupName = (TextView) rootView.findViewById(R.id.tv_groupName);
        ib_chooseIcon = (ImageButton) rootView.findViewById(R.id.ib_chooseIcon);
        et_name = (EditText) rootView.findViewById(R.id.et_name);

        et_name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                }
                return false;
            }
        });

        searchView = (SearchView) rootView.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);
        searchView.setOnSuggestionListener(this);
        searchView.setIconifiedByDefault(false);

        EditText searchViewEditText = VU.getSearchViewEditText(searchView);
        if(searchViewEditText!=null) {
            searchViewEditText.setTextColor(c.getResources().getColor(R.color.edo_black));
            searchViewEditText.setHintTextColor(c.getResources().getColor(R.color.edo_gray));
        }
        ImageView searchViewSearchIcon = VU.getSearchViewSearchHintIcon(searchView);
        if(searchViewSearchIcon!=null){
            searchViewSearchIcon.setVisibility(View.GONE);
            searchViewSearchIcon.setImageResource(android.R.drawable.ic_menu_search);
        }

        ImageView searchViewCloseIcon = VU.getSearchViewCloseButton(searchView);
        if(searchViewSearchIcon!=null){
            searchViewCloseIcon.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
        }

        ll_members_container = (LinearLayout) rootView.findViewById(R.id.ll_members_container);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);


        ib_chooseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoPicker();
            }
        });

        if(oldGroup!=null){
            isNew = false;
            updateViews(oldGroup);
        }else{
            isNew = true;
        }

        setBackPressed(rootView);


        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void updateViews(LContact oldGroup){
        this.oldGroup = oldGroup;
        contactPhotoLoader.loadImage(oldGroup, iv_group_photo);

        et_name.setText(oldGroup.getName());

        originalName = oldGroup.getName();

        loadMembers();
    }

    public LContact getOldGroup(){
        return oldGroup;
    }

    public boolean isNew(){
        return isNew;
    }

    public void setName(String name){
        et_name.setText(name);
    }

    public String getName(){
        return et_name.getText().toString();
    }

    public String getNewName(){
        String name = et_name.getText().toString();

        if(originalName!=null && !originalName.equals(name)){
            return name;
        }

        return null;
    }

    public Bitmap getGroupPhoto(){
        return groupPhoto;
    }

    public ArrayList<LContact> getMembers(){
        return members;
    }

    public ArrayList<LContact> getNewMembers(){
        if(originalMembers!=null && originalMembers.size()>0 && members!=null && members.size()>0){
            ArrayList<LContact> newMemebers = new ArrayList<>();
            for(LContact member: members){
                if(!originalMembers.contains(member)){
                    newMemebers.add(member);
                }
            }
            return newMemebers;
        }

        return null;
    }

    public ArrayList<LContact> getRemovedMembers(){
        if(originalMembers!=null && originalMembers.size()>0 && members!=null && members.size()>0){
            ArrayList<LContact> removedMemebers = new ArrayList<>();


            for(LContact originalMember: originalMembers){
                if(!members.contains(originalMember)){
                    removedMemebers.add(originalMember);
                }
            }
            return removedMemebers;
        }

        return null;
    }





    //****************************************************************************************************************************
    //* Members
    //****************************************************************************************************************************

    public void clearMembers(){
        this.members.clear();
        ll_members_container.removeAllViews();
    }

    public void addMembers(ArrayList<LContact> members){
        this.members.addAll(members);
        for(int i=0; i<members.size(); i++){
            View view = getMemberLayout(i,members.get(i));
            ll_members_container.addView(view,0);
        }
    }

    public void addMember(LContact member){
        this.members.add(member);
        View view = getMemberLayout(this.members.size()-1,member);
        ll_members_container.addView(view,0);
    }

    public void removeMember(LContact member, View view){
        this.members.remove(member);
        ll_members_container.removeView(view);
    }

    public void loadMembers(){
        new MembersTask().execute();
    }

    class MembersTask extends AsyncTask<String, String, ArrayList<LContact>> {

        @Override
        protected void onPreExecute() {

            c.runOnUiThread(new Runnable() {
                public void run() {
                    ll_members_container.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                }
            });

            super.onPreExecute();
        }

        @Override
        protected ArrayList<LContact> doInBackground(String... params) {
            return groupDAO.getLDAO().getContactsOfGroup(oldGroup.getId());
        }


        @Override
        protected void onPostExecute(final ArrayList<LContact> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    if (result != null) {
                        clearMembers();
                        addMembers(result);
                        originalMembers = new ArrayList<LContact>(result);
                    }
                    ll_members_container.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                }
            });
            this.cancel(true);
        }
    }


    public View getMemberLayout(int position, final LContact member) {
        if (member == null || member.getPrincipalEmailAddress() == null)
            return null;

        final View rootView = inflater.inflate(R.layout.row_edo_contact_right_image, ll_members_container, false);

        ContactViewHolder vh = new ContactViewHolder(rootView);
        vh.setContactViews(c,member,contactPhotoLoader,app.getEdoUserId(),null,null,""+(position+1),null);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EdoDialog dialog = new EdoDialog(c);
                dialog.setTitle(R.string.removeMember);
                dialog.setText(c.getResources().getString(R.string.removeContactFromGroupQuestion).
                        replace(Constants.PH_CONTACTNAME, member.getNameSurname()));
                dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

                    @Override
                    public void onClick(View v) {
                        removeMember(member, rootView);
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
        });

        return rootView;
    }



    //Search
    class AsyncSearchContacts extends AsyncTask<String, Cursor, Cursor>{

        String newText;

        public AsyncSearchContacts(String name) {
            super();
            this.newText = name;
        }


        @Override
        protected Cursor doInBackground(String... params) {

            if(JavaUtils.isNotEmpty(newText)){
                return new LContactDAO(c).getContactsOnlyCursorByNameLike(newText);
            }


            return null;
        }


        @Override
        protected void onPostExecute(Cursor cursor) {
            // TODO Auto-generated method stub
            super.onPostExecute(cursor);

            if(cursor!=null && cursor.getCount() > 0) {
                isNewContact = false;

                resultContacts.clear();

                CursorAdapterContact simple = new CursorAdapterContact(c, cursor, contactPhotoLoader, R.drawable.ic_action_new);

                searchView.setSuggestionsAdapter(simple);


                while(cursor.moveToNext()){
                    resultContacts.add(new LContact(c, cursor));
                }


            } else {
                isNewContact = true;

                String email = newText;
                String name = email.contains("@")? email.split("@")[0] : newText;

                LContact econtact = new LContact(name, (String)null, (String)null);
                econtact.setPrincipalEmailAddress(email);

                resultContacts.clear();
                resultContacts.add(econtact);

                searchView.setSuggestionsAdapter(CursorAdapterNewQuickContact.getQuickContactCursorAdapter(c, econtact.getId(), c.getResources().getString(R.string.createContact), email));

            }
        }

    }

    ArrayList<LContact> resultContacts = new ArrayList<LContact> ();


    boolean isNewContact = false;

    public boolean onQueryTextChange(String newText) {
        if(JavaUtils.isNotEmpty(newText))
            AUtils.executeAsyncTask(new AsyncSearchContacts(newText), "");
        return false;
    }


    @Override
    public boolean onSuggestionClick(int position) {
        if(members!=null && members.size()<LContact.MAX_MEMBERS){
            LContact member = resultContacts.get(position);

            if(isNewContact && !JavaUtils.isEmailAddress(member.getPrincipalEmailAddress())){
                edoUI.showToastLong(c.getResources().getString(R.string.wrongEmailAddress), null);
                return false;
            }

            if(!members.contains(member)){
                addMember(member);
            }

        }else{
            edoUI.showToastLong(
                    c.getResources().getString(R.string.youCanAddMaxUsersPerGroup).replace(Constants.PH_NUM, ""+LContact.MAX_MEMBERS),
                    null);
        }

        searchView.setQuery("", false);
        AUI.keyboardHide(c);
        return true;
    }


    @Override
    public boolean onSuggestionSelect(int arg0) {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }













    //****************************************************************************************************************************
    //* GROUP PHOTO
    //****************************************************************************************************************************
    private Bitmap groupPhoto = null;

    int GALLERY_INTENT_CALLED = 3;
    int GALLERY_KITKAT_INTENT_CALLED = 4;

    public void photoPicker(){
        if (Build.VERSION.SDK_INT <19){
            Intent intent = new Intent();
            intent.setType("image/jpeg");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.photo)),GALLERY_INTENT_CALLED);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/jpeg");
            startActivityForResult(intent, GALLERY_KITKAT_INTENT_CALLED);
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;
        if (null == data) return;
        final Uri originalUri;
        if (requestCode == GALLERY_INTENT_CALLED) {
            originalUri = data.getData();
        } else if (requestCode == GALLERY_KITKAT_INTENT_CALLED) {
            originalUri = data.getData();
            //	        final int takeFlags = data.getFlags()
            //	                & (Intent.FLAG_GRANT_READ_URI_PERMISSION
            //	                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // Check for the freshest data.
            //	        getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
        }else{
            originalUri = null;
        }

        String photoPath = FileInfo.getRealPathFromURI(c, originalUri);

        if(photoPath==null){

            String mimeType = FileInfo.getMimeFromURI(c.getContentResolver(), originalUri);

            if(mimeType!=null && mimeType.contains("image")){

                String name = FileInfo.getContentNameFromURI(c.getContentResolver(), originalUri);

                loading.setTitle(R.string.loading);
                loading.setSubTitle(name!=null?name:c.getResources().getString(R.string.photo));
                loading.show();

                new Thread(){@Override public void run(){
                    Looper.myLooper();//Looper.prepare();
                    groupPhoto = getBitmapFromUri(c, originalUri);
                    setGroupPhotoIcon();
                    loading.dismiss();
                }}.start();

            }

        }else{
			groupPhoto = ImageResizer.decodeSampledBitmapFromFile(photoPath, (int) AUI.convertDpToPixel(c, WFileDAO.MAX_SIDE_THUMB_SIZE_DP));
            setGroupPhotoIcon();
        }

    }

    public void setGroupPhotoIcon(){
        if(groupPhoto!=null){
            groupPhoto = ImageUtils.getCroppedCircleCenterBitmap(groupPhoto, null);

            c.runOnUiThread(new Runnable() {
                public void run() {
                    iv_group_photo.setImageBitmap(groupPhoto);
                }
            });

//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    uploadPhoto("" + System.currentTimeMillis());
//                }
//            }).start();


        }else{
            edoUI.showToastLong(c.getResources().getString(R.string.errorRetry), null);
        }
    }

    public String uploadPhoto(String groupId){
        if(groupPhoto!=null){
            String localPath = Constants.getExternalCacheDir(c).getPath()+File.separator+"temp";
            String name = groupId+".png";
            String filePath = localPath + File.separator + name;
            eLog.v("THUMBNAIL", "Thumbnail filePath " + filePath);
            if(ImageUtils.saveBitmapToFile(groupPhoto, Bitmap.CompressFormat.PNG, localPath, name)) {
                eLog.v("THUMBNAIL", "Thumbnail size " + FileInfo.getFileKByteSize(filePath) + "kb");
                String thumbnailLink = WFileDAO.uploadThumbail(c, filePath);
                eLog.d("THUMBNAIL", "thumbnailLink" + thumbnailLink);
                if (thumbnailLink == null && new File(filePath) != null) {
                    new File(filePath).delete();
                }
                return thumbnailLink;
            }
        }
        return null;
    }



    public Bitmap getBitmapFromUri(Context c, Uri uri){
        try{
            ParcelFileDescriptor parcelFileDescriptor =
                    c.getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = ImageResizer.decodeSampledBitmapFromDescriptor(fileDescriptor, (int) AUI.convertDpToPixel(c, WFileDAO.MAX_SIDE_THUMB_SIZE_DP));
            parcelFileDescriptor.close();
            return image;
        }catch (Exception e){
        }
        return null;
    }













    public void setBackPressed(View v){
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

//                    if (mViewPager.getCurrentItem()!=0) {
//                        mViewPager.setCurrentItem(mViewPager.getCurrentItem()-1);
//                        return true;
//                    }
                }
                return false;
            }
        } );
    }

}
