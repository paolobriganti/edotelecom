package io.edo.edomobile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.io.Serializable;
import java.util.ArrayList;

import io.edo.db.global.beans.ContactFileDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactFileDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.EdoMessageBuilder;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoDialogListView;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoIntentManager;
import io.edo.ui.EdoUI;
import io.edo.ui.TransitionInfos;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.ListAdapterOptionsText;
import io.edo.ui.adapters.files.FilesAdapter;
import io.edo.utilities.Constants;
import io.edo.utilities.TrackConstants;

public class Frag_ContactSpaceFiles extends Frag_Base
        implements UIManager.FilesUpdatesListener, FilesAdapter.FileClickListener, FilesAdapter.FileLongClickListener{

    private final static String LISTENER_ID = Frag_ContactSpaceFiles.class.getName();

    private LSpace space = null;
    private ArrayList<LFile> files = new ArrayList<LFile>();
    private FilesAdapter mAdapter;
    private StaggeredGridLayoutManager lm;

    private int sortFilesBy = Constants.SORT_BY_MDATE_DESC;

    private View card_breadcrumbs;
    private TextView tv_breadcrumbs;

    public static final String EXTRA_IS_INBOX = "IS_INBOX";
    boolean isInbox = false;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Frag_ContactSpaceFiles() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(DBConstants.SPACE)) {
            Serializable spaceSerial = getArguments().getSerializable(DBConstants.SPACE);
            if(spaceSerial!=null) {
                space = (LSpace) spaceSerial;
            }else{
                space = getTimelineSpace();
            }
        }else{
            space = getTimelineSpace();
        }

        isInbox = getArguments().getBoolean(EXTRA_IS_INBOX,false);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        String message = !isInbox ? EdoMessageBuilder.getMessageBlancSheetOfFiles(c, contact):
                EdoMessageBuilder.getMessageBlankSheetOfInbox(c, contact);
        tv_blankSheet.setText(Html.fromHtml(message));
        if(isInbox){
            iv_blankSheet.setVisibility(View.GONE);
        }else if(contact.isPersonal(c)){
            iv_blankSheet.setImageResource(R.drawable.blank_files);
        }else if(contact.isGroup()){
            iv_blankSheet.setImageResource(R.drawable.blank_files);
        }else{
            iv_blankSheet.setImageResource(R.drawable.blank_files);
        }

        card_breadcrumbs = rootView.findViewById(R.id.card_breadcrumbs);
        card_breadcrumbs.setVisibility(!space.isDefault()?View.VISIBLE:View.GONE);

        tv_breadcrumbs = (TextView) rootView.findViewById(R.id.tv_breadcrumbs);

        tv_breadcrumbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upLevel();
            }
        });
        tv_breadcrumbs.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                chooseFolder();
                return true;
            }
        });

        lm = new StaggeredGridLayoutManager(EdoUI.getFilesColumnsNumber(c), StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(lm);

        mAdapter = new FilesAdapter(getActivity(), app.thumbnailFileLoader, files);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setFileClickListener(this);
        mAdapter.setLongFileClickListener(this);

        if(isInbox){
            fabButton.setVisibility(View.GONE);
        }

        loadFiles();

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        app.uiManager.addFilesUpdatesListener(LISTENER_ID, this);
    }

    @Override
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public ImageLoader getImageLoader() {
        return app.thumbnailFileLoader;
    }

    public void onFabButtonClick(View v){
        if(!selectFolder){
            filesPicker.pickersSlider(getCurrentContainer(), space, gv_pickers, slidingPickers, -1);
            app.sendEventTrack(TrackConstants.EVENT_ADD_FILE_ON_SPACE_CLICK, TrackConstants.KEY_CONTEXT, contact.isPersonal(c) ? TrackConstants.VALUE_CONTEXT_PERSONAL : (contact.isGroup() ? TrackConstants.VALUE_CONTEXT_GROUP : TrackConstants.VALUE_CONTEXT_CONTACTS));
        }else{
            fileOptions.addFolderDialog(loading, space);
        }

    }

    @Override
    public int getContentViewResourceId() {
        return R.layout.fragment_contact_files;
    }

    @Override
    public boolean canAddFirstSpace() {
        return false;
    }

    @Override
    public boolean canHideFooterOnScroll() {
        return true;
    }

    @Override
    public LSpace getCurrentSpace() {
        return space;
    }


    //*******************************
    //* Files
    //*******************************
    public void loadFiles()
    {
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new FilesTask(false).execute();
            }
        });
    }

    public void refreshFiles(){
        if(app.isUpdateInProgress) {
            srlayout.setRefreshing(false);
            return;
        }
        new FilesTask(true).execute();
    }


    class FilesTask extends AsyncTask<String, String, ArrayList<LFile>> {
        boolean isRefreshing = false;

        public FilesTask(boolean isRefreshing) {
            this.isRefreshing = isRefreshing;
        }

        @Override
        protected void onPreExecute() {
            app.isUpdateInProgress = true;

            if(isRefreshing){
                srlayout.setRefreshing(true);
            }

            super.onPreExecute();
        }

        @Override
        protected ArrayList<LFile> doInBackground(String... params) {
            EdoContainer container = getCurrentContainer();
            if(container!=null) {

                if(isRefreshing){
                    LFile[] files = new ContactFileDAO(c).downloadAllFileOfFolderOf((LContact) getCurrentContainer(), space.getCurrentFolder().getId());
                    if(files==null || files.length<=0){
                        return null;
                    }
                }

                if(getTimelineSpace()==null) {
                    onNoTimelineSpaceFound();
                }

                setBreadcrumbsViews();
                LFile currentFolder = space.getCurrentFolder();

                if(mOnSpaceSelectedListener!=null)
                    mOnSpaceSelectedListener.onSpaceSelected(space);

                if(!isInbox)
                    return fileDAO.getLDAO().getSubFiles(currentFolder, sortFilesBy, -1, -1, false);
                else
                    return new LContactFileDAO(c).getFileListBy(contact);

            }
            return null;
        }


        @Override
        protected void onPostExecute(final ArrayList<LFile> result) {
            app.isUpdateInProgress = false;
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    mAdapter.clearFiles();
                    if (result != null && result.size()>0) {
                        mAdapter.addFiles(result);
                        showList(true);
                    }else{
                        showList(false);
                    }
                    space.setFilesOfCurrentFolder(result);
                    mProgressBar.setVisibility(View.GONE);
                    showFooter();

                    if(isRefreshing){
                        srlayout.setRefreshing(false);
                    }
                }
            });
            this.cancel(true);
        }
    }

    public void addFileToList(LFile file){
        showList(true);
        if(!files.contains(file)) {
            mAdapter.addFile(file);
            lm.scrollToPosition(0);
        }
    }

    public void updateFileOnList(LFile file){
        mAdapter.updateFile(file);
    }

    public void updateFileOnList(int index, LFile file){
        mAdapter.updateFile(index, file);
    }

    public void removeFileFromList(LFile file){
        mAdapter.removeFile(file);
    }

    public void setBreadcrumbsViews(){
        if(space==null)
            return;

        String breadCrumbs = c.getResources().getString(R.string.Spaces);
        final ArrayList<LFile> parentFolders = space.getParentFolders(c);
        if(parentFolders!=null && parentFolders.size()>0) {
            for (LFile folder : parentFolders) {
                breadCrumbs += "/" + folder.getName();
            }
        }
        breadCrumbs += "/"+space.getCurrentFolder().getName();
        final String finalBreadCrumbs = breadCrumbs;
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_breadcrumbs.setText(finalBreadCrumbs);
            }
        });
    }

    @Override
    public void onClick(FilesAdapter.ViewHolder v, LFile file) {
        if(!file.isSelected() && !isSelOptionsActionModeEnabled) {
            if (file.isFolder()) {
                goToFolder(file);
            } else if (file.isSnippet()) {
                c.startActivity(EdoIntentManager.getSnippetIntent(c, getCurrentContainer(), !isInbox?space:file.getSpace(c), file, null));
            } else {
                showPreview(v, file);
            }
        }else{
            addSelectedFile(v,file);
        }
    }


    @Override
    public void onLongClick(FilesAdapter.ViewHolder v, LFile file) {
        addSelectedFile(v,file);
    }


    public void addSelectedFile(FilesAdapter.ViewHolder vh, LFile file){
        startSelOptionsActionMode();
        if(!selFiles.contains(file)){
            selFiles.add(file);
            file.setIsSelected(true);
            vh.setSelected(true);
            setSelOptionsActionModeTitle("" + selFiles.size());
        }else{
            selFiles.remove(file);
            file.setIsSelected(false);
            vh.setSelected(false);
            setSelOptionsActionModeTitle("" + selFiles.size());
            if(selFiles.size()==0){
                stopSelOptionsActionMode();
            }
        }
    }

    @Override
    public void onSelectionFinished() {
        super.onSelectionFinished();

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (LFile file : files) {
                    file.setIsSelected(false);
                }
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onRefresh() {
        refreshFiles();
    }


    //*******************************
    //* UPDATES
    //*******************************
    @Override
    public synchronized void onInsert(EdoContainer container, final LFile file) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean isCorrectSpace = isInbox || (file.getParentId()!=null && file.getParentId().equals(space.getCurrentFolder().getId()));
                if (file != null &&
                        isCorrectSpace &&
                        !files.contains(file)) {

                    addFileToList(file);


                }

            }
        });
    }

    @Override
    public synchronized void onUpdate(EdoContainer container, final LFile file) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file != null)
                    updateFileOnList(file);
            }
        });
    }

    @Override
    public void onUploadToCloud(EdoContainer container, final LFile file, final int progressStatus, final int progressPercent) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean isCorrectSpace = isInbox || (file.getParentId()!=null && file.getParentId().equals(space.getCurrentFolder().getId()));

                if (file != null && isCorrectSpace) {
                    if (!files.contains(file)) {
                        addFileToList(file);
                    }
                    FilesAdapter.ViewHolder vh = mAdapter.getViewHolder(file.getId());
                    if (vh != null) {
                        String text = c.getResources().getString(R.string.upload).toUpperCase();
                        vh.setProgress(progressStatus, progressPercent,text);
                    }
                }
            }
        });
    }

    @Override
    public void onDownloadFromCloud(EdoContainer container, final LFile file, final int progressStatus, final int progressPercent) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean isCorrectSpace = isInbox || (file.getParentId()!=null && file.getParentId().equals(space.getCurrentFolder().getId()));


                if (file != null && isCorrectSpace) {

                    if (!files.contains(file)) {
                        addFileToList(file);
                    }
                    FilesAdapter.ViewHolder vh = mAdapter.getViewHolder(file.getId());
                    if (vh != null) {
                        String text = c.getResources().getString(R.string.download).toUpperCase();
                        vh.setProgress(progressStatus,progressPercent,text);
                    }
                }
            }
        });
    }

    @Override
    public synchronized void onDelete(EdoContainer container, final LFile file) {
        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (file != null)
                    removeFileFromList(file);
            }
        });
    }

    @Override
    public synchronized void onSyncList(LActivity a) {
        if(app.isUpdateInProgress)
            return;

        if(a.hasBeenProcessed())
            loadFiles();
    }









    public void showPreview(FilesAdapter.ViewHolder v, final LFile file) {

        TransitionInfos transitionInfo = new TransitionInfos(c, v.iv_file, file.getId(), file.getName(), file.getvType());


        Intent gallery = EdoIntentManager.getFilePageIntent(c,
                getCurrentContainer(), !isInbox?space:null,
                file, transitionInfo);
        c.startActivity(gallery);


        // Override transitions: we don't want the normal window animation in addition
        // to our custom one
        c.overridePendingTransition(0, 0);
    }


    public void upLevel(){
        if(isTopLevel()){
            if(mLevelUpFinishListener!=null)
                mLevelUpFinishListener.onLevelUpFinished(null);
        }else{
            goToFolder(null);
        }

    }


    public void goToFolder(final LFile nextFolder){
        if(space!=null){
            new Thread(){@Override public void run(){
                Looper.myLooper();//Looper.prepare();
                LFile previousFolder = space.getCurrentFolder();
                LFile currentFolder = nextFolder;

                if(currentFolder==null) {
                    currentFolder = fileDAO.getLDAO().get(previousFolder.getParentId());
                }

                space.setCurrentFolder(currentFolder);

                c.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadFiles();
                    }
                });


            }}.start();
        }
    }


    public boolean isTopLevel() {
        if(space!=null){
            return space.isTopLevel();
        }
        return true;
    }

    public void chooseFolder(){
        if(space!=null){
            new Thread(){@Override public void run(){Looper.myLooper();Looper.prepare();
                if(!space.getCurrentFolder().isSpace()){

                    final ArrayList<String> titles = new ArrayList<String>();

                    final ArrayList<LFile> parentFolders = space.getParentFolders(c);

                    if(parentFolders!=null && parentFolders.size()>0){
                        int count = 0;
                        for(LFile folder:parentFolders){
                            String name = "";
                            for(int i=0; i<count; i++){
                                name += "  ";
                            }
                            name += folder.getName();
                            titles.add(name);
                            count++;
                        }

                        c.runOnUiThread(new Runnable() {public void run() {
                            final EdoDialogListView dialog = new EdoDialogListView(c);
                            dialog.setTitle(space.getCurrentFolder().getName());

                            ListAdapterOptionsText adapter = new ListAdapterOptionsText(c, titles);
                            dialog.setAdapter(adapter);
                            dialog.setPositiveButtonText(R.string.cancel);
                            dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

                                @Override
                                public void onClick(View arg0) {

                                    dialog.cancel();

                                }
                            });
                            dialog.setOnListItemClickListener(new EdoDialogListView.OnListItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                                    goToFolder(parentFolders.get(position));

                                    dialog.cancel();
                                }
                            });


                            dialog.show();
                        }});


                    }
                }





            }}.start();
        }



    }


    LevelUpFinishListener mLevelUpFinishListener;

    public void setLevelUpFinishListener(LevelUpFinishListener listener) {
        mLevelUpFinishListener = listener;
    }

    public interface LevelUpFinishListener {
        void onLevelUpFinished(View v);
    }


    OnSpaceSelectedListener mOnSpaceSelectedListener;

    public void setOnSpaceSelectedListener(OnSpaceSelectedListener listener) {
        mOnSpaceSelectedListener = listener;
    }

    public interface OnSpaceSelectedListener {
        void onSpaceSelected(LSpace space);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        app.uiManager.removeListener(LISTENER_ID);
    }

    @Override
    public boolean onBackPressed() {
        if(!isTopLevel()){
            upLevel();
            return true;
        }


        return super.onBackPressed();
    }
}
