package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.paolobriganti.utils.JavaUtils;

import java.io.Serializable;
import java.util.ArrayList;

import io.edo.db.global.uibeans.GroupUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.imageutils.ThumbnailManager;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 07/07/15.
 */
public class Act_GroupEditor  extends FragmentActivity {
    Activity c;
    ThisApp app;

    EdoProgressDialog loading;
    EdoUI edoUI;


    GroupUIDAO groupDAO;
    LContact groupToEdit = null;

    Frag_GroupEditor fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_contact_editor);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        c = this;
        app = ThisApp.getInstance(c);


        loading = new EdoProgressDialog(this, R.string.group, R.string.loading);
        edoUI = new EdoUI(c);

        groupDAO = new GroupUIDAO(c);

        Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            if (extras.containsKey(DBConstants.CONTAINER)) {
                Serializable contactSerial = extras.getSerializable(DBConstants.CONTAINER);
                if (contactSerial != null) {
                    groupToEdit = (LContact) contactSerial;
                }
            }
        }

        Bundle arguments = new Bundle();
        arguments.putSerializable(DBConstants.CONTAINER, groupToEdit);
        fragment = new Frag_GroupEditor();
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.contact_editor_container, fragment)
                .commit();

        if(groupToEdit!=null){
            getActionBar().setTitle(groupToEdit.getNameSurname());
        }else{
            getActionBar().setTitle(R.string.createGroup);
        }

    }




    public void saveGroup(){
        boolean isInternetOn = !EdoUI.dialogNoInternet((Activity) c,
                new Runnable() {public void run() {
                    saveGroup();
                }},
                null);

        if(isInternetOn){

            final String name = fragment.getName();

            if(JavaUtils.isNotEmpty(name)){

                if(fragment.getMembers()!=null && fragment.getMembers().size()>=LContact.MIN_MEMBERS-1){
                    loading.setTitle(name);
                    loading.setSubTitle(fragment.isNew()?R.string.savingNewGroup:R.string.updatingGroup);
                    loading.show();


                    new Thread(){@Override public void run(){
                        Looper.myLooper();


                        if(fragment.isNew()){
                            LContact cgroup = new LContact(c, name, app.getEdoUserId(), fragment.getMembers());


                            String thumbnailLink = fragment.uploadPhoto(cgroup.getId());
                            if(thumbnailLink!=null){
                                cgroup.setThumbnail(thumbnailLink);
                            }


                            if(groupDAO.add(cgroup)!=null){
                                if(fragment.getGroupPhoto()!=null && JavaUtils.isEmpty(cgroup.getThumbnail())){
                                    edoUI.showToastLong(c.getResources().getString(R.string.groupCreatedButImageNotUploaded), null);
                                }else{
                                    edoUI.showToastLong(c.getResources().getString(R.string.groupCreated), null);
                                }
                                goBackWithResult(cgroup);
                                return;
                            }else{
                                edoUI.showToastLong(c.getResources().getString(R.string.errorRetry), null);
                            }
                        }else if(fragment.getOldGroup()!=null){
                            LContact oldGroup = fragment.getOldGroup();

                            oldGroup.setName(name);
                            oldGroup.setGroupMembers(c, fragment.getMembers());


                            // Update members
                            if(fragment.getMembers().size()>0){
                                //Check which members are been added
                                boolean ok = true;

                                //Saving new members
                                ArrayList<LContact> newMembers = fragment.getNewMembers();
                                if(newMembers!=null && newMembers.size()>0){

                                    ok = groupDAO.addContactsToGroup(oldGroup, newMembers)!=null;

                                }

                                //Detect and remove removed members
                                ArrayList<LContact> removedMembers = fragment.getRemovedMembers();
                                if(removedMembers!=null && removedMembers.size()>0){
                                    for(LContact ori: removedMembers){
                                        groupDAO.deleteContactFromGroup(oldGroup, ori);
                                    }
                                }

                                if(!ok){
                                    loading.dismiss();
                                    edoUI.showToastLong(c.getResources().getString(R.string.errorRetry), null);
                                    Act_GroupEditor.this.finish();
                                    return;
                                }

                            }

                            //Update MetaData
                            boolean updateMeta = false;

                            //Update thumbnail
                            String thumbnailLink = fragment.uploadPhoto(oldGroup.getId());
                            if(thumbnailLink!=null){
                                oldGroup.setThumbnail(thumbnailLink);
                                updateMeta = true;
                                ThumbnailManager.removeThumbanilFromMemory(c,oldGroup.getId());
                            }


                            //Update newName
                            boolean rename = fragment.getNewName()!=null;
                            if(rename){
                                oldGroup.setName(fragment.getNewName());
                                updateMeta = true;
                            }

                            if(updateMeta) {
                                if(groupDAO.updateGroupMetadata(oldGroup,rename)!=null){
                                    goBackWithResult(oldGroup);
                                }else{
                                    edoUI.showToastLong(c.getResources().getString(R.string.errorRetry)+"2", null);
                                }
                            }else{
                                goBackWithResult(oldGroup);
                            }

                        }
                        loading.dismiss();
                    }}.start();


                }else{
                    edoUI.showToastLong(
                            c.getResources().getString(R.string.addAlmostNUMMembers).replace(Constants.PH_NUM, ""+(LContact.MIN_MEMBERS-1)),
                            null);

                }



            }else{
                edoUI.showToastLong(c.getResources().getString(R.string.pleaseEnterTheNameOfTheGroup), null);
            }
        }
    }



    public void goBackWithResult(final LContact group){
        if(group!=null){

            if(loading!=null)loading.dismiss();
            Intent extra = new Intent();
            extra.putExtra(DBConstants.CONTAINER, group);
            setResult(RESULT_OK, extra);
            Act_GroupEditor.this.finish();


        }

    }





    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contacteditor, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.post:
                saveGroup();

                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//            AUI.navigateUpTo(c, new Intent(c, Act_GroupActiveList.class));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };
}
