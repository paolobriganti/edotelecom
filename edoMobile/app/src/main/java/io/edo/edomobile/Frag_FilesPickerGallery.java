package io.edo.edomobile;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.edo.db.local.beans.LFile;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.ui.imageutils.ThumbnailFilePickerGalleryLoader;

/**
 * Created by PaoloBriganti on 12/06/15.
 */
public class Frag_FilesPickerGallery extends Frag_FilesPicker {

    private ThumbnailFilePickerGalleryLoader imageLoader;

    public Frag_FilesPickerGallery() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageLoader = new ThumbnailFilePickerGalleryLoader(c);
    }


    @Override
    public ThumbnailFileLoader getThumbnailFileLoader() {
        return imageLoader;
    }



    @Override
    public ArrayList<LFile> getFiles(LFile folder) {

        ArrayList<LFile> mFiles = new ArrayList<LFile>();

        // Get the files in the directory
        File mDirectory = (File) getFile(folder);
        if(mDirectory==null && mediaFolders.size()==0){
            mediaFolders = getPathOfAllMedia(c);
        }

        ArrayList<File> files = mDirectory!=null? mediaFoldersMap.get(mDirectory):mediaFolders;
        if(files != null && files.size() > 0) {
            for(File f : files) {
                if(f.isHidden() && !mShowHiddenFiles) {
                    // Don't add the file
                    continue;
                }

                // Add the file the ArrayAdapter
                mFiles.add(getEdoFile(f));
            }


        }

        return mFiles;
    }

    @Override
    public LFile getEdoFile(Object file) {
        if(file!=null) {
            File f = (File) file;

            LFile edoFile = LFile.newLocalFile(null, null, f.getPath(), null, null);
            if (f.isDirectory()) {
                edoFile.setType(LFile.FILE_TYPE_EDOFOLDER);
                edoFile.setThumbnail(getThumbUriForFolderPreview(f));
            }
            edoFile.setIsSelected(isSelected(edoFile));
            edoFile.setmDate(f.lastModified());
            return edoFile;
        }
        return null;
    }

    @Override
    public LFile getParentFolder(LFile currentFolder) {
        return null;
    }

    @Override
    public String getBreadCrumbs(LFile currentFolder) {

        File mDirectory = (File) getFile(currentFolder);
        if(mDirectory!=null){
            return mDirectory.getName();
        }

        return c.getResources().getString(R.string.gallery);
    }

    @Override
    public Comparator<LFile> getFileComparator() {
        return new FileDateDescComparator();
    }





    Map<File, ArrayList<File>> mediaFoldersMap= Collections.synchronizedMap(new HashMap<File, ArrayList<File>>());
    ArrayList<File> mediaFolders = new ArrayList<File>();
    public ArrayList<File> getPathOfAllMedia(Activity activity) {
        ArrayList<File> mediaFolderList = new ArrayList<File>();
        String nameOfFile = null;
        Uri uri;
        Cursor cursor;
        int column_index;
        int column_displayname;
        int lastIndex;
        // absolutePathOfImages.clear();


        String[] projection = { MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME };

        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        cursor = activity.managedQuery(uri, projection, null, null, null);
        column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

        column_displayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);



        while (cursor.moveToNext()) {

            String absolutePathOfImage = cursor.getString(column_index);
            nameOfFile = cursor.getString(column_displayname);

            lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);

            lastIndex = lastIndex >= 0 ? lastIndex
                    : nameOfFile.length() - 1;

            try{
                String absolutePathOfFileWithoutFileName = absolutePathOfImage.substring(0, lastIndex);

                File folder = new File(absolutePathOfFileWithoutFileName);



                if(mediaFoldersMap.get(folder)==null){
                    mediaFoldersMap.put(folder, new ArrayList<File>());
                    mediaFolderList.add(folder);
                }

                if (absolutePathOfImage != null) {
                    ArrayList<File> files = mediaFoldersMap.get(folder);
                    File file = new File(absolutePathOfImage);
                    if(file.exists() && !file.isDirectory())
                        files.add(file);
                }
            }catch (Exception e){
            }


        }


        uri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        cursor = activity.managedQuery(uri, projection, null, null, null);
        column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

        column_displayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);

        while (cursor.moveToNext()) {

            String absolutePathOfVideo = cursor.getString(column_index);
            nameOfFile = cursor.getString(column_displayname);

            lastIndex = absolutePathOfVideo.lastIndexOf(nameOfFile);

            lastIndex = lastIndex >= 0 ? lastIndex
                    : nameOfFile.length() - 1;

            try{
                String absolutePathOfFileWithoutFileName = absolutePathOfVideo
                        .substring(0, lastIndex);

                File folder = new File(absolutePathOfFileWithoutFileName);

                if(mediaFoldersMap.get(folder)==null){
                    mediaFoldersMap.put(folder, new ArrayList<File>());
                    mediaFolderList.add(folder);
                }

                if (absolutePathOfVideo != null) {
                    ArrayList<File> files = mediaFoldersMap.get(folder);
                    File file = new File(absolutePathOfVideo);
                    if(file.exists() && !file.isDirectory())
                        files.add(file);
                }
            }catch (Exception e){
            }

        }


        return mediaFolderList;
    }


    private class FileDescComparator implements Comparator<File> {
        @Override
        public int compare(File f1, File f2) {
            if(f1 == f2) {
                return 0;
            }
            if(f1.isDirectory() && f2.isFile()) {
                // Show directories above files
                return -1;
            }
            if(f1.isFile() && f2.isDirectory()) {
                // Show files below directories
                return 1;
            }
            // Sort the directories alphabetically
            return new Date(f2.lastModified()).compareTo(new Date(f1.lastModified()));
        }
    }



    private String getThumbUriForFolderPreview(File folder){
        // Show the folder icon
        ArrayList<File> files = mediaFoldersMap.get(folder);
        try{
            Collections.sort(files, new FileDescComparator());
        }catch (Exception e){}
        return files!=null && files.size()>0? files.get(0).getPath():null;
    }
}
