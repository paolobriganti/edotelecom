package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.WebUtils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import io.edo.api.linkparser.LinkParser;
import io.edo.db.global.uibeans.ContactFileUIDAO;
import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.global.uibeans.MessageUIDAO;
import io.edo.db.global.uibeans.SpaceUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LSpaceDAO;
import io.edo.db.local.beans.support.LocalFiles;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoIntentManager;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;
import io.edo.utilities.TrackConstants;
import io.edo.views.ViewPagerPlus;

public abstract class Act_Forward extends FragmentActivity  implements Frag_ContactList.Callbacks, ViewPager.OnPageChangeListener{
    Activity c;
    ThisApp app;

    ViewPagerPlus mViewPager;
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;

    EdoProgressDialog loading;
    EdoUI edoUI;

    public static final int SECTION_SELECTION = 0;
    public static final int SECTION_SPACES = 1;

    ContactUIDAO contactDAO;

    ArrayList<LFile> filesToShare = new ArrayList<>();
    boolean isForward = false;

    String messageToShare = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_fromapppicker);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        c = this;
        app = ThisApp.getInstance(c);

        if(!app.isLoginComplete()){
            startActivity(new Intent(c, Act_00_SplashScreen.class));
            finish();
            return;
        }

        contactDAO = new ContactUIDAO(c);

        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());
//        mAppSectionsPagerAdapter.setContact(ThisApp.getInstance(c).getUserContact());

        mViewPager = (ViewPagerPlus) this.findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setSwipeable(false);

        loading = new EdoProgressDialog(this, R.string.contact, R.string.loading);
        edoUI = new EdoUI(c);

        if(!isOrganize()){
            getActionBar().setTitle(R.string.share);
//            getActionBar().setSubtitle(R.string.tapToShareOrLongPressToOrganize);
        }else{
            getActionBar().setTitle(R.string.Organize);
        }


        processExtras(getIntent());


        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));
    }

    public abstract boolean isOrganize();

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(position == SECTION_SELECTION){
            getActionBar().setTitle(R.string.share);
            mViewPager.setSwipeable(false);
        }else if(position == SECTION_SPACES){
            getActionBar().setTitle(R.string.Organize);
            mViewPager.setSwipeable(true);
        }

        invalidateOptionsMenu();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {
        Frag_ContactOrganizer frag_contactOrganizer;

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case SECTION_SPACES:
                    Bundle arguments2 = new Bundle();
//                    arguments2.putSerializable(DBConstants.CONTAINER, contact);
                    arguments2.putBoolean(Frag_ContactOrganizer.EXTRA_SELECT_FOLDER, true);
                    frag_contactOrganizer = new Frag_ContactOrganizer();
                    frag_contactOrganizer.setArguments(arguments2);
                    return frag_contactOrganizer;

                default:
                    final Frag_ContactList fragment = new Frag_ContactList();
                    Bundle arguments = new Bundle();
                    arguments.putSerializable(Frag_ContactList.EXTRA_SELECT_FOR_ADD, true);
                    fragment.setArguments(arguments);
                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Section " + (position + 1);
        }


        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }


        public void setContact(LContact contact){
            this.frag_contactOrganizer.setContact(contact);
        }

        public LContact getCurrentContact(){
            return this.frag_contactOrganizer.getCurrentContact();
        }

        public LSpace getCurrentSpace(){
            return this.frag_contactOrganizer.getCurrentSpace();
        }
    }


    @Override
    public void onCreateContactButtonClick() {
    }

    @Override
    public void onCreateGroupButtonClick() {

    }

    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		PROCESS EXTRAS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    void processExtras(Intent intent){
        // Get intent, action and MIME type
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            app.sendEventTrack(isOrganize() ? TrackConstants.EVENT_ORGANIZE_FROM_TPA : TrackConstants.EVENT_SHARE_FROM_TPA);

            if ("text/plain".equals(type)) {
                processText(intent); // Handle text being sent
            } else {
                processFile(intent); // Handle single file being sent
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            app.sendEventTrack(isOrganize() ? TrackConstants.EVENT_ORGANIZE_FROM_TPA : TrackConstants.EVENT_SHARE_FROM_TPA);

            processFiles(intent); // Handle multiple files being sent

        } else if (Constants.ACTION_SEND_MULTIPLE_BY_EDO.equals(action) && intent.hasExtra(Constants.EXTRA_FILES)) {
            app.sendEventTrack(isOrganize() ? TrackConstants.EVENT_ORGANIZE_FROM_EDO : TrackConstants.EVENT_SHARE_FROM_EDO);

            processEdoFiles(intent);
        } else {
            this.finish();
        }
    }




    void processText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            processPlainText(c,sharedText);
        }else{
            EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_LONG);
            Act_Forward.this.finish();
        }
    }

    void processPlainText(Context c, String plainText){
        new TextTask(c,plainText).execute();
    }

    class TextTask extends AsyncTask<String, String, ArrayList<LFile>> {

        Context c;
        String plainText;

        public TextTask(Context c, String plainText){

            this.c = c;
            this.plainText = plainText;
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            loading.setTitle(R.string.loading);
            loading.setSubTitle(R.string.downloadInProgress);
            loading.show();

        }

        @Override
        protected ArrayList<LFile> doInBackground(String... params) {
            ArrayList<LFile> files = new ArrayList<>();

            if(JavaUtils.isWebUrl(plainText)){
                LFile file = LinkParser.getThumbnailAndNameOfWebLink(c,plainText);

                if(file!=null){
                    files.add(file);
                }
            }else{
                ArrayList<String> links = WebUtils.getLinks(plainText);
                if(links!=null && links.size()>0){
                    for(String link: links){
                        LFile file = LinkParser.getThumbnailAndNameOfWebLink(c,link);
                        if(file!=null){
                            files.add(file);
                        }
                    }
                }else{
                    messageToShare = plainText;
                }
            }



            return files;
        }

        @Override
        protected void onPostExecute(ArrayList<LFile> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if(result!=null && result.size()>0){
                filesToShare.clear();
                filesToShare.addAll(result);
            }else if(JavaUtils.isEmpty(messageToShare)){
                EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_LONG);
                Act_Forward.this.finish();
            }

            loading.dismiss();

        }

    }





    void processFile(Intent intent) {
        Uri uri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (uri != null) {
            ArrayList<Uri> uris = new ArrayList<>();
            uris.add(uri);
            processUris(c,intent,uris);
        }else{
            EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_LONG);
            Act_Forward.this.finish();
        }
    }

    void processFiles(Intent intent) {
        ArrayList<Uri> uris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (uris != null) {
            processUris(c,intent,uris);
        }else{
            EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_LONG);
            Act_Forward.this.finish();
        }
    }

    void processUris(Context c, Intent intent, ArrayList<Uri> uris){
        new UriTask(c,intent,uris).execute();
    }

    class UriTask extends AsyncTask<String, String, ArrayList<LFile>> {

        Context c;
        Intent intent;
        ArrayList<Uri> uris;

        public UriTask(Context c, Intent intent, ArrayList<Uri> uris){

            this.c = c;
            this.intent = intent;
            this.uris = uris;
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            loading.setTitle(R.string.loading);
            loading.setSubTitle(R.string.downloadInProgress);
            loading.show();

        }

        @Override
        protected ArrayList<LFile> doInBackground(String... params) {
            ArrayList<LFile> files = new ArrayList<>();

            if (uris != null && uris.size()>0) {
                for(int i=0; i<uris.size(); i++){
                    String filePath = FileInfo.getRealPathFromURI(c, uris.get(i));
                    File file = null;
                    try{
                        file = new File(filePath);
                    }catch(Exception e){}

                    if(filePath!=null && file!=null && file.exists()){
                        String mime = FileInfo.getMime(filePath);
                        LFile efile = LFile.newLocalFile(null,null,filePath,mime,app.getEdoUserId());
                        files.add(efile);
                    }else{
                        filePath = FileInfo.createFileFromUri(c, intent, uris.get(i), Constants.getExternalCacheDir(c).getPath(), i+"_"+System.currentTimeMillis());
                        if(filePath!=null && file!=null && file.exists()) {
                            String mime = FileInfo.getMime(filePath);
                            LFile efile = LFile.newLocalFile(null, null, filePath, mime, app.getEdoUserId());
                            files.add(efile);
                        }
                    }
                }

            }
            return files;
        }

        @Override
        protected void onPostExecute(ArrayList<LFile> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if(result!=null && result.size()>0){
                filesToShare.clear();
                filesToShare.addAll(result);
            }else{
                EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_LONG);
                Act_Forward.this.finish();
            }

            loading.dismiss();

        }

    }


    void processEdoFiles(Intent intent) {
        isForward = true;
        Serializable filesSerial = intent.getSerializableExtra(Constants.EXTRA_FILES);
        if(filesSerial!=null){
            LocalFiles efiles = (LocalFiles) filesSerial;
            filesToShare = efiles.getFiles();
        }else{
            EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_LONG);
            Act_Forward.this.finish();
        }
    }









    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		SEND FILE
    //*****************************************************************************************************************************************************************************************************************************************************************************************************


    @Override
    public void onItemSelected(LContact contact) {
        if(!isOrganize()) {
            if (contact != null) {
                if (contact.getStatus() == LContact.STATUS_CREATION_IN_PROGRESS && !JavaUtils.isEmailAddress(contact.getPrincipalEmailAddress())) {
                    edoUI.showToastLong(c.getResources().getString(R.string.wrongEmailAddress), null);
                    return;
                }
                saveToDefaultSpace(contact);
            }
        }else{
            onItemLongClick(contact);
        }
    }

    @Override
    public void onItemLongClick(LContact contact) {
        if(contact!=null){

            if(contact.isPersonal(c) || contact.isActive() || contact.isGroup()){
                mAppSectionsPagerAdapter.setContact(contact);
                mViewPager.setCurrentItem(SECTION_SPACES);
            }else{
                if(contact.getStatus()==LContact.STATUS_CREATION_IN_PROGRESS && !JavaUtils.isEmailAddress(contact.getPrincipalEmailAddress())){
                    edoUI.showToastLong(c.getResources().getString(R.string.wrongEmailAddress), null);
                    return;
                }
                saveToDefaultSpace(contact);
            }
        }
    }


    public void saveToDefaultSpace(final LContact contact){
        if(contact==null)
            return;

        new Thread(new Runnable() {
            @Override
            public void run() {


                LSpace defaultSpace = new LSpaceDAO(c).getContactDefaultSpace(contact);


                if(defaultSpace==null) {
                    c.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if(!contact.isPersonal(c)) {
                                final EdoDialog dialog = new EdoDialog(c);

                                dialog.setTitle(contact.getPrincipalEmailAddress());
                                dialog.setText(c.getResources().getString(R.string.startSharingWithContactName).replace(Constants.PH_CONTACTNAME, contact.getNameSurname()));
                                dialog.setPositiveButtonText(R.string.letsStart);

                                dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        createDefaultSpaceAndSave(contact);

                                        dialog.dismiss();
                                    }
                                });

                                dialog.show();
                            }else{
                                createDefaultSpaceAndSave(contact);
                            }
                        }
                    });
                }else{
                    saveTo(contact,defaultSpace);
                }
            }
        }).start();
    }


    public void createDefaultSpaceAndSave(final LContact contact){
        new Thread() {
            @Override
            public void run() {
                Looper.myLooper();

                runOnUiThread(new Runnable() {
                    public void run() {
                        loading.setTitle(contact.getNameSurname());
                        loading.setSubTitle(R.string.loading);
                        loading.show();
                    }
                });

                LSpace defaultSpace = new SpaceUIDAO(c).initDefaultSpace(contact);

                if (defaultSpace != null) {

                    saveTo(contact,defaultSpace);

                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            edoUI.showToastLong(c.getResources().getString(R.string.errorRetry), null);
                        }
                    });
                }

                loading.dismiss();

            }
        }.start();
    }



    public void saveTo(final LContact contact, final LSpace space){
        if(space==null){
            edoUI.showToastLong(R.string.pleaseSelectASpace,null);
        }

        if(contact==null || space==null){
            return;
        }

        new Thread() {
            @Override
            public void run() {
                Looper.myLooper();

                runOnUiThread(new Runnable() {
                    public void run() {
                        loading.setTitle(contact.getNameSurname());
                        loading.setSubTitle(R.string.sharingFiles);
                        loading.show();
                    }
                });
                ContactFileUIDAO contactFileUIDAO = new ContactFileUIDAO(c);

                if(JavaUtils.isNotEmpty(messageToShare)){

                    if(space.isDefault()){
                        MessageUIDAO messageUIDAO = new MessageUIDAO(c);
                        messageUIDAO.sendMessage(contact,messageToShare);
                    }else{
                        LFile file = LFile.newSnippetFile(null,null,messageToShare,app.getEdoUserId());
                        contactFileUIDAO.add(contact,space,file);
                    }

                }else if(filesToShare!=null && filesToShare.size()>0){

                    for(LFile file: filesToShare){
                            if(!isForward){
                                contactFileUIDAO.add(contact,space,file);
                            }else{
                                contactFileUIDAO.copyFileToContact(contact,space,file.getId(),file);
                            }
                    }
                }

                if(space.isDefault()){
                    goToContact(contact, null);
                }else{
                    goToContact(contact, space);
                }



                loading.dismiss();

            }
        }.start();
    }


    public void goToContact(final LContact contact, final LSpace space){
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(space==null){
                    c.startActivity(EdoIntentManager.getContactsIntent(c, contact));
                }else{
                    c.startActivity(EdoIntentManager.getContactsIntent(c, contact, space));
                }

                Act_Forward.this.finish();
            }
        });

    }









    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contacteditor, menu);

        MenuItem post = menu.findItem(R.id.post);
        if(mViewPager!=null)
            post.setVisible(mViewPager.getCurrentItem()==SECTION_SPACES);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.post:
                saveTo(mAppSectionsPagerAdapter.getCurrentContact(),mAppSectionsPagerAdapter.getCurrentSpace());
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };
}
