package io.edo.edomobile;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LSpaceDAO;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.FilePickers;

/**
 * A fragment representing a single Contact detail screen.
 * This fragment is either contained in a {@link Act_ContactActiveList}
 * in two-pane mode (on tablets) or a {@link Act_ContactDetail}
 * on handsets.
 */
public class Frag_ContactOrganizer extends Fragment{
    Activity c;
    ThisApp app;

    private LContact contact;

    public FilePickers filesPicker;

    public EdoProgressDialog loading;

    public static final String EXTRA_SECTION = "organizeExtraSection";
    public static final int SECTION_INBOX = -1;
    public static final int SECTION_SPACES = 0;
    public static final int SECTION_NOTES = 1;


    boolean selectFolder;
    public static final String EXTRA_SELECT_FOLDER = "selectFolder";

    LSpace currentSpace;

    public Frag_ContactOrganizer() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        c = getActivity();
        app = ThisApp.getInstance(c);

        if (getArguments().containsKey(DBConstants.CONTAINER)) {
            Serializable contactSerial = getArguments().getSerializable(DBConstants.CONTAINER);
            if(contactSerial!=null) {
                contact = (LContact) contactSerial;
            }else{
                contact = app.getUserContact();
            }
        }else{
            contact = app.getUserContact();
        }

        if (getArguments().containsKey(EXTRA_SELECT_FOLDER)) {
            selectFolder = getArguments().getBoolean(EXTRA_SELECT_FOLDER, false);
        }


        loading = new EdoProgressDialog(c, R.string.loading, R.string.loading);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_sections, container, false);

//        setBackPressed(rootView);

        if(getArguments().containsKey(DBConstants.SPACE)){
            Serializable spaceSerial = getArguments().getSerializable(DBConstants.SPACE);
            if(spaceSerial!=null){
                setSpace((LSpace)spaceSerial);
                return rootView;
            }
        }

        int sectionsSection = SECTION_SPACES;
        if (getArguments().containsKey(EXTRA_SECTION)) {
            sectionsSection = getArguments().getInt(EXTRA_SECTION, SECTION_SPACES);
        }
        switch (sectionsSection){
            case SECTION_INBOX:
                setSectionInbox();
                break;
            default:
                setSpaceFragment();
                break;
        }

        return rootView;
    }

    public void setContact(LContact contact){
        this.contact = contact;
        this.setSpaceFragment();
    }
    public LContact getCurrentContact(){
        return this.contact;
    }

    public void setSpaceFragment(){
        Bundle arguments = new Bundle();
        arguments.putSerializable(DBConstants.CONTAINER, contact);
        final Frag_ContactSpaces fragment = new Frag_ContactSpaces();
        fragment.setArguments(arguments);
        fragment.setFilesPicker(filesPicker);
        fragment.setSpaceSelectedListener(new Frag_ContactSpaces.SpaceSelectedListener() {
            @Override
            public void onSpaceSelect(View v, LSpace space) {
                setFilesFragment(space, false);
            }
        });
        getChildFragmentManager().beginTransaction()
                .replace(R.id.section_container, fragment)
                .commit();
    }

    public void setSectionInbox(){
        LSpace space = new LSpaceDAO(c).getContactDefaultSpace(contact);
        setFilesFragment(space, true);
    }

    public void setSpace(LSpace space){
        setFilesFragment(space, false);
    }

    public void setFilesFragment(LSpace space, boolean isInbox){
        Bundle arguments = new Bundle();
        arguments.putSerializable(DBConstants.CONTAINER, contact);
        arguments.putSerializable(DBConstants.SPACE, space);
        arguments.putBoolean(EXTRA_SELECT_FOLDER, selectFolder);
        arguments.putSerializable(DBConstants.CONTAINER, contact);
        arguments.putSerializable(Frag_ContactSpaceFiles.EXTRA_IS_INBOX, isInbox);
        final Frag_ContactSpaceFiles fragment = new Frag_ContactSpaceFiles();
        fragment.setArguments(arguments);
        fragment.setFilesPicker(filesPicker);
        fragment.setLevelUpFinishListener(new Frag_ContactSpaceFiles.LevelUpFinishListener() {
            @Override
            public void onLevelUpFinished(View v) {
                setSpaceFragment();
            }
        });
        fragment.setOnSpaceSelectedListener(new Frag_ContactSpaceFiles.OnSpaceSelectedListener() {
            @Override
            public void onSpaceSelected(LSpace space) {
                currentSpace = space;
            }
        });

        getChildFragmentManager().beginTransaction()
                .replace(R.id.section_container, fragment)
                .commit();
    }

    public LSpace getCurrentSpace(){
        return currentSpace;
    }



    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void setFilesPicker(FilePickers filesPicker){
        this.filesPicker = filesPicker;
    }

//    public void setBackPressed(View v){
//        v.setFocusableInTouchMode(true);
//        v.requestFocus();
//        v.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK) {
//
////                    if (mViewPager.getCurrentItem()!=0) {
////                        mViewPager.setCurrentItem(mViewPager.getCurrentItem()-1);
////                        return true;
////                    }
//                }
//                return false;
//            }
//        } );
//    }


}
