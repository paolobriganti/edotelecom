package io.edo.edomobile;

import android.app.Activity;
import android.app.Service;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.paolobriganti.android.VU;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import io.edo.api.TransferOperation;
import io.edo.db.local.LocalDBManager;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.LUser;
import io.edo.db.local.beans.LUserDAO;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.web.beans.WStatus;
import io.edo.db.web.request.EdoRequest;
import io.edo.edomobile.Service_DwnUpl.AsyncTaskManager;
import io.edo.ui.UIManager;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.ui.imageutils.FilePageLoader;
import io.edo.ui.imageutils.ThumbnailActivityLoader;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.ui.imageutils.ThumbnailSpaceLoader;
import io.edo.utilities.ObjectId;
import io.edo.utilities.TrackConstants;
import io.fabric.sdk.android.Fabric;

public class ThisApp extends android.support.multidex.MultiDexApplication{


	public static ThisApp getInstance(Context c){
		return (ThisApp) c.getApplicationContext();
	}

	public static ThisApp getInstance(Activity activity){
		return (ThisApp) activity.getApplication();
	}

	public static ThisApp getInstance(Service service){
		return (ThisApp) service.getApplication();
	}






	//*******************************
	//* DATABASE
	//*******************************

	private LocalDBManager db;
	public boolean isDBUpdatingInProgress = false;

	public LocalDBManager getDB() {
		if(db==null)
			startDB();
		return db;
	}

	public void startDB() {
		db = new LocalDBManager(this, true);
	}


	//*******************************
	//* UserInfo
	//*******************************
	String alias = null;
	private LUser user;

	public void setUser(LUser user){
		this.user = user;

		this.identifyUser(user);

	}

	public String getEdoUserId(){
		if(user!=null){
			return user.getId();
		}
		return null;
	}

	public String getUserName(){
		if(user!=null){
			return user.getName();
		}
		return null;
	}

	public String getUserNameSurname(){
		if(user!=null){
			return user.getUserNameSurname();
		}
		return null;
	}

	public String getUserEmail(){
		if(user!=null){
			return user.getLoginEmail();
		}
		return null;
	}

	public boolean isLoginComplete(){
		if(user!=null){
			return user.isLoginComplete();
		}
		return false;
	}

	public LContact getUserContact(){
		if(user!=null){
			LContact contact = user.getUserContact();
			contact.setName(getApplicationContext().getResources().getString(R.string.you));
			contact.setSurname("");
			return contact;
		}
		return null;
	}

	public WStatus getNotificationStatus(){
		if(user!=null){
			return user.getWStatus();
		}
		return null;
	}

	public boolean saveNotificationStatus(WStatus status){
		if(user!=null){
			user.setNotificationStatus(status);
			return new LUserDAO(this).updateNotificationStatus(user.getId(), user.getNotificationStatus());
		}
		return false;
	}


	public int getLoginStatus(){
		if(user!=null){
			return user.getLoginStatus();
		}
		return LUser.LOGIN_0_NOT_EXECUTED;
	}

	public boolean saveLoginStatus(int loginStatus){
		if(user!=null){
			user.setLoginStatus(loginStatus);
			new LUserDAO(this).updateLoginStatus(user.getId(), user.getLoginStatus());
		}
		return false;
	}


	public String getEdoToken(){
		if(user!=null){
			return user.getEdoToken();
		}
		return null;
	}

	public String getEdoUserHeaderParam(){
		if(user!=null){
			return user.getEdoUserHeaderParam();
		}
		return null;
	}


	public String getDeviceTypeHeaderParam(){
		if(user!=null){
			return user.getDeviceTypeHeaderParam(this);
		}
		return null;
	}


	public long getLastTokenUpdate(){
		if(user!=null){
			return user.getLastTokenUpdate();
		}
		return 0;
	}

	public boolean saveEdoToken(String edoToken){
		if(user!=null){
			user.setEdoToken(edoToken);
			user.setLastTokenUpdate(System.currentTimeMillis());
			new LUserDAO(this).updateEdoToken(user.getId(), user.getEdoToken());
		}
		return false;
	}

	//*******************************
	//* CURRENT
	//*******************************
//	public boolean isTabsFragmentForeground = false;
	private String currentTimelineContainerId = null;
	public void setCurrentTimelineContainerId(EdoContainer container){
		if(container != null){
			currentTimelineContainerId = new String(container.getContainerId());
		}else
			currentTimelineContainerId = null;
	}

	public String getTimelineCurrentContainerId() {
		return currentTimelineContainerId;
	}
	//	public LContact currentChatContact = null;
//	public LFile currentFile = null;


	//*******************************
	//* CONTACTS
	//*******************************
	private ArrayList<LContact> activeContacts = new ArrayList<LContact>();
	public ArrayList<LContact> getActiveContacts(){
		if(activeContacts==null || activeContacts.size()<=0){
			activeContacts = new LContactDAO(this).getActiveList();
		}
		return activeContacts;
	}
	public void clearContacts(){
		if(activeContacts!=null){
			activeContacts.clear();
		}
	}








	//*******************************
	//* Login
	//*******************************
	public boolean isLoginDownloadDataInProgress = false;



	//*******************************
	//* Transfer Operations
	//*******************************
	private int transferServiceOperationsCount = 0;
	public Map<String, TransferOperation> failedOperations=Collections.synchronizedMap(new HashMap<String,TransferOperation>());
	public AsyncTaskManager asyncTaskMng = null;

	public int getTransferServiceOperationsCount() {
		return transferServiceOperationsCount;
	}


	public void setTransferServiceOperationsCount(int transferServiceOperationsCount) {
//		this.transferServiceOperationsCount = transferServiceOperationsCount;
//		EdoUIUpdate uiUpdate = new EdoUIUpdate(null, EdoUIUpdate.EVENT_TRANSFER_OPERATION,
//				transferServiceOperationsCount>0 ? EdoUIUpdate.STATUS_IN_PROGRESS : EdoUIUpdate.STATUS_COMPLETE,
//						null, null, null);
//        sendUIUpdate(null, uiUpdate);
	}





	//*******************************
	//* Notes and Chat
	//*******************************
	public boolean isNotesUIOpen = false;
	public boolean isChatOpen = false;



	//*******************************
	//* Notifications
	//*******************************
	public long lastNotificationWithSound = 0L;


	//*******************************
	//* Thumbnails Cache
	//*******************************
	public ThumbnailFileLoader thumbnailFileLoader;
	public ThumbnailActivityLoader thumbnailActivityLoader;
	public ThumbnailSpaceLoader thumbnailSpaceLoader;
	public ContactPhotoLoader contactPhotoLoader;
	public FilePageLoader filePageLoader;




	//*******************************
	//* UI Updates
	//*******************************
	public UIManager uiManager = new UIManager();

	public boolean isContactsUpdateInProgress = false;
	public boolean isUpdateInProgress = false;




















	//*******************************
	//* ANALYTICS
	//*******************************
	boolean timelineSeen = false;
	boolean spacesSeen = false;
	boolean notesSeen = false;


	public static final String PROJECT_TOKEN = "3646e86425c2a8fb9c3eebb0f393f243";
	MixpanelAPI analytics;


	private void identifyUser(LUser user){
		if(analytics==null)
			return;

		if(user==null){
			alias = new ObjectId().toString();
			analytics.alias(alias,null);
		}else{
			try {

				analytics.getPeople().identify(user.getId());

				JSONObject props = new JSONObject();
				props.put("$first_name", user.getName());
				props.put("$last_name", user.getSurname());
				props.put("$email", user.getLoginEmail());

				TimeZone tz = TimeZone.getTimeZone("UTC");
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
				df.setTimeZone(tz);
				String dateAsISO = df.format(new ObjectId(user.getId()).getDate());
				props.put("$created", dateAsISO);

				analytics.getPeople().set(props);
				if(alias!=null){
					analytics.alias(user.getId(),alias);
				}
				analytics.identify(user.getId());



			}catch (JSONException e){}
		}
	}

	public void sendEventTrack(String eventName){
		sendEventTrack(eventName,null,null);
	}

	public void sendEventTrack(String eventName, String actionName){
		sendEventTrack(eventName,TrackConstants.KEY_ACTION, actionName);
	}

	public void sendEventTrack(String eventName, String newPropKey, Object newPropValue){
		if(analytics==null)
			return;

		if(newPropKey!=null && newPropValue!=null) {
			JSONObject props = new JSONObject();
			try {
				props.put(newPropKey, newPropValue);

				analytics.track(eventName, props);
			} catch (JSONException e) {
			}
		}else{
			analytics.track(eventName);
		}
	}

	public void startTimeOf(String event){
		if(analytics==null)
			return;

		analytics.timeEvent(event);
	}

	public void sendEventTrack(String eventName, HashMap<String,Object> properties){
		if(analytics==null)
			return;

		if(properties!=null) {
			JSONObject props = new JSONObject();
			try {
				for(String key:properties.keySet()){
					Object value = properties.get(key);
					props.put(key, value);
				}
				analytics.track(eventName, props);
			} catch (JSONException e) {
			}
		}else{
			analytics.track(eventName);
		}
	}



















	//*******************************
	//* App Life cycle
	//*******************************

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		VU.overrideFont(getApplicationContext(), "SERIF", "fonts/FontName.ttf");

		if(!EdoRequest.IS_TEST_APP)
			Fabric.with(this, new Crashlytics());

		// Get tracker.
		if(!EdoRequest.IS_TEST_APP){
			analytics = MixpanelAPI.getInstance(this,PROJECT_TOKEN);
		}



		startDB();
		setUser(new LUserDAO(this).getPrincipalUser());




	}


	@Override
	public void onTerminate() {
		// TODO Auto-generated method stub
		super.onTerminate();
		db.close();


	}
}
