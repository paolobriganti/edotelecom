package io.edo.edomobile;

import android.os.Bundle;

import com.google.api.client.util.DateTime;
import com.google.api.services.drive.model.File;
import com.paolobriganti.android.ASI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import io.edo.api.google.GoogleDrive;
import io.edo.api.google.GoogleResponse;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.LServiceDAO;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoUI;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.ui.imageutils.ThumbnailFilePickerGoogleDriveLoader;

/**
 * Created by PaoloBriganti on 12/06/15.
 */
public class Frag_FilesPickerGoogleDrive extends Frag_Base_FilePicker {


    protected boolean mShowHiddenFiles = false;
    protected String[] acceptedFileExtensions;
    protected String[] acceptedFileTypes;

    private final static String DEFAULT_INITIAL_DIRECTORY = "root";

    private ThumbnailFilePickerGoogleDriveLoader imageLoader;

    private String serviceId;
    GoogleDrive drive;

    public Map<String, ArrayList<File>> foldersFiles= Collections.synchronizedMap(new HashMap<String, ArrayList<File>>());
    public Map<String, com.google.api.services.drive.model.File> folders=Collections.synchronizedMap(new HashMap<String, com.google.api.services.drive.model.File>());


    public Frag_FilesPickerGoogleDrive() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        boolean isInternetOn = !EdoUI.dialogNoInternet(c,
                new Runnable() {public void run() {
                    c.finish();
                    c.startActivity(c.getIntent());
                }},
                null);
        if(isInternetOn){

            if(getArguments().containsKey(Act_FilePicker.EXTRA_SHOW_HIDDEN_FILES)) {
                mShowHiddenFiles = getArguments().getBoolean(Act_FilePicker.EXTRA_SHOW_HIDDEN_FILES, false);
            }

            if(getArguments().containsKey(Act_FilePicker.EXTRA_ACCEPTED_FILE_EXTENSIONS)) {
                ArrayList<String> collection = getArguments().getStringArrayList(Act_FilePicker.EXTRA_ACCEPTED_FILE_EXTENSIONS);
                acceptedFileExtensions = collection.toArray(new String[collection.size()]);
            }
            if(getArguments().containsKey(Act_FilePicker.EXTRA_ACCEPTED_FILE_TYPES)) {
                ArrayList<String> collection = getArguments().getStringArrayList(Act_FilePicker.EXTRA_ACCEPTED_FILE_TYPES);
                acceptedFileTypes = collection.toArray(new String[collection.size()]);
            }

            if(getArguments().containsKey(DBConstants.SERVICE_ID)) {
                serviceId = getArguments().getString(DBConstants.SERVICE_ID);
            }else{
                c.finish();
                return;
            }

            imageLoader = new ThumbnailFilePickerGoogleDriveLoader(c);

        }


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public int getContentLayoutResourceId() {
        return R.layout.fragment_files_picker;
    }

    @Override
    public ThumbnailFileLoader getThumbnailFileLoader() {
        return imageLoader;
    }

    @Override
    public LSpace getSpace() {
        LSpace space = new LSpace();
        return space;
    }


    @Override
    public ArrayList<LFile> getFiles(LFile efolder) {

        if(!ASI.isInternetOn(c))
            return null;

        if(drive==null){
            LServiceDAO lsDao = new LServiceDAO(c);
            LService service = lsDao.get(serviceId);
            if(service!=null)
                drive = new GoogleDrive(c, service);

            if(drive==null)
                return null;

            GoogleResponse response = drive.getAllFilesOfFolder(null);
            if(response!=null){
                ArrayList<File> folderFiles = (ArrayList<File>) response.result;
                if(folderFiles!=null && folderFiles.size()>0)
                    foldersFiles.put(DEFAULT_INITIAL_DIRECTORY, new ArrayList<File>(folderFiles));
            }
        }



        File folder = (File) getFile(efolder);
        ArrayList<File> folderFiles = new ArrayList<File>();
        if(foldersFiles!=null && foldersFiles.size()>0){
            if(folder!=null){
                folders.put(folder.getId().toString(), folder);
                ArrayList<File> files = foldersFiles.get(folder.getId().toString());
                if(files!=null && files.size()>0){
                    folderFiles.addAll(files);
                }
            }else{
                ArrayList<File> files = foldersFiles.get(DEFAULT_INITIAL_DIRECTORY);
                if(files!=null && files.size()>0){
                    folderFiles.addAll(files);
                }

            }
        }

        if(folder!=null && folderFiles.size()==0){

            GoogleResponse response = drive.getAllFilesOfFolder(folder.getId().toString());
            if(response!=null){
                folderFiles = (ArrayList<File>) response.result;
                if(folderFiles!=null && folderFiles.size()>0){
                    foldersFiles.put(folder.getId().toString(), folderFiles);
                }
            }
        }

        if(folderFiles!=null && folderFiles.size()>0){
            ArrayList<LFile> files = new ArrayList<LFile>();
            for(File file:folderFiles){

//                ParentReferencse parentReference = new ParentReference();
//                if(folder!=null)
//                    parentReference.setId(folder.getId());
//                else
//                    parentReference.setIsRoot(true);
//                ArrayList<ParentReference> parents = new ArrayList<ParentReference>();
//                parents.add(parentReference);
//                file.setParents(parents);

                files.add(getEdoFile(file));
            }
            return files;
        }


        return null;
    }

    @Override
    public LFile getEdoFile(Object file) {
        if(file!=null) {
            File f = (File) file;

            LFile edoFile = new LFile(null,f,null);
            if (f.getMimeType().equalsIgnoreCase(GoogleDrive.MIME_TYPE_FOLDER)) {
                edoFile.setType(LFile.FILE_TYPE_EDOFOLDER);
            }
            edoFile.setIsSelected(isSelected(edoFile));
            edoFile.setmDate(f.getModifiedDate().getValue());
            return edoFile;
        }
        return null;
    }

    @Override
    public Object getFile(LFile edoFile) {
        if(edoFile!=null){

            File file = new File();
            file.setId(edoFile.getCloudId());
            file.setAlternateLink(edoFile.getWebUri());
            file.setTitle(edoFile.getNameWithExt());
            file.setMimeType(edoFile.getType());
            file.setModifiedByMeDate(new DateTime(edoFile.getmDate()));
            file.setThumbnailLink(edoFile.getThumbnail());

            return file;
        }

        return null;
    }

    @Override
    public String getUniqueKeyForFile(LFile edoFile) {
        return edoFile.getCloudId();
    }


    @Override
    public LFile getParentFolder(LFile currentFolder) {
        File mDirectory = (File) getFile(currentFolder);
        if(mDirectory != null
                && mDirectory.getParents()!=null
                && mDirectory.getParents().size()>0
                && !mDirectory.getParents().get(0).getIsRoot()) {

            return getEdoFile(folders.get(mDirectory.getParents().get(0).getId()));
        }

        return null;
    }

    @Override
    public String getBreadCrumbs(LFile currentFolder) {
        File mDirectory = (File) getFile(currentFolder);
        if(mDirectory!=null){
            return mDirectory.getTitle();
        }

        return c.getResources().getString(R.string.googleDrive);
    }

    @Override
    public Comparator<LFile> getFileComparator() {
        return new FileFolderFirstComparator();
    }



//    private class FileComparator implements Comparator<File> {
//        @Override
//        public int compare(File f1, File f2) {
//            if(f1 == f2) {
//                return 0;
//            }
//            if(f1.isDirectory() && f2.isFile()) {
//                // Show directories above files
//                return -1;
//            }
//            if(f1.isFile() && f2.isDirectory()) {
//                // Show files below directories
//                return 1;
//            }
//            // Sort the directories alphabetically
//            return f1.getName().compareToIgnoreCase(f2.getName());
//        }
//    }
}
