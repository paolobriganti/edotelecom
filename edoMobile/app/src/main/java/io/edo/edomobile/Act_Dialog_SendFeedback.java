package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import com.google.gson.Gson;
import com.paolobriganti.android.AUI;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.LocalDBManager;
import io.edo.db.web.request.EdoRequest;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;

public class Act_Dialog_SendFeedback extends Activity {
	public ThisApp app;
	
	Activity c;
	LocalDBManager db;
	EdoUI edoUI;

	EditText et_text;



	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		AUI.setStatusBarColor(this, this.getResources().getColor(R.color.edo_blue_dark));
		EdoUI.setOrientationTo(this);
		
		c = this;
		app = ThisApp.getInstance(this);
		db = app.getDB();	
		
		edoUI = new EdoUI(c);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		overridePendingTransition(0,0);
		setContentView(R.layout.dialog_sendfeedback);

//		WindowManager.LayoutParams params = getWindow().getAttributes();  
//		params.width = this.getResources().getDisplayMetrics().widthPixels;
//		params.height = this.getResources().getDisplayMetrics().heightPixels;
//		this.getWindow().setAttributes(params); 

		registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));

		et_text = (EditText) findViewById(R.id.et_text);
	}


	@Override
	public void onResume() {
		super.onResume();

	}


	@Override
	public void onPause() {
		super.onPause();
	}


	public void bt_send(View v){
		boolean isInternetOn = !EdoUI.dialogNoInternet(c,
				new Runnable() {public void run() {
					bt_send(null);
				}}, 
				null);

		if(isInternetOn){
			final String text = et_text.getText().toString();
			if(JavaUtils.isNotEmpty(text)){
				new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
				Feedback feed = new Feedback(text);
				String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_FEEDBACK;
				EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, feed.toJson());
				request.send(c);
				}}.start();
			}else{
				edoUI.showToastLong(c.getResources().getString(R.string.noFeedbackSent), null);
			}
			finish();
		}
	}

	class Feedback{
		String message;

		public Feedback(String message) {
			super();
			this.message = message;
		}

		public String toJson(){
			return new Gson().toJson(this);
		}
	}

	public void bt_cancel(View v){
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mKillReceiver);
	}
	private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, final Intent intent) {
			finish();
		}
	};
}