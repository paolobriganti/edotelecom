package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;

import com.paolobriganti.android.VU;

import java.io.Serializable;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 12/06/15.
 */
public class Act_ContactFileFilter extends Act_SearchFileBase {

	public final static String EXTRA_FILTER_TYPE = "filterType";
	String filterType;


	Activity c;
	ThisApp app;

	LContact contact;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_contactfilefilter);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		getActionBar().setTitle(R.string.files);

		c = this;
		app = ThisApp.getInstance(c);

		if (savedInstanceState == null) {
			Intent intent = getIntent();

			if (intent.getExtras().containsKey(DBConstants.CONTAINER)) {
				Serializable contactSerial = intent.getSerializableExtra(DBConstants.CONTAINER);
				if(contactSerial!=null) {
					contact = (LContact) contactSerial;
				}else{
					contact = app.getUserContact();
				}
			}else{
				contact = app.getUserContact();
			}

			String title = contact.getNameComplete() + VU.getColoredTextString(c, R.color.lime, VU.getBoldTextString(" | " + c.getResources().getString(R.string.files)));
			c.getActionBar().setTitle(Html.fromHtml(title));

			Bundle arguments = new Bundle();
			if(getIntent().hasExtra(EXTRA_FILTER_TYPE))
				arguments.putString(EXTRA_FILTER_TYPE, getIntent().getStringExtra(EXTRA_FILTER_TYPE));

			arguments.putSerializable(DBConstants.CONTAINER, contact);
			arguments.putSerializable(Frag_ContactSpaceFiles.EXTRA_IS_INBOX, true);
			final Frag_ContactSpaceFiles fragment = new Frag_ContactSpaceFiles();
			fragment.setArguments(arguments);

			getSupportFragmentManager().beginTransaction()
					.replace(R.id.files_container, fragment)
					.commit();


		}


		registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));

	}



	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//*		ACTION BAR
	//*****************************************************************************************************************************************************************************************************************************************************************************************************

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.filesearch, menu);

		MenuItem searchItem = menu.findItem(R.id.searchFiles);
		searchItem.setVisible(true);
		SearchView searchView = (SearchView) searchItem.getActionView();
		setupSearchView(searchView, searchItem);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {

			case android.R.id.home:
				onBackPressed();
				break;

			default:
				break;

		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();

//		AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));


	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
	}
	private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, final Intent intent) {
			finish();
		}
	};

}
