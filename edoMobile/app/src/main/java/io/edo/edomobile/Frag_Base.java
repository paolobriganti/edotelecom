package io.edo.edomobile;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paolobriganti.android.AUtils;
import com.paolobriganti.utils.imageLoader.ImageListFragment;

import java.io.Serializable;
import java.util.ArrayList;

import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.global.uibeans.SpaceUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LSpaceDAO;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.FileOptions;
import io.edo.ui.FilePickers;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.ui.interfaces.HidingScrollListener;
import io.edo.ui.itemanimator.CustomItemAnimator;
import io.edo.utilities.Constants;
import io.edo.views.SlidingUpPanelLayout;

public abstract class Frag_Base extends ImageListFragment implements SwipeRefreshLayout.OnRefreshListener {
    public Activity c;
    public ThisApp app;


    public LContact contact;
    public SpaceUIDAO spaceDAO;
    public FileUIDAO fileDAO;


    public SwipeRefreshLayout srlayout;

    public RecyclerView mRecyclerView;
    public ProgressBar mProgressBar;

    public LinearLayout ll_blankSheet;
    public TextView tv_blankSheet;
    public ImageView iv_blankSheet;

    public LinearLayout ll_footer;
    public View fabButton;

    public SlidingUpPanelLayout slidingPickers;

    public FilePickers filesPicker;
    public FileOptions fileOptions;

    public EdoProgressDialog loading;

    public boolean selectFolder = false;



    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Frag_Base() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        c = getActivity();
        app = ThisApp.getInstance(c);

        if (getArguments()!=null && getArguments().containsKey(DBConstants.CONTAINER)) {
            Serializable contactSerial = getArguments().getSerializable(DBConstants.CONTAINER);
            if(contactSerial!=null) {
                contact = (LContact) contactSerial;
            }else{
                contact = app.getUserContact();
            }
        }else{
            contact = app.getUserContact();
        }

        if (getArguments().containsKey(Frag_ContactOrganizer.EXTRA_SELECT_FOLDER)) {
            selectFolder = getArguments().getBoolean(Frag_ContactOrganizer.EXTRA_SELECT_FOLDER, false);
        }

        spaceDAO = new SpaceUIDAO(c);
        fileDAO = new FileUIDAO(c);

        loading = new EdoProgressDialog(c, R.string.loading, R.string.loading);

        fileOptions = new FileOptions(c, getCurrentContainer());
        this.app.thumbnailFileLoader = ThumbnailFileLoader.getInstance(getActivity());

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(getContentViewResourceId(), container, false);

        ll_footer = (LinearLayout) rootView.findViewById(R.id.ll_footer);
        fabButton = (ImageButton) rootView.findViewById(R.id.fab_button);
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFabButtonClick(v);
            }
        });

        gv_pickers = (GridView) rootView.findViewById(R.id.gv_pickers);
        slidingPickers = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        ll_blankSheet = (LinearLayout) rootView.findViewById(R.id.ll_blankSheet);
        tv_blankSheet = (TextView) rootView.findViewById(R.id.tv_blankSheet);
        iv_blankSheet = (ImageView) rootView.findViewById(R.id.iv_blankSheet);



        srlayout = (SwipeRefreshLayout) rootView.findViewById(R.id.sr_list);
        srlayout.setOnRefreshListener(this);
        srlayout.setColorScheme(R.color.edo_blue_dark, R.color.edo_blue, R.color.edo_blue_light, R.color.edo_blue_very_light);
        EdoUI.setDistanceToTrigger(c, srlayout);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list);
        mRecyclerView.setItemAnimator(new CustomItemAnimator());

        if(canHideFooterOnScroll()) {

            mRecyclerView.addOnScrollListener(new HidingScrollListener() {
                @Override
                public void onHide() {
                    hideFooter();
                }

                @Override
                public void onShow() {
                    showFooter();
                }
            });
        }

        ///show progress
        mRecyclerView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        if(ll_blankSheet!=null)
            ll_blankSheet.setVisibility(View.GONE);

        setBackPressed(rootView);



        return rootView;
    }




    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void setFilesPicker(FilePickers filesPicker){
        this.filesPicker = filesPicker;
    }


    public void onFabButtonClick(View v){
        filesPicker.pickersSlider(getCurrentContainer(), getTimelineSpace(), gv_pickers, slidingPickers, FilePickers.CONDITION_NO_FOLDERS);
    }


    public void showList(boolean show){
        ll_blankSheet.setVisibility(show?View.GONE:View.VISIBLE);
        mRecyclerView.setVisibility(show?View.VISIBLE:View.GONE);
    }


    public void hideFooter() {
        int fabBottomMargin = 0;

        if (ll_footer.getLayoutParams() instanceof FrameLayout.LayoutParams){
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) ll_footer.getLayoutParams();
            fabBottomMargin = lp.bottomMargin;
        }
        ll_footer.animate().translationY(ll_footer.getHeight()+fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();

    }

    public void showFooter(){
        ll_footer.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }






    public abstract int getContentViewResourceId();

    public abstract boolean canAddFirstSpace();

    public abstract boolean canHideFooterOnScroll();

    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		FILES
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public LSpace timelineSpace;
    public GridView gv_pickers;

    public EdoContainer getCurrentContainer(){
        return contact;
    }

    public LSpace getTimelineSpace(){
        if(timelineSpace!=null)
            return timelineSpace;

        LSpaceDAO spaceDAO = new LSpaceDAO(c);
        return spaceDAO.getContactDefaultSpace(contact);
    }


    public void onNoTimelineSpaceFound(){
        if(canAddFirstSpace()) {
            addFirstSpace();
        }
    }

    public void addFirstSpace() {
        Runnable positiveAction = new Runnable() {

            @Override
            public void run() {
                addFirstSpace();
            }
        };

//        Runnable negativeAction = new Runnable() {
//
//            @Override
//            public void run() {
//                Act_00_SplashScreen.this.finish();
//            }
//        };

        if(!EdoUI.dialogNoInternet(c, positiveAction, null)){
            AUtils.executeAsyncTask(new AsyncAddFirstSpace(), "");
        }


    }


    public class AsyncAddFirstSpace extends AsyncTask<String, String, LSpace>{

        EdoContainer container = null;

        public AsyncAddFirstSpace() {
            super();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            c.runOnUiThread(new Runnable() {
                public void run() {
                    loading.setTitle(R.string.initialization);
                    loading.setSubTitle(R.string.loading);
                    loading.show();
                }
            });

        }



        @Override
        protected LSpace doInBackground(String... params) {
            container = getCurrentContainer();



            LSpace newSpace = null;

            if(container instanceof LContact){
                newSpace = spaceDAO.initDefaultSpace((LContact) container);
            }



            return newSpace;
        }

        @Override
        protected void onPostExecute(LSpace newSpace) {
            super.onPostExecute(newSpace);

            if(newSpace!=null){
                timelineSpace = newSpace;
            }else{
                if(getCurrentContainer().isGroup()){
                    final LContact group = (LContact) getCurrentContainer();
                    if(!group.isUserOwnerOfTheGroup(app.getEdoUserId())){
                        LContact owner = new LContactDAO(c).get(group.getGroupOwnerId());
                        final String ownerName = owner!=null? owner.getNameSurname() : "";
                        c.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String oops = c.getResources().getString(R.string.groupOwnerShouldAccessTheGroupFirstOwnerNameGroupName)
                                        .replace(Constants.PH_ADMINNAME, ownerName)
                                        .replace(Constants.PH_GROUPNAME, group.getName());
                                EdoUI.oneButtonDialog(c, c.getResources().getString(R.string.groupOwnerShouldAccessTheGroupFirst),oops);
                            }
                        });
                    }
                }
            }

            onPostDefaultSpaceCreation(newSpace);

            c.runOnUiThread(new Runnable() {
                public void run() {
                    loading.dismiss();
                }
            });
        }
    }


    public void onPostDefaultSpaceCreation(LSpace defaultSpace){

    }

    public abstract LSpace getCurrentSpace();




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		SELECTION
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    ArrayList<LFile> selFiles = new ArrayList<>();

    ActionMode selOptionsActionMode = null;


    public void startSelOptionsActionMode(){
        if(!isSelOptionsActionModeEnabled) {
            selOptionsActionMode = c.startActionMode(new SelOptionsActionMode());
        }
    }

    public void stopSelOptionsActionMode(){
        if(isSelOptionsActionModeEnabled && selOptionsActionMode!=null) {
            selOptionsActionMode.finish();
        }
    }

    public void setSelOptionsActionModeTitle(String title){
        if(selOptionsActionMode!=null)
            selOptionsActionMode.setTitle(title);
    }


    boolean isSelOptionsActionModeEnabled = false;

    public final class SelOptionsActionMode implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            isSelOptionsActionModeEnabled = true;

            MenuInflater inflater = c.getMenuInflater();
            inflater.inflate(R.menu.fileoptions, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            final ArrayList<LFile> filesSelected = new ArrayList<>(selFiles);

            switch(item.getItemId()) {

                case R.id.forward:
                    fileOptions.forwardShare(filesSelected);
                    break;

                case R.id.share:
                    fileOptions.shareWith(filesSelected);
                    break;

                case R.id.download:

                    c.runOnUiThread(new Runnable() {
                        public void run() {
                            loading.setTitle(R.string.downloadAll);
                            loading.setSubTitle(R.string.initialization);
                            loading.show();
                        }
                    });
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            if(filesSelected!=null && filesSelected.size()>0) {
                                for (LFile file : filesSelected) {
                                    fileOptions.downloadFile(getCurrentContainer(), getCurrentSpace(), file);
                                }
                            }
                            loading.dismiss();
                        }
                    }).start();

                    break;

                case R.id.delete:
                    fileOptions.deleteFilesDialog(getCurrentSpace(),filesSelected);
                    break;

                default:
                    break;

            }

            mode.finish();

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

            isSelOptionsActionModeEnabled = false;

            onSelectionFinished();

            selFiles.clear();
        }
    }

    public void onSelectionFinished(){}




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		RECEIVERS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

//    @Override
//    public abstract void onRefresh();




    public void setBackPressed(View v){
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return onBackPressed();
                }
                return false;
            }
        } );
    }


    public boolean onBackPressed(){
        if(slidingPickers!=null)
        if (slidingPickers.isExpanded()) {
            slidingPickers.collapsePane();
            return true;
        }
        return false;
    }




}
