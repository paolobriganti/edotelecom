package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 07/07/15.
 */
public class Act_ContactSelector extends Act_ContactEditorBase implements Frag_ContactList.Callbacks, ViewPager.OnPageChangeListener{

    ViewPager mViewPager;
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;

    EdoProgressDialog loading;
    EdoUI edoUI;

    public static final int SECTION_SELECTION = 0;
    public static final int SECTION_CREATION = 1;


    ContactUIDAO contactDAO;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_contact_selector);

        contactDAO = new ContactUIDAO(c);

        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) this.findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(this);

        loading = new EdoProgressDialog(this, R.string.contact, R.string.loading);
        edoUI = new EdoUI(c);

        getActionBar().setTitle(R.string.startSharingWithContactOrGroup);

        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(position == SECTION_SELECTION){
            getActionBar().setTitle(R.string.startSharingWithContactOrGroup);
        }else if(position == SECTION_CREATION){
            getActionBar().setTitle(R.string.addContact);
        }

        invalidateOptionsMenu();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        Frag_ContactEditor frag_contactEditor;

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case SECTION_CREATION:
                    frag_contactEditor = new Frag_ContactEditor();
                    return frag_contactEditor;

                default:
                    final Frag_ContactList fragment = new Frag_ContactList();
                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Section " + (position + 1);
        }

        public Frag_ContactEditor getFragContactEditor(){
            return frag_contactEditor;
        }
    }

    @Override
    public Frag_ContactEditor getFragContactEditor() {
        return mAppSectionsPagerAdapter.getFragContactEditor();
    }







    @Override
    public void onItemSelected(LContact contact) {
        if(contact!=null){
            if(contact.getStatus()==LContact.STATUS_CREATION_IN_PROGRESS){
                mAppSectionsPagerAdapter.getFragContactEditor().setEmailAddress(contact.getPrincipalEmailAddress());
                mViewPager.setCurrentItem(SECTION_CREATION);
            }else{
                finishWithResult(contact, true);
            }
        }
    }

    @Override
    public void onItemLongClick(LContact contact) {

    }

    @Override
    public void onCreateContactButtonClick() {
        mViewPager.setCurrentItem(SECTION_CREATION);
    }

    @Override
    public void onCreateGroupButtonClick() {
        startActivityForResult(new Intent(c,Act_GroupEditor.class),Constants.CONTACT_SELECT_RESULT);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        if(resultCode == Activity.RESULT_OK) {
            switch(requestCode) {

                case Constants.CONTACT_SELECT_RESULT:
                    LContact newContact = (LContact) intent.getSerializableExtra(DBConstants.CONTAINER);
                    if(newContact!=null){
                        finishWithResult(newContact,true);
                    }

                    break;

                default:
                    break;
            }


        }

    }












    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contacteditor, menu);

        MenuItem post = menu.findItem(R.id.post);
        post.setVisible(mViewPager.getCurrentItem()==SECTION_CREATION);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.post:
                saveContact();

                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//
////            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));
//
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };
}
