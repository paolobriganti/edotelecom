package io.edo.edomobile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.imageLoader.ImageLoader;

import java.util.ArrayList;

import io.edo.db.global.beans.ContactFileDAO;
import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.EdoMessageBuilder;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialogListView;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoUI;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.ListAdapterOptions;
import io.edo.ui.adapters.files.SpacesAdapter;
import io.edo.ui.imageutils.ThumbnailSpaceLoader;
import io.edo.utilities.TrackConstants;

public class Frag_ContactSpaces extends Frag_Base
        implements SpacesAdapter.SpaceClickListener, SpacesAdapter.SpaceLongClickListener, UIManager.SpacesUpdatesListener{

    private final static String LISTENER_ID = Frag_ContactSpaces.class.getName();

    private ArrayList<LSpace> spaces = new ArrayList<LSpace>();
    private SpacesAdapter mAdapter;
    private GridLayoutManager lm;

    private ThumbnailSpaceLoader thumbnailSpaceLoader;

    private int spanCount = 3;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Frag_ContactSpaces() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        thumbnailSpaceLoader = ThumbnailSpaceLoader.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        String message = EdoMessageBuilder.getMessageBlancSheetOfSpaces(c, contact);
        tv_blankSheet.setText(Html.fromHtml(message));
        if(contact.isPersonal(c)){
            iv_blankSheet.setImageResource(R.drawable.blank_personal_spaces);
        }else if(contact.isGroup()){
            iv_blankSheet.setImageResource(R.drawable.blank_group_spaces);
        }else{
            iv_blankSheet.setImageResource(R.drawable.blank_contact_spaces);
        }

        spanCount = EdoUI.getSpacesColumnsNumber(c);

        lm = new GridLayoutManager(c, spanCount);
        mRecyclerView.setLayoutManager(lm);

        mAdapter = new SpacesAdapter(getActivity(), thumbnailSpaceLoader, contact, spaces);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setSpaceClickListener(this);
        mAdapter.setLongSpaceClickListener(this);



        loadSpaces();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        app.uiManager.addSpacesUpdatesListener(LISTENER_ID, this);
    }

    @Override
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public ImageLoader getImageLoader() {
        return app.thumbnailFileLoader;
    }


    public void onFabButtonClick(View v){
        fileOptions.addSpaceDialog(getCurrentContainer());
        app.sendEventTrack(TrackConstants.EVENT_ADD_SPACE_CLICK, TrackConstants.KEY_CONTEXT, contact.isPersonal(c) ? TrackConstants.VALUE_CONTEXT_PERSONAL : (contact.isGroup() ? TrackConstants.VALUE_CONTEXT_GROUP : TrackConstants.VALUE_CONTEXT_CONTACTS));
    }

    @Override
    public int getContentViewResourceId() {
        return R.layout.fragment_contact_spaces;
    }

    @Override
    public boolean canAddFirstSpace() {
        return false;
    }

    @Override
    public boolean canHideFooterOnScroll() {
        return true;
    }

    @Override
    public LSpace getCurrentSpace() {
        return null;
    }


    //*******************************
    //* Spaces
    //*******************************
    public void loadSpaces(){
        new SpacesTask(false).execute();
    }

    public void refreshSpaces(){
        if(app.isUpdateInProgress) {
            srlayout.setRefreshing(false);
            return;
        }
        new SpacesTask(true).execute();
    }

    class SpacesTask extends AsyncTask<String, String, ArrayList<LSpace>> {
        boolean isRefreshing = false;

        public SpacesTask(boolean isRefreshing) {
            this.isRefreshing = isRefreshing;
        }

        @Override
        protected void onPreExecute() {
            app.isUpdateInProgress = true;

            if(isRefreshing){
                srlayout.setRefreshing(true);
            }

            super.onPreExecute();
        }

        @Override
        protected ArrayList<LSpace> doInBackground(String... params) {
            EdoContainer container = getCurrentContainer();
            if(container!=null) {

                if(isRefreshing){
                    LFile[] files = new ContactFileDAO(c).downloadAllFileOf((LContact)getCurrentContainer(),null);
                    if(files==null || files.length<=0){
                        return null;
                    }
                }

                if(getTimelineSpace()==null) {
                    onNoTimelineSpaceFound();
                }

                if(container.isContactOrGroup()){
                    return spaceDAO.getLDAO().getContactSpaces((LContact) container);
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(final ArrayList<LSpace> result) {
            app.isUpdateInProgress = false;
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            c.runOnUiThread(new Runnable() {
                public void run() {
                    mAdapter.clearSpaces();

                    if (result != null && result.size()>0) {
                        if(result.size()<spanCount){
                            int spanCount = EdoUI.getSpacesMinColumnsNumber(c)>result.size()? EdoUI.getSpacesMinColumnsNumber(c):result.size();
                            lm.setSpanCount(spanCount);
                        }

                        mAdapter.addSpaces(result);
                        showList(true);
                    }else{
                        showList(false);
                    }
                    mProgressBar.setVisibility(View.GONE);

                    if(isRefreshing){
                        srlayout.setRefreshing(false);
                    }
                }
            });
            this.cancel(true);
        }
    }

    public void addSpaceToList(LSpace space){
        showList(true);
        int count = spaces.size()+1;
        if(count<spanCount){
            lm.setSpanCount(count);
        }else{
            lm.setSpanCount(spanCount);
        }
        if(!spaces.contains(space)) {
            mAdapter.addSpace(space);
        }
    }

    public void updateSpaceOnList(LSpace space){
        mAdapter.updateSpace(space);
    }

    public void updateSpaceOnList(int index, LSpace space){
        mAdapter.updateSpace(index, space);
    }

    public void removeSpaceFromList(LSpace space){
        mAdapter.removeSpace(space);
    }


    @Override
    public void onClick(View v, LSpace space) {
        if(mSpaceSelectedListener!=null){
            mSpaceSelectedListener.onSpaceSelect(v, space);
        }
    }
    @Override
    public void onLongClick(View v, LSpace space) {
        settingsDialog(space);
    }

    public void settingsDialog(final LSpace space){
        if(space!=null){
            final EdoDialogListView dialog = new EdoDialogListView(c);
            dialog.setTitle(space.getName());

            final ArrayList<Integer> titles = new ArrayList<Integer>();


            titles.add(R.string.edit);
            titles.add(R.string.delete);

            ListAdapterOptions adapter = new ListAdapterOptions(c, titles);
            dialog.setAdapter(adapter);
            dialog.setPositiveButtonText(R.string.cancel);
            dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

                @Override
                public void onClick(View arg0) {
                    dialog.cancel();
                }
            });
            dialog.setOnListItemClickListener(new EdoDialogListView.OnListItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    switch (titles.get(position)) {
                        case R.string.edit:
                            fileOptions.editSpaceDialog(getCurrentContainer(), space);
                            break;

                        case R.string.delete:
                            remove(space);
                            break;

                        default:
                            break;
                    }
                    dialog.cancel();
                }
            });
            dialog.show();
        }
    }


    public void remove(final LSpace space){
        if(space!=null){
            final EdoDialog dialog = new EdoDialog(c);
            if(space!=null && JavaUtils.isNotEmpty(space.getName()))
                dialog.setTitle(space.getName());
            else
                dialog.setTitle(R.string.spaceName);
            dialog.setText(R.string.doYouWantToRemoveThisSpace);
            dialog.setPositiveButtonText(R.string.yes);
            dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {

                @Override
                public void onClick(View v) {
                    new Thread(){@Override public void run(){
                        Looper.myLooper();

                        if(space!=null){
                            c.runOnUiThread(new Runnable() {
                                public void run() {
                                    String spaceName = space.getCurrentFolder().getName();
                                    loading.setTitle(spaceName);
                                    loading.setSubTitle(c.getResources().getString(R.string.deleting));
                                    loading.show();
                                }
                            });
                            if(new FileUIDAO(c).deleteFile(getCurrentContainer(),space,space,true,false)!=null){
                                onDelete(getCurrentContainer(), space);
                            }else{
                                c.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        EdoUI.showToast(c, R.string.errorRetry, null, Toast.LENGTH_LONG);
                                    }
                                });
                            }
                            c.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loading.dismiss();
                                }
                            });
                        }

                    }}.start();
                    dialog.dismiss();
                }
            });
            dialog.setNegativeButtonText(R.string.no);
            dialog.setOnNegativeClickListener(new EdoDialog.OnNegativeClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }


    @Override
    public void onRefresh() {
        refreshSpaces();
    }


    //*******************************
    //* UPDATES
    //*******************************
    @Override
    public void onInsert(final EdoContainer container, final LSpace space) {
        if(app.isUpdateInProgress)
            return;

        if(space!=null && space.isDefault())
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (container != null &&
                        container.equals(getCurrentContainer()) &&
                        !spaces.contains(space)) {


                    addSpaceToList(space);


                }

            }
        });
    }

    @Override
    public void onUpdate(final EdoContainer container, final LSpace space) {


        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateSpaceOnList(space);
            }
        });
    }

    @Override
    public void onDelete(final EdoContainer container, final LSpace space) {


        if(app.isUpdateInProgress)
            return;

        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                removeSpaceFromList(space);
            }
        });
    }

    @Override
    public void onSyncList(LActivity a) {
        if(app.isUpdateInProgress)
            return;

        if(a.hasBeenProcessed())
            loadSpaces();
    }




    SpaceSelectedListener mSpaceSelectedListener;

    public void setSpaceSelectedListener(SpaceSelectedListener listener) {
        mSpaceSelectedListener = listener;
    }

    public interface SpaceSelectedListener {
        void onSpaceSelect(View v, LSpace space);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        app.uiManager.removeListener(LISTENER_ID);
    }
}
