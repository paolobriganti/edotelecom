package io.edo.edomobile;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.paolobriganti.android.AUI;

import java.io.Serializable;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.ui.FilePickers;


public class Act_ContactDetail extends Act_ContactBase{
    public FilePickers filesPicker;

//    private static final int SCALE_DELAY = 30;
//    private LinearLayout rowContainer;

//    private Toolbar toolbar;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);

        isContactDetailActivity = true;

        mTwoPane = false;


//        rowContainer = (LinearLayout) findViewById(R.id.row_container2);
//        toolbar = (Toolbar) findViewById(R.id.activity_transition_header);


//        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
//            setEnterAnimationLollipop(savedInstanceState);
//            setExitAnimationLollipop();
//        }else{
            onCreated(savedInstanceState);
//        }




    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        processExtras(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//
////        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
////            runExitAnimationLollipop();
////        }else{
//            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));
////        }
//
//
//    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void setEnterAnimationLollipop(final Bundle savedInstanceState){
//        getWindow().getEnterTransition().addListener(new TransitionAdapter() {
//
//            @Override
//            public void onTransitionEnd(Transition transition) {
//
//                super.onTransitionEnd(transition);
//
//                getWindow().getEnterTransition().removeListener(this);
//
//                for (int i = 0; i < rowContainer.getChildCount(); i++) {
//
//                    View rowView = rowContainer.getChildAt(i);
//                    rowView.animate().setStartDelay(i * SCALE_DELAY)
//                            .scaleX(1).scaleY(1);
//                }
//
//                onCreated(savedInstanceState);
//            }
//        });
//    }


//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void setExitAnimationLollipop(){
//        Slide slideExitTransition = new Slide(Gravity.BOTTOM);
//        slideExitTransition.excludeTarget(android.R.id.navigationBarBackground, true);
//        slideExitTransition.excludeTarget(android.R.id.statusBarBackground, true);
//    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void runExitAnimationLollipop(){
//        for (int i = 0; i < rowContainer.getChildCount(); i++) {
//
//            View rowView = rowContainer.getChildAt(i);
//
//            ViewPropertyAnimator propertyAnimator = rowView.animate()
//                    .setStartDelay(i * SCALE_DELAY)
//                    .scaleX(0).scaleY(0)
//                    .setListener(new AnimatorAdapter() {
//
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//
//                            super.onAnimationEnd(animation);
//                            finishAfterTransition();
//                        }
//                    });
//        }
//    }




    public void onCreated(Bundle savedInstanceState){

        if (savedInstanceState == null) {

            processExtras(getIntent());

        }



        if(contact == null)
            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));


    }


    public void processExtras(Intent intent){
        if(intent==null && intent.getExtras()==null)
            return;

        if (intent.getExtras().containsKey(DBConstants.CONTAINER)) {
            Serializable contactSerial = intent.getSerializableExtra(DBConstants.CONTAINER);
            if(contactSerial!=null) {
                contact = (LContact) contactSerial;
            }else{
                contact = app.getUserContact();
            }
        }else{
            contact = app.getUserContact();
        }
        setContactDetailFragment(Frag_ContactDetail.SECTION_TIMELINE, Frag_ContactOrganizer.SECTION_SPACES);

        // Show the Up button in the action bar.
        getActionBar().setDisplayHomeAsUpEnabled(true);

    }







}
