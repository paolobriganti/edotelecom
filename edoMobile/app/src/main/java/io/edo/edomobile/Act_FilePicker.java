package io.edo.edomobile;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;

import io.edo.db.local.beans.support.DBConstants;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 12/06/15.
 */
public class Act_FilePicker extends FragmentActivity{

	public final static String EXTRA_SOURCE = "source";
	public final static int EXTRA_SOURCE_LOCAL_DISK = 0;
	public final static int EXTRA_SOURCE_LOCAL_DISK_GALLERY = 1;
	public final static int EXTRA_SOURCE_GOOGLE_DRIVE = 2;

	public final static String EXTRA_FILE_PATH = "file_path";
	public final static String EXTRA_SHOW_HIDDEN_FILES = "show_hidden_files";
	public final static String EXTRA_ACCEPTED_FILE_EXTENSIONS = "accepted_file_extensions";
	public final static String EXTRA_ACCEPTED_FILE_TYPES = "accepted_file_mimes";


	Activity c;
	ThisApp app;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_filepicker);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setTitle(R.string.fileExplorer);

		c = this;
		app = ThisApp.getInstance(c);

		if (savedInstanceState == null) {

			Bundle arguments = new Bundle();
			if(getIntent().hasExtra(EXTRA_FILE_PATH))
				arguments.putString(EXTRA_FILE_PATH, getIntent().getStringExtra(EXTRA_FILE_PATH));
			if(getIntent().hasExtra(EXTRA_SHOW_HIDDEN_FILES))
				arguments.putBoolean(EXTRA_SHOW_HIDDEN_FILES, getIntent().getBooleanExtra(EXTRA_SHOW_HIDDEN_FILES, false));
			if(getIntent().hasExtra(EXTRA_ACCEPTED_FILE_EXTENSIONS))
				arguments.putStringArrayList(EXTRA_ACCEPTED_FILE_EXTENSIONS, getIntent().getStringArrayListExtra(EXTRA_ACCEPTED_FILE_EXTENSIONS));
			if(getIntent().hasExtra(EXTRA_ACCEPTED_FILE_TYPES))
				arguments.putStringArrayList(EXTRA_ACCEPTED_FILE_TYPES, getIntent().getStringArrayListExtra(EXTRA_ACCEPTED_FILE_TYPES));

			int source = getIntent().getIntExtra(EXTRA_SOURCE,EXTRA_SOURCE_LOCAL_DISK);

			switch (source){
				case EXTRA_SOURCE_LOCAL_DISK_GALLERY:
					getActionBar().setLogo(R.drawable.file_picker_raster);
					Frag_FilesPickerGallery fragment1 = new Frag_FilesPickerGallery();
					fragment1.setArguments(arguments);
					getSupportFragmentManager().beginTransaction()
							.add(R.id.files_container, fragment1)
							.commit();
					break;

				case EXTRA_SOURCE_GOOGLE_DRIVE:
					getActionBar().setLogo(R.drawable.file_picker_drive);
					Frag_FilesPickerGoogleDrive fragment2 = new Frag_FilesPickerGoogleDrive();
					if(getIntent().hasExtra(DBConstants.SERVICE_ID))
						arguments.putString(DBConstants.SERVICE_ID, getIntent().getStringExtra(DBConstants.SERVICE_ID));
					fragment2.setArguments(arguments);
					getSupportFragmentManager().beginTransaction()
							.add(R.id.files_container, fragment2)
							.commit();
					break;

				default:
					getActionBar().setLogo(R.drawable.file_picker_other);
					Frag_FilesPicker fragment = new Frag_FilesPicker();
					fragment.setArguments(arguments);
					getSupportFragmentManager().beginTransaction()
							.add(R.id.files_container, fragment)
							.commit();
					break;

			}


		}


		registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));

	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();

//		AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));


	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
	}
	private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, final Intent intent) {
			finish();
		}
	};

}
