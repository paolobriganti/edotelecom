package io.edo.edomobile;

import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import com.paolobriganti.utils.FileInfo;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Comparator;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoUI;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.ui.imageutils.ThumbnailFilePickerLoader;

/**
 * Created by PaoloBriganti on 12/06/15.
 */
public class Frag_FilesPicker extends Frag_Base_FilePicker {

    protected File mainDirectory;

    protected boolean mShowHiddenFiles = false;
    protected String[] acceptedFileExtensions;
    protected String[] acceptedFileTypes;

    private final static String DEFAULT_INITIAL_DIRECTORY = Environment.getExternalStorageDirectory().toString();

    private ThumbnailFilePickerLoader imageLoader;

    public Frag_FilesPicker() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            EdoUI.showToast(c, R.string.noExternalMemoryfound, null, Toast.LENGTH_LONG);
            c.finish();
            return;
        }

        mainDirectory = new File(DEFAULT_INITIAL_DIRECTORY);

        if(getArguments().containsKey(Act_FilePicker.EXTRA_FILE_PATH)) {
            mainDirectory = new File(getArguments().getString(Act_FilePicker.EXTRA_FILE_PATH));
        }
        if(getArguments().containsKey(Act_FilePicker.EXTRA_SHOW_HIDDEN_FILES)) {
            mShowHiddenFiles = getArguments().getBoolean(Act_FilePicker.EXTRA_SHOW_HIDDEN_FILES, false);
        }

        if(getArguments().containsKey(Act_FilePicker.EXTRA_ACCEPTED_FILE_EXTENSIONS)) {
            ArrayList<String> collection = getArguments().getStringArrayList(Act_FilePicker.EXTRA_ACCEPTED_FILE_EXTENSIONS);
            acceptedFileExtensions = collection.toArray(new String[collection.size()]);
        }
        if(getArguments().containsKey(Act_FilePicker.EXTRA_ACCEPTED_FILE_TYPES)) {
            ArrayList<String> collection = getArguments().getStringArrayList(Act_FilePicker.EXTRA_ACCEPTED_FILE_TYPES);
            acceptedFileTypes = collection.toArray(new String[collection.size()]);
        }

        imageLoader = new ThumbnailFilePickerLoader(c);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public int getContentLayoutResourceId() {
        return R.layout.fragment_files_picker;
    }

    @Override
    public ThumbnailFileLoader getThumbnailFileLoader() {
        return imageLoader;
    }

    @Override
    public LSpace getSpace() {
        LSpace space = new LSpace();
        return space;
    }


    @Override
    public ArrayList<LFile> getFiles(LFile folder) {

        ArrayList<LFile> mFiles = new ArrayList<LFile>();

        // Set the extension file filter
        ExtensionFilenameFilter filter = new ExtensionFilenameFilter(acceptedFileTypes);

        // Get the files in the directory
        File mDirectory = (File) getFile(folder);
        if(mDirectory==null){
            mDirectory = mainDirectory;
        }

        File[] files = mDirectory.listFiles(filter);
        if(files != null && files.length > 0) {
            for(File f : files) {
                if(f.isHidden() && !mShowHiddenFiles) {
                    // Don't add the file
                    continue;
                }

                // Add the file the ArrayAdapter
                mFiles.add(getEdoFile(f));
            }


        }

        return mFiles;
    }

    @Override
    public LFile getEdoFile(Object file) {
        if(file!=null) {
            File f = (File) file;

            LFile edoFile = LFile.newLocalFile(null, null, f.getPath(), FileInfo.getMime(f.getPath()), null);
            if (f.isDirectory()) {
                edoFile.setType(LFile.FILE_TYPE_EDOFOLDER);
            }
            edoFile.setIsSelected(isSelected(edoFile));
            edoFile.setmDate(f.lastModified());
            return edoFile;
        }
        return null;
    }

    @Override
    public Object getFile(LFile edoFile) {
        if(edoFile!=null)
            return new File(edoFile.getLocalUri());
        return null;
    }

    @Override
    public String getUniqueKeyForFile(LFile edoFile) {
        return edoFile.getLocalUri();
    }


    @Override
    public LFile getParentFolder(LFile currentFolder) {
        File mDirectory = (File) getFile(currentFolder);
        if(mDirectory==null){
            mDirectory = mainDirectory;
        }
        if(mDirectory.getParentFile() != null) {
            return getEdoFile(mDirectory.getParentFile());
        }
        return null;
    }

    @Override
    public String getBreadCrumbs(LFile currentFolder) {
        File mDirectory = (File) getFile(currentFolder);
        if(mDirectory==null){
            mDirectory = mainDirectory;
        }

        return mDirectory.getPath();
    }

    @Override
    public Comparator<LFile> getFileComparator() {
        return new FileFolderFirstComparator();
    }


    private class ExtensionFilenameFilter implements FilenameFilter {
        private String[] mExtensions;

        public ExtensionFilenameFilter(String[] extensions) {
            super();
            mExtensions = extensions;
        }

        @Override
        public boolean accept(File dir, String filename) {
            if(new File(dir, filename).isDirectory()) {
                // Accept all directory names
                return true;
            }
            if(mExtensions != null && mExtensions.length > 0) {
                for(int i = 0; i < mExtensions.length; i++) {
                    if(filename.endsWith(mExtensions[i])) {
                        // The filename ends with the extension
                        return true;
                    }
                }
                // The filename did not match any of the extensions
                return false;
            }
            // No extensions has been set. Accept all file extensions.
            return true;
        }
    }

//    private class FileComparator implements Comparator<File> {
//        @Override
//        public int compare(File f1, File f2) {
//            if(f1 == f2) {
//                return 0;
//            }
//            if(f1.isDirectory() && f2.isFile()) {
//                // Show directories above files
//                return -1;
//            }
//            if(f1.isFile() && f2.isDirectory()) {
//                // Show files below directories
//                return 1;
//            }
//            // Sort the directories alphabetically
//            return f1.getName().compareToIgnoreCase(f2.getName());
//        }
//    }
}
