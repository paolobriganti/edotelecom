package io.edo.edomobile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paolobriganti.android.ASI;
import com.paolobriganti.android.AUI;
import com.paolobriganti.android.VU;
import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.api.ApiInfo;
import io.edo.db.global.beans.ContactDAO;
import io.edo.db.global.beans.ServiceDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LService;
import io.edo.db.web.request.EdoAuthManager;
import io.edo.db.web.request.EdoRequest;
import io.edo.settings.EdoPreferences;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialogOne.OnPositiveClickListener;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.utilities.Constants;
import io.edo.views.EdoProgressBar;

public class Act_03_Settings extends FragmentActivity {
	public ThisApp app;
	private Act_03_Settings c;

	public EdoPreferences preferences;
	public EdoProgressDialog loading;

	//** 	Contact 	**
	public ContactDAO contactDAO;

	//** 	Service 	**
	public ServiceDAO serviceDAO;
	private LService service;


	private ImageView iv_contact_photo;
	private TextView tv_contactName;
	private TextView tv_cloudEmail;
	private EdoProgressBar progressBar;
	private TextView tv_space;
	private ListView lv_settings;



	@Override
	protected void onCreate(Bundle savedInstanceState) {

		AUI.setStatusBarColor(this, this.getResources().getColor(R.color.edo_blue_dark));
		EdoUI.setOrientationTo(this);

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		overridePendingTransition(0,0);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_03_settings);

		app = ThisApp.getInstance(this);
		c=this;


		preferences = new EdoPreferences(c);
		loading = new EdoProgressDialog(c, R.string.loading, R.string.loading);


		//** 	Contact 	**
		contactDAO = new ContactDAO(c);

		//** 	Service 	**
		serviceDAO = new ServiceDAO(c);

		iv_contact_photo = (ImageView) findViewById(R.id.iv_contact_photo);
		tv_contactName = (TextView) findViewById(R.id.tv_contactName);
		tv_cloudEmail = (TextView) findViewById(R.id.tv_cloudEmail);
		progressBar = (EdoProgressBar) findViewById(R.id.progressBar);
		tv_space = (TextView) findViewById(R.id.tv_space);
		lv_settings = (ListView) findViewById(R.id.lv_settings);



		if(JavaUtils.isNotEmpty(app.getUserEmail())){
			service = serviceDAO.getLDAO().getServiceByEmail(app.getUserEmail());

			if(service!=null){
				new Thread(){@Override public void run(){Looper.myLooper();Looper.prepare();
				final LContact contact = app.getUserContact();
				if(contact!=null){
					runOnUiThread(new Runnable() {public void run() {
						ContactPhotoLoader.getInstance(c).loadImage(contact, iv_contact_photo);
					}});
				}
				}}.start();

				tv_contactName.setText(app.getUserNameSurname());
				tv_cloudEmail.setText(service.getEmail());

				new Thread(){@Override public void run(){Looper.myLooper();
				try{
					if(ASI.isInternetOn(c)){
						ApiInfo apiInfo = new ApiInfo(c, app.getDB(), service);
						if(apiInfo!=null){
							Long freeBytes = apiInfo.getFreeBytes();
							Long totalBytes = apiInfo.getTotalBytes();
							String freeSpace = apiInfo.getBytesForView(freeBytes);
							String totalSpace = apiInfo.getBytesForView(totalBytes);
							final String cInfo = freeSpace+" "+getResources().getString(R.string.freeOf)+" "+totalSpace;
							final int progress = JavaUtils.getIntegerValueOfDouble(apiInfo.getOccupiedBytes()/1000000, 0);
							final int max = JavaUtils.getIntegerValueOfDouble(totalBytes/1000000, 0);
							runOnUiThread(new Runnable() {public void run() {
								tv_space.setText(cInfo);
								progressBar.setMax(max);
								progressBar.setProgress(progress);
								VU.setVisibilityWithAnimationToView(c, progressBar, ProgressBar.VISIBLE, R.anim.appear, (long)1000);
								VU.setVisibilityWithAnimationToView(c, tv_space, TextView.VISIBLE, R.anim.appear, (long)1000);
							}});
						}
					}
				}catch(NullPointerException e){}
				}}.start();
			}else{
				logout();
			}

		}




		ListAdapterCheckBox adapter = new ListAdapterCheckBox();
		lv_settings.setAdapter(adapter);
		VU.expandListView(lv_settings);
		//		mainScrollView.post(new Runnable() {
		//
		//			@Override
		//			public void run() {
		//				mainScrollView.fullScroll(ScrollView.FOCUS_UP);
		//			}
		//		});

	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if(loading != null){
			loading.resume();
		}
	}

	@Override
	public void onPause() {
		super.onPause();


		if(loading != null){
			loading.pause();
		}
	}

	//	@Override
	//	protected void onStop() {
	//		// TODO Auto-generated method stub
	//		super.onStop();
	//	
	//		if(ASI.isThisAppBackground(c))
	//			Act_03_Settings.this.finish();
	//	}

	public class ListAdapterCheckBox extends BaseAdapter {
		private final ArrayList<String> titles = new ArrayList<String>();
		private final ArrayList<String> subTitles = new ArrayList<String>();
		private final ArrayList<Boolean> checked = new ArrayList<Boolean>();

		boolean change = false;

		public ListAdapterCheckBox() {
			preferences.load(c);

			//  0
			String name = c.getPackageName().contains("test")?"TEST_APP":c.getResources().getString(R.string.app_name);
			String version = AUI.getAppVersion(c) + 
					((EdoRequest.HOST_PREFIX==EdoRequest.HOST_PREFIX_TEST) ? "_test" : 
						((EdoRequest.IS_TEST_APP) ? "_production" : ""));
			this.titles.add(name+" "+version);
			this.subTitles.add(getResources().getString(R.string.visitEdoWebSite));
			this.checked.add(false);


			//	1
			this.titles.add(getResources().getString(R.string.notificationSound));
			this.subTitles.add(getResources().getString(R.string.notificationSoundSub));
			this.checked.add(preferences.isNotySoundEnabled());

			//	2
//			this.titles.add(getResources().getString(R.string.listView));
//			this.subTitles.add(getResources().getString(R.string.listViewSub));
//			this.checked.add(preferences.getFilesView()==Constants.VISUALIZATION_LIST);


			//  2
			this.titles.add(getResources().getString(R.string.sendFeedback));
			this.subTitles.add(getResources().getString(R.string.sendFeedbackSub));
			this.checked.add(false);

			//  3
			this.titles.add(getResources().getString(R.string.credits));
			this.subTitles.add(getResources().getString(R.string.creditsSub));
			this.checked.add(false);

			//	4
			this.titles.add(getResources().getString(R.string.logout));
			this.subTitles.add(getResources().getString(R.string.logoutSub));
			this.checked.add(false);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = convertView;
			if(rowView==null)
				rowView = inflater.inflate(R.layout.row_title_sub_check, parent, false);

			TextView title = (TextView) rowView.findViewById(R.id.tv_title);
			TextView subTitle = (TextView) rowView.findViewById(R.id.tv_subTitle);
			final CheckBox cb = (CheckBox) rowView.findViewById(R.id.cb);

			title.setText(titles.get(position));
			subTitle.setText(subTitles.get(position));
			cb.setChecked(checked.get(position));

			if(position==0 || position==3 || position==4 || position==5)
				cb.setVisibility(CheckBox.GONE);
			else
				cb.setVisibility(CheckBox.VISIBLE);


			rowView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if(position==0){
						bt_edoWebSite();
					}else if(position==2){
						bt_feedback();
					}else if(position==3){
						bt_credits();
					}else if(position==4){
						bt_logout();
					}else{
						cb.setChecked(!cb.isChecked());
						onCheck(cb, position);
					}

				}
			});

			cb.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onCheck(cb, position);
				}
			});

			return rowView;
		}


		public void onCheck(CheckBox cb, int position){
			checked.set(position, cb.isChecked());
			switch (position) {
			case 1:
				preferences.setNotySoundEnabled(cb.isChecked());
				preferences.save(c);
				break;

			case 2:
				preferences.setFilesView(cb.isChecked()?Constants.VISUALIZATION_LIST:Constants.VISUALIZATION_GRID);
				preferences.save(c);

				break;

			default:
				break;
			}
		}


		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return titles.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return titles.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public boolean isChecked(int position){
			return checked.get(position);
		}
	} 

	public void bt_edoWebSite(){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.edo.io"));
		startActivity(browserIntent);
	}

	public void bt_credits(){
		EdoUI.oneButtonDialog(c, R.string.credits, R.string.creditsText);
	}

	public void bt_logout(){
		final EdoDialog dialog = new EdoDialog(c);
		dialog.setTitle(R.string.logout);
		dialog.setText(R.string.logoutText);
		dialog.setOnPositiveClickListener(new OnPositiveClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();

				logout();
			}
		});

		dialog.show();
	}

	public void logout(){
		loading = new EdoProgressDialog(c, R.string.logout, R.string.loading);
		loading.show();
		new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
		EdoAuthManager.logout(c);

		loading.dismiss();
		Act_03_Settings.this.finish();
		}}.start();
	}


	public void bt_feedback(){
		Intent intent = new Intent(c, Act_Dialog_SendFeedback.class);
		startActivity(intent);
	}


	//	public static void setSmartButtons(final Act_01_Main c){
	//		runOnUiThread(new Runnable() {public void run() {
	//			bt_personal.setImageResource(R.drawable.bt_personal_d);
	//			bt_personal.setBackgroundColor(Color.TRANSPARENT);
	//			bt_contacts.setImageResource(R.drawable.nb_personal_e);
	//			bt_contacts.setBackgroundColor(Color.TRANSPARENT);
	//			bt_settings.setImageResource(R.drawable.bt_settings_e);
	//			bt_settings.setBackgroundResource(R.drawable.tab_indicator_bg_white_line_large_blue);
	//			new Thread(){@Override public void run(){Looper.myLooper();Looper.prepare();
	//				EdoUI.setBtTimeline(c, app.getDB(), null, bt_contacts, tv_contacts, false);
	//			}}.start();
	//		}});
	//	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case android.R.id.home:
			Act_03_Settings.this.finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}




