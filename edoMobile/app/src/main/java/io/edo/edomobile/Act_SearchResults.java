package io.edo.edomobile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paolobriganti.android.VU;
import com.paolobriganti.android.ui.SearchBar;
import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.global.uibeans.SpaceUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.Comment;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoDialog;
import io.edo.ui.EdoDialogOne;
import io.edo.ui.EdoIntentManager;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.EdoUI;
import io.edo.ui.adapters.contacts.ContactViewHolder;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.ui.imageutils.ThumbnailFileLoader;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 12/08/15.
 */
public class Act_SearchResults  extends AppCompatActivity
        implements SearchBar.OnQueryTextChangeListener
//        SearchView.OnQueryTextListener, SearchView.OnSuggestionListener

{

    FragmentActivity c;
    ThisApp app;

    public EdoProgressDialog loading;

    private LinearLayout ll_contactsTitle;
    private LinearLayout ll_contactsContainer;

    private LinearLayout ll_filesTitle;
    private LinearLayout ll_filesContainer;

    private LayoutInflater inflater;

    private Toolbar toolbar;
    private android.support.v7.app.ActionBar actionBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(0, 0);

        setContentView(R.layout.act_searchresult);

        c = this;
        app = ThisApp.getInstance(this);

        inflater = c.getLayoutInflater();

        loading = new EdoProgressDialog(c,R.string.loading,R.string.loading);

        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.search_activity_toolbar);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setIcon(android.R.color.transparent);

        ll_contactsTitle = (LinearLayout) findViewById(R.id.ll_contactsTitle);
        ll_contactsContainer = (LinearLayout) findViewById(R.id.ll_contactsContainer);

        ll_filesTitle = (LinearLayout) findViewById(R.id.ll_filesTitle);
        ll_filesContainer = (LinearLayout) findViewById(R.id.ll_filesContainer);

        setupContactsSearch();
        setupFilesSearch();

        SearchBar searchBar = new SearchBar(toolbar,
                c.getResources().getColor(R.color.edo_black),
                c.getResources().getColor(R.color.edo_gray_light),
                c.getResources().getString(R.string.search));
        searchBar.setOnQueryTextChangeListener(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
    }




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.search, menu);
//
//        MenuItem searchItem = menu.findItem(R.id.search);
//        SearchView searchView = (SearchView) searchItem.getActionView();
//
//        setupSearchView(searchView, searchItem);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    String searchName;
    public boolean onQueryTextChange(String newText) {
        searchName = newText;
        if(JavaUtils.isNotEmpty(newText)){
            searchContacts(newText);
            searchFiles(newText);
        }
        return false;
    }





    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		CONTACTS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    private ContactUIDAO contactDAO;

    private ContactPhotoLoader contactPhotoLoader;

    public void setupContactsSearch(){
        contactDAO = new ContactUIDAO(c);

        contactPhotoLoader = ContactPhotoLoader.getInstance(c);
    }

    public void searchContacts(String text){
        new AsyncSearchContacts(text).execute();
    }

    class AsyncSearchContacts extends AsyncTask<String, ArrayList<LContact>, ArrayList<LContact>> {
        String text;

        public AsyncSearchContacts(String text) {
            super();
            this.text = text;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }


        @Override
        protected ArrayList<LContact> doInBackground(String... params) {

            if(JavaUtils.isNotEmpty(text)){
                return contactDAO.getLDAO().getContactByNameLike(text,true);
            }

            return null;
        }


        @Override
        protected void onPostExecute(final ArrayList<LContact> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            this.cancel(true);

            if(JavaUtils.isEmpty(text)){
                ll_contactsContainer.setVisibility(View.GONE);
                ll_contactsTitle.setVisibility(View.GONE);

            }else if(result!=null && result.size()>0 && text.equals(searchName)){
                c.runOnUiThread(new Runnable() {
                    public void run() {
                        if (result != null) {
                            ll_contactsContainer.removeAllViews();

                            int max = result.size()>10?10:result.size();
                            for(int i=0; i<max; i++){
                                LContact contact = result.get(i);
                                ll_contactsContainer.addView(getContactView(text, contact));
                            }

                        }
                        ll_contactsContainer.setVisibility(View.VISIBLE);
                        ll_contactsTitle.setVisibility(View.VISIBLE);
                    }
                });

            }else{
                ll_contactsContainer.setVisibility(View.GONE);
                ll_contactsTitle.setVisibility(View.GONE);
            }
        }
    }

    public View getContactView(String text, final LContact contact){
        final View rowView = inflater.inflate(R.layout.row_edo_contact_left_image, ll_contactsContainer, false);
        ContactViewHolder vh = new ContactViewHolder(rowView);
        vh.setContactViews(c,contact,contactPhotoLoader,null,null,null,null,text);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contact.isActive() || contact.isGroup()) {
                    goToContact(contact);
                } else {
                    initAndGoToContactDialog(contact);
                }
            }
        });
        rowView.setBackgroundResource(R.drawable.bg_button_light);
        return rowView;
    }

    public void initAndGoToContactDialog(final LContact contact){
        if(!contact.isActive() && !contact.isHidden() && !contact.isGroup()) {
            final EdoDialog dialog = new EdoDialog(c);

            dialog.setTitle(contact.getPrincipalEmailAddress());
            dialog.setText(c.getResources().getString(R.string.startSharingWithContactName).replace(Constants.PH_CONTACTNAME, contact.getNameSurname()));
            dialog.setPositiveButtonText(R.string.letsStart);

            dialog.setOnPositiveClickListener(new EdoDialogOne.OnPositiveClickListener() {
                @Override
                public void onClick(View v) {
                    initAndGoToContact(contact);

                    dialog.dismiss();
                }
            });

            dialog.show();

        }else if(contact.isHidden()){
            initAndGoToContact(contact);
        }
    }

    public void initAndGoToContact(final LContact contact){
        new Thread() {
            @Override
            public void run() {
                Looper.myLooper();

                runOnUiThread(new Runnable() {
                    public void run() {
                        loading.setTitle(contact.getNameSurname());
                        loading.setSubTitle(R.string.loading);
                        loading.show();
                    }
                });

                LSpace defaultSpace = new SpaceUIDAO(c).initDefaultSpace(contact);

                if (defaultSpace != null) {

                    goToContact(contact);

                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            EdoUI.showToast(c, c.getResources().getString(R.string.errorRetry), null,Toast.LENGTH_LONG);
                        }
                    });
                }

                loading.dismiss();

            }
        }.start();
    }

    public void goToContact(final LContact contact){
        c.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                c.startActivity(EdoIntentManager.getContactsIntent(c, contact));
                Act_SearchResults.this.finish();
            }
        });
    }



    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		FILES
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    private FileUIDAO fileDAO;
    ThumbnailFileLoader thumbnailFileLoader;

    public void setupFilesSearch(){
        fileDAO = new FileUIDAO(c);
        thumbnailFileLoader = ThumbnailFileLoader.getInstance(this);
    }

    public void searchFiles(String text){
        new AsyncSearchFiles(text).execute();
    }


    class AsyncSearchFiles extends AsyncTask<String, ArrayList<LFile>, ArrayList<LFile>> {
        String text;

        public AsyncSearchFiles(String text) {
            super();
            this.text = text;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }


        @Override
        protected ArrayList<LFile> doInBackground(String... params) {

            if(JavaUtils.isNotEmpty(text)){
                return fileDAO.getLDAO().getFilesByNameLike(text);
            }

            return null;
        }


        @Override
        protected void onPostExecute(final ArrayList<LFile> result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            this.cancel(true);

            if(JavaUtils.isEmpty(text)){
//                searchView.setSuggestionsAdapter(null);

                ll_filesContainer.setVisibility(View.GONE);
                ll_filesTitle.setVisibility(View.GONE);

            }else if(result!=null && result.size()>0 && text.equals(searchName)){
                c.runOnUiThread(new Runnable() {
                    public void run() {
                        if (result != null) {
                            ll_filesContainer.removeAllViews();

                            int max = result.size()>10?10:result.size();
                            for(int i=0; i<max; i++){
                                LFile file = result.get(i);
                                ll_filesContainer.addView(getFileView(text,file));
                            }
                        }
                        ll_filesContainer.setVisibility(View.VISIBLE);
                        ll_filesTitle.setVisibility(View.VISIBLE);
                    }
                });

            }else{
                ll_filesContainer.setVisibility(View.GONE);
                ll_filesTitle.setVisibility(View.GONE);
            }
        }

        public View getFileView(String searchText, final LFile file){
            final View rowView = inflater.inflate(R.layout.row_edo_file, ll_filesContainer, false);

            TextView tv_title = (TextView) rowView.findViewById(R.id.tv_title);
            tv_title.setText(file.getNameWithExt());

            if(JavaUtils.isNotEmpty(searchText) && JavaUtils.isNotEmpty(tv_title.getText().toString())){
                String newName = tv_title.getText().toString();
                if(newName.toLowerCase().contains(searchText.toLowerCase())){
                    tv_title.setText(VU.highlightText(newName, searchText, c.getResources().getColor(R.color.green_light)));
                }
            }

            final TextView tv_subtitle = (TextView) rowView.findViewById(R.id.tv_sub_title);


            if(JavaUtils.isNotEmpty(searchText)){
                if(file.getCommentsJson()!=null && file.getCommentsJson().toLowerCase().contains(searchText.toLowerCase())){
                    Comment[] comments = file.getComments();
                    if(comments!=null && comments.length>0){
                        for(Comment comment:comments){
                            if(comment.getMessage().toLowerCase().contains(searchText.toLowerCase())){
                                SpannableString spannable = VU.highlightText(comment.getMessage(), searchText, c.getResources().getColor(R.color.green_light));
                                tv_subtitle.setText(spannable);
                                break;
                            }
                        }
                    }
                }else{
                    new Thread(){@Override public void run(){
                        Looper.myLooper();
                        EdoContainer container = fileDAO.getLDAO().getContainerOf(file);
                        final String subTitle = container.getNameComplete();
                        c.runOnUiThread(new Runnable() {public void run() {
                            tv_subtitle.setText(subTitle);
                        }});
                    }}.start();
                }
            }
            ImageView iv_thumb = (ImageView) rowView.findViewById(R.id.thumb);
            thumbnailFileLoader.loadImage(file, iv_thumb);

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToFile(file);
                }
            });

            rowView.setBackgroundResource(R.drawable.bg_button_light_green);
            return rowView;
        }
    }


    public void goToFile(final LFile file){
        Act_SearchResults.this.finish();
        Intent i = EdoIntentManager.getFileIntent(c, file);
        startActivity(i);
    }
}
