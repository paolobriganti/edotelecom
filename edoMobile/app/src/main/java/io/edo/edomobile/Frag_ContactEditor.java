package io.edo.edomobile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.paolobriganti.utils.JavaUtils;

import java.io.Serializable;

import io.edo.api.google.GoogleContactsEdo;
import io.edo.api.google.GooglePlusContactInfo;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.ui.EdoUI;
import io.edo.ui.imageutils.ContactPhotoLoader;

/**
 * A fragment representing a single Contact detail screen.
 * This fragment is either contained in a {@link Act_ContactActiveList}
 * in two-pane mode (on tablets) or a {@link Act_ContactDetail}
 * on handsets.
 */
public class Frag_ContactEditor extends Fragment{
    FragmentActivity c;
    ThisApp app;

    private LContact contact;

    private ContactPhotoLoader contactPhotoLoader;

    private boolean isNew = true;

    private ImageView iv_contact_photo;
    private TextView tv_contactName;
    private EditText et_emailaddress;
    private EditText et_name;
    private ProgressBar pb_circle;

    private EdoUI edoUI;

    public Frag_ContactEditor() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        c = getActivity();
        app = ThisApp.getInstance(c);

        contactPhotoLoader = ContactPhotoLoader.getInstance(c);

        edoUI = new EdoUI(c);

        if (getArguments()!=null && getArguments().containsKey(DBConstants.CONTAINER)) {
            Serializable contactSerial = getArguments().getSerializable(DBConstants.CONTAINER);
            if (contactSerial != null) {
                contact = (LContact) contactSerial;
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_editor, container, false);


        iv_contact_photo = (ImageView) rootView.findViewById(R.id.iv_contact_photo);
        tv_contactName = (TextView) rootView.findViewById(R.id.tv_contactName);
        et_emailaddress = (EditText) rootView.findViewById(R.id.et_emailaddress);
        et_name = (EditText) rootView.findViewById(R.id.et_name);
        pb_circle = (ProgressBar) rootView.findViewById(R.id.pb_circle);

        et_emailaddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                String email = et_emailaddress.getText().toString();
                if(!hasFocus && JavaUtils.isNotEmpty(email) && JavaUtils.isEmailAddress(email)){
                    if(!email.equalsIgnoreCase(app.getUserEmail()))
                        loadInfo();
                    else{
                        edoUI.showToastLong(c.getResources().getString(R.string.youCantAddYourEmailAddress), null);
                        et_emailaddress.setText(null);
                    }
                }
            }
        });

        et_name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                }
                return false;
            }
        });

        if(contact!=null){
            isNew = false;
            updateContactViews(contact);
        }else{
            isNew = true;
        }

        setBackPressed(rootView);


        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void updateContactViews(LContact contact){
        this.contact = contact;
        contactPhotoLoader.loadImage(contact, iv_contact_photo);

        et_name.setText(contact.getNameSurname());
//        et_surname.setText(contact.getSurname());
        et_emailaddress.setText((contact.getPrincipalEmailAddress()));
        if(!isNew){
            et_emailaddress.setFocusable(false);
        }
    }






    public void loadInfo(){
        String name = et_name.getText().toString();
        String emailAddress = et_emailaddress.getText().toString();
        new InfoLoader(name,emailAddress).execute();
    }



    public class InfoLoader extends AsyncTask<String, String, GooglePlusContactInfo>{

        String name;
        String emailAddress;

        public InfoLoader(String name, String emailAddress){
            this.name = name;
            this.emailAddress = emailAddress;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            c.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    et_name.setActivated(false);
                    pb_circle.setVisibility(View.VISIBLE);

                    LContact contact = new LContact(name, null, emailAddress);

                    contactPhotoLoader.loadImage(contact, iv_contact_photo);
                }
            });

        }

        @Override
        protected GooglePlusContactInfo doInBackground(String... params) {

            return GoogleContactsEdo.getInfoFromGooglePlus(c, emailAddress);
        }

        @Override
        protected void onPostExecute(final GooglePlusContactInfo result) {
            super.onPostExecute(result);

            c.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (result != null) {
                        if (JavaUtils.isNotEmpty(result.getNameSurname())) {
                            et_name.setText(result.getNameSurname());
                        }
                    }

                    et_name.setActivated(true);
                    pb_circle.setVisibility(View.GONE);
                }
            });
        }
    }




    public boolean isNew(){
        return isNew;
    }

    public void setName(String name){
        et_name.setText(name);
    }

    public String getName(){
        return et_name.getText().toString();
    }
    public void setEmailAddress(String emailAddress){
        et_emailaddress.setText(emailAddress);
    }

    public String getEmailAddress(){
        return et_emailaddress.getText().toString();
    }






    public void setBackPressed(View v){
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

//                    if (mViewPager.getCurrentItem()!=0) {
//                        mViewPager.setCurrentItem(mViewPager.getCurrentItem()-1);
//                        return true;
//                    }
                }
                return false;
            }
        } );
    }

}
