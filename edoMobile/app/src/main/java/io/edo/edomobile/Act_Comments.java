package io.edo.edomobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.DBConstants;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.CommentsUI;
import io.edo.ui.CommentsUIContacts;
import io.edo.ui.CommentsUIPersonal;
import io.edo.ui.EdoProgressDialog;
import io.edo.ui.UIManager;
import io.edo.ui.adapters.filepage.FilePageHolder;
import io.edo.ui.imageutils.ContactPhotoLoader;
import io.edo.ui.imageutils.FilePageLoader;
import io.edo.ui.imageutils.ThumbnailManager;
import io.edo.ui.material.ui.PageDetailActivity;
import io.edo.ui.material.widget.ObservableScrollView;
import io.edo.utilities.Constants;

/**
 * Created by PaoloBriganti on 03/07/15.
 */
public class Act_Comments extends PageDetailActivity implements UIManager.FilesUpdatesListener{

    FragmentActivity c;
    ThisApp app;

    private final static String LISTENER_ID = Act_Comments.class.getName();

    private EdoContainer container;
    private LSpace space;
    private LFile file;

    private CommentsUI commentsUI = null;

    private EdoProgressDialog loading;

    public LinearLayout ll_personalCommentsContainer;
    public LinearLayout ll_contactsCommentsContainer;

    private ImageView iv_image;

    private TextView headerTitle;

    private View mScrollViewChild;
    private ObservableScrollView mScrollView;
    private View mHeaderBox;
    private View mHeaderBackgroundBox;
    private View mHeaderShadow;
    private View mDetailsContainer;
    private View mPhotoViewContainer;
    private ImageView ib_fabButton;

    FilePageHolder vh;

    FilePageLoader filePageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_comments);

        overridePendingTransition(R.anim.translate_down_on, R.anim.translate_down_off);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);

        c = this;
        app = ThisApp.getInstance(c);

        onCreated(savedInstanceState);


        registerReceiver(mKillReceiver, new IntentFilter(Constants.ACTION_ACTIVITY_FINISH));

    }

    public void onCreated(Bundle savedInstanceState){

        iv_image = (ImageView) c.findViewById(R.id.iv_image);

        headerTitle = (TextView) c.findViewById(R.id.title);

        mScrollView = (ObservableScrollView) c.findViewById(R.id.scroll_view);
        mScrollViewChild = c.findViewById(R.id.scroll_view_child);
        mDetailsContainer = c.findViewById(R.id.details_container);
        mHeaderBox = c.findViewById(R.id.header);
        mHeaderBackgroundBox = c.findViewById(R.id.header_background);
        mHeaderShadow = c.findViewById(R.id.header_shadow);
        mPhotoViewContainer = c.findViewById(R.id.photo_container);
        ib_fabButton = (ImageButton) c.findViewById(R.id.ib_fabButton);

        vh = new FilePageHolder(mPhotoViewContainer);

        if (savedInstanceState == null) {

            String extraText = null;

            if (getIntent().getExtras().containsKey(DBConstants.CONTAINER)) {
                Serializable containerSerial = getIntent().getSerializableExtra(DBConstants.CONTAINER);
                if(containerSerial!=null) {
                    container = (EdoContainer) containerSerial;
                }else{
                    container = app.getUserContact();
                }
            }

            if (getIntent().getExtras().containsKey(DBConstants.SPACE)) {
                Serializable tabSerial = getIntent().getSerializableExtra(DBConstants.SPACE);
                if(tabSerial!=null) {
                    space = (LSpace) tabSerial;
                }
            }

            if (getIntent().getExtras().containsKey(DBConstants.FILE)) {
                Serializable fileSerial = getIntent().getSerializableExtra(DBConstants.FILE);
                if(fileSerial!=null) {
                    file = (LFile) fileSerial;
                    if(file!=null){
                        file = new LFileDAO(c).get(file.getId());
                    }
                }
            }

            ll_personalCommentsContainer = (LinearLayout) c.findViewById(R.id.personalCommentsContentLayout);
            ll_contactsCommentsContainer = (LinearLayout) c.findViewById(R.id.contactsCommentsContentLayout);

            if(container!=null && file!=null) {

                if(container.isPersonal(c)){
                    commentsUI = new CommentsUIPersonal(c,ll_personalCommentsContainer);
                }else{
                    commentsUI = new CommentsUIContacts(c,(LContact)container,
                            ContactPhotoLoader.getInstance(c),ll_contactsCommentsContainer, false);
                    ib_fabButton.setVisibility(View.GONE);
                }

                getActionBar().setTitle(file.getName());
            }else{
                finish();
            }

        }


        filePageLoader = FilePageLoader.getInstance(this);
        filePageLoader.setCropCenterImage(true);

        setFileViews();
//        if(contact == null)
//            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));


    }

    @Override
    protected void onResume() {
        super.onResume();

        app.uiManager.addFilesUpdatesListener(LISTENER_ID,this);
    }

    public void setFileViews(){
        if(file==null)
            Act_Comments.this.finish();

        c.runOnUiThread(new Runnable() {
            public void run() {
                headerTitle.setText(R.string.Comments);
                if(file.isSnippet()){
                    vh.showSnippetView(file,getSupportFragmentManager());
                }else{
                    filePageLoader.loadImage(space, file, vh);
                }

                int colorRes = ThumbnailManager.getColorRes(c, file.getvType());
                setHeaderColor(c.getResources().getColor(colorRes), true);
            }
        });
    }


    @Override
    public View getScrollViewChild() {
        return mScrollViewChild;
    }

    @Override
    public ObservableScrollView getScrollView() {
        return mScrollView;
    }

    @Override
    public View getFabButton() {
        return ib_fabButton;
    }

    @Override
    public View getHeaderBox() {
        return mHeaderBox;
    }

    @Override
    public View getHeaderBackgroundBox() {
        return mHeaderBackgroundBox;
    }

    @Override
    public View getHeaderShadow() {
        return mHeaderShadow;
    }

    @Override
    public View getDetailsContainer() {
        return mDetailsContainer;
    }

    @Override
    public View getPhotoViewContainer() {
        return mPhotoViewContainer;
    }

    @Override
    public TextView[] getTitles() {
        return null;
    }

    @Override
    public boolean isHeaderOverlay() {
        return true;
    }

    @Override
    public boolean hasPhoto() {
        return true;
    }

    @Override
    public void onSetupFinished() {
        super.onSetupFinished();
        commentsUI.setComments(file,true);
        getScrollView().fullScroll(View.FOCUS_DOWN);
    }

    public void bt_post(View v){
        commentsUI.postComment();
    }





    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTION BAR
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.snippet, menu);
//
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

//            case R.id.post:
//                bt_post(null);
//
//                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//            AUI.navigateUpTo(c, new Intent(c, Act_ContactActiveList.class));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        app.uiManager.removeListener(LISTENER_ID);
        try{unregisterReceiver(mKillReceiver);}catch(Exception e){}
    }
    private BroadcastReceiver mKillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            finish();
        }
    };

    @Override
    public void onInsert(EdoContainer container, LFile lFile) {

    }

    @Override
    public void onUpdate(EdoContainer container, LFile lFile) {
        if (file != null &&
                file.getId().equals(lFile.getId())) {

            commentsUI.setComments(lFile,true);

        }
    }

    @Override
    public void onDelete(EdoContainer container, LFile lFile) {
        if (file != null &&
                file.getId().equals(lFile.getId())) {

            this.finish();

        }
    }

    @Override
    public void onSyncList(LActivity a) {

    }

    @Override
    public void onUploadToCloud(EdoContainer container, LFile file, int progressStatus, int progressPercent) {

    }

    @Override
    public void onDownloadFromCloud(EdoContainer container, LFile file, int progressStatus, int progressPercent) {

    }


}
    
