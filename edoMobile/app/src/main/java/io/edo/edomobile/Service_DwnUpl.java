package io.edo.edomobile;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.paolobriganti.utils.UniversalProgressListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.edo.api.ApiFile;
import io.edo.api.ApiResponse;
import io.edo.api.Download;
import io.edo.api.FileDownloadInfo;
import io.edo.api.FileUploadInfo;
import io.edo.api.TransferOperation;
import io.edo.api.Upload;
import io.edo.api.google.GoogleRespCode;
import io.edo.db.global.EdoActivityManager;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.EdoNotification;
import io.edo.ui.EdoUI;
import io.edo.ui.UIManager;
import io.edo.utilities.Constants;

public class Service_DwnUpl extends Service{
	ThisApp app;

	Service c;
	EdoUI edoUI;
	private Handler handler;

	EdoActivityManager manager;

	public static final String ACTION_DOWNLOAD = "download";
	public static final String ACTION_UPLOAD = "upload";
	public static final String ACTION_UPLOAD_AND_SHARE = "uploadShare";

	private ExecutorService executorService  = Executors.newSingleThreadExecutor();

	public static final String SOURCE = "source";
	public static final String ACTION = "action";
	public static final String ACTION_STOP = "stop";

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		c = this;

		app = ThisApp.getInstance(this);

		app.asyncTaskMng = new AsyncTaskManager();

		manager = new EdoActivityManager(c);

		edoUI = new EdoUI(c);
	}

	public class Binder_DwnUpl extends Binder {
		public Service_DwnUpl getService() {
			// Return this instance of LocalService so clients can call public methods
			return Service_DwnUpl.this;
		}
	}


	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		handler = new Handler();


		if(intent!=null){

			Bundle extras = intent.getExtras();
			if (extras != null) {

				String action = extras.getString(ACTION);
				Object source = extras.getSerializable(SOURCE);


				if(action.equals(ACTION_DOWNLOAD)){
					Download download = (Download) source;

					LActivity a = sendActivity(null, download.container,
							LActivity.EVENT_DOWNLOAD_FROM_CLOUD, download.file,
							LActivity.STATUS_PENDING, 0,
							c.getResources().getString(R.string.downloadPending));

					download.a = a;

					AsyncDownloadFile asyncDwn = new AsyncDownloadFile(this, download);
					app.asyncTaskMng.put(download.file.getId(), asyncDwn);


				}else if(action.equals(ACTION_UPLOAD) || action.equals(ACTION_UPLOAD_AND_SHARE)){
					Upload upload = (Upload) source;

					LActivity a = sendActivity(upload.a, upload.container,
							LActivity.EVENT_UPLOAD_TO_CLOUD, upload.file,
							LActivity.STATUS_PENDING, 0,
							c.getResources().getString(R.string.uploadPending));
					upload.a = a;

					AsyncUpload asyncUpl = new AsyncUpload(this, upload);
					app.asyncTaskMng.put(upload.file.getId(), asyncUpl);

//				}else if(action.equals(ACTION_UPLOAD_AND_SHARE)){
//					Upload upload = (Upload) source;
//					AsyncUploadShare asyncUplShare = new AsyncUploadShare(this,  upload);
//					app.asyncTaskMng.put(upload.file.getId(), asyncUplShare);
//
//					EdoUIUpdate uiUpdate = new EdoUIUpdate(null,  
//							EdoUIUpdate.EVENT_UPLOAD_TO_CLOUD, EdoUIUpdate.PROGRESS_STATUS_PENDING, 0, upload.file, 
//							c.getResources().getString(R.string.uploadPending));
//					EdoUIUpdateManager.sendUpdate(c, upload.file.getId(), uiUpdate);

				}else if(action.equals(ACTION_STOP)){

					app.asyncTaskMng.stop((String)source, false);
				}
			}
		}





		return START_STICKY;
	}


	public class AsyncTaskManager {

		public Map<String, TransferTask> operations=Collections.synchronizedMap(new HashMap<String,TransferTask>());

		public synchronized void put(String id, TransferTask task){
			operations.put(id, task);
			task.executeOnExecutor(executorService, "");
			app.setTransferServiceOperationsCount(getSize());
		}

		public synchronized void stop(String id, boolean isFinished){

			if(operations!=null){
				TransferTask task = operations.get(id);

				if(task!=null){

					task.cancel(true);

					if(!isFinished){


						if(task.getProgressListener()!=null){
							task.getProgressListener().onStopByUser();
						}
						task.setCloseStream(true);
					}

				}

				operations.remove(id);
			}


			app.setTransferServiceOperationsCount(getSize());



			if(operations==null || operations.size()<1){
				Service_DwnUpl.this.stopSelf();
			}
		}

		public boolean isTaskInPending(String id){
			return operations.get(id)!=null;
		}

		public int getSize(){
			if(operations!=null){
				return operations.size();
			}
			return 0;
		}
	}


	public class TransferTask extends AsyncTask<String, String, String>{
		public String id = null;
		boolean closeStream = false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if(app.asyncTaskMng!=null){
				if(!app.asyncTaskMng.isTaskInPending(id)){
					this.cancel(true);
				}
			}
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			return null;
		}

		public UniversalProgressListener getProgressListener(){
			return null;
		}

		public boolean closeStream() {
			return closeStream;
		}

		public void setCloseStream(boolean closeStream) {
			this.closeStream = closeStream;
		}



	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		app.setTransferServiceOperationsCount(0);
		app.asyncTaskMng = null;
	}





	//*********************************************************************
	//*		UPLOAD
	//*********************************************************************
	public static synchronized void upload(final Context c, LSpace tab,  LFile file, LActivity a){

		Upload upload = new Upload(ThisApp.getInstance(c).getUserContact(), tab, file, true);
		upload.a = a;
		startOperation(c, ACTION_UPLOAD, upload);
	}

	public static synchronized void uploadAndShare(final Context c, LContact contact, LSpace tab, LFile file, LActivity a){
		Upload upload = new Upload(contact, tab, file, contact.isEdoUser());
		upload.a = a;
		startOperation(c, ACTION_UPLOAD_AND_SHARE, upload);
	}




	public class AsyncUpload extends TransferTask{
		UploadProgressListener listener;
		Context c;
		FileUploadInfo fui;
		Upload upload;

		public AsyncUpload(Context c, Upload upload) {
			super();
			this.c = c;
			this.upload = upload;
			this.id = upload.file.getId();

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if(listener!=null)listener.onPreStart(null, null);

		}

		@Override
		protected String doInBackground(String... params) {
			fui = new FileUploadInfo(upload.file.getLocalUri());
			fui.setUploadId(upload.uploadId);
			fui.setRange(upload.range);
			fui.a = upload.a;

			listener = new UploadProgressListener(c, fui, upload, this);

			EdoContainer container = upload.container;

			ApiResponse apiResp = ApiFile.uploadFile(c, container, upload.tab, upload.file, fui, listener);
			if(apiResp!=null && apiResp.errorCode!=null && apiResp.errorCode==GoogleRespCode.ERROR_FILE_NOT_FOUND){
				if(listener!=null)listener.onError(apiResp.errorCode);
			}else if(apiResp!=null && apiResp.errorCode!=null){
				if(listener!=null)listener.onError(apiResp.errorCode);

				if(apiResp.result!=null && apiResp.result instanceof FileUploadInfo){
					fui = (FileUploadInfo) apiResp.result;

					upload.uploadId = fui.getUploadId();
					upload.range = fui.getRange();
				}

				TransferOperation operation = new TransferOperation(upload.file.getId(), ACTION_UPLOAD, upload);
				app.failedOperations.put(operation.id, operation);
			}else if(apiResp==null){
				if(listener!=null)listener.onInterrupt();

				TransferOperation operation = new TransferOperation(upload.file.getId(), ACTION_UPLOAD, upload);
				app.failedOperations.put(operation.id, operation);
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if(app.asyncTaskMng!=null)
				app.asyncTaskMng.stop(upload.file.getId(), true);

		}

		@Override
		public UniversalProgressListener getProgressListener() {
			// TODO Auto-generated method stub
			return listener;
		}
	}





	//*********************************************************************
	//*		UPLOAD AND SHARE
	//*********************************************************************
//	public class AsyncUploadShare extends TransferTask{
//		public UploadProgressListener listener;
//		Context c;
//		Upload upload;
//		FileUploadInfo fui;
//
//		public AsyncUploadShare(Context c, Upload upload) {
//			super();
//			this.c = c;
//			this.upload = upload;
//			this.id = upload.file.getId();
//		}
//
//		@Override
//		protected void onPreExecute() {
//			// TODO Auto-generated method stub
//			super.onPreExecute();
//			if(listener!=null)listener.onPreStart(null, null);
//
//		}
//
//		@Override
//		protected String doInBackground(String... params) {
//			fui = new FileUploadInfo(upload.file.getLocalUri());
//
//			listener = new UploadProgressListener(c, upload, this);
//
//			Object container = null;
//			if(upload.category != null){
//				container = upload.category;
//			}else if(upload.contact != null){
//				container = upload.contact;
//			}
//
//			ApiResponse apiResp = ApiFile.uploadShareFile(c,app.getDB(), container, upload.tab, upload.file, !upload.isEdoUser, fui, listener);
//
//			if(apiResp!=null && apiResp.errorCode!=null && apiResp.errorCode==GoogleRespCode.ERROR_FILE_NOT_FOUND){
//				if(listener!=null)listener.onError(apiResp.errorCode);
//			}else if(apiResp==null || (apiResp!=null && apiResp.errorCode!=null)){
//				if(listener!=null)listener.onError(null);
//				TransferOperation operation = new TransferOperation(upload.file.getId(), ACTION_UPLOAD_AND_SHARE, upload);
//				app.failedOperations.put(operation.id, operation);
//			}
//
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(String result) {
//			// TODO Auto-generated method stub
//			super.onPostExecute(result);
//			
//			if(app.asyncTaskMng!=null)
//				app.asyncTaskMng.stop(upload.file.getId(), true);
//
//		}
//
//		@Override
//		public UniversalProgressListener getProgressListener() {
//			// TODO Auto-generated method stub
//			return listener;
//		}
//	}












	private class UploadProgressListener implements UniversalProgressListener {
		Context c;
		EdoNotification noty;
		Upload upload;
		TransferTask asyncTask;
		boolean isStopped = false;
		FileUploadInfo fui;

		public UploadProgressListener(Context context, FileUploadInfo fui, Upload upload, TransferTask asyncTask) {
			super();
			this.c = context;
			this.fui = fui;
			this.upload = upload;
			this.noty = new EdoNotification(c,null);
			this.asyncTask = asyncTask;

			noty.buildFileTransferNotification(EdoNotification.ID_UPLOAD, upload.file, c.getResources().getString(R.string.uploadPending));
			noty.showNotification();
		}


		@Override
		public void onPreStart(InputStream is, OutputStream os) {

			isStopped = false;
			if(asyncTask.closeStream()){
				if(is!=null){
					try {
						is.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					is = null;
				}
				onStopByUser();
				return;
			}



			sendActivity(upload.a, upload.container,
					LActivity.EVENT_UPLOAD_TO_CLOUD, upload.file,
					LActivity.STATUS_PENDING, 0,
					c.getResources().getString(R.string.uploadPending));




		}



		@Override
		public void onStart(InputStream is, OutputStream os) {

			isStopped = false;
			if(asyncTask.closeStream()){
				if(is!=null){
					try {
						is.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					is = null;
				}
				onStopByUser();
				return;
			}

			sendActivity(upload.a, upload.container,
					LActivity.EVENT_UPLOAD_TO_CLOUD, upload.file,
					LActivity.STATUS_STARTED, 0,
					c.getResources().getString(R.string.starting));

			noty.showProgress(0, true, null, c.getResources().getString(R.string.uploadInProgress));

			//			handler.post(new Runnable() {public void run() {
			//
			//				edoUI.showToastShort(c.getResources().getString(R.string.uploadPosOfTot)
			//						.replace(Constants.PH_FILENAME, upload.file.getNameWithExt())
			//						.replace(Constants.PH_TOT, ""+(app.asyncTaskMng.getSize()-1)), null);
			//			}});

		}



		@Override
		public void onProgressChanged(InputStream is, OutputStream os, int progress) {


			if(asyncTask.closeStream()){
				if(is!=null){
					try {
						is.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					is = null;
				}
				onStopByUser();
				return;
			}

			sendActivity(upload.a, upload.container,
					LActivity.EVENT_UPLOAD_TO_CLOUD, upload.file,
					LActivity.STATUS_IN_PROGRESS, progress,
					c.getResources().getString(R.string.uploadInProgress));

			noty.showProgress(progress, false, null, c.getResources().getString(R.string.uploadInProgress)+" "+progress+"/100");
		}


		@Override
		public void onStopByUser() {
			isStopped = true;

			fui.setStopUpload(true);


			sendActivity(upload.a, upload.container,
					LActivity.EVENT_UPLOAD_TO_CLOUD, upload.file,
					LActivity.STATUS_STOP, 0,
					c.getResources().getString(R.string.uploadStopped));

			noty.showProgress(100, false, null, c.getResources().getString(R.string.uploadStopped));
		}



		@Override
		public void onError(Integer errorCode) {
			if(!isStopped){
				handler.post(new Runnable() {public void run() {
					String message = c.getResources().getString(R.string.uploadError);
					edoUI.showToastShort(message, null);
					sendActivity(upload.a, upload.container,
							LActivity.EVENT_UPLOAD_TO_CLOUD, upload.file,
							LActivity.STATUS_ERROR, 0,
							message);
					noty.showProgress(100, false, null, message);
				}});

			}
		}



		@Override
		public void onFinish() {
			sendActivity(upload.a, upload.container,
					LActivity.EVENT_UPLOAD_TO_CLOUD, upload.file,
					LActivity.STATUS_COMPLETE, 100, null);

			EdoNotification.cancelNotification(c, EdoNotification.ID_TRANSFER);

		}



		@Override
		public void onPostFinish() {
			//			if(asyncTask.isCancelled()){
			//				onStop();
			//				return;
			//			}
		}


		@Override
		public void onInterrupt() {
			handler.post(new Runnable() {
				public void run() {
					String message = c.getResources().getString(R.string.uploadError);
					edoUI.showToastShort(message, null);
					sendActivity(upload.a, upload.container,
							LActivity.EVENT_UPLOAD_TO_CLOUD, upload.file,
							LActivity.STATUS_ERROR, 0,
							message);
					noty.showProgress(100, false, null, message);
				}
			});

		}
	}













































	//*********************************************************************
	//*		DOWNLOAD
	//*********************************************************************
	public synchronized void download(Context c, EdoContainer container, LSpace tab,
									  String folderPath, LFile file){
		startOperation(c, ACTION_DOWNLOAD, new Download(container, tab, folderPath, file));
	}




	public class AsyncDownloadFile extends TransferTask{
		DownloadProgressListener listener;
		Context c;
		Download download;
		FileDownloadInfo fdi;

		boolean removeUpdates=true;

		public AsyncDownloadFile(Context c, Download download) {
			super();
			this.c = c;
			this.download = download;
			this.id = download.file.getId();

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if(listener!=null)listener.onPreStart(null, null);
		}

		@Override
		protected String doInBackground(String... params) {
			fdi = new FileDownloadInfo(download.folderPath, download.file.getName(), download.file.getExtension());
			fdi.a = download.a;

			listener = new DownloadProgressListener(c, download, this);
			ApiResponse apiResp = ApiFile.downloadFile(c, download.container, download.tab, download.file, fdi, listener);
			if(apiResp!=null && apiResp.errorCode!=null && apiResp.errorCode==GoogleRespCode.ERROR_FILE_NOT_FOUND){
				if(listener!=null)listener.onError(apiResp.errorCode);
			}else if(apiResp!=null && apiResp.errorCode!=null){
				if(listener!=null)listener.onError(null);
				TransferOperation operation = new TransferOperation(download.file.getId(), ACTION_DOWNLOAD, download);
				app.failedOperations.put(operation.id, operation);
			}else if(apiResp==null){

				TransferOperation operation = new TransferOperation(download.file.getId(), ACTION_DOWNLOAD, download);
				app.failedOperations.put(operation.id, operation);
				if(listener!=null)listener.onInterrupt();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if(app.asyncTaskMng!=null)
				app.asyncTaskMng.stop(download.file.getId(), true);

		}


		@Override
		public UniversalProgressListener getProgressListener() {
			// TODO Auto-generated method stub
			return listener;
		}
	}






	class DownloadProgressListener implements UniversalProgressListener {
		Context c;
		Download download;
		EdoNotification noty;
		TransferTask asyncTask;
		boolean isStopped;
		OutputStream outputStream;

		public DownloadProgressListener(Context context, Download download, TransferTask asyncTask) {
			super();
			this.c = context;
			this.download = download;
			this.noty = new EdoNotification(c,null);
			this.asyncTask = asyncTask;

			noty.buildFileTransferNotification(EdoNotification.ID_DOWNLOAD, download.file, c.getResources().getString(R.string.downloadPending));
			noty.showNotification();
		}



		@Override
		public void onPreStart(InputStream is, OutputStream os) {
			isStopped = false;

			if(os!=null){
				outputStream = os;
			}

			if(asyncTask.closeStream()){
				if(os!=null){
					try {
						os.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					os = null;
				}
				onStopByUser();
				return;
			}

			sendActivity(download.a, download.container,
					LActivity.EVENT_DOWNLOAD_FROM_CLOUD, download.file,
					LActivity.STATUS_PENDING, 0,
					c.getResources().getString(R.string.starting));

		}



		@Override
		public void onStart(InputStream is, OutputStream os) {
			isStopped = false;

			if(os!=null){
				outputStream = os;
			}

			if(asyncTask.closeStream()){
				if(os!=null){
					try {
						os.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					os = null;
				}
				onStopByUser();
				return;
			}



			//			handler.post(new Runnable() {public void run() {
			//				edoUI.showToastShort(c.getResources().getString(R.string.downloadPosOfTot)
			//						.replace(Constants.PH_FILENAME, download.file.getNameWithExt())
			//						.replace(Constants.PH_TOT, ""+(app.asyncTaskMng.getSize()-1)), null);
			//			}});

		}



		@Override
		public void onProgressChanged(InputStream is, OutputStream os, int progress) {
			if(os!=null){
				outputStream = os;
			}

			if(asyncTask.closeStream()){
				if(os!=null){
					try {
						os.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					os = null;
				}
				onStopByUser();
				return;
			}

			sendActivity(download.a, download.container,
					LActivity.EVENT_DOWNLOAD_FROM_CLOUD, download.file,
					LActivity.STATUS_IN_PROGRESS, progress,
					c.getResources().getString(R.string.downloadInProgress));

			noty.showProgress(progress, false, null, c.getResources().getString(R.string.downloadInProgress)+" "+progress+"/100");
		}



		@Override
		public void onError(Integer errorCode) {
			if(!isStopped){
				if(errorCode!=null && errorCode==GoogleRespCode.ERROR_FILE_NOT_FOUND){
					handler.post(new Runnable() {public void run() {
						String message = c.getResources().getString(R.string.fileNotFoundInCloud)
								.replace(Constants.PH_FILENAME, download.file.getNameWithExt());
						edoUI.showToastShort(message, null);

						sendActivity(download.a, download.container,
								LActivity.EVENT_DOWNLOAD_FROM_CLOUD, download.file,
								LActivity.STATUS_ERROR, 0,
								message);

						noty.showProgress(100, false, null, message);
					}});
				}else{
					handler.post(new Runnable() {public void run() {
						String message = c.getResources().getString(R.string.downloadError);
						edoUI.showToastShort(message, null);

						sendActivity(download.a, download.container,
								LActivity.EVENT_DOWNLOAD_FROM_CLOUD, download.file,
								LActivity.STATUS_ERROR, 0,
								message);

						noty.showProgress(100, false, null, message);
					}});

				}
			}
		}



		@Override
		public void onFinish() {

			sendActivity(download.a, download.container,
					LActivity.EVENT_DOWNLOAD_FROM_CLOUD, download.file,
					LActivity.STATUS_COMPLETE, 100,
					null);
		}



		@Override
		public void onPostFinish() {
			// TODO Auto-generated method stub
			LFile file = new LFileDAO(c).get(download.file.getId());
			if(file.isStored()){
				noty.setFileIntent(file);
				noty.showProgress(100, false, null, c.getResources().getString(R.string.downloadComplete));
			}else{
				EdoNotification.cancelNotification(c, EdoNotification.ID_TRANSFER);
			}
		}



		@Override
		public void onStopByUser() {


			try {
				if(outputStream!=null){
					outputStream.close();
					outputStream = null;
				}

				sendActivity(download.a, download.container,
						LActivity.EVENT_DOWNLOAD_FROM_CLOUD, download.file,
						LActivity.STATUS_STOP, 0,
						null);

				noty.showProgress(0, false, null, c.getResources().getString(R.string.downloadStopped));

				isStopped = true;

			} catch (IOException e) {
				e.printStackTrace();
			}
		}



		@Override
		public void onInterrupt() {

			try {
				if(outputStream!=null){
					outputStream.close();
					outputStream = null;
				}

				sendActivity(download.a, download.container,
						LActivity.EVENT_DOWNLOAD_FROM_CLOUD, download.file,
						LActivity.STATUS_STOP, 0,
						null);

				noty.showProgress(0, false, null, c.getResources().getString(R.string.downloadStopped));

				isStopped = true;

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}




























	public LActivity sendActivity(LActivity originalActivity, EdoContainer container, int event, LFile resource,
								  int status, int progressPercent, String message){

		LActivity a = new LActivity(LActivity.getContext(c,container), container, app.getUserContact(),
				event, LActivity.RESOURCE_FILE, resource,
				status, progressPercent,
				message);

		if(originalActivity!=null){
			a.setId(originalActivity.getId());
		}
		return manager.processActivity(a,false);
	}







	//*********************************************************************
	//*		START
	//*********************************************************************
	public static synchronized void startOperation(final Context c, String action, Serializable source){
		Intent i = new Intent(c, Service_DwnUpl.class);

		i.putExtra(ACTION, action);
		i.putExtra(SOURCE, source);
		c.startService(i);
	}



	//*********************************************************************
	//*		STOP
	//*********************************************************************
	public static void stop(Context c, String fileId){
		Intent i = new Intent(c, Service_DwnUpl.class);
		i.putExtra(ACTION, ACTION_STOP);
		i.putExtra(SOURCE, fileId);
		c.startService(i);


		LActivity a = UIManager.getActivityByResourceId(c,fileId);
		if(a!=null && (a.isUploadEvent() || a.isDownloadEvent())){
			a.setStatus(LActivity.STATUS_STOP);
			new EdoActivityManager(c).processActivity(a, false, false);
			ThisApp app = ThisApp.getInstance(c);
			app.uiManager.removeActivityByResourceId(fileId);
		}

	}




}	
