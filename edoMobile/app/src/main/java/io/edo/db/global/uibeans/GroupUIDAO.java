package io.edo.db.global.uibeans;

import android.content.Context;

import java.util.ArrayList;

import io.edo.db.global.beans.GroupDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LGroupMemberDAO;
import io.edo.db.local.beans.support.EdoResource;
import io.edo.db.web.beans.WEntity;
import io.edo.ui.UIManager;

public class GroupUIDAO extends BaseUIDAO{
    GroupDAO groupDAO;

    public GroupUIDAO(Context context) {
        super(context);
        groupDAO = new GroupDAO(context);
    }

    @Override
    public LGroupMemberDAO getLDAO() {
        return groupDAO.getLDAO();
    }

    public LContact add(LContact group) {

        WEntity entity = groupDAO.add(group);
        if(entity!=null){

            if(entity.act!=null) {
                entity.act.setResource(group);
                entity.act = processActivity(entity.act);
            }

            return entity.group;
        }
        return null;
    }


    public LContact updateGroupMetadata(LContact group, boolean hasRename) {
        WEntity entity = groupDAO.updateGroupMetadata(group, hasRename);
        if(entity!=null){

            if(entity.act!=null) {
                entity.act.setResource(group);
                entity.act = processActivity(entity.act);
            }

            return entity.group;
        }
        return null;
    }

    public LContact[] addContactsToGroup(LContact group, ArrayList<LContact> members) {
        return groupDAO.addContactsToGroup(group,members);
    }


    public LContact delete(LContact group) {
        WEntity entity = groupDAO.delete(group);
        if(entity!=null){

            if(entity.act!=null)
                entity.act = processActivity(entity.act);

            return entity.group;
        }
        return null;
    }


    public boolean deleteContactFromGroup(LContact group, LContact member){

        return groupDAO.deleteContactFromGroup(group,member);
    }

    public boolean leaveTheGroup(LContact group){

        return groupDAO.leaveTheGroup(group);
    }


    public LContact download(String id){
        return groupDAO.download(id);
    }



    public LContact[] downloadAll(){
        LContact[] contacts = groupDAO.downloadAll();

        LActivity a = new LActivity();
        a.setEvent(LActivity.EVENT_SYNC_ALL);
        a.setStatus(LActivity.STATUS_COMPLETE);
        app.uiManager.syncResourcesList(a, UIManager.ContactsUpdatesListener.class);

        return contacts;
    }







    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTIVITY
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public LActivity getFakeActivity(int event, EdoResource resource){
        LActivity a = new LActivity(LActivity.CONTEXT_PERSONAL, event, LActivity.RESOURCE_GROUP);
        a.setResource(resource);
        return a;
    }


}
