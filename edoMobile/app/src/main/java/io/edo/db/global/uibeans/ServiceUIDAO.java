package io.edo.db.global.uibeans;

import android.content.Context;

import io.edo.db.global.beans.BaseDAO;
import io.edo.db.global.beans.ServiceDAO;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.LServiceDAO;

public class ServiceUIDAO extends BaseDAO{
    ServiceDAO serviceDAO;

    public ServiceUIDAO(Context context) {
        super(context);
        serviceDAO = new ServiceDAO(context);
    }

    @Override
    public LServiceDAO getLDAO() {
        return serviceDAO.getLDAO();
    }

    public boolean addService(final LService lService){
        return serviceDAO.addService(lService);
    }

    public boolean updateService(final LService lService){
        return updateService(lService);
    }

    public boolean removeService(final LService lService){
        return removeService(lService);
    }

    public LService[] downloadAllServices(){
        return downloadAllServices();
    }




}
