package io.edo.db.local.beans.support;


public class DBConstants {
	public final static String CONTACT_ID = "contact_id";
	public final static String CATEGORY_ID = "category_id";
	public final static String CATEGORY_STATE = "category_state";
	public final static String FILE_ID = "note_id";
	public final static String FILE_STATE = "file_state";
	public final static String CATEGORY = "Category";
	public final static String EDOCONTACT = "EdoContact";
	public final static String CONTAINER = "container";
	public final static String FILE = "EdoLFile";
	public final static String SPACE = "LSpace";
	public final static String NOTE_STATE = "note_state";
	public final static String TAB_ID = "tab_id";
	public final static String FOLDER_ID = "folder_id";
	public final static String TAB_STATE = "tab_state";
	public static final String SERVICE_ID = "SERVICE_ID";
	public static final String SERVICE = "EdoLService";
	public final static String PUSH_MSG = "LActivity";

}
