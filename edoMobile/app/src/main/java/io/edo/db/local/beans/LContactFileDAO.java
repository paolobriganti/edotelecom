package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.support.LSpace;

public class LContactFileDAO extends LBaseDAO<LContactFile,LContactFile,ArrayList<LSpace>,LContact> {

    String TABLE_NAME = LocalDBHelper.TABLE_CONTACT_FILE;

    private String[] keys = {LocalDBHelper.KEY_CONTACTID,
            LocalDBHelper.KEY_FILEID};


    public LContactFileDAO(Context context) {
        super(context);
    }

    @Override
    public ContentValues createContentValues(LContactFile contactFile) {
        ContentValues values = new ContentValues();
        if(contactFile.getContactId()!=null)values.put( LocalDBHelper.KEY_CONTACTID, contactFile.getContactId() );
        if(contactFile.getFileId()!=null)values.put( LocalDBHelper.KEY_FILEID, contactFile.getFileId() );
        return values;
    }

    @Override
    public synchronized boolean insert(LContactFile contactFile) {
        if(new LFileDAO(context).insertFromWebServer(contactFile.getFile())){
            ContentValues initialValues = createContentValues(contactFile);
            long lid = db.insert(TABLE_NAME, initialValues);
            return lid>0;
        }
        return false;
    }

    public synchronized boolean insert(LContact contact, LFile file){
        return insert(new LContactFile(contact,file));
    }

    @Override
    public synchronized boolean insertFromWebServer(LContactFile contactFile) {
        if(contactFile.getFile()!=null && contactFile.getContact()!=null) {
            return new LContactFileDAO(context).insert(contactFile);
        }
        return false;
    }

    public synchronized boolean insertFromWebServer(LContact contact, LFile file){
        return insertFromWebServer(new LContactFile(contact, file));
    }


    @Override
    public synchronized boolean update(LContactFile contactFile) {
        ContentValues updatedValues = createContentValues(contactFile);

        String whereClause = LocalDBHelper.KEY_CONTACTID+"= '"+contactFile.getContactId()+"' AND "+
                LocalDBHelper.KEY_FILEID+"= "+contactFile.getFileId();
        boolean updated = db.update(TABLE_NAME, updatedValues, whereClause);

        return updated;
    }

    @Override
    public synchronized boolean delete(LContactFile contactFile) {
        String whereClause = LocalDBHelper.KEY_FILEID+" = "+ contactFile.getFileId();
        boolean deleted = db.delete(TABLE_NAME, whereClause);

        return deleted;
    }

    @Override
    public synchronized LContactFile get(LContactFile contactFile) {

        LContactFile oldContactFile = null;

        String whereClause = LocalDBHelper.KEY_CONTACTID+"= '?' AND "+
                LocalDBHelper.KEY_FILEID+"= ?";
        Cursor cursor = db.select(TABLE_NAME, keys, whereClause, new String[]{contactFile.getContactId(), contactFile.getFileId()});
        try {

            if(cursor.moveToNext()){
                oldContactFile = new LContactFile(cursor);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }



        return oldContactFile;
    }

    @Override
    public synchronized ArrayList<LSpace> getList() {
        return getListBy(app.getUserContact());
    }

    @Override
    public ArrayList<LSpace> getListBy(LContact contact) {
        ArrayList<LSpace> spaces = new ArrayList<LSpace>();



        if(contact!=null && contact.getId()!=null){

            String query = "SELECT * FROM "+LocalDBHelper.TABLE_CONTACT_FILE+" cf INNER JOIN "+LocalDBHelper.TABLE_FILE+" f " +
                    "ON cf."+LocalDBHelper.KEY_FILEID+"=f."+LocalDBHelper.KEY_ID+" " +
                    "WHERE cf."+LocalDBHelper.KEY_CONTACTID+"=? AND " +
                    "f."+LocalDBHelper.KEY_TYPE+" == '"+LFile.FILE_TYPE_EDOTAB+"' AND " +
                    "f."+LocalDBHelper.KEY_STATUS+" IS NOT '"+LFile.STATUS_DEFAULT+"' " +
                    "ORDER BY f."+LocalDBHelper.KEY_ID+" DESC";


            Cursor cursor = db.executeQuery(query, new String[]{String.valueOf(contact.getId())});
            try {

                while(cursor.moveToNext()){
                    LSpace space = new LSpace(cursor);
                    spaces.add(space);
                }

            } finally {
                if(cursor != null)
                    cursor.close();
            }

        }

        return spaces;
    }


    public synchronized LContact getContactOfFile(LFile file){
        String query = "SELECT * "+

                "FROM "+LocalDBHelper.TABLE_CONTACT_FILE+
                " cf INNER JOIN "+LocalDBHelper.TABLE_CONTACT+" c " +
                "ON cf."+LocalDBHelper.KEY_CONTACTID+"=c."+LocalDBHelper.KEY_ID+" " +

                "WHERE cf."+LocalDBHelper.KEY_FILEID+"=?";

        if(file!=null && file.getId()!=null){
            LContact contact = null;
            Cursor cursor = db.executeQuery(query, new String[]{String.valueOf(file.getId())});
            try {

                if(cursor.moveToNext()){
                    contact = new LContact(context, cursor);
                }

            } finally {
                if(cursor != null)
                    cursor.close();
            }
            return contact;
        }

        return null;
    }

    public ArrayList<LFile> getListByTipeOf(LContact contact, String fileType) {
        ArrayList<LFile> files = new ArrayList<LFile>();



        if(contact!=null && contact.getId()!=null){

            String query = "SELECT * FROM "+LocalDBHelper.TABLE_CONTACT_FILE+" cf INNER JOIN "+LocalDBHelper.TABLE_FILE+" f " +
                    "ON cf."+LocalDBHelper.KEY_FILEID+"=f."+LocalDBHelper.KEY_ID+" " +
                    "WHERE cf."+LocalDBHelper.KEY_CONTACTID+"=? AND " +
                    "f."+LocalDBHelper.KEY_TYPE+" == ? " +
                    "ORDER BY f."+LocalDBHelper.KEY_ID+" DESC";


            Cursor cursor = db.executeQuery(query, new String[]{String.valueOf(contact.getId()),fileType});
            try {

                while(cursor.moveToNext()){
                    files.add(new LFile(cursor));
                }

            } finally {
                if(cursor != null)
                    cursor.close();
            }

        }

        return files;
    }

    public ArrayList<LFile> getFileListBy(LContact contact) {
        ArrayList<LFile> files = new ArrayList<LFile>();



        if(contact!=null && contact.getId()!=null){

            String query = "SELECT * FROM "+LocalDBHelper.TABLE_CONTACT_FILE+" cf INNER JOIN "+LocalDBHelper.TABLE_FILE+" f " +
                    "ON cf."+LocalDBHelper.KEY_FILEID+"=f."+LocalDBHelper.KEY_ID+" " +
                    "WHERE cf."+LocalDBHelper.KEY_CONTACTID+"=? AND " +
                    "f."+LocalDBHelper.KEY_TYPE+" IS NOT '"+LFile.FILE_TYPE_EDOTAB+"' AND "+
                    "f."+LocalDBHelper.KEY_TYPE+" IS NOT '"+LFile.FILE_TYPE_EDOFOLDER+"' "+
                    "ORDER BY f."+LocalDBHelper.KEY_ID+" DESC";


            Cursor cursor = db.executeQuery(query, new String[]{String.valueOf(contact.getId())});
            try {

                while(cursor.moveToNext()){
                    files.add(new LFile(cursor));
                }

            } finally {
                if(cursor != null)
                    cursor.close();
            }

        }

        return files;
    }

}
