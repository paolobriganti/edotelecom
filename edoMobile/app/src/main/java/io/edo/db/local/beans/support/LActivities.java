package io.edo.db.local.beans.support;

import android.content.Context;

import com.paolobriganti.android.VU;

import java.util.ArrayList;
import java.util.HashMap;

import io.edo.db.local.beans.LActivity;
import io.edo.edomobile.R;
import io.edo.utilities.Constants;

public class LActivities {
	private ArrayList<LActivity> activities;
	private int unreadNumber = 0;

	private String lastNotificationUnread;

	public static final String ID_NEW_EVENTS = "NEW_EVENTS";
	public static final String ID_BLANK_SHEET = "BLANK_SHEET";


	public LActivities() {
		super();
		this.activities = new ArrayList<LActivity>();
		this.unreadNumber = 0;
	}

	public LActivities(ArrayList<LActivity> activities, int unreadNumber) {
		super();
		this.activities = activities;
		this.unreadNumber = unreadNumber;
	}

	public ArrayList<LActivity> getactivities() {
		return activities;
	}

	public void setactivities(ArrayList<LActivity> activities) {
		this.activities = activities;
	}

	public int getUnreadNumber() {
		return unreadNumber;
	}

	public void setUnreadNumber(int unreadNumber) {
		this.unreadNumber = unreadNumber;
	}

	public String getLastNotificationUnread() {
		return lastNotificationUnread;
	}

	public void setLastNotificationUnread(String lastNotificationUnread) {
		this.lastNotificationUnread = lastNotificationUnread;
	}

	public static final int MAX_AGGREGATION_TIME = 3600000;

	public void add(LActivity activity){
		if(activities==null || activity==null)
			return;

		activities.add(activity);
	}

	private HashMap<String, LActivity> newActivitiesByResources = new HashMap<String, LActivity>();

	public boolean addActivityToDescList(Context c, LActivity currentActivity) {
		if (activities == null || currentActivity == null)
			return false;

		if (currentActivity.isChatEvent()) {
			LActivity activityUpdated = null;

			if (activities.size() > 0) {
				LActivity newLActivity = activities.get(activities.size() - 1);
				LActivity oldLActivity = currentActivity;

				if (oldLActivity.isChatEvent() && !oldLActivity.hasError() &&
						newLActivity.isChatEvent() && !newLActivity.hasError() &&
						newLActivity.getTimeStamp() - oldLActivity.getTimeStamp() < MAX_AGGREGATION_TIME) {

//					if (oldLActivity.getSenderId().equals(newLActivity.getSenderId())) {
//
//
//						String desc = oldLActivity.getMessage() + "\n" + newLActivity.getAggregationMessage();
//
//						newLActivity.setAggregationMessage(desc);
//
//						activityUpdated = newLActivity;
//					}

				}

			}

			if (activityUpdated != null) {
				activities.remove(activities.size() - 1);
			} else {
				activityUpdated = currentActivity;
			}

			activities.add(activityUpdated);

			return true;

		} else {

			LActivity newActivity = newActivitiesByResources.get(currentActivity.getResourceId());
			if(newActivity!=null){
				currentActivity.setVisualizationThanNewActivity(newActivity);
			}else{
				currentActivity.setVisualization(currentActivity.getDefVisualization());
				if (!currentActivity.isHiddenVisualization()) {
					newActivitiesByResources.put(currentActivity.getResourceId(), currentActivity);
				}
			}

			if (!currentActivity.isHiddenVisualization()) {

				activities.add(currentActivity);

				return true;
			} else {
				return false;
			}
		}
//		activities.add(currentActivity);
//		return true;
	}




	public boolean updateNewAndRemoveLast(Context c, LActivity lastLActivity, LActivity newLActivity){

		if (newLActivity.isChatEvent()) {

			if (activities.size() > 0) {
				LActivity oldLActivity = lastLActivity;

				if (oldLActivity.isChatEvent() && oldLActivity.hasBeenProcessed() &&
						newLActivity.isChatEvent() && newLActivity.hasBeenProcessed() &&
						newLActivity.getTimeStamp() - oldLActivity.getTimeStamp() < MAX_AGGREGATION_TIME) {

//					if (oldLActivity.getSenderId().equals(newLActivity.getSenderId())) {
//						String desc = oldLActivity.getAggregationMessage() + "\n" + newLActivity.getAggregationMessage();
//
//						newLActivity.setAggregationMessage(desc);
//
//						return true;
//					}

				}

			}

			return false;

		} else {

//			if(newLActivity.isCommentEvent())
//				newLActivity.getFileResource(c);

			if (lastLActivity.isTheSameTypeOf(newLActivity)) {
				return true;
			}
		}

		return false;
	}







	public static LActivity getNewEventsMsg(Context c, int unreadNumber){
		LActivity a = new LActivity();
		a.setId(ID_NEW_EVENTS);
		a.setResourceType(LActivity.RESOURCE_NEW_EVENTS);
		a.setMessage(c.getResources().getString(R.string.numNewEvents).replace(Constants.PH_NUM, VU.getBoldTextString("" + unreadNumber)));
		return a;
	}

	public static LActivity getBlancSheetMsg(Context c, EdoContainer container){
		LActivity a = new LActivity();
		a.setId(ID_BLANK_SHEET);
		a.setContext(LActivity.getContext(c, container));
		a.setContainer(container);
		a.setSender(container);
		a.setResourceType(LActivity.RESOURCE_BLANC_SHEET);
		a = EdoMessageBuilder.setMessagesOfResourceBlancSheet(c,a);
		return a;
	}


}
