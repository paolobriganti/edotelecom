package io.edo.db.web.beans;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import io.edo.db.local.beans.support.EdoResource;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class WStatus implements EdoResource {
	
	private int[] t;    // [0,0,0,0,0] stato di visualizzazione del tutorial,
	private String las;    // last activity seen, sarà l’_id dell’ultima notifica visualizzata (con exchange = notification)
	private Map<String, String> cls;    // {containerId : lastMessageId ...} stato di lettura delle conversazioni chat
	private boolean sli;    // bool, slider visualizzato

	
	public int[] getTutorialVisualizationState() {
		return t;
	}

	public void setTutorialVisualizationState(int[] t) {
		this.t = t;
	}

	public String getLastNotitificationSeenId() {
		return las;
	}

	public void setLastNotitificationSeenId(String las) {
		this.las = las;
	}

	public Map<String, String> getLastChatStateList() {
		return cls;
	}

	public void setLastChatStateList(Map<String, String> cls) {
		this.cls = cls;
	}
	
	public void setLastMessageRed(String containerId, String lastNotificationId) {
		if(this.cls==null){
			this.cls = new HashMap<String, String>();
		}
		this.cls.put(containerId, lastNotificationId);

	}

	public boolean isSliderSeen() {
		return sli;
	}

	public void setSliderSeen(boolean sli) {
		this.sli = sli;
	}

	public String toJson(){
		return new Gson().toJson(this);
	}

	public static WStatus fromJson(String json){
		try{
			return new Gson().fromJson(json, WStatus.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WStatus:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WStatus JSON:\n"+json);
			return null;
		}
	}

	@Override
	public String getId() {
		return las;
	}

	@Override
	public String getName() {
		return las;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WStatus wStatus = (WStatus) o;

		return !(las != null ? !las.equals(wStatus.las) : wStatus.las != null);

	}

}

