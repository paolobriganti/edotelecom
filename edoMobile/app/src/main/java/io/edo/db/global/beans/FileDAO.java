package io.edo.db.global.beans;

import android.content.Context;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.WEntity;
import io.edo.db.web.beans.WFileDAO;

public class FileDAO extends BaseDAO{

    LFileDAO lfileDAO;

    public FileDAO(Context context) {
        super(context);
        this.lfileDAO = new LFileDAO(context);
    }

    @Override
    public LFileDAO getLDAO() {
        return lfileDAO;
    }

    public WEntity update(EdoContainer container, LSpace space, LFile newFile) {
        WEntity entity = WFileDAO.updateFile(context, container, newFile);
        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(newFile,entity.file);
            }else{
                entity.file = newFile;
            }

            lfileDAO.update(entity.file);

            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act,container,space,entity.file);
            }

            return entity;
        }
        return null;
    }

    public WEntity rename(EdoContainer container, LSpace space, LFile newFile) {
        LFile renameFile = LFile.baseForWebUpdate(space,newFile);
        renameFile.setName(newFile.getName());

        WEntity entity = WFileDAO.updateFile(context, container, renameFile);
        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(newFile,entity.file);
            }else{
                entity.file = newFile;
            }

            lfileDAO.update(entity.file);

            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act,container,space,entity.file);
            }

            return entity;
        }
        return null;
    }

    public WEntity star(EdoContainer container, LSpace space, LFile newFile) {
        LFile starFile = LFile.baseForWebUpdate(space,newFile);
        starFile.setIsStarred(newFile.isStarred());

        WEntity entity = WFileDAO.updateFile(context, container, starFile);
        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(newFile,entity.file);
            }else{
                entity.file = newFile;
            }

            lfileDAO.update(entity.file);

            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act,container,space,entity.file);
            }

            return entity;
        }
        return null;
    }


    public WEntity updateContentOnly(EdoContainer container, LSpace space, LFile newFile, boolean editing) {
        LFile contentFile = LFile.baseForWebUpdate(space,newFile);
        contentFile.setContent(newFile.getContent());

        WEntity entity = WFileDAO.updateContentOnly(context, contentFile, editing);
        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(newFile,entity.file);
            }else{
                entity.file = newFile;
            }

            lfileDAO.update(entity.file);

            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act,container,space,entity.file);
            }

            return entity;
        }
        return null;
    }




    public WEntity move(EdoContainer container, LSpace space, LFile newFile) {
        WEntity entity = WFileDAO.moveFile(context, container, newFile);

        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(newFile,entity.file);
            }else{
                entity.file = newFile;
            }

            lfileDAO.update(entity.file);

            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act,container,space,entity.file);
            }

            return entity;
        }
        return null;
    }

    public WEntity deleteFile(EdoContainer container, LSpace space, LFile file, boolean removeFromCloud){
        WEntity entity = WFileDAO.deleteFile(context, space, file, removeFromCloud);
        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(file,entity.file);
            }else{
                entity.file = file;
            }


            lfileDAO.delete(entity.file);


            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act,container,space,entity.file);
            }

            return entity;
        }
        return null;
    }

    public LFile download(String id){
        LFile file = WFileDAO.getFile(context,id);
        if(file!=null){
            lfileDAO.insertFromWebServer(file);
        }
        return file;
    }


    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTIVITY
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public LActivity addExtraToActivity(LActivity act, EdoContainer container, LSpace spaceWithCurrentFolder, LFile file){
        if(act==null)
            return null;

        act.setStatus(LActivity.STATUS_COMPLETE);
        act.setExtra(app.getUserContact(), container,spaceWithCurrentFolder,file);

        return act;
    }
}