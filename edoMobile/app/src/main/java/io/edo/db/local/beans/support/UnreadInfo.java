package io.edo.db.local.beans.support;

import android.database.Cursor;

public class UnreadInfo {
	public static final String KEY_UNREADFILESCOUNT = "unreadFilesCount";
	public static final String KEY_UNREADNOTESCOUNT = "unreadNotesCount";
	public static final String KEY_UNREADCHATMESSAGESCOUNT = "unreadChatMessageCount";
	public static final String KEY_UNREADGROUPCOUNT = "unreadGroupCount";
	
	
	private Integer unreadFilesCount;
	private Integer unreadNotesCount;
	private Integer unreadChatMessageCount;
	private Integer unreadGroupCount;


	public UnreadInfo(Integer unreadFilesCount, Integer unreadNotesCount, Integer unreadChatMessageCount, Integer unreadGroupCount) {
		this.unreadFilesCount = unreadFilesCount;
		this.unreadNotesCount = unreadNotesCount;
		this.unreadChatMessageCount = unreadChatMessageCount;
		this.unreadGroupCount = unreadGroupCount;
	}

	public UnreadInfo(Cursor cursor) {
		super();

		setUnreadFilesCount(cursor.getInt(cursor.getColumnIndex(KEY_UNREADFILESCOUNT)));
		setUnreadNotesCount(cursor.getInt(cursor.getColumnIndex(KEY_UNREADNOTESCOUNT)));
		setUnreadChatMessageCount(cursor.getInt(cursor.getColumnIndex(KEY_UNREADCHATMESSAGESCOUNT)));
		setUnreadGroupCount(cursor.getInt(cursor.getColumnIndex(KEY_UNREADGROUPCOUNT)));
	}


	public int getUnreadFilesCount() {
		if(unreadFilesCount!=null)
			return unreadFilesCount;
		return 0;
	}


	public void setUnreadFilesCount(Integer unreadFilesCount) {
		this.unreadFilesCount = unreadFilesCount;
	}


	public int getUnreadNotesCount() {
		if(unreadNotesCount!=null)
			return unreadNotesCount;
		return 0;
	}


	public void setUnreadNotesCount(Integer unreadNotesCount) {
		this.unreadNotesCount = unreadNotesCount;
	}


	public int getUnreadChatMessageCount() {
		if(unreadChatMessageCount!=null)
			return unreadChatMessageCount;
		return 0;
	}


	public void setUnreadChatMessageCount(Integer unreadChatMessageCount) {
		this.unreadChatMessageCount = unreadChatMessageCount;
	}

	public int getUnreadGroupCount() {
		if(unreadGroupCount!=null)
			return unreadGroupCount;
		return 0;
	}

	public void setUnreadGroupCount(Integer unreadGroupCount) {
		this.unreadGroupCount = unreadGroupCount;
	}

	public int getTotalCount(){
		int count = 0;
		if(unreadFilesCount!=null)
			count += unreadFilesCount;
		if(unreadNotesCount!=null)
			count += unreadNotesCount;
		if(unreadChatMessageCount!=null)
			count += unreadChatMessageCount;
		if(unreadGroupCount!=null)
			count += unreadGroupCount;
		return count;
	}
}
