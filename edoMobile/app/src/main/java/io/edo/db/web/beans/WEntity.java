package io.edo.db.web.beans;

import com.google.gson.Gson;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.LUser;
import io.edo.db.local.beans.support.Comment;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class WEntity {
	public String id;
	public int success;
	public LActivity act;
	public LContact contact;
	public LContact[] contacts;
	public LFile file;
	public Comment note;
	public LContact group;
	public LService service;
	public LUser user;
	public String url;
	public String name;
	public String thumb;

	public String toJson(){
		return new Gson().toJson(this);
	}

	public static WEntity fromJson(String json){
		try{
			return new Gson().fromJson(json, WEntity.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WEntity:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WEntity JSON:\n"+json);
			return null;
		}
	}
}
