package io.edo.db.global.beans;

import android.content.Context;

import io.edo.db.global.EdoActivityManager;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.web.beans.WActivityDAO;
import io.edo.ui.OnProgressChangeListener;

/**
 * Created by PaoloBriganti on 22/06/15.
 */
public class ActivityDAO extends BaseDAO{

    LActivityDAO lActivityDAO;

    public ActivityDAO(Context context){
        super(context);
        lActivityDAO = new LActivityDAO(context);
    }

    @Override
    public LActivityDAO getLDAO() {
        return lActivityDAO;
    }

    public LActivity[] downloadAll(boolean isInit, OnProgressChangeListener listener) {
        return downloadAllBy(null, isInit, listener);
    }

    public LActivity[] downloadAllBy(EdoContainer container, boolean isInit, OnProgressChangeListener listener) {
        return downloadAllBy(container,-1, isInit,listener);
    }

    public LActivity[] downloadAllBy(EdoContainer container, int offset, boolean isInit, OnProgressChangeListener listener) {
        LActivity[] activities = WActivityDAO.getActivitiesByContainerId(context,container,offset);
        if(activities!=null && activities.length>0){
            long lastProgressSended = System.currentTimeMillis();

            EdoActivityManager am = new EdoActivityManager(context);

            int count = 0;

            for(int i=activities.length-1; i>=0; i--){
                LActivity a = activities[i];
                if(a!=null){

                    if(!a.hasToBeSavedOnLocalDB() || getLDAO().exists(a.getId())){
                        continue;
                    }


                    a.setStatus(LActivity.STATUS_COMPLETE);
                    a.loadExtraFromDB(context, false);

                    am.processActivity(a,true,isInit);

                    if(System.currentTimeMillis()-lastProgressSended>1500){
                        lastProgressSended = System.currentTimeMillis();
                        if (listener != null) listener.onProgressChange(count, activities.length, a, null);
                    }

                }
                count++;
            }

            return activities;
        }
        return null;
    }


}
