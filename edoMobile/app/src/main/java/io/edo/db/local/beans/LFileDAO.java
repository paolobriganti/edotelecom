package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;


public class LFileDAO extends LBaseDAO<LFile,String,ArrayList<LFile>,ArrayList<String>> {

	public static String TABLE_NAME = LocalDBHelper.TABLE_FILE;

    public static String[] tableKeys = {LocalDBHelper.KEY_ID,
			LocalDBHelper.KEY_WEBURI,
			LocalDBHelper.KEY_CLOUDID,
			LocalDBHelper.KEY_NAME,
			LocalDBHelper.KEY_EXTENTION,
			LocalDBHelper.KEY_THUMBNAIL,
			LocalDBHelper.KEY_ISSTARRED,
			LocalDBHelper.KEY_STATUS,
			LocalDBHelper.KEY_NOTE,
			LocalDBHelper.KEY_TYPE,
			LocalDBHelper.KEY_MDATE,
			LocalDBHelper.KEY_COLLECTIONID,
			LocalDBHelper.KEY_PARENTID,
			LocalDBHelper.KEY_SERVICEID,
			LocalDBHelper.KEY_COLLABSEMAILS,
			LocalDBHelper.KEY_PATH,
			LocalDBHelper.KEY_CREATEDBY,
			LocalDBHelper.KEY_CONTENT,

			LocalDBHelper.KEY_LOCALURI,

			LocalDBHelper.KEY_ISUNREAD,
			LocalDBHelper.KEY_HASNEWNOTES,
			LocalDBHelper.KEY_NOTESCOUNT};


	public LFileDAO(Context context) {
		super(context);
	}

	@Override
	public ContentValues createContentValues(LFile file) {
		ContentValues values = new ContentValues();
		if(file.getId()!=null)values.put( LocalDBHelper.KEY_ID, file.getId() );
		values.put( LocalDBHelper.KEY_WEBURI, file.getWebUri() );
		values.put( LocalDBHelper.KEY_CLOUDID, file.getCloudId() );
		if(file.getName()!=null)values.put( LocalDBHelper.KEY_NAME, file.getName() );
		if(file.getExtension()!=null)values.put( LocalDBHelper.KEY_EXTENTION, file.getExtension() );
		if(file.getThumbnail()!=null)values.put( LocalDBHelper.KEY_THUMBNAIL, file.getThumbnail() );
		if(file.getIsStarred()!=null)values.put( LocalDBHelper.KEY_ISSTARRED, file.getIsStarred() );
		if(file.getStatus()!=null)values.put( LocalDBHelper.KEY_STATUS, file.getStatus() );
		if(file.getCommentsJson()!=null)values.put( LocalDBHelper.KEY_NOTE, file.getCommentsJson() );
		if(file.getType()!=null)values.put( LocalDBHelper.KEY_TYPE, file.getType() );
		if(file.getmDate()!=null)values.put( LocalDBHelper.KEY_MDATE, file.getmDate());
		if(file.getCollectionId()!=null)values.put( LocalDBHelper.KEY_COLLECTIONID, file.getCollectionId() );
		if(file.getParentId()!=null)values.put( LocalDBHelper.KEY_PARENTID, file.getParentId() );
		if(file.getServiceId()!=null)values.put( LocalDBHelper.KEY_SERVICEID, file.getServiceId() );
		if(file.getCollabsEmailsJson()!=null)values.put( LocalDBHelper.KEY_COLLABSEMAILS, file.getCollabsEmailsJson() );
		if(file.getPath()!=null)values.put( LocalDBHelper.KEY_PATH, file.getPath() );
		if(file.getCreatedBy()!=null)values.put( LocalDBHelper.KEY_CREATEDBY, file.getCreatedBy() );
		if(file.getContent()!=null)values.put( LocalDBHelper.KEY_CONTENT, file.getContent() );

		values.put( LocalDBHelper.KEY_LOCALURI, file.getLocalUri() );

		if(file.getIsUnread()!=null)values.put( LocalDBHelper.KEY_ISUNREAD, file.getIsUnread() );
		if(file.getHasNewComments()!=null)values.put( LocalDBHelper.KEY_HASNEWNOTES, file.getHasNewComments() );
		values.put( LocalDBHelper.KEY_NOTESCOUNT, file.getCommentsCount() );
		return values;
	}

	@Override
	public synchronized boolean insert(LFile file) {
        ContentValues initialValues = createContentValues(file);
        long lid = db.insert(TABLE_NAME, initialValues);
        return lid>0;
	}

    @Override
	public synchronized boolean insertFromWebServer(LFile file) {
        boolean done = false;
        if(file!=null){

            LFile oldFile = get(file.getId());

            if(oldFile!=null){
                delete(oldFile);

                if(!oldFile.isStored()){
                    file.setLocalUri(oldFile.getLocalUri());
                }

            }

            done = insert(file);

            eLog.w("EdoActivityMan", "done: " + done);
        }

        return done;
	}




	@Override
	public synchronized boolean update(LFile file) {
        ContentValues updatedValues = createContentValues(file);

        String whereClause = LocalDBHelper.KEY_ID+" == '"+ file.getId() + "'";
        boolean updated = db.update(TABLE_NAME, updatedValues, whereClause);

        return updated;
	}

	@Override
	public synchronized boolean delete(LFile file) {
        String whereClause = LocalDBHelper.KEY_ID+" == '"+ file.getId() + "'";
        boolean deleted =  db.delete(TABLE_NAME, whereClause);

        return deleted;
	}

	@Override
	public synchronized LFile get(String id) {
        if(id==null)
            return null;

        String whereClause = LocalDBHelper.KEY_ID+" == ?";
        Cursor cursor = db.select(TABLE_NAME, tableKeys,whereClause, new String[]{id});
        LFile file = null;
        try {

            if(cursor.moveToNext()){
                file = new LFile(cursor);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }


        return file;
	}

	@Override
	public synchronized ArrayList<LFile> getList() {
        ArrayList<LFile> files = new ArrayList<LFile> ();
        Cursor cursor = null;
        try {

            cursor = db.selectAll(TABLE_NAME, tableKeys);
            while(cursor.moveToNext()){
                LFile file = new LFile(cursor);
                files.add(file);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }


        return files;
	}

    @Override
    public ArrayList<LFile> getListBy(ArrayList<String> fileIds) {
        if(fileIds==null)
            return null;

        ArrayList<LFile> files = new ArrayList<LFile> ();

        String whereClause = "";
        for(int i=0; i<fileIds.size(); i++){
            whereClause+= LocalDBHelper.KEY_ID + " = '"+fileIds.get(i)+"'";
            if(i<fileIds.size()-1){
                whereClause += " OR ";
            }
        }


        Cursor cursor = db.select(TABLE_NAME, tableKeys, whereClause, null);
        try {
            Map<String, LFile> idFiles= Collections.synchronizedMap(new HashMap<String, LFile>());
            while(cursor.moveToNext()){
                LFile file = new LFile(cursor);
                idFiles.put(file.getId(), file);
            }
            if(idFiles.size()>0){
                for(String id: fileIds){
                    files.add(idFiles.get(id));
                }
            }
        } finally {
            if(cursor != null)
                cursor.close();
        }


        return files;
    }


    public synchronized ArrayList<LFile> getSubFiles(LFile parentFile, int sortBy, int offset, int limit, boolean folderOnly){
        if(parentFile==null)
            return null;



        ArrayList<LFile> files = new ArrayList<LFile> ();
        String parentId = parentFile.getId();
        String whereClause = LocalDBHelper.KEY_PARENTID+"=?";

        if(folderOnly){
            whereClause += " AND "+LocalDBHelper.KEY_TYPE+" = '"+ LFile.FILE_TYPE_EDOFOLDER+"'";
        }

        String orderBy = LocalDBHelper.KEY_ISSTARRED + " DESC, ";


        switch (sortBy) {

            case Constants.SORT_BY_CDATE_DESC:
                orderBy += LocalDBHelper.KEY_ID+" DESC";
                break;

            case Constants.SORT_BY_CDATE_ASC:
                orderBy += LocalDBHelper.KEY_ID+" ASC";
                break;

            case Constants.SORT_BY_MDATE_DESC:
                orderBy += LocalDBHelper.KEY_MDATE+" DESC";
                break;

            case Constants.SORT_BY_MDATE_ASC:
                orderBy += LocalDBHelper.KEY_MDATE+" ASC";
                break;

            case Constants.SORT_BY_NAME_ASC:
                orderBy += LocalDBHelper.KEY_NAME+" ASC";
                break;

            case Constants.SORT_BY_NAME_DESC:
                orderBy += LocalDBHelper.KEY_NAME+" DESC";
                break;

            default:
                orderBy += LocalDBHelper.KEY_MDATE+" DESC";
                break;
        }

        if(offset!=-1 && limit!=-1){
            orderBy += " LIMIT "+offset+","+limit;
        }
        Cursor cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{parentId}, orderBy);
        try {
            while(cursor.moveToNext()){
                LFile file = new LFile(cursor);
                files.add(file);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }

        return files;
    }


    public synchronized ArrayList<LFile> getFilesFromCursor(Cursor cursor){ //DO NOT CLOSE THIS CURSOR!

        ArrayList<LFile> files = new ArrayList<LFile> ();
        while(cursor.moveToNext()){
            LFile file = new LFile(cursor);
            files.add(file);
        }

        if(files.size()>0)
            return files;
        else
            return null;
    }


    public synchronized EdoContainer getContainerOf(LFile lfile){
        if(lfile!=null){
            LContactFileDAO contactFile = new LContactFileDAO(context);
            LContact contact = contactFile.getContactOfFile(lfile);
            if(contact!=null){
                return contact;
            }

            return app.getUserContact();
        }
        return null;
    }


    public synchronized Cursor getFileCursorByLikeName(String name){
        String whereClause = "("+LocalDBHelper.KEY_NAME+" LIKE ? OR "+LocalDBHelper.KEY_NOTE+" LIKE ? ) AND "+
                LocalDBHelper.KEY_TYPE+" IS NOT '"+LFile.FILE_TYPE_EDOTAB+"'";


        return db.select(TABLE_NAME, tableKeys, whereClause, new String[]{"%" + name + "%","%" + name + "%"});
    }


    public synchronized ArrayList<LFile> getFilesByNameLike(String name){

        ArrayList<LFile> files = new ArrayList<LFile> ();

        Cursor cursor = getFileCursorByLikeName(name);
        try {

            while(cursor.moveToNext()){
                LFile file = new LFile(cursor);

                files.add(file);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }



        return files;
    }

    public synchronized ArrayList<LFile> getFilesNotSavedToServer(){


        ArrayList<LFile> files = new ArrayList<LFile> ();

        String whereClause = LocalDBHelper.KEY_STATUS+" == ? ";

        Cursor cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{LFile.STATUS_NOT_SAVED_TO_SERVER});
        try {

            while(cursor.moveToNext()){
                LFile file = new LFile(cursor);

                files.add(file);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }

        return files;
    }
}