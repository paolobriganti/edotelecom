package io.edo.db.web.beans;

import android.content.Context;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.Comment;
import io.edo.db.web.request.EdoRequest;


public class WCommentDAO {


    public static WEntity addComment(Context context, LFile localFile, Comment comment){
        String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_FILE + EdoRequest.PATH_SEPARATOR + localFile.getId()  + EdoRequest.PATH_NOTE;

        EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, comment.toJson());

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.note = comment;
            }
            return entity;
        }
        return null;
    }

    public static WEntity updateComment(Context context, LFile localFile, Comment comment){

        String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_FILE + EdoRequest.PATH_SEPARATOR + localFile.getId()  + EdoRequest.PATH_NOTE;

        EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, comment.toJson());

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.note = comment;
            }
            return entity;
        }
        return null;
    }

	public static LFile getComments(Context context, LFile localFile, boolean hasNewComments){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_FILE + EdoRequest.PATH_SEPARATOR + localFile.getId() + EdoRequest.PATH_NOTES;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(context);

		if(!EdoRequest.hasEdoErrors(response)){
			localFile.setComments(response);

			if(hasNewComments){
				localFile.setHasNewComments(true);
			}

			return localFile;
		}

		return null;
	}









}
