package io.edo.db.global.beans;

import android.content.Context;

import com.google.gson.Gson;
import com.paolobriganti.utils.JavaUtils;

import java.util.Map;

import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.local.beans.LBaseDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LStatusDAO;
import io.edo.db.web.beans.WStatus;
import io.edo.db.web.beans.WStatusDAO;
import io.edo.ui.UIManager;
import io.edo.utilities.Constants;
import io.edo.utilities.ObjectId;
import io.edo.utilities.eLog;

public class StatusDAO extends BaseDAO{

	LStatusDAO lDAO;

	public StatusDAO(Context context) {
		super(context);
		lDAO = new LStatusDAO(context);
	}


	@Override
	public LBaseDAO getLDAO() {
		return lDAO;
	}


	public WStatus send(String lastNotificationSeenId){
		WStatus status = app.getNotificationStatus();
		if(status!=null){
			status.setLastNotitificationSeenId(lastNotificationSeenId);
			if(WStatusDAO.sendStatus(context,status)){
				lDAO.insertFromWebServer(status);
			}
		}
		return status;
	}

	public WStatus update(String containerId, String lastNotificationSeenId){
		WStatus status = app.getNotificationStatus();
		if(status!=null){
			status.setLastMessageRed(containerId, lastNotificationSeenId);
			if(WStatusDAO.updateStatus(context, containerId,lastNotificationSeenId)){
				lDAO.insertFromWebServer(status);
			}
		}
		return status;
	}

	public WStatus download(boolean updateUI){
		WStatus status = WStatusDAO.getStatus(context);
		return updateLocalData(status, updateUI);
	}



	public WStatus updateLocalData(String clsJson){
		Map<String, String> cls;
		try{
			cls = new Gson().fromJson(clsJson, Map.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "CLS:\n" + e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "CLS JSON:\n"+clsJson);
			return null;
		}
		eLog.v("EdoActivityMan", "CLS SIZE: " + (cls!=null? cls.size():0));
		if(cls!=null && cls.size()>0){
			WStatus status = app.getNotificationStatus();
			if(status==null)
				status = new WStatus();

			for(String key: cls.keySet()){
				String value = cls.get(key);

				status.setLastMessageRed(key, value);

				boolean done = new LActivityDAO(context).setAllActivitiesRedBy(key, key);
				eLog.i("EdoActivityMan", "SET ALL RED: " + done);
				if (done) {
					long mDate = System.currentTimeMillis();
					ObjectId objectId = new ObjectId(value);
					if(objectId!=null && objectId.getDate()!=null){
						mDate = objectId.getDate().getTime();
					}
					new ContactUIDAO(context).highlightContact(new LContact(key), mDate, false);
				}

			}
			LActivity a = new LActivity();
			a.setEvent(LActivity.EVENT_SYNC_ALL);
			a.setStatus(LActivity.STATUS_COMPLETE);
			app.uiManager.syncResourcesList(a, UIManager.ContactsUpdatesListener.class);
			return updateLocalData(status,false);
		}
		return null;
	}



	public WStatus updateLocalData(WStatus status, boolean updateUI){
		if(status!=null){
			lDAO.insertFromWebServer(status);

			if(updateUI) {

				//Update Notification read
				LActivityDAO aDAO = new LActivityDAO(context);

				if (JavaUtils.isNotEmpty(status.getLastNotitificationSeenId())) {
					boolean done = aDAO.setAllActivitiesRedIdIfOlderThan(status.getLastNotitificationSeenId());
				}

				if (status.getLastChatStateList() != null && status.getLastChatStateList().size() > 0) {
					ContactUIDAO contactUIDAO = new ContactUIDAO(context);
					for (String containerId : status.getLastChatStateList().keySet()) {
						if (status.getLastChatStateList().get(containerId) != null) {
							String notificationId = status.getLastChatStateList().get(containerId);
							boolean done = aDAO.setAllActivitiesRedByContainerIdIfOlderThan(containerId, notificationId);
							eLog.i("EdoActivityMan", "STATUS SET ALL RED: " +done);
							if (done) {
								long mDate = System.currentTimeMillis();
								ObjectId objectId = new ObjectId(notificationId);
								if(objectId!=null && objectId.getDate()!=null){
									mDate = objectId.getDate().getTime();
								}
								contactUIDAO.highlightContact(new LContact(containerId), mDate, false);
							}
						}
					}
					LActivity a = new LActivity();
					a.setEvent(LActivity.EVENT_SYNC_ALL);
					a.setStatus(LActivity.STATUS_COMPLETE);
					app.uiManager.syncResourcesList(a, UIManager.ContactsUpdatesListener.class);
				}

			}


		}
		return status;
	}

}
