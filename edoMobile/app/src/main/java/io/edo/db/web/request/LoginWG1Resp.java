package io.edo.db.web.request;

import android.content.Context;

import com.google.gson.Gson;
import com.paolobriganti.utils.StringFile;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class LoginWG1Resp {
	public static final String FILE_NAME = "LWG_RESP_1";
	
	public String token;
	public long date;

	public String toJson(){
		return new Gson().toJson(this);
	}

	public static LoginWG1Resp fromJson(String json){
		try{
			return new Gson().fromJson(json, LoginWG1Resp.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LoginWG1Resp:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LoginWG1Resp JSON:\n"+json);
			return null;
		}
	}
	
	public void save(Context c){
		date = System.currentTimeMillis();
		String json = toJson();
		StringFile.write(c, FILE_NAME, json);
	}
	
	public static LoginWG1Resp load(Context c){
		if(StringFile.fileExists(c, FILE_NAME)){
			String json = StringFile.read(c, FILE_NAME);
			LoginWG1Resp lwg = fromJson(json);
			if(System.currentTimeMillis()-lwg.date>600000){
				StringFile.deleteFile(c, FILE_NAME);
				return null;
			}
			return lwg;
		}else{
			return null;
		}
	}
	
	
	public static boolean remove(Context c){
		if(StringFile.fileExists(c, FILE_NAME)){
			return StringFile.deleteFile(c, FILE_NAME);
		}
		return false;
	}
}
