package io.edo.db.local.beans;

import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;
import com.paolobriganti.utils.JavaUtils;

import java.io.Serializable;

import io.edo.db.global.beans.ContactDAO;
import io.edo.db.global.beans.GroupDAO;
import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.EdoResource;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.WActivity;
import io.edo.db.web.beans.WMessage;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.Constants;
import io.edo.utilities.ObjectId;
import io.edo.utilities.eLog;

public class LActivity extends WActivity implements EdoResource,Serializable{
    private static final long serialVersionUID = 1L;

    public static final int VALUE_ERROR = -999999999;
    public static final int VALUE_CLIENT_START = VALUE_ERROR+1;

    public static final String EXCHANGE_NOTIFICATION = "n";
    public static final String EXCHANGE_REALTIME = "r";

    public static final String CONTEXT_PERSONAL = "p";
    public static final String CONTEXT_CONTACT = "c";
    public static final String CONTEXT_GROUP = "g";

    public static final int RESOURCE_USER = 1;
    public static final int RESOURCE_CATEGORY = 2;
    public static final int RESOURCE_CONTACT = 3;
    public static final int RESOURCE_GROUP = 4;
    public static final int RESOURCE_SERVICE = 5;
    public static final int RESOURCE_FILE = 6;
    public static final int RESOURCE_MEMBER = 7;
    public static final int RESOURCE_STATUS = 8;
    //CLIENT RESOURCE TYPES
    public static final int RESOURCE_BLANC_SHEET = VALUE_CLIENT_START+1;
    public static final int RESOURCE_NEW_EVENTS = VALUE_CLIENT_START+2;

    public static final int EVENT_INSERT = 1;
    public static final int EVENT_DELETE = 2;
    public static final int EVENT_UPDATE = 3;
    public static final int EVENT_COMMENT_INSERT = 5;
    public static final int EVENT_COMMENT_UPDATED = 6;
    public static final int EVENT_INSERT_SPACE = 7;
    public static final int EVENT_JOIN = 10;
    public static final int EVENT_LEAVE = 11;
    public static final int EVENT_MESSAGE = 101;
    //CLIENT EVENTS
    public static final int EVENT_SYNC_ALL = VALUE_CLIENT_START+1;
    public static final int EVENT_UPLOAD_TO_CLOUD = VALUE_CLIENT_START+2;
    public static final int EVENT_DOWNLOAD_FROM_CLOUD = VALUE_CLIENT_START+3;
    public static final int EVENT_DOWNLOAD_FILE_OF_FOLDER = VALUE_CLIENT_START+4;
    public static final int EVENT_HIGHLIGHT_CONTACT = VALUE_CLIENT_START+5;

    public static final int SUB_EVENT_RENAMED = 1;
    public static final int SUB_EVENT_MOVED = 2;
    public static final int SUB_EVENT_ICON_MODIFIED = 3;
    public static final int SUB_EVENT_PENDING_USER_ADDED = 4;
    public static final int SUB_EVENT_PENDING_USER_REMOVED = 5;
    public static final int SUB_EVENT_STARRED = 6;
    public static final int SUB_EVENT_UNSTARRED = 7;
    public static final int SUB_EVENT_CONTENT_CHANGED = 8;
    public static final int SUB_EVENT_EDITING = 9;
    public static final int SUB_EVENT_CONTENT_COMMITTED = 10;
    public static final int SUB_EVENT_MOVE_OUT = 20;
    public static final int SUB_EVENT_MOVE_IN = 21;


    //CLIENT STATUS
    public static final int STATUS_STARTED = 0;
    public static final int STATUS_PENDING = 1;
    public static final int STATUS_IN_PROGRESS = 2;
    public static final int STATUS_ERROR = 3;
    public static final int STATUS_STOP = 4;
    public static final int STATUS_COMPLETE = 5;
    public static final int STATUS_DEFAULT_SPACE = 6;
    public static final int STATUS_NOT_SAVED_TO_SERVER = 7;

    //VISUALIZATION
    public static final int VISUALIZATION_HIDDEN = 1;
    public static final int VISUALIZATION_REDUX = 2;
    public static final int VISUALIZATION_FULL = 3;



    //Extra
    private String containerName = null;
    private String senderName = null;

    private String spaceName = null;
    private String vtype = null;

    private String content = null;
    private String shortDescription = null;
    private String longDescription = null;

    private Integer isUnread = 0;
    private Integer status = 0;


    //Floating
    private String aggregationMessage;
    private int progressPercent = 0;
    private EdoContainer sender;
    private EdoContainer container;
    private LSpace space;
    private EdoResource resource;
    private boolean isSelected;
    private String oldActivityId;
    private int visualization;

    public LActivity getActivity(){
        return this;
    }


    public LActivity(Cursor cursor) {

        setId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_ID)));
        setExchange(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_EXCHANGE)));
        setContext(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTEXT)));
        setContainerId((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTAINERID))));
        setSenderId((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_SENDERID))));
        setResourceType(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_RESOURCETYPE)));
        setResourceId((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_RESOURCEID))));
        setMessage((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_MESSAGE))));
        setEvent(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_EVENT)));
        setSubEvent(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_SUBEVENT)));
        setOriginalParentId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_ORIGINALPARENTID)));
        setParentId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_PARENTID)));
        setParentName(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_PARENTNAME)));
        setIsFolder(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ISFOLDER)));
        setOriginalName(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_ORIGINALNAME)));
        setResourceName((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_RESOURCENAME))));

        setContainerName((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTAINERNAME))));
        setSenderName((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_SENDERNAME))));

        setSpaceName(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_SPACENAME)));

        setVtype((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_VTYPE))));

        setContent((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTENT))));
        setShortDescription((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_SHORTDESCRIPTION))));
        setLongDescription((cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_LONGDESCRIPTION))));

        setIsUnread((cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ISUNREAD))));
        setStatus((cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_STATUS))));

    }



    public LActivity() {
        setId(new ObjectId().toString());
    }

    public LActivity(String id) {
        setId(id);
    }

    public LActivity(String context, int event, int resourceType) {
        setId(new ObjectId().toString());
        setContext(context);
        setEvent(event);
        setResourceType(resourceType);
    }

    public LActivity(String context, EdoContainer container, EdoContainer sender, int event, int resourceType, EdoResource resource) {
        setId(new ObjectId().toString());
        setContainer(container);
        setSender(sender);
        setContext(context);
        setEvent(event);
        setResourceType(resourceType);
        setResource(resource);
    }

    public LActivity(String context, EdoContainer container, EdoContainer sender, int event, int resourceType, EdoResource resource, int status, int progressPercent, String message) {
        setId(new ObjectId().toString());
        setContainer(container);
        setSender(sender);
        setContext(context);
        setEvent(event);
        setResourceType(resourceType);
        setResource(resource);
        setStatus(status);
        setProgressPercent(progressPercent);
        setMessage(message);
    }

    public LActivity(String currentUserId, WMessage msg) {
        this.setId(new ObjectId().toString());
        this.setExchange(LActivity.EXCHANGE_REALTIME);
        this.setContext(msg.getContext());
        this.setResourceType(msg.getResourceType());
        this.setResourceId(msg.getResourceId());
        this.setEvent(msg.getEvent());
        this.setContainerId(msg.getContainerId());
        this.setSenderId(currentUserId);
        this.setStatus(LActivity.STATUS_IN_PROGRESS);
        this.setMessage(msg.getArgument());
        this.isUnread = 0;
    }

    public String getContainerName() {
        return containerName;
    }

    private void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getVtype() {
        return vtype;
    }

    private void setVtype(String vtype) {
        this.vtype = vtype;
    }

    public String getContent() {
        return content;
    }

    private void setContent(String content) {
        this.content = content;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Integer getIsUnread() {
        return isUnread;
    }

    public void setIsUnread(Integer isUnread) {
        this.isUnread = isUnread;
    }
    public boolean isUnread() {
        return isUnread!=null && isUnread==1;
    }

    public void setIsUnread(boolean isUnread) {
        this.isUnread = isUnread?1:0;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public EdoContainer getSender() {
        return sender;
    }

    public void setSender(EdoContainer sender) {
        this.sender = sender;

        if(sender!=null){
            setSenderId(sender.getContainerId());
            setSenderName(sender.getNameComplete());
        }
    }

    public EdoContainer getContainer() {
        return container;
    }

    public void setContainer(EdoContainer container) {
        this.container = container;

        if(container!=null){
            setContainerId(container.getContainerId());
            setContainerName(container.getNameComplete());
        }
    }

    public LSpace getSpace() {
        return space;
    }

    public void setSpace(LSpace space) {
        this.space = space;

        if(space!=null){
            setSpaceName(space.getName());
        }
    }

    public EdoResource getResource() {
        return resource;
    }

    public void setResource(EdoResource resource) {
        this.resource = resource;

        if(resource!=null){
            if(resource instanceof LContact){
                setResourceId(((LContact)resource).getContainerId());

            }else{
                setResourceId(resource.getId());
            }

            setResourceName(resource.getName());
            if(resource instanceof LFile) {
                setVtype(((LFile) resource).getvType());
            }
        }
    }



    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getOldActivityId() {
        return oldActivityId;
    }

    public void setOldActivityId(String oldActivityId) {
        this.oldActivityId = oldActivityId;
    }

    public int getProgressPercent() {
        return progressPercent;
    }

    public void setProgressPercent(int progressPercent) {
        this.progressPercent = progressPercent;
    }

    public int getVisualization() {
        return visualization;
    }

    public void setVisualization(int visualization) {
        this.visualization = visualization;
    }

    public String getAggregationMessage() {
        if(JavaUtils.isEmpty(aggregationMessage))
            return getMessage();
        return aggregationMessage;
    }

    public void setAggregationMessage(String aggregationMessage) {
        this.aggregationMessage = aggregationMessage;
    }

    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		DERIVED
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    //FILE RESOURCE
    public boolean  isFileResource(){
        return getResourceType() == RESOURCE_FILE;
    }

    public boolean isCommentEvent(){
        return getEvent()==EVENT_COMMENT_INSERT || getEvent()==EVENT_COMMENT_UPDATED;
    }

    public boolean isUploadEvent(){
        return getEvent()==EVENT_UPLOAD_TO_CLOUD;
    }

    public boolean isDownloadEvent(){
        return getEvent()==EVENT_DOWNLOAD_FROM_CLOUD;
    }

    public boolean isDelete(){
        return getEvent()==EVENT_DELETE;
    }

    public boolean isSpaceResource() {
        if(isFileResource() && getEvent() == EVENT_INSERT_SPACE)
            return true;
        if(vtype!=null)
            return vtype.equals(LFile.FILE_TYPE_EDOTAB);
        return false;
    }

    public LFile getFileResource(){
        if(resource!=null && resource instanceof LFile){
            return (LFile)resource;
        }else if(isFileResource()){
            LFile file = new LFile();
            file.setId(this.getResourceId());
            file.setName(this.getResourceName());
            file.setvType(this.getVtype());
            file.setType(this.getVtype());
            return file;
        }

        return null;
    }

    public LFile getFileResource(Context c){
        if(resource!=null && resource instanceof LFile){
            return (LFile)resource;
        }
        resource = new LFileDAO(c).get(getResourceId());
        return (LFile)resource;
    }

    //CONTACT RESOURCE
    public boolean  isContactResource(){
        return getResourceType() == RESOURCE_CONTACT;
    }

    public LContact getContactResource(){
        if(resource!=null && resource instanceof LContact){
            return (LContact)resource;
        }else if(isFileResource()){
            LContact contact = new LContact();
            contact.setId(this.getResourceId());
            contact.setName(this.getResourceName());
            return contact;
        }

        return null;
    }

    public LContact getContactResource(Context c){
        if(resource!=null && resource instanceof LContact){
            return (LContact)resource;
        }
        return new LContactDAO(c).get(getResourceId());
    }



    //GROUP RESOURCE
    public boolean  isGroupResource(){
        return getResourceType() == RESOURCE_GROUP;
    }

    //MEMBER RESOURCE
    public boolean  isMemberResource(){
        return getResourceType() == RESOURCE_MEMBER;
    }

    //SERVICE RESOURCE
    public boolean  isServiceResource(){
        return getResourceType() == RESOURCE_SERVICE;
    }

    //STATUS RESOURCE
    public boolean  isStatusResource(){
        return getResourceType() == RESOURCE_STATUS;
    }




    //CHAT
    public boolean isChatEvent(){
        return getExchange()!=null && getExchange().equals(EXCHANGE_REALTIME);
    }



    //EVENTS
    public boolean hasToBeSavedOnLocalDB(){
        return this.getResourceType()!=LActivity.RESOURCE_STATUS &&
                this.getResourceType()!=LActivity.RESOURCE_CONTACT &&
                this.getSubEvent()!=LActivity.SUB_EVENT_CONTENT_CHANGED &&
                this.getSubEvent()!=LActivity.SUB_EVENT_EDITING &&
                this.getStatus()!=LActivity.STATUS_NOT_SAVED_TO_SERVER &&
                        (this.getEvent() > 0 ||
                                (this.getEvent() == LActivity.EVENT_UPLOAD_TO_CLOUD &&
                                        (this.getStatus() == STATUS_ERROR || this.getStatus() == STATUS_STOP)));
    }
    public boolean canBeShownOnTimeline(){
        eLog.v("EdoActivityMan", "getMessage() " + this.getMessage());
        return this.getMessage()!=null &&
                this.getResourceType()!=LActivity.RESOURCE_CONTACT &&
                this.getSubEvent()!=LActivity.SUB_EVENT_CONTENT_CHANGED &&
                this.getSubEvent()!=LActivity.SUB_EVENT_EDITING &&
                (this.getEvent()>0 || this.getEvent()==LActivity.EVENT_UPLOAD_TO_CLOUD );
    }



    //STATUS
    public boolean isInProgress() {
        return status!=null && status==STATUS_IN_PROGRESS;
    }

    public boolean hasError() {
        return status!=null && status==STATUS_ERROR;
    }

    public boolean hasBeenProcessed(){
        if(getStatus()!=null){
            return getStatus() == STATUS_COMPLETE;
        }
        return false;
    }

    public boolean isDefaultSpace(){
        if(getStatus()!=null){
            return getStatus() == STATUS_DEFAULT_SPACE;
        }
        return false;
    }


    //CONTEXT
    public boolean isPersonal()
    {
        if(getContext()!=null)
            return getContext().equals(CONTEXT_PERSONAL);
        return false;
    }
    public boolean isGroupContainer(){
        return getContext().equals(CONTEXT_GROUP);
    }

    public static String getContext(Context context, EdoContainer container){
        boolean isCurrentUser = container.isPersonal(context);
        return isCurrentUser ? LActivity.CONTEXT_PERSONAL :
                ((container.isGroup()? LActivity.CONTEXT_GROUP : LActivity.CONTEXT_CONTACT));
    }



    //CONTAINER
    public boolean isPersonalContainer(String userId) {
        if (getSenderId() == null)
            return false;
        if(this.getContainerId()!=null && this.getContainerId().equals(userId))
            return true;
        return false;
    }

    public boolean isPersonalContainer(Context c) {
        String userId = ThisApp.getInstance(c).getEdoUserId();
        return isPersonalContainer(userId);
    }


    //SENDER
    public boolean isFromCurrentEdoUser(String userId) {
        if(this.isPersonal())
            return true;
        if (getSenderId() == null)
            return false;
        if(this.getSenderId()!=null && this.getSenderId().equals(userId))
            return true;
        if(this.getSenderId()!=null && this.getSenderId().equals("me"))
            return true;
        return false;
    }

    public boolean isFromCurrentEdoUser(Context c) {
        String userId = ThisApp.getInstance(c).getEdoUserId();
        return isFromCurrentEdoUser(userId);
    }



    //ID
    public long getTimeStamp(){
        if(getId()!=null){
            try {
                ObjectId objectId = new ObjectId(getId());
                return objectId.getDate().getTime();
            }catch (Exception e){

            }
        }
        return 0;
    }



    //VISUALIZATION
    public int getDefVisualization(){
        if(isFullDefVisualization()){
            return VISUALIZATION_FULL;
        }else if(isHiddenDefVisualization()){
            return VISUALIZATION_HIDDEN;
        }
        return VISUALIZATION_REDUX;
    }


    public boolean isFullVisualization(){
        return visualization == VISUALIZATION_FULL;
    }
    public boolean isFullDefVisualization(){
        switch (getResourceType()){
            case RESOURCE_GROUP:
            case RESOURCE_MEMBER:
                return false;
        }

        switch (getEvent()){
            case EVENT_INSERT:
            case EVENT_COMMENT_INSERT:
            case EVENT_COMMENT_UPDATED:
            case EVENT_UPLOAD_TO_CLOUD:
            case EVENT_DOWNLOAD_FROM_CLOUD:
                return true;
            case EVENT_UPDATE:
                switch (getSubEvent()){
                    case SUB_EVENT_STARRED:
                        return true;
                }
        }
        return false;
    }
    public boolean isReduxVisualization(){
        return visualization == VISUALIZATION_REDUX;
    }
    public boolean isReduxDefVisualization(){
        return !isFullDefVisualization() && !isHiddenDefVisualization();
    }
    public boolean isHiddenVisualization(){
        return visualization == VISUALIZATION_HIDDEN;
    }
    public boolean isHiddenDefVisualization(){
        switch (getEvent()){
            case EVENT_UPDATE:
                switch (getSubEvent()){
                    case SUB_EVENT_EDITING:
                        return true;
                    case SUB_EVENT_CONTENT_CHANGED:
                        return true;
                    case SUB_EVENT_PENDING_USER_ADDED:
                        return true;
                    case SUB_EVENT_PENDING_USER_REMOVED:
                        return true;
                }
        }
        return false;
    }


    public boolean isTheSameTypeOf(LActivity a){
        if(a==null)
            return false;

        if(getResourceId()!=null && a.getResourceId()!=null && !getResourceId().equals(a.getResourceId()))
            return false;

        if(getResourceType()!=a.getResourceType())
            return false;

        if(getEvent()==a.getEvent() && getSubEvent()==a.getSubEvent())
            return true;

        switch (getEvent()){
            case EVENT_COMMENT_INSERT:
            case EVENT_COMMENT_UPDATED:
                return a.getEvent()==EVENT_COMMENT_INSERT || a.getEvent()==EVENT_COMMENT_UPDATED;
//            case EVENT_UPDATE:
//                switch (getSubEvent()){
//                    case SUB_EVENT_STARRED:
//                        return true;
//                }
        }
        return false;
    }

    public int setVisualizationThanNewActivity(LActivity newActivity){
        int visualization = this.getDefVisualization();
        switch (newActivity.getEvent()){
            case EVENT_COMMENT_INSERT:
            case EVENT_COMMENT_UPDATED:
                visualization = VISUALIZATION_HIDDEN;
                break;
            case EVENT_UPDATE:
                switch (newActivity.getSubEvent()){
                    case SUB_EVENT_STARRED:
                        if(this.getSubEvent()==SUB_EVENT_UNSTARRED){
                            visualization = VISUALIZATION_HIDDEN;
                        }else{
                            visualization = VISUALIZATION_REDUX;
                        }
                        break;
                    case SUB_EVENT_UNSTARRED:
                        if(this.getSubEvent()==SUB_EVENT_STARRED){
                            visualization = VISUALIZATION_HIDDEN;
                        }else{
                            visualization = VISUALIZATION_REDUX;
                        }
                        break;
                }
                break;
            case EVENT_DELETE:
                visualization = VISUALIZATION_HIDDEN;
                break;
        }
        this.setVisualization(visualization);
        return visualization;
    }



    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		JSON
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    public static LActivity fromJson(String json){
        try{
            return new Gson().fromJson(json, LActivity.class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WActivity:\n" + e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WActivity JSON:\n"+json);
            return null;
        }
    }

    public static LActivity[] fromJsonToArray(String json){
        try{
            return new Gson().fromJson(json, LActivity[].class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WActivity[]:\n"+e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WActivity[] JSON:\n"+json);
            return null;
        }
    }




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		EXTRA
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public LActivity loadExtraFromDB(Context context, boolean alsoFromServer){
        ThisApp app = ThisApp.getInstance(context);

        this.loadContainerSenderFromDB(context,alsoFromServer);

        if(this.resource==null && this.getResourceId()!=null){
            if(this.isFileResource()) {
                setResource(new LFileDAO(context).get(this.getResourceId()));
                if(this.resource!=null){
                    setSpace(((LFile)resource).getSpace(context));
                }
            }else if(this.isContactResource() || this.isGroupResource() || this.isMemberResource()){
                setResource(new LContactDAO(context).get(this.getResourceId()));
            }
        }
        return this;
    }
    public LActivity loadContainerSenderFromDB(Context context, boolean fromServer){
        ThisApp app = ThisApp.getInstance(context);

        //CONTAINER
        if(this.getContainer()==null) {
            if (this.isPersonal() || (this.getContainerId()!=null && this.getContainerId().equals(app.getEdoUserId()))) {

                this.setContainer(app.getUserContact());
                eLog.d("EdoActivityMan", "CONTAINER IS CURRENT EDO USER");

            } else if(this.getContainerId()!=null){

                LContact container = new LContactDAO(context).get(this.getContainerId());
                if (container == null && fromServer) {
                    if(this.isGroupContainer()){
                        container = new GroupDAO(context).download(this.getContainerId());
//                        eLog.d("EdoActivityMan", "CONTAINER GROUP DOWNLOADED: " +container!=null?container.getName():"null");
                    }else{
                        container = new ContactDAO(context).download(this.getContainerId());
//                        eLog.d("EdoActivityMan", "CONTAINER CONTACT DOWNLOADED: "+container!=null?container.getName():"null");
                    }
                }
                this.setContainer(container);

            }
        }
        eLog.d("EdoActivityMan", "CONTAINER: " + this.getContainerName());


        //SENDER
        if(this.getSender()==null) {
            if (this.isPersonal() || (this.getSenderId()!=null && this.getSenderId().equals(app.getEdoUserId()))) {

                this.setSender(app.getUserContact());
                eLog.i("EdoActivityMan", "SENDER IS CURRENT EDO USER");

            } else if(this.getSenderId()!=null){

                LContact sender = new LContactDAO(context).get(this.getSenderId());
                if (sender == null && fromServer) {
                    sender = new ContactDAO(context).download(this.getSenderId());
                    eLog.i("EdoActivityMan", "SENDER DOWNLOADED: " + sender!=null?sender.getName():"null");
                }
                this.setSender(sender);

            }
        }
        eLog.d("EdoActivityMan", "SENDER: " + this.getSenderName());
        return this;
    }


    public LActivity setExtra(EdoContainer sender, EdoContainer container, LSpace spaceWithCurrentFolder, EdoResource resource){
        this.setContainer(container);
        this.setSender(sender);
        this.setSpace(spaceWithCurrentFolder);
        this.setResource(resource);
        return this;
    }

    @Override
    public String getName() {
        return null;
    }
}
