package io.edo.db.local;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Looper;

import io.edo.db.web.request.EdoAuthManager;
import io.edo.edomobile.Act_00_SplashScreen;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoNotification;

public class LocalDBHelper extends SQLiteOpenHelper {
    private final Context mContext;

    public static final String DB_NAME = "LOCAL_EDO";
    private static final int DB_VERSION = 17;

    public static final String TABLE_CONTACT = "edoContact";
    public static final String TABLE_CONTACT_FILE = "edoContactFile";
    public static final String TABLE_FILE = "edoFile";
    public static final String TABLE_GROUP_MEMBER = "edoGroupMember";
    public static final String TABLE_SERVICE = "edoService";
    public static final String TABLE_USER = "edoUser";
    public static final String TABLE_ACTIVITY = "edoActivity";



    public static final String KEY_ACCESS_TOKEN = "accessToken";
    public static final String KEY_CATEGORYID = "categoryId";
    public static final String KEY_CLOUDID = "cloudId";
    public static final String KEY_COLLABSEMAILS = "collabsEmails";
    public static final String KEY_COLLECTIONID = "collectionId";
    public static final String KEY_CONFIGURATION = "configuration";
    public static final String KEY_CONTACTID = "contactId";
    public static final String KEY_CONTAINERID = "containerId";
    public static final String KEY_CONTAINERNAME = "containerName";
    public static final String KEY_CONTENT = "content";
    public static final String KEY_CONTEXT = "context";
    public static final String KEY_CREATEDBY = "createdBy";
    public static final String KEY_DEVICEID = "deviceId";
    public static final String KEY_DEVICETYPE = "deviceType";
    public static final String KEY_EDOUSERID = "edoUserId";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_EMAILSYNCDATE = "emailSyncDate";
    public static final String KEY_EXCHANGE = "exchange";
    public static final String KEY_EXTENDEDINFO = "extendedInfo";
    public static final String KEY_EXTENTION = "extention";
    public static final String KEY_EVENT = "event";
    public static final String KEY_FILEID = "fileId";
    public static final String KEY_GROUPID = "groupId";
    public static final String KEY_VTYPE = "vType";
    public static final String KEY_HASBEENPROCESSED = "hasBeenProcessed";
    public static final String KEY_HASNEWNOTES = "hasNewNotes";
    public static final String KEY_ID = "_id";
    public static final String KEY_ISFOLDER = "isFolder";
    public static final String KEY_ISSTARRED = "isStarred";
    public static final String KEY_ISUNREAD = "isUnread";
    public static final String KEY_LASTNOTIFICATIONMESSAGE = "lastNotificationMessage";
    public static final String KEY_LASTTOKENUPDATE = "lastTokenUpdate";
    public static final String KEY_LOCALURI = "localUri";
    public static final String KEY_LOGINCREDENTIALS = "loginCredentials";
    public static final String KEY_LOGINDATE = "loginDate";
    public static final String KEY_LOGINEMAIL = "loginEmail";
    public static final String KEY_LOGINSTATUS = "loginStatus";
    public static final String KEY_LONGDESCRIPTION = "longDescription";
    public static final String KEY_MDATE = "mDate";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_NAME = "name";
    public static final String KEY_NOTE = "note";
    public static final String KEY_NOTESCOUNT = "notesCount";
    public static final String KEY_NOTIFICATIONID = "notificationId";
    public static final String KEY_NOTIFICATIONSTATUS = "notificationStatus";
    public static final String KEY_ORIGINALPARENTID = "originalParentId";
    public static final String KEY_ORIGINALNAME = "originalName";
    public static final String KEY_OWNERID = "ownerId";
    public static final String KEY_PARENTID = "parentDirId";
    public static final String KEY_PARENTNAME = "parentName";
    public static final String KEY_PATH = "path";
    public static final String KEY_PRINCIPALEMAILADDRESS = "principalEmailAddress";
    public static final String KEY_ACTIVITYCOUNT = "activityCount";
    public static final String KEY_RANK = "rank";
    public static final String KEY_RESOURCEID = "resourceId";
    public static final String KEY_RESOURCEKIND = "resourceKind";
    public static final String KEY_RESOURCENAME = "resourceName";
    public static final String KEY_RESOURCETYPE = "resourceType";
    public static final String KEY_SDATE = "sDate";
    public static final String KEY_SENDERID = "senderId";
    public static final String KEY_SHORTDESCRIPTION = "shortDescription";
    public static final String KEY_SENDERNAME = "senderName";
    public static final String KEY_SERVICEID = "serviceId";
    public static final String KEY_SPACENAME = "spaceName";
    public static final String KEY_STATUS = "status";
    public static final String KEY_SUBEVENT = "subevent";
    public static final String KEY_SURNAME = "surname";
    public static final String KEY_THUMBNAIL = "thumbnail";
    public static final String KEY_TIMESTAMP = "timeStamp";
    public static final String KEY_TYPE = "type";
    public static final String KEY_WEBURI = "webUri";





    private static final String SQL_CREATE_TABLE_CONTACT = "create table "+TABLE_CONTACT+" (" +
            KEY_ID+" text unique, " +
            KEY_CLOUDID+" text unique, " +
            KEY_EDOUSERID+" String, " +
            KEY_NAME+" text, " +
            KEY_SURNAME+" text, " +
            KEY_ISSTARRED+" Integer, " +
            KEY_STATUS+" text, " +
            KEY_RANK+" text, " +
            KEY_EMAILSYNCDATE+" integer, " +
            KEY_MDATE+" integer, " +
            KEY_SDATE+" integer, " +
            KEY_OWNERID+" text, " +
            KEY_THUMBNAIL+" text, " +
            KEY_EXTENDEDINFO+" text," +
            KEY_ISUNREAD+" Integer, " +
            KEY_PRINCIPALEMAILADDRESS+" text, " +
            " UNIQUE ("+KEY_ID+") ON CONFLICT REPLACE); "+
            " UNIQUE ("+KEY_EDOUSERID+") ON CONFLICT REPLACE); "+
            " UNIQUE ("+KEY_PRINCIPALEMAILADDRESS+") ON CONFLICT REPLACE); "+
            "CREATE INDEX "+TABLE_CONTACT+"_index ON "+TABLE_CONTACT+" ("+KEY_ID+", "+KEY_NAME+", "+KEY_SURNAME+", "+KEY_PRINCIPALEMAILADDRESS+");";

    private static final String SQL_CREATE_TABLE_CONTACTFILE = "create table "+TABLE_CONTACT_FILE+" (" +
            KEY_CONTACTID+" String, " +
            KEY_FILEID+" String," +
            " UNIQUE ("+KEY_CONTACTID+","+KEY_FILEID+") ON CONFLICT IGNORE); "+
            "CREATE INDEX "+TABLE_CONTACT_FILE+"_index ON "+TABLE_CONTACT_FILE+" ("+KEY_CONTACTID+", "+KEY_FILEID+");";

    private static final String SQL_CREATE_TABLE_FILE = "create table "+TABLE_FILE+" (" +
            KEY_ID+" text unique, " +
            KEY_WEBURI+" text, " +
            KEY_CLOUDID+" text, " +
            KEY_NAME+" text, " +
            KEY_EXTENTION+" text, " +
            KEY_THUMBNAIL+" text, " +
            KEY_ISSTARRED+" Integer, " +
            KEY_STATUS+" text, " +
            KEY_NOTE+" text, " +
            KEY_TYPE+" text, " +
            KEY_VTYPE+" text, " +
            KEY_MDATE+" integer, " +
            KEY_COLLECTIONID+" text," +
            KEY_PARENTID+" String , " +
            KEY_SERVICEID+" String," +
            KEY_COLLABSEMAILS+" String," +
            KEY_PATH+" String , " +
            KEY_CREATEDBY+" text," +
            KEY_CONTENT+" text," +

            KEY_LOCALURI+" text, " +

            KEY_ISUNREAD+" Integer, " +
            KEY_HASNEWNOTES+" Integer, " +
            KEY_NOTESCOUNT+" Integer, " +

            " UNIQUE ("+KEY_ID+") ON CONFLICT REPLACE); "+
            "CREATE INDEX "+TABLE_FILE+"_index ON "+TABLE_FILE+" ("+KEY_ID+", "+KEY_PARENTID+");";


    private static final String SQL_CREATE_TABLE_GROUPMEMBER = "create table "+TABLE_GROUP_MEMBER+" (" +
            KEY_CONTACTID+" String, " +
            KEY_GROUPID+" String," +
            " UNIQUE ("+KEY_CONTACTID+","+KEY_GROUPID+") ON CONFLICT REPLACE); "+
            "CREATE INDEX "+TABLE_GROUP_MEMBER+"_index ON "+TABLE_GROUP_MEMBER+" ("+KEY_CONTACTID+", "+KEY_GROUPID+");";

    private static final String SQL_CREATE_TABLE_SERVICE = "create table "+TABLE_SERVICE+" (" +
            KEY_ID+" text unique, " +
            KEY_NAME+" text, " +
            KEY_EMAIL+" text, " +
            KEY_ACCESS_TOKEN+" text, " +
            KEY_CONFIGURATION+" text, " +
            KEY_STATUS+" text, " +
            KEY_MDATE+" integer," +
            KEY_ISUNREAD+" Integer, " +
            " UNIQUE ("+KEY_ID+") ON CONFLICT REPLACE); "+
            "CREATE INDEX "+TABLE_SERVICE+"_index ON "+TABLE_SERVICE+" ("+KEY_ID+", "+KEY_EMAIL+");";

    private static final String SQL_CREATE_TABLE_USER = "create table "+TABLE_USER+" (" +
            KEY_ID+" text, " +
            KEY_LOGINEMAIL+" text, " +
            KEY_LOGINCREDENTIALS+" text, " +
            KEY_CONTACTID+" String, " +
            KEY_STATUS+" text, " +

            KEY_LOGINDATE+" integer," +
            KEY_LOGINSTATUS+" integer," +
            KEY_DEVICEID+" text, " +
            KEY_NAME+" text," +
            KEY_SURNAME+" text," +
            KEY_LASTTOKENUPDATE+" integer," +
            KEY_NOTIFICATIONSTATUS+" text," +

            " UNIQUE ("+KEY_ID+") ON CONFLICT REPLACE); "+
            "CREATE INDEX "+TABLE_USER+"_index ON "+TABLE_USER+" ("+KEY_ID+");";

    public static final String SQL_CREATE_TABLE_ACTIVITY = "create table "+TABLE_ACTIVITY+" (" +
            KEY_ID+" text, " +
            KEY_EXCHANGE+" text, " +
            KEY_CONTEXT+" text," +
            KEY_CONTAINERID+" text," +
            KEY_SENDERID+" text," +
            KEY_RESOURCETYPE+" integer, " +
            KEY_RESOURCEID+" text," +
            KEY_MESSAGE+" text," +
            KEY_EVENT+" integer, " +
            KEY_SUBEVENT+" integer, " +
            KEY_ORIGINALPARENTID+" text, " +
            KEY_PARENTID+" text, " +
            KEY_PARENTNAME+" text, " +
            KEY_ISFOLDER+" integer, " +
            KEY_ORIGINALNAME+" text, " +
            KEY_RESOURCENAME+" text, " +

            KEY_CONTAINERNAME+" text," +
            KEY_SENDERNAME+" text," +

            KEY_SPACENAME+" text, " +

            KEY_VTYPE+" text, " +

            KEY_CONTENT+" text, " +
            KEY_SHORTDESCRIPTION+" text," +
            KEY_LONGDESCRIPTION+" text," +

            KEY_ISUNREAD+" integer," +
            KEY_STATUS+" integer, " +

            " UNIQUE ("+KEY_ID+") ON CONFLICT REPLACE); "+
            "CREATE INDEX "+TABLE_ACTIVITY+"_index ON "+TABLE_ACTIVITY+" ("+KEY_ID+", "+KEY_RESOURCEID+", "+KEY_CONTAINERID+", "+KEY_SENDERID+");";




    public LocalDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase database) {
        createDatabase(database);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase database, final int oldVersion, final int newVersion ) {
        new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
            ThisApp app = ThisApp.getInstance(mContext);


            if(oldVersion<=1){
                addColumn(database,TABLE_CONTACT,KEY_STATUS,"text");
            }

            if(oldVersion<=3){
                addColumn(database,TABLE_CONTACT, KEY_ISUNREAD,"integer");
                addColumn(database,TABLE_FILE, KEY_ISUNREAD,"integer");
                addColumn(database,TABLE_SERVICE, KEY_ISUNREAD,"integer");
            }

            if(oldVersion<=6){
                addColumn(database,TABLE_FILE, KEY_NOTESCOUNT,"integer");
            }

            if(oldVersion<=7){
                addColumn(database,TABLE_FILE, KEY_HASNEWNOTES, "integer");
            }

            if(oldVersion<=8){
                addColumn(database,TABLE_FILE, KEY_PATH, "text");
            }

            if(oldVersion<=9){
                addColumn(database,TABLE_CONTACT,KEY_THUMBNAIL,"text");
                addColumn(database,TABLE_CONTACT,KEY_OWNERID,"text");
                addColumn(database,TABLE_FILE,KEY_CREATEDBY,"text");
            }

            if(oldVersion<=12){
                addColumn(database,TABLE_USER, KEY_LOGINDATE, "integer");
                addColumn(database,TABLE_USER, KEY_LOGINSTATUS, "integer");
                addColumn(database,TABLE_USER, KEY_DEVICEID, "text");
                addColumn(database,TABLE_USER, KEY_NAME, "text");
                addColumn(database,TABLE_USER, KEY_SURNAME, "text");
                addColumn(database,TABLE_USER, KEY_LASTTOKENUPDATE, "integer");
                addColumn(database,TABLE_USER, KEY_NOTIFICATIONSTATUS, "text");

                addColumn(database,TABLE_CONTACT, KEY_PRINCIPALEMAILADDRESS, "text");

                addColumn(database,TABLE_FILE,KEY_THUMBNAIL,"text");
                addColumn(database,TABLE_FILE,KEY_SERVICEID,"text");
                addColumn(database,TABLE_FILE,KEY_COLLABSEMAILS,"text");

            }

            if(oldVersion<=16) {
                addColumn(database, TABLE_FILE, KEY_CONTENT, "text");
            }

            if(oldVersion<17) {
                EdoAuthManager.logout(mContext);
                new EdoNotification(mContext, Act_00_SplashScreen.class).showSimpleNotification(mContext.getResources().getString(R.string.app_name),
                        mContext.getResources().getString(R.string.newVersionMessage));
            }


            app.isDBUpdatingInProgress = false;
        }}.start();
    }

    private boolean addColumn(SQLiteDatabase database, String intTable, String column, String columnType){
        if(!existsColumnInTable(database,intTable,column)) {
            database.execSQL("ALTER TABLE "+intTable+" ADD COLUMN "+column+" "+columnType);
            return true;
        }
        return false;
    }

    private boolean existsColumnInTable(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
        try{
            //query 1 row
            Cursor mCursor  = inDatabase.rawQuery( "SELECT * FROM " + inTable + " LIMIT 0", null );

            //getColumnIndex gives us the index (0 to ...) of the column - otherwise we get a -1
            return mCursor.getColumnIndex(columnToCheck) != -1;

        }catch (Exception Exp){
            //something went wrong. Missing the database? The table?
            return false;
        }
    }


    public static void createDatabase(SQLiteDatabase database){
        database.execSQL(SQL_CREATE_TABLE_CONTACT);
        database.execSQL(SQL_CREATE_TABLE_CONTACTFILE);
        database.execSQL(SQL_CREATE_TABLE_FILE);
        database.execSQL(SQL_CREATE_TABLE_GROUPMEMBER);
        database.execSQL(SQL_CREATE_TABLE_SERVICE);
        database.execSQL(SQL_CREATE_TABLE_USER);
        database.execSQL(SQL_CREATE_TABLE_ACTIVITY);
    }

    public static void dropDatabase(SQLiteDatabase database){
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_CONTACT);
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_CONTACT_FILE);
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_FILE);
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_GROUP_MEMBER);
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_SERVICE);
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_USER);
        database.execSQL("DROP TABLE IF EXISTS "+TABLE_ACTIVITY);
    }














}
