package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import io.edo.db.local.LocalDBHelper;
import io.edo.edomobile.ThisApp;


public class LContactDAO extends LBaseDAO<LContact,String, ArrayList<LContact>, String> {
	String TABLE_NAME = LocalDBHelper.TABLE_CONTACT;

	public static final String QUERY_UNREAD_INFO =
			"(SELECT COUNT(*) "+
					"FROM "+LocalDBHelper.TABLE_ACTIVITY+" p " +
					"WHERE (con."+LocalDBHelper.KEY_EDOUSERID+"==p."+LocalDBHelper.KEY_CONTAINERID+" OR con."+LocalDBHelper.KEY_ID+"==p."+LocalDBHelper.KEY_CONTAINERID+") " +
					"AND p."+LocalDBHelper.KEY_ISUNREAD+"==1) " +
					"AS "+LocalDBHelper.KEY_ACTIVITYCOUNT+", "+

					"(SELECT pm."+LocalDBHelper.KEY_SHORTDESCRIPTION+" "+
					"FROM "+LocalDBHelper.TABLE_ACTIVITY+" pm " +
					"WHERE (con."+LocalDBHelper.KEY_EDOUSERID+"==pm."+LocalDBHelper.KEY_CONTAINERID+" OR con."+LocalDBHelper.KEY_ID+"==pm."+LocalDBHelper.KEY_CONTAINERID+") " +
                    "AND pm."+LocalDBHelper.KEY_SHORTDESCRIPTION+" IS NOT NULL " +
                    "ORDER BY pm."+LocalDBHelper.KEY_ID+" DESC LIMIT 1) " +
					"AS "+LocalDBHelper.KEY_LASTNOTIFICATIONMESSAGE+" ";

	public static String[] tableKeys = {LocalDBHelper.KEY_ID, 
		LocalDBHelper.KEY_CLOUDID, 
		LocalDBHelper.KEY_EDOUSERID, 
		LocalDBHelper.KEY_NAME,
		LocalDBHelper.KEY_SURNAME, 
		LocalDBHelper.KEY_ISSTARRED,
		LocalDBHelper.KEY_STATUS, 
		LocalDBHelper.KEY_RANK, 
		LocalDBHelper.KEY_EMAILSYNCDATE,
		LocalDBHelper.KEY_MDATE,
		LocalDBHelper.KEY_EXTENDEDINFO,
		LocalDBHelper.KEY_ISUNREAD,
		LocalDBHelper.KEY_THUMBNAIL,
		LocalDBHelper.KEY_OWNERID,
		LocalDBHelper.KEY_PRINCIPALEMAILADDRESS};

	public LContactDAO(Context context) {
		super(context);
	}

    @Override
    public ContentValues createContentValues(LContact contact) {

        ContentValues values = new ContentValues();
        if(contact.getId()!=null)values.put( LocalDBHelper.KEY_ID, contact.getId() );
        values.put( LocalDBHelper.KEY_CLOUDID, contact.getCloudIdServiceIdJsonList() );
        if(contact.getEdoUserId()!=null)values.put( LocalDBHelper.KEY_EDOUSERID, contact.getEdoUserId() );
        if(contact.getName()!=null)values.put( LocalDBHelper.KEY_NAME, contact.getName() );
        if(contact.getSurname()!=null)values.put( LocalDBHelper.KEY_SURNAME, contact.getSurname() );
        if(contact.getIsStarred()!=null)values.put( LocalDBHelper.KEY_ISSTARRED, contact.getIsStarred());
        if(contact.getStatus()!=null)values.put( LocalDBHelper.KEY_STATUS, contact.getStatus() );
        if(contact.getRank()!=null)values.put( LocalDBHelper.KEY_RANK, contact.getRank() );
        if(contact.getmDate()!=null)values.put( LocalDBHelper.KEY_MDATE, contact.getmDate() );
        if(contact.getExtendedInfosJson()!=null)values.put( LocalDBHelper.KEY_EXTENDEDINFO, contact.getExtendedInfosJson() );
        if(contact.getIsUnread()!=null)values.put(LocalDBHelper.KEY_ISUNREAD, contact.getIsUnread());
        if(contact.getThumbnail()!=null)values.put(LocalDBHelper.KEY_THUMBNAIL, contact.getThumbnail());
        if(contact.getGroupOwnerId()!=null)values.put(LocalDBHelper.KEY_OWNERID, contact.getGroupOwnerId());
        if(contact.getPrincipalEmailAddress()!=null)values.put( LocalDBHelper.KEY_PRINCIPALEMAILADDRESS, contact.getPrincipalEmailAddress() );
        return values;

    }

    @Override
    public synchronized boolean insert(LContact contact) {
        if(contact.getName()!=null)contact.setName(contact.getName().replace("'", "\'"));
        if(contact.getSurname()!=null)contact.setSurname(contact.getSurname().replace("'", "\'"));
        if(contact.getPrincipalEmailAddress()!=null)contact.setPrincipalEmailAddress(contact.getPrincipalEmailAddress().replace("'", "\'"));
        ContentValues initialValues = createContentValues(contact);
        long lid = db.insert(TABLE_NAME, initialValues);



        return lid>0;
    }


    private void insertAll(ArrayList<LContact> newContacts) {
//        ContentValues initialValues = createContentValues(new LContact());
//        String sql = db.getInsertQuery(TABLE_NAME,new );
//        SQLiteStatement statement = db.getDataBase().compileStatement(sql);
//        db.getDataBase().beginTransaction();
//        for (int i = 0; i<100; i++) {
//            statement.clearBindings();
//            statement.bindS(1, i);
//            statement.bindLong(2, i);
//            statement.bindLong(3, i*i);
//            statement.execute();
//        }
//        db.getDataBase().setTransactionSuccessful();
//        db.getDataBase().endTransaction();
    }


    @Override
    public synchronized boolean insertFromWebServer(LContact contact) {
        boolean inserted = false;

        if(contact.getPrincipalEmailAddress()!=null &&
                JavaUtils.isNotEmpty(contact.getPrincipalEmailAddress())){

            boolean readyToAdd = true;

            LContact oldContact = getByEmail(contact.getPrincipalEmailAddress());

            if(oldContact!=null){

                readyToAdd = delete(oldContact);

                if(contact.isSuggested()){
                    oldContact.setName(contact.getName());
                    oldContact.setSurname(contact.getSurname());
                    if(!oldContact.isActive() && !oldContact.isHidden())
                        oldContact.setStatus(contact.getStatus());
                    oldContact.setThumbnail(contact.getThumbnail());
                    contact = oldContact;

                }else if(JavaUtils.isEmpty(contact.getName()) && JavaUtils.isNotEmpty(oldContact.getName())){
                    contact.setName(oldContact.getName());
                    contact.setSurname(oldContact.getSurname());
                }

            }

            if(readyToAdd && contact!=null && insert(contact)){

                inserted = true;

            }

        }

        return inserted;
    }

    @Override
    public synchronized boolean update(LContact contact) {
        if(contact.getName()!=null)contact.setName(contact.getName().replace("'", "\'"));
        if(contact.getSurname()!=null)contact.setSurname(contact.getSurname().replace("'", "\'"));
        if(contact.getPrincipalEmailAddress()!=null)contact.setPrincipalEmailAddress(contact.getPrincipalEmailAddress().replace("'", "\'"));

        contact.setmDate(System.currentTimeMillis());

        ContentValues updatedValues = createContentValues(contact);
        String whereClause = LocalDBHelper.KEY_ID+" = '"+ contact.getId()+"'";
        boolean updated = db.update(TABLE_NAME, updatedValues, whereClause);
        return updated;
    }

    public synchronized boolean updateMDate(String idOrUserId, long mDate){
        ContentValues values = new ContentValues();
        values.put(LocalDBHelper.KEY_MDATE, mDate);
        String whereClause =  LocalDBHelper.KEY_ID+"= '"+idOrUserId+"' OR "+LocalDBHelper.KEY_EDOUSERID+"= '"+idOrUserId+"'";
        boolean updated = db.update(TABLE_NAME, values, whereClause);

        return updated;
    }

    public synchronized boolean updateThumbail(String idOrUserId, String thumbnail){
        ContentValues values = new ContentValues();
        values.put( LocalDBHelper.KEY_THUMBNAIL, thumbnail );
        String whereClause =  LocalDBHelper.KEY_ID+"= '"+idOrUserId+"' OR "+LocalDBHelper.KEY_EDOUSERID+"= '"+idOrUserId+"'";
        boolean updated = db.update(TABLE_NAME, values, whereClause);

        return updated;
    }

    @Override
    public synchronized boolean delete(LContact contact) {
        boolean deleted = false;

        if(contact!=null){
            String whereClause = LocalDBHelper.KEY_ID+" = '"+ contact.getId()+"'";
            deleted =  db.delete(TABLE_NAME, whereClause);

            if(deleted){
               new LActivityDAO(context).setAllActivitiesRedBy(contact.getId(),contact.getEdoUserId());
            }
        }
        return deleted;
    }

    @Override
    public synchronized LContact get(String idOrEdoUserId) {
        if(idOrEdoUserId==null)
            return null;

        LContact eContact = null;
        String query = "SELECT con.* , " +

                QUERY_UNREAD_INFO +

                "FROM "+LocalDBHelper.TABLE_CONTACT+" con "+
                "WHERE "+LocalDBHelper.KEY_ID+" = ? "+
                "OR "+LocalDBHelper.KEY_EDOUSERID+" = ? ";


        Cursor cursor = db.executeQuery(query, new String[]{idOrEdoUserId, idOrEdoUserId});
        try {

            if(cursor.moveToNext()){
                eContact = new LContact(context, cursor);
                eContact.setUnreadPushCount(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ACTIVITYCOUNT)));
                eContact.setLastNotificationMessage(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_LASTNOTIFICATIONMESSAGE)));
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }



        return eContact;
    }

    public synchronized LContact getByEmail(String principalEmailAddress){
        if(principalEmailAddress==null)
            return null;

        String whereClause = LocalDBHelper.KEY_PRINCIPALEMAILADDRESS+"= ?";
        Cursor cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{principalEmailAddress});
        LContact con = null;
        try {

            if(cursor.moveToNext()){
                con = new LContact(context,cursor);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }

        return con;
    }

    @Override
    public synchronized ArrayList<LContact> getList() {
        return getList(-1,-1);
    }

    @Override
    public ArrayList<LContact> getListBy(String status) {
        String userId = app.getEdoUserId();
        String userEmail = app.getUserEmail();

        ArrayList<LContact> contacts = new ArrayList<LContact> ();
        String query = "SELECT con.* , " +

                QUERY_UNREAD_INFO+

                "FROM "+LocalDBHelper.TABLE_CONTACT+" con "+
                "WHERE con."+LocalDBHelper.KEY_EDOUSERID+" IS NOT ? "+
                "AND con."+LocalDBHelper.KEY_PRINCIPALEMAILADDRESS+" IS NOT ? " +
                "ORDER BY con."+LocalDBHelper.KEY_STATUS+" == ?, "+"con."+LocalDBHelper.KEY_NAME+" COLLATE NOCASE ASC";

                ;


        Cursor cursor = db.executeQuery(query, new String[]{userId,userEmail,status});
        try {

            while(cursor.moveToNext()){

                LContact contact = new LContact(context,cursor);
                contact.setUnreadPushCount(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ACTIVITYCOUNT)));
                contact.setLastNotificationMessage(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_LASTNOTIFICATIONMESSAGE)));
                contacts.add(contact);

            }

        } finally {
            if(cursor != null)
                cursor.close();
        }


        return contacts;
    }


    public synchronized ArrayList<LContact> getList(int offset, int limit){
        String userId = app.getEdoUserId();
        String userEmail = app.getUserEmail();


        ArrayList<LContact> contacts = new ArrayList<LContact> ();
        String query = "SELECT con.* " +

                "FROM "+LocalDBHelper.TABLE_CONTACT+" con "+
                "WHERE con."+LocalDBHelper.KEY_EDOUSERID+" IS NOT ? "+
                "AND con."+LocalDBHelper.KEY_PRINCIPALEMAILADDRESS+" IS NOT ? " +
                "ORDER BY con."+LocalDBHelper.KEY_NAME+" COLLATE NOCASE ASC";

        if(offset>=0 && limit>=0){
            query += " LIMIT "+offset+","+limit;
        }


        Cursor cursor = db.executeQuery(query, new String[]{userId,userEmail});
        try {

            while(cursor.moveToNext()){
                LContact contact = new LContact(context,cursor);
                contacts.add(contact);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }



        return contacts;
    }




    public synchronized ArrayList<LContact> getActiveList(){

        String userId = app.getEdoUserId();

        ArrayList<LContact> contacts = new ArrayList<LContact> ();
        String query = "SELECT con.* , " +

                QUERY_UNREAD_INFO+

                "FROM "+LocalDBHelper.TABLE_CONTACT+" con "+
                "WHERE con."+LocalDBHelper.KEY_EDOUSERID+" IS NOT ? "+
                "AND (con."+LocalDBHelper.KEY_STATUS+" == '"+LContact.STATUS_ACTIVE+"' "
                + "OR con."+LocalDBHelper.KEY_STATUS+" == '"+LContact.STATUS_SUGGESTED+"' "
                + "OR con."+LocalDBHelper.KEY_OWNERID+" IS NOT NULL) "+
                "ORDER BY con."+LocalDBHelper.KEY_MDATE+" DESC"

                ;


        Cursor cursor = db.executeQuery(query, new String[]{userId});
        try {

            while(cursor.moveToNext()){

                LContact contact = new LContact(context,cursor);
                contact.setUnreadPushCount(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ACTIVITYCOUNT)));
                contact.setLastNotificationMessage(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_LASTNOTIFICATIONMESSAGE)));
                contacts.add(contact);

            }

        } finally {
            if(cursor != null)
                cursor.close();
        }


        return contacts;
    }

    public synchronized ArrayList<LContact> getContactByNameLike(String name, final boolean activeFirst){


        String userId = ThisApp.getInstance(context).getEdoUserId();
        String userEmail = ThisApp.getInstance(context).getEdoUserId();

        ArrayList<LContact> contacts = new ArrayList<LContact> ();

        String surname = name;
        if(name.contains(" ") && name.split(" ").length>1)
            surname = name.split(" ")[1];

        String whereClause = "("+ LocalDBHelper.KEY_NAME+" LIKE ? OR "+LocalDBHelper.KEY_SURNAME+" LIKE ? OR "+LocalDBHelper.KEY_PRINCIPALEMAILADDRESS+" LIKE ?) " +
                "AND "+ LocalDBHelper.KEY_EDOUSERID+" IS NOT ? "+
                "AND "+LocalDBHelper.KEY_PRINCIPALEMAILADDRESS+" IS NOT ? ";


        Cursor cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{"%" + name + "%", "%" + surname + "%", "%" + name + "%", userId, userEmail});
        try {

            while(cursor.moveToNext()){
                LContact contact = new LContact(context,cursor);

                contacts.add(contact);
            }


            final String nameLike = name;

            Comparator<LContact> comparator = new Comparator<LContact>() {

                @Override
                public int compare(LContact con1, LContact con2) {
                    // TODO Auto-generated method stub

                    if(activeFirst){
                        int compActive = compareContactActive(con1, con2);
                        if(compActive!=0){
                            return compActive;
                        }
                    }

                    int compName = compareContactAttribute(con1.getName(), con2.getName(), nameLike);
                    if(compName==0){
                        int compSurname = compareContactAttribute(con1.getSurname(), con2.getSurname(), nameLike);
                        if(compSurname==0){
                            int compEmail = compareContactAttribute(con1.getPrincipalEmailAddress(), con2.getPrincipalEmailAddress(), nameLike);
                            return compEmail;
                        }
                        return compSurname;
                    }
                    return compName;
                }
            };

            Collections.sort(contacts, comparator);




        } finally {
            if(cursor != null)
                cursor.close();
        }



        return contacts;
    }

    private int compareContactAttribute(String att1, String att2, String text){
        if(att1==null && att2!=null)
            return -1;
        else if(att1!=null && att2==null)
            return 1;
        else if(att1==null && att2 == null)
            return 0;


        int diffName1 = att1.compareTo(text);
        int diffName2 = att2.compareTo(text);
        if(diffName1>diffName2){
            return -1;
        }else if(diffName2>diffName1){
            return 1;
        }
        return 0;
    }

    private int compareContactActive(LContact contact1, LContact contact2){
        String att1 = contact1.getStatus();
        String att2 = contact2.getStatus();

        if(att1==null && att2==null)
            return 0;
        else if((att1!=null && att1.equals(LContact.STATUS_ACTIVE)) && (att2!=null && att2.equals(LContact.STATUS_ACTIVE)))
            return contact2.getmDate().compareTo(contact1.getmDate());
        else if((att1!=null && att1.equals(LContact.STATUS_ACTIVE)) && (att2==null || !att2.equals(LContact.STATUS_ACTIVE)))
            return -1;
        else if((att2!=null && att2.equals(LContact.STATUS_ACTIVE)) && (att1==null || !att1.equals(LContact.STATUS_ACTIVE)))
            return 1;
        else if((att1!=null && att1.equals(LContact.STATUS_HIDDEN)) && (att2==null || !att2.equals(LContact.STATUS_HIDDEN)))
            return -1;
        else if((att2!=null && att2.equals(LContact.STATUS_HIDDEN)) && (att1==null || !att1.equals(LContact.STATUS_HIDDEN)))
            return 1;
        else
            return 0;
    }

    public synchronized Cursor getContactsOnlyCursorByNameLike(String name){
        if(name==null)
            return null;

        String userId = ThisApp.getInstance(context).getEdoUserId();
        String userEmail = ThisApp.getInstance(context).getUserEmail();

        String surname = name;
        if(name.contains(" ") && name.split(" ").length>1)
            surname = name.split(" ")[1];

        String whereClause = "("+ LocalDBHelper.KEY_NAME+" LIKE ? OR "+LocalDBHelper.KEY_SURNAME+" LIKE ? OR "+LocalDBHelper.KEY_PRINCIPALEMAILADDRESS+" LIKE ?) " +
                "AND "+ LocalDBHelper.KEY_EDOUSERID+" IS NOT ? "+
                "AND "+LocalDBHelper.KEY_PRINCIPALEMAILADDRESS+" IS NOT ? " +
                "AND "+LocalDBHelper.KEY_OWNERID+" IS NULL ";


        return db.select(TABLE_NAME, tableKeys, whereClause, new String[]{"%"+ name +"%","%"+ surname +"%","%"+ name +"%",userId,userEmail}, LocalDBHelper.KEY_NAME+" ASC");

    }
}
