package io.edo.db.local.beans;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;

import com.google.gson.Gson;
import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.WebUtils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.support.Comment;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.EdoResource;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LSpaceDAO;
import io.edo.ui.UIManager;
import io.edo.utilities.Constants;
import io.edo.utilities.ObjectId;
import io.edo.utilities.eLog;

public class LFile implements EdoResource {
    private static final long serialVersionUID = 1L;

    public static final String FILE_TYPE_EDOTAB = "edotab";
    public static final String FILE_TYPE_EDOFOLDER = "edofolder";
    public static final String FILE_TYPE_SNIPPET = "edonote";
    public static final String FILE_TYPE_WEBLINK = "weblink";
    public static final String FILE_TYPE_YOUTUBE_VIDEO = "ytv";
//	public static final String FILE_TYPE_GMAP = "gmap";

    public static final String STATUS_DEFAULT = "default";
    public static final String STATUS_NOT_SAVED_TO_SERVER = "notSavedToServer";

    public static final String FILE_PATH_SEPARATOR = "|";

    private String _id=null;
    private String uri=null;
    private String cloudId=null;
    private String name=null;
    private String extension=null;
    private String thumbnail=null;
    private Integer isStarred = 0;
    private String status = null;
    private Comment[] notes = null;
    private String type = null;
    private Long mDate = null;
    private String collectionId = null;
    private String parent = null;
    private String serviceId = null;
    private String parentCloudId = null;
    private String[] collabsEmails;
    private String path = null;
    private String createdBy;
    private String content;

    private String localUri = null;

    private String vType = null;

    private Integer isUnread = null;
    private Integer hasNewComments = null;
    private Integer commentsCount = null;

    private Boolean isSelected = null;


    public LFile() {
        super();
    }

    public LFile (String id){
        this._id = id;
    }

    public LFile getLFile(){
        return this;
    }

    public LFile(Cursor cursor) {
        setId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_ID)));
        setWebUri(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_WEBURI)));
        setCloudId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CLOUDID)));
        setName(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_NAME)));
        setExtension(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_EXTENTION)));
        setThumbnail(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_THUMBNAIL)));
        setIsStarred(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ISSTARRED)) == 1);
        setStatus(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_STATUS)));
        setComments(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_NOTE)));
        setType(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_TYPE)));
        setmDate(cursor.getLong(cursor.getColumnIndex(LocalDBHelper.KEY_MDATE)));
        setCollectionId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_COLLECTIONID)));
        setParentId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_PARENTID)));
        setServiceId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_SERVICEID)));
        setCollabsEmails(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_COLLABSEMAILS)));
        setPath(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_PATH)));
        setCreatedBy(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CREATEDBY)));
        setContent(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTENT)));

        setLocalUri(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_LOCALURI)));

        setIsUnread((cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ISUNREAD))));
        setHasNewComments((cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_HASNEWNOTES))));
        setCommentsCount((cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_NOTESCOUNT))));
    }


    public static LFile copy(LFile toFolder, LFile fileToBeCopied){
        LFile edoFile = fileToBeCopied.getLFile();
        ObjectId id = new ObjectId();
        edoFile._id = id.toString();
        edoFile.parent = toFolder.getId();
        edoFile.notes = null;
        edoFile.mDate = System.currentTimeMillis();

        if(edoFile.type.equals(LFile.FILE_TYPE_EDOFOLDER)){
            String oldPath = toFolder.path!=null ? (toFolder.path + LFile.FILE_PATH_SEPARATOR) : "";
            edoFile.path = oldPath + toFolder.getId();
        }

        return edoFile;
    }



    public static LFile newLocalFile(String parentEdoFolderId, String cloudId, String filePath, String mime, String createdBy){
        LFile edoFile = new LFile();
        ObjectId id = new ObjectId();
        edoFile._id = id.toString();
        edoFile.localUri = filePath;
        edoFile.cloudId = cloudId;
        edoFile.name = FilenameUtils.getBaseName(filePath);
        edoFile.isStarred = 0;
        edoFile.parent = parentEdoFolderId;
        if(mime!=null)
            edoFile.type = mime;
        else
            edoFile.type = FileInfo.getMime(filePath);
        edoFile.extension = FilenameUtils.getExtension(filePath);
        edoFile.mDate = System.currentTimeMillis();
        edoFile.createdBy = createdBy;
        edoFile.content = null;

        edoFile.commentsCount = 0;
        edoFile.isUnread = 0;
        edoFile.hasNewComments = 0;

        edoFile.vType = edoFile.generateVtype();

        return edoFile;
    }

    public static LFile newLinkFile(String parentDirId, String name, String link, String createdBy){
        LFile edoFile = new LFile();
        ObjectId id = new ObjectId();
        edoFile._id = id.toString();
        edoFile.name = name;
        if(JavaUtils.isEmpty(name))
            edoFile.name = link;
        edoFile.uri = link;
        edoFile.isStarred = 0;
        edoFile.parent = parentDirId;
        edoFile.type = FILE_TYPE_WEBLINK;
        if(WebUtils.isYoutubeUrl(link))
            edoFile.type = FILE_TYPE_YOUTUBE_VIDEO;
        edoFile.mDate = System.currentTimeMillis();
        edoFile.createdBy = createdBy;
        edoFile.content = null;

        edoFile.commentsCount = 0;
        edoFile.isUnread = 0;
        edoFile.hasNewComments = 0;

        edoFile.vType = edoFile.generateVtype();

        return edoFile;
    }

    public static LFile newSnippetFile(String parentDirId, String snippetTitle, String snippetText, String createdBy){
        LFile edoFile = new LFile();
        ObjectId id = new ObjectId();
        edoFile._id = id.toString();

        String name = snippetTitle;
        if(JavaUtils.isEmpty(name)){
            name = snippetText;
            if(snippetText.length()>10){
                name = snippetText.substring(0, 10).replaceAll(" ", "_").replaceAll("/", "");
            }
        }

        edoFile.name = name;
        edoFile.uri = null;
        edoFile.isStarred = 0;
        edoFile.parent = parentDirId;
        edoFile.type = FILE_TYPE_SNIPPET;
        edoFile.mDate = System.currentTimeMillis();
        edoFile.createdBy = createdBy;
        edoFile.content = snippetText;

        edoFile.commentsCount = 0;
        edoFile.isUnread = 0;
        edoFile.hasNewComments = 0;

        edoFile.vType = edoFile.generateVtype();

        return edoFile;
    }



    public static LFile newFolderFile(LFile parentFolder, String folderName, String createdBy){
        LFile edoFile = new LFile();
        ObjectId id = new ObjectId();
        edoFile._id = id.toString();
        edoFile.name = folderName;
        edoFile.parent = parentFolder._id;
        edoFile.isStarred = 0;
        edoFile.type = FILE_TYPE_EDOFOLDER;
        edoFile.mDate = System.currentTimeMillis();
        edoFile.createdBy = createdBy;

        if(edoFile.type.equals(LFile.FILE_TYPE_EDOFOLDER)){
            String oldPath = parentFolder.path!=null ? (parentFolder.path + LFile.FILE_PATH_SEPARATOR) : "";
            edoFile.path = oldPath + parentFolder.getId();
        }

        edoFile.commentsCount = 0;
        edoFile.isUnread = 0;
        edoFile.hasNewComments = 0;

        edoFile.vType = edoFile.generateVtype();

        return edoFile;
    }

    public LFile(String parentId, com.google.api.services.drive.model.File dFile, String createdBy){
        ObjectId id = new ObjectId();
        this._id = id.toString();
        this.uri = dFile.getAlternateLink();
        this.cloudId = dFile.getId();
        this.name = FilenameUtils.getBaseName(dFile.getTitle());
        this.extension = FilenameUtils.getExtension(dFile.getTitle());
        this.isStarred = 0;
        this.parent = parentId;
        this.type = dFile.getMimeType();
        this.mDate = System.currentTimeMillis();
        this.createdBy = createdBy;
        this.thumbnail = dFile.getThumbnailLink();

        this.commentsCount = 0;
        this.isUnread = 0;
        this.hasNewComments = 0;

        this.vType = this.generateVtype();

    }

    public static LFile baseForWebUpdate(LSpace space, LFile fileToUpdate){
        LFile edoFile = new LFile();
        edoFile._id = fileToUpdate.getId();
        edoFile.cloudId = fileToUpdate.getCloudId();
        edoFile.type = fileToUpdate.getType();
        edoFile.serviceId = fileToUpdate.getServiceId();
//        edoFile.parentCloudId = space.getCloudId();
        edoFile.isStarred = null;
        return edoFile;
    }


    public String getWebFileJson() {
        LFile wfile = new LFile();
        wfile._id = this.getId();
        wfile.uri = this.getWebUri();
        wfile.cloudId = this.getCloudId();
        wfile.name = this.getName();
        wfile.extension = this.getExtension();
        wfile.isStarred = this.getIsStarred();
        wfile.status = this.getStatus();
        wfile.notes = this.getComments();
        wfile.type = this.getType();
        wfile.mDate = this.getmDate();
        //		wfile.createdBy = this.getCreatedby();
        wfile.collectionId = this.getCollectionId();
        wfile.parent = this.getParentId();
        wfile.path = this.getPath();

        wfile.content = this.getContent();

        if(!wfile.isWebLink())
            wfile.serviceId = this.serviceId;

        wfile.parentCloudId = this.parentCloudId;

        if(wfile.isSpace())
            wfile.collabsEmails = this.getCollabsEmails();

        if(wfile.isSpace())
            wfile.thumbnail = this.getThumbnail();
        else
            wfile.thumbnail = null;

        return wfile.toJson();
    }



    @Override
    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }


    public String getWebUri() {
        return uri;
    }

    public void setWebUri(String uri) {
        this.uri = uri;
    }

    public String getCloudId() {
        return cloudId;
    }

    public void setCloudId(String cloudId) {
        this.cloudId = cloudId;
    }
    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Boolean isStarred() {
        return isStarred!=null && isStarred == 1;
    }

    public Integer getIsStarred(){
        return isStarred;
    }

    public void setIsStarred(Boolean isStarred) {
        this.isStarred = isStarred ? 1:0;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getmDate() {
        return mDate;
    }

    public void setmDate(Long mDate) {
        this.mDate = mDate;
    }

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }

    public String getParentId() {
        return parent;
    }

    public void setParentId(String parentId) {
        this.parent = parentId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getParentCloudId() {
        return parentCloudId;
    }

    public void setParentCloudId(String parentCloudId) {
        this.parentCloudId = parentCloudId;
    }

    public String[] getCollabsEmails() {
        return collabsEmails;
    }

    public String getCollabsEmailsJson() {
        if(collabsEmails!=null && collabsEmails.length>0){
            return new Gson().toJson(collabsEmails);
        }

        return null;
    }

    public void setCollabsEmails(String[] collabsEmails) {
        this.collabsEmails = collabsEmails;
    }

    public void setCollabsEmails(String collabsEmailsJson) {
        try{
            this.collabsEmails = new Gson().fromJson(collabsEmailsJson, String[].class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "collabsEmails[]:\n"+e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "collabsEmails[] JSON:\n"+collabsEmailsJson);

            if(JavaUtils.isEmailAddress(collabsEmailsJson)){
                eLog.i(Constants.TAG_JSON_SYNTAX_EXCEPTION, "collabsEmailsJson is an email address: "+collabsEmailsJson);
                this.collabsEmails = new String[]{collabsEmailsJson};
            }


        }
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLocalUri() {
        return localUri;
    }

    public void setLocalUri(String localUri) {
        this.localUri = localUri;
    }

    public String getvType() {
        if(vType==null){
            vType = generateVtype();
        }
        return vType;
    }

    public void setvType(String vType) {
        this.vType = vType;
    }

    public Integer getIsUnread() {
        return isUnread;
    }

    public boolean isUnread() {
        return isUnread!=null && isUnread==1;
    }

    public void setIsUnread(Integer isUnread) {
        this.isUnread = isUnread;
    }

    public Integer getHasNewComments() {
        return hasNewComments;
    }

    public void setHasNewComments(Integer hasNewComments) {
        this.hasNewComments = hasNewComments;
    }

    public void setHasNewComments(boolean hasNewComments) {
        this.hasNewComments = hasNewComments ? 1:0;
    }

    public boolean hasNewComments() {
        return hasNewComments!=null && hasNewComments==1;
    }

    public Comment[] getComments() {
        return notes;
    }

    public String getCommentsJson() {

        if(notes!=null && notes.length>0){
            return new Gson().toJson(notes);
        }

        return null;
    }

    public void setComments(Comment[] comments) {
        this.notes = comments;
        this.setCommentsCount();
    }

    public void setComments(ArrayList<Comment> comments) {
        if(comments==null)
            return;

        Comment[] cs = new Comment[comments.size()];
        comments.toArray(cs);

        this.notes = cs;
        this.setCommentsCount();
    }

    public void setComments(String commentsJson) {
        setComments(Comment.fromJsonList(commentsJson));
    }

    public boolean isSelected() {
        return isSelected!=null?isSelected:false;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }






    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		DERIVED
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    //NAME

    public String getNameWithExt() {
        if(extension!=null)
            return name+"."+extension;
        return name;
    }



    //THUMBNAIL

    public boolean hasThumbnail() {
        return JavaUtils.isNotEmpty(thumbnail);
    }

    public int getThumbnailColor(Context c, int defColorRes){
        if(hasThumbnail()){
            try {
                return Color.parseColor(this.getThumbnail());
            }catch (Exception e){}
        }
        return c.getResources().getColor(defColorRes);
    }

    //PATH

    public ArrayList<String> getPathIds(){
        if(JavaUtils.isNotEmpty(this.path)){
            if(this.path.contains(FILE_PATH_SEPARATOR)){
                String[] ids = this.path.split("["+FILE_PATH_SEPARATOR+"]");
                return new ArrayList<String>(Arrays.asList(ids));
            }else{
                ArrayList<String> ids = new ArrayList<String>();
                ids.add(this.path);
                return ids;
            }
        }

        return null;
    }

//    public String getDevicePath(Context context, LocalDBManager db){
//        LSpaceDAO spaceDAO = new LSpaceDAO(context);
//        if(!this.isSpace()){
//            LSpace space = this.getSpace(context);
//            if(space!=null){
//                String folderPath = spaceDAO.getSpaceFolderPath(null, space);
//                if(folderPath!=null)
//                    return folderPath+File.separator+getNameWithExt();
//            }
//        }else{
//            return spaceDAO.getSpaceFolderPath(null, this);
//        }
//        return null;
//    }


    public LSpace getSpace(Context context){
        LFileDAO fileDAO = new LFileDAO(context);
        LSpaceDAO spaceDAO = new LSpaceDAO(context);
        LFile currentFolder = fileDAO.get(this.getParentId());
        if (currentFolder != null) {
            String spaceId = currentFolder.getId();
            if (!currentFolder.isSpace()) {

                ArrayList<String> pathIds = currentFolder.getPathIds();

                if (pathIds != null && pathIds.size() > 0) {
                    spaceId = pathIds.get(0);
                } else {
                    spaceId = null;
                }
            }
            if (JavaUtils.isNotEmpty(spaceId)) {
                LSpace space = spaceDAO.get(spaceId);
                if (space != null) {
                    space.setCurrentFolder(currentFolder);
                    return space;
                }

            }
        }


        return null;
    }




    // DATE

    public long getCDate() {
        try{
            ObjectId id = new ObjectId(_id);
            return id.getDate().getTime();
        }catch (Exception e){}
        return System.currentTimeMillis();
    }



    //LOCATION

    public boolean isStored() {
        if(JavaUtils.isNotEmpty(localUri) && new File(localUri).exists())
            return true;
        return false;
    }

    public boolean isInCloud() {
        if(JavaUtils.isNotEmpty(cloudId))
            return true;
        return false;
    }



    //SERVICE

    public LService getService(Context c){

        LService service = serviceId!=null ? new LServiceDAO(c).get(serviceId) : null;
        if(service==null){
            service = new LServiceDAO(c).getPrincipalService();
        }

        return service;
    }



    //COMMENTS

    public ArrayList<Comment> getCommentsList(){
        if(notes!=null){
            return new ArrayList<Comment>(Arrays.asList(notes));
        }
        return null;
    }


    public void setCommentSingle(Comment comment) {
        Comment[] comments = new Comment[1];
        comments[0] = comment;
        setComments(comments);
    }

    public void addNewComment(Comment comment){
        ArrayList<Comment> commentsList = new ArrayList<Comment>();

        if(notes != null){
            commentsList = getCommentsList();
        }

        commentsList.add(comment);
        setComments(commentsList.toArray(new Comment[commentsList.size()]));
    }

    public void removeComment(int commentIndex){

        ArrayList<Comment> commentsList = getCommentsList();

        if(commentsList!=null && commentsList.size()>0){
            commentsList.remove(commentIndex);
        }

        setComments(commentsList.toArray(new Comment[commentsList.size()]));
    }


    public int getCommentsCount() {
        if(commentsCount!=null && commentsCount>0)
            return commentsCount;

        if(notes!=null && notes.length>0)
            return notes.length;

        return 0;
    }

    public boolean hasComments() {
        return (getCommentsCount()> 0) || (notes!=null && notes.length>0);
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public void setCommentsCount() {
        this.commentsCount = notes!=null && notes.length>0 ? notes.length : 0;
    }


    public synchronized EdoContainer getContainer(Context context){
        LFileDAO fileDAO = new LFileDAO(context);
        return fileDAO.getContainerOf(this);
    }


    //STATUS
    public boolean isUploading(Context c){
        final LActivity a = UIManager.getActivityByResourceId(c, this.getId());
        return a!=null && a.getEvent() == LActivity.EVENT_UPLOAD_TO_CLOUD && a.isInProgress();
    }

    public boolean isDownloading(Context c){
        final LActivity a = UIManager.getActivityByResourceId(c, this.getId());
        return a!=null && a.getEvent() == LActivity.EVENT_DOWNLOAD_FROM_CLOUD && a.isInProgress();
    }

    public boolean hasTransferError(Context c) {
        final LActivity a = UIManager.getActivityByResourceId(c, this.getId());
        return a != null && a.hasError();
    }

    public int getProgressPercent(Context c){
        final LActivity a = UIManager.getActivityByResourceId(c, this.getId());
        if(a!=null)
            return a.getProgressPercent();
        return -1;
    }

    public boolean isNotSavedToServer() {
        return status != null && status.equals(STATUS_NOT_SAVED_TO_SERVER);
    }






    //TYPE

    public boolean isSpace(){
        return JavaUtils.isNotEmpty(this.getType()) && this.getType().equals(LFile.FILE_TYPE_EDOTAB);
    }
    public boolean isFolder(){
        return JavaUtils.isNotEmpty(this.getType()) && this.getType().equals(FILE_TYPE_EDOFOLDER);
    }

    public boolean isSnippet(){
        return type!=null && type.equals(FILE_TYPE_SNIPPET);
    }

    public boolean isWebLink(){
        return (type!=null && type.equals(FILE_TYPE_WEBLINK) && JavaUtils.isWebUrl(uri)) || isYoutubeLink();
    }

    public boolean isYoutubeLink(){
        return type!=null && type.equals(FILE_TYPE_YOUTUBE_VIDEO) && JavaUtils.isWebUrl(uri);
    }

//	public boolean isGMap(){
//		return type!=null && type.equals(FILE_TYPE_GMAP) && JavaUtils.isNotEmpty(cloudId);
//	}

    public boolean isGdoc(){
        return type!=null && (type.contains("vnd.google-apps") || type.contains("gdoc")) && JavaUtils.isNotEmpty(cloudId);
    }

    public boolean isImageRaster(){
        if(type!=null && (type.contains("image")))
            return true;
        else if(vType!=null && vType.equals(FileInfo.TYPE_RASTER))
            return true;
        else{
            this.vType = generateVtype();
            if(vType!=null && vType.equals(FileInfo.TYPE_RASTER))
                return true;
        }
        return false;
    }

    public boolean isVideo(){
        if(type!=null && (type.contains("video")))
            return true;
        else if(vType!=null && vType.equals(FileInfo.TYPE_VIDEO))
            return true;
        else{
            this.vType = generateVtype();
            if(vType!=null && vType.equals(FileInfo.TYPE_VIDEO))
                return true;
        }
        return false;
    }

    public boolean isAudio(){
        if(type!=null && (type.contains("audio")))
            return true;
        else if(vType!=null && vType.equals(FileInfo.TYPE_AUDIO))
            return true;
        else{
            this.vType = generateVtype();
            if(vType!=null && vType.equals(FileInfo.TYPE_AUDIO))
                return true;
        }
        return false;
    }

    public boolean isOnlyMetaFile(){
        return isSpace() || isFolder() || isSnippet() || isWebLink() || isGdoc();
    }

    public String generateVtype(){

        if(isFolder()) {
            return FileInfo.TYPE_FOLDER;
        }else if(isWebLink()){
            return FileInfo.TYPE_WEB;
        }else if(isGdoc()){
            return FileInfo.TYPE_GDOC;
        }else if(isOnlyMetaFile()){
            return getType();
        }

        String vType = null;

        if(JavaUtils.isNotEmpty(extension))
            vType = FileInfo.getTypeByExt(extension);
        if((JavaUtils.isEmpty(vType) || (JavaUtils.isNotEmpty(vType) && (vType).equals(FileInfo.TYPE_UNKNOWN))) && JavaUtils.isNotEmpty(type))
            vType = FileInfo.getTypeByMime(type);
        if((JavaUtils.isEmpty(vType) || (JavaUtils.isNotEmpty(vType) && (vType).equals(FileInfo.TYPE_UNKNOWN))) && JavaUtils.isNotEmpty(localUri))
            vType = FileInfo.getTypeByPath(localUri);
        if((JavaUtils.isEmpty(vType) || (JavaUtils.isNotEmpty(vType) && (vType).equals(FileInfo.TYPE_UNKNOWN)))) {
            vType = getType();
        }
        return vType;
    }

    public boolean canHaveACloudPreview(){
        String vType = getvType();
        return vType!=null && (vType.equals(FileInfo.TYPE_LAYOUT) ||
                vType.equals(FileInfo.TYPE_PRESENTATION) ||
                vType.equals(FileInfo.TYPE_RASTER) ||
                vType.equals(FileInfo.TYPE_SPREADSHEET) ||
                vType.equals(FileInfo.TYPE_VECTOR) ||
                vType.equals(FileInfo.TYPE_VIDEO) ||
                vType.equals(FileInfo.TYPE_GDOC) ||
                vType.equals(FileInfo.TYPE_TEXT) ||
                vType.equals(FileInfo.TYPE_WEB));
    }

    public boolean canHaveAManualPreview(){
        return hasThumbnail() || isWebLink() || isSnippet() || isSpace();
    }

    public String getTypeToShowInAView(Context c){
        String fileType = "";
        if(JavaUtils.isNotEmpty(getvType()) && !getvType().equals(FileInfo.TYPE_LAYOUT)){
            int id = c.getResources().getIdentifier("FILE_TYPE_"+getvType().toUpperCase(), "string", c.getPackageName());
            if(id!=0){
                String type = c.getResources().getString(id).toUpperCase();
                if(JavaUtils.isNotEmpty(type)){
                    return type;
                }
            }
        }
        return getExtension();
    }


    public boolean isDownloadable(){
        return !isOnlyMetaFile() && isInCloud();
    }

    public boolean isUploadable(){
        return !isOnlyMetaFile() && isStored();
    }





    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		EQUAL
    //*****************************************************************************************************************************************************************************************************************************************************************************************************



    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LFile other = (LFile) obj;
        if (_id == null) {
            if (other._id != null)
                return false;
        } else if (!_id.equals(other._id))
            return false;
        return true;
    }





    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		JSON
    //*****************************************************************************************************************************************************************************************************************************************************************************************************



    private String toJson(){
        return new Gson().toJson(this);
    }

    public static LFile fromJson(String json){
        try{
            return new Gson().fromJson(json, LFile.class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LFile:\n"+e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LFile JSON:\n"+json);
            return null;
        }
    }

    public static LFile[] fromJsonToArray(String json){
        try{
            return new Gson().fromJson(json, LFile[].class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LFile[]:\n"+e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LFile[] JSON:\n"+json);
            return null;
        }
    }

    public Comment[] commentFromJsonToArray(String json){
        try{
            return new Gson().fromJson(json, Comment[].class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "Comment[]:\n"+e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "Comment[] JSON:\n"+json);
            return null;
        }
    }
}
