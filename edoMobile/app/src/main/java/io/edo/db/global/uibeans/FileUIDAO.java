package io.edo.db.global.uibeans;

import android.content.Context;

import java.io.File;
import java.util.ArrayList;

import io.edo.db.global.beans.FileDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactFile;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.WEntity;

public class FileUIDAO extends BaseUIDAO {

    FileDAO fileDAO;

    public FileUIDAO(Context context) {
        super(context);
        this.fileDAO = new FileDAO(context);
    }

    @Override
    public LFileDAO getLDAO() {
        return fileDAO.getLDAO();
    }

    public LActivity update(EdoContainer container, LSpace space, LFile file) {
        WEntity entity = fileDAO.update(container, space, file);
        return processActivity(entity);
    }

    public LActivity rename(EdoContainer container, LSpace space, LFile file) {
        WEntity entity = fileDAO.rename(container, space, file);
        return processActivity(entity);
    }

    public LActivity star(EdoContainer container, LSpace space, LFile file) {
        WEntity entity = fileDAO.star(container, space, file);
        return processActivity(entity);
    }


    public LActivity updateContentOnly(EdoContainer container, LSpace space, LFile file, boolean editing) {
        WEntity entity = fileDAO.updateContentOnly(container, space, file, editing);
        return processActivity(entity);
    }

    public LActivity move(EdoContainer container, LSpace toSpaceWithCurrentFolder, LFile file) {
        LFile moveFile = LFile.baseForWebUpdate(toSpaceWithCurrentFolder,file);

        moveFile.setParentId(file.getParentId());
        moveFile.setParentCloudId(file.getParentCloudId());

        if(file.isFolder()){
            moveFile.setPath(toSpaceWithCurrentFolder.getPath());
        }

        WEntity entity = fileDAO.move(container, toSpaceWithCurrentFolder, moveFile);
        return processActivity(entity);
    }

    public LActivity fileDownloaded(LFile file, LActivity a) {
        if(fileDAO.getLDAO().update(file)){
            if(a!=null) {
                a.setEvent(LActivity.EVENT_UPDATE);
                a.setMessage(null);
                a.setShortDescription(null);
                a.setLongDescription(null);
                return processActivity(a, false);
            }
        }
        return null;
    }

    public LActivity deleteFile(EdoContainer container, LSpace space, LFile file, boolean removeFromEdo, boolean removeFromLocal) {
        if(removeFromLocal){
            removeLocalFile(file);
        }

        if(!removeFromEdo && removeFromLocal){
            LActivity activity = new LActivity(LActivity.getContext(context,container), container, app.getUserContact(),
                    LActivity.EVENT_UPDATE, LActivity.RESOURCE_FILE, file, -1, -1, null);
            return processActivity(activity);
        }

        WEntity entity = fileDAO.deleteFile(container, space, file, true);
        return processActivity(entity);
    }

    public LFile download(String id) {
        return fileDAO.download(id);
    }


    public void removeLocalFile(final LFile file){
        try{
            File stFile = new File(file.getLocalUri());
            if(stFile.exists()){
                stFile.delete();

                file.setLocalUri(null);
                getLDAO().update(file);
            }

        }catch(NullPointerException e){}
    }


    public synchronized void saveToServerFilesWithError(){
            ArrayList<LFile> files = getLDAO().getFilesNotSavedToServer();
            if(files!=null && files.size()>0){
                for(LFile file: files){


                    LContact container = (LContact)file.getContainer(context);
                    if(container==null)
                        continue;
                    LSpace space = file.getSpace(context);
                    if(space==null)
                        continue;


                    LContactFile cf = new LContactFile(container,file);
                    cf.setSpaceWithCurrentFolder(space);
                    boolean done = new ContactFileUIDAO(context).add(cf)!=null;
                }
            }
    }
}