package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LActivities;
import io.edo.db.local.beans.support.UnreadInfo;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.eLog;


public class LActivityDAO extends LBaseDAO<LActivity,String,LActivities, EdoContainer> {
    static String TABLE_NAME = LocalDBHelper.TABLE_ACTIVITY;

    public static final String W_EXCHANGE_IS_NOTIFICATION = LocalDBHelper.KEY_EXCHANGE+"='"+ LActivity.EXCHANGE_NOTIFICATION+"'";
    public static final String W_EXCHANGE_IS_REALTIME = LocalDBHelper.KEY_EXCHANGE+"='"+ LActivity.EXCHANGE_REALTIME+"'";
    public static final String W_CONTEXT_IS_CONTACT = LocalDBHelper.KEY_CONTEXT+"='"+ LActivity.CONTEXT_CONTACT+"'";
    public static final String W_CONTEXT_IS_PERSONAL = LocalDBHelper.KEY_CONTEXT+"='"+ LActivity.CONTEXT_PERSONAL+"'";
    public static final String W_CONTEXT_IS_GROUP = LocalDBHelper.KEY_CONTEXT+"='"+ LActivity.CONTEXT_GROUP+"'";
    public static final String W_CONTEXT_IS_CONTACT_OR_GROUP = "("+W_CONTEXT_IS_CONTACT+ " OR "+W_CONTEXT_IS_GROUP+")";
    public static final String W_RESOURCE_TYPE_IS_CATEGORY = LocalDBHelper.KEY_RESOURCETYPE+"='"+ LActivity.RESOURCE_CATEGORY+"'";
    public static final String W_RESOURCE_TYPE_IS_CONTACT = LocalDBHelper.KEY_RESOURCETYPE+"='"+ LActivity.RESOURCE_CONTACT+"'";
    public static final String W_RESOURCE_TYPE_IS_GROUP = LocalDBHelper.KEY_RESOURCETYPE+"='"+ LActivity.RESOURCE_GROUP+"'";
    public static final String W_RESOURCE_TYPE_IS_FILE = LocalDBHelper.KEY_RESOURCETYPE+"='"+ LActivity.RESOURCE_FILE+"'";
    public static final String W_RESOURCE_TYPE_IS_MEMBER = LocalDBHelper.KEY_RESOURCETYPE+"='"+ LActivity.RESOURCE_MEMBER+"'";
    public static final String W_RESOURCE_TYPE_IS_STATUS = LocalDBHelper.KEY_RESOURCETYPE+"='"+ LActivity.RESOURCE_STATUS+"'";
    public static final String W_EVENT_IS_MESSAGE = LocalDBHelper.KEY_EVENT+"='"+ LActivity.EVENT_MESSAGE+"'";

    public static final String W_IS_FILE_RESOURCE_OF_CONTACT_OR_GROUP = W_EXCHANGE_IS_NOTIFICATION+" AND "+W_CONTEXT_IS_CONTACT_OR_GROUP+" AND "+W_RESOURCE_TYPE_IS_FILE;
    public static final String W_IS_CHAT_EVENT_OF_CONTACT_OR_GROUP = W_EXCHANGE_IS_REALTIME+" AND ("+W_CONTEXT_IS_CONTACT+ " OR "+W_CONTEXT_IS_GROUP+") AND "+W_EVENT_IS_MESSAGE;
    public static final String W_IS_GROUP_OR_MEMEBER_RESOURCE = W_EXCHANGE_IS_NOTIFICATION+" AND ("+W_RESOURCE_TYPE_IS_GROUP+" OR "+W_RESOURCE_TYPE_IS_MEMBER+")";

    public static final String W_HAS_TO_BE_UNREAD = LocalDBHelper.KEY_ISUNREAD+"==1";
    public static final String W_TIME_IS_MAX = LocalDBHelper.KEY_ID+" = (SELECT MAX("+LocalDBHelper.KEY_ID+") FROM "+TABLE_NAME+")";


    private String[] tableKeys = {
            LocalDBHelper.KEY_ID,
            LocalDBHelper.KEY_EXCHANGE,
            LocalDBHelper.KEY_CONTEXT,
            LocalDBHelper.KEY_CONTAINERID,
            LocalDBHelper.KEY_SENDERID,
            LocalDBHelper.KEY_RESOURCETYPE,
            LocalDBHelper.KEY_RESOURCEID,
            LocalDBHelper.KEY_MESSAGE,
            LocalDBHelper.KEY_EVENT,
            LocalDBHelper.KEY_SUBEVENT,
            LocalDBHelper.KEY_ORIGINALPARENTID,
            LocalDBHelper.KEY_PARENTID,
            LocalDBHelper.KEY_PARENTNAME,
            LocalDBHelper.KEY_ISFOLDER,
            LocalDBHelper.KEY_ORIGINALNAME,
            LocalDBHelper.KEY_RESOURCENAME,

            LocalDBHelper.KEY_CONTAINERNAME,
            LocalDBHelper.KEY_SENDERNAME,

            LocalDBHelper.KEY_SPACENAME,

            LocalDBHelper.KEY_VTYPE,

            LocalDBHelper.KEY_CONTENT,
            LocalDBHelper.KEY_SHORTDESCRIPTION,
            LocalDBHelper.KEY_LONGDESCRIPTION,

            LocalDBHelper.KEY_ISUNREAD,
            LocalDBHelper.KEY_STATUS};


    public LActivityDAO(Context context) {
        super(context);
    }

    @Override
    public ContentValues createContentValues(LActivity activity) {

        ContentValues values = new ContentValues();

        if(activity.getId()!=null)values.put( LocalDBHelper.KEY_ID, activity.getId() );
        if(activity.getExchange()!=null)values.put( LocalDBHelper.KEY_EXCHANGE, activity.getExchange() );
        if(activity.getContext()!=null)values.put( LocalDBHelper.KEY_CONTEXT, activity.getContext() );
        if(activity.getContainerId()!=null)values.put( LocalDBHelper.KEY_CONTAINERID, activity.getContainerId() );
        if(activity.getSenderId()!=null)values.put( LocalDBHelper.KEY_SENDERID, activity.getSenderId() );
        if(activity.getResourceType()!= LActivity.VALUE_ERROR)values.put( LocalDBHelper.KEY_RESOURCETYPE, activity.getResourceType() );
        if(activity.getResourceId()!=null)values.put( LocalDBHelper.KEY_RESOURCEID, activity.getResourceId() );
        if(activity.getMessage()!=null)values.put( LocalDBHelper.KEY_MESSAGE, activity.getMessage() );
        if(activity.getEvent()!= LActivity.VALUE_ERROR)values.put( LocalDBHelper.KEY_EVENT, activity.getEvent() );
        if(activity.getSubEvent()!= LActivity.VALUE_ERROR)values.put( LocalDBHelper.KEY_SUBEVENT, activity.getSubEvent() );
        if(activity.getOriginalParentId()!=null)values.put( LocalDBHelper.KEY_ORIGINALPARENTID, activity.getOriginalParentId() );
        if(activity.getParentId()!=null)values.put( LocalDBHelper.KEY_PARENTID, activity.getParentId() );
        if(activity.getParentName()!=null)values.put( LocalDBHelper.KEY_PARENTNAME, activity.getParentName() );
        values.put( LocalDBHelper.KEY_ISFOLDER, activity.isFolder()?1:0  );
        if(activity.getOriginalName()!=null)values.put( LocalDBHelper.KEY_ORIGINALNAME, activity.getOriginalName() );
        if(activity.getResourceName()!=null)values.put( LocalDBHelper.KEY_RESOURCENAME, activity.getResourceName() );

        if(activity.getContainerName()!=null)values.put( LocalDBHelper.KEY_CONTAINERNAME, activity.getContainerName() );
        if(activity.getSenderName()!=null)values.put( LocalDBHelper.KEY_SENDERNAME, activity.getSenderName() );

		if(activity.getSpaceName()!=null)values.put( LocalDBHelper.KEY_SPACENAME, activity.getSpaceName() );

        if(activity.getVtype()!=null)values.put( LocalDBHelper.KEY_VTYPE, activity.getVtype() );

        if(activity.getContent()!=null)values.put( LocalDBHelper.KEY_CONTENT, activity.getContent() );
        if(activity.getShortDescription()!=null)values.put( LocalDBHelper.KEY_SHORTDESCRIPTION, activity.getShortDescription() );
        if(activity.getLongDescription()!=null)values.put( LocalDBHelper.KEY_LONGDESCRIPTION, activity.getLongDescription() );

        if(activity.getStatus()!=null)values.put( LocalDBHelper.KEY_STATUS, activity.getStatus() );
        if(activity.getIsUnread()!=null)values.put( LocalDBHelper.KEY_ISUNREAD, activity.getIsUnread() );

        return values;
    }





    @Override
    public synchronized boolean insert(LActivity a) {

//        if(a.getContext().equals(LActivity.CONTEXT_CONTACT) || a.getContext().equals(LActivity.CONTEXT_GROUP)){
//            new LContactDAO(context, db).updateContactMDate(a.getContainerId(), System.currentTimeMillis());
//        }
        eLog.i("EdoActivityMan", "SAVED TO DB " + a.getId());
        ContentValues initialValues = createContentValues(a);
        return db.insert(TABLE_NAME, initialValues)>0;
    }

    @Override
    public synchronized boolean insertFromWebServer(LActivity a) {
        if(a.getOldActivityId()!=null) {
            boolean d = delete(new LActivity(a.getOldActivityId()));
            eLog.w("EdoActivityMan", "DELETE OLD ACTIVITY "+d);
        }

        if(exists(a.getId())){
            delete(a);
        }

        return insert(a);
    }

    @Override
    public synchronized boolean update(LActivity a) {
        ContentValues updatedValues = createContentValues(a);

        String whereClause = LocalDBHelper.KEY_ID+" = '"+ a.getId()+"'";
        boolean updated = db.update(TABLE_NAME, updatedValues, whereClause);

        return updated;
    }

    @Override
    public synchronized boolean delete(LActivity a) {
        String whereClause = LocalDBHelper.KEY_ID+" = '"+ a.getId()+"' OR "+LocalDBHelper.KEY_RESOURCEID+" = '"+ a.getResourceId()+"'";
        boolean deleted = db.delete(TABLE_NAME, whereClause);

        return deleted;
    }

    public synchronized boolean deleteAllActivitiesOfContainer(LContact contact) {
        String whereClause = LocalDBHelper.KEY_CONTAINERID+" = '"+ contact.getContainerId()+"' " +
                "OR "+LocalDBHelper.KEY_CONTAINERID+" = '"+ contact.getId()+"'";
        boolean deleted = db.delete(TABLE_NAME, whereClause);

        return deleted;
    }

    @Override
    public synchronized LActivity get(String id) {
        LActivity a = null;
        Cursor cursor = null;
        try {

            String whereClause = LocalDBHelper.KEY_ID+"=?";
            cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{id});
            if(cursor.moveToNext()){
                a = new LActivity(cursor);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }
        return a;
    }

    public synchronized LActivity getByResourceId(String resourceId) {
        LActivity a = null;
        Cursor cursor = null;
        try {

            String whereClause = LocalDBHelper.KEY_RESOURCEID+"=?";
            cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{resourceId});
            if(cursor.moveToNext()){
                a = new LActivity(cursor);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }
        return a;
    }

    @Override
    public synchronized LActivities getList() {
        LActivities list = new LActivities();
        Cursor cursor = null;

        LActivity newEvents = null;

        try {
            int unreadCount = 0;

            String orderBy = LocalDBHelper.KEY_ID+" DESC";
            cursor = db.select(TABLE_NAME, tableKeys, null, null, orderBy);

            while(cursor.moveToNext()){
                LActivity a = new LActivity(cursor);

                if(a.isUnread()){
                    unreadCount ++;
                }else if(unreadCount>0 && newEvents==null){
                    newEvents = LActivities.getNewEventsMsg(context, list.getactivities().size());
                    list.add(newEvents);
                }

                list.addActivityToDescList(context, a);
            }

//            if(list.getactivities()==null || list.getactivities().size()<=0){
//                LActivity blancsheet = LActivities.getBlancSheetMsg(context, container);
//                list.add(blancsheet);
//            }

        } finally {
            if(cursor != null)
                cursor.close();
        }
        return list;
    }

    @Override
    public LActivities getListBy(EdoContainer container) {
        if(container.getContainerId()==null)
            return null;

        LActivities list = new LActivities();
        Cursor cursor = null;

        LActivity newEvents = null;

        try {
            int unreadCount = 0;

            String whereClause = "("+LocalDBHelper.KEY_CONTAINERID+"=? OR "+LocalDBHelper.KEY_CONTAINERID+"=? ) AND " +
                    LocalDBHelper.KEY_MESSAGE+" IS NOT NULL";
            String orderBy = LocalDBHelper.KEY_ID+" DESC";
            cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{container.getId(),container.getContainerId()}, orderBy);

            while(cursor.moveToNext()){
                LActivity a = new LActivity(cursor);

                if(a.isUnread()){
                    unreadCount ++;

                    if(unreadCount==1){
                        list.setLastNotificationUnread(a.getId());
                    }

                }else if(unreadCount>0 && newEvents==null){
                    newEvents = LActivities.getNewEventsMsg(context, list.getactivities().size());
                    list.add(newEvents);
                }

                list.addActivityToDescList(context, a);
            }

            if(unreadCount>0){
                list.setUnreadNumber(unreadCount);
            }

            if(list.getactivities()==null || list.getactivities().size()<=0){
                LActivity blancsheet = LActivities.getBlancSheetMsg(context, container);
                list.add(blancsheet);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }

        return list;
    }



    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		DERIVED
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    //READ/UNREAD
    public synchronized boolean setAllActivitiesRedBy(String contactId, String edoUserId) {
        eLog.w("EdoActivityMan", "SET ALL ACTIVITY READ BY: "+contactId+" "+edoUserId);

        if(edoUserId==null){
            edoUserId = contactId;
        }

        ContentValues values = new ContentValues();
        values.put(LocalDBHelper.KEY_ISUNREAD, 0);

        String whereClause = LocalDBHelper.KEY_CONTAINERID + " == '" + edoUserId + "' OR "+LocalDBHelper.KEY_CONTAINERID + " == '" + contactId + "'";
        boolean updated = db.update(TABLE_NAME, values, whereClause);

        return updated;
    }

    public synchronized int getUnreadNumber() {
        Cursor cursor = null;


        int unreadCount = 0;

        try {

            String whereClause = LocalDBHelper.KEY_ISUNREAD+" == 1";
            cursor = db.select(TABLE_NAME, tableKeys, whereClause, null, null);

            unreadCount = cursor.getCount();

//            while(cursor.moveToNext()) {
//                LActivity a = new LActivity(cursor);
//            }


        } finally {
            if(cursor != null)
                cursor.close();
        }

        return unreadCount;
    }

    public synchronized boolean setAllActivitiesRedIdIfOlderThan(String lastActivitySeenId) {

        ContentValues values = new ContentValues();
        values.put(LocalDBHelper.KEY_ISUNREAD, 0);

        String whereClause = LocalDBHelper.KEY_ID + " <= '" + lastActivitySeenId + "'";

        boolean updated = db.update(TABLE_NAME, values, whereClause);
        return updated;
    }

    public synchronized boolean setAllActivitiesRedByContainerIdIfOlderThan(String containerId, String lastActivitySeenId) {

        ContentValues values = new ContentValues();
        values.put(LocalDBHelper.KEY_ISUNREAD, 0);

        String whereClause = LocalDBHelper.KEY_CONTAINERID + " = '" + containerId + "' AND "+
                LocalDBHelper.KEY_ID + " <= '" + lastActivitySeenId + "'";

        boolean updated = db.update(TABLE_NAME, values, whereClause);
        return updated;
    }

    //FOR NOTIFICATION

    public synchronized LActivity getLastContactOrGroupActivity(){
        LActivity a = null;
        Cursor cursor = null;
        try {
            String whereClause = W_CONTEXT_IS_CONTACT_OR_GROUP+
                    " AND "+W_TIME_IS_MAX +
                    " GROUP BY "+LocalDBHelper.KEY_ID;

            String orderBy = LocalDBHelper.KEY_ID+" DESC";

            cursor = db.select(TABLE_NAME, tableKeys,whereClause, null, orderBy);
            if(cursor.moveToNext()){
                a = new LActivity(cursor);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }
        return a;
    }

    public synchronized UnreadInfo getUnreadInfos(){
        Cursor cursor = null;
        UnreadInfo unreadInfo = null;
        try {

            String userId = ThisApp.getInstance(context).getEdoUserId();


            String query = "SELECT " +

                    "(SELECT count(*) FROM "+TABLE_NAME+" "+
                    "WHERE "+ W_IS_FILE_RESOURCE_OF_CONTACT_OR_GROUP +
                    "AND "+W_HAS_TO_BE_UNREAD+" "+
                    "AND "+ LocalDBHelper.KEY_EVENT+" IS NOT '"+LActivity.EVENT_COMMENT_INSERT+"' " +
                    "AND "+LocalDBHelper.KEY_SENDERID+" IS NOT '"+userId+"') " +
                    "AS "+UnreadInfo.KEY_UNREADFILESCOUNT+", "+

                    "(SELECT count(*) FROM "+TABLE_NAME+" "+
                    "WHERE "+ W_IS_CHAT_EVENT_OF_CONTACT_OR_GROUP +
                    "AND "+W_HAS_TO_BE_UNREAD+" "+
                    "AND "+LocalDBHelper.KEY_SENDERID+" IS NOT '"+userId+"') " +
                    "AS "+UnreadInfo.KEY_UNREADCHATMESSAGESCOUNT+", "+

                    "(SELECT count(*) FROM "+TABLE_NAME+" "+
                    "WHERE "+ W_IS_FILE_RESOURCE_OF_CONTACT_OR_GROUP +
                    "AND "+W_HAS_TO_BE_UNREAD+" "+
                    "AND "+ LocalDBHelper.KEY_EVENT+"='"+LActivity.EVENT_COMMENT_INSERT+"' " +
                    "AND "+LocalDBHelper.KEY_SENDERID+" IS NOT '"+userId+"') " +
                    "AS "+UnreadInfo.KEY_UNREADNOTESCOUNT+", "+

                    "(SELECT count(*) FROM "+TABLE_NAME+" "+
                    "WHERE "+ W_IS_GROUP_OR_MEMEBER_RESOURCE +
                    "AND "+W_HAS_TO_BE_UNREAD+" "+
                    "AND "+LocalDBHelper.KEY_SENDERID+" IS NOT '"+userId+"') " +
                    "AS "+UnreadInfo.KEY_UNREADGROUPCOUNT+" "

                    ;


            cursor = db.executeQuery(query, null);

            cursor.moveToFirst();
            unreadInfo = new UnreadInfo(cursor);

        } finally {
            if(cursor != null)
                cursor.close();
        }

        return unreadInfo;
    }


    public synchronized ArrayList<String> getContainerNamesOfUnreadActivities(){
        Cursor cursor = null;
        ArrayList<String> strings = new ArrayList<String>();

        try {

            String userId = ThisApp.getInstance(context).getEdoUserId();


            String whereClause = "( "+W_IS_FILE_RESOURCE_OF_CONTACT_OR_GROUP +
                    " OR "+ W_IS_CHAT_EVENT_OF_CONTACT_OR_GROUP +
                    " OR "+ W_IS_GROUP_OR_MEMEBER_RESOURCE +" )" +
                    " AND "+W_HAS_TO_BE_UNREAD+
                    " AND "+LocalDBHelper.KEY_LONGDESCRIPTION+" IS NOT NULL "+
                    " AND "+LocalDBHelper.KEY_SENDERID+" IS NOT '"+userId+"'";

            String groupBy = LocalDBHelper.KEY_CONTAINERID;
            String orderBy = LocalDBHelper.KEY_ID+" DESC";

            cursor= db.getDataBase().rawQuery("SELECT DISTINCT "+LocalDBHelper.KEY_CONTAINERID+","+ LocalDBHelper.KEY_CONTAINERNAME+
                    " FROM "+TABLE_NAME+
                    " WHERE "+whereClause+
                    " GROUP BY "+groupBy+
                    " ORDER BY "+orderBy, null);

            while(cursor.moveToNext()){
                String name = cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTAINERNAME));
                if(JavaUtils.isNotEmpty(name))
                    strings.add(name);
            }



        } finally {
            if(cursor != null)
                cursor.close();
        }

        return strings;

    }


    public synchronized String getContactContextUnreadBigTextHTML(int limit){
        Cursor cursor = null;

        String bigText = null;

        try {

            String whereClause = "( "+W_IS_FILE_RESOURCE_OF_CONTACT_OR_GROUP +
                    " OR "+ W_IS_CHAT_EVENT_OF_CONTACT_OR_GROUP  +
                    " OR "+ W_IS_GROUP_OR_MEMEBER_RESOURCE +" )"+
                    " AND "+W_HAS_TO_BE_UNREAD+
                    " AND "+LocalDBHelper.KEY_LONGDESCRIPTION+" IS NOT NULL ";

            String orderBy = LocalDBHelper.KEY_ID+" DESC";

            String query = "SELECT * FROM "+TABLE_NAME+" WHERE "+whereClause+" ORDER BY "+orderBy;

            if(limit>=0){
                query += " LIMIT "+limit;
            }

            cursor = db.getDataBase().rawQuery(query, null);
            bigText = "";
            while(cursor.moveToNext()){
                String message = cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_LONGDESCRIPTION));
                if(message!=null)
                    bigText += message+"<BR>";
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }

        return bigText;
    }

//    public synchronized ArrayList<String> getResourceGroupOrGroupMemberMessages(long timeIntervalInMillis){
//        Cursor cursor = null;
//
//        ArrayList<String> strings = new ArrayList<String>();
//
//        try {
//
//            String whereClause = "("+W_RESOURCE_TYPE_IS_GROUP+" OR " +W_RESOURCE_TYPE_IS_MEMBER+")";
//
//            if(timeIntervalInMillis>=0){
//                long after = System.currentTimeMillis() - timeIntervalInMillis;
//                whereClause += " AND "+LocalDBHelper.KEY_TIMESTAMP+">"+after;
//            }
//
//            String orderBy = LocalDBHelper.KEY_TIMESTAMP+" DESC";
//
//            String query = "SELECT * FROM "+TABLE_NAME+" WHERE "+whereClause+" ORDER BY "+orderBy;
//
//
//
//            cursor = db.getDataBase().rawQuery(query, null);
//            while(cursor.moveToNext()){
//                String message = cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_MESSAGE));
//                if(message!=null)
//                    strings.add(message);
//            }
//
//        } finally {
//            if(cursor != null)
//                cursor.close();
//        }
//
//        return strings;
//    }


    public synchronized ArrayList<LActivity> getAllMessagesWithError() {
        Cursor cursor = null;

        ArrayList <LActivity> activities = new ArrayList<LActivity>();

        try {

            String whereClause = LocalDBHelper.KEY_STATUS+" == "+LActivity.STATUS_ERROR + " AND "+
                    LocalDBHelper.KEY_EVENT+" == "+LActivity.EVENT_MESSAGE;
            cursor = db.select(TABLE_NAME, tableKeys, whereClause, null, null);


            while(cursor.moveToNext()) {
                LActivity a = new LActivity(cursor);
                activities.add(a);
            }


        } finally {
            if(cursor != null)
                cursor.close();
        }

        return activities;
    }
}
