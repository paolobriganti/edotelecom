package io.edo.db.local.beans;

import android.database.Cursor;

import java.io.Serializable;

import io.edo.db.local.LocalDBHelper;

public class LGroupMember  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String contactId;
	private String groupId;
	
	
	
	
	public LGroupMember(LContact group, LContact member) {
		super();
		this.contactId = member.getId();
		this.groupId = group.getId();
	}

	public LGroupMember(Cursor cursor) {
		setGroupId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_GROUPID)));
		setContactId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTACTID)));
	}

	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

//	public String toJson(){
//		return new Gson().toJson(this);
//	}
//	
//	public static LGroupMember fromJson(String json){
//		try{
//			return new Gson().fromJson(json, LGroupMember.class);
//		}catch (com.google.gson.JsonSyntaxException e){
//			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LGroupMember:\n"+e.getMessage());
//			return null;
//		}
//	}
}
