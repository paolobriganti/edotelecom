package io.edo.db.local.beans;

import android.content.Context;
import android.database.Cursor;

import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.extensions.Email;
import com.google.gson.Gson;
import com.paolobriganti.android.Contact;
import com.paolobriganti.utils.JavaUtils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.support.CloudIdServiceId;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.EdoResource;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.ContactExtendedInfos;
import io.edo.db.web.beans.EmailAddress;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.Constants;
import io.edo.utilities.ObjectId;
import io.edo.utilities.TrackConstants;
import io.edo.utilities.eLog;

public class LContact implements EdoContainer,EdoResource, Serializable{
    private static final long serialVersionUID = 1L;

    public static final String STATUS_INACTIVE = "inactive";
    public static final String STATUS_ACTIVE = "active";
    public static final String STATUS_HIDDEN = "hidden";
    public static final String STATUS_BLOCKED = "blocked";
    public static final String STATUS_SUGGESTED = "suggested";
    public static final String STATUS_CREATION_IN_PROGRESS = "creationInProgress";

    public static final String NO_THUMB = "nothumb";

    //Web Contact Values
    private String _id=null;
    private String cloudId=null;
    private String userId=null;
    private String name=null;
    private String surname=null;
    private Integer isStarred = null;
    private String status = null;
    private Integer rank = null;
    private Long mDate = null;
    private EmailAddress[] emailAddresses = null;
    private String filesListId = null;
    private ContactExtendedInfos extendedInfos = null;

    //Web Group Values
    private LContact[] contacts;//lista dei contatti (i contatti sono completi) membri del gruppo (solo utenti edo)
    private LContact[] pending; //lista dei contatti (i contatti sono completi) che non sono utenti edo e sono stati invitati al gruppo, gli _id di questi contatti sono validi solo per l’owner
    private String ownerId; 	//edo user id dell’owner/amministratore del gruppo
    private String thumbnail;	//url thumbnail (string)

    //Local Contact Values
    private Integer isUnread = null;
    private String principalEmailAddress = null;
    private Integer unreadPushCount = null;
    private Integer memberColor;



    private String lastNotificationMessage;

    public LContact() {
        super();
        ObjectId id = new ObjectId();
        this._id = id.toString();
    }

    public LContact(String id) {
        this._id = id;
    }

    public LContact(Context context, Cursor cursor) {
        super();

        setId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_ID)));
        setCloudIdServiceIdJsonList(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CLOUDID)));
        setEdoUserId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_EDOUSERID)));
        setName(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_NAME)));
        setSurname(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_SURNAME)));
        setIsStarred(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ISSTARRED))==1);
        setStatus(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_STATUS)));
        setRank(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_RANK)));
        setmDate(cursor.getLong(cursor.getColumnIndex(LocalDBHelper.KEY_MDATE)));
        setExtendedInfos(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_EXTENDEDINFO)));
        setIsUnread((cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ISUNREAD))));
        setThumbnail(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_THUMBNAIL)));
        setGroupOwnerId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_OWNERID)));
        setPrincipalEmailAddress(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_PRINCIPALEMAILADDRESS)));

        if(isGroup()){
            setGroupMembers(context, new LGroupMemberDAO(context).getContactsOfGroup(getId()));
        }
    }

    public LContact(String name, String surname, String principalEmailAddress) {
        super();
        ObjectId id = new ObjectId();
        this._id = id.toString();
        this.name = name;
        this.surname = surname;
        this.mDate = System.currentTimeMillis();
        this.setPrincipalEmailAddress(principalEmailAddress);
        this.isUnread = 0;
    }



    public LContact getWebContact() {
        LContact wcontact = new LContact();
        wcontact._id = getId();
        wcontact.cloudId = getCloudIdServiceIdJsonList();
        wcontact.userId = getEdoUserId();
        wcontact.name = getName();
        wcontact.surname = getSurname();
        wcontact.isStarred = getIsStarred();
        wcontact.rank = getRank();
        wcontact.mDate = getmDate();
        wcontact.status = getStatus();
        wcontact.emailAddresses = getEmailAddresses();
        wcontact.extendedInfos = getExtendedInfos();
        return wcontact;
    }

    public String getWebContactJson() {
        return getWebContact().toJson();
    }


    public static LContact[] getWebContacts(LContact[] contacts){
        LContact[] wcontacts = new LContact[0];
        if(contacts!=null && contacts.length>0){
            wcontacts = new LContact[contacts.length];
            for(int i=0; i<wcontacts.length; i++){
                wcontacts[i] = contacts[i].getWebContact();
            }
        }

        return wcontacts;
    }

    public static String getWebContactJson(LContact[] contacts) {
        return new Gson().toJson(getWebContacts(contacts));
    }

    public LContact(String serviceId, ContactEntry contact, Email email) {
        super();
        ObjectId id = new ObjectId();
        this._id = id.toString();
        if(contact.getName()!=null){
            if(contact.getName().getGivenName()!=null)
                this.name = contact.getName().getGivenName().getValue();
            else if(contact.getName().getNamePrefix()!=null)
                this.name = contact.getName().getNamePrefix().getValue();

            if(contact.getName().getAdditionalName()!=null)
                this.name = this.name +" "+ contact.getName().getAdditionalName().getValue();

            if(contact.getName().getFamilyName()!=null)
                this.surname = contact.getName().getFamilyName().getValue();
            else if(contact.getName().getNameSuffix()!=null)
                this.surname = contact.getName().getNameSuffix().getValue();


            if(JavaUtils.isEmpty(name) && contact.getName().getFullName()!=null){
                this.name = contact.getName().getFullName().getValue();
                this.surname = null;
            }
        }
        this.mDate = System.currentTimeMillis();

        ContactExtendedInfos cei = new ContactExtendedInfos();
        cei.serviceId = serviceId;
        cei.cloudId = contact.getId();
        this.extendedInfos = cei;

        this.setPrincipalEmailAddress(email.getAddress());
        this.isUnread = 0;
    }

    public LContact(Contact contact, String email) {
        super();
        ObjectId id = new ObjectId();
        this._id = id.toString();
        this.name = contact.getName();
        this.mDate = System.currentTimeMillis();
        this.setPrincipalEmailAddress(email);
        this.isUnread = 0;
    }




    @Override
    public String getId() {
        return _id;
    }

    @Override
    public String getEdoUserId() {
        return userId;
    }

    @Override
    public String getContainerId() {
        if(JavaUtils.isNotEmpty(userId))
            return userId;
        return _id;
    }

    @Override
    public int getSectionNumber() {
        return Constants.SECTION_NUMBER_CONTACTS;
    }

    public void setId(String id) {
        this._id = id;
    }


//	public String getUserId() {
//		return userId;
//	}

    public void setEdoUserId(String userId) {
        this.userId = userId;
    }


    public String getCloudIdServiceIdJsonList() {
        return cloudId;
    }

    public String getCloudId(String serviceId) {
        if(cloudId!=null){
            return CloudIdServiceId.getCloudId(cloudId, serviceId);
        }
        return null;
    }

    public ArrayList<CloudIdServiceId> getCloudIdServiceId() {
        if(cloudId!=null){
            return CloudIdServiceId.fromJsonToList(cloudId);
        }
        return null;
    }



    public String getServiceIdOfCloudFolder() {
        if(cloudId!=null){
            CloudIdServiceId[] list = CloudIdServiceId.fromJson(cloudId);
            if(list!=null && list.length>0)
                return list[0].serviceId;
        }
        return null;
    }

    public void addServiceIdCloudId(String serviceId, String cloudId) {
        this.cloudId = CloudIdServiceId.addCloudIdServiceIdAndReturnJson(cloudId, new CloudIdServiceId(cloudId, serviceId));
    }

    public void setServiceIdCloudId(String serviceId, String cloudId) {
        this.cloudId = CloudIdServiceId.setCloudIdServiceIdAndReturnJson(cloudId, new CloudIdServiceId(cloudId, serviceId));
    }

    public void removeServiceIdCloudId(String serviceId, String cloudId) {
        this.cloudId = CloudIdServiceId.removeCloudIdServiceIdAndReturnJson(this.cloudId, cloudId);
    }

    public void setCloudIdServiceIdJsonList(String cloudId) {
        this.cloudId = cloudId;
    }

    @Override
    public String getName() {
        if(JavaUtils.isNotEmpty(name))
            return name;
        return getPrincipalEmailAddress();
    }

    public String getNameSurname(){
        return getNameComplete();
    }

    @Override
    public String getNameComplete() {
        if(isGroup())
            return name;
        if(JavaUtils.isNotEmpty(name) && JavaUtils.isNotEmpty(surname))
            return name+" "+surname;
        else if(JavaUtils.isNotEmpty(getName()))
            return getName();
        else if(JavaUtils.isNotEmpty(surname))
            return surname;
        return "";
    }

    @Override
    public String getTrackSectionName() {
        return isContact() ? TrackConstants.SCREEN_NAME_CONTACT
                :(isGroup() ? TrackConstants.SCREEN_NAME_GROUP : null);
    }

    @Override
    public String getPhysicalFolderPath(Context c) {
        if(Constants.getDiskCacheDir(c,Constants.SECTION_NAME_CONTACTS)==null)
            return null;

        String folderName = JavaUtils.isNotEmpty(getNameSurname())? getNameSurname():"Contact";
        return Constants.getDiskCacheDir(c,Constants.SECTION_NAME_CONTACTS).getPath()+File.separator+
                folderName.replace(" ", "_");
    }

    @Override
    public String regenerateCloudTab(Context context, LSpace space) {
        return null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }



    public Boolean isStarred() {
        return isStarred!=null && isStarred == 1;
    }

    public Integer getIsStarred(){
        return isStarred;
    }

    public void setIsStarred(Boolean isStarred) {
        this.isStarred = isStarred ? 1:0;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Long getmDate() {
        return mDate;
    }

    public void setmDate(Long mDate) {
        this.mDate = mDate;
    }

    public EmailAddress[] getEmailAddresses() {
        if(principalEmailAddress!=null){
            if(emailAddresses!=null && emailAddresses.length>0){
                this.emailAddresses[0] = new EmailAddress(principalEmailAddress);
            }else{
                this.emailAddresses = new EmailAddress[]{new EmailAddress(principalEmailAddress)};
            }
        }
        return emailAddresses;
    }

    public void setEmailAddresses(EmailAddress[] emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public String getFilesListId() {
        return filesListId;
    }

    public void setFilesListId(String filesListId) {
        this.filesListId = filesListId;
    }

    public ContactExtendedInfos getExtendedInfos() {
        return extendedInfos;
    }

    public void setExtendedInfos(String extendedInfoJson) {
        this.extendedInfos = ContactExtendedInfos.fromJson(extendedInfoJson);
    }
    public String getExtendedInfosJson() {
        if(extendedInfos!=null)
            return extendedInfos.toJson();
        return null;
    }

    public LContact[] getContacts() {
        return contacts;
    }

    public void setContacts(LContact[] contacts) {
        this.contacts = contacts;
    }

    public LContact[] getPending() {
        return pending;
    }

    public void setPending(LContact[] pending) {
        this.pending = pending;
    }

    public String getGroupOwnerId() {
        return ownerId;
    }

    public void setGroupOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean hasNoThumbnail(){
        return thumbnail!=null && thumbnail.equals(NO_THUMB);
    }

    public Integer getIsUnread() {
        return isUnread;
    }

    public void setIsUnread(Integer isUnread) {
        this.isUnread = isUnread;
    }

    public String getPrincipalEmailAddress() {
        if(principalEmailAddress==null && emailAddresses!=null && emailAddresses.length>0){
            this.principalEmailAddress = emailAddresses[0].getEmail();
        }
        return principalEmailAddress;
    }

    public void setPrincipalEmailAddress(String principalEmailAddress) {
        this.principalEmailAddress = principalEmailAddress;
    }

    public Integer getUnreadPushCount() {
        return unreadPushCount;
    }

    public void setUnreadPushCount(Integer unreadPushCount) {
        this.unreadPushCount = unreadPushCount;
    }

    public String getLastNotificationMessage() {
        return lastNotificationMessage;
    }

    public void setLastNotificationMessage(String lastNotificationMessage) {
        this.lastNotificationMessage = lastNotificationMessage;
    }




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		DERIVED
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    //ID

    @Override
    public boolean isPersonal(String userId) {
        return hasSameId(userId);
    }

    @Override
    public boolean isPersonal(Context c) {
        String userId = ThisApp.getInstance(c).getEdoUserId();
        return isPersonal(userId);
    }

    @Override
    public boolean isContact() {
        return !isGroup();
    }

    @Override
    public boolean isGroup() {
        return JavaUtils.isNotEmpty(ownerId);
    }

    @Override
    public boolean isContactOrGroup() {
        return isContact()||isGroup();
    }

    public boolean isEdoUser(){
        return JavaUtils.isNotEmpty(userId);
    }

    public boolean hasSameId(String id){
        if (id == null)
            return false;
        if(this._id!=null && this._id.equals(id))
            return true;
        if (this.userId!=null && this.userId.equals(id))
            return true;
        return false;
    }


    //STATUS

    public boolean isActive() {
        if(JavaUtils.isNotEmpty(status))
            return status.equals(LContact.STATUS_ACTIVE);
        return false;
    }

    public boolean isSuggested() {
        if(JavaUtils.isNotEmpty(status))
            return status.equals(LContact.STATUS_SUGGESTED);
        return false;
    }

    public boolean isHidden() {
        if(JavaUtils.isNotEmpty(status))
            return status.equals(LContact.STATUS_HIDDEN);
        return false;
    }

    public boolean isNewForServer() {
        return !isGroup() && !isActive() && !isHidden();
    }


    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		GROUP
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public final static int MIN_MEMBERS = 2;
    public final static int MAX_MEMBERS = 200;

    public LContact(Context c, String name, String ownerId, ArrayList<LContact> members){
        ObjectId id = new ObjectId();
        this._id = id.toString();
        this.name = name;
        this.ownerId = ownerId;
        this.mDate = System.currentTimeMillis();
        setGroupMembers(c, members);
    }

    public String getCreateGroupRequestBody(){
        LContact group = new LContact();
        group._id = getId();
        group.name = getName();
        group.thumbnail = getThumbnail();

        group.contacts = getWebContacts(this.contacts);

        group.filesListId = null;
        group.pending = null;
        group.ownerId = null;
        group.isUnread = null;
        group.principalEmailAddress = null;
        group.unreadPushCount = null;
        group.memberColor = null;

        return group.toJson();
    }

    public String getUpdateGroupRequestBody(boolean hasRename){
        LContact group = new LContact();
        group.name = hasRename?getName():null;
        group.thumbnail = getThumbnail();
        group.cloudId = this.cloudId;

        group.filesListId = null;
        group.contacts = null;
        group.pending = null;
        group.ownerId = null;
        group.isUnread = null;
        group.principalEmailAddress = null;
        group.unreadPushCount = null;
        group.memberColor = null;

        return group.toJson();
    }

    public void setGroupMembers(Context c, LContact[] members){
        if(members!=null)
            setGroupMembers(c, new ArrayList<LContact>(Arrays.asList(members)));
    }

    public void setGroupMembers(Context c, ArrayList<LContact> members){
        if(members!=null && members.size()>=MIN_MEMBERS-1 && members.size()<=MAX_MEMBERS-1){
            LContact[] wcontacts = new LContact[members.size()];
            for(int i=0; i<wcontacts.length; i++){
                wcontacts[i] = members.get(i);

//                int mod = i % 9;
//                int colorId = c.getResources().getIdentifier("edo_group_chat_"+mod, "color", c.getPackageName());
//                if(colorId!=0)
//                    wcontacts[i].setMemberColor(c.getResources().getColor(colorId));

            }
            this.contacts = wcontacts;
        }
    }

    public ArrayList<LContact> getGroupMembers(Context c){
        ArrayList<LContact> members = new ArrayList<LContact>();
        if(contacts!=null && contacts.length>0){
            for(int i=0; i<contacts.length; i++){
                LContact contact = contacts[i];

                int mod = i % 9;
                int colorId = c.getResources().getIdentifier("edo_group_chat_"+mod, "color", c.getPackageName());
                if(colorId!=0)
                    contacts[i].setMemberColor(c.getResources().getColor(colorId));

                members.add(contact);
            }
        }

//        if(pending!=null && pending.length>0){
//            for(LContact contact:pending){
//                if(!currentEdoUserId.equals(ownerId)) {
//                    contact.setId(new ObjectId().toString()); //BECAUSE THE ID OF PENDING CONTACT COMES FROM OWNER CONTACTS
//                }
//                members.add(contact);
//            }
//        }

        return members;
    }

    public LContact getGroupMember(String memberId){
        if(contacts!=null && contacts.length>0){
            for(LContact contact:contacts){
                if(contact.hasSameId(memberId))
                    return contact;
            }
        }

        if(pending!=null && pending.length>0){
            for(LContact contact:pending){
                if(contact.hasSameId(memberId))
                    return contact;
            }
        }

        return null;
    }

    public String[] getGroupMembersEmails(Context c){
        if(this.isGroup()){
            return new LGroupMemberDAO(c).getEmailsOfEdoMembers(this.getId());
        }
        return null;
    }

    public int getGroupMembersCount(){
        int count = contacts!=null? contacts.length : 0;
        count += pending!=null? pending.length : 0;
        return count;
    }


    public int getMemberColor(Context c) {
        if(memberColor!=null)
            return memberColor;
        return c.getResources().getColor(R.color.edo_contact_chat);
    }

    public void setMemberColor(Integer memberColor) {
        this.memberColor = memberColor;
    }

    public boolean isUserOwnerOfTheGroup(String edoUserId){
        return JavaUtils.isNotEmpty(ownerId) && ownerId.equals(edoUserId);
    }








    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		EQUAL
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LContact other = (LContact) obj;
        boolean isIdEqual = _id != null && other._id != null && _id.equals(other._id);
        boolean isUserIdEqual = userId != null && other.userId != null && userId.equals(other.userId);
        if (!isIdEqual && !isUserIdEqual)
            return false;
        return true;
    }




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		JSON
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    private String toJson(){
        return new Gson().toJson(this);
    }

    public static LContact fromJson(String json){
        try{
            return new Gson().fromJson(json, LContact.class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LContact:\n"+e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LContact JSON:\n"+json);
            return null;
        }
    }
    public static LContact[] fromJsonToArray(String json){
        try{
            return new Gson().fromJson(json, LContact[].class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LContact[]:\n"+e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LContact[] JSON:\n"+json);
            return null;
        }

    }
}

