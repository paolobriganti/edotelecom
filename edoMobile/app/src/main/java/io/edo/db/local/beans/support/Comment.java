package io.edo.db.local.beans.support;

import com.google.gson.Gson;

import java.io.Serializable;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class Comment implements Serializable{
	private static final long serialVersionUID = 1L;

	public static final String TYPE_ACTIONABLE = "actionable";

	public String userId = null;
	public Long cDate = null;
	public String type = null;
	public String status = null;
	public String message = null;
	public String buttonLabel= null;

	public Comment(String userId, Long cDate, String type, String status, String message) {
		super();
		this.userId = userId;
		this.cDate = cDate;
		this.type = type;
		this.status = status;
		this.message = message;
	}

	public Comment(String userId, String message) {
		super();
		this.userId = userId;
		this.message = message;
		this.cDate = System.currentTimeMillis();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getcDate() {
		return cDate;
	}

	public void setcDate(Long cDate) {
		this.cDate = cDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String toJson(){
		return new Gson().toJson(this);
	}

	public String getButtonLabel() {
		return buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		this.buttonLabel = buttonLabel;
	}

	public static Comment fromJson(String json){
		
		try{
			return new Gson().fromJson(json, Comment.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "Comment:\n"+e.getMessage());
			return null;
		}
	}

	public static Comment[] fromJsonList(String json){
		
		try{
			return new Gson().fromJson(json, Comment[].class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "Comments:\n"+e.getMessage());
			return null;
		}
	}
}
