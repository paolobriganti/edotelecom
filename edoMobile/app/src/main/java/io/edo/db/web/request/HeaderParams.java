package io.edo.db.web.request;

public class HeaderParams{
	String key;
	String value;
	
	public HeaderParams(String key, String value){
		this.key = key;
		this.value = value;
	}
	@Override
	public String toString() {
		return key+"="+value;
	}
}
