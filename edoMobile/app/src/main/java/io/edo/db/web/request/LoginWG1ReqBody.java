package io.edo.db.web.request;

import com.google.gson.Gson;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class LoginWG1ReqBody {
	public String _id;
	public String type;
	public String model;
	public String os;
	public String os_ver;
	public String push_id;

	public String toJson(){
		return new Gson().toJson(this);
	}

	public static LoginWG1ReqBody fromJson(String json){
		try{
			return new Gson().fromJson(json, LoginWG1ReqBody.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LoginWG1ReqBody:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LoginWG1ReqBody JSON:\n"+json);
			return null;
		}
	}
}
