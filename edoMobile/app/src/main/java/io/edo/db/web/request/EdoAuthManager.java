package io.edo.db.web.request;

import android.content.Context;

import com.google.gson.Gson;
import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LUser;
import io.edo.db.local.beans.LUserDAO;
import io.edo.db.web.beans.Device;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoNotification;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class EdoAuthManager {

	public static final long REFRESH_TOKEN_EXPIRATION_TIME = 1296000000; //1296000000 = 15 days

	//*********************************************************************
	//*		LOGIN WITH GOOGLE
	//*********************************************************************


	public static LoginWG1Resp loginWithGoogle1(final Context c, final String gcmRegistrationId){
		LoginWG1Resp loginWG1Resp = LoginWG1Resp.load(c);
		if(loginWG1Resp==null){

			String url = EdoRequest.HOST_PREFIX + EdoRequest.PATH_LOGIN_GOOGLE;
			Device device = new Device(c, gcmRegistrationId);

			LoginWG1ReqBody reqBody = new LoginWG1ReqBody();
			reqBody._id = device.get_id();
			reqBody.type = device.getType();
			reqBody.model = device.getModel();
			reqBody.os = device.getOs();
			reqBody.os_ver = device.getOs_ver();
			reqBody.push_id = device.getPush_id();

			EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, url, reqBody.toJson());

			String response = request.send(c);
			
			if(!EdoRequest.hasEdoErrors(response)){
				loginWG1Resp = LoginWG1Resp.fromJson(response);
				if(loginWG1Resp!=null){
					loginWG1Resp.save(c);
					return loginWG1Resp;
				}
			}

		}else{
			return loginWG1Resp;
		}
		return null;
	}

	public static String getLoginWithGoogleUrl(Context context, String token){
		String url = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE_GOOGLE + EdoRequest.QUERY_SEPARATOR + 
				EdoRequest.VALUE_TOKEN + "=" + token + "&" + EdoRequest.VALUE_CONFIGURE + "=1";
		return url;
	}




	public static boolean loginWithGoogle2(final Context c, String code, String token){
		if(JavaUtils.isNotEmpty(token)){

			String url = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE_GOOGLE + EdoRequest.QUERY_SEPARATOR + 
					EdoRequest.VALUE_STATE + "=%2bt::" + token +"&"+ EdoRequest.VALUE_CODE + "=" + code + "&origin=app";


			EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, url, null);

			String response = request.send(c);
			
			if(!EdoRequest.hasEdoErrors(response)){
				return true;
			}
		}
		return false;
	}




	public static int loginWithGoogle3(final Context c, String token){
		int result = EdoRequest.ERROR_GENERAL;

		if(JavaUtils.isNotEmpty(token)){

			String url = EdoRequest.HOST_PREFIX + EdoRequest.PATH_LOGIN_GOOGLE + EdoRequest.QUERY_SEPARATOR + EdoRequest.VALUE_TOKEN + "=" + token;

			EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, url, null);

			String response = request.send(c);
			
			if(!EdoRequest.hasEdoErrors(response)){

				//Se non ci sono errori creo l'oggetto LoginResponseParam dalla risposta

				LoginWG3Resp responseParam = LoginWG3Resp.fromJson(response);

				if(responseParam!=null && responseParam.user!=null && responseParam.contact!=null){

					LUser user = responseParam.user;
					LContact contact = responseParam.contact;
					boolean isRegistration = responseParam.registration;

					if(user!=null && JavaUtils.isNotEmpty(user.getId())){
						//Salvo alcune preferenze per ottenerle più velocemente in seguito senza fare query al db

						user.setName(contact.getName());
						user.setSurname(contact.getSurname());
						user.setLastTokenUpdate(System.currentTimeMillis());
						
						user.setLoginDate(System.currentTimeMillis());
						user.setDeviceId(LUser.getDeviceId(c));
						
						//Creo gli oggetti user e contact e li salvo sul database
						LUserDAO userDAO = new LUserDAO(c);
						userDAO.insertFromWebServer(user);

						ThisApp.getInstance(c).setUser(user);

						result = isRegistration?EdoRequest.SUCCESS_REGISTRATION:EdoRequest.SUCCESS_LOGIN;
					}
				}
			}else{
				result = EdoRequest.ERROR_GENERAL;
			}
			LoginWG1Resp.remove(c);
		}
		return result;
	}





	//*********************************************************************
	//*		LOGOUT
	//*********************************************************************

	public static boolean logout(Context c){
		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_LOGOUT;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(c);

		LoginWG1Resp.remove(c);
		
		EdoUI.resetApplicationDataAndRestart(c);

		return !EdoRequest.hasEdoErrors(response);
	}




	//*********************************************************************
	//*		TOKEN
	//*********************************************************************
	public static boolean hasRefreshToken(Context c){
		ThisApp app = ThisApp.getInstance(c);
		return (System.currentTimeMillis()-app.getLastTokenUpdate()<REFRESH_TOKEN_EXPIRATION_TIME);
	}
	
	public static int refreshToken(Context c, boolean force){
		ThisApp app = ThisApp.getInstance(c);
		if(System.currentTimeMillis()-app.getLastTokenUpdate()>=REFRESH_TOKEN_EXPIRATION_TIME || force){

			if(ASI.isInternetOn(c)){
				
				if(EdoRequest.IS_TEST_APP){
					EdoNotification.showQuickNotification(c, EdoNotification.ID_TEST, 
							"TOKEN", "UPDATING TOKEN", null, null, null);
				}
				
				
				String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_LOGIN;

				EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, null);

				String response = request.send(c);
				
				if(!EdoRequest.hasEdoErrors(response)){
					EdoToken edoT = EdoToken.fromJson(response);

					if(edoT!=null && JavaUtils.isNotEmpty(edoT.token)){
						app.saveEdoToken(edoT.token);
						return Constants.RESPONSE_SUCCESS;
					}else{

						EdoUI.resetApplicationDataAndRestart(c);

						return Constants.RESPONSE_FAILED;
					}
				}
				
			}else{
				return Constants.RESPONSE_NO_INTERNET_CONNECTION;
			}
		}
		return Constants.RESPONSE_SUCCESS;
	}


	static class EdoToken {
		public String token;
		public String id;

		public String toJson(){
			return new Gson().toJson(this);
		}

		public static EdoToken fromJson(String json){
			try{
				return new Gson().fromJson(json, EdoToken.class);
			}catch (com.google.gson.JsonSyntaxException e){
				eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "EdoToken:\n"+e.getMessage());
				eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "EdoToken JSON:\n"+json);
			}
			return null;
		}
	}
























	//*********************************************************************
	//*		OLD LOGIN
	//*********************************************************************	
	//	public static int executeRegistration(final Context c, LocalDBManager db, final String name, final String surname, 
	//			final String email, final String password, final String gcmRegistrationId){
	//
	//		//Creo le ValuePairs: i parametri da passare alla request
	//
	//		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	//		nameValuePairs.add(new BasicNameValuePair("name", name));
	//		nameValuePairs.add(new BasicNameValuePair("surname", surname));
	//
	//
	//		Device device = new Device(c, gcmRegistrationId);
	//
	//		nameValuePairs.add(new BasicNameValuePair("device_id", device.get_id()));
	//		nameValuePairs.add(new BasicNameValuePair("device_type", device.getType()));
	//		nameValuePairs.add(new BasicNameValuePair("device_model", device.getModel()));
	//		nameValuePairs.add(new BasicNameValuePair("device_os", device.getOs()));
	//		nameValuePairs.add(new BasicNameValuePair("device_os_ver", device.getOs_ver()));
	//		nameValuePairs.add(new BasicNameValuePair("device_push_id", device.getPush_id()));
	//
	//		nameValuePairs.add(new BasicNameValuePair("email", email));
	//		nameValuePairs.add(new BasicNameValuePair("password", password));
	//
	//		String url = EdoRequest.URL_PREFIX + EdoRequest.URL_REGISTRATION;
	//
	//		//Invio la richiesta e ottengo una risposta
	//
	//		String response = HttpRequest.sendPost(url, null, nameValuePairs, null);
	//
	//		int result = EdoRequest.ERROR_GENERAL;
	//
	//		//Controllo se la risposta sia o meno un errore
	//
	//		RequestError re = RequestError.fromJson(response);
	//		if(re==null || (re!=null && JavaUtils.isEmpty(re.error))){
	//
	//			//Se non ci sono errori creo l'oggetto webEntity dalla risposta
	//
	//			WEntity e = WEntity.fromJson(response);
	//
	//			if(e!=null && e.user!=null && JavaUtils.isNotEmpty(e.user.loginCredentials)){
	//
	//				//Salvo alcune preferenze per ottenerle più velocemente in seguito senza fare query al db
	//
	//				EdoPreferences userInfo = new EdoPreferences();
	//				userInfo.setLoginToken(e.user.getLoginCredentials());
	//				userInfo.setEdoUserId(e.user.get_id());
	//				userInfo.setEdoUser(c,e.user.get_id());
	//				userInfo.setUserEmail(email);
	//				userInfo.setUserName(name);
	//				userInfo.setUserSurname(surname);
	//				userInfo.save(c);
	//
	//				//Creo gli oggetti user e contact e li salvo sul database
	//
	//				ObjectId obId = new ObjectId(e.user.get_id());
	//				Long date = obId.getTime();
	//
	//				LUser user = new LUser();
	//				user.setId(e.user.get_id());
	//				user.setcDate(date);
	//				user.setContactId(e.user.getContactId());	
	//				user.setLoginCredentials(userInfo.getEdoUser());    //Salvo l'edoUser userId+":"+deviceId
	//				user.setLoginEmail(email);
	//				user.setmDate(date);
	//
	//				LUserDAO userDAO = new LUserDAO(c,db);
	//				userDAO.addUser(user);
	//
	//				LContact contact = new LContact();
	//				contact.setId(e.user.getContactId());
	//				contact.setEdoUserId(e.user.get_id());
	//				contact.setmDate(date);
	//				contact.setName(name);
	//				contact.setSurname(surname);
	//
	//
	//				LContactDAO contactDAO = new LContactDAO(c,db);
	//				LEmailAddress localEmailAddress = new LEmailAddress(contact.getId(), email);
	//				contactDAO.insertContact(contact,localEmailAddress,true);
	//
	//
	//				result = EdoRequest.SUCCESS;
	//			}
	//		}else if(re!=null && JavaUtils.isNotEmpty(re.error)){
	//			result = JavaUtils.getIntegerValue(re.error, EdoRequest.ERROR_GENERAL);
	//		}
	//
	//		return result;
	//	}
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//	public static int executeLogin(final Context c, LocalDBManager db, final String email, final String password, final String gcmRegistrationId){
	//
	//		//Creo le ValuePairs: i parametri da passare alla request	
	//
	//		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	//		nameValuePairs.add(new BasicNameValuePair("email", email));
	//		nameValuePairs.add(new BasicNameValuePair("password", password));
	//
	//		Device device = new Device(c, gcmRegistrationId);
	//
	//		nameValuePairs.add(new BasicNameValuePair("device_id", device.get_id()));
	//		nameValuePairs.add(new BasicNameValuePair("device_type", device.getType()));
	//		nameValuePairs.add(new BasicNameValuePair("device_model", device.getModel()));
	//		nameValuePairs.add(new BasicNameValuePair("device_os", device.getOs()));
	//		nameValuePairs.add(new BasicNameValuePair("device_os_ver", device.getOs_ver()));
	//		nameValuePairs.add(new BasicNameValuePair("device_push_id", device.getPush_id()));
	//
	//		String url = EdoRequest.URL_PREFIX + EdoRequest.URL_LOGIN;
	//
	//		//Invio la richiesta e ottengo una risposta
	//
	//		String response = HttpRequest.sendPost(url, null, nameValuePairs, null);
	//		int result = EdoRequest.ERROR_GENERAL;
	//
	//		RequestError re = RequestError.fromJson(response);
	//		if(re==null || (re!=null && JavaUtils.isEmpty(re.error))){
	//
	//			//Se non ci sono errori creo l'oggetto LoginResponseParam dalla risposta
	//
	//			LoginResponseParams responseParam = LoginResponseParams.fromJson(response);
	//
	//			if(responseParam!=null && JavaUtils.isNotEmpty(responseParam.token)){
	//
	//				//Creo l'oggetto contact dalla risposta
	//
	//				LContact contact = responseParam.contact;
	//
	//				//Salvo alcune preferenze per ottenerle più velocemente in seguito senza fare query al db
	//
	//				EdoPreferences userInfo = new EdoPreferences(c);
	//				userInfo.setEdoUserId(responseParam.id);
	//				userInfo.setLoginToken(responseParam.token);
	//				userInfo.setEdoUser(c,responseParam.id);
	//				userInfo.setUserEmail(email);
	//				userInfo.setUserName(contact.getName());
	//				userInfo.setUserSurname(contact.getSurname());
	//				userInfo.save(c);
	//
	//				//Creo gli oggetti user e contact e li salvo sul database
	//
	//				ObjectId obId = new ObjectId(responseParam.id);
	//				Long date = obId.getTime();
	//
	//				LUser user = new LUser();
	//				user.setId(responseParam.id);
	//				user.setcDate(date);
	//				user.setContactId(responseParam.contact._id);
	//				user.setLoginCredentials(userInfo.getEdoUser());       //Salvo l'edoUser userId+":"+deviceId
	//				user.setLoginEmail(email);
	//				user.setmDate(date);
	//
	//				LUserDAO userDAO = new LUserDAO(c,db);
	//				userDAO.addUser(user);
	//
	//
	//				LContactDAO contactDAO = new LContactDAO(c,db);
	//				contact.userId = user.getId();
	//				contactDAO.addFromWeb(responseParam.contact,false,true, null);
	//
	//				result = EdoRequest.SUCCESS;
	//
	//			}
	//		}else if(re!=null && JavaUtils.isNotEmpty(re.error)){
	//			result = JavaUtils.getIntegerValue(re.error, EdoRequest.ERROR_GENERAL);
	//		}
	//
	//		return result;
	//	}
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//



}
