package io.edo.db.global.uibeans;

import android.content.Context;

import io.edo.db.global.EdoActivityManager;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LBaseDAO;
import io.edo.db.web.beans.WEntity;
import io.edo.edomobile.ThisApp;

/**
 * Created by PaoloBriganti on 19/06/15.
 */
public abstract class BaseUIDAO {
    public ThisApp app;
    public Context context;
    public EdoActivityManager manager;

    public BaseUIDAO(Context context){
        this.context = context;
        this.app = ThisApp.getInstance(context);
        this.manager = new EdoActivityManager(context);
    }


    public abstract LBaseDAO getLDAO();

    public LActivity processActivity(WEntity entity) {
        return processActivity(entity,true);
    }

    public LActivity processActivity(WEntity entity, boolean saveToDB) {
        if(entity!=null){
            if(entity.act!=null){
                entity.act = processActivity(entity.act, saveToDB);
                return entity.act;
            }
        }
        return null;
    }

    public LActivity processActivity(LActivity activity){
        return manager.processActivity(activity);
    }

    public LActivity processActivity(LActivity activity, boolean saveToDB){
        return manager.processActivity(activity, saveToDB);
    }

    public LActivity processActivity(LActivity activity, boolean saveToDB, EdoActivityManager.Callback callback){
        return manager.processActivity(activity, saveToDB, false, callback);
    }
}
