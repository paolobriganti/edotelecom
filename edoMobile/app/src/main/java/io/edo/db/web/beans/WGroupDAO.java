package io.edo.db.web.beans;

import android.content.Context;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.local.beans.LContact;
import io.edo.db.web.request.EdoRequest;
import io.edo.edomobile.ThisApp;

public class WGroupDAO {


	public static final String ID_OF_CURRENT_USER_FOR_LEAVE_GROUP = "me";


	public static WEntity createGroup(final Context context, final LContact group){

		//Send contacts to server
		WEntity contactsEnity = WContactDAO.addContacts(context,group.getGroupMembers(context));

		group.setGroupMembers(context, contactsEnity.contacts);

		if(group.getGroupMembersCount()>=LContact.MIN_MEMBERS-1 && group.getGroupMembersCount()<=LContact.MAX_MEMBERS-1){
			String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_GROUP +"?sendEmail=1";


			EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, group.getCreateGroupRequestBody());

			String response = request.send(context);
			if(!EdoRequest.hasEdoErrors(response)){
				WEntity entity = WEntity.fromJson(response);
				if(entity!=null){
					final LContact newGroup = entity.group;

					if(newGroup!=null && JavaUtils.isNotEmpty(newGroup.getId())){
						group.setId(newGroup.getId());

						entity.group = group;
                        entity.contacts = contactsEnity.contacts;
                        return entity;
					}
				}
			}
		}

		return null;
	}


	public static boolean buildSuggested(Context context){


		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_GROUP_SUGGESTED;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, null);

		String response = request.send(context);

		if(!EdoRequest.hasEdoErrors(response))
			return response.contains(EdoRequest.SUCCESS);

		return false;
	}





	public static WEntity updateGroupMetadata(Context context, LContact group, boolean hasRename){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_GROUP + EdoRequest.PATH_SEPARATOR + group.getId();

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, group.getUpdateGroupRequestBody(hasRename));

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.group = group;
            }
            return entity;
        }
        return null;
	}


    public static LContact[] addContactsToGroup(Context context, String groupId, ArrayList<LContact> members){

        //Send contacts to server
        WEntity contactsEnity = WContactDAO.addContacts(context,members);
        LContact[] newContacts = contactsEnity.contacts;

        if(newContacts!=null && newContacts.length>0){
            String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_GROUP + EdoRequest.PATH_SEPARATOR + groupId + EdoRequest.PATH_MEMBERS;

            String body = LContact.getWebContactJson(newContacts);

            EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, body);

            String response = request.send(context);
            if(!EdoRequest.hasEdoErrors(response)){
                LContact group = LContact.fromJson(response);
                if(group!=null){
                    return newContacts;
                }
            }
        }


        return null;
    }

	public static WEntity deleteGroup(Context context, LContact cgroup){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_GROUP + EdoRequest.PATH_SEPARATOR + cgroup.getId()+
				EdoRequest.QUERY_SEPARATOR +
				EdoRequest.VALUE_SERVICEID + "=" + cgroup.getServiceIdOfCloudFolder() + "&"+
				EdoRequest.VALUE_KEEP + "=0";

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_DELETE, targetURL, null);

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.group = cgroup;
            }
            return entity;
        }
        return null;
	}





	/**
	 *  Questo handler permette sia all’owner di rimuovere un membro dal gruppo che ai membri di lasciare il gruppo
	 *	Remove:
	 *	@param containerId è l’edo user id del contatto da rimuovere o nel caso il contatto sia pending è l’_id del contatto. Solo l’owner può rimuovere membri dal gruppo.
	 *	Leave:
	 *	@param containerId  = me, permette ai membri di abbandonare il gruppo. L’owner non può abbandonare il gruppo
	 */

	public static boolean deleteContactFromGroup(Context context, String groupId, String containerId){

        ThisApp app = ThisApp.getInstance(context);

        if(app.getEdoUserId().equals(containerId)){
            containerId = "me";
        }else if(app.getUserContact().getId().equals(containerId)){
            containerId = "me";
        }

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_GROUP + EdoRequest.PATH_SEPARATOR + groupId + EdoRequest.PATH_MEMBERS + EdoRequest.PATH_SEPARATOR + containerId;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_DELETE, targetURL, null);

		String response = request.send(context);

		if(!EdoRequest.hasEdoErrors(response))
			return response.contains(EdoRequest.SUCCESS);

		return false;
	}

	public static boolean leaveTheGroup(Context context, String groupId){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_GROUP + EdoRequest.PATH_SEPARATOR + groupId + EdoRequest.PATH_MEMBERS + EdoRequest.PATH_SEPARATOR + ID_OF_CURRENT_USER_FOR_LEAVE_GROUP;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_DELETE, targetURL, null);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			if(response.contains(EdoRequest.SUCCESS)){
				return true;
			}
		}
		return false;
	}







    public static LContact getGroup(Context context, String groupId){


        String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_GROUP + EdoRequest.PATH_SEPARATOR + groupId;

        EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

        String response = request.send(context);

        LContact newGroup = LContact.fromJson(response);
        if(!EdoRequest.hasEdoErrors(response)){
            if(newGroup!=null && JavaUtils.isNotEmpty(newGroup.getId())){
                return newGroup;
            }
        }
        return null;
    }

    public static LContact[] getGroups(Context context){
        String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_USER_GROUPS;

        EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

        String response = request.send(context);

        LContact[] webGroups = LContact.fromJsonToArray(response);
        if(!EdoRequest.hasEdoErrors(response)){
            if(webGroups!=null && webGroups.length>0){
                return webGroups;
            }
        }
        return null;
    }

}
