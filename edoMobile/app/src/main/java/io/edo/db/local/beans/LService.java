package io.edo.db.local.beans;

import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.support.EdoResource;
import io.edo.db.local.beans.support.ServiceConfiguration;
import io.edo.edomobile.R;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class LService  implements EdoResource {
	private static final long serialVersionUID = 1L;

	
	private String _id = null;
	private String name = null;
	private String email = null;
	private String accessToken = null;
	private String configuration = null;
	private String status = null;
	private Long mDate = null;
	
	private Integer isUnread = null;

	public static final String SERVICE_NAME_GOOGLE = "google";
	public static final String SERVICE_NAME_DROPBOX = "dropbox";
	public static final String SERVICE_NAME_SKYDRIVE = "skydrive";
	public static final String SERVICE_NAME_LINK_PARSER = "linkparser";
	
	public LService() {
	}
	
	public LService(String id) {
		this._id = id;
	}
	

	
	public LService(Cursor cursor){
		setId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_ID)));
		setName(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_NAME)));
		setName(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_NAME)));
		setEmail(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_EMAIL)));
		setAccessToken(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_ACCESS_TOKEN)));
		setConfiguration(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONFIGURATION)));
		setStatus(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_STATUS)));
		setmDate(cursor.getLong(cursor.getColumnIndex(LocalDBHelper.KEY_MDATE)));
		setIsUnread((cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_ISUNREAD))));
	}
	
	
	



	public String getWebServiceJson() {
		LService wservice = new LService();
		wservice._id = this.getId();
		wservice.name = this.getName();
		wservice.email = this.getEmail();
		wservice.accessToken = this.getAccessToken();
		wservice.configuration = this.getConfiguration();
		wservice.status = this.getStatus();
		wservice.mDate = this.getmDate();
		
		wservice.isUnread = null;

		return wservice.toJson();
	}

	


	private String toJson(){
		return new Gson().toJson(this);
	}

	public static LService fromJson(String json){
		try{
			return new Gson().fromJson(json, LService.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LService:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LService JSON:\n"+json);
			return null;
		}
	}

	public static LService[] fromJsonToArray(String json){
		try{
			return new Gson().fromJson(json, LService[].class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LService[]:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LService[] JSON:\n"+json);
			return null;
		}
	}

	@Override
	public String getId() {
		return _id;
	}





	public void setId(String id) {
		this._id = id;
	}




	@Override
	public String getName() {
		return name;
	}





	public void setName(String name) {
		this.name = name;
	}





	public String getEmail() {
		return email;
	}

	public String getEmailUserName(int maxLength) {
		if(email!=null && email.length()>maxLength){
			if(email.contains("@")){
				String user = email.split("@")[0];
				if(user.length()>maxLength)
					return user.substring(0, maxLength-1)+"..."; 
				else
					return user;
			}
		}			
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}





	public String getAccessToken() {
		return accessToken;
	}





	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}





	public String getConfiguration() {
		return configuration;
	}

	
	public ServiceConfiguration getServiceConfiguration(){
		if(JavaUtils.isNotEmpty(configuration)){
			return ServiceConfiguration.fromJson(configuration);
		}
		return null;
	}



	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}





	public String getStatus() {
		return status;
	}





	public void setStatus(String status) {
		this.status = status;
	}





	public Long getmDate() {
		return mDate;
	}





	public void setmDate(Long mDate) {
		this.mDate = mDate;
	}




	public Integer getIcon(Context c) {
		return c.getResources().getIdentifier("ic_"+name, "drawable", c.getPackageName());
	}
	
	public Integer getCloudIcon(Context c) {
		if(name.equals(SERVICE_NAME_GOOGLE))
			return R.drawable.ic_googledrive;
		
		return getIcon(c);
	}
	
	public Integer getPickerCloudIcon(Context c) {
		if(name.equals(SERVICE_NAME_GOOGLE))
			return R.drawable.bt_picker_drive;
		
		return getIcon(c);
	}
	
	
	public String getCommercialName(Context c) {
		if(name.equals(SERVICE_NAME_GOOGLE))
			return c.getResources().getString(R.string.google);
		else if(name.equals(SERVICE_NAME_DROPBOX))
			return c.getResources().getString(R.string.dropBox);
		else if(name.equals(SERVICE_NAME_SKYDRIVE))
			return c.getResources().getString(R.string.skyDrive);

		return null;
	}

	public String getCommercialCloudName(Context c) {
		return c.getResources().getString(getCommercialCloudNameResource(c));
	}

	public int getCommercialCloudNameResource(Context c) {
		if(name.equals(SERVICE_NAME_GOOGLE))
			return R.string.googleDrive;
		else if(name.equals(SERVICE_NAME_DROPBOX))
			return R.string.dropBox;
		else if(name.equals(SERVICE_NAME_SKYDRIVE))
			return R.string.skyDrive;

		return R.string.cloudService;
	}


//	public String toJson(){
//		return new Gson().toJson(this);
//	}
//	
//	public static LService fromJson(String json){
//		try{
//			return new Gson().fromJson(json, LService.class);
//		}catch (com.google.gson.JsonSyntaxException e){
//			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LService:\n"+e.getMessage());
//			return null;
//		}
//	}
	
	
	
	public Integer getIsUnread() {
		return isUnread;
	}

	public boolean isUnread() {
		return isUnread!=null && isUnread==1;
	}

	public void setIsUnread(Integer isUnread) {
		this.isUnread = isUnread;
	}
	
	public void setIsUnread(boolean isUnread) {
		this.isUnread = isUnread?1:0;
	}
}
