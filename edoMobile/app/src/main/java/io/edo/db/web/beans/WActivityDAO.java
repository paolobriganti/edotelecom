package io.edo.db.web.beans;

import android.content.Context;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.web.request.EdoRequest;
import io.edo.edomobile.ThisApp;

public class WActivityDAO {



	public static LActivity[] getActivitiesByContainerId(Context context, EdoContainer container, int offset){
		ThisApp app = ThisApp.getInstance(context);


		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_USER_ACTIVITIES;

		if(container!=null){
			targetURL += EdoRequest.PATH_SEPARATOR + (container.isPersonal(context)?"me":(container.getContainerId()));

			if(container.isContact()){
				targetURL+="?f=1";
			}
		}

		if(offset>0){
			if(targetURL.contains("?")){
				targetURL += "&offset="+offset;
			}else{
				targetURL += "?offset="+offset;
			}
		}

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			return LActivity.fromJsonToArray(response);
		}
		return null;
	}



}
