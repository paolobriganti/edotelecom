package io.edo.db.web.beans;

import android.content.Context;

import io.edo.db.local.beans.LService;
import io.edo.db.web.request.EdoRequest;

public class WServiceDAO {


	public static boolean addService(Context context, LService localService){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE;


		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, localService.getWebServiceJson());

		String response = request.send(context);


		return !EdoRequest.hasEdoErrors(response);
	}


	public static boolean updateService(Context context, LService localService){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE + EdoRequest.PATH_SEPARATOR + localService.getId();

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, localService.getWebServiceJson());

		String response = request.send(context);


		return !EdoRequest.hasEdoErrors(response);
	}

	public static LService updateGoogleAccessToken(Context context, String serviceId){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE_GOOGLE + EdoRequest.PATH_SEPARATOR + serviceId;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, null);

		String response = request.send(context);

		if(!EdoRequest.hasEdoErrors(response)){
			LService webService = LService.fromJson(response);
			if(webService!=null){
				return webService;
			}
		}
		return null;
	}

	public static boolean deleteService(Context context, LService localService){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE + EdoRequest.PATH_SEPARATOR + localService.getId();

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_DELETE, targetURL, null);

		String response = request.send(context);

		return !EdoRequest.hasEdoErrors(response);
	}

	public static LService[] getServices(Context context){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_USER_SERVICES;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			return LService.fromJsonToArray(response);
		}
		return null;
	}

	public static LService getLinkParserService(Context context){
		String targetURL = EdoRequest.URL_LINK_THUMBNAIL_LOGIN;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			return LService.fromJson(response);
		}
		return null;
	}




}
