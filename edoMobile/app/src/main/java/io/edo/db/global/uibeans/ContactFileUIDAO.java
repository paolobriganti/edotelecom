package io.edo.db.global.uibeans;

import android.content.Context;

import java.util.ArrayList;

import io.edo.db.global.beans.ContactFileDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactFile;
import io.edo.db.local.beans.LContactFileDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.WEntity;


public class ContactFileUIDAO extends BaseUIDAO{

    ContactFileDAO contactFileDAO;

    public ContactFileUIDAO(Context context) {
        super(context);
        contactFileDAO = new ContactFileDAO(context);
    }

    @Override
    public LContactFileDAO getLDAO() {
        return contactFileDAO.getLDAO();
    }

    public LActivity add(LContactFile cf) {
        WEntity entity = contactFileDAO.add(cf);
        return processActivity(entity);
    }

    public LActivity add(final LContact contact, final LSpace spaceWithCurrentFolder, final LFile file){

        WEntity entity = contactFileDAO.add(contact,spaceWithCurrentFolder,file);

        return processActivity(entity);
    }

    public void addAll(final LContact contact, final LSpace spaceWithCurrentFolder, final ArrayList<LFile> files){
        LActivity act = null;

        if(files!=null && files.size()>0){
            for(LFile file:files){
                WEntity entity = contactFileDAO.add(contact,spaceWithCurrentFolder,file);
                act = entity.act;
                processActivity(act);
            }
        }
    }

    public LActivity fileUploaded(final LContact contact, final LSpace spaceWithCurrentFolder, final LFile file, final LActivity a){
        WEntity entity = contactFileDAO.add(new LContactFile(contact, spaceWithCurrentFolder,file));
        if(entity!=null && a!=null && entity.act!=null){
            entity.act.setOldActivityId(a.getId());
//            entity.act.setEvent(LActivity.EVENT_UPDATE);
        }
        return processActivity(entity, true);
    }

    public LActivity copyFileToContact(final LContact toContact, LSpace space, String oldFileId, LFile newFile){
        WEntity entity = contactFileDAO.copyFileToContact(toContact, space, oldFileId, newFile);

        return processActivity(entity);
    }

    public LActivity moveFileToContact(final LContact toContact, LSpace space, String oldFileId, LFile newFile){
        WEntity entity = contactFileDAO.moveFileToContact(toContact, space, oldFileId, newFile);

        return processActivity(entity);
    }



    public LFile[] downloadAllFileOf(LContact contact){
        return downloadAllFileOf(contact);
    }

    public LFile[] downloadAllFileOfFolderOf(LContact contact, String folderId){
        return downloadAllFileOfFolderOf(contact,folderId);
    }





}
