package io.edo.db.web.beans;

import android.content.Context;

import com.google.gson.Gson;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class WMessage {
	private String c; //: 'p' or 'c', * #context: personal or contact.
	private String cid; //: _id, #container id, a contact or category id
	//this is the edo id of the recipient
	//for contact realtime chat.
	private int rt; //: ResourceType, #resource type code.
	private String rid; //: _id, #resource id of 'rt' type.
	private int a; //: Action, * #action code.
	private String arg; // str #arguments to the action, this should
	//contain e.g. the chat messages.
	
	public WMessage(String c, String cid, int rt, String rid, int a,
			String arg) {
		super();
		this.c = c;
		this.cid = cid;
		this.rt = rt;
		this.rid = rid;
		this.a = a;
		this.arg = arg;
	}

	
//	public WMessage(LActivity msg) {
//		super();
//		this.c = msg.getContext();
//		this.cid = msg.getContainerId();
//		this.rt = ""+msg.getResourceType();
//		this.rid = msg.getResourceId();
//		this.a = ""+msg.getEvent();
//		this.arg = msg.getMessage();
//	}
	
	public WMessage() {
		// TODO Auto-generated constructor stub
	}


	public WMessage(Context c, EdoContainer container, String text){
		if(container!=null){
			this.c = LActivity.getContext(c,container);
			this.cid = container.getContainerId();
			this.a = LActivity.EVENT_MESSAGE;
			this.arg = text;
		}
	}

	public WMessage(LActivity aMessage){
		if(aMessage!=null){
			this.c = aMessage.getContext();
			this.cid = aMessage.getContainerId();
			this.a = LActivity.EVENT_MESSAGE;
			this.arg = aMessage.getMessage();
		}
	}
	
	
	
	public String getContext() {
		return c;
	}

	public void setContext(String c) {
		this.c = c;
	}

	public String getContainerId() {
		return cid;
	}

	public void setContainerId(String cid) {
		this.cid = cid;
	}

	public int getResourceType() {
		return rt;
	}

	public void setResourceType(int rt) {
		this.rt = rt;
	}

	public String getResourceId() {
		return rid;
	}

	public void setResourceId(String rid) {
		this.rid = rid;
	}

	public int getEvent() {
		return a;
	}

	public void setEvent(int a) {
		this.a = a;
	}

	public String getArgument() {
		return arg;
	}

	public void setArgument(String arg) {
		this.arg = arg;
	}
	
	
	
	public String toJson(){
		return new Gson().toJson(this);
	}

	public static WMessage fromJson(String json){
		try{
			return new Gson().fromJson(json, WMessage.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WMessage:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WMessage JSON:\n"+json);
			return null;
		}
	}

}
