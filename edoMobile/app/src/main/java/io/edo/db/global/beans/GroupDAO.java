package io.edo.db.global.beans;

import android.content.Context;

import java.util.ArrayList;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.LGroupMember;
import io.edo.db.local.beans.LGroupMemberDAO;
import io.edo.db.web.beans.WContactDAO;
import io.edo.db.web.beans.WEntity;
import io.edo.db.web.beans.WGroupDAO;
import io.edo.utilities.ObjectId;

public class GroupDAO extends BaseDAO{
    LContactDAO lContactDAO;
    LGroupMemberDAO lGroupMemberDAO;

    public GroupDAO(Context context) {
        super(context);
        lContactDAO = new LContactDAO(context);
        lGroupMemberDAO = new LGroupMemberDAO(context);
    }

    @Override
    public LGroupMemberDAO getLDAO() {
        return lGroupMemberDAO;
    }

    public WEntity add(LContact group) {
        WEntity entity = WGroupDAO.createGroup(context, group);
        if(entity!=null && entity.group!=null && entity.contacts!=null){
//            entity.group = WContactDAO.updateValuesFromWeb(group, entity.group);
            entity.group = group;
            lGroupMemberDAO.insertGroupFromWebServer(entity.group, true);

            for(LContact newWebContact : entity.contacts) {
                if (newWebContact != null) {
                    lContactDAO.insertFromWebServer(newWebContact);
                }
            }
        }
        return entity;
    }


    public WEntity updateGroupMetadata(LContact group, boolean hasRename) {
        WEntity entity = WGroupDAO.updateGroupMetadata(context, group, hasRename);

        if(entity!=null){
            entity.group = group;
            lGroupMemberDAO.insertGroupFromWebServer(entity.group, false);
        }
        return entity;
    }

    public LContact[] addContactsToGroup(LContact group, ArrayList<LContact> members) {
        LContact[] newMembers = WGroupDAO.addContactsToGroup(context, group.getId(), members);

        if(newMembers!=null && newMembers.length>0){
            for(LContact member:newMembers)
                if(lContactDAO.insertFromWebServer(member))
                    lGroupMemberDAO.insertFromWebServer(new LGroupMember(group,member));
        }
        return newMembers;
    }


    public WEntity delete(LContact group) {
        WEntity entity = WGroupDAO.deleteGroup(context, group);

        if(entity!=null){
            entity.group = WContactDAO.updateValuesFromWeb(group, entity.group);
            lContactDAO.delete(group);
        }

        return entity;
    }


    public boolean deleteContactFromGroup(LContact group, LContact member){
        if(WGroupDAO.deleteContactFromGroup(context, group.getId(),member.getContainerId())){
            return lGroupMemberDAO.delete(new LGroupMember(group,member));
        }

        return false;
    }

    public boolean leaveTheGroup(LContact group){
        if(WGroupDAO.leaveTheGroup(context, group.getId())){
            if(lContactDAO.delete(group))
                return lGroupMemberDAO.deleteGroupMembers(group.getId());
        }

        return false;
    }


    public LContact download(String id){
        LContact wgroup = WGroupDAO.getGroup(context, id);
        if(wgroup!=null) {
            updatePendingContacts(wgroup);
            lGroupMemberDAO.insertGroupFromWebServer(wgroup, false);
        }
        return wgroup;
    }



    public LContact[] downloadAll(){
        LContact[] wgroups = WGroupDAO.getGroups(context);
        if(wgroups!=null && wgroups.length>0)
            for(LContact wgroup: wgroups) {
                updatePendingContacts(wgroup);
                lGroupMemberDAO.insertGroupFromWebServer(wgroup, false);
            }
        return wgroups;
    }





    public void updatePendingContacts(LContact wgroup){
        if(wgroup.getPending()!=null && wgroup.getPending().length>0 && !wgroup.isUserOwnerOfTheGroup(app.getEdoUserId())){
            for(LContact pending: wgroup.getPending()){
                pending.setId(new ObjectId().toString()); //BECAUSE THE ID OF PENDING CONTACT COMES FROM OWNER CONTACTS
            }
        }
    }


}
