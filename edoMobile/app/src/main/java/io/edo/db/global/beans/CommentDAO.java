package io.edo.db.global.beans;

import android.content.Context;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.support.Comment;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.WCommentDAO;
import io.edo.db.web.beans.WEntity;
import io.edo.utilities.eLog;

public class CommentDAO extends BaseDAO{

	LFileDAO lfileDAO;

	public CommentDAO(Context context) {
		super(context);
		this.lfileDAO = new LFileDAO(context);
	}

	@Override
	public LFileDAO getLDAO() {
		return lfileDAO;
	}

	public WEntity addComment(final Context c,
							  final EdoContainer container,
							  final LFile file,
							  final String message){

		String edoUserId = app.getEdoUserId();
		boolean isPersonal = container.isPersonal(edoUserId);

		final Comment comment = new Comment(edoUserId, message);

		WEntity entity = null;

		if(isPersonal){
			file.setCommentSingle(comment);
			entity = WCommentDAO.updateComment(context, file,comment);
		}else if(container.isContactOrGroup()){
			file.addNewComment(comment);
			entity = WCommentDAO.addComment(context, file, comment);
		}

		if(entity!=null){
			entity.note = comment;
			entity.file = file;

			lfileDAO.update(file);

			if(entity.act!=null)
				entity.act = addExtraToActivity(entity.act,container,null,file);
			return entity;
		}
		return null;
	}



	public LFile downloadComments(LFile file, boolean isPush){
		file = WCommentDAO.getComments(context, file, isPush);
		if(file!=null)
			lfileDAO.update(file);
		return file;
	}






	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//*		ACTIVITY
	//*****************************************************************************************************************************************************************************************************************************************************************************************************

	public LActivity addExtraToActivity(LActivity act, EdoContainer container, LSpace spaceWithCurrentFolder, LFile file){
		if(act==null)
			return null;

		act.setStatus(LActivity.STATUS_COMPLETE);

		act.setExtra(app.getUserContact(), container, spaceWithCurrentFolder, file);
		return act;
	}



}
