package io.edo.db.global.beans;

import android.content.Context;

import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactFile;
import io.edo.db.local.beans.LContactFileDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.Comment;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.EdoResource;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.WContactFileDAO;
import io.edo.db.web.beans.WEntity;
import io.edo.db.web.beans.WFileDAO;
import io.edo.edomobile.Service_DwnUpl;
import io.edo.ui.OnProgressChangeListener;
import io.edo.utilities.ObjectId;


public class ContactFileDAO extends BaseDAO{

    LContactFileDAO lcontactFileDAO;

    public ContactFileDAO(Context context) {
        super(context);
        lcontactFileDAO = new LContactFileDAO(context);
    }

    @Override
    public LContactFileDAO getLDAO() {
        return lcontactFileDAO;
    }

    public WEntity add(LContactFile cf) {
        LFile file = cf.getFile();
        if(file.isNotSavedToServer()){
            file.setStatus(null);
        }

        WEntity entity = WContactFileDAO.addFileToContact(context,
                cf.getContact(),
                cf.getSpaceWithCurrentFolder(),
                cf.getFile());

        LFile newFile = cf.getFile();
        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(newFile,entity.file);
            }else{
                entity.file = newFile;
            }

            cf.setFile(entity.file);

            lcontactFileDAO.insertFromWebServer(cf);

            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act, cf.getContact(), cf.getSpaceWithCurrentFolder(), cf.getFile());
            }
        }else if(JavaUtils.isEmpty(newFile.getStatus())){
            entity = new WEntity();
            entity.file = newFile;

            LActivity act = getFakeActivity(LActivity.EVENT_INSERT, cf.getContact(), newFile);
            act = addExtraToActivity(act,cf.getContact(),cf.getSpaceWithCurrentFolder(),newFile);
            act.setStatus(LActivity.STATUS_NOT_SAVED_TO_SERVER);
            entity.act = act;

            newFile.setStatus(LFile.STATUS_NOT_SAVED_TO_SERVER);
            lcontactFileDAO.insertFromWebServer(cf);
        }


        return entity;
    }

    public WEntity add(final LContact contact, final LSpace spaceWithCurrentFolder, final LFile file){

        file.setParentId(spaceWithCurrentFolder.getCurrentFolder().getId());
        file.setServiceId(spaceWithCurrentFolder.getServiceId());
        file.setCollabsEmails(spaceWithCurrentFolder.getCollabsEmails());

        if(file.isStored() && !file.isInCloud()){
            WEntity entity = new WEntity();

            LActivity act = getFakeActivity(LActivity.EVENT_INSERT, contact, file);
            act = addExtraToActivity(act,contact,spaceWithCurrentFolder,file);
            act.setStatus(LActivity.STATUS_STARTED);

            entity.act = act;
            entity.file = file;

            if(contact.equals(app.getUserContact()))
                new Service_DwnUpl().upload(context, spaceWithCurrentFolder, file, act);
            else
                new Service_DwnUpl().uploadAndShare(context, contact, spaceWithCurrentFolder, file, act);

            lcontactFileDAO.insert(new LContactFile(contact,file));

            return entity;
        }

        if(file.isInCloud()){
            return copyFileToContact(contact, spaceWithCurrentFolder, "cloudCopy", file);
        }

        if(file.isOnlyMetaFile()){
            LContactFile contactFile = new LContactFile(contact,file);
            contactFile.setSpaceWithCurrentFolder(spaceWithCurrentFolder);
            return add(contactFile);
        }

        return null;
    }

    public WEntity copyFileToContact(final LContact toContact, LSpace space, String oldFileId, LFile newFile){
        newFile.setId(new ObjectId().toString());
        newFile.setParentId(space.getCurrentFolder().getId());
        newFile.setServiceId(space.getServiceId());
        newFile.setParentCloudId(space.getCurrentFolder().getCloudId()!=null ? space.getCurrentFolder().getCloudId() : null);
        newFile.setCollabsEmails(space.getCollabsEmails());
        newFile.setComments((Comment[])null);

        WEntity entity = WContactFileDAO.copyFileToContact(context, toContact, space, oldFileId, newFile);

        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(newFile,entity.file);
            }else{
                entity.file = newFile;
            }

            lcontactFileDAO.insertFromWebServer(new LContactFile(toContact, entity.file));

            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act,toContact,space, entity.file);
            }
        }
        return entity;
    }

    public WEntity moveFileToContact(final LContact toContact, LSpace space, String oldFileId, LFile newFile){
        newFile.setId(new ObjectId().toString());
        newFile.setParentId(space.getCurrentFolder().getId());
        newFile.setServiceId(space.getServiceId());
        newFile.setCollabsEmails(space.getCollabsEmails());
        newFile.setComments((Comment[]) null);

        WEntity entity = WContactFileDAO.copyFileToContact(context, toContact, space, oldFileId, newFile);

        if(entity!=null){

            if(entity.file!=null){
                entity.file = WFileDAO.updateValuesFromWeb(newFile,entity.file);
            }else{
                entity.file = newFile;
            }

            lcontactFileDAO.insertFromWebServer(new LContactFile(toContact, entity.file));

            if(entity.act!=null){
                entity.act = addExtraToActivity(entity.act,toContact,space, entity.file);
            }
        }
        return entity;
    }



    public LFile[] downloadAllFileOf(LContact contact, OnProgressChangeListener listener){
        LFile[] files = WContactFileDAO.getContactFiles(context, contact);
        if(files!=null && files.length>0) {

            long lastProgressSended = System.currentTimeMillis();

            for (int i = 0; i < files.length; i++) {
                LFile webFile = files[i];
                lcontactFileDAO.insertFromWebServer(new LContactFile(contact, webFile));

                if(System.currentTimeMillis()-lastProgressSended>1500){
                    lastProgressSended = System.currentTimeMillis();
                    if (listener != null) listener.onProgressChange(i, files.length, webFile, null);
                }
            }
        }
        return files;
    }


    public LFile downloadFileOf(LContact contact, String fileId){
        LFile file = WFileDAO.getFile(context, fileId);
        if(file!=null)
            lcontactFileDAO.insertFromWebServer(new LContactFile(contact,file));
        return file;
    }


    public LFile[] downloadAllFileOfFolderOf(LContact contact, String folderId){
        LFile[] files = WContactFileDAO.getContactFolderFiles(context, contact, folderId);
        if(files!=null && files.length>0)
            for(LFile file: files){
                lcontactFileDAO.insertFromWebServer(new LContactFile(contact,file));
            }
        return files;
    }




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTIVITY
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    public LActivity getFakeActivity(int event, EdoContainer container, EdoResource resource){
        LActivity a = new LActivity(LActivity.getContext(context, container), event, LActivity.RESOURCE_FILE);
        a.setResource(resource);
        return a;
    }


    public LActivity addExtraToActivity(LActivity act, LContact contact, LSpace spaceWithCurrentFolder, LFile file){
        act.setStatus(LActivity.STATUS_COMPLETE);
        act.setExtra(app.getUserContact(), contact, spaceWithCurrentFolder, file);
        return act;
    }

}
