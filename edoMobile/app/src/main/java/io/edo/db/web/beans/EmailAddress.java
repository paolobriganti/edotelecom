package io.edo.db.web.beans;

import java.io.Serializable;


public class EmailAddress implements Serializable{
	private static final long serialVersionUID = 1L;

	public static final String SCOPE_PRINCIPAL = "principal";
	public static final String SCOPE_HOME = "home";
	public static final String SCOPE_WORK = "work";
	
	
	private String scope = null;
	private String email = null;

	public EmailAddress(String scope, String email) {
		super();
		this.setScope(scope);
		this.setEmail(email);
	}
	
	public EmailAddress(String email) {
		super();
		this.setScope(SCOPE_PRINCIPAL);
		this.setEmail(email);
	}
	
	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}