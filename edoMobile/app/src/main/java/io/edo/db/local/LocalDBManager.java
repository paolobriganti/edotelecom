package io.edo.db.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;

public class LocalDBManager {

	@SuppressWarnings("unused")
	private static final String LOG_TAG = LocalDBManager.class.getSimpleName();

	private Context context;
	private SQLiteDatabase database;
	private LocalDBHelper dbHelper;

	public LocalDBManager(Context context) {
		this.context = context;
		open(false);
	}

	public LocalDBManager(Context context, boolean forceOpen) {
		this.context = context;
		open(forceOpen);
	}

	public LocalDBManager open(boolean force) throws SQLException {
		dbHelper = new LocalDBHelper(context);
		if(database==null || !database.isOpen() || force)
			database = dbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		dbHelper.close();
		if(database!=null && database.isOpen())
			database.close();
	}

	public SQLiteDatabase getDataBase() {
		return database;
	}

	public boolean dropCreateDataBase() {
		LocalDBHelper.dropDatabase(database);
		LocalDBHelper.createDatabase(database);
		return true;
	}

	//create
	public long insert(String TABLE_NAME, ContentValues initialValues) {
		open(false);

		long id = database.insertWithOnConflict(TABLE_NAME, null, initialValues, SQLiteDatabase.CONFLICT_REPLACE);

		return id;
	}

	//update
	public boolean update(String TABLE_NAME, ContentValues updateValues, String whereClause) {
		open(false);


		boolean updated = database.update(TABLE_NAME, updateValues, whereClause, null) > 0;

		return updated;
	}

	//delete
	public boolean delete(String TABLE_NAME, String whereClause) {
		open(false);

		boolean deleted =  database.delete(TABLE_NAME, whereClause, null) > 0;

		return deleted;
	}

	//fetch all
	public Cursor selectAll(String TABLE_NAME, String[] tableKeys) {
		open(false);

		Cursor cursor = database.query(TABLE_NAME, tableKeys, null, null, null, null, null);

		return cursor;
	}

	public Cursor selectAll(String TABLE_NAME, String[] tableKeys, String orderBy) {
		open(false);


		Cursor cursor = database.query(TABLE_NAME, tableKeys, null, null, null, null, orderBy);

		return cursor;
	}

	//fetch
	public Cursor select(String TABLE_NAME, String[] tableKeys, String whereClause, String[] selectionArgs) {
		open(false);

		Cursor mCursor = database.query(true, TABLE_NAME, tableKeys, whereClause, selectionArgs, null, null, null, null);

		return mCursor;
	}



	public Cursor select(String TABLE_NAME, String[] tableKeys, String whereClause, String[] selectionArgs, String orderBy) {
		open(false);

		Cursor mCursor = database.query(true, TABLE_NAME, tableKeys, whereClause, selectionArgs, null, null,orderBy,null);

		return mCursor;
	}

	//execute
	public Cursor executeQuery(String sqlQuery, String[] values) {
		open(false);

		Cursor mCursor = database.rawQuery(sqlQuery, values);

		return mCursor;
	}












	public long getLastUpdateTime(){
		File f = context.getDatabasePath(dbHelper.getDatabaseName());
		return f!=null? f.lastModified():0;
	}




	public String getInsertQuery(String table, ContentValues initialValues) {
		String nullColumnHack = null;

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT");
		sql.append(" INTO ");
		sql.append(table);
		sql.append('(');

		Object[] bindArgs = null;
		int size = (initialValues != null && initialValues.size() > 0)
				? initialValues.size() : 0;
		if (size > 0) {
			bindArgs = new Object[size];
			int i = 0;
			for (String colName : initialValues.keySet()) {
				sql.append((i > 0) ? "," : "");
				sql.append(colName);
				bindArgs[i++] = initialValues.get(colName);
			}
			sql.append(')');
			sql.append(" VALUES (");
			for (i = 0; i < size; i++) {
				sql.append((i > 0) ? ",?" : "?");
			}
		} else {
			sql.append(nullColumnHack + ") VALUES (NULL");
		}
		sql.append(')');

		return sql.toString();
	}





//	ArrayList<File> mediaFiles = new ArrayList<File>();
//
//	public ArrayList<File> getMediaFiles() {
//		return mediaFiles;
//	}
//
//	public void setMediaFiles(ArrayList<File> mediaFiles) {
//		this.mediaFiles = mediaFiles;
//	}
//	
//	public String getLocalFilePath(String fileName) {
//		for(File file:mediaFiles){
//			String path = file.toString();
//			if(path.contains(fileName))
//				return path;
//		}
//		return null;
//	}
}
