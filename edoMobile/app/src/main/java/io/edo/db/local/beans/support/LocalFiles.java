package io.edo.db.local.beans.support;

import java.io.Serializable;
import java.util.ArrayList;

import io.edo.db.local.beans.LFile;

public class LocalFiles implements Serializable{
	private static final long serialVersionUID = 1L;
	
	ArrayList<LFile> files = new ArrayList<LFile>();

	public LocalFiles(){}

	public LocalFiles(ArrayList<LFile> files){
		this.files = files;
	}

	public LocalFiles(LFile file){
		this.files.add(file);
	}

	public ArrayList<LFile> getFiles(){
		return files;
	}

}
