package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import io.edo.db.local.LocalDBHelper;



public class LUserDAO extends LBaseDAO<LUser,String,ArrayList<LUser>,String> {

	String TABLE_NAME = LocalDBHelper.TABLE_USER;

	private String[] tableKeys = {LocalDBHelper.KEY_ID, 
			LocalDBHelper.KEY_LOGINEMAIL, 
			LocalDBHelper.KEY_LOGINCREDENTIALS,
			LocalDBHelper.KEY_STATUS,
			LocalDBHelper.KEY_CONTACTID,
			
			LocalDBHelper.KEY_LOGINDATE,
			LocalDBHelper.KEY_LOGINSTATUS,
			LocalDBHelper.KEY_DEVICEID,
			LocalDBHelper.KEY_NAME,
			LocalDBHelper.KEY_SURNAME,
			LocalDBHelper.KEY_LASTTOKENUPDATE,
			LocalDBHelper.KEY_NOTIFICATIONSTATUS};

	public LUserDAO(Context context) {
		super(context);
	}


	@Override
	public ContentValues createContentValues(LUser user) {

		ContentValues values = new ContentValues();
		if(user.getId()!=null)values.put( LocalDBHelper.KEY_ID, user.getId() );
		if(user.getLoginEmail()!=null)values.put( LocalDBHelper.KEY_LOGINEMAIL, user.getLoginEmail() );
		if(user.getEdoToken()!=null)values.put( LocalDBHelper.KEY_LOGINCREDENTIALS, user.getEdoToken() );
		if(user.getStatus()!=null)values.put( LocalDBHelper.KEY_STATUS, user.getStatus() );
		if(user.getContactId()!=null)values.put( LocalDBHelper.KEY_CONTACTID, user.getContactId() );
		
		if(user.getLoginDate()!=null)values.put( LocalDBHelper.KEY_LOGINDATE, user.getLoginDate() );
		if(Integer.valueOf(user.getLoginStatus())!=null)values.put( LocalDBHelper.KEY_LOGINSTATUS, user.getLoginStatus() );
		if(user.getDeviceId()!=null)values.put( LocalDBHelper.KEY_DEVICEID, user.getDeviceId() );
		if(user.getName()!=null)values.put( LocalDBHelper.KEY_NAME, user.getName() );
		if(user.getSurname()!=null)values.put( LocalDBHelper.KEY_SURNAME, user.getSurname() );
		if(user.getLastTokenUpdate()!=null)values.put( LocalDBHelper.KEY_LASTTOKENUPDATE, user.getLastTokenUpdate() );
		if(user.getNotificationStatus()!=null)values.put( LocalDBHelper.KEY_NOTIFICATIONSTATUS, user.getNotificationStatus() );
		return values;
	}

	@Override
	public synchronized boolean insert(LUser user) {
		ContentValues initialValues = createContentValues(user);
		long lid = db.insert(TABLE_NAME, initialValues);
		return lid>0;
	}

	@Override
	public synchronized boolean insertFromWebServer(LUser user) {

        if(insert(user)) {
            LContactDAO contactDAO = new LContactDAO(context);
            LContact contact = user.getUserContact();
            return contactDAO.insertFromWebServer(contact);
        }

		return false;
	}

	@Override
	public synchronized boolean update(LUser user) {
        ContentValues values = createContentValues(user);
        boolean updated = updateUserValues(user.getId(), values);
        return updated;
	}

    public synchronized boolean updateLoginStatus(String id, int loginStatus){
        ContentValues values = new ContentValues();
        values.put(LocalDBHelper.KEY_LOGINSTATUS, loginStatus);
        return updateUserValues(id, values);
    }

    public synchronized boolean updateEdoToken(String id, String edoToken){
        ContentValues values = new ContentValues();
        values.put( LocalDBHelper.KEY_LOGINCREDENTIALS, edoToken );
        values.put( LocalDBHelper.KEY_LASTTOKENUPDATE, System.currentTimeMillis() );
        return updateUserValues(id, values);
    }

    public synchronized boolean updateNotificationStatus(String id, String notificationStatus){
        ContentValues values = new ContentValues();
        values.put( LocalDBHelper.KEY_NOTIFICATIONSTATUS, notificationStatus );
        return updateUserValues(id, values);
    }

    private synchronized boolean updateUserValues(String id, ContentValues updatedValues){
        String whereClause = LocalDBHelper.KEY_ID+" = '"+ id +"'";
        boolean updated = db.update(TABLE_NAME, updatedValues, whereClause);
        return updated;
    }

	@Override
	public synchronized boolean delete(LUser user) {
        String whereClause = LocalDBHelper.KEY_ID+" == '"+ user.getId() + "'";
        boolean deleted =  db.delete(TABLE_NAME, whereClause);
        return deleted;
	}

	@Override
	public synchronized LUser get(String id) {
        String whereClause = LocalDBHelper.KEY_ID+" == '"+ id + "'";
        Cursor cursor = db.select(TABLE_NAME, tableKeys, whereClause, null);
        LUser user = null;
        try {
            if(cursor.moveToNext()){
                user = new LUser(cursor);
            }
        } finally {
            if(cursor != null)
                cursor.close();
        }
        return user;
	}

    public synchronized LUser getPrincipalUser() {
        Cursor cursor = db.selectAll(TABLE_NAME, tableKeys);
        LUser user = null;
        try {
            if(cursor.moveToNext()){
                user = new LUser(cursor);
            }
        } finally {
            if(cursor != null)
                cursor.close();
        }
        return user;
    }


	@Override
	public synchronized ArrayList<LUser> getList() {
        Cursor cursor = db.selectAll(TABLE_NAME, tableKeys);
        ArrayList<LUser> users = new ArrayList<LUser>();
        try {
            while (cursor.moveToNext()){
                LUser user = new LUser(cursor);
                users.add(user);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }
        return users;
	}

    @Override
    public ArrayList<LUser> getListBy(String s) {
        return null;
    }


}
