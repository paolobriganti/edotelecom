package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;

import io.edo.db.local.LocalDBManager;
import io.edo.edomobile.ThisApp;

/**
 * Created by PaoloBriganti on 19/06/15.
 */
public abstract class LBaseDAO<Resource, ResourceId, List, Filter>{
    public ThisApp app;
    public Context context;
    public LocalDBManager db;

    public LBaseDAO(Context context){
        this.context = context;
        this.app = ThisApp.getInstance(context);
        this.db = app.getDB();
    }

    public abstract ContentValues createContentValues(Resource resource);
    public abstract boolean insert(Resource resource);
    public abstract boolean insertFromWebServer(Resource resource);
    public abstract boolean update(Resource resource);
    public abstract boolean delete(Resource resource);
    public abstract Resource get(ResourceId resourceId);
    public boolean exists(ResourceId resourceId){
        if(resourceId==null)
            return false;
        return get(resourceId)!=null;
    }
    public abstract List getList();
    public abstract List getListBy(Filter filter);
}
