package io.edo.db.global.beans;

import android.content.Context;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.web.beans.WEntity;
import io.edo.db.web.beans.WMessage;
import io.edo.db.web.beans.WMessageDAO;

public class MessageDAO extends BaseDAO{
    LActivityDAO lActivityDAO;

    public MessageDAO(Context context) {
        super(context);
        lActivityDAO = new LActivityDAO(context);
    }

    @Override
    public LActivityDAO getLDAO() {
        return lActivityDAO;
    }

    public WEntity sendMessage(LActivity aMessage){

        WEntity entity = WMessageDAO.sendMessage(context, new WMessage(aMessage));

        if(entity!=null){
            aMessage.setOldActivityId(aMessage.getId());
            aMessage.setId(entity.act.getId());
            aMessage.setStatus(LActivity.STATUS_COMPLETE);
//            lActivityDAO.insertFromWebServer(aMessage);
            entity.act = aMessage;
        }else{
            entity = new WEntity();
            aMessage.setOldActivityId(aMessage.getId());
            aMessage.setStatus(LActivity.STATUS_ERROR);
            entity.act = aMessage;
        }

        return entity;
    }

}
