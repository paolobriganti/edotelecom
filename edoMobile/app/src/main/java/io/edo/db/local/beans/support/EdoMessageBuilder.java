package io.edo.db.local.beans.support;

import android.content.Context;

import com.paolobriganti.android.VU;
import com.paolobriganti.utils.JavaUtils;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class EdoMessageBuilder {





	public static LActivity setMessagesOfResourceFileToActivity(Context c, LActivity a, int numOccurs){
		boolean isFromCurrentUser = a.isFromCurrentEdoUser(c);
		boolean isPersonalContainer = a.isPersonalContainer(c);
		String resourceName = a.getResourceName()!=null? a.getResourceName():c.getResources().getString(R.string.file);
		String containerName = a.getContainerName();
		String senderName = a.getSenderName();
		String folderName = a.getParentName();
		boolean isGroup = a.isGroupContainer();
		LFile file = a.getFileResource(c);


		if(containerName == null) {
			eLog.v("EdoActivityMan", "RESOURCE FILE MESSAGE ERROR");
			eLog.w("EdoActivityMan", "event " + a.getEvent());
			eLog.w("EdoActivityMan", "resourceName " + resourceName);
			eLog.w("EdoActivityMan", "containerName "+containerName);
			eLog.w("EdoActivityMan", "senderName "+senderName);
			eLog.w("EdoActivityMan", "folderName "+folderName);
			return a;
		}


		String shortDesc = null;
		String longDesc = null;
		String message = null;

		switch (a.getEvent()) {
			case LActivity.EVENT_UPLOAD_TO_CLOUD:
				return a;

			case LActivity.EVENT_INSERT:



				if(isPersonalContainer) {
					if (file != null && (file.isFolder() || file.isSnippet())) {
						longDesc = c.getResources().getString(R.string.youCreatedFileName)
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
								);
					}else{
						longDesc = c.getResources().getString(R.string.youUploadedFileName)
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
								);
					}

					shortDesc = longDesc;
					message = shortDesc;
				}else if(isFromCurrentUser){
					if (file != null && (file.isFolder() || file.isSnippet())) {
						longDesc = c.getResources().getString(R.string.youCreatedFileName)
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
								);
					}else {
						longDesc = c.getResources().getString(R.string.youSharedFileName)
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
								);
					}
					shortDesc = longDesc;
					message = shortDesc;
				}else{

					if(JavaUtils.isEmpty(folderName) || folderName.equalsIgnoreCase("default")) {
						if (file != null && (file.isFolder() || file.isSnippet())) {
							shortDesc = c.getResources().getString(R.string.contactNameCreatedFileName)
									.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
									.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

							message = c.getResources().getString(R.string.contactNameCreatedFileName)
									.replace(Constants.PH_CONTACTNAME, "")
									.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
						}else{
							shortDesc = c.getResources().getString(R.string.contactNameSharedFileName)
									.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
									.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

							message = c.getResources().getString(R.string.contactNameSharedFileName)
									.replace(Constants.PH_CONTACTNAME, "")
									.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
						}

					}else{
						shortDesc = c.getResources().getString(R.string.contactNameSharedFileNameInFolderName)
								.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
								.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));

						message = c.getResources().getString(R.string.contactNameSharedFileNameInFolderName)
								.replace(Constants.PH_CONTACTNAME, "")
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
								.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));
					}

					if(isGroup) {
						if (file != null && (file.isFolder() || file.isSnippet())) {
							longDesc = c.getResources().getString(R.string.contactNameCreatedFileNameInGroupName)
									.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
									.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
									.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(containerName));

						}else {
							longDesc = c.getResources().getString(R.string.contactNameSharedFileNameInGroupName)
									.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
									.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
									.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(containerName));
						}
					}else{
						longDesc = shortDesc;
					}

				}

//				if(file!=null && file.isSnippet() && JavaUtils.isNotEmpty(file.getContent())){
//					message = VU.getBoldTextString(file.getName())+"\n"+file.getContent();
//					message = message.replace("\n","<br>");
//				}


				break;


			case LActivity.EVENT_INSERT_SPACE:
				if(isPersonalContainer){
					eLog.d("EdoActivityMan", "MESSAGE | isPersonalContainer | is def:" +a.isDefaultSpace());

					if(a.isDefaultSpace()){
						return a;
					}

					longDesc = c.getResources().getString(R.string.youCreatedFileName)
							.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
							);
					shortDesc = longDesc;
					message = shortDesc;



				}else if(isFromCurrentUser){
					eLog.d("EdoActivityMan", "MESSAGE | isFromCurrentUser | is def:" +a.isDefaultSpace());

					if(a.isDefaultSpace()){
						return a;
					}

					longDesc = c.getResources().getString(R.string.youCreatedFileName)
							.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
							);
					shortDesc = longDesc;
					message = shortDesc;

				}else{
					if(a.isDefaultSpace()){
						shortDesc = c.getResources().getString(R.string.contactNameAddedYouOnEdo)
								.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"");

						message = c.getResources().getString(R.string.contactNameAddedYouOnEdo)
								.replace(Constants.PH_CONTACTNAME, "");
					}else{
						shortDesc = c.getResources().getString(R.string.contactNameCreatedFileName)
								.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

						message = c.getResources().getString(R.string.contactNameCreatedFileName)
								.replace(Constants.PH_CONTACTNAME, "")
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
					}

					if(isGroup) {
						eLog.d("EdoActivityMan", "MESSAGE | isGroup | is def:" +a.isDefaultSpace());

						if(a.isDefaultSpace()){
							return a;
						}


						longDesc = c.getResources().getString(R.string.contactNameCreatedFileNameInGroupName)
								.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
								.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(containerName));

					}else{
						longDesc = shortDesc;
					}

					eLog.d("EdoActivityMan", "MESSAGE | isContact | is def:" +a.isDefaultSpace());
					eLog.d("EdoActivityMan", "MESSAGE:" +shortDesc);

				}
				break;


			case LActivity.EVENT_UPDATE:

				if(a.getSubEvent()!=LActivity.VALUE_ERROR) {
					switch (a.getSubEvent()) {
						case LActivity.SUB_EVENT_RENAMED:

							String originalName = a.getOriginalName()!=null?a.getOriginalName():"";

							if(isFromCurrentUser){
								longDesc = c.getResources().getString(R.string.youRenamedOriginalNameInFileName)
										.replace(Constants.PH_ORIGINALNAME, VU.getBoldTextString(originalName))
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
								shortDesc = longDesc;
								message = shortDesc;
							}else{

								longDesc = c.getResources().getString(R.string.contactNameRenamedOriginalNameInFileName)
										.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
										.replace(Constants.PH_ORIGINALNAME, VU.getBoldTextString(originalName))
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

								shortDesc = c.getResources().getString(R.string.contactNameRenamedOriginalNameInFileName)
										.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
										.replace(Constants.PH_ORIGINALNAME, VU.getBoldTextString(originalName))
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

								message = c.getResources().getString(R.string.contactNameRenamedOriginalNameInFileName)
										.replace(Constants.PH_CONTACTNAME, "")
										.replace(Constants.PH_ORIGINALNAME, VU.getBoldTextString(originalName))
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
							}

							break;

						case LActivity.SUB_EVENT_MOVED:


							if(isFromCurrentUser){
								if(numOccurs>1){
									longDesc = c.getResources().getString(R.string.youMovedNUMFilesInFolderName)
											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(""+numOccurs)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));
								}else{
									longDesc = c.getResources().getString(R.string.youMovedFileNameInFolderName)
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));
								}

								shortDesc = longDesc;
								message = shortDesc;
							}else{
								if(numOccurs>1) {

									longDesc = c.getResources().getString(R.string.contactNameMovedNUMFilesInFolderName)
											.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(""+numOccurs)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));

									shortDesc = c.getResources().getString(R.string.contactNameMovedNUMFilesInFolderName)
											.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(""+numOccurs)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));

									message = c.getResources().getString(R.string.contactNameMovedNUMFilesInFolderName)
											.replace(Constants.PH_CONTACTNAME, "")
											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(""+numOccurs)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));

								}else{
									longDesc = c.getResources().getString(R.string.contactNameMovedFileNameInFolderName)
											.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));

									shortDesc = c.getResources().getString(R.string.contactNameMovedFileNameInFolderName)
											.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));

									message = c.getResources().getString(R.string.contactNameMovedFileNameInFolderName)
											.replace(Constants.PH_CONTACTNAME, "")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));
								}
							}

							break;

						case LActivity.SUB_EVENT_STARRED:

							if(isFromCurrentUser){
								longDesc = c.getResources().getString(R.string.youMarkAsFavoriteFileName)
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
								shortDesc = longDesc;
								message = shortDesc;
							}else{

								longDesc = c.getResources().getString(R.string.contactNameMarkAsFavoriteFileName)
										.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

								shortDesc = c.getResources().getString(R.string.contactNameMarkAsFavoriteFileName)
										.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

								message = c.getResources().getString(R.string.contactNameMarkAsFavoriteFileName)
										.replace(Constants.PH_CONTACTNAME, "")
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
							}

							break;

						case LActivity.SUB_EVENT_UNSTARRED:

							if(isFromCurrentUser){
								longDesc = c.getResources().getString(R.string.youRemovedFromFavoritesFileName)
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
								shortDesc = longDesc;
								message = shortDesc;
							}else{

								longDesc = c.getResources().getString(R.string.contactNameRemovedFromFavoritesFileName)
										.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

								shortDesc = c.getResources().getString(R.string.contactNameRemovedFromFavoritesFileName)
										.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

								message = c.getResources().getString(R.string.contactNameRemovedFromFavoritesFileName)
										.replace(Constants.PH_CONTACTNAME, "")
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
							}

							break;

						case LActivity.SUB_EVENT_CONTENT_COMMITTED:
							if(isPersonalContainer){
								longDesc = c.getResources().getString(R.string.youUploadedFileName)
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
										);
								shortDesc = longDesc;
								message = shortDesc;
							}else if(isFromCurrentUser){
								longDesc = c.getResources().getString(R.string.youSharedFileName)
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
										);
								shortDesc = longDesc;
								message = shortDesc;
							}else{

								if(JavaUtils.isEmpty(folderName)) {

									shortDesc = c.getResources().getString(R.string.contactNameSharedFileName)
											.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

									message = c.getResources().getString(R.string.contactNameSharedFileName)
											.replace(Constants.PH_CONTACTNAME, "")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
								}else{
									shortDesc = c.getResources().getString(R.string.contactNameSharedFileNameInFolderName)
											.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));

									message = c.getResources().getString(R.string.contactNameSharedFileNameInFolderName)
											.replace(Constants.PH_CONTACTNAME, "")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));
								}

								if(isGroup) {

									longDesc = c.getResources().getString(R.string.contactNameSharedFileNameInGroupName)
											.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(containerName));

								}else{
									longDesc = shortDesc;
								}

							}

//							if(file!=null && file.isSnippet() && JavaUtils.isNotEmpty(file.getContent())) {
//								message = VU.getBoldTextString(file.getName()) + "\n" + file.getContent();
//								message = message.replace("\n", "<br>");
//							}

							break;

						case LActivity.SUB_EVENT_MOVE_OUT:
							if(isFromCurrentUser){
								longDesc = c.getResources().getString(R.string.youRemovedFileName)
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
								shortDesc = longDesc;
								message = shortDesc;
							}else{

								longDesc = c.getResources().getString(R.string.contactNameRemovedFileName)
										.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

								shortDesc = c.getResources().getString(R.string.contactNameRemovedFileName)
										.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

								message = c.getResources().getString(R.string.contactNameRemovedFileName)
										.replace(Constants.PH_CONTACTNAME, "")
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
							}
							break;

						case LActivity.SUB_EVENT_MOVE_IN:
							if(isPersonalContainer){
								longDesc = c.getResources().getString(R.string.youUploadedFileName)
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
										);
								shortDesc = longDesc;
								message = shortDesc;
							}else if(isFromCurrentUser){
								longDesc = c.getResources().getString(R.string.youSharedFileName)
										.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName))
										);
								shortDesc = longDesc;
								message = shortDesc;
							}else{

								if(JavaUtils.isEmpty(folderName)) {

									shortDesc = c.getResources().getString(R.string.contactNameSharedFileName)
											.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

									message = c.getResources().getString(R.string.contactNameSharedFileName)
											.replace(Constants.PH_CONTACTNAME, "")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
								}else{
									shortDesc = c.getResources().getString(R.string.contactNameSharedFileNameInFolderName)
											.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));

									message = c.getResources().getString(R.string.contactNameSharedFileNameInFolderName)
											.replace(Constants.PH_CONTACTNAME, "")
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_FOLDERNAME, VU.getBoldTextString(folderName));
								}

								if(isGroup) {

									longDesc = c.getResources().getString(R.string.contactNameSharedFileNameInGroupName)
											.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
											.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)))
											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(containerName));

								}else{
									longDesc = shortDesc;
								}

							}
							break;

					}
				}

				break;

			case LActivity.EVENT_DELETE:

				if(isFromCurrentUser){
					longDesc = c.getResources().getString(R.string.youRemovedFileName)
							.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
					shortDesc = longDesc;
					message = shortDesc;
				}else{

					longDesc = c.getResources().getString(R.string.contactNameRemovedFileName)
							.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(senderName))
							.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

					shortDesc = c.getResources().getString(R.string.contactNameRemovedFileName)
							.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
							.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

					message = c.getResources().getString(R.string.contactNameRemovedFileName)
							.replace(Constants.PH_CONTACTNAME, "")
							.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
				}

				break;
			case LActivity.EVENT_COMMENT_INSERT:
			case LActivity.EVENT_COMMENT_UPDATED:

				int commentsCount = file!=null? file.getCommentsCount():1;

				if(commentsCount>1) {

					longDesc = c.getResources().getString(R.string.NUMCommentsOnFileName)
							.replace(Constants.PH_NUM, VU.getBoldTextString(""+commentsCount))
							.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

					shortDesc = longDesc;

					message = shortDesc;

				}else{
					if (isFromCurrentUser) {
						longDesc = c.getResources().getString(R.string.youCommentedFileName)
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
						shortDesc = longDesc;
					} else {


						longDesc = c.getResources().getString(R.string.contactNameCommentedFileName)
								.replace(Constants.PH_CONTACTNAME, isGroup?VU.getBoldTextString(senderName):"")
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));

						shortDesc = c.getResources().getString(R.string.contactNameCommentedFileName)
								.replace(Constants.PH_CONTACTNAME, "")
								.replace(Constants.PH_FILENAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(resourceName)));
					}


				}

				if (file != null && file.getComments() != null && file.getComments().length > 0) {
					message = file.getComments()[file.getComments().length - 1].message;
				}

				break;

		}


		if(longDesc!=null)
			a.setLongDescription(longDesc);
		if(shortDesc!=null)
			a.setShortDescription(shortDesc);
		if(message!=null)
			a.setMessage(message);

		return a;
	}







	public static LActivity setMessagesOfChatToActivity(Context c, LActivity a){
		boolean isFromCurrentUser = a.isFromCurrentEdoUser(c);
		boolean isPersonalContainer = a.isPersonalContainer(c);
		String containerName = a.getContainerName();
		String senderName = a.getSenderName();
		boolean isGroup = a.isGroupContainer();

		if(containerName == null || senderName == null || a.getMessage() == null) {
			eLog.v("EdoActivityMan", "CHAT MESSAGE ERROR");
			eLog.w("EdoActivityMan", "containerName "+containerName);
			eLog.w("EdoActivityMan", "senderName "+senderName);
			eLog.w("EdoActivityMan", "message "+a.getMessage());
			return a;
		}

		String longDesc = null;
		String shortDesc = null;

		if(isPersonalContainer || isFromCurrentUser){
			longDesc = a.getMessage();
			shortDesc = a.getMessage();
		}else {
			if (!isGroup) {
				longDesc = VU.getBoldTextString(senderName) + ": " + a.getMessage();
				shortDesc = a.getMessage();
			} else {
				longDesc = VU.getBoldTextString(senderName + "@" + containerName) + ": " + a.getMessage();
				shortDesc = VU.getBoldTextString(senderName) + ": " + a.getMessage();
			}
		}

		a.setLongDescription(longDesc);
		a.setShortDescription(shortDesc);

		return a;
	}


	public static LActivity setMessagesOfResourceGroupToActivity(Context c, LActivity a, int numOccurs){
		boolean isFromCurrentUser = a.isFromCurrentEdoUser(c);
		String groupName = a.getResourceName();
		String adminName = a.getSenderName();
		int membersCount = 0;

		LContact group = a.getContactResource(c);
		if(group!=null) {
			membersCount = group.getGroupMembersCount();
		}

		if(groupName == null) {
			eLog.v("EdoActivityMan", "RESOURCE GROUP MESSAGE ERROR");
			eLog.w("EdoActivityMan", "groupId " + a.getResourceId());
			eLog.w("EdoActivityMan", "groupName "+groupName);
			eLog.w("EdoActivityMan", "adminName "+adminName);
			return a;
		}

		String shortDesc = null;
		String longDesc = null;
		String message = null;

		switch (a.getEvent()) {
			case LActivity.EVENT_INSERT:

				if(isFromCurrentUser){
//					membersCount = membersCount;

					longDesc = c.getResources().getString(R.string.youCreatedGroupNameGroupWithNUMOtherMember)
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
							.replace(Constants.PH_NUM, VU.getBoldTextString(membersCount > 0 ? ("" + membersCount) : " "))
					;
					shortDesc = longDesc;
					message = shortDesc;

				}else{

					membersCount = membersCount-1;

					longDesc = c.getResources().getString(R.string.adminNameCreatedGroupNameGroupWithYouAndNUMOtherMember)
							.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
							.replace(Constants.PH_NUM, VU.getBoldTextString(membersCount > 0 ? ("" + membersCount) : ""));

					shortDesc = longDesc;

					message = c.getResources().getString(R.string.adminNameCreatedGroupNameGroupWithYouAndNUMOtherMember)
							.replace(Constants.PH_ADMINNAME, "")
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
							.replace(Constants.PH_NUM, VU.getBoldTextString(membersCount > 0 ? ("" + membersCount) : ""));

				}
				break;


			case LActivity.EVENT_UPDATE:

				if(a.getSubEvent()!=LActivity.VALUE_ERROR) {
					switch (a.getSubEvent()) {
						case LActivity.SUB_EVENT_RENAMED:

							String originalName = a.getOriginalName()!=null?a.getOriginalName():"";

							if(isFromCurrentUser){
								longDesc = c.getResources().getString(R.string.youChangedOriginalNameInGroupName)
										.replace(Constants.PH_ORIGINALNAME, VU.getBoldTextString(originalName))
										.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));
								shortDesc = longDesc;
								message = shortDesc;
							}else{

								longDesc = c.getResources().getString(R.string.adminNameChangedOriginalNameInGroupName)
										.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
										.replace(Constants.PH_ORIGINALNAME, VU.getBoldTextString(originalName))
										.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));

								shortDesc = longDesc;

								message = c.getResources().getString(R.string.adminNameChangedOriginalNameInGroupName)
										.replace(Constants.PH_ADMINNAME, "")
										.replace(Constants.PH_ORIGINALNAME, VU.getBoldTextString(originalName))
										.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));
							}

							break;

						case LActivity.SUB_EVENT_ICON_MODIFIED:

							if(isFromCurrentUser){
								longDesc = c.getResources().getString(R.string.youChangedIconOfGroupName)
										.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));
								shortDesc = c.getResources().getString(R.string.youChangedGroupIcon);
								message = shortDesc;
							}else{

								longDesc = c.getResources().getString(R.string.adminNameChangedIconOfGroupName)
										.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
										.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));

								shortDesc = c.getResources().getString(R.string.adminNameChangedGroupIcon)
										.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName));

								message = c.getResources().getString(R.string.adminNameChangedGroupIcon)
										.replace(Constants.PH_ADMINNAME, "");
							}


							break;

						case LActivity.SUB_EVENT_PENDING_USER_ADDED:

//							if(isFromCurrentUser){
//								if(numOccurs>1){
//
//									longDesc = c.getResources().getString(R.string.youAddedNUMNewMembersToGroupName)
//											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(""+numOccurs)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//									shortDesc = longDesc;
//									message = shortDesc;
//
//								}else{
//									longDesc = c.getResources().getString(R.string.youAddedContactNameToGroupName)
//											.replace(Constants.PH_CONTACTNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(memberName)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//									shortDesc = c.getResources().getString(R.string.youChangedGroupIcon);
//									message = shortDesc;
//								}
//							}else{
//
//								if(numOccurs>1){
//									longDesc = c.getResources().getString(R.string.adminNameAddedNUMNewMembersToGroupName)
//											.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
//											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString("" + numOccurs)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//
//									shortDesc = longDesc;
//
//									message = c.getResources().getString(R.string.adminNameAddedNUMNewMembersToGroupName)
//											.replace(Constants.PH_ADMINNAME, "")
//											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(""+numOccurs)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//								}else{
//									longDesc = c.getResources().getString(R.string.adminNameAddedContactNameToGroupName)
//											.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
//											.replace(Constants.PH_CONTACTNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//
//									shortDesc = longDesc;
//
//									message = c.getResources().getString(R.string.adminNameAddedContactNameToGroupName)
//											.replace(Constants.PH_CONTACTNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//								}
//
//							}
//
//							break;

						case LActivity.SUB_EVENT_PENDING_USER_REMOVED:

//							if(isFromCurrentUser){
//								if(numOccurs>1){
//
//									longDesc = c.getResources().getString(R.string.youRemovedNUMMembersFromGroupName)
//											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(""+numOccurs)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//									shortDesc = longDesc;
//									message = shortDesc;
//
//								}else{
//									longDesc = c.getResources().getString(R.string.youRemovedContactNameFromGroupName)
//											.replace(Constants.PH_CONTACTNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//									shortDesc = c.getResources().getString(R.string.youChangedGroupIcon);
//									message = shortDesc;
//								}
//							}else{
//
//								if(numOccurs>1){
//									longDesc = c.getResources().getString(R.string.adminNameRemovedNUMMembersFromGroupName)
//											.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
//											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString("" + numOccurs)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//
//									shortDesc = longDesc;
//
//									message = c.getResources().getString(R.string.adminNameRemovedNUMMembersFromGroupName)
//											.replace(Constants.PH_ADMINNAME, "")
//											.replace(Constants.PH_NUM, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(""+numOccurs)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//								}else{
//									longDesc = c.getResources().getString(R.string.adminNameRemovedContactNameFromGroupName)
//											.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
//											.replace(Constants.PH_CONTACTNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//
//									shortDesc = longDesc;
//
//									message = c.getResources().getString(R.string.adminNameRemovedContactNameFromGroupName)
//											.replace(Constants.PH_CONTACTNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
//											.replace(Constants.PH_GROUPNAME, VU.getBoldTextString(groupName));
//								}
//
//							}

							break;


					}
				}

				break;

			case LActivity.EVENT_DELETE:


				break;


		}

		a.setLongDescription(longDesc);
		a.setShortDescription(shortDesc);
		a.setMessage(message);

		return a;
	}


	public static LActivity setMessagesOfResourceMemberToActivity(Context c, LActivity a){
		boolean isCurrentUserTheSender = a.getSenderId().equals(ThisApp.getInstance(c).getEdoUserId());
		boolean isCurrentUserTheResource = a.getResourceId().equals(ThisApp.getInstance(c).getEdoUserId());
		String groupName = a.getContainerName();
		String adminName = a.getSenderName();
		String memberName = a.getResourceName();
		a.loadContainerSenderFromDB(c, false);
		int membersCount = 0;
		if(a.getContainer()!=null) {
			LContact group = (LContact)a.getContainer();
			membersCount = group.getGroupMembersCount();
		}

		if(groupName == null) {
			eLog.v("EdoActivityMan", "RESOURCE GROUP MESSAGE ERROR");
			eLog.w("EdoActivityMan", "groupName " + groupName);
			eLog.w("EdoActivityMan", "adminName " + adminName);
			return a;
		}

		String shortDesc = null;
		String longDesc = null;
		String message = null;


		switch (a.getEvent()) {
			case LActivity.EVENT_JOIN:
				if(isCurrentUserTheResource){
					membersCount = membersCount-2;

					longDesc = c.getResources().getString(R.string.adminNameCreatedGroupNameGroupWithYouAndNUMOtherMember)
							.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
							.replace(Constants.PH_NUM, VU.getBoldTextString(membersCount > 0 ? ("" + membersCount) : ""));

					shortDesc = longDesc;

					message = c.getResources().getString(R.string.adminNameCreatedGroupNameGroupWithYouAndNUMOtherMember)
							.replace(Constants.PH_ADMINNAME, "")
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)))
							.replace(Constants.PH_NUM, VU.getBoldTextString(membersCount > 0 ? ("" + membersCount) : ""));



				}else{
					a.setSenderName(memberName);

					longDesc = c.getResources().getString(R.string.contactNameJoinedGroupName)
							.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(memberName))
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));

					shortDesc = longDesc;

					message = c.getResources().getString(R.string.contactNameJoinedGroupName)
							.replace(Constants.PH_CONTACTNAME, "")
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));

				}
				break;

			case LActivity.EVENT_LEAVE:
				if(isCurrentUserTheResource && !isCurrentUserTheSender){
					longDesc = c.getResources().getString(R.string.adminNameRemovedYouFromGroupName)
							.replace(Constants.PH_ADMINNAME, VU.getBoldTextString(adminName))
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName))
							);
					shortDesc = longDesc;
					message = c.getResources().getString(R.string.adminNameRemovedYouFromGroupName)
							.replace(Constants.PH_ADMINNAME, "")
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName))
							);
				}else{

					a.setSenderName(memberName);

					longDesc = c.getResources().getString(R.string.contactNameLeftGroupName)
							.replace(Constants.PH_CONTACTNAME, VU.getBoldTextString(memberName))
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));

					shortDesc = longDesc;

					message = c.getResources().getString(R.string.contactNameLeftGroupName)
							.replace(Constants.PH_CONTACTNAME, "")
							.replace(Constants.PH_GROUPNAME, VU.getColoredTextString(c, R.color.edo_blue_file, VU.getBoldTextString(groupName)));

				}
				break;
		}

		a.setLongDescription(longDesc);
		a.setShortDescription(shortDesc);
		a.setMessage(message);

		return a;
	}


	public static LActivity setMessagesOfResourceBlancSheet(Context c, LActivity a){

		String containerName = a.getContainerName();

		String shortDesc = null;
		String longDesc = null;
		String message = null;

		if(a.isPersonal()){
			longDesc = c.getResources().getString(R.string.blankSheetTimelinePersonal)
					.replace(Constants.PH_USERNAME, ThisApp.getInstance(c).getUserName());
			shortDesc = longDesc;
			message = shortDesc;
		}else if(a.isGroupContainer()){
			longDesc = c.getResources().getString(R.string.blankSheetTimelineGroups)
					.replace(Constants.PH_GROUPNAME, containerName);

			shortDesc = longDesc;

			message = shortDesc;

		}else{
			longDesc = c.getResources().getString(R.string.blankSheetTimelineContacts)
					.replace(Constants.PH_CONTACTNAME, containerName);

			shortDesc = longDesc;

			message = shortDesc;
		}

		a.setLongDescription(longDesc);
		a.setShortDescription(shortDesc);
		a.setMessage(message);

		return a;
	}

	public static String getMessageBlancSheetOfActiveContactList(Context c){

		return c.getResources().getString(R.string.blankSheetActiveContactList);
	}

	public static String getMessageBlancSheetOfSpaces(Context c, EdoContainer container){

		String containerName = container.getName();

		String message = null;

		if(container.isPersonal(c)){
			message = c.getResources().getString(R.string.blankSheetSpacesPersonal);
		}else if(container.isGroup()){
			message = c.getResources().getString(R.string.blankSheetSpacesGroups)
					.replace(Constants.PH_GROUPNAME, containerName);

		}else{
			message = c.getResources().getString(R.string.blankSheetSpacesContacts)
					.replace(Constants.PH_CONTACTNAME, containerName);
		}

		return message;
	}

	public static String getMessageBlancSheetOfFiles(Context c, EdoContainer container){

		String containerName = container.getName();

		String message = null;

//		if(container.isPersonal(c)){
		message = c.getResources().getString(R.string.blankSheetFiles);
//		}else if(container.isGroup()){
//			message = c.getResources().getString(R.string.blankSheetFiles)
//					.replace(Constants.PH_GROUPNAME, containerName);
//
//		}else{
//			message = c.getResources().getString(R.string.blankSheetFiles)
//					.replace(Constants.PH_CONTACTNAME, containerName);
//		}

		return message;
	}

	public static String getMessageBlancSheetOfNotes(Context c, EdoContainer container){

		String containerName = container.getName();

		String message = null;

		if(container.isPersonal(c)){
			message = c.getResources().getString(R.string.blankSheetNotesPersonal);
		}else if(container.isGroup()){
			message = c.getResources().getString(R.string.blankSheetNotesGroups)
					.replace(Constants.PH_GROUPNAME, containerName);

		}else{
			message = c.getResources().getString(R.string.blankSheetNotesContacts)
					.replace(Constants.PH_CONTACTNAME, containerName);
		}

		return message;
	}

	public static String getMessageBlankSheetOfInbox(Context c, EdoContainer container){

		String containerName = container.getName();

		String message = null;

		if(container.isPersonal(c)){
			message = c.getResources().getString(R.string.blankSheetInbox);
		}else if(container.isGroup()){
			message = c.getResources().getString(R.string.blankSheetInboxContainerName)
					.replace(Constants.PH_CONTAINERNAME, containerName);

		}else{
			message = c.getResources().getString(R.string.blankSheetInboxContainerName)
					.replace(Constants.PH_CONTAINERNAME, containerName);
		}

		return message;
	}



}
