package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import io.edo.db.local.LocalDBHelper;
import io.edo.edomobile.ThisApp;


public class LServiceDAO extends LBaseDAO<LService,String,ArrayList<LService>,String> {

	String TABLE_NAME = LocalDBHelper.TABLE_SERVICE;

	private String[] tableKeys = {LocalDBHelper.KEY_ID, 
			LocalDBHelper.KEY_NAME, 
			LocalDBHelper.KEY_EMAIL,
			LocalDBHelper.KEY_ACCESS_TOKEN,
			LocalDBHelper.KEY_CONFIGURATION,
			LocalDBHelper.KEY_STATUS,
			LocalDBHelper.KEY_MDATE,
			LocalDBHelper.KEY_ISUNREAD};

	public LServiceDAO(Context context) {
		super(context);
	}

	@Override
	public ContentValues createContentValues(LService service) {
		ContentValues values = new ContentValues();
		if(service.getId()!=null)values.put( LocalDBHelper.KEY_ID, service.getId() );
		if(service.getName()!=null)values.put( LocalDBHelper.KEY_NAME, service.getName() );
		if(service.getEmail()!=null)values.put( LocalDBHelper.KEY_EMAIL, service.getEmail() );
		if(service.getAccessToken()!=null)values.put( LocalDBHelper.KEY_ACCESS_TOKEN, service.getAccessToken() );
		if(service.getConfiguration()!=null)values.put( LocalDBHelper.KEY_CONFIGURATION, service.getConfiguration() );
		if(service.getStatus()!=null)values.put( LocalDBHelper.KEY_STATUS, service.getStatus() );
		if(service.getmDate()!=null)values.put( LocalDBHelper.KEY_MDATE, service.getmDate() );
		if(service.getIsUnread()!=null)values.put( LocalDBHelper.KEY_ISUNREAD, service.getIsUnread() );
		return values;
	}

	@Override
	public synchronized boolean insert(LService service) {

		ContentValues initialValues = createContentValues(service);
		long lid = db.insert(TABLE_NAME, initialValues);
		return lid>0;
	}

	@Override
	public synchronized boolean insertFromWebServer(LService service) {
		LService oldService = get(service.getId());
		if(oldService!=null && oldService.getmDate()<service.getmDate())
			update(service);
		else if(oldService==null)
			insert(service);

		return true;
	}

	@Override
	public synchronized boolean update(LService service) {
		ContentValues updatedValues = createContentValues(service);
		String whereClause = LocalDBHelper.KEY_ID+" == '"+ service.getId() + "'";
		boolean updated = db.update(TABLE_NAME, updatedValues, whereClause);
		return updated;
	}

	@Override
	public synchronized boolean delete(LService service) {
		String whereClause = LocalDBHelper.KEY_ID+" == '"+ service.getId() + "'";
		boolean deleted =  db.delete(TABLE_NAME, whereClause);
		return deleted;
	}

	@Override
	public synchronized LService get(String serviceId) {
		if(serviceId==null)
			return null;

		Cursor cursor = null;
		LService service = null;
		try {

			String whereClause = LocalDBHelper.KEY_ID+" == ?";
			cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{serviceId});

			if(cursor.moveToNext()){
				service = new LService(cursor);
			}

		} finally {
			if(cursor != null)
				cursor.close();
		}

		return service;
	}

	public synchronized LService getPrincipalService(){

		String userEmail = ThisApp.getInstance(context).getUserEmail();
		if(userEmail!=null)
			return getServiceByEmail(userEmail);

		return null;
	}

	public synchronized LService getServiceByEmail(String email){
		if(email==null)
			return null;


		Cursor cursor = null;
		LService service = null;

		try {
			String whereClause = LocalDBHelper.KEY_EMAIL+" == ?";
			cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{email});

			while(cursor.moveToNext()){
				service = new LService(cursor);
			}


		} finally {
			if(cursor != null)
				cursor.close();
		}


		return service;
	}

	public synchronized LService getServiceByName(String name){
		if(name==null)
			return null;


        ArrayList<LService> services = getListBy(name);
        if(services.size()>0)
		    return services.get(0);

        return null;
	}


	@Override
	public synchronized ArrayList<LService> getList() {
		Cursor cursor = null;
		ArrayList<LService> services = new ArrayList<LService> ();

		try {

			cursor = db.selectAll(TABLE_NAME, tableKeys);
			while(cursor.moveToNext()){
				LService service = new LService(cursor);
				services.add(service);
			}


		} finally {
			if(cursor != null)
				cursor.close();
		}

		return services;
	}

	@Override
	public ArrayList<LService> getListBy(String name) {
		if(name==null)
			return null;


		Cursor cursor = null;
		ArrayList<LService> services = new ArrayList<LService> ();

		try {
			String whereClause = LocalDBHelper.KEY_NAME+" == ?";
			cursor = db.select(TABLE_NAME, tableKeys, whereClause, new String[]{name});

			while(cursor.moveToNext()){
				LService service = new LService(cursor);
				services.add(service);
			}


		} finally {
			if(cursor != null)
				cursor.close();
		}


		return services;
	}


}
