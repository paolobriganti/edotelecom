package io.edo.db.web.beans;

import com.google.gson.Gson;

import java.io.Serializable;

import io.edo.db.local.beans.LActivity;

public class WActivity implements Serializable{

	private String _id;    // ObjectId - univoco, consente di risalire anche al timestamp
	private String c;    // exchange [n,r]notification o realtime
	private String cx;    // context [p,c,g]personal, contact, groups
	private String cid;    // container (category, contact, group) id ObjectId
	private String s;    // sender id (edo user id or contact id)
	private Integer e;    // event/action/type è il tipo di azione [new,updated,desk,...]
	private Integer rt;    // resource type
	private String rid;    // resource id
	private String pid;    // parent id
	private String msg;    // per i messaggi di chat
    //Optional:
	private Integer se;    // sub event
	private String op;    // parent id precedente di un file
	private String on;    // original name
	private String pn;    // parent name
	private String rn;   //Resource name
	private boolean f;   //Is folder
	
	
	
	
	public String getId() {
		return _id;
	}

	public void setId(String _id) {
		this._id = _id;
	}

	public String getExchange() {
		return c;
	}

	public void setExchange(String c) {
		this.c = c;
	}

	public String getContext() {
		return cx;
	}

	public void setContext(String cx) {
		this.cx = cx;
	}

	public String getContainerId() {
		return cid;
	}

	public void setContainerId(String cid) {
		this.cid = cid;
	}

	public String getSenderId() {
		return s;
	}

	public void setSenderId(String s) {
		this.s = s;
	}

	public int getEvent() {
		return e!=null? e: LActivity.VALUE_ERROR;
	}

	public void setEvent(Integer e) {
		this.e = e;
	}

	public int getResourceType() {
		return rt!=null? rt: LActivity.VALUE_ERROR;
	}

	public void setResourceType(Integer rt) {
		this.rt = rt;
	}

	public String getResourceId() {
		return rid;
	}

	public void setResourceId(String rid) {
		this.rid = rid;
	}
	
	public String getParentId() {
		return pid;
	}

	public void setParentId(String parentId) {
		this.pid = parentId;
	}

	public String getMessage() {
		return msg;
	}

	public void setMessage(String msg) {
		this.msg = msg;
	}

	public int getSubEvent() {
		return se!=null?se:LActivity.VALUE_ERROR;

	}

	public void setSubEvent(Integer se) {
		this.se = se;
	}

	public String getOriginalParentId() {
		return op;
	}

	public void setOriginalParentId(String op) {
		this.op = op;
	}

	public String getOriginalName() {
		return on;
	}

	public void setOriginalName(String on) {
		this.on = on;
	}

	public String getParentName() {
		return pn;
	}

	public void setParentName(String parentName) {
		this.pn = parentName;
	}

	public String getResourceName() {
		return rn;
	}

	public void setResourceName(String resourceName) {
		this.rn = resourceName;
	}

	public boolean isFolder() {
		return f;
	}

	public void setIsFolder(boolean isFolder) {
		this.f = isFolder;
	}
	public void setIsFolder(int isFolder) {
		this.f = isFolder==1;
	}

	public String toJson(){
		return new Gson().toJson(this);
	}




	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//*		EQUAL
	//*****************************************************************************************************************************************************************************************************************************************************************************************************


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WActivity wActivity = (WActivity) o;

		return !(_id != null ? !_id.equals(wActivity._id) : wActivity._id != null);

	}
}
