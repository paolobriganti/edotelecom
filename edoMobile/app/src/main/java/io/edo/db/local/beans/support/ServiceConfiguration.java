package io.edo.db.local.beans.support;

import com.google.gson.Gson;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class ServiceConfiguration {
	public String rootFolder;
	public String personalFolder;
	public String contactsFolder;

	public String toJson(){
		return new Gson().toJson(this);
	}

	public static ServiceConfiguration fromJson(String json){

		try{
			return new Gson().fromJson(json, ServiceConfiguration.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "GoogleConfiguration:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "GoogleConfiguration JSON:\n"+json);
			return null;
		}
	}

}
