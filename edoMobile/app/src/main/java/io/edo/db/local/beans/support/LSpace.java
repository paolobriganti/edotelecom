package io.edo.db.local.beans.support;

import android.content.Context;
import android.database.Cursor;

import com.paolobriganti.utils.JavaUtils;

import java.io.Serializable;
import java.util.ArrayList;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.LServiceDAO;
import io.edo.edomobile.R;
import io.edo.utilities.ObjectId;

public class LSpace extends LFile implements Serializable{
	private static final long serialVersionUID = 1L;

	private LService service;
	private LFile currentFolder;
    private ArrayList<LFile> files = new ArrayList<>();

    public LSpace() {
        ObjectId id = new ObjectId();
        this.setId(id.toString());
        this.setType(FILE_TYPE_EDOTAB);
        this.setmDate(System.currentTimeMillis());
    }

    public LSpace(Cursor cursor) {
        super(cursor);
    }

//    public LSpace newTimelineSpaceWithPrincipalService(Context c, LContact toContact, String spaceName){
//        LSpace space = newSpaceWithPrincipalService(c, toContact, spaceName);
//        return space;
//    }

//    public LSpace newSpaceWithPrincipalService(Context c, LContact toContact, String spaceName){
//        ThisApp app = ThisApp.getInstance(c);
//        LServiceDAO lsDao = new LServiceDAO(c);
//        LService principalService = lsDao.getPrincipalService();
//        return new LSpace(spaceName, principalService.getId(), new String[]{toContact.getPrincipalEmailAddress()},null,toContact.getCloudId(principalService.getId()));
//    }


    public LSpace (Context context, EdoContainer container, LFile file){
        this(context, file.getName(), container, file.getServiceId(), file.getParentId(), file.getParentCloudId());
        this.setId(file.getId());
        this.setThumbnail(file.getThumbnail());
        this.setStatus(file.getStatus());
    }

	public LSpace(Context context, String spaceName, EdoContainer container, String serviceId, String parentId, String parentCloudId){
        ObjectId id = new ObjectId();
        this.setId(id.toString());
        this.setName(spaceName);
        this.setType(FILE_TYPE_EDOTAB);
        this.setmDate(System.currentTimeMillis());
        this.setServiceId(serviceId);
        this.setParentId(parentId);
        this.setParentCloudId(parentCloudId);
        this.setvType(LFile.FILE_TYPE_EDOTAB);


        if(!container.isPersonal(context) && container.isContactOrGroup()) {

            LContact contact = (LContact)container;
            String[] collabsEmails = new String[]{contact.getPrincipalEmailAddress()};


            if (container.isGroup()) {
                collabsEmails = contact.getGroupMembersEmails(context);
            }

            this.setCollabsEmails(collabsEmails);

        }
	}

    public static LSpace newDefaultSpace(Context c, LContact contact){
        LSpace space = new LSpace(c,
                c.getResources().getString(R.string._default),
                contact,
                new LServiceDAO(c).getPrincipalService().getId(),
                null,
                null
        );
        space.setStatus(LFile.STATUS_DEFAULT);
        return space;
    }

	public ArrayList<LFile> getFilesOfCurrentFolder() {
		return files;
	}
	public void setFilesOfCurrentFolder(ArrayList<LFile> files) {
		this.files = files;
	}
    public LFile getFile(int position) {
        return files!=null && files.size()>position ? files.get(position) : null;
    }

	public LService getService(Context c, boolean fromDB){
		if(service==null || fromDB){
            LServiceDAO lsDao = new LServiceDAO(c);

			if(JavaUtils.isNotEmpty(this.getServiceId())){
				service = lsDao.get(this.getServiceId());
			}

            if(service==null){
                service = lsDao.getPrincipalService();
                setServiceId(service.getId());
                new LFileDAO(c).update(this);
            }
		}
		return service;
	}
	
	public LFile getCurrentFolder() {
		if(this.currentFolder!=null)
			return this.currentFolder;
        this.currentFolder = this;
		return this.currentFolder;
	}
	public void setCurrentFolder(LFile currentFolder) {
		this.currentFolder = currentFolder;
	}
	
	
	public ArrayList<LFile> getParentFolders(Context c){
		if(getCurrentFolder()!=null && !getCurrentFolder().isSpace()){
			ArrayList<LFile> parentFolders = new LFileDAO(c).getListBy(currentFolder.getPathIds());
			return parentFolders;
		}
		return null;
	}


    public boolean isDefault(){
        return this.getStatus()!=null && this.getStatus().equals(LFile.STATUS_DEFAULT);
    }

	public boolean isTopLevel(){
		return this.getCurrentFolder()!=null && getCurrentFolder().getId().equals(this.getId());
	}

    public String getIconName(){
        if(this.getThumbnail()!=null){
            return this.getThumbnail();
        }
        return "files";
    }

//    public int getIconRes(Context c) {
//        int icon = c.getResources().getIdentifier("cat_"+getIconName(), "drawable", c.getPackageName());
//        if(icon!=0)
//            return icon;
//        return R.drawable.cat_files;
//    }

    public int getForegroundColorRes(Context c) {
        int icon = c.getResources().getIdentifier("cat_"+getIconName(), "color", c.getPackageName());
        if(icon!=0)
            return icon;
        return R.color.cat_files;
    }


    public synchronized String getPhysicalPath(Context context, EdoContainer container){

        if(container==null){
            container = new LFileDAO(context).getContainerOf(this);
        }

        String path = container.getPhysicalFolderPath(context);

//		path += File.separator+this.getName();

        path = path.replace(" ", "_");

        java.io.File dir = new java.io.File (path);
        dir.mkdirs();
        if(dir.exists())
            return path;
        return null;
    }
	
}
