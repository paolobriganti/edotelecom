package io.edo.db.local.beans.support;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactFile;
import io.edo.db.local.beans.LContactFileDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;


public class LSpaceDAO extends LContactFileDAO {


	public LSpaceDAO(Context context){
		super(context);
	}

    //***************************
	//		USER SPACES			*
	//***************************
	public synchronized ArrayList<LSpace> getUserSpaces(){
		return getList();
	}

	public synchronized boolean addUserSpace(LSpace space){
		return false;
	}





	//***************************
	//		Contacts Spaces		*
	//***************************
    public synchronized boolean addSpaceToContact(LContact contact, LSpace space){
        if(contact!=null && space!=null){
            return insert(new LContactFile(contact,space));
        }
        return false;
    }

    public synchronized LSpace get(String id){
        if(id==null)
            return null;

        String whereClause = LocalDBHelper.KEY_ID+" == ? AND "+LocalDBHelper.KEY_TYPE+" == ?";
        Cursor cursor = db.select(LFileDAO.TABLE_NAME, LFileDAO.tableKeys, whereClause, new String[]{id,LFile.FILE_TYPE_EDOTAB});
        LSpace space = null;
        try {

            if(cursor.moveToNext()){
                space = new LSpace(cursor);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }


        return space;
    }

	public synchronized ArrayList<LSpace> getContactSpaces(LContact contact){
		return getListBy(contact);
	}


    public synchronized LSpace getContactDefaultSpace(LContact contact){
        LSpace space = null;



        if(contact!=null && contact.getId()!=null){

            String query = "SELECT * FROM "+LocalDBHelper.TABLE_CONTACT_FILE+" cf INNER JOIN "+LocalDBHelper.TABLE_FILE+" f " +
                    "ON cf."+LocalDBHelper.KEY_FILEID+"=f."+LocalDBHelper.KEY_ID+" " +
                    "WHERE cf."+LocalDBHelper.KEY_CONTACTID+"=? AND " +
                    "f."+LocalDBHelper.KEY_TYPE+" == '"+LFile.FILE_TYPE_EDOTAB+"' AND " +
                    "f."+LocalDBHelper.KEY_STATUS+" == '"+LFile.STATUS_DEFAULT+"' ";


            Cursor cursor = db.executeQuery(query, new String[]{String.valueOf(contact.getId())});
            try {

                if(cursor.moveToNext()){
                    space = new LSpace(cursor);
                }

            } finally {
                if(cursor != null)
                    cursor.close();
            }

        }

        return space;
    }

    public synchronized LSpace getSpaceByName(LContact contact, String name){
        LSpace space = null;



        if(contact!=null && contact.getId()!=null){

            String query = "SELECT * FROM "+LocalDBHelper.TABLE_CONTACT_FILE+" cf INNER JOIN "+LocalDBHelper.TABLE_FILE+" f " +
                    "ON cf."+LocalDBHelper.KEY_FILEID+"=f."+LocalDBHelper.KEY_ID+" " +
                    "WHERE cf."+LocalDBHelper.KEY_CONTACTID+"=? AND " +
                    "f."+LocalDBHelper.KEY_TYPE+" == '"+LFile.FILE_TYPE_EDOTAB+"' AND " +
                    "f."+LocalDBHelper.KEY_NAME+" == ? ";


            Cursor cursor = db.executeQuery(query, new String[]{String.valueOf(contact.getId()),name});
            try {

                if(cursor.moveToNext()){
                    space = new LSpace(cursor);
                }

            } finally {
                if(cursor != null)
                    cursor.close();
            }

        }

        return space;
    }

    public synchronized LSpace getSpaceById(LContact contact, String spaceId){
        LSpace space = null;



        if(contact!=null && contact.getId()!=null){

            String query = "SELECT * FROM "+LocalDBHelper.TABLE_CONTACT_FILE+" cf INNER JOIN "+LocalDBHelper.TABLE_FILE+" f " +
                    "ON cf."+LocalDBHelper.KEY_FILEID+"=f."+LocalDBHelper.KEY_ID+" " +
                    "WHERE cf."+LocalDBHelper.KEY_CONTACTID+"=? AND " +
                    "f."+LocalDBHelper.KEY_TYPE+" == '"+LFile.FILE_TYPE_EDOTAB+"' AND " +
                    "f."+LocalDBHelper.KEY_ID+" == ? ";


            Cursor cursor = db.executeQuery(query, new String[]{String.valueOf(contact.getId()),spaceId});
            try {

                if(cursor.moveToNext()){
                    space = new LSpace(cursor);
                }

            } finally {
                if(cursor != null)
                    cursor.close();
            }

        }

        return space;
    }






//	public synchronized String getSpaceFolderPath(EdoContainer container, LFile spaceFile){
//		String path = "";
//
//		if(container==null){
//			container = new LFileDAO(context).getContainerOf(spaceFile);
//		}
//
//		path += container.getPhysicalFolderPath();
//
////		path += File.separator+spaceFile.getName();
//
//		path = path.replace(" ", "_");
//
//		java.io.File dir = new java.io.File (path);
//		dir.mkdirs();
//		if(dir.exists())
//			return path;
//		return null;
//	}



    String[] spaceIcons = new String[]{
            "files",
            "work",
            "notes",
            "social",
            "ideas",
            "bills",
            "receipts",
            "todo",
            "prescriptions",
            "links",
            "flight",
            "places",
            "photo",
            "video",
            "pics",
            "music",
            "screenshots",
            "spreadsheet",
            "documents",
            "presentations",
            "recipes",
            "books",
            "gift",
            "university"
    };

    public ArrayList<LSpace> getSpaceSamples(){
        ArrayList<LSpace> spaces = new ArrayList<>();
        for(String spaceIcon : spaceIcons){
            LSpace space = new LSpace();
            space.setId(spaceIcon);
            int resName = context.getResources().getIdentifier("cat_"+spaceIcon,"string",context.getPackageName());
            if(resName>0){
                space.setName(context.getResources().getString(resName));
            }
            space.setThumbnail(spaceIcon);
            spaces.add(space);
        }
        return spaces;
    }




}
