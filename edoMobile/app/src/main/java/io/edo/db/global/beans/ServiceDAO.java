package io.edo.db.global.beans;

import android.content.Context;

import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.LServiceDAO;
import io.edo.db.web.beans.WServiceDAO;

public class ServiceDAO extends BaseDAO{
    LServiceDAO lServiceDAO;

    public ServiceDAO(Context context) {
        super(context);
        lServiceDAO = new LServiceDAO(context);
    }

    @Override
    public LServiceDAO getLDAO() {
        return lServiceDAO;
    }

    public boolean addService(final LService lService){
        if(WServiceDAO.addService(context, lService))
            return lServiceDAO.insert(lService);
        return false;
    }

    public boolean updateService(final LService lService){
        if(WServiceDAO.updateService(context, lService))
            return lServiceDAO.update(lService);
        return false;
    }

    public boolean removeService(final LService lService){
        if(WServiceDAO.deleteService(context, lService))
            return lServiceDAO.delete(lService);
        return false;
    }

//    public LService download(String id){
//
//        LService service = WServiceDAO.getService(context);
//        if(services!=null && services.length>0){
//            for (int i=0; i<services.length; i++){
//                LService service = services[i];
//                lServiceDAO.insertFromWebServer(service);
//            }
//        }
//
//        return services;
//    }

    public LService[] downloadAllServices(){

        LService[] services = WServiceDAO.getServices(context);
        if(services!=null && services.length>0){
            for (int i=0; i<services.length; i++){
                LService service = services[i];
                lServiceDAO.insertFromWebServer(service);
            }
        }

        return services;
    }




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		TPS
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    public LService updateGoogleAccessToken(String serviceId){

        LService updatedService = WServiceDAO.updateGoogleAccessToken(context, serviceId);

        if(updatedService!=null){
            lServiceDAO.update(updatedService);
            return updatedService;
        }
        return null;
    }

    public LService downloadLinkParserService(){
        LService service = WServiceDAO.getLinkParserService(context);

        if(service!=null){
            lServiceDAO.insertFromWebServer(service);
            return service;
        }
        return null;
    }

}
