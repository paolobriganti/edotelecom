package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;

import io.edo.db.web.beans.WStatus;

/**
 * Created by PaoloBriganti on 21/07/15.
 */
public class LStatusDAO extends LBaseDAO<WStatus,String, Void, String> {


    public LStatusDAO(Context context) {
        super(context);
    }

    @Override
    public ContentValues createContentValues(WStatus wStatus) {
        return null;
    }

    @Override
    public boolean insert(WStatus status) {
        return app.saveNotificationStatus(status);
    }

    @Override
    public boolean insertFromWebServer(WStatus status){
        return insert(status);
    }

    @Override
    public boolean update(WStatus status) {
        return insert(status);
    }

    @Override
    public boolean delete(WStatus wStatus) {
        return false;
    }

    @Override
    public WStatus get(String s) {
        return app.getNotificationStatus();
    }

    @Override
    public Void getList() {
        return null;
    }

    @Override
    public Void getListBy(String s) {
        return null;
    }

}
