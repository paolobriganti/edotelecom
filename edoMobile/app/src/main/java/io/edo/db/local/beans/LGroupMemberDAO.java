package io.edo.db.local.beans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import io.edo.db.local.LocalDBHelper;
import io.edo.utilities.ObjectId;

public class LGroupMemberDAO extends LBaseDAO<LGroupMember, LGroupMember, ArrayList<LContact>, LContact> {

	String TABLE_NAME = LocalDBHelper.TABLE_GROUP_MEMBER;

	private String[] keys = {LocalDBHelper.KEY_GROUPID, LocalDBHelper.KEY_CONTACTID};

	public LGroupMemberDAO(Context context) {
		super(context);
	}

    @Override
    public ContentValues createContentValues(LGroupMember groupMember) {
        ContentValues values = new ContentValues();
        if(groupMember.getGroupId()!=null)values.put( LocalDBHelper.KEY_GROUPID, groupMember.getGroupId() );
        if(groupMember.getContactId()!=null)values.put( LocalDBHelper.KEY_CONTACTID, groupMember.getContactId() );
        return values;
    }

    @Override
    public synchronized boolean insert(LGroupMember groupMember) {
        ContentValues initialValues = createContentValues(groupMember);
        long lid = db.insert(TABLE_NAME, initialValues);
        return lid>0;
    }

    @Override
    public synchronized boolean insertFromWebServer(LGroupMember groupMember) {
        return insert(groupMember);
    }

    public synchronized boolean insertGroupFromWebServer(LContact group, boolean isCreation){
        LContactDAO cdao = new LContactDAO(context);

        group.setEdoUserId(group.getId());

        LContact oldCGroup = cdao.get(group.getId());

        if(oldCGroup!=null){
            deleteGroupMembers(oldCGroup.getId());
        }

        boolean allContactsAdded = true;

        ArrayList<LContact> members = group.getGroupMembers(context);

        if(group.getPending()!=null && group.getPending().length>0){
            for(LContact contact:group.getPending()){
                if(!app.getEdoUserId().equals(group.getGroupOwnerId())) {//BECAUSE THE ID OF PENDING CONTACT COMES FROM OWNER CONTACTS
                    LContact oldContact = new LContactDAO(context).getByEmail(contact.getPrincipalEmailAddress());
                    if(oldContact!=null){
                        contact = oldContact;
                    }else
                        contact.setId(new ObjectId().toString());
                }
                members.add(contact);
            }
        }

        if(members!=null && members.size()>0)
            for(LContact contact : members){


                cdao.insertFromWebServer(contact);

                if(!insert(new LGroupMember(group, contact))){
                    allContactsAdded = false;
                }
            }

        if(allContactsAdded){
            if(oldCGroup==null){
                if(cdao.insert(group)){
                    return true;
                }
            }else{
                if(cdao.update(group)){
                    return true;
                }
            }
        }


        return false;
    }

    @Override
    public synchronized boolean update(LGroupMember groupMember) {
        return false;
    }

    @Override
    public synchronized boolean delete(LGroupMember groupMember) {
        String whereClause = LocalDBHelper.KEY_CONTACTID+" = '"+ groupMember.getContactId() +"' AND "+
                LocalDBHelper.KEY_GROUPID+" = '"+ groupMember.getGroupId() +"'";
        boolean deleted =  db.delete(TABLE_NAME, whereClause);

        return deleted;
    }

    public synchronized boolean deleteGroupMembers(String groupId){

        String whereClause = LocalDBHelper.KEY_GROUPID+" = '"+ groupId+"'";
        boolean deleted =  db.delete(TABLE_NAME, whereClause);

        return deleted;
    }

    @Override
    public synchronized LGroupMember get(LGroupMember groupMember) {
        LGroupMember oldGroupMember = null;

        String whereClause = LocalDBHelper.KEY_GROUPID+"= '?' AND "+
                LocalDBHelper.KEY_CONTACTID+"= ?";
        Cursor cursor = db.select(TABLE_NAME, keys, whereClause, new String[]{groupMember.getGroupId(), groupMember.getContactId()});
        try {

            if(cursor.moveToNext()){
                oldGroupMember = new LGroupMember(cursor);
            }

        } finally {
            if(cursor != null)
                cursor.close();
        }

        return oldGroupMember;
    }

    @Override
    public synchronized ArrayList<LContact> getList() {
        ArrayList<LContact> contacts = new ArrayList<LContact> ();

        String query = "SELECT * "+

                "FROM "+LocalDBHelper.TABLE_GROUP_MEMBER+" gm INNER JOIN "+LocalDBHelper.TABLE_CONTACT+" con " +
                "ON gm."+LocalDBHelper.KEY_CONTACTID+"=con."+LocalDBHelper.KEY_ID+" ";

        Cursor cursor = db.executeQuery(query, null);
        try {

            while(cursor.moveToNext()){
                LContact contact = new LContact(context,cursor);
                contacts.add(contact);

            }

        } finally {
            if(cursor != null)
                cursor.close();
        }



        return contacts;
    }

    @Override
    public synchronized ArrayList<LContact> getListBy(LContact group) {
        return getContactsOfGroup(group.getId());
    }

    public synchronized ArrayList<LContact> getContactsOfGroup(String groupId){
        ArrayList<LContact> contacts = new ArrayList<LContact> ();

        String query = "SELECT * "+

                "FROM "+LocalDBHelper.TABLE_GROUP_MEMBER+" gm INNER JOIN "+LocalDBHelper.TABLE_CONTACT+" con " +
                "ON gm."+LocalDBHelper.KEY_CONTACTID+"=con."+LocalDBHelper.KEY_ID+" " +
                "WHERE gm."+LocalDBHelper.KEY_GROUPID+"='"+groupId+"' ";

        Cursor cursor = db.executeQuery(query, null);
        try {

            while(cursor.moveToNext()){
                LContact contact = new LContact(context,cursor);
                contacts.add(contact);

            }

        } finally {
            if(cursor != null)
                cursor.close();
        }



        return contacts;
    }



	public synchronized String[] getEmailsOfEdoMembers(String groupId){
		String[] emails = null;

		String query = "SELECT con."+LocalDBHelper.KEY_PRINCIPALEMAILADDRESS+" "+

				"FROM "+LocalDBHelper.TABLE_GROUP_MEMBER+" gm INNER JOIN "+LocalDBHelper.TABLE_CONTACT+" con " +
				"ON gm."+LocalDBHelper.KEY_CONTACTID+"=con."+LocalDBHelper.KEY_ID+" " +
				"WHERE gm."+LocalDBHelper.KEY_GROUPID+"='"+groupId+"' AND con."+LocalDBHelper.KEY_EDOUSERID+" IS NOT NULL";



		Cursor cursor = db.executeQuery(query, null);
		try {

			int total = cursor.getCount();
			int count = 0;
            emails = new String[total];
			if(total>0){

				while(cursor.moveToNext()){
					emails[count] = cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_PRINCIPALEMAILADDRESS));
					count++;
				}
			}

		} finally {
			if(cursor != null)
				cursor.close();
		}



		return emails;
	}














}
