package io.edo.db.global.beans;

import android.content.Context;

import java.util.ArrayList;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.support.EdoResource;
import io.edo.db.web.beans.WContactDAO;
import io.edo.db.web.beans.WEntity;

public class ContactDAO extends BaseDAO{

    LContactDAO lContactDAO;

    public ContactDAO(Context context) {
        super(context);
        lContactDAO = new LContactDAO(context);
    }

    @Override
    public LContactDAO getLDAO() {
        return lContactDAO;
    }

    public WEntity add(LContact contact) {
        WEntity entity = WContactDAO.addContact(context, contact);

        if(entity!=null){
            entity.contact = WContactDAO.updateValuesFromWeb(contact, entity.contact);
            if(entity.contact!=null){
                lContactDAO.insertFromWebServer(entity.contact);
            }
            entity.act = getFakeActivity(LActivity.EVENT_INSERT,entity.contact);
        }

        return entity;
    }

    public WEntity addAll(ArrayList<LContact> lcontacts) {
        WEntity entity = WContactDAO.addContacts(context, lcontacts);

        if(entity!=null && entity.contacts!=null){
            for(LContact newWebContact : entity.contacts) {
                if (newWebContact != null) {
                    lContactDAO.insertFromWebServer(newWebContact);
                }
            }
        }


        return entity;
    }

    public WEntity update(LContact contact) {
        WEntity entity = WContactDAO.updateContact(context, contact);
        if(entity!=null){
            entity.contact = WContactDAO.updateValuesFromWeb(contact, entity.contact);

            if(entity.contact!=null){
                boolean updated = lContactDAO.insertFromWebServer(entity.contact);
            }

        }
        return entity;
    }


    public WEntity delete(LContact contact) {
        WEntity entity = WContactDAO.deleteContact(context, contact);


        if(entity!=null){
            entity.contact = WContactDAO.updateValuesFromWeb(contact, entity.contact);
            if(entity.contact!=null){
                lContactDAO.delete(entity.contact);
            }
        }

        return entity;
    }



    public LContact download(String id){
        LContact contact = WContactDAO.getContact(context, id, true);
        if(contact!=null)
            lContactDAO.insertFromWebServer(contact);
        return contact;
    }



    public LContact[] downloadAll(){
        LContact[] contacts = WContactDAO.getContacts(context);
        if(contacts!=null && contacts.length>0)
            for(LContact contact: contacts)
                lContactDAO.insertFromWebServer(contact);
        return contacts;
    }

    public LContact[] downloadAllSuggested(){
        LContact[] contacts = WContactDAO.getSuggestedContacts(context);
        if(contacts!=null && contacts.length>0)
            for(LContact contact: contacts){
                contact.setStatus(LContact.STATUS_SUGGESTED);
                lContactDAO.insertFromWebServer(contact);
            }
        return contacts;
    }





    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTIVITY
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    public static LActivity getFakeActivity(int event, EdoResource resource){
        LActivity a = new LActivity(LActivity.CONTEXT_PERSONAL, event, LActivity.RESOURCE_CONTACT);
        a.setResource(resource);
        return a;
    }

}
