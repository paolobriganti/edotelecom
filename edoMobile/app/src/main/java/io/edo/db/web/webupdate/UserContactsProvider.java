package io.edo.db.web.webupdate;

import android.content.Context;

import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.NumberUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.edo.api.ApiContacts;
import io.edo.db.global.beans.ContactDAO;
import io.edo.db.global.beans.ContactFileDAO;
import io.edo.db.global.beans.GroupDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LService;
import io.edo.db.web.beans.WContactDAO;
import io.edo.db.web.beans.WGroupDAO;
import io.edo.edomobile.R;
import io.edo.utilities.eLog;

public class UserContactsProvider {

	Context c; 
	ArrayList<LService> services;
	boolean downloadContactPhoto;

	OnContactsProgressChangeListener mListener;
	ContactDAO cDAO;
	GroupDAO gDAO;

	public UserContactsProvider(Context c,
			ArrayList<LService> services, boolean downloadContactPhoto) {
		super();
		this.c = c;
		this.services = services;
		this.downloadContactPhoto = downloadContactPhoto;
		this.cDAO = new ContactDAO(c);
		this.gDAO = new GroupDAO(c);
	}


	public ArrayList<LContact> saveContactsToDB(boolean registration){
		int count = 0;
		if(mListener!=null)mListener.onProgressChange(1, null, c.getResources().getString(R.string.loading));

		Map<String, LContact> oldContacts=Collections.synchronizedMap(new HashMap<String, LContact>());
		ArrayList<LContact> oldContactsList = cDAO.getLDAO().getList();
		if(oldContactsList!=null && oldContactsList.size()>0){
			for(LContact contact:oldContactsList){
				oldContacts.put(contact.getPrincipalEmailAddress(),contact);
			}
		}

		ArrayList<LContact> edoContacts = new ArrayList<LContact>();

		// BUILD SUGGESTED CONTACTS FROM SERVER
		if(registration){
			WContactDAO.buildSuggested(c);

			WGroupDAO.buildSuggested(c);

			if(mListener!=null)mListener.onProgressChange(3, null, c.getResources().getString(R.string.loading));
		}


		// CONTACTS FROM EDO SERVER
		if(mListener!=null)mListener.onProgressChange(10, null, c.getResources().getString(R.string.syncContacts));
		
		
		final LContact[] edoContactsArray = cDAO.downloadAll();
		if(edoContacts!=null)
			for(LContact contact:edoContactsArray){
				oldContacts.put(contact.getPrincipalEmailAddress(), contact);
				edoContacts.add(contact);
				count++;
			}


		// GROUPS FROM EDO SERVER
		if(mListener!=null)mListener.onProgressChange(20, null, c.getResources().getString(R.string.syncGroups));

		final LContact[] edoGroups = gDAO.downloadAll();
		if(edoGroups!=null)
			for(LContact group:edoGroups){
				edoContacts.add(group);
				count++;
			}



		// SUGGESTED CONTACTS
//		if(activeContacts!=null && activeContacts.size()<ApiContacts.LIMIT_SUGGESTED_CONTACTS){
//			if(mListener!=null)mListener.onProgressChange(40, null, c.getResources().getString(R.string.syncContacts));
//
//			final LContact[] suggestedContacts = cDAO.downloadAllSuggested();
//
//			if(suggestedContacts!=null)
//				for(LContact contact:suggestedContacts){
//					LContact oldContact = allContacts.get(contact.getPrincipalEmailAddress());
//					if(oldContact==null){
//						allContacts.put(contact.getPrincipalEmailAddress(), contact);
//					}else{
//						if(!oldContact.isActive()){
//							oldContact.setStatus(contact.getStatus());
//						}
//						oldContact.setThumbnail(contact.getThumbnail());
//					}
//					count++;
//				}
//		}




		ArrayList<LContact> newContacts = new ArrayList<LContact>();


		// DEVICE CONTACTS
		if(mListener!=null)mListener.onProgressChange(30, null, c.getResources().getString(R.string.syncContacts));


		count = 0;
		final ArrayList<LContact> deviceContacts = ApiContacts.getDeviceContacts(c);


		if(deviceContacts!=null && deviceContacts.size()>0)
			for(LContact contact: deviceContacts){
				String principalEmailAddress = contact.getPrincipalEmailAddress();
				if(principalEmailAddress!=null && oldContacts.get(principalEmailAddress)==null){
					eLog.d("CONTACTS", "DEVICE CONTACT: " + contact.getName() + " / " + principalEmailAddress + " ADDED TO QUEUE");
					newContacts.add(contact);
//					allContacts.put(principalEmailAddress, contact);
				}else{
					eLog.w("CONTACTS", "SERVICE CONTACT FOUND: "+contact.getName()+" / "+principalEmailAddress+" ALREADY ADDED TO QUEUE");
				}
				count++;
			}
		else
			eLog.w("CONTACTS", "NO DEVICE CONTACTS");


		// SERVICES CONTACTS
		if(mListener!=null)mListener.onProgressChange(40, null, c.getResources().getString(R.string.syncContacts));

		count = 0;

		final ArrayList<LContact> servicesContacts = ApiContacts.getContactsFromAllServices(c, services);

		if(servicesContacts!=null)
			for(LContact contact:servicesContacts){
				String principalEmailAddress = contact.getPrincipalEmailAddress();
				if(principalEmailAddress!=null && oldContacts.get(principalEmailAddress)==null){
					eLog.d("CONTACTS", "SERVICE CONTACT: "+contact.getName()+"/"+principalEmailAddress+" ADDED TO QUEUE");
					newContacts.add(contact);
//					allContacts.put(principalEmailAddress, contact);
				}else{
					eLog.e("CONTACTS", "SERVICE CONTACT FOUND: " + contact.getName() + "/" + principalEmailAddress + " ALREADY ADDED TO QUEUE");
				}
				count++;
			}






		// NEW CONTACT SAVING LOCAL DB

		long startTime = System.currentTimeMillis();

		if(mListener!=null)mListener.onProgressChange(50, null, c.getResources().getString(R.string.savingNewContacts));

		long lastProgressSended = System.currentTimeMillis();
		if(newContacts!=null && newContacts.size()>0){
			for(int i=0; i<newContacts.size(); i++){
				cDAO.getLDAO().insert(newContacts.get(i));
				if(System.currentTimeMillis()-lastProgressSended>1500){
					lastProgressSended = System.currentTimeMillis();
					if(mListener!=null)mListener.onProgressChange(NumberUtils.getPercent(i, newContacts.size(), 51, 69), null, c.getResources().getString(R.string.loading));
				}
			}
		}

		long interval = (System.currentTimeMillis()-startTime)/1000;

		eLog.w("CONTACTS", "NEW CONTACT SAVING LOCAL DB TIME: "+interval);




		// CONTCTS FILES SAVING
		startTime = System.currentTimeMillis();

		ContactFileDAO contactFileDAO = new ContactFileDAO(c);

		if(mListener!=null)mListener.onProgressChange(70, null, c.getResources().getString(R.string.savingNewContacts));
		if(edoContacts!=null && edoContacts.size()>0){
			for(int i=0; i<edoContacts.size(); i++){
				String name = JavaUtils.isNotEmpty(edoContacts.get(i).getName()) ? edoContacts.get(i).getName(): "";
				String surname = JavaUtils.isNotEmpty(edoContacts.get(i).getSurname()) ? edoContacts.get(i).getSurname() : "";
				String nameSurname = name + " " + surname;


				LContact contact = edoContacts.get(i);
				if(contact.isActive()||contact.isHidden()||contact.isGroup()){
					contactFileDAO.downloadAllFileOf(contact,null);
					if(mListener!=null)mListener.onProgressChange(NumberUtils.getPercent(i, edoContacts.size(), 71, 90), nameSurname, c.getResources().getString(R.string.downloadingFilesOf)+" "+nameSurname);
				}
			}

		}

		interval = (System.currentTimeMillis()-startTime)/1000;

		eLog.w("CONTACTS", "CONTCTS FILES SAVING TIME: "+interval);



		ArrayList<LContact> allContactsArray = new ArrayList<LContact>(oldContacts.values());
		allContactsArray.addAll(newContacts);

		if(allContactsArray!=null && allContactsArray.size()>0){

			//Downloading contacts photos
//			ApiContacts.buildPhotoData(c, db, new ArrayList<LContact>(allContacts.values()), edoGroups, false);

			return allContactsArray;
		}

		return null;
	}





	public void setOnProgressChangeListener(OnContactsProgressChangeListener listener) {
		mListener = listener;
	}

	public interface OnContactsProgressChangeListener {
		void onProgressChange(int percent, String contactName, String info);
	}

}
