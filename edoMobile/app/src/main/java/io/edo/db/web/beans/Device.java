package io.edo.db.web.beans;

import android.content.Context;

import com.google.gson.Gson;
import com.paolobriganti.android.ASI;

import java.io.Serializable;

import io.edo.db.local.beans.LUser;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class Device implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String _id = null;
	private String type = null;
	private String model = null;
	private String os = null;
	private String os_ver = null;
	private String push_id = null;
	private Long cDate = null;

	public Device(Context c, String push_id) {
		super();
		this._id = LUser.getDeviceId(c);
		this.type = LUser.getDeviceType(c);
		this.model = ASI.get_device_model();
		this.os = "android";
		this.os_ver = ASI.get_OS_RELEASE_Version();
		this.push_id = push_id;
		this.cDate = System.currentTimeMillis();
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getOs_ver() {
		return os_ver;
	}

	public void setOs_ver(String os_ver) {
		this.os_ver = os_ver;
	}

	public String getPush_id() {
		return push_id;
	}

	public void setPush_id(String push_id) {
		this.push_id = push_id;
	}

	

	public String toJson(){
		return new Gson().toJson(this);
	}

	public static Device fromJson(String json){
		
		try{
			return new Gson().fromJson(json, Device.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "Device:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "Device JSON:\n"+json);
			return null;
		}
	}
}
