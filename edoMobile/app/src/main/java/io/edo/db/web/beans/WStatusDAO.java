package io.edo.db.web.beans;

import android.content.Context;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;

import java.util.HashMap;

import io.edo.db.web.request.EdoRequest;

public class WStatusDAO {

	public static WStatus getStatus(Context context){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_USER_STATUS;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			return WStatus.fromJson(response);
		}
		return null;
	}


	public static boolean sendStatus(Context context, WStatus status){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_USER_STATUS;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, status.toJson());

		HttpResponse response = request.execute(context);

		return !EdoRequest.hasErrors(response);
	}

	public static boolean updateStatus(Context context, String containerId, String lastNotificationSeenId){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_USER_STATUS;

		HashMap<String, String> map = new HashMap<>();
		map.put(containerId,lastNotificationSeenId);

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, new Gson().toJson(map));

		HttpResponse response = request.execute(context);

		return !EdoRequest.hasErrors(response);
	}
	
}
