package io.edo.db.web.request;

import com.google.gson.Gson;

import io.edo.db.local.beans.LContact;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class LoginResponseParams {
	public String token;
	public String id;
	public String deviceId;
	public LContact contact;

	public String toJson(){
		return new Gson().toJson(this);
	}

	public static LoginResponseParams fromJson(String json){
		try{
			return new Gson().fromJson(json, LoginResponseParams.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LoginResponseParams:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LoginResponseParams JSON:\n"+json);
			return null;
		}
	}
}
