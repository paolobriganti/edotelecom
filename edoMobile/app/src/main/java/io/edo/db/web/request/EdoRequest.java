package io.edo.db.web.request;

import android.content.Context;

import com.google.gson.Gson;
import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.JavaUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;

import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class EdoRequest {


	public static final String HOST_PREFIX_PRODUCTION = "https://service.edo.io";
	public static final String HOST_PREFIX_TEST = "https://servicetest.edo.io";
	public static final String HOST_PREFIX_TEST2 = "https://edotest2.cloudapp.net";





	public static final String HOST_PREFIX = HOST_PREFIX_TEST2;





	public static final boolean IS_TEST_APP = false;





	public static final String HOST_PREFIX_APP = HOST_PREFIX.equals(HOST_PREFIX_PRODUCTION)?"https://app.edo.io":
			HOST_PREFIX.equals(HOST_PREFIX_TEST)?"https://edoapp.cloudapp.net:10443":"https://edotest2.cloudapp.net:10443";
























	public static final String REQUEST_TYPE_GET = "get";
	public static final String REQUEST_TYPE_POST = "post";
	public static final String REQUEST_TYPE_DELETE = "delete";
	public static final String REQUEST_TYPE_PUT = "put";

	public static final String REQUEST_DUMP = "dump";

	public static final String PATH_API = "/api/v1";

	public static final String PATH_SEPARATOR = "/";
	public static final String QUERY_SEPARATOR = "?";

	public static final String PATH_INVITE = "/invite";
	public static final String PATH_LOGIN = "/login";
	public static final String PATH_LOGIN_GOOGLE = "/login/google";
	public static final String PATH_LOGOUT = "/logout";
	public static final String PATH_REGISTRATION = "/registration";
	public static final String PATH_CATEGORY = "/category";
	public static final String PATH_PERSONAL = "/personal";
	public static final String PATH_CONTACT = "/contact";
	public static final String PATH_CONTACTS = "/contacts";
	public static final String PATH_CONTACT_SUGGESTED = "/contact/suggested";
	public static final String PATH_FEEDBACK = "/feedback";
	public static final String PATH_GROUP = "/group";
	public static final String PATH_INFO = "/info";
	public static final String PATH_MEMBERS = "/members";
	public static final String PATH_GROUP_SUGGESTED = "/group/suggested";
	public static final String PATH_SERVICE_GOOGLE = "/service/google";
	public static final String PATH_SERVICE = "/service";
	public static final String PATH_FILE = "/file";
	public static final String PATH_FILES = "/files";
	public static final String PATH_CONTENT = "/content";
	public static final String PATH_NOTE = "/note";
	public static final String PATH_NOTES = "/notes";
	public static final String PATH_REALTIME = "/realtime";
	public static final String PATH_REGENERATION = "/regeneration";
	public static final String PATH_USER = "/user";
	public static final String PATH_USER_DEVICE = "/user/device";
	public static final String PATH_USER_CATEGORIES = "/user/categories";
	public static final String PATH_USER_GROUPS = "/user/groups";
	public static final String PATH_USER_SERVICES = "/user/services";
	public static final String PATH_USER_CONTACTS = "/user/contacts";
	public static final String PATH_USER_ACTIVITIES = "/user/activities";
	public static final String PATH_USER_STATUS = "/user/status";
	public static final String PATH_EMAIL = "/email";
	public static final String PATH_THUMBNAIL = "/thumbnail?filename=";

	public static final String PATH_SUCCESS = "/success";



	public static final String URL_LINK_THUMBNAIL = HOST_PREFIX_APP+"/preview/webpage";
	public static final String URL_LINK_THUMBNAIL_LOGIN = HOST_PREFIX_APP+"/preview/login";
	public static final String URL_LINK_CONTACT_THUMB = HOST_PREFIX_APP+"/contact/info";
	public static final String URL_SPACE_THUMBNAIL = "https://edoappstatic.blob.core.windows.net/img/category_bg/"+ Constants.PH_SPACEICONNAME+".jpg";


	public static final String VALUE_CONFIGURE = "configure";
	public static final String VALUE_STATE = "state";
	public static final String VALUE_CODE = "code";
	public static final String VALUE_TOKEN = "token";
	public static final String VALUE_SERVICEID = "serviceId";
	public static final String VALUE_KEEP = "keep";

	public static final int ERROR_GENERAL = HttpStatus.SC_BAD_REQUEST;
	public static final int SUCCESS_REGISTRATION = -1;
	public static final int SUCCESS_LOGIN = 0;

	public static final String REQUEST_STATUS_NO_INTERNET = "no_internet";


	public static final String EDO_SCHEME = "service.edo.io";
	public static final String EDO_HOST_CONTINUE_LOGIN = "continuelogin";

	public static final String SUCCESS = "succes";

	public String type;
	public String body;
	public String url;
	public java.io.File file;



	public EdoRequest(String type, String url, String body) {
		super();
		this.type = type;
		this.body = body;
		this.url = url;

		eLog.d("E_D_O_SERVICE", type.toUpperCase()+": "+url);
		eLog.w("E_D_O_SERVICE", "BODY \n"+body);
	}


	public EdoRequest() {
		// TODO Auto-generated constructor stub
	}


	public String send(Context c){
		String stringResponse = null;

		HttpResponse response = execute(c);

		if(!hasErrors(response)){

			try {
				if(response.getEntity()!=null){
					stringResponse = EntityUtils.toString(response.getEntity());

					eLog.w("E_D_O_SERVICE", "SERVER RESPONSE "+stringResponse);
				}else{
					eLog.w("E_D_O_SERVICE", "SERVER RESPONSE NULL");

				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}else if(isForbidden(c, response)){
			eLog.e("E_D_O_SERVICE", "ERROR FORBIDDEN");
		}

		return stringResponse;
	}




	public HttpResponse execute(Context c){
		ArrayList<HeaderParams> headerParams = getHeaderParams(c);

		HttpResponse response = null;

		if(ASI.isInternetOn(c)){

			if(JavaUtils.isNotEmpty(type) && type.equals(REQUEST_TYPE_GET)){
				response = HttpRequest.sendGet(url, headerParams);
			}else if(JavaUtils.isNotEmpty(type) && type.equals(REQUEST_TYPE_POST)){
				response = HttpRequest.sendPost(url, headerParams, null, body);
			}else if(JavaUtils.isNotEmpty(type) && type.equals(REQUEST_TYPE_DELETE)){
				response = HttpRequest.sendDelete(url, headerParams);
			}else if(JavaUtils.isNotEmpty(type) && type.equals(REQUEST_TYPE_PUT)){
				response = HttpRequest.sendPut(url, headerParams, null, body);
			}

		}

		return response;
	}





	public String toJson(){
		return new Gson().toJson(this);
	}

	public static EdoRequest fromJson(String json){
		try{
			return new Gson().fromJson(json, EdoRequest.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "EdoRequest:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "EdoRequest JSON:\n"+json);
			return null;
		}
	}





	public ArrayList<HeaderParams> getHeaderParams(Context c){
		ThisApp app = ThisApp.getInstance(c);
		if(JavaUtils.isNotEmpty(app.getEdoToken()) && JavaUtils.isNotEmpty(app.getEdoUserHeaderParam())){
			ArrayList<HeaderParams> params = new ArrayList<HeaderParams>();
			params.add(new HeaderParams("Edo-Token", app.getEdoToken()));
			params.add(new HeaderParams("Edo-User", app.getEdoUserHeaderParam()));
			params.add(new HeaderParams("Edo-Agent", app.getDeviceTypeHeaderParam()));
			return params;
		}
		return null;
	}




	public static boolean hasErrors(HttpResponse response){

		if(response!=null){
			int statusCode = response.getStatusLine().getStatusCode();
			boolean hasErrors = statusCode >= HttpStatus.SC_MULTIPLE_CHOICES;

			if(hasErrors) {
				if (response.getEntity() != null) {
					try {
						String stringResponse = EntityUtils.toString(response.getEntity());

						eLog.w("E_D_O_SERVICE", "RESPONSE_STATUS_CODE " + statusCode + ": " + stringResponse);
					} catch (Exception e) {
						eLog.w("E_D_O_SERVICE", "RESPONSE_STATUS_CODE " + statusCode);
					}

				} else {
					eLog.w("E_D_O_SERVICE", "RESPONSE_STATUS_CODE " + statusCode + ": no response");
				}
			}else{
				eLog.w("E_D_O_SERVICE", "RESPONSE_STATUS_CODE " + statusCode);
			}

			return hasErrors;

		}


		return true;
	}


	public static boolean isForbidden(Context c, HttpResponse response){
		if(response==null)
			return false;


		int statusCode = response.getStatusLine().getStatusCode();

		eLog.w("E_D_O_SERVICE", "CHECK RESPONSE_STATUS_CODE "+statusCode);

		if(statusCode==HttpStatus.SC_FORBIDDEN){
			EdoUI.resetApplicationDataAndRestart(c);
			return true;
		}


		return false;
	}


	public static boolean hasEdoErrors(String response){

		if(response!=null){

			RequestError re = new EdoRequest().new RequestError().fromJson(response);

			return re!=null && JavaUtils.isNotEmpty(re.error);
		}


		return true;
	}


	public class RequestError {
		public String error;

		public String toJson(){
			return new Gson().toJson(this);
		}

		public RequestError fromJson(String json){
			try{
				return new Gson().fromJson(json, RequestError.class);
			}catch (com.google.gson.JsonSyntaxException e){
				//eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "RequestError:\n"+e.getMessage());
				//eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "RequestError JSON:\n"+json);
				return null;
			}
		}
	}
}