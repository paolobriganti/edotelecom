package io.edo.db.web.webupdate;

import android.app.Activity;
import android.content.Context;

import com.paolobriganti.utils.NumberUtils;

import java.util.ArrayList;

import io.edo.db.global.beans.ActivityDAO;
import io.edo.db.global.beans.ContactFileDAO;
import io.edo.db.global.beans.ServiceDAO;
import io.edo.db.global.beans.StatusDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LService;
import io.edo.db.web.webupdate.UserContactsProvider.OnContactsProgressChangeListener;
import io.edo.edomobile.R;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoNotification;
import io.edo.ui.OnProgressChangeListener;
import io.edo.ui.UIManager;

public class EdoGeneralUpdate {

	public static final int DOWNLOAD_ALL = 0;
	public static final int DOWNLOAD_PERSONAL = 1;
	public static final int DOWNLOAD_CONTACTS = 2;

	public static boolean getWebDBToLocalDB(final Activity c, boolean isRegistration, int whatToDownload, final EdoNotification notification){
//		if(db.getMediaFiles()==null || db.getMediaFiles().size()==0)
//			db.setMediaFiles(FileInfo.getAllMediaFiles(c));

		notification.buildDataNotification(EdoNotification.ID_DOWNLOAD_DATA, c.getResources().getString(R.string.loading), c.getResources().getString(R.string.syncData));
		notification.showNotification();
		
		LService[] services = new ServiceDAO(c).downloadAllServices();
		EdoNotification.cancelNotification(c, EdoNotification.ID_DOWNLOAD_DATA);
		if(services!=null && services.length>0){
			switch (whatToDownload) {
			case DOWNLOAD_ALL:
				personalSection(c, notification, false);
				contactsSection(c, notification, isRegistration);
				break;
				
			case DOWNLOAD_PERSONAL:
				personalSection(c, notification, false);
				break;
				
			case DOWNLOAD_CONTACTS:
				contactsSection(c, notification, isRegistration);
				break;

			default:
				break;
			}


			
			return true;
		}

		return false;
	}




	public static boolean personalSection(final Activity c, final EdoNotification notification, boolean downloadActivities){
//		if(db.getMediaFiles()==null || db.getMediaFiles().size()==0)
//			db.setMediaFiles(FileInfo.getAllMediaFiles(c));

		notification.buildDataNotification(EdoNotification.ID_DOWNLOAD_DATA_PERSONAL, c.getResources().getString(R.string.loading), c.getResources().getString(R.string.syncData));
		notification.showNotification();

		setProgress(c, LActivity.CONTEXT_PERSONAL, null, 20, notification, EdoNotification.ID_DOWNLOAD_DATA_PERSONAL, true);

		LContact contact = ThisApp.getInstance(c).getUserContact();
//		if(contact==null){
//			LUser user = new LUserDAO(c).getPrincipalUser();
//			if(user!=null){
//				contact = user.getUserContact();
//				ThisApp.getInstance(c).setUser(user);
//			}else{
//				return false;
//			}
//		}


		OnProgressChangeListener listener1 = new OnProgressChangeListener() {

			@Override
			public void onProgressChange(int current, int total, Object object,
										 String info) {

				setProgress(c, LActivity.CONTEXT_PERSONAL, null, NumberUtils.getPercent(current, total, 0, 80), notification, EdoNotification.ID_DOWNLOAD_DATA_PERSONAL, true);

			}
		};

        ContactFileDAO contactFileDAO = new ContactFileDAO(c);
        contactFileDAO.downloadAllFileOf(contact,listener1);


		setProgress(c, LActivity.CONTEXT_PERSONAL, null, 80, notification, EdoNotification.ID_DOWNLOAD_DATA_PERSONAL, true);


		if(downloadActivities) {
			OnProgressChangeListener listener2 = new OnProgressChangeListener() {

				@Override
				public void onProgressChange(int current, int total, Object object,
											 String info) {

					setProgress(c, LActivity.CONTEXT_PERSONAL, null, NumberUtils.getPercent(current, total, 81, 99), notification, EdoNotification.ID_DOWNLOAD_DATA_PERSONAL, true);

				}
			};
			new ActivityDAO(c).downloadAllBy(contact, true, listener2);
		}

		setProgress(c, LActivity.CONTEXT_PERSONAL, c.getResources().getString(R.string.spacesSyncComplete), 100, notification, EdoNotification.ID_DOWNLOAD_DATA_PERSONAL, false);
		EdoNotification.cancelNotification(c, EdoNotification.ID_DOWNLOAD_DATA_PERSONAL);
		return true;
	}





	public static ArrayList<LContact> contactsSection(final Activity c, final EdoNotification notification, boolean isRegistration){
//		if(db.getMediaFiles()==null || db.getMediaFiles().size()==0)
//			db.setMediaFiles(FileInfo.getAllMediaFiles(c));
		ThisApp app = ThisApp.getInstance(c);

		app.isContactsUpdateInProgress = true;

		notification.buildDataNotification(EdoNotification.ID_DOWNLOAD_DATA_CONTACTS, c.getResources().getString(R.string.loading), c.getResources().getString(R.string.syncData));
		notification.showNotification();
		
		UserContactsProvider ucp = new UserContactsProvider(c, null, false);
		
		ucp.setOnProgressChangeListener(new OnContactsProgressChangeListener() {

			@Override
			public void onProgressChange(int percent, String contactName, String info) {
				setProgress(c, LActivity.CONTEXT_CONTACT, info, percent, notification, EdoNotification.ID_DOWNLOAD_DATA_CONTACTS, false);
			}
		});
		
		final ArrayList<LContact> allContacts = ucp.saveContactsToDB(isRegistration);


		OnProgressChangeListener listener2 = new OnProgressChangeListener() {

			@Override
			public void onProgressChange(int current, int total, Object object,
										 String info) {

				setProgress(c, LActivity.CONTEXT_CONTACT, null, NumberUtils.getPercent(current, total, 91, 99), notification, EdoNotification.ID_DOWNLOAD_DATA_CONTACTS, true);

			}
		};

		new ActivityDAO(c).downloadAll(true,listener2);

		new StatusDAO(c).download(false);

		setProgress(c, LActivity.CONTEXT_CONTACT, c.getResources().getString(R.string.contactsSyncComplete), 100, notification, EdoNotification.ID_DOWNLOAD_DATA_CONTACTS, false);
		EdoNotification.cancelNotification(c, EdoNotification.ID_DOWNLOAD_DATA_CONTACTS);

		app.isContactsUpdateInProgress = false;

		return allContacts;
	}



	public static void setProgress(Context c, 
			String context, String subTitle, Integer percent,
			EdoNotification notification, Integer idNotification, boolean indeterminate){

		if(notification!=null && idNotification!=null && percent!=null){
			notification.setId(idNotification);

			notification.showProgress(percent, indeterminate, c.getResources().getString(R.string.loading)+" "+percent+"%", subTitle);
		}
		if(context!=null && percent!=null){
			
			
			if(percent>0 && percent<100){
				sendFakeActivity(c,context,percent+"%",LActivity.STATUS_IN_PROGRESS,percent);
			}else if(percent>=100){
				sendFakeActivity(c,context,percent+"%",LActivity.STATUS_COMPLETE,percent);
			}
		}
	}


	public static LActivity sendFakeActivity(Context c, String context, String message, int progressStatus, int progressPercent){
		LActivity a = new LActivity();
		a.setContext(context);
		a.setEvent(LActivity.EVENT_SYNC_ALL);
		a.setMessage(message);
		a.setStatus(progressStatus);
		a.setProgressPercent(progressPercent);
		ThisApp.getInstance(c).uiManager.syncResourcesList(a, UIManager.ContactsUpdatesListener.class);
		return a;
	}






//	public static void getOldGroupOfContact(final Context c){
//		if(c==null)
//			return;
//
//		new Thread(){@Override public void run(){Looper.myLooper();//Looper.prepare();
//		boolean isLoginComplete = ThisApp.getInstance(c).isLoginComplete();
//
//		if(isLoginComplete){
//			EdoGeneralUpdate.setProgress(c, EdoUIUpdate.ID_CONTACTS, null, null, 1, null, null, false);
//
//			ThisApp app = ThisApp.getInstance(c);
//
//			if(app.getDB()==null){
//				while(app.getDB()==null);
//			}
//
//			final ArrayList<LContact> edoGroups = WGroupDAO.getGroupsAndAddToLocal(c, app.getDB(), null);
//			if(edoGroups!=null && edoGroups.size()>0){
//				EdoGeneralUpdate.setProgress(c, EdoUIUpdate.ID_CONTACTS, null, null, 50, null, null, false);
//
//
//
//				for(LContact group: edoGroups){
//					WContactFileDAO.getContactFiles(c, app.getDB(), group, false, false, null);
//					new LActivityDAO(c, app.getDB()).deletePushMsgOfContainerId(group.getId());
//				}
//
//
//				String message = c.getResources().getString(R.string.youHaveOneNewGroupOnEdo);
//				if(edoGroups.size()>1)
//					message = c.getResources().getString(R.string.youHaveNUMNewGroupOnEdo).replace(Constants.PH_NUM, ""+edoGroups.size());
//
//				EdoNotification.showQuickNotification(c, EdoNotification.ID_WELCOME,
//						c.getResources().getString(R.string.groups),
//						message,
//						null, null,
//						EdoIntentManager.getContactsIntent(c, null, null, false, null));
//
//			}
//
//			EdoGeneralUpdate.setProgress(c, EdoUIUpdate.ID_CONTACTS, null, null, 100, null, null, false);
//
//
//		}
//		}}.start();
//	}




}


