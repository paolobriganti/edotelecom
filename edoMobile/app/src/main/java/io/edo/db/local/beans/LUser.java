package io.edo.db.local.beans;

import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;
import com.paolobriganti.android.ASI;

import java.io.Serializable;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.web.beans.Device;
import io.edo.db.web.beans.WStatus;
import io.edo.ui.EdoUI;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class LUser  implements Serializable{
	private static final long serialVersionUID = 1L;

	public static final String DEVICE_TYPE_SMARTPHONE = "smartphone";
	public static final String DEVICE_TYPE_TABLET = "tablet";
	public static final String DEVICE_TYPE_DESKTOP = "desktop";
	public static final String DEVICE_TYPE_TV = "tv";
	
	private String _id=null;
	private String loginEmail=null;
	private String loginCredentials=null;
	private String status=null;
	private String contactId=null;
	
	private Device[] devices = null;
	private String[] contact_list = null;
	private String[] category_list = null;
	private String[] group_list = null;
	private String[] service_list = null;
	
	
	//UserInfo
	public static final int LOGIN_0_NOT_EXECUTED = 0;
	public static final int LOGIN_1_TEMP_AUTHENTICATION_EXECUTED = 1;
	public static final int LOGIN_2_GOOGLE_AUTHORIZATION_OBTAINED = 2;
	public static final int LOGIN_3_AUTHENTICATION_ENDED = 3;
	public static final int LOGIN_4_DATA_DOWNLOAD_FINISHED = 4;
	
	private Long loginDate;
	private Integer loginStatus;
	private String deviceId; 
	private String name;
	private String surname;
	private Long lastTokenUpdate;
	private String notificationStatus;
	
	
	
	public LUser(Cursor cursor) {
		super();

		setId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_ID)));
		setLoginEmail(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_LOGINEMAIL)));
		setEdoToken(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_LOGINCREDENTIALS)));
		setStatus(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_STATUS)));
		setContactId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTACTID)));
		
		setLoginDate(cursor.getLong(cursor.getColumnIndex(LocalDBHelper.KEY_LOGINDATE)));
		setLoginStatus(cursor.getInt(cursor.getColumnIndex(LocalDBHelper.KEY_LOGINSTATUS)));
		setDeviceId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_DEVICEID)));
		setName(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_NAME)));
		setSurname(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_SURNAME)));
		setLastTokenUpdate(cursor.getLong(cursor.getColumnIndex(LocalDBHelper.KEY_LASTTOKENUPDATE)));
		setNotificationStatus(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_NOTIFICATIONSTATUS)));
	}



	public String getId() {
		return _id;
	}



	public void setId(String _id) {
		this._id = _id;
	}



	public String getLoginEmail() {
		return loginEmail;
	}



	public void setLoginEmail(String loginEmail) {
		this.loginEmail = loginEmail;
	}



	public String getEdoToken() {
		return loginCredentials;
	}



	public void setEdoToken(String loginToken) {
		this.loginCredentials = loginToken;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getContactId() {
		return contactId;
	}



	public void setContactId(String contactId) {
		this.contactId = contactId;
	}



	public Device[] getDevices() {
		return devices;
	}



	public void setDevices(Device[] devices) {
		this.devices = devices;
	}



	public String[] getContact_list() {
		return contact_list;
	}



	public void setContact_list(String[] contact_list) {
		this.contact_list = contact_list;
	}



	public String[] getCategory_list() {
		return category_list;
	}



	public void setCategory_list(String[] category_list) {
		this.category_list = category_list;
	}



	public String[] getGroup_list() {
		return group_list;
	}



	public void setGroup_list(String[] group_list) {
		this.group_list = group_list;
	}



	public String[] getService_list() {
		return service_list;
	}



	public void setService_list(String[] service_list) {
		this.service_list = service_list;
	}



	public int getLoginStatus() {
		if(loginStatus!=null){
			return loginStatus;
		}
		return LUser.LOGIN_0_NOT_EXECUTED;
	}

	public boolean isLoginComplete() {
		return getLoginStatus()==LUser.LOGIN_4_DATA_DOWNLOAD_FINISHED;
	}

	public void setLoginStatus(Integer loginStatus) {
		this.loginStatus = loginStatus;
	}



	public String getDeviceId() {
		return deviceId;
	}



	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getSurname() {
		return surname;
	}



	public void setSurname(String surname) {
		this.surname = surname;
	}



	public Long getLastTokenUpdate() {
		return lastTokenUpdate;
	}



	public void setLastTokenUpdate(Long lastTokenUpdate) {
		this.lastTokenUpdate = lastTokenUpdate;
	}



	public String getNotificationStatus() {
		return notificationStatus;
	}



	public void setNotificationStatus(String notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	
	public WStatus getWStatus() {
		if(notificationStatus!=null)
			return WStatus.fromJson(notificationStatus);
		return null;
	}



	public void setNotificationStatus(WStatus status) {
		if(status!=null)
			this.notificationStatus = status.toJson();
	}

	
	public Long getLoginDate() {
		return loginDate;
	}



	public void setLoginDate(Long loginDate) {
		this.loginDate = loginDate;
	}

	
	public String getEdoUserHeaderParam() {
		return _id+":"+deviceId;
	}
	
	public String getDeviceTypeHeaderParam(Context c) {
		return (EdoUI.isTablet(c) ? "t":"s")+"a";
	}
	
	public String getUserNameSurname() {
		return name+" "+surname;
	}
	
	public LContact getUserContact(){
		if(_id!=null){
			LContact contact = new LContact(contactId);
			contact.setEdoUserId(_id);
			contact.setName(name);
			contact.setSurname(surname);
			contact.setPrincipalEmailAddress(loginEmail);
			contact.setStatus(LContact.STATUS_ACTIVE);
			return contact;
		}
		return null;
	}
	
	public static String getDeviceType(Context c){
		String deviceType = LUser.DEVICE_TYPE_SMARTPHONE;
		if(ASI.isTablet(c))
			deviceType = LUser.DEVICE_TYPE_TABLET;
		return deviceType;
	}
	
	public static String getDeviceId(Context c){
		return ASI.get_device_id(c);
	}
	
	
	
	
	
	
	
	
	
	
	
	public String toJson(){
		return new Gson().toJson(this);
	}

	public static LUser fromJson(String json){
		try{
			return new Gson().fromJson(json, LUser.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LUser:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LUser JSON:\n"+json);
			return null;
		}
	}
}
