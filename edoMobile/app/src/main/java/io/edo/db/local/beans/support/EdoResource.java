package io.edo.db.local.beans.support;

import java.io.Serializable;

public interface EdoResource extends Serializable{

	String getId();

	String getName();

	boolean equals(Object obj);
	
}
