package io.edo.db.global.uibeans;

import android.content.Context;

import io.edo.db.global.beans.CommentDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LBaseDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.WEntity;

public class CommentUIDAO extends BaseUIDAO{

	CommentDAO commentDAO;

	public CommentUIDAO(Context context) {
		super(context);
		commentDAO = new CommentDAO(context);
	}

	@Override
	public LBaseDAO getLDAO() {
		return commentDAO.getLDAO();
	}


	public LFile addComment(final Context c,
							  final EdoContainer container,
							  final LFile file,
							  final String message){

		WEntity entity = commentDAO.addComment(c,container,file,message);

		if(entity!=null){
            processActivity(entity);

			return entity.file;
		}
		return null;
	}



	public LFile downloadComments(LFile file, boolean isPush){
		return commentDAO.downloadComments(file,isPush);
	}






	//*****************************************************************************************************************************************************************************************************************************************************************************************************
	//*		ACTIVITY
	//*****************************************************************************************************************************************************************************************************************************************************************************************************

	public LActivity addExtraToActivity(LActivity act, EdoContainer container, LSpace spaceWithCurrentFolder, LFile file){
		if(act==null)
			return null;

		act.setStatus(LActivity.STATUS_COMPLETE);
		act.setExtra(app.getUserContact(),container,spaceWithCurrentFolder,file);
		return act;
	}



}
