package io.edo.db.global.uibeans;

import android.content.Context;

import io.edo.db.global.beans.ActivityDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.ui.OnProgressChangeListener;
import io.edo.ui.UIManager;
import io.edo.utilities.ObjectId;

/**
 * Created by PaoloBriganti on 22/06/15.
 */
public class ActivityUIDAO extends BaseUIDAO {
    ActivityDAO activityDAO;

    public ActivityUIDAO(Context context) {
        super(context);
        activityDAO = new ActivityDAO(context);
    }


    public boolean delete(LActivity a){

        return getLDAO().delete(a);
    }


    @Override
    public LActivityDAO getLDAO() {
        return activityDAO.getLDAO();
    }

    public LActivity[] downloadAll(OnProgressChangeListener listener) {
        return downloadAllBy(null,listener);
    }

    public LActivity[] downloadAllBy(EdoContainer container, OnProgressChangeListener listener) {
        return downloadAllBy(container,-1,listener);
    }

    public LActivity[] downloadAllBy(EdoContainer container, int offset, OnProgressChangeListener listener) {
        LActivity[] activities = activityDAO.downloadAllBy(container, offset, true, listener);

        if(activities!=null && activities.length>0){
            LActivity a = new LActivity();
            a.setEvent(LActivity.EVENT_SYNC_ALL);
            a.setStatus(LActivity.STATUS_COMPLETE);
            app.uiManager.syncResourcesList(a, UIManager.ActivitiesUpdatesListener.class);
        }

        return activities;
    }




    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		DERIVED
    //*****************************************************************************************************************************************************************************************************************************************************************************************************

    //READ/UNREAD
    public synchronized boolean setAllActivitiesRedBy(String contactId, String edoUserId) {
        return getLDAO().setAllActivitiesRedBy(contactId,edoUserId);
    }
}
