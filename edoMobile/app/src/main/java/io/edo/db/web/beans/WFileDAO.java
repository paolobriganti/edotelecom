package io.edo.db.web.beans;

import android.content.Context;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.request.EdoRequest;


public class WFileDAO {


    public static WEntity updateFile(Context context, EdoContainer container, LFile localFile){
        return updateFile(context,container,localFile,false);
    }

    public static WEntity moveFile(Context context, EdoContainer container, LFile localFile){
        return updateFile(context, container,localFile,true);
    }

	public static WEntity updateFile(Context context, EdoContainer container, LFile localFile, boolean move){

		if(localFile.isNotSavedToServer()){
			return null;
		}

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_FILE + EdoRequest.PATH_SEPARATOR + localFile.getId()
				+EdoRequest.QUERY_SEPARATOR+ "cid="+container.getContainerId()
				;

		String body = localFile.getWebFileJson();

		if(move){
			targetURL = targetURL + "?move=1";
		}

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, body);

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.file = localFile;
            }
            return entity;
        }
        return null;
	}

	public static WEntity updateContentOnly(Context context, LFile localFile, boolean editing){

		if(localFile.isNotSavedToServer()){
			return null;
		}

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_FILE + EdoRequest.PATH_SEPARATOR + localFile.getId() + EdoRequest.PATH_CONTENT;

		String body = localFile.getContent();

		if(editing){
			targetURL = targetURL + "?editing=1";
		}
//		else{
//			targetURL = targetURL + "?commit=1";
//		}

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, body);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			WEntity entity = WEntity.fromJson(response);
			if(entity==null){
				entity = new WEntity();
				entity.file = localFile;
			}
			return entity;
		}
		return null;
	}
	

	public static WEntity deleteFile(Context context, LSpace space, LFile localFile, boolean removeFromCloud){

		if(localFile.isNotSavedToServer()){
			return null;
		}

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_FILE + EdoRequest.PATH_SEPARATOR + localFile.getId() +
				  "?deskId=" + space.getId();

		if(localFile.getCloudId()!=null){

			int keep = removeFromCloud? 0:1;
			targetURL += "&keep=" + keep;

			LService lservice = space.getService(context,true);
			if(lservice!=null){
				targetURL += "&serviceId=" + lservice.getId();
			}
		}

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_DELETE, targetURL, null);

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.file = localFile;
            }
            return entity;
        }
        return null;
	}

	public static LFile getFile(final Context context, String fileId){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_FILE + EdoRequest.PATH_SEPARATOR + fileId;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			return LFile.fromJson(response);
		}
		return null;
	}













    public static LFile updateValuesFromWeb(LFile lFile, LFile wFile){
        if(lFile!=null && wFile!=null) {
            String id = wFile.getId() != null && !wFile.getId().equalsIgnoreCase("None") ? wFile.getId() : lFile.getId();
            lFile.setId(id);
			String cloudId = wFile.getCloudId() != null && !wFile.getCloudId().equalsIgnoreCase("None") ? wFile.getCloudId() : lFile.getCloudId();
			lFile.setCloudId(cloudId);
            return lFile;
        }
        return lFile;
    }














    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		THUMBANIL
    //*****************************************************************************************************************************************************************************************************************************************************************************************************



    public static final int MAX_SIDE_THUMB_SIZE_DP = 150;



	public static String uploadThumbail(Context context, String thumbPath){
		if(thumbPath!=null && new File(thumbPath).exists()){
			String fileNameWithExt = FilenameUtils.getName(thumbPath);
			if(!fileNameWithExt.endsWith(".png")){
				fileNameWithExt += ".png";
			}
			String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_THUMBNAIL+fileNameWithExt;
			EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, thumbPath);
			String response = request.send(context);
			if(!EdoRequest.hasEdoErrors(response)){
				WEntity entity = WEntity.fromJson(response);
				if(entity!=null && entity.url!=null){
					return entity.url;
				}
			}
		}
		return null;
	}


}
