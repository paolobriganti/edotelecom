package io.edo.db.local.beans.support;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class CloudIdServiceId {

	public String cloudId = null;
	public String serviceId = null;



	public CloudIdServiceId(String cloudId, String serviceId) {
		super();
		this.cloudId = cloudId;
		this.serviceId = serviceId;
	}

	public static String toJson(ArrayList<CloudIdServiceId> cloudIdServiceId){
		CloudIdServiceId[] array = new CloudIdServiceId[cloudIdServiceId.size()];
		cloudIdServiceId.toArray(array);
		return toJson(array);
	}

	public static String toJson(CloudIdServiceId[] cloudIdServiceId){
		return new Gson().toJson(cloudIdServiceId);
	}


	public static ArrayList<CloudIdServiceId> fromJsonToList(String json){
		if(json!=null){
			CloudIdServiceId[] array = fromJson(json);
			if(array!=null)
				return new ArrayList<CloudIdServiceId>(Arrays.asList(array));
		}
		return null;
	}
	public static CloudIdServiceId[] fromJson(String json){
		if(json!=null){
			try{
				return new Gson().fromJson(json, CloudIdServiceId[].class);
			}catch (com.google.gson.JsonSyntaxException e){
				eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "CloudIdServiceId[]:\n"+e.getMessage());
				return null;
			}
		}
		return null;
	}

	public static String addCloudIdServiceIdAndReturnJson(String oldCloudIdServiceIds, CloudIdServiceId cloudIdServiceId){
		if(oldCloudIdServiceIds!=null){
			ArrayList<CloudIdServiceId> cloudIdServiceIds = fromJsonToList(oldCloudIdServiceIds);
			if(cloudIdServiceIds!=null){
				cloudIdServiceIds.add(cloudIdServiceId);
				return toJson(cloudIdServiceIds);
			}else{
				cloudIdServiceIds = new ArrayList<CloudIdServiceId>();
				cloudIdServiceIds.add(cloudIdServiceId);
				return toJson(cloudIdServiceIds);
			}
		}
		return null;
	}

	public static String setCloudIdServiceIdAndReturnJson(String oldCloudIdServiceIds, CloudIdServiceId cloudIdServiceId){
		if(oldCloudIdServiceIds!=null){
			ArrayList<CloudIdServiceId> cloudIdServiceIds = fromJsonToList(oldCloudIdServiceIds);
			if(cloudIdServiceIds!=null){
				boolean set = false;
				for(int i=0; i<cloudIdServiceIds.size(); i++){
					if(cloudIdServiceId.serviceId.equals(cloudIdServiceIds.get(i).serviceId)){
						cloudIdServiceIds.set(i, cloudIdServiceId);
						set = true;
						break;
					}
				}
				if(!set){
					cloudIdServiceIds.add(cloudIdServiceId);
				}
				return toJson(cloudIdServiceIds);
			}else{
				cloudIdServiceIds = new ArrayList<CloudIdServiceId>();
				cloudIdServiceIds.add(cloudIdServiceId);
				return toJson(cloudIdServiceIds);
			}
		}
		return null;
	}
	
	public static String removeCloudIdServiceIdAndReturnJson(String oldCloudIdServiceIds, String cloudId){
		if(oldCloudIdServiceIds!=null){
			ArrayList<CloudIdServiceId> multiCloudIds = fromJsonToList(oldCloudIdServiceIds);
			if(multiCloudIds!=null){
				for(CloudIdServiceId mci:multiCloudIds){
					if(mci.cloudId.equals(cloudId)){
						multiCloudIds.remove(mci);
						return toJson(multiCloudIds);
					}
				}
			}
		}
		return oldCloudIdServiceIds;
	}

	public static String getCloudId(String cloudIdServiceIds, String serviceId){
		if(cloudIdServiceIds!=null){
			ArrayList<CloudIdServiceId> multiCloudIds = fromJsonToList(cloudIdServiceIds);
			if(multiCloudIds!=null)
				for(CloudIdServiceId mci:multiCloudIds){
					if(mci.serviceId.equals(serviceId))
						return mci.cloudId;
				}
		}
		return null;
	}
}
