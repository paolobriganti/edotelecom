package io.edo.db.web.beans;

import android.content.Context;

import io.edo.db.web.request.EdoRequest;

public class WMessageDAO {



	public static WEntity sendMessage(Context context, WMessage message){
		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_REALTIME;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, message.toJson());


		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			WEntity entity = WEntity.fromJson(response);
			if(entity!=null) {
				return entity;
			}
		}
		return null;
	}
}
