package io.edo.db.web.beans;

import android.content.Context;

import io.edo.db.web.request.EdoRequest;

public class WDeviceDAO {



	public static boolean updateDevice(Context context, Device device){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_USER_DEVICE + EdoRequest.PATH_SEPARATOR + device.get_id();

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, device.toJson());

		String response = request.send(context);

		return !EdoRequest.hasEdoErrors(response);
	}
}
