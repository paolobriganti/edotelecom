package io.edo.db.local.beans.support;

import android.content.Context;

import java.io.Serializable;

public interface EdoContainer extends Serializable{

	boolean isPersonal(String userId);
	boolean isPersonal(Context c);
	boolean isContact();
	boolean isGroup();
	boolean isContactOrGroup();
	
	String getId();
	String getEdoUserId();
	String getContainerId();
	int getSectionNumber();
	
	String getName();
	String getNameComplete();
	String getTrackSectionName();
	
	String getPhysicalFolderPath(Context c);

	String regenerateCloudTab(Context c, LSpace space);
	
	boolean equals(Object obj);
	
}
