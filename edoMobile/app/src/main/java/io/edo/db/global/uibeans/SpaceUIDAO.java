package io.edo.db.global.uibeans;

import android.content.Context;

import io.edo.db.global.beans.SpaceDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LSpaceDAO;
import io.edo.db.web.beans.WEntity;


public class SpaceUIDAO extends BaseUIDAO{
    SpaceDAO spaceDAO;

    public SpaceUIDAO(Context context) {
        super(context);
        spaceDAO = new SpaceDAO(context);
    }

    @Override
    public LSpaceDAO getLDAO() {
        return spaceDAO.getLDAO();
    }


    //***************************
    //		Contacts Spaces		*
    //***************************

    public LActivity addNewSpaceToContact(final LContact contact, LSpace space){
        WEntity entity = spaceDAO.addNewSpaceToContact(contact, space);

        return processActivity(entity);
    }


    public LSpace initDefaultSpace(LContact contact){
        WEntity entity = init(contact);
        if(entity!=null){

            if(entity.act!=null){
                entity.act.setResource(entity.file);
                processActivity(entity);
            }

            return (LSpace)entity.file;
        }
        return null;
    }

    public LContact initContact(LContact contact){
        WEntity entity = init(contact);
        if(entity!=null){

            if (entity.act!=null){
                entity.act.setResource(entity.file);
                processActivity(entity);
            }

            return entity.contact;
        }
        return null;
    }

    private WEntity init(LContact contact){
        ContactUIDAO contactDAO = new ContactUIDAO(context);

        if(contact.isNewForServer()){
            LContact newContact = contactDAO.add(contact, null);
            if(newContact!=null){
                contact = newContact;
                if(!contact.isActive()){
                    contactDAO.activeContact(contact);
                    WEntity entity = spaceDAO.addDefaultSpaceTo(contact);
                    if(entity!=null){
                        entity.contact = contact;
                    }
                    return entity;
                }
            }
        }

        contactDAO.activeContact(contact);

        int filesNumber = spaceDAO.getFilesOf(contact, null, false);

        boolean canCreateDefaultSpace = contact.isContact() || (
                contact.isGroup() && contact.isUserOwnerOfTheGroup(app.getEdoUserId())
                );

        if(filesNumber<=0 && canCreateDefaultSpace){
            WEntity entity = spaceDAO.addDefaultSpaceTo(contact);
            if(entity!=null){
                entity.contact = contact;
            }
            return entity;
        } else {
            LSpace defaultSpace = spaceDAO.getLDAO().getContactDefaultSpace(contact);

            if(defaultSpace!=null){
                WEntity entity = new WEntity();
                entity.file = defaultSpace;

//                LActivity act = spaceDAO.getFakeActivity(LActivity.EVENT_INSERT_SPACE, contact, defaultSpace);
//                act = spaceDAO.addExtraToActivity(act, contact, defaultSpace, defaultSpace);
//                if(defaultSpace.getCreatedBy()!=null){
//                    act.setSenderId(defaultSpace.getCreatedBy());
//                }
//                entity.act = act;

                entity.contact = contact;


                new ActivityUIDAO(context).downloadAllBy(contact,null);


                return entity;
            }else if(canCreateDefaultSpace){
                WEntity entity = spaceDAO.addDefaultSpaceTo(contact);
                if(entity!=null){
                    entity.contact = contact;
                }
                return entity;
            }

        }
        return null;
    }
}
