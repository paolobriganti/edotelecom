package io.edo.db.global.uibeans;

import android.content.Context;

import io.edo.db.global.beans.MessageDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.web.beans.WEntity;
import io.edo.db.web.beans.WMessage;

public class MessageUIDAO extends BaseUIDAO{
    MessageDAO messageDAO;

    public MessageUIDAO(Context context) {
        super(context);
        messageDAO = new MessageDAO(context);
    }

    @Override
    public LActivityDAO getLDAO() {
        return messageDAO.getLDAO();
    }

    public LActivity sendMessage(LContact container, final String messageText){

        final WMessage message = new WMessage(context, container, messageText);

        final LActivity messageActivity = new LActivity(app.getEdoUserId(),message);

        return sendMessage(messageActivity);
    }


    public LActivity sendMessage(final LActivity messageActivity){

        processActivity(messageActivity);

        new Thread(){@Override public void run(){
            WEntity entity = messageDAO.sendMessage(messageActivity);
            processActivity(entity);
        }}.start();

        return messageActivity;
    }

}
