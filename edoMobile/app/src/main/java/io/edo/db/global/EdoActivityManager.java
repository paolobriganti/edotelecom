package io.edo.db.global;

import android.content.Context;

import com.paolobriganti.android.AUtils;
import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.global.beans.ContactDAO;
import io.edo.db.global.beans.ContactFileDAO;
import io.edo.db.global.beans.FileDAO;
import io.edo.db.global.beans.GroupDAO;
import io.edo.db.global.beans.ServiceDAO;
import io.edo.db.global.beans.StatusDAO;
import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LActivityDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LGroupMember;
import io.edo.db.local.beans.LGroupMemberDAO;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.support.EdoMessageBuilder;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.beans.WStatus;
import io.edo.edomobile.Act_ContactActiveList;
import io.edo.edomobile.ThisApp;
import io.edo.ui.EdoNotification;
import io.edo.ui.UIManager;
import io.edo.ui.imageutils.ThumbnailManager;
import io.edo.utilities.eLog;

/**
 * Created by PaoloBriganti on 26/06/15.
 */
public class EdoActivityManager {
    ThisApp app;
    Context context;
    UIManager uiManager;

    LActivityDAO lActivityDAO;


    public EdoActivityManager(Context context) {
        this.context = context;
        this.app = ThisApp.getInstance(context);
        this.uiManager = app.uiManager;
        this.lActivityDAO = new LActivityDAO(context);
    }

    public static LActivity processActivity(Context context, String jsonActivity){
        eLog.i("EdoActivityMan", "PUSH ARRIVED: \n" + jsonActivity);

        EdoActivityManager edoActivityManager = new EdoActivityManager(context);
        LActivity activity = LActivity.fromJson(jsonActivity);
        eLog.i("EdoActivityMan", "ACTIVITY ARRIVED: \n" + activity.getId());
        if(activity!=null){
            activity.setStatus(LActivity.STATUS_COMPLETE);
            activity.setIsUnread(!activity.isFromCurrentEdoUser(context));
            return edoActivityManager.processActivity(activity);
        }
        AUtils.sendPushNotificationHeartBeat(context);

        return null;
    }

    public LActivity processActivity(LActivity a){
        return processActivity(a,true);
    }

    public LActivity processActivity(LActivity a, boolean saveToDB){
        return processActivity(a,saveToDB,false);
    }

    public LActivity processActivity(LActivity a, boolean saveToDB, boolean isInit){
        return processActivity(a,saveToDB,isInit, null);
    }

    public LActivity processActivity(final LActivity a, boolean saveToDB, boolean isInit, Callback callback){

        app.clearContacts();

        //CONTAINER/SENDER
        if(!isInit)
            a.loadContainerSenderFromDB(context,true);


        //RESOURCE
        Class<?> listenerClass = UIManager.ActivitiesUpdatesListener.class;
        if(a.getResourceType()!=LActivity.VALUE_ERROR) {
            switch (a.getResourceType()) {
                case LActivity.RESOURCE_CONTACT:

                    eLog.w("EdoActivityMan", "RESOURCE_CONTACT");
                    ContactDAO cdao = new ContactDAO(context);
                    if(!isInit) {
                        if (a.getEvent() == LActivity.EVENT_DELETE) {

                            if(!isInit && a.getResource()==null){
                                LContact contact = cdao.getLDAO().get(a.getResourceId());
                                cdao.getLDAO().delete(contact);
                                a.setResource(contact);
                            }

                            eLog.v("EdoActivityMan", "DELETE RESOURCE_CONTACT");

                        } else if (a.getResource() == null) {

                            a.setResource(cdao.download(a.getResourceId()));

                            eLog.v("EdoActivityMan", "DOWNLOAD RESOURCE_CONTACT");

                            if (a.getResource() == null){
                                return null;
                            }
                        }
                    }


                    listenerClass = UIManager.ContactsUpdatesListener.class;
                    break;
                case LActivity.RESOURCE_GROUP:

                    eLog.w("EdoActivityMan", "RESOURCE_GROUP");

                    GroupDAO gdao = new GroupDAO(context);

                    if(!isInit) {
                        if (a.getEvent() == LActivity.EVENT_DELETE) {


                            ContactDAO c2dao = new ContactDAO(context);

                            if(!isInit && a.getResource()==null){
                                LContact group = c2dao.getLDAO().get(a.getResourceId());
                                c2dao.getLDAO().delete(group);
                                a.setResource(group);
                            }

                            a.setIsUnread(false);

                            eLog.v("EdoActivityMan", "DELETE RESOURCE_GROUP");

                        } else if (a.getResource() == null) {

                            a.setResource(gdao.download(a.getResourceId()));

                            eLog.v("EdoActivityMan", "DOWNLOAD RESOURCE_GROUP");

                            if (a.getResource() == null){
                                return null;
                            }
                        }
                    }

                     EdoMessageBuilder.setMessagesOfResourceGroupToActivity(context, a, 1);
                    listenerClass = UIManager.ContactsUpdatesListener.class;
                    break;
                case LActivity.RESOURCE_FILE:
                    if(a.getContainer()==null)
                        return null;

                    boolean isSpace = false;

                    eLog.w("EdoActivityMan", "RESOURCE_FILE");

                    FileDAO fdao = new FileDAO(context);

                    if(!isInit) {
                        if (a.getEvent() == LActivity.EVENT_DELETE) {

                            if(!isInit && a.getResource()==null){
                                LFile file = fdao.getLDAO().get(a.getResourceId());
                                if(file!=null) {
                                    fdao.getLDAO().delete(file);
                                    a.setResource(file);
                                }
                            }

                            eLog.v("EdoActivityMan", "DELETE RESOURCE_FILE ");

                        } else if (a.getResource() == null) {

                            a.setResource(new ContactFileDAO(context).downloadFileOf((LContact) a.getContainer(), a.getResourceId()));

                            eLog.v("EdoActivityMan", "DOWNLOAD RESOURCE_FILE " + a.getResource());

                            if (a.getResource() != null) {
                                LFile file = (LFile) a.getResource();
                                if (!file.isSpace()) {
                                    a.setSpace(file.getSpace(context));
                                }
                            }

                            if (a.getResource() == null){
                                return null;
                            }
                        }
                    }


                    if (a.getResource() instanceof LSpace){
                        isSpace = true;
                        if(((LSpace)a.getResource()).isDefault()){
                            a.setStatus(LActivity.STATUS_DEFAULT_SPACE);
                        }

                    }else if(a.getResource() instanceof LFile){

                        LFile file = (LFile)a.getResource();

                        if(file!=null && file.getvType().equals(LFile.FILE_TYPE_EDOTAB)) {
                            isSpace = true;
                            LSpace space = new LSpace(context, (LContact) a.getContainer(), file);
                            a.setResource(space);
                            if(space.isDefault())
                                a.setStatus(LActivity.STATUS_DEFAULT_SPACE);

                        }
                    }


                    eLog.v("EdoActivityMan", "isSpace: "+isSpace+" is default: "+a.isDefaultSpace());

                    if(a.getContainer()!=null && ((LContact)a.getContainer()).isHidden()){
                        LContact contact = ((LContact)a.getContainer());
                        contact.setStatus(LContact.STATUS_ACTIVE);
                        new LContactDAO(context).update(contact);
                    }

                    EdoMessageBuilder.setMessagesOfResourceFileToActivity(context, a, 1);
                    if(!isSpace)
                        listenerClass = UIManager.FilesUpdatesListener.class;
                    else {
                        listenerClass = UIManager.SpacesUpdatesListener.class;
                    }
                    break;
                case LActivity.RESOURCE_MEMBER:

                    eLog.w("EdoActivityMan", "RESOURCE_MEMBER");

                    ContactDAO cmdao = new ContactDAO(context);

                    if(!isInit) {
                        if (a.getEvent() == LActivity.EVENT_LEAVE) {

                            LContact member = cmdao.getLDAO().get(a.getResourceId());
                            if(member!=null) {
                                eLog.v("EdoActivityMan", "LEAVE RESOURCE_MEMBER: " + member.getName());
                                a.setResource(member);
                            }

                        } else if (a.getResource() == null) {
                            if(!a.getResourceId().equals(app.getEdoUserId())){
                                a.setResource(cmdao.download(a.getResourceId()));
                                eLog.v("EdoActivityMan", "DOWNLOAD RESOURCE_MEMBER");
                            }else{
                                eLog.v("EdoActivityMan", "IS CURRENT RESOURCE_MEMBER");
                                a.setResource(app.getUserContact());
                            }

                            if (a.getResource() == null){
                                return null;
                            }

                        }
                    }

                    EdoMessageBuilder.setMessagesOfResourceMemberToActivity(context, a);
                    listenerClass = UIManager.ContactsUpdatesListener.class;
                    break;
                case LActivity.RESOURCE_SERVICE:

                    eLog.w("EdoActivityMan", "RESOURCE_SERVICE");

                    ServiceDAO sdao = new ServiceDAO(context);

                    if(!isInit) {
                        if (a.getEvent() == LActivity.EVENT_DELETE) {

                            boolean delete = sdao.getLDAO().delete(new LService(a.getResourceId()));

                            a.setResource(new LService(a.getResourceId()));

                            eLog.v("EdoActivityMan", "DELETE RESOURCE_MEMBER: "+delete);

                        } else if (a.getResource() == null) {

                            sdao.downloadAllServices();

                            eLog.v("EdoActivityMan", "DOWNLOAD RESOURCE_SERVICE");

                            if (a.getResource() == null){
                                return null;
                            }
                        }
                    }


                    break;

                case LActivity.RESOURCE_STATUS:

                    eLog.w("EdoActivityMan", "RESOURCE_STATUS");

                    if(!isInit) {
                        if (a.getResource() == null) {
//                            a.setEvent(LActivity.VALUE_ERROR);

                            processStatus(a);

//                            if (a.getResource() == null){
//                                return null;
//                            }
                        }
                    }



                    break;
            }
        }

        final Class<?> listenerCls = listenerClass;
        Runnable updateUIRunnable = null;


        //EVENT
        if(a.getEvent()!=LActivity.VALUE_ERROR) {
            switch (a.getEvent()) {
                case LActivity.EVENT_INSERT:
                case LActivity.EVENT_HIGHLIGHT_CONTACT:

                    eLog.i("EdoActivityMan", "EVENT_INSERT");

                    if (a.getResource() != null) {
                        if (a.getResource() instanceof LFile && ((LFile) a.getResource()).isSpace()) {
                            updateUIRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    uiManager.insertResource(a, UIManager.SpacesUpdatesListener.class);
                                }
                            };

                        } else {
                            updateUIRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    uiManager.insertResource(a, listenerCls);
                                }
                            };


                        }
                    }
                    break;
                case LActivity.EVENT_DELETE:

                    eLog.i("EdoActivityMan", "EVENT_DELETE");

                    updateUIRunnable = new Runnable() {
                        @Override
                        public void run() {
                            uiManager.deleteResource(a, listenerCls);
                        }
                    };

                    break;
                case LActivity.EVENT_UPDATE:

                    eLog.i("EdoActivityMan", "EVENT_UPDATE");

                    if (a.getResource() != null) {
                        ThumbnailManager.removeThumbanilFromMemory(context, a.getResourceId());
                        updateUIRunnable = new Runnable() {
                            @Override
                            public void run() {
                                uiManager.updateResource(a, listenerCls);
                            }
                        };
                    }

                    if(!processSubEvent(a))
                        return null;

                    break;
                case LActivity.EVENT_COMMENT_INSERT:

                    eLog.i("EdoActivityMan", "EVENT_COMMENT_INSERT");

                    updateUIRunnable = new Runnable() {
                        @Override
                        public void run() {
                            uiManager.updateResource(a, listenerCls);
                        }
                    };

                    break;
                case LActivity.EVENT_COMMENT_UPDATED:

                    eLog.i("EdoActivityMan", "EVENT_COMMENT_UPDATED");

                    updateUIRunnable = new Runnable() {
                        @Override
                        public void run() {
                            uiManager.updateResource(a, listenerCls);
                        }
                    };

                    break;
                case LActivity.EVENT_INSERT_SPACE:

                    eLog.i("EdoActivityMan", "EVENT_INSERT_SPACE");

                    updateUIRunnable = new Runnable() {
                        @Override
                        public void run() {
                            uiManager.insertResource(a, UIManager.SpacesUpdatesListener.class);
                        }
                    };

                    break;
                case LActivity.EVENT_JOIN:

                    eLog.i("EdoActivityMan", "EVENT_JOIN");

                    if(a.getResourceId().equals(app.getEdoUserId())){
                        LContact group = (LContact)a.getContainer();
                        if(group!=null){
                            a.setResource(group);
                            a.setContext(LActivity.CONTEXT_GROUP);
                            a.setResourceType(LActivity.RESOURCE_GROUP);
                            a.setEvent(LActivity.EVENT_INSERT);

                            eLog.i("EdoActivityMan", "NEW GROUP ARRIVED: " + group.getName());

                            updateUIRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    uiManager.insertResource(a, listenerCls);
                                }
                            };
                        }
                    }else{
                        LGroupMemberDAO mdao = new LGroupMemberDAO(context);
                        LContact group = (LContact)a.getContainer();
                        LContact member = (LContact)a.getResource();

                        if(group!=null){
                            a.setResource(group);
                            a.setContext(LActivity.CONTEXT_GROUP);
                            a.setResourceType(LActivity.RESOURCE_GROUP);
                            a.setEvent(LActivity.EVENT_UPDATE);

                            if(mdao.insertFromWebServer(new LGroupMember(group,member))) {
                                eLog.i("EdoActivityMan", "NEW MEMBER INSERTED TO GROUP: "+group.getName());

                                updateUIRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        uiManager.updateResource(a, listenerCls);
                                    }
                                };
                            }
                        }

                    }
                    break;
                case LActivity.EVENT_LEAVE:

                    eLog.i("EdoActivityMan", "EVENT_LEAVE");

                    eLog.i("EdoActivityMan", a.getResourceId()+" == "+app.getEdoUserId());

                    if(a.getResourceId().equals(app.getEdoUserId())){
                        LContact group = (LContact)a.getContainer();
                        if(group!=null){
                            if(new LContactDAO(context).delete(group)){
                                a.setResource(group);
                                a.setContext(LActivity.CONTEXT_GROUP);
                                a.setResourceType(LActivity.RESOURCE_GROUP);
                                a.setEvent(LActivity.EVENT_DELETE);


                                a.setIsUnread(false);

                                eLog.i("EdoActivityMan", "GROUP " + group.getName()+" DELETED");

                                updateUIRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        uiManager.deleteResource(a, listenerCls);
                                    }
                                };
                            }
                        }
                    }else{
                        LGroupMemberDAO mdao = new LGroupMemberDAO(context);
                        LContact group = (LContact)a.getContainer();
                        if(group!=null && a.getResource()!=null){
                            if(mdao.delete(new LGroupMember(group, (LContact)a.getResource()))){
                                eLog.v("EdoActivityMan", "DELETE RESOURCE_MEMBER: " + a.getResource().getName());
                                ArrayList<LContact> members = mdao.getContactsOfGroup(group.getId());
                                group.setGroupMembers(context, members);
                                a.setResource(group);
                                a.setContext(LActivity.CONTEXT_GROUP);
                                a.setResourceType(LActivity.RESOURCE_GROUP);
                                a.setEvent(LActivity.EVENT_UPDATE);


                                eLog.i("EdoActivityMan", "DELETE RESOURCE_MEMBER: " + a.getResource().getName()+" OF GROUP " + group.getName() + " DELETED");

                                updateUIRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        uiManager.updateResource(a, listenerCls);
                                    }
                                };
                            }
                        }
                    }


                    break;
                case LActivity.EVENT_MESSAGE:

                    eLog.i("EdoActivityMan", "EVENT_MESSAGE "+a.getId());
                    EdoMessageBuilder.setMessagesOfChatToActivity(context, a);


                    updateUIRunnable = new Runnable() {
                        @Override
                        public void run() {
                            uiManager.insertResource(a, listenerCls);
                        }
                    };

                    break;
                case LActivity.EVENT_UPLOAD_TO_CLOUD:

                    eLog.i("EdoActivityMan", "EVENT_UPLOAD_TO_CLOUD");
                    saveToDB = false;

                    updateUIRunnable = new Runnable() {
                        @Override
                        public void run() {
                            uiManager.uploadToCloud(a);
                        }
                    };

                    break;
                case LActivity.EVENT_DOWNLOAD_FROM_CLOUD:

                    eLog.i("EdoActivityMan", "EVENT_DOWNLOAD_FROM_CLOUD");
                    saveToDB = false;

                    updateUIRunnable = new Runnable() {
                        @Override
                        public void run() {
                            uiManager.downloadFromCloud(a);
                        }
                    };

                    break;
            }
        }

        saveToDB = saveToDB && a.hasToBeSavedOnLocalDB();

        if(saveToDB) {
            lActivityDAO.insertFromWebServer(a);
        }else{
            eLog.w("EdoActivityMan", "ACTIVITY NOT SAVED");
        }


        if(!isInit && a.getContainer()!=null &&
                a.canBeShownOnTimeline() &&
                a.getEvent() != LActivity.EVENT_HIGHLIGHT_CONTACT &&
                a.getSender()!=null && a.getSender().isContactOrGroup() &&
                !a.isPersonal()){

            new EdoNotification(context, Act_ContactActiveList.class).showContactsNotification(a);

            boolean contactHighlighted = new ContactUIDAO(context).highlightContact((LContact) a.getContainer(),System.currentTimeMillis(), true);
            eLog.w("EdoActivityMan", "contactHighlighted " + contactHighlighted);
        }

        if(callback!=null){
            callback.onProcessFinished();
        }
        eLog.w("EdoActivityMan", "START UI UPDATE "+a.getId());
        if(updateUIRunnable!=null)
            updateUIRunnable.run();

        return a;
    }







    public boolean processSubEvent(LActivity a){
        if(a.getSubEvent()!=LActivity.VALUE_ERROR) {
            switch (a.getSubEvent()) {
                case LActivity.SUB_EVENT_RENAMED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_RENAMED");
                    break;

                case LActivity.SUB_EVENT_MOVED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_MOVED");
                    break;

                case LActivity.SUB_EVENT_ICON_MODIFIED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_ICON_MODIFIED");
                    break;

                case LActivity.SUB_EVENT_PENDING_USER_ADDED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_PENDING_USER_ADDED");
                    break;

                case LActivity.SUB_EVENT_PENDING_USER_REMOVED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_PENDING_USER_REMOVED");
                    break;

                case LActivity.SUB_EVENT_STARRED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_STARRED");
                    break;

                case LActivity.SUB_EVENT_UNSTARRED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_UNSTARRED");
                    break;

                case LActivity.SUB_EVENT_CONTENT_CHANGED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_CONTENT_CHANGED");
                    break;
                case LActivity.SUB_EVENT_EDITING:

                    eLog.i("EdoActivityMan", "SUB_EVENT_EDITING");
                    break;

                case LActivity.SUB_EVENT_CONTENT_COMMITTED:

                    eLog.i("EdoActivityMan", "SUB_EVENT_CONTENT_COMMITTED");
                    break;

                case LActivity.SUB_EVENT_MOVE_OUT:

                    eLog.i("EdoActivityMan", "SUB_EVENT_MOVE_OUT");
                    break;

                case LActivity.SUB_EVENT_MOVE_IN:

                    eLog.i("EdoActivityMan", "SUB_EVENT_MOVE_IN");
                    break;

            }
        }
        return true;
    }





    private void processStatus(LActivity as){
        String statusJson = as.getMessage();
        eLog.i("EdoActivityMan", "processStatus: "+statusJson);
        if(JavaUtils.isNotEmpty(statusJson)){
            StatusDAO statusDAO = new StatusDAO(context);

            WStatus status = statusDAO.updateLocalData(statusJson);
            eLog.d("EdoActivityMan", "status: "+status);

            if(status==null)
                status = WStatus.fromJson(statusJson);

            if(status!=null){
                eLog.v("EdoActivityMan", "PROCESS STATUS: "+status.getLastChatStateList().toString());

                EdoNotification.cancelNotification(context, EdoNotification.ID_CONTACTS);
                as.setResource(statusDAO.updateLocalData(status,true));
            }
        }
    }





    public interface Callback {
        public void onProcessFinished();
    }


}
