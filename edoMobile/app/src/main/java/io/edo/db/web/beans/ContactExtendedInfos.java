package io.edo.db.web.beans;

import com.google.gson.Gson;

import java.io.Serializable;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class ContactExtendedInfos implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public String serviceId;
	public String cloudId;
	
	public String toJson(){
		return new Gson().toJson(this);
	}
	
	public static ContactExtendedInfos fromJson(String json){
		try{
			return new Gson().fromJson(json, ContactExtendedInfos.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "ExtendedInfos:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "ExtendedInfos JSON:\n"+json);
			return null;
		}
			
	}
}
