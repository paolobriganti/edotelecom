package io.edo.db.web.request;

import com.google.gson.Gson;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LUser;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;
public class LoginWG3Resp {
	public static final String FILE_NAME = "LWG_RESP_1";
	
	public LUser user;
	public LContact contact;
	public boolean registration;

	public String toJson(){
		return new Gson().toJson(this);
	}

	public static LoginWG3Resp fromJson(String json){
		try{
			return new Gson().fromJson(json, LoginWG3Resp.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LoginWG3Resp:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "LoginWG3Resp JSON:\n"+json);
			return null;
		}
	}
	
}
