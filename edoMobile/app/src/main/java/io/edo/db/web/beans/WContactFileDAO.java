package io.edo.db.web.beans;

import android.content.Context;

import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.db.global.beans.GroupDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.CloudIdServiceId;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.web.request.EdoRequest;

public class WContactFileDAO {


	public static WEntity addFileToContact(Context context, LContact contact, LSpace spaceWithParentFolder, LFile file){
		String targetURL = getURLPrefix(context,contact) +
                EdoRequest.PATH_FILES +"?sendEmail=1";

		String parentCloudId = null;

//		eLog.i("EDO","CONTACT ID: "+contact.getCloudIdServiceIdJsonList());

		if(file!=null && !file.isSpace() && spaceWithParentFolder!=null && spaceWithParentFolder.getCurrentFolder()!=null){
            parentCloudId = spaceWithParentFolder.getCurrentFolder().getCloudId()!=null ? spaceWithParentFolder.getCurrentFolder().getCloudId() : "null";
	        file.setParentCloudId(parentCloudId);
            file.setServiceId(spaceWithParentFolder.getServiceId());
        }else if(file!=null && file.isSpace()){

			if(contact.isGroup()){
				String cloudIdServiceIdJsonList = contact.getCloudIdServiceIdJsonList();
				if(cloudIdServiceIdJsonList==null){
					LContact group = new GroupDAO(context).download(contact.getId());
					if(group!=null){
						contact.setCloudIdServiceIdJsonList(group.getCloudIdServiceIdJsonList());
						cloudIdServiceIdJsonList = contact.getCloudIdServiceIdJsonList();
					}
				}

				if(cloudIdServiceIdJsonList!=null){
					ArrayList<CloudIdServiceId> list = contact.getCloudIdServiceId();
					if(list!=null && list.size()>0){
						file.setParentCloudId(list.get(0).cloudId); //SOLUZIONE MOMENTANEA VISTA LA PRESENZA DI UN UNICO SERVIZIO
					}
				}else if(file.getStatus()!=null && file.getStatus().equals(LFile.STATUS_DEFAULT)){
					file.setParentCloudId("null");
				}

				if(file.getParentCloudId()==null)
					return null;

			}else{
				String contactCloudId = contact.getCloudId(file.getServiceId());
				parentCloudId = contactCloudId!=null ? contactCloudId : "null";
				file.setParentCloudId(parentCloudId);

				if(file.getParentCloudId()==null)
					return null;

			}
		}


		if(file!=null){

			EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, file.getWebFileJson());
            String response = request.send(context);
			if(!EdoRequest.hasEdoErrors(response)){
                WEntity entity = WEntity.fromJson(response);
                if(entity==null){
                    entity = new WEntity();
                    entity.file = file;
                }

				if(entity.file!=null){
					if(file!=null){
						if(entity.file.getCloudId()!=null)
							file.setCloudId(entity.file.getCloudId());

						if(file.isFolder() && JavaUtils.isEmpty(file.getCloudId()))
							return null;
						else {
							entity.file = file;
							return entity;
						}
					}else if(spaceWithParentFolder!=null){
						spaceWithParentFolder.setCloudId(entity.file.getCloudId());

						if(JavaUtils.isEmpty(spaceWithParentFolder.getCloudId()))
							return null;
						else{
							entity.file = spaceWithParentFolder;
							return entity;
						}
					}
				}
			}
		}


		return null;
	}


	public static WEntity copyFileToContact(Context context, LContact contact, LSpace toSpace, String oldFileId, LFile newFile){


		String targetURL = getURLPrefix(context, contact) +
                EdoRequest.PATH_FILES + "?copy="+oldFileId;

//        String[] collabsEmails = (toSpace.getCollabsEmails()!=null && toSpace.getCollabsEmails().length>0)? toSpace.getCollabsEmails() : new String[0];

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, newFile.getWebFileJson());
		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.file = newFile;
            }
			if(entity.file!=null){
				newFile.setWebUri(entity.file.getWebUri());
				newFile.setCloudId(entity.file.getCloudId());
				entity.file = newFile;
				return entity;
			}
		}
		return null;
	}


	public static LFile[] getContactFiles(Context context, LContact contact){

        String targetURL = getURLPrefix(context,contact) +
                            EdoRequest.PATH_FILES;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			return LFile.fromJsonToArray(response);
		}
		return null;
	}


	public static LFile[] getContactFolderFiles(Context context, LContact contact, String folderId){

        String targetURL = getURLPrefix(context,contact) +
                EdoRequest.PATH_FILES;

		if(JavaUtils.isNotEmpty(folderId))
			targetURL += EdoRequest.PATH_SEPARATOR + folderId;

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
			return LFile.fromJsonToArray(response);
		}
		return null;
	}

    public static String getURLPrefix(Context context, LContact contact){
        boolean isCurrentUser = contact.isPersonal(context);

        return EdoRequest.HOST_PREFIX +
                (isCurrentUser ? EdoRequest.PATH_PERSONAL :
                        ((!contact.isGroup()? EdoRequest.PATH_CONTACT : EdoRequest.PATH_GROUP) + EdoRequest.PATH_SEPARATOR + contact.getId()));
    }

}
