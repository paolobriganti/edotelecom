package io.edo.db.global.uibeans;

import android.content.Context;

import java.util.ArrayList;

import io.edo.db.global.EdoActivityManager;
import io.edo.db.global.beans.ContactDAO;
import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactDAO;
import io.edo.db.local.beans.support.EdoResource;
import io.edo.db.web.beans.WEntity;
import io.edo.ui.UIManager;
import io.edo.utilities.eLog;

public class ContactUIDAO extends BaseUIDAO{

    ContactDAO contactDAO;

    public ContactUIDAO(Context context) {
        super(context);
        contactDAO = new ContactDAO(context);
    }

    @Override
    public LContactDAO getLDAO() {
        return contactDAO.getLDAO();
    }


    public LContact add(LContact contact, EdoActivityManager.Callback callback) {
        WEntity entity = contactDAO.add(contact);
        if(entity!=null && entity.contact!=null){

            if(entity.act!=null) {
                entity.act.setResource(entity.contact);
                processActivity(entity.act, false, callback);
            }
            return entity.contact;
        }
        return null;
    }

    public LContact[] addAll(ArrayList<LContact> lcontacts) {
        WEntity entity = contactDAO.addAll(lcontacts);

        if(entity!=null && entity.contacts!=null){
            return entity.contacts;
        }


        return null;
    }

    public boolean update(LContact contact) {
        WEntity entity = contactDAO.update(contact);

        if(entity!=null){
            if(entity.act!=null){

                if(entity.act!=null){
                    entity.act.setResource(entity.contact);
                }

                processActivity(entity.act,false);
            }

            return true;
        }
        return false;
    }

    public boolean activeContact(LContact econtact){

        if(econtact.isHidden()){
            econtact.setStatus(LContact.STATUS_ACTIVE);
            return update(econtact);
        }

        econtact.setStatus(LContact.STATUS_ACTIVE);
        return getLDAO().update(econtact);
    }

    public boolean hideContact(LContact econtact){
        econtact.setStatus(LContact.STATUS_HIDDEN);

        return update(econtact);
    }

    public boolean highlightContact(LContact contact, Long mDate, boolean updateUI){
        LContact newContact = getLDAO().get(contact.getId());
        eLog.i("EdoActivityMan", "CONTACT: " + contact.getName());
        if(newContact!=null){
            boolean done = true;
            if(mDate!=null) {
                newContact.setmDate(mDate);
                done = getLDAO().updateMDate(contact.getId(),mDate);
            }
            eLog.i("EdoActivityMan", "HILIGHT CONTACT: " + done);

            if(updateUI)
                app.uiManager.updateResource(ContactDAO.getFakeActivity(LActivity.EVENT_HIGHLIGHT_CONTACT,newContact), UIManager.ContactsUpdatesListener.class);
            return done;
        }
       return false;
    }




    public WEntity delete(LContact contact) {
        return contactDAO.delete(contact);
    }



    public LContact download(String id){
        return contactDAO.download(id);
    }



    public LContact[] downloadAll(){
        LContact[] contacts = contactDAO.downloadAll();

        LActivity a = new LActivity();
        a.setEvent(LActivity.EVENT_SYNC_ALL);
        a.setStatus(LActivity.STATUS_COMPLETE);
        app.uiManager.syncResourcesList(a, UIManager.ContactsUpdatesListener.class);

        return contacts;
    }







    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    //*		ACTIVITY
    //*****************************************************************************************************************************************************************************************************************************************************************************************************
    public LActivity getFakeActivity(int event, EdoResource resource){
        LActivity a = new LActivity(LActivity.CONTEXT_PERSONAL, event, LActivity.RESOURCE_CONTACT);
        a.setResource(resource);
        return a;
    }

}
