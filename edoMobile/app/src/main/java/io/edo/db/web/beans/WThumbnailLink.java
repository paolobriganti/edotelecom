package io.edo.db.web.beans;

import com.google.gson.Gson;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

/**
 * Created by PaoloBriganti on 16/05/15.
 */
public class WThumbnailLink {
    public String title;
    public String preview;


    public String toJson(){
        return new Gson().toJson(this);
    }

    public static WThumbnailLink fromJson(String json){
        try{
            return new Gson().fromJson(json, WThumbnailLink.class);
        }catch (com.google.gson.JsonSyntaxException e){
            eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WThumbnailLink:\n" + e.getMessage());
            eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "WThumbnailLink JSON:\n"+json);
            return null;
        }
    }
}
