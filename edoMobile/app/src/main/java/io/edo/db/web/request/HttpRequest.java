package io.edo.db.web.request;


import com.paolobriganti.utils.FileInfo;
import com.paolobriganti.utils.JavaUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import io.edo.utilities.eLog;

public class HttpRequest {


	private final static boolean checkCertificate = false;


	// HTTP GET request
	public static HttpResponse sendGet(String targetURL, ArrayList<HeaderParams> headerParams) {
		try {        
			

			

			
			HttpClient client = new DefaultHttpClient();
			
			if(!checkCertificate)
				client = getNewHttpClientWithoutCertificateValidation();
			
			//Create GET request
			HttpGet request = new HttpGet();
			request.setURI(new URI(targetURL));

			//Add header
			if(headerParams!=null && headerParams.size()>0)
				for(int i=0; i<headerParams.size();	i++)
					request.addHeader(headerParams.get(i).key, headerParams.get(i).value);

			//Execute Request
			return client.execute(request);
			
			
		} catch (URISyntaxException e) {
			return null;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}   
		
		
	}
















	// HTTP POST request
	public static HttpResponse sendPost(String targetURL, ArrayList<HeaderParams> headerParams, List<NameValuePair> nameValuePairs, String body) {
		
		try{
			
			HttpClient client = new DefaultHttpClient();
			
			if(!checkCertificate)
				client = getNewHttpClientWithoutCertificateValidation();
			
			//Create POST request
			HttpPost request = new HttpPost();
			request.setURI(new URI(targetURL));

			
			
			
			//Add header
			if(headerParams!=null && headerParams.size()>0)
				for(int i=0; i<headerParams.size();	i++)
					request.addHeader(headerParams.get(i).key, headerParams.get(i).value);

			
			//Add body
			if(body!=null){
				if(JavaUtils.isJSONValid(body)){
				
					eLog.v("E_D_O_SERVICE", "THE BODY IS JSON");
					
					request.setEntity(new StringEntity(body, "UTF-8"));
				
				}else if(JavaUtils.isNotEmpty(body) && new java.io.File(body).exists()){
					java.io.File file = new java.io.File(body);
					
					String mime = FileInfo.getMime(body);
					if(JavaUtils.isEmpty(mime)){
						mime = "application/octet-stream";
					}
					
					eLog.v("E_D_O_SERVICE", "THE BODY IS FILE with mime: "+mime+" and size "+FileInfo.getFileKByteSize(body)+"kb");
					request.setEntity(new FileEntity(file, mime));
					
				}else{
					eLog.v("E_D_O_SERVICE", "THE BODY IS NOT RECOGNIZED");
				}
					
			
			
			//Add Params
			}else if(nameValuePairs!=null){
		        request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}
			
			//Execute Request
			return client.execute(request);
			
			
		} catch (URISyntaxException e) {
			return null;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}   
		
		
	}


	// HTTP DELETE request
	public static HttpResponse sendDelete(String targetURL, ArrayList<HeaderParams> headerParams) {
		try {        
			

			HttpClient client = new DefaultHttpClient();
			
			if(!checkCertificate)
				client = getNewHttpClientWithoutCertificateValidation();
			
			//Create Delete request
			HttpDelete request = new HttpDelete();
			request.setURI(new URI(targetURL));

			//Add header
			if(headerParams!=null && headerParams.size()>0)
				for(int i=0; i<headerParams.size();	i++)
					request.addHeader(headerParams.get(i).key, headerParams.get(i).value);

			//Execute Request
			return client.execute(request);
			
			
		} catch (URISyntaxException e) {
			return null;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}   
		
		
	}

	
	
	// HTTP PUT request
	public static HttpResponse sendPut(String targetURL, ArrayList<HeaderParams> headerParams, List<NameValuePair> nameValuePairs, String jbody) {
		
		try{
			
			HttpClient client = new DefaultHttpClient();
			
			if(!checkCertificate)
				client = getNewHttpClientWithoutCertificateValidation();
			
			//Create PUT request
			HttpPut request = new HttpPut();
			request.setURI(new URI(targetURL));


			
			
			//Add header
			if(headerParams!=null && headerParams.size()>0)
				for(int i=0; i<headerParams.size();	i++)
					request.addHeader(headerParams.get(i).key, headerParams.get(i).value);

			
			//Add body
			if(JavaUtils.isNotEmpty(jbody)){
				request.setEntity(new StringEntity(jbody, "UTF-8"));
			//Add Params
			}else if(nameValuePairs!=null){
		        request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			}
			
			//Execute Request
			return client.execute(request);
			
			
		} catch (URISyntaxException e) {
			return null;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}   
		
		
	}








	public static String getStringResponse(HttpResponse response){
		try {
			if(response!=null && response.getEntity()!=null)
				return EntityUtils.toString(response.getEntity());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}






	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	public static HttpClient getNewHttpClientWithoutCertificateValidation() {
	    try {
	        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	        trustStore.load(null, null);

	        SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

	        HttpParams params = new BasicHttpParams();
	        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	        HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

	        SchemeRegistry registry = new SchemeRegistry();
	        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	        registry.register(new Scheme("https", sf, 443));

	        ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

	        return new DefaultHttpClient(ccm, params);
	    } catch (Exception e) {
	        return new DefaultHttpClient();
	    }
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



	
}
