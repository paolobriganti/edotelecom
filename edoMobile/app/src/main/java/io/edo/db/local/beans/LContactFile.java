package io.edo.db.local.beans;

import android.database.Cursor;

import java.io.Serializable;

import io.edo.db.local.LocalDBHelper;
import io.edo.db.local.beans.support.LSpace;

public class LContactFile  implements Serializable{
	private static final long serialVersionUID = 1L;

	private LContact contact;
	private LFile file;
    private LSpace spaceWithCurrentFolder;
	
	public LContactFile(LContact contact, LFile file) {
		super();
		this.contact = contact;
		this.file = file;
	}

    public LContactFile(LContact contact, LSpace spaceWithCurrentFolder, LFile file) {
        super();
        this.contact = contact;
        this.spaceWithCurrentFolder = spaceWithCurrentFolder;
        this.file = file;
    }

	public LContactFile(Cursor cursor) {
		setContactId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_CONTACTID)));
		setFileId(cursor.getString(cursor.getColumnIndex(LocalDBHelper.KEY_FILEID)));
	}

    public LContact getContact() {
        return contact;
    }

    public void setContact(LContact contact) {
        this.contact = contact;
    }

    public LFile getFile() {
        return file;
    }

    public void setFile(LFile file) {
        this.file = file;
    }

    public String getContactId() {
        if(contact!=null)
		    return contact.getId();
        return null;
	}

	public void setContactId(String contactId) {
		this.contact = new LContact(contactId);
	}

	public String getFileId() {
        if(file!=null)
            return file.getId();
        return null;
	}

	public void setFileId(String fileId) {
		this.file = new LFile(fileId);
	}

    public LSpace getSpaceWithCurrentFolder() {
        return spaceWithCurrentFolder;
    }

    public void setSpaceWithCurrentFolder(LSpace spaceWithCurrentFolder) {
        this.spaceWithCurrentFolder = spaceWithCurrentFolder;
    }
}
