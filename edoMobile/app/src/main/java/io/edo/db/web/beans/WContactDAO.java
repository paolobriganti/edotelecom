package io.edo.db.web.beans;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import io.edo.api.EmailBodyEdo;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.LServiceDAO;
import io.edo.db.web.request.EdoRequest;
import io.edo.edomobile.ThisApp;


public class WContactDAO{

	public static WEntity addContact(Context context, LContact localContact){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_CONTACT;


		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, localContact.getWebContactJson());

		String response = request.send(context);
		if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.contact = localContact;
            }
            return entity;
		}
		return null;
	}



	public static WEntity addContacts(Context context, ArrayList<LContact> lcontacts){
		ArrayList<LContact> newContacts = new ArrayList<LContact>();
        WEntity entity = null;
		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_CONTACT + "?batch=1";

		int NUM = 100;

		int parts = 1;

		if(lcontacts.size()>NUM){
			parts = lcontacts.size()/NUM;
			int r = lcontacts.size() % NUM;
			if(r>0) parts++;
		}



		int count = 0;
		for(int a=0; a<parts; a++){
			int end = count+NUM-1;
			List<LContact> subContacts = null;
			if(end>=lcontacts.size()){
				end = lcontacts.size();
			}



			if(end-count>0){
				subContacts = lcontacts.subList(count, end);

				LContact[] lcs = new LContact[subContacts.size()];
				subContacts.toArray(lcs);

				String body = LContact.getWebContactJson(lcs);

				EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, body);

				String response = request.send(context);
				if(!EdoRequest.hasEdoErrors(response)){
                    entity = WEntity.fromJson(response);
					if(entity!=null){
						LContact[] wcontacts = entity.contacts;

						if(wcontacts!=null && wcontacts.length>0){

							for(int i=0; i<wcontacts.length; i++){
								newContacts.add(updateValuesFromWeb(subContacts.get(i),wcontacts[i]));
							}

						}
					}
				}
				count = end+1;
			}
		}


        LContact[] contactsArray = new LContact[newContacts.size()];
        newContacts.toArray(contactsArray);

        if(entity!=null)
            entity.contacts = contactsArray;

		return entity;
	}






	public static WEntity updateContact(Context context, LContact localContact){
		if(localContact.isGroup()){
			return null;
		}

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_CONTACT + EdoRequest.PATH_SEPARATOR + localContact.getId();

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_PUT, targetURL, localContact.getWebContactJson());

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.contact = localContact;
            }
            return entity;
        }
        return null;
	}

	public static WEntity deleteContact(Context context, LContact localContact){
		if(localContact.isGroup()){
			return null;
		}

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_CONTACT + EdoRequest.PATH_SEPARATOR + localContact.getId();

		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_DELETE, targetURL, null);

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            WEntity entity = WEntity.fromJson(response);
            if(entity==null){
                entity = new WEntity();
                entity.contact = localContact;
            }
            return entity;
        }
        return null;
	}

    public static boolean buildSuggested(Context context){
        String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_CONTACT_SUGGESTED;

        EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, "null");

        request.send(context);

        return true;
    }


    public static LContact getContact(Context context, String contactId, boolean checkAlsoEdoUserId){

        String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_CONTACT + EdoRequest.PATH_SEPARATOR + contactId;

        if(checkAlsoEdoUserId){
            targetURL += "?f=1";
        }

        EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

        String response = request.send(context);
        if(!EdoRequest.hasEdoErrors(response)){
            return LContact.fromJson(response);
        }
        return null;
    }

    public static LContact[] getContacts(Context context){
        String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_USER_CONTACTS;

        EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

        String response = request.send(context);

        if(!EdoRequest.hasEdoErrors(response)){
            return LContact.fromJsonToArray(response);
        }
        return null;
    }


    public static LContact[] getSuggestedContacts(Context context){
        LService service = new LServiceDAO(context).getPrincipalService();

        if(service==null)
            return null;

        String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE_GOOGLE + EdoRequest.PATH_SEPARATOR +
                service.getId() + EdoRequest.PATH_CONTACTS ;

        EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_GET, targetURL, null);

        String response = request.send(context);

        if(!EdoRequest.hasEdoErrors(response)){
            return LContact.fromJsonToArray(response);
        }


        return null;
    }

	public static boolean sendInvite(Context context, LContact contact){

		String targetURL = EdoRequest.HOST_PREFIX + EdoRequest.PATH_INVITE;

		ThisApp app = ThisApp.getInstance(context);

		EmailBodyEdo body = new EmailBodyEdo(EmailBodyEdo.TYPE_INVITE, 
				app.getUserName(), 
				app.getUserEmail(), 
				contact.getNameSurname()!=null?contact.getNameSurname():contact.getPrincipalEmailAddress(), 
						contact.getPrincipalEmailAddress());


		EdoRequest request = new EdoRequest(EdoRequest.REQUEST_TYPE_POST, targetURL, body.toJson());

		String response = request.send(context);

		return !EdoRequest.hasEdoErrors(response);
	}




    public static LContact updateValuesFromWeb(LContact lcontact, LContact wcontact){
        if(lcontact!=null && wcontact!=null) {
            String id = wcontact.getId() != null && !wcontact.getId().equalsIgnoreCase("None") ? wcontact.getId() : lcontact.getId();
            lcontact.setId(id);
            String edoUserId = wcontact.getEdoUserId() != null && !wcontact.getEdoUserId().equalsIgnoreCase("None") ? wcontact.getEdoUserId() : lcontact.getEdoUserId();
            lcontact.setEdoUserId(edoUserId);
            String status = wcontact.getStatus() != null && !wcontact.getStatus().equalsIgnoreCase("None") ? wcontact.getStatus() : lcontact.getStatus();
            lcontact.setStatus(status);
            return lcontact;
        }
        return lcontact;
    }
}
