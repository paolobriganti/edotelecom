package io.edo.db.global.beans;

import android.content.Context;

import io.edo.db.global.uibeans.ContactUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LContactFile;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.LSpace;
import io.edo.db.local.beans.support.LSpaceDAO;
import io.edo.db.web.beans.WEntity;


public class SpaceDAO extends ContactFileDAO{
    LSpaceDAO lSpaceDAO;
    
    public SpaceDAO(Context context) {
        super(context);
        lSpaceDAO = new LSpaceDAO(context);
    }

    @Override
    public LSpaceDAO getLDAO() {
        return lSpaceDAO;
    }

    //***************************
    //		Contacts Spaces		*
    //***************************

    public WEntity addNewSpaceToContact(final LContact contact, LSpace space){
        LContactFile contactFile = new LContactFile(contact,space);
        contactFile.setSpaceWithCurrentFolder(space);

        return add(contactFile);
    }



    public WEntity addDefaultSpaceTo(final LContact contact){

        if(contact!=null && contact!=null){

            return addNewSpaceToContact(contact, LSpace.newDefaultSpace(context,contact));
        }

        return null;
    }

    public int getFilesOf(LContact contact, LSpace space, boolean updateContact){
        ContactUIDAO contactDAO = new ContactUIDAO(context);

        LContact lcontact = contact;
        if(updateContact){
            if(!lcontact.isGroup()){
                contact = contactDAO.download(contact.getId());
                if(contact!=null){
                    lcontact = contact;
                }
            }else{
                lcontact = new GroupDAO(context).download(lcontact.getId());
            }
        }
        if(lcontact!=null){

            LFile[] files = downloadAllFileOf(lcontact,null);
            if(files!=null){
                for(LFile file: files){
                    if(file.isSpace())
                        return files.length;
                }

            }
        }
        return 0;
    }

}
