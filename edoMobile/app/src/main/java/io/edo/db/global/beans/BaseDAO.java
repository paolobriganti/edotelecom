package io.edo.db.global.beans;

import android.content.Context;

import io.edo.db.local.beans.LBaseDAO;
import io.edo.edomobile.ThisApp;

/**
 * Created by PaoloBriganti on 19/06/15.
 */
public abstract class BaseDAO{
    public ThisApp app;
    public Context context;


    public BaseDAO(Context context){
        this.context = context;
        this.app = ThisApp.getInstance(context);
    }


    public abstract LBaseDAO getLDAO();

}
