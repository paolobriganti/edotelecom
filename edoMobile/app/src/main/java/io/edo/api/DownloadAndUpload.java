package io.edo.api;

import java.io.Serializable;

public class DownloadAndUpload implements Serializable{
	private static final long serialVersionUID = 1L;
	public Download download;
	public Upload upload;

	public DownloadAndUpload(Download download, Upload upload) {
		super();
		this.download = download;
		this.upload = upload;
	}

}
