/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.edo.api.google.gcm;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.JavaUtils;

import java.io.IOException;

import io.edo.utilities.eLog;

/**
 * Main UI for the demo app.
 */
public class GCMActivity extends FragmentActivity {


	public GCMUtils GCM;

	GCMActivity c;
//	String regid;

	GoogleCloudMessaging gcm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		c=this;
		GCM = new GCMUtils(this);
	}


	public String startRegistration(boolean sendRegistrationToBackend){
		String regid = null;
		// Check device for Play Services APK. If check succeeds, proceed with GCM registration.
		if (GCM.getGoogleCloudMessaging()!=null) {
			gcm = GCM.getGoogleCloudMessaging();
			regid = GCM.getRegistrationId();

			if (JavaUtils.isEmpty(regid)) {
				return register(sendRegistrationToBackend);
			}else if(sendRegistrationToBackend) {
				GCM.sendRegistrationIdToBackend(regid);
			}
		}
		eLog.w(GCMUtils.TAG, "GCM REGISTRATION ID: " + regid);
		return regid;
	}



	@Override
	protected void onResume() {
		super.onResume();
		// Check device for Play Services APK.
		GCM.checkPlayServices();
	}



	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and the app versionCode in the application's
	 * shared preferences.
	 */

	public static final int MAX_ATTEMPTS = 10;
	int attempts = 1;
	public String register(boolean sendRegistrationToBackend) {
		eLog.i("GCM", "register: ");
		String regid = null;
		String msg = "";
		try {

			eLog.i("GCM", "GCM: "+gcm);
			if (gcm == null) {
				gcm = GoogleCloudMessaging.getInstance(c);
			}
			eLog.i("GCM","GET NEW REG ID");
			regid = GCM.getNewRegId(sendRegistrationToBackend);
			eLog.i("GCM","Device registered, registration ID=" + regid);
		} catch (IOException ex) {
			eLog.w("GCM","Error :" + ex.getMessage());
			// If there is an error, don't just keep trying to register.
			// Require the user to click a button again, or perform
			// exponential back-off.
			if(ASI.isInternetOn(c)) {
				if (attempts <= MAX_ATTEMPTS) {
					eLog.v("GCM","Retry. Attempt: " + attempts);
					try {
						Thread.sleep(500 * attempts);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					attempts++;
					return register(sendRegistrationToBackend);
				}
			}
		}

		return regid;
	}





}


