package io.edo.api;

import android.content.Context;

import com.google.api.services.drive.model.About;
import com.paolobriganti.android.ASI;

import io.edo.api.google.GoogleDrive;
import io.edo.api.google.GoogleResponse;
import io.edo.db.local.LocalDBManager;
import io.edo.db.local.beans.LService;
import io.edo.utilities.Constants;

public class ApiInfo {
	
	public static final String ACTION_DOWNLOAD_COMPLETE = Constants.ACTIONS_PREFIX+"DOWNLOAD_COMPLETE";
	public static final String ACTION_UPLOAD_COMPLETE = Constants.ACTIONS_PREFIX+"UPLOAD_COMPLETE";
	public static final String ACTION_CONTACTS_DOWNLOAD_COMPLETE = Constants.ACTIONS_PREFIX+"CONTACTS_DOWNLOAD_COMPLETE";
	public static final String ACTION_EMAIL_SYNC_COMPLETE = Constants.ACTIONS_PREFIX+"EMAIL_SYNC_COMPLETE";
	
	LService service = null;
	LocalDBManager db;
	About about = null;

	public ApiInfo(Context c, LocalDBManager db, final LService service){
		this.db = db;
		if(ASI.isInternetOn(c)){
			this.service = service;

			
			if(service!=null && service.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				GoogleDrive drive = new GoogleDrive(c, service);
				GoogleResponse response = drive.getAbout();
				if(response!=null){
					about = (About) response.result;
				}
			}	
		}
	}


	public synchronized Long getTotalBytes(){

		if(service!=null && service.getName().equals(LService.SERVICE_NAME_GOOGLE) && about!=null){
			return about.getQuotaBytesTotal();
		}	
		
		return null;
	}

	public synchronized Long getOccupiedBytes(){
		if(service!=null && service.getName().equals(LService.SERVICE_NAME_GOOGLE) && about!=null){
			return about.getQuotaBytesUsed();
		}	
		
		return null;
	}
	
	public synchronized Long getFreeBytes(){
		if(service!=null && service.getName().equals(LService.SERVICE_NAME_GOOGLE) && about!=null){
			return about.getQuotaBytesTotal()-about.getQuotaBytesUsed();
		}	
		
		return null;
	}
	
	public synchronized String getBytesForView(Long bytes){
		if(bytes!=null){
			if(bytes>=1000000000){
				bytes = bytes/1000000000;
				return bytes+" GB";
			}else if(bytes>=1000000){
				bytes = bytes/1000000;
				return bytes+" MB";
			}else if(bytes>=1000){
				bytes = bytes/1000;
				return bytes+" KB";
			}else{
				return bytes+" bytes";
			}
		}
		return null;
	}
}
