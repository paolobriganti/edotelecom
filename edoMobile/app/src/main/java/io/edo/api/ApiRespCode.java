package io.edo.api;


public class ApiRespCode{
	public static final int SUCCESS = 000;
	public static final int FAILED = 001;
	public static final int ERROR_AUTHORIZATION = 401;
	public static final int ERROR_FILE_NOT_FOUND = 404;
}
