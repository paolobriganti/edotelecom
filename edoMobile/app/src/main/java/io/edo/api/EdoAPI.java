package io.edo.api;



public class EdoAPI {
	
	public static final String WEB_CLIENT_ID = "897500908664-d3vof54slkc6pbt1rr3b3vf57pr9k27f.apps.googleusercontent.com";
	
	public static final String GOOGLE_OAUTH_SCOPES = 
//			Plus.SCOPE_PLUS_LOGIN.dD() + " " +
			"https://www.googleapis.com/auth/drive "+
//			"https://mail.google.com/ "+
			"https://www.google.com/m8/feeds "+
			"https://www.googleapis.com/auth/userinfo.profile "+
			"https://www.googleapis.com/auth/userinfo.email";
	
	public static final String GOOGLE_WEB_SCOPE = "oauth2:server:client_id:"+WEB_CLIENT_ID+":api_scope:"+GOOGLE_OAUTH_SCOPES;
	
}
