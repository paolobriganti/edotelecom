package io.edo.api;

import android.content.Context;
import android.graphics.Bitmap;

import com.paolobriganti.android.ASI;
import com.paolobriganti.android.AUtils;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.UniversalProgressListener;

import java.io.File;

import io.edo.api.google.GoogleDriveEdo;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;

public class ApiFile {


//	public synchronized static ApiResponse createContainerCloudFolder(Context context, Object container){
//		if(ASI.isInternetOn(context)){
//			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				ApiResponse id = GoogleDriveEdo.createContainerCloudFolder(context, lService.getId(), container);
//				return id;
//			}
//		}
//		return null;
//	}
//
//	public synchronized static ApiResponse copyFile(Context context, String destinationFolderCloudId, LFile edoFile){
//		if(ASI.isInternetOn(context)){
//			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				ApiResponse id = GoogleDriveEdo.copyFile(context, lService.getId(), destinationFolderCloudId, edoFile);
//				return id;
//			}
//		}
//		return null;
//	}
//
//	public synchronized static ApiResponse moveFile(Context context, String destinationFolderCloudId, LFile edoFile){
//		if(ASI.isInternetOn(context)){
//			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				ApiResponse id = GoogleDriveEdo.moveFile(context, lService.getId(), destinationFolderCloudId, edoFile);
//				return id;
//			}
//		}
//		return null;
//	}









	//*********************************************************************
	//*		UPLOAD
	//*********************************************************************

	public synchronized static ApiResponse uploadFile(final Context c, EdoContainer container, LSpace tab, LFile file, FileUploadInfo fui, UniversalProgressListener listener){
		if(fui.getFileContent()==null)
			return null;
		
		if(ASI.isInternetOn(c)){
			long startTimeInMillis = System.currentTimeMillis();

			LService lService = tab.getService(c, false);
			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				ApiResponse response = GoogleDriveEdo.uploadFile(c, container, tab, file, fui, listener);
				if(response!=null && (response.result instanceof String) && JavaUtils.isNotEmpty((String) response.result)){
					
				}
				return response;
			}
		}
		return null;
	}




	//	public synchronized static boolean categoryCloudAllMetaDataSync(Context c, LCategory category){
	//		if(ASI.isInternetOn(c)){
	//			LServiceDAO lsDao = new LServiceDAO(c);
	//			LService lService = lsDao.getService(category.getServiceId());
	//			if(lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
	//				GoogleDriveEdo.addAllDriveFilesToCategory(c, category, lService);
	//				return true;
	//			}
	//		}
	//		return false;
	//	}

















	//*********************************************************************
	//*		DOWNLOAD
	//*********************************************************************



	public synchronized static ApiResponse downloadFile(Context c, EdoContainer container, LSpace tab, LFile edoFile, FileDownloadInfo fdi, UniversalProgressListener progressListener){
		if(fdi.getFileContent()==null)
			return null;
		
		if(ASI.isInternetOn(c)){
			long startTimeInMillis = System.currentTimeMillis();
		
			LService lService = tab.getService(c, false);
			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				ApiResponse path = GoogleDriveEdo.downloadFile(c, tab, edoFile, fdi, progressListener);
				
				File file = new File(fdi.getFilePath());
				if(file.exists()){
					

					AUtils.forceMediaUpdate(c, file.toString());
				}
				
				return path;
			}
		}
		return null;
	}


//	public static ApiResponse downloadSaveThumbnailFromCloud(Context c, LService lService, LFile edoFile){
//		if(ASI.isInternetOn(c)){
//
//
//			int	heightSizePx = ThumbnailManager.getDownloadSideSize(c);
//
//
//			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				ApiResponse response = GoogleDriveEdo.getSaveThumbnail(c, lService.getId(), edoFile, null, heightSizePx);
////				boolean isSaved = response!=null? (Boolean)response.result:false;
////				if(isSaved)
////					EdoUIUpdateManager.sendUpdate(c, edoFile.getId(), EdoUIUpdateManager.EVENT_UPDATE, edoFile, (String)null);
//				return response;
//			}
//		}
//		return null;
//	}

	public synchronized static ApiResponse getThumbnailUrl(Context c, LService lService, LFile edoFile, int heightSizePx){
		if(ASI.isInternetOn(c)){
			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){

				return GoogleDriveEdo.getThumbnailUrl(c, lService.getId(), edoFile, heightSizePx);
			}
		}
		return null;
	}
	
	public synchronized static Bitmap getThumbnailImage(Context c, LService lService, String thumbLink){
		if(ASI.isInternetOn(c)){
			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				return GoogleDriveEdo.getThumbnailImage(c, lService.getId(), thumbLink);
			}
		}
		return null;
	}
	
	
	public synchronized static ApiResponse getDownloadUrl(Context c, LService lService, LFile edoFile){
		if(ASI.isInternetOn(c)){
			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				return GoogleDriveEdo.getDownloadUrl(c, lService.getId(), edoFile);
			}
		}
		return null;
	}

	public synchronized static ApiResponse getAlternateLink(Context c, LService lService, LFile edoFile){
		if(ASI.isInternetOn(c)){
			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				return GoogleDriveEdo.getAlternateLink(c, lService.getId(), edoFile);
			}
		}
		return null;
	}

	public synchronized static ApiResponse getDefaultOpenWithLink(Context c, LService lService, LFile edoFile){
		if(ASI.isInternetOn(c)){
			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				return GoogleDriveEdo.getDefaultOpenWithLink(c, lService.getId(), edoFile);
			}
		}
		return null;
	}
	
	public synchronized static ApiResponse getFileSizeLong(Context c, LService lService, LFile edoFile){
		if(ASI.isInternetOn(c)){
			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				return GoogleDriveEdo.getFileSizeLong(c, lService.getId(), edoFile);
			}
		}
		return null;
	}





















	//*********************************************************************
	//*		SHARE
	//*********************************************************************
	
//	public synchronized static ApiResponse shareFile(final Context c, String[] collabsEmails, LFile file, boolean sendNotificationEmail){
//		if(ASI.isInternetOn(c)){
//			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				ApiResponse response = GoogleDriveEdo.shareFile(c, lService.getId(), file, collabsEmails, sendNotificationEmail);
//				boolean isAllowed = response!=null? (Boolean)response.result:false;
//				return response;
//			}
//		}
//		return null;
//	}
//
//
//	public synchronized static ApiResponse uploadShareFile(final Context c, Object container, LSpace tab, LFile file, boolean sendNotificationEmail, FileUploadInfo fui, UniversalProgressListener listener){
//		if(ASI.isInternetOn(c)){
//			LService lService = tab.getService(c);
//			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				ApiResponse resp = uploadFile(c, lService, container, tab, file, fui, listener);
//				String cloudId = resp!=null? (String)resp.result:null;
//				if(JavaUtils.isNotEmpty(cloudId)){
//					file.setCloudId(cloudId);
//					ApiResponse response = GoogleDriveEdo.shareFile(c, lService.getId(), file, tab.getCollabsEmails(), sendNotificationEmail);
//					boolean isAllowed = response!=null? (Boolean)response.result:false;
//					return response;
//				}
//			}
//		}
//		return null;
//	}





//	public synchronized static ApiResponse renameFile(Context c, LFile efile){
//		if(ASI.isInternetOn(c)){
//			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				return GoogleDriveEdo.renameMetaFile(c, lService.getId(), efile);
//			}
//		}
//		return null;
//	}










	//*********************************************************************
	//*		REMOVE
	//*********************************************************************


//	public synchronized static ApiResponse removeFile(Context c, final LService lService, final String fileCloudId){
//		if(ASI.isInternetOn(c)){
//			if(lService!=null && lService.getName().equals(LService.SERVICE_NAME_GOOGLE)){
//				return GoogleDriveEdo.removeFile(c, lService.getId(), fileCloudId);
//			}	
//
//		}
//		return null;
//	}
}