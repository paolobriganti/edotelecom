package io.edo.api;

import com.google.gson.Gson;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class EmailBodyEdo {
	public static final String HTML_FILES_PATH = "html_files/";
	public static final String HTML_SHARED_EMAIL = "shared_email.html";
	public static final String HTML_SHARED_GROUP_EMAIL = "shared_group_email.html";
	public static final String HTML_INVITATION_EMAIL = "invitation_email.html";
	public static final String HTML_INVITATION_GROUP_EMAIL = "invitation_group_email.html";
	
	public static final String PH_RECIPIENT = "{{ recipient }}"; //CONTACT NAME
	public static final String PH_SENDER = "{{ sender }}"; //USER
	public static final String PH_GROUP = "{{ group }}"; //GROUP NAME
	public static final String PH_FILE_NAME = "{{ filename }}"; //FILE NAME
	public static final String PH_LINK_TO_FILE = "{{ link_to_file }}"; //LINK TO FILE
	public static final String PH_PREVIEW = "{{ preview }}"; //PREVIEW LINK
	
	
	public static final String TYPE_INVITE = "invite";
	public static final String TYPE_SHARE = "share";

//	public String type; // 'invite' or 'share'
	public String from_user; // <sending user name>
	public String from_email; // <sending user email>
	public String to_user; // <reciever name>
	public String to_email; // <reciever email>
	//If type is 'share' the following paramenters are mandatory:
	public String file_link; // google alternate link, or url of the file
	public String preview; // url of filetype icon eg: "https://edostatic.blob.core.windows.net/shared-email/file_audio.png"
	public String filename; // file name	

	public EmailBodyEdo(String type, String from_user, String from_email,
			String to_user, String to_email) {
		super();
//		this.type = type;
		this.from_user = from_user;
		this.from_email = from_email;
		this.to_user = to_user;
		this.to_email = to_email;
	}




	public String toJson(){
		return new Gson().toJson(this);
	}

	public EmailBodyEdo fromJson(String json){
		try{
			return new Gson().fromJson(json, EmailBodyEdo.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "Body:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "Body JSON:\n"+json);
			return null;
		}

	}
}
