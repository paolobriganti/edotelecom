package io.edo.api;

import java.io.Serializable;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;

public class Upload implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int MINIMUM_CHUNK_SIZE = 262144;
	public static final int MAXIMUM_CHUNK_SIZE = 10485760;

	public EdoContainer container;
	public LSpace tab;
	public LFile file;

	public boolean isEdoUser=false;

	public boolean isRunning=false;

	public String uploadId;
	
	public long range = 0;

	public LActivity a;

	public Upload() {}

	public Upload(EdoContainer container, LSpace tab, LFile file, boolean isEdoUser) {
		super();
		this.container = container;
		this.file = file;
		this.tab = tab;
		this.isEdoUser = isEdoUser;
	}

}
