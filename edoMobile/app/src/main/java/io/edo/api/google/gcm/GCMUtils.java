package io.edo.api.google.gcm;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.paolobriganti.utils.JavaUtils;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import io.edo.db.local.LocalDBManager;
import io.edo.db.web.beans.Device;
import io.edo.db.web.beans.WDeviceDAO;
import io.edo.utilities.eLog;

public class GCMUtils {
	public static final String TAG = "GCM";
	
	
	public static final String ACTION_REGISTRATION = "io.edo.edomobile.GCM_REGISTRATION";
	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static final String REG_TIME = "registration_time";
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private static final long REG_EXPIRATION_TIME = 21600000; // 21600000 ms = 6 hours

	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	public String SENDER_ID = "897500908664";

	Context context;

	/**
	 * Tag used on log messages.
	 */

	AtomicInteger msgId = new AtomicInteger();

	GoogleCloudMessaging gcm;

	public GCMUtils(Context context){
		this.context = context;

		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(context);
		} else {
			gcm = null;
		}
	}

	public GoogleCloudMessaging getGoogleCloudMessaging(){
		return gcm;
	}



	public String getNewRegId(final boolean sendRegistrationToBackend) throws IOException{
		if(gcm==null){
			gcm = GoogleCloudMessaging.getInstance(context);
		}
		String regid = gcm.register(SENDER_ID);
		eLog.i("GCM","new regid =" + regid);
		if(JavaUtils.isNotEmpty(regid)){
			if(sendRegistrationToBackend)
				sendRegistrationIdToBackend(regid);

			storeRegistrationId(context, regid);
		}
		return regid;
	}











	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	public boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				try {
					GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context,
							PLAY_SERVICES_RESOLUTION_REQUEST).show();
				}catch (Exception e){

					try{
						Intent browser;
						browser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms"));
						context.startActivity(browser);
					}catch(Exception ee){
						Intent goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.gms"));
						goToMarket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						try {
							context.startActivity(goToMarket);
						}catch (ActivityNotFoundException e1){}
					}

				}catch (Error e){

					try{
						Intent browser;
						browser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms"));
						context.startActivity(browser);
					}catch(Exception ee){
						Intent goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.gms"));
						goToMarket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.startActivity(goToMarket);
					}

				}
			} else {
				((Activity) context).finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Stores the registration ID and the app versionCode in the application's
	 * {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration ID
	 */
	public void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGcmPreferences();
		int appVersion = getAppVersion(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.putLong(REG_TIME, System.currentTimeMillis());
		editor.commit();

		eLog.i("GCM", "new reg time: =" + System.currentTimeMillis());
	}

	/**
	 * Gets the current registration ID for application on GCM service, if there is one.
	 * <p>
	 * If result is empty, the app needs to register.
	 *
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	public String getRegistrationId() {
		Context appContext = context.getApplicationContext();
		final SharedPreferences prefs = getGcmPreferences();

		Long registrationTime = prefs.getLong(REG_TIME, 0);

		eLog.w("GCM", "TIME EXPIRED? " + registrationTime);

		if(registrationTime==null || System.currentTimeMillis()-registrationTime>REG_EXPIRATION_TIME){
			return null;
		}


		String registrationId = prefs.getString(PROPERTY_REG_ID, "");

		eLog.w("GCM", "registrationId? " + (registrationId));

		if (registrationId.isEmpty()) {
			return null;
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(appContext);
		if (registeredVersion != currentVersion) {
			return null;
		}
		return registrationId;
	}



	// Send an upstream message.
	public void sendMessage() {

		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					Bundle data = new Bundle();
					data.putString("my_message", "Hello World");
					data.putString("my_action", "com.google.android.gcm.demo.app.ECHO_NOW");
					String id = Integer.toString(msgId.incrementAndGet());
					gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
					msg = "Sent message";
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				eLog.i(GCMUtils.TAG, "GCM MESSAGE SENDING RESPONSE :\n"+ msg);
			}
		}.execute(null, null, null);
	}


	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	public SharedPreferences getGcmPreferences() {
		// This sample app persists the registration ID in shared preferences, but
		// how you store the regID in your app is up to you.
		return context.getSharedPreferences(GCMActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}
	/**
	 * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
	 * messages to your app. Not needed for this demo since the device sends upstream messages
	 * to a server that echoes back the message using the 'from' address in the message.
	 */
	public void sendRegistrationIdToBackend(final String registrationId) {
		if(JavaUtils.isNotEmpty(registrationId)){
			final Device device = new Device(context, registrationId);

			new Thread(){@Override public void run(){Looper.myLooper();
			boolean updated = WDeviceDAO.updateDevice(context, device);
				eLog.v("GCM", "sendRegistrationToBackEnd: =" + updated);
			}}.start();
		}
	}
	
	public static boolean updateRegistrationId(Context c, LocalDBManager db, boolean force){
		GCMUtils GCM = new GCMUtils(c);
		String regId = GCM.getRegistrationId();
		if(JavaUtils.isEmpty(regId) || force){
			try {
				return GCM.getNewRegId(true)!=null;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean hasRegistrationId(Context c){
		String regId = getRegistrationId();
		return JavaUtils.isNotEmpty(regId);
	}
}
