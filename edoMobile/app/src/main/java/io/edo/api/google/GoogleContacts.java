package io.edo.api.google;

import android.content.Context;

import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.gdata.client.Query;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.Link;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.contacts.ContactGroupEntry;
import com.google.gdata.data.contacts.ContactGroupFeed;
import com.google.gdata.util.ServiceException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import io.edo.db.local.beans.LService;
import io.edo.utilities.eLog;


public class GoogleContacts {
	private static final String TAG = "GoogleContact";

	ContactsService contactsService;

	LService localService;
	Context c;

	boolean isAttempt = false;


	public GoogleContacts(Context context, LService localService) {
		super();
		this.localService = localService;
		this.c=context;

		contactsService = new ContactsService(EdoGoogleManager.APPLICATION_NAME);
		contactsService.setHeader("Authorization", "Bearer " + localService.getAccessToken());
	}





	//*********************************************************************
	//*		AUTHORIZATION TOKEN
	//*********************************************************************
	private boolean refreshAccessToken() {
		eLog.d(TAG, "START GOOGLE ACCESS TOKEN REQUEST");
		localService = EdoGoogleManager.refreshAccessToken(c, localService);
		return localService != null;
	}




	//*********************************************************************
	//*		GROUPS
	//*********************************************************************
	private static final String GROUPS_URL = "https://www.google.com/m8/feeds/groups/default/full";

	private String getIdOfMyGroup() {


		try {
			URL feedUrl = new URL(GROUPS_URL);

			ContactGroupFeed resultFeed = contactsService.getFeed(feedUrl, ContactGroupFeed.class);
			// "My Contacts" group id will always be the first one in the answer
			ContactGroupEntry entry = resultFeed.getEntries().get(0);

			isAttempt=false;
			return entry.getId();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GoogleJsonResponseException e) {
			GoogleJsonError error = e.getDetails();
			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));


			if(error==null || error.getCode() ==  GoogleRespCode.ERROR_AUTHORIZATION){
				if(refreshAccessToken() && !isAttempt){
					isAttempt=true;
					return getIdOfMyGroup();
				}
			}


		} catch (IOException e) {
			eLog.e(TAG,"IOEXCEPTION Message:\n"+e.getMessage());
		} catch (NullPointerException e){
			if(refreshAccessToken() && !isAttempt){
				isAttempt=true;
				return getIdOfMyGroup();
			}
		}
		return null;
	}



	//*********************************************************************
	//*		CONTACTS
	//*********************************************************************

	private static final String CONTACTS_URL = "https://www.google.com/m8/feeds/contacts/default/full";
	private static final int MAX_NB_CONTACTS = 1000;

	public List<ContactEntry> getContacts() {

		try {
			URL feedUrl = new URL(CONTACTS_URL);
			Query myQuery = new Query(feedUrl);
			// to retrieve contacts of the group I found just above
			String groupId = getIdOfMyGroup();
			myQuery.setStringCustomParameter("group", groupId);
			myQuery.setMaxResults(MAX_NB_CONTACTS);
			ContactFeed resultFeed = contactsService.query(myQuery, ContactFeed.class);
			List<ContactEntry> contactEntries = resultFeed.getEntries();

			isAttempt=false;
			return contactEntries;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GoogleJsonResponseException e) {
			GoogleJsonError error = e.getDetails();
			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));


			if(error==null || error.getCode() ==  GoogleRespCode.ERROR_AUTHORIZATION){
				if(refreshAccessToken() && !isAttempt){
					isAttempt=true;
					return getContacts();
				}
			}

		} catch (NullPointerException e) {


			if(refreshAccessToken() && !isAttempt){
				isAttempt=true;
				return getContacts();
			}

		} catch (IOException e) {
			eLog.e(TAG,"IOEXCEPTION Message:\n"+e.getMessage());
		}
		return null;
	}

	public ContactEntry getContactEntry(String contactId) {
		
		ContactEntry contact = null;
		try {
			
			contact = contactsService.getEntry(new URL(contactId), ContactEntry.class);
			Link photoLink = contact.getContactPhotoLink();
			
		} catch (MalformedURLException e) {
			eLog.e(TAG, "MalformedURLException");
			e.printStackTrace();
		} catch (ServiceException e) {
			eLog.e(TAG, "ServiceException");
			e.printStackTrace();
		} catch (GoogleJsonResponseException e) {
			GoogleJsonError error = e.getDetails();
			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));


			if(error==null || error.getCode() ==  GoogleRespCode.ERROR_AUTHORIZATION){
				if(refreshAccessToken() && !isAttempt){
					isAttempt=true;
					return getContactEntry(contactId);
				}
			}

		} catch (NullPointerException e) {
			eLog.e(TAG, "NullPointerException");

			if(refreshAccessToken() && !isAttempt){
				isAttempt=true;
				return getContactEntry(contactId);
			}

		} catch (IOException e) {
			eLog.e(TAG,"IOEXCEPTION Message:\n"+e.getMessage());
		}
		// Do something with the contact.
		return contact;
	}


	public boolean downloadPhoto(Link photoLink, String filePath){
		try{
			eLog.d(TAG, "CONTACT PHOTO LINK: "+photoLink);
			if (photoLink != null && contactsService!=null) {
				InputStream in = contactsService.getStreamFromLink(photoLink);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				RandomAccessFile file = new RandomAccessFile(filePath, "rw");
				byte[] buffer = new byte[4096];
				while (true) {
					int read;
					if ((read = in.read(buffer)) != -1) {
						out.write(buffer, 0, read);
					} else {
						break;
					}
				}
				file.write(out.toByteArray());
				file.close();
				return true;
			}
		} catch (MalformedURLException e) {
			eLog.e(TAG, "MalformedURLException");
			e.printStackTrace();
		} catch (ServiceException e) {
			eLog.e(TAG, "ServiceException");
			e.printStackTrace();
		} catch (GoogleJsonResponseException e) {
			GoogleJsonError error = e.getDetails();
			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));


			if(error==null || error.getCode() ==  GoogleRespCode.ERROR_AUTHORIZATION){
				if(refreshAccessToken() && !isAttempt){
					isAttempt=true;
					return downloadPhoto(photoLink, filePath);
				}
			}

		} catch (NullPointerException e) {
			eLog.e(TAG, "NullPointerException");

			if(refreshAccessToken() && !isAttempt){
				isAttempt=true;
				return downloadPhoto(photoLink, filePath);
			}

		} catch (IOException e) {
			eLog.e(TAG,"IOEXCEPTION Message:\n"+e.getMessage());
		}
		return false;
	}
	
	public boolean downloadPhoto(ContactEntry entry, String filePath){
		return downloadPhoto(entry.getContactPhotoLink(), filePath);
	}
	
}
