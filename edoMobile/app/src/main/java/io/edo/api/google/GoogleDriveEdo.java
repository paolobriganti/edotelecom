package io.edo.api.google;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.PermissionList;
import com.paolobriganti.utils.UniversalProgressListener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.edo.api.ApiResponse;
import io.edo.api.FileDownloadInfo;
import io.edo.api.FileUploadInfo;
import io.edo.db.global.uibeans.ContactFileUIDAO;
import io.edo.db.global.uibeans.FileUIDAO;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LFileDAO;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.LServiceDAO;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;
import io.edo.ui.OnProgressChangeListener;

public class GoogleDriveEdo {





	//*********************************************************************
	//*		GOOGLE DRIVE: TOOLS
	//*********************************************************************

	public static Drive getGoogleDrive(Context context, String serviceId){
		LService lService = new LServiceDAO(context).get(serviceId);
		GoogleDrive drive = new GoogleDrive(context, lService);
		return drive.getDriveService();
	}










	//*********************************************************************
	//*		GOOGLE DRIVE: UPLOAD
	//*********************************************************************

	public static ApiResponse uploadFile(Context context, EdoContainer container, LSpace space, LFile edoFile, FileUploadInfo fui, UniversalProgressListener listener){
		com.google.api.services.drive.model.File cloudFile = null;
		LFile folder = space.getCurrentFolder();
		if(folder!=null){
			LService lService = space.getService(context, false);

			if(folder.getCloudId()!=null){
				GoogleDrive drive = new GoogleDrive(context, lService);
				fui.setParentCloudId(folder.getCloudId());
				GoogleResponse response = drive.uploadFile(fui, listener);

				if(response!=null && response.result!=null && response.result instanceof File){
					cloudFile = (File) response.result;
					if(cloudFile!=null){
						edoFile = new LFileDAO(context).get(edoFile.getId());
						if(edoFile!=null){
							edoFile.setCloudId(cloudFile.getId());
							edoFile.setWebUri(cloudFile.getAlternateLink());
							if(cloudFile.getMimeType()!=null){
								edoFile.setType(cloudFile.getMimeType());
							}
							if(container.isContactOrGroup()){
								new ContactFileUIDAO(context).fileUploaded((LContact)container, space, edoFile, fui.a);
							}
							
							listener.onPostFinish();
							
							return new ApiResponse(cloudFile.getId(), (Integer)null);
						}
					}

				}else if(response!=null && response.errorCode!=null && response.errorCode == GoogleRespCode.ERROR_FILE_NOT_FOUND){
//					LSpace newSpace = CloudFolderRegeneration.regenerateFolder(context, lService, container, space);
//					if(newSpace!=null){
//						return uploadFile(context, container, newSpace, edoFile, fui, listener);
//					}
				}else if(response!=null && response.errorCode!=null && response.errorCode == GoogleRespCode.ERROR){

					return new ApiResponse(response.result, GoogleRespCode.ERROR);

				}
			}else{
//				LSpace newSpace = CloudFolderRegeneration.generateFolder(context, lService, container, space);
//				if(newSpace!=null){
//					return uploadFile(context, container, newSpace, edoFile, fui, listener);
//				}
			}
		}



		return null;

	}

















	//*********************************************************************
	//*		GOOGLE DRIVE: DOWNLOAD
	//*********************************************************************



	public static ApiResponse downloadFile(Context c, LSpace space, LFile edoFile, FileDownloadInfo fdi, UniversalProgressListener progressListener){

		final GoogleDrive drive = new GoogleDrive(c, space.getService(c, false));

		fdi.setFileDownloadId(edoFile.getCloudId());

		int responseCode = drive.downloadFileDefault(fdi, progressListener);
		FileUIDAO fileDAO = new FileUIDAO(c);
		if(responseCode == GoogleRespCode.SUCCESS){
			edoFile.setLocalUri(fdi.getFilePath());
			fileDAO.fileDownloaded(edoFile, fdi.a);
			
			progressListener.onPostFinish();
			
			return new ApiResponse(fdi.getFilePath(), (Integer)null);
		}else if(responseCode == GoogleRespCode.ERROR_FILE_NOT_FOUND){
			edoFile.setCloudId(null);
			fileDAO.getLDAO().update(edoFile);
		}

		fileDAO.removeLocalFile(edoFile);
		return new ApiResponse(null, ApiResponse.getApiResponseCodeFromGoogleResponse(responseCode));
	}






//	public static ApiResponse getSaveThumbnail(Context c, String serviceId, LFile efile, String thumbnailLink, int heightSizePx){
//		GoogleResponse response = null;
//		if(efile.getCloudId()!=null){
//			String fileKind = efile.getKind();
//			//			String fileType = efile.getType();
//			if((fileKind!=null && (fileKind.equals(FileInfo.TYPE_LAYOUT) ||
//					fileKind.equals(FileInfo.TYPE_PRESENTATION) ||
//					fileKind.equals(FileInfo.TYPE_RASTER) ||
//					fileKind.equals(FileInfo.TYPE_SPREADSHEET) ||
//					fileKind.equals(FileInfo.TYPE_VECTOR) ||
//					fileKind.equals(FileInfo.TYPE_VIDEO) ||
//					fileKind.equals(FileInfo.TYPE_GDOC) ||
//					fileKind.equals(FileInfo.TYPE_TEXT) ||
//					fileKind.equals(FileInfo.TYPE_WEB)))
//					){
//				LService lService = ServiceDAO.getService(c, serviceId);
//				GoogleDrive drive = new GoogleDrive(c, lService);
//				if(JavaUtils.isEmpty(thumbnailLink)){
//					response = drive.getDriveFile(efile.getCloudId());
//					if(response!=null){
//						com.google.api.services.drive.model.File file = (File) response.result;
//						if(file!=null){
//							thumbnailLink = file.getThumbnailLink();
//						}else if(response.errorCode!=null && response.errorCode == GoogleRespCode.ERROR_FILE_NOT_FOUND){
//							LFileDAO lfd = new LFileDAO(c);
//							efile.setCloudId(null);
//							lfd.updateFile(null, efile, true, false);
//
//						}
//					}
//				}
//				if(JavaUtils.isNotEmpty(thumbnailLink)){
//
//					while(heightSizePx>1){
//						String[] linkParts = thumbnailLink.split("=s");
//						if(linkParts.length>0){
//							thumbnailLink = linkParts[0];
//						}
//						thumbnailLink = thumbnailLink+"=s"+heightSizePx;
//
//
//						response = drive.download(thumbnailLink);
//						InputStream is = response!=null ? (InputStream) (response.result):null;
//						if(is!=null){
//							try{
//								Bitmap src = BitmapFactory.decodeStream(is);
//								if(src!=null){
//									ThumbnailManager.resizeAndSaveThumbnail(c, efile, src, heightSizePx);
//									src.recycle();
//									src = null;
//									return new ApiResponse(true, (Integer)null);
//								}
//							}catch (OutOfMemoryError e){
//								heightSizePx=(heightSizePx*2)/3;
//							}
//						}
//					}
//				}
//			}
//		}
//		return new ApiResponse(false, response);
//	}


	public static Bitmap getThumbnailImage(Context c, String serviceId, String thumbnailLink){	
		LService lService = new LServiceDAO(c).get(serviceId);
		GoogleDrive drive = new GoogleDrive(c, lService);

		GoogleResponse response = drive.download(thumbnailLink);
		InputStream is = response!=null ? (InputStream) (response.result):null;
		if(is!=null){
			Bitmap src = BitmapFactory.decodeStream(is);
			if(src!=null){
				return src;
			}
		}
		return null;
	}



	public static ApiResponse getThumbnailUrl(Context c, String serviceId, LFile efile, int heightSizePx){
		LService lService = new LServiceDAO(c).get(serviceId);
		GoogleDrive drive = new GoogleDrive(c, lService);


		GoogleResponse response = drive.getDriveFile(efile.getCloudId());
		if(response!=null){
			com.google.api.services.drive.model.File file = (File) response.result;
			if(file!=null){
				String thumbnailLink = file.getThumbnailLink();
				String[] linkParts = thumbnailLink.split("=s");
				if(linkParts.length>0){
					thumbnailLink = linkParts[0];
				}
				thumbnailLink = thumbnailLink+"=s"+heightSizePx;

				return new ApiResponse(thumbnailLink, (Integer)null);
			}else if(response.errorCode!=null && response.errorCode == GoogleRespCode.ERROR_FILE_NOT_FOUND){
				FileUIDAO lfd = new FileUIDAO(c);
				efile.setCloudId(null);
				lfd.getLDAO().update(efile);
			}
		}
		return new ApiResponse(null, response);
	}

	public static ApiResponse getDownloadUrl(Context c, String serviceId, LFile efile){
		LService lService = new LServiceDAO(c).get(serviceId);
		GoogleDrive drive = new GoogleDrive(c, lService);

		GoogleResponse response = drive.getDriveFile(efile.getCloudId());
		if(response!=null){
			com.google.api.services.drive.model.File file = (File) response.result;
			if(file!=null){
				return new ApiResponse(file.getDownloadUrl(), (Integer)null);
			}else if(response.errorCode!=null && response.errorCode == GoogleRespCode.ERROR_FILE_NOT_FOUND){
				FileUIDAO lfd = new FileUIDAO(c);
				efile.setCloudId(null);
				lfd.getLDAO().update(efile);
			}
		}
		return new ApiResponse(null, response);
	}


	public static ApiResponse getAlternateLink(Context c, String serviceId, LFile efile){
		LService lService = new LServiceDAO(c).get(serviceId);
		GoogleDrive drive = new GoogleDrive(c, lService);

		GoogleResponse response = drive.getDriveFile(efile.getCloudId());
		if(response!=null){
			com.google.api.services.drive.model.File file = (File) response.result;
			if(file!=null){
				return new ApiResponse(file.getAlternateLink(), (Integer)null);
			}else if(response.errorCode!=null && response.errorCode == GoogleRespCode.ERROR_FILE_NOT_FOUND){
				FileUIDAO lfd = new FileUIDAO(c);
				efile.setCloudId(null);
				lfd.getLDAO().update(efile);
			}
		}
		return new ApiResponse(null, response);
	}

	public static ApiResponse getDefaultOpenWithLink(Context c, String serviceId, LFile efile){
		LService lService = new LServiceDAO(c).get(serviceId);
		GoogleDrive drive = new GoogleDrive(c, lService);

		GoogleResponse response = drive.getDriveFile(efile.getCloudId());
		if(response!=null){
			com.google.api.services.drive.model.File file = (File) response.result;
			if(file!=null){
				return new ApiResponse(file.getDefaultOpenWithLink(), (Integer)null);
			}else if(response.errorCode!=null && response.errorCode == GoogleRespCode.ERROR_FILE_NOT_FOUND){
				FileUIDAO lfd = new FileUIDAO(c);
				efile.setCloudId(null);
				lfd.getLDAO().update(efile);
			}
		}
		return new ApiResponse(null, response);
	}

	public static ApiResponse getFileSizeLong(Context c, String serviceId, LFile efile){
		LService lService = new LServiceDAO(c).get(serviceId);
		GoogleDrive drive = new GoogleDrive(c, lService);

		GoogleResponse response = drive.getDriveFile(efile.getCloudId());
		if(response!=null){
			com.google.api.services.drive.model.File file = (File) response.result;
			if(file!=null){
				return new ApiResponse(file.getFileSize(), (Integer)null);
			}else if(response.errorCode!=null && response.errorCode == GoogleRespCode.ERROR_FILE_NOT_FOUND){
				FileUIDAO lfd = new FileUIDAO(c);
				efile.setCloudId(null);
				lfd.getLDAO().update(efile);
			}
		}
		return new ApiResponse(null, response);
	}


	//*********************************************************************
	//*		GOOGLE DRIVE: OTHER
	//*********************************************************************
	public static LContact[] getContactsOfSharedFiles(Context c, String serviceId, int limit, final OnProgressChangeListener listener){
		LService lService = new LServiceDAO(c).get(serviceId);
		GoogleDrive drive = new GoogleDrive(c, lService);

		GoogleResponse response = drive.getSharedFiles();

		Map<String, LContact> allContacts=Collections.synchronizedMap(new HashMap<String, LContact>());
		
		ArrayList<File> sharedFiles = new ArrayList<File>();
		if(response!=null && response.result != null){
			sharedFiles = (ArrayList<File>) response.result;
			if(sharedFiles!=null && sharedFiles.size()>0){
				for(File file: sharedFiles){
					PermissionList list = drive.retrievePermissions(file.getId());
					if(list!=null && list.getItems().size()>0){
						for(int i=0; i<list.getItems().size(); i++){
							
							String name = list.getItems().get(i).getName();
							String emailAddress = list.getItems().get(i).getEmailAddress();
							
							LContact contact = new LContact(name, 
									null, 
									emailAddress);
							
							allContacts.put(emailAddress, contact);
							
							if(allContacts.size()>=limit)
								break;
						}
					}
					if(allContacts.size()>=limit)
						break;
				}

				if(allContacts.size()>0)
					return allContacts.values().toArray(new LContact[allContacts.values().size()]);
			}
		}
		return null;
	}

}
