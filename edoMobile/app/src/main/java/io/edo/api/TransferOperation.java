package io.edo.api;

import java.io.Serializable;

public class TransferOperation {
	public String id;
	public String action;
	public Serializable source;
	
	public TransferOperation(String id, String action, Serializable source) {
		super();
		this.id = id;
		this.action = action;
		this.source = source;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransferOperation other = (TransferOperation) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
