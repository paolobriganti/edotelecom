package io.edo.api;


import com.paolobriganti.utils.FileInfo;

import java.io.File;

import io.edo.db.local.beans.LActivity;

public class FileDownloadInfo {

	private String filePath;
	private String fileMime;
	private File fileContent;
	
	private String fileDownloadId;
	private long range = 0;

	public LActivity a;

	private boolean stopDownload = false;
	
	public FileDownloadInfo(String folderPath, String fileName, String fileExtension) {
		super();
		this.filePath = folderPath+File.separator+fileName+"."+fileExtension;
		this.fileMime = FileInfo.getMime(filePath);
		
		
		try{
			this.fileContent = new File(filePath);
			this.fileContent.getParentFile().mkdirs();
		}catch(NullPointerException e){
			
		}
	}
    
	public FileDownloadInfo(String filePath) {
		super();
		this.filePath = filePath;
		this.fileMime = FileInfo.getMime(filePath);
		
		this.fileContent = new File(filePath);
		this.fileContent.getParentFile().mkdirs();
	}
    
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileMime() {
		return fileMime;
	}

	public void setFileMime(String fileMime) {
		this.fileMime = fileMime;
	}

	public File getFileContent() {
		return fileContent;
	}

	public void setFileContent(File fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileDownloadId() {
		return fileDownloadId;
	}

	public void setFileDownloadId(String fileDownloadId) {
		this.fileDownloadId = fileDownloadId;
	}

	public long getRange() {
		return range;
	}

	public void setRange(long range) {
		this.range = range;
	}

	public boolean isStopDownload() {
		return stopDownload;
	}

	public void setStopDownload(boolean stopDownload) {
		this.stopDownload = stopDownload;
	}

}
