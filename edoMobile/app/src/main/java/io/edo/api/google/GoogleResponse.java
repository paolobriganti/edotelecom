package io.edo.api.google;

public class GoogleResponse {
	public Object result;
	public Integer errorCode;
	
	
	public GoogleResponse(Object result, Integer errorCode) {
		super();
		this.result = result;
		this.errorCode = errorCode;
	}
	
	
}
