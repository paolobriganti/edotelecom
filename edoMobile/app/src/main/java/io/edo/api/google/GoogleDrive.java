package io.edo.api.google;

import android.content.Context;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.drive.model.PermissionList;
import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.UniversalProgressListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import javax.net.ssl.HttpsURLConnection;

import io.edo.api.FileDownloadInfo;
import io.edo.api.FileUploadInfo;
import io.edo.api.Upload;
import io.edo.db.local.beans.LService;
import io.edo.utilities.eLog;

public class GoogleDrive {
	private static final String TAG = "GoogleDrive";
	public static final String MIME_TYPE_FOLDER = "application/vnd.google-apps.folder";

	public static final String DRIVE_HOST = "https://www.googleapis.com";
	public static final String DRIVE_URL = "/drive/v2";
	public static final String DRIVE_UPLOAD_URL = "/upload/drive/v2/files";
	public static final String DRIVE_DOWNLOAD_URL = DRIVE_URL+"/files";


	private boolean stop = false;
	LService localService;
	Drive drive;
	Drive.Builder builder;
	Context c;

	boolean isAttempt = false;
	int retryCount = 0;

	public GoogleDrive(Context context, LService localService) {
		super();
		this.c=context;
		this.localService = localService;
		if(localService!=null)
			setDriveService(localService.getAccessToken());
	}

	public void setLService(LService localService){
		this.localService = localService;
	}

	public void stopProcesses(){
		stop = true;
	}

	public void setDriveService(String accessToken){
		Credential credential = new GoogleCredential().setAccessToken(accessToken);
		builder = new Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential).setHttpRequestInitializer(credential);
		builder.setApplicationName(EdoGoogleManager.APPLICATION_NAME);
		drive = builder.build();
	}


	public Drive getDriveService(){
		return drive;
	}



	//*********************************************************************
	//*		AUTHORIZATION TOKEN
	//*********************************************************************
	private boolean refreshAccessToken() {
		localService = EdoGoogleManager.refreshAccessToken(c, localService);
		if(localService!=null){
			setDriveService(localService.getAccessToken());
			return true;
		}
		return false;
	}







	//*********************************************************************
	//*		GOOGLE DRIVE: TOOLS
	//*********************************************************************


	public GoogleResponse getAbout(){
		try {
			About about = drive.about().get().execute();
			isAttempt=false;
			return new GoogleResponse(about, null);
		} catch (GoogleJsonResponseException e) {
			GoogleJsonError error = e.getDetails();
			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));

			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION)
				if(refreshAccessToken() && !isAttempt){
					isAttempt=true;
					return getAbout();
				}
			return new GoogleResponse(null, error.getCode());
		} catch (IOException e) {
			eLog.e(TAG,"An error occurred: " + e);
		}
		return null;
	}






	public GoogleResponse getDriveFile(String fileId){
		if(JavaUtils.isNotEmpty(fileId)){
			try {
				File file = drive.files().get(fileId).execute();
				if(file!=null){
					isAttempt=false;
					return new GoogleResponse(file, null);
				}
			} catch (GoogleJsonResponseException e) {
				GoogleJsonError error = e.getDetails();
				eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
				eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));

				if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
					if(refreshAccessToken() && !isAttempt){
						isAttempt=true;
						return getDriveFile(fileId);
					}
				}
				return new GoogleResponse(null, error.getCode());
			} catch (IOException e) {
				eLog.e(TAG,"An error occurred: " + e);
			}
		}
		return null;
	}




	public GoogleResponse getAllFiles(){
		ArrayList<File> result = new ArrayList<File>();
		try {
			Files.List request = drive.files().list();
			do {
				try {
					FileList files = request.setQ("trashed = false").execute();
					result.addAll(files.getItems());
					request.setPageToken(files.getNextPageToken());
					isAttempt=false;
				} catch (GoogleJsonResponseException e) {
					GoogleJsonError error = e.getDetails();
					eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
					eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));

					if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION)
						if(refreshAccessToken() && !isAttempt){
							isAttempt=true;
							return getAllFiles();
						}

					return new GoogleResponse(null, error.getCode());

				}
			} while (request.getPageToken() != null &&
					request.getPageToken().length() > 0);
		} catch (IOException e) {
			eLog.e(TAG,"An error occurred: " + e);
		}
		return new GoogleResponse(result, null);
	}


	public GoogleResponse getAllFilesOfFolder(String folderId){
		ArrayList<File> result = new ArrayList<File>();
		try {
			Files.List request = drive.files().list();
			do {
				try {
					folderId = folderId==null? "root" : folderId;
					String query = "'"+folderId+"'" + " in parents and trashed = false";
					eLog.i(TAG, ""+query);
					FileList files = request.setQ(query).execute();
					result.addAll(files.getItems());
					request.setPageToken(files.getNextPageToken());
					isAttempt=false;
				} catch (GoogleJsonResponseException e) {
					GoogleJsonError error = e.getDetails();
					eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
					eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));

					if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION)
						if(refreshAccessToken() && !isAttempt){
							isAttempt=true;
							return getAllFilesOfFolder(folderId);
						}

					return new GoogleResponse(null, error.getCode());

				}
			} while (request.getPageToken() != null &&
					request.getPageToken().length() > 0);
		} catch (IOException e) {
			eLog.e(TAG,"An error occurred: " + e);
		}
		eLog.i(TAG, "files num: "+result.size());
		return new GoogleResponse(result, null);
	}



	public GoogleResponse getSharedFiles(){
		ArrayList<File> result = new ArrayList<File>();
		try {
			Files.List request = drive.files().list();
			do {
				try {
					FileList files = request.setQ("sharedWithMe = true and trashed = false").execute();
					result.addAll(files.getItems());
					request.setPageToken(files.getNextPageToken());
					isAttempt=false;
				} catch (GoogleJsonResponseException e) {
					GoogleJsonError error = e.getDetails();
					eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
					eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));

					if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION)
						if(refreshAccessToken() && !isAttempt){
							isAttempt=true;
							return getAllFiles();
						}

					return new GoogleResponse(null, error.getCode());

				}
			} while (request.getPageToken() != null &&
					request.getPageToken().length() > 0);
		} catch (IOException e) {
			eLog.e(TAG,"An error occurred: " + e);
		}
		return new GoogleResponse(result, null);
	}


	public PermissionList retrievePermissions(String fileId) {
		try {
			PermissionList permissions = drive.permissions().list(fileId).execute();
			return permissions;
		} catch (IOException e) {
			System.out.println("An error occurred: " + e);
		}

		return null;
	}



	//	public GoogleResponse generateEdoFolder(){
	//		try {
	//			// File's metadata.
	//			File body = new File();
	//			body.setTitle(Constants.CLOUD_FOLDER_EDO_STORAGE);
	//			body.setMimeType("application/vnd.google-apps.folder");
	//			File edoDir = drive.files().insert(body).execute();
	//			if (edoDir != null) {
	//				eLog.i(TAG,"EDO FOLDER CREATED!");
	//			}
	//			isAttempt=false;
	//			return new GoogleResponse(edoDir, null);
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION)
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return generateEdoFolder();
	//				}
	//			return new GoogleResponse(null, error.getCode());
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//		return null;
	//	}
	//
	//
	//	public GoogleResponse createFolder(String parentId, String folderName){
	//		try {
	//			// File's metadata.
	//			File body = new File();
	//			body.setTitle(folderName);
	//			body.setMimeType("application/vnd.google-apps.folder");
	//
	//			if(parentId!=null){
	//				ParentReference parent = new ParentReference();
	//				parent.setId(parentId);
	//				body.setParents(Arrays.asList(parent)); 
	//			}
	//
	//			File dir = drive.files().insert(body).execute();
	//			if (dir != null) {
	//				eLog.i(TAG, "FOLDER "+folderName+" CREATED!");
	//			}
	//			isAttempt=false;
	//			return new GoogleResponse(dir, null);
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG, "FOLDER "+folderName+" NOT CREATED!");
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION)
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return createFolder(parentId, folderName);
	//				}
	//			return new GoogleResponse(null, error.getCode());
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//		return null;
	//	}






	//*********************************************************************
	//*		GOOGLE DRIVE: UPLOAD
	//*********************************************************************

	public synchronized GoogleResponse uploadFile(FileUploadInfo fui, final UniversalProgressListener progressListener) {
		String uploadId = fui.getUploadId();
		if(JavaUtils.isEmpty(uploadId)){
			uploadId = getUploadID(fui);
			fui.setUploadId(uploadId);
			if(JavaUtils.isNotEmpty(uploadId)){
				return putFileWithUploadID(fui, progressListener);			
			}			
		}else{
			long range = requestUploadStatus(fui);
			fui.setRange(range);
			return putFileWithUploadID(fui, progressListener);			
		}



		return null;
	}


	private GoogleResponse putFileWithUploadID(FileUploadInfo fui, final UniversalProgressListener progressListener) {
		eLog.d(TAG, "[putFileWithUploadID] +++");

		String token = localService.getAccessToken();
		String mimeType = fui.getFileMime();
		String uploadId = fui.getUploadId();
		long range = fui.getRange();
		java.io.File fileContent = fui.getFileContent();

		int chunkSize = Upload.MINIMUM_CHUNK_SIZE;

		String fileName = fileContent.getName();
		String contentLength = String.valueOf(fileContent.length());

		eLog.i(TAG, "[putFileWithUploadID] uploadId:" + uploadId);
		eLog.i(TAG, "[putFileWithUploadID] range:" + range);
		eLog.v(TAG, "[putFileWithUploadID] chunkSize:" + chunkSize);
		eLog.v(TAG, "[putFileWithUploadID] fileName : " + fileName);
		eLog.v(TAG, "[putFileWithUploadID] contentLength : " + contentLength);
		eLog.v(TAG, "[putFileWithUploadID] mimeType : " + mimeType);

		int responseCode = GoogleRespCode.ERROR;
		String responseString = null;
		long totalBytesFromDataInputStream = 0;

		try {
			String url = DRIVE_HOST+DRIVE_UPLOAD_URL+"?uploadType=resumable&upload_id=" + uploadId;
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("PUT");
			con.setRequestProperty("Authorization", "Bearer " + token);
			con.setRequestProperty("Content-Type", mimeType);
			//con.setFixedLengthStreamingMode(MediaHttpUploader.MINIMUM_CHUNK_SIZE);

			long nextByte = 0;
			if (range == 0) {               
				con.setRequestProperty("Content-Length", contentLength);
			}
			else {
				nextByte = range + 1;
				long remainContentLength = Long.parseLong(contentLength) - nextByte;
				con.setRequestProperty("Content-Length", String.valueOf(remainContentLength));
				String contentRange = "bytes " + (range + 1) + "-" + ( Long.parseLong(contentLength) - 1) + "/" + contentLength;
				con.setRequestProperty("Content-Range", contentRange);

				eLog.d(TAG, "[putFileWithUploadID] Content-Length : " + String.valueOf(remainContentLength));
				eLog.d(TAG, "[putFileWithUploadID] Content-Range : " + contentRange);
			}
			con.setDoOutput(true); 
			con.setChunkedStreamingMode(chunkSize);

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());

			DataInputStream inputStream = new DataInputStream(new FileInputStream(fileContent)); 

			if(progressListener!=null)progressListener.onPreStart(inputStream,null);
			if(progressListener!=null)progressListener.onStart(inputStream,null);

			int lastPercent = 0;
			long totalSize = fileContent.length();
			int bytes = 0;              
			byte[] bufferOut = new byte[chunkSize];
			while ((bytes = inputStream.read(bufferOut)) != -1) {      

				if (nextByte > 0) {
					nextByte = nextByte - bytes;
					totalBytesFromDataInputStream += bytes;
					if (nextByte >= 0) continue;
				}

				wr.write(bufferOut, 0, bytes);  
				totalBytesFromDataInputStream += bytes;

				eLog.d(TAG, "[putFileWithUploadID] progress: " + totalBytesFromDataInputStream+"/"+totalSize);

				// Here you can call the method which updates progress
				// be sure to wrap it so UI-updates are done on the main thread!
				int percent = (int) (100 * totalBytesFromDataInputStream / totalSize);
				if(lastPercent!=percent){
					lastPercent = percent;
					if(progressListener!=null)progressListener.onProgressChanged(inputStream, null, percent);
					eLog.d(TAG, "[putFileWithUploadID] percent:" + percent);
				}


				if(fui.isStopUpload())
					break;
			}                                  

			eLog.d(TAG, "[putFileWithUploadID] totalBytesFromDataInputStream:" + totalBytesFromDataInputStream);
			wr.flush();
			wr.close();

			responseCode = con.getResponseCode();

			eLog.d(TAG, "[putFileWithUploadID] Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			responseString = response.toString();

			//	        eLog.d(TAG, "[putFileWithUploadID] isUploadCompleted:" + isUploadCompleted);
			//	        eLog.d(TAG, "[putFileWithUploadID] isInterrupted:" + isInterrupted);
		} catch (Exception e) {
			e.printStackTrace();            
			//	        eLog.d(TAG, "[putFileWithUploadID] isInterrupted:" + isInterrupted);
			//	        eLog.d(TAG, "[putFileWithUploadID] totalBytesFromDataInputStream:" + totalBytesFromDataInputStream);
		} catch (OutOfMemoryError e){}


		fui.setUploadId(uploadId);
		fui.setRange(totalBytesFromDataInputStream);



		if(!fui.isStopUpload()){

			if(responseCode == GoogleRespCode.ERROR_AUTHORIZATION){
				if(refreshAccessToken() && !isAttempt){
					isAttempt=true;
					return putFileWithUploadID(fui, progressListener);
				}
			}else if(responseCode >= GoogleRespCode.ERROR){


				if(ASI.isInternetOn(c)){
					retryCount++;

					try {
						Thread.sleep(retryCount*200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					return putFileWithUploadID(fui, progressListener);
				}else{

					return new GoogleResponse(fui, responseCode);
				}

			}


			retryCount = 0;
			isAttempt=false;


			eLog.v(TAG, "RESPONSE = "+responseString);

			if(JavaUtils.isJSONValid(responseString)){

				File file = getFileFromJson(responseString);

				if(progressListener!=null)progressListener.onFinish();

				return new GoogleResponse(file, null);
			}
		}

		eLog.d(TAG, "[putFileWithUploadID] ---");

		return null;
	}

	private String getUploadID(FileUploadInfo fui) {         
		eLog.d(TAG, "[sendResumableHttpRequest] +++");

		String token = localService.getAccessToken();
		String parentCloudId = fui.getParentCloudId();
		String mimeType = fui.getFileMime();
		String upload_id = fui.getUploadId();

		java.io.File fileContent = fui.getFileContent();

		String fileName = fileContent.getName();
		String contentLength = String.valueOf(fileContent.length());

		eLog.d(TAG, "[sendResumableHttpRequest] fileName : " + fileName);
		eLog.d(TAG, "[sendResumableHttpRequest] contentLength : " + contentLength);
		eLog.d(TAG, "[sendResumableHttpRequest] mimeType : " + mimeType);

		int responseCode = GoogleRespCode.ERROR;

		try {
			String url = DRIVE_HOST+DRIVE_UPLOAD_URL+"?uploadType=resumable";
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Authorization", "Bearer " + token);
			con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			con.setRequestProperty("X-Upload-Content-Type", mimeType);
			con.setRequestProperty("X-Upload-Content-Length", contentLength);       

			JSONObject parent = new JSONObject();
			parent.put("id", parentCloudId);

			JSONArray parents = new JSONArray();
			parents.put(parent);

			JSONObject jobj = new JSONObject();
			jobj.put("title", fileName);
			jobj.put("parents", parents);

			eLog.e(TAG, "[sendResumableHttpRequest] JSONBody : " + jobj.toString());

			byte[] postData = jobj.toString().getBytes();           

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.write(postData);
			wr.flush();
			wr.close();

			responseCode = con.getResponseCode();
			String location = con.getHeaderField("Location");
			if (location!=null && location.contains("upload_id")) {
				String[] uploadParameters = location.split("upload_id");
				upload_id = uploadParameters[1].replace("=", "");
			}else{
				responseCode = GoogleRespCode.ERROR_AUTHORIZATION;
			}

			eLog.d(TAG, "[sendResumableHttpRequest] Response Code : " + responseCode);
			eLog.d(TAG, "[sendResumableHttpRequest] Response Location : " + location);
			eLog.d(TAG, "[sendResumableHttpRequest] Response uploadID : " + upload_id);

			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();            
		}               


		if(responseCode == GoogleRespCode.ERROR_AUTHORIZATION){
			if(refreshAccessToken() && !isAttempt){
				isAttempt=true;
				return getUploadID(fui);
			}
		}



		eLog.d(TAG, "[sendResumableHttpRequest] ---");

		return upload_id;
	}



	private long requestUploadStatus(FileUploadInfo fui) {           
		eLog.d(TAG, "[requestUploadStatus] +++");

		String token = localService.getAccessToken();
		String filePath = fui.getFilePath();
		String uploadId = fui.getUploadId();

		String range_so_far = "0";

		java.io.File fileContent = new java.io.File(filePath);
		String contentLength = String.valueOf(fileContent.length());

		eLog.d(TAG, "[requestUploadStatus] contentLength : " + contentLength);

		int responseCode = GoogleRespCode.ERROR;

		try {
			String url = DRIVE_HOST+DRIVE_UPLOAD_URL+"?uploadType=resumable&upload_id=" + uploadId;
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("PUT");
			con.setRequestProperty("Authorization", "Bearer " + token);
			con.setRequestProperty("Content-Length", "0");
			con.setRequestProperty("Content-Range", "bytes */*");           

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.flush();
			wr.close();

			responseCode = con.getResponseCode();
			String rangeHeader = con.getHeaderField("Range");
			if (rangeHeader != null && rangeHeader.length() > 0) {
				String[] range = rangeHeader.split("-");
				range_so_far = range[1];
			}

			eLog.d(TAG, "[requestUploadStatus] Response Code : " + responseCode);
			eLog.d(TAG, "[requestUploadStatus] Response rangeHeader : " + rangeHeader);
			eLog.d(TAG, "[requestUploadStatus] Response range_so_far : " + range_so_far);                        

			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();
		} catch (Exception e) {
			e.printStackTrace();            
		}               

		if(responseCode == GoogleRespCode.ERROR_AUTHORIZATION){
			if(refreshAccessToken() && !isAttempt){
				isAttempt=true;
				return requestUploadStatus(fui);
			}
		}

		eLog.d(TAG, "[requestUploadStatus] ---");

		return Long.parseLong(range_so_far);
	}   







	public File getFileFromJson(String json){
		try {
			JSONObject object = new JSONObject(json);


			File file = new File();

			if(object.has("id"))file.setId(object.getString("id"));
			if(object.has("kind"))file.setKind(object.getString("kind"));
			if(object.has("etag"))file.setEtag(object.getString("etag"));
			if(object.has("selfLink"))file.setSelfLink(object.getString("selfLink"));
			if(object.has("webContentLink"))file.setWebContentLink(object.getString("webContentLink"));
			if(object.has("alternateLink"))file.setAlternateLink(object.getString("alternateLink"));
			if(object.has("iconLink"))file.setIconLink(object.getString("iconLink"));
			if(object.has("thumbnailLink"))file.setThumbnailLink(object.getString("thumbnailLink"));
			if(object.has("title"))file.setTitle(object.getString("title"));
			if(object.has("mimeType"))file.setMimeType(object.getString("mimeType"));
			if(object.has("selfLink"))file.setSelfLink(object.getString("selfLink"));
			if(object.has("selfLink"))file.setSelfLink(object.getString("selfLink"));
			if(object.has("originalFilename"))file.setOriginalFilename(object.getString("originalFilename"));
			if(object.has("fileExtension"))file.setFileExtension(object.getString("fileExtension"));
			if(object.has("md5Checksum"))file.setMd5Checksum(object.getString("md5Checksum"));
			if(object.has("fileSize"))file.setFileSize(object.getLong("fileSize"));
			if(object.has("quotaBytesUsed"))file.setQuotaBytesUsed(object.getLong("quotaBytesUsed"));
			if(object.has("editable"))file.setEditable(object.getBoolean("editable"));
			if(object.has("copyable"))file.setCopyable(object.getBoolean("copyable"));
			if(object.has("writersCanShare"))file.setWritersCanShare(object.getBoolean("writersCanShare"));
			if(object.has("appDataContents"))file.setAppDataContents(object.getBoolean("appDataContents"));
			if(object.has("headRevisionId"))file.setHeadRevisionId(object.getString("headRevisionId"));

			return file;

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

















	//*********************************************************************
	//*		GOOGLE DRIVE: DOWNLOAD
	//*********************************************************************


	//	public int downloadFile(FileDownloadInfo fdi, final UniversalProgressListener progressListener){
	//		eLog.d(TAG, "[downloadFile] +++");
	//
	//	    String token = localService.getAccessToken();
	//		String mimeType = fdi.getFileMime();
	//		String downloadId = fdi.getFileDownloadId();
	//		long range = fdi.getRange();
	//	    java.io.File fileContent = fdi.getFileContent();
	//	    
	//	    int chunkSize = Upload.MINIMUM_CHUNK_SIZE;
	//	    
	//	    String fileName = fileContent.getName();
	//	    String contentLength = String.valueOf(fileContent.length());
	//
	//	    eLog.i(TAG, "[downloadFile] downloadId:" + downloadId);
	//		eLog.i(TAG, "[downloadFile] range:" + range);
	//		eLog.v(TAG, "[downloadFile] chunkSize:" + chunkSize);
	//		eLog.v(TAG, "[downloadFile] fileName : " + fileName);
	//		eLog.v(TAG, "[downloadFile] contentLength : " + contentLength);
	//		eLog.v(TAG, "[downloadFile] mimeType : " + mimeType);
	//
	//	    int responseCode = GoogleRespCode.ERROR;
	//		String responseString = null;
	//		long totalBytesFromDataInputStream = 0;
	//
	//	    try {
	//	        String url = DRIVE_HOST+DRIVE_DOWNLOAD_URL+"/"+downloadId;
	//	        URL obj = new URL(url);
	//	        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	//
	//	        //add reuqest header
	//	        con.setRequestMethod("GET");
	//	        con.setRequestProperty("Authorization", "Bearer " + token);
	////	        con.setRequestProperty("Content-Type", mimeType);
	//	        //con.setFixedLengthStreamingMode(MediaHttpUploader.MINIMUM_CHUNK_SIZE);
	//
	//	        long nextByte = 0;
	//	        if (range > 0) {               
	//	            nextByte = range + 1;
	//	        }
	//	        con.setDoOutput(true); 
	//	        con.setChunkedStreamingMode(chunkSize);
	//
	//	        DataOutputStream wr = new DataOutputStream(new FileOutputStream(fileContent));
	//
	//	        DataInputStream inputStream = new DataInputStream(con.getInputStream()); 
	//
	//	        if(progressListener!=null)progressListener.onPreStart(inputStream,null);
	//			if(progressListener!=null)progressListener.onStart(inputStream,null);
	//
	//			int lastPercent = 0;
	//			long totalSize = fileContent.length();
	//			int bytes = 0;              
	//	        byte[] bufferOut = new byte[chunkSize];
	//	        while ((bytes = inputStream.read(bufferOut)) != -1) {      
	//	        	
	//	            if (nextByte > 0) {
	//	                nextByte = nextByte - bytes;
	//	                totalBytesFromDataInputStream += bytes;
	//	                if (nextByte >= 0) continue;
	//	            }
	//	            
	//	            wr.write(bufferOut, 0, bytes);  
	//	            totalBytesFromDataInputStream += bytes;
	//	            
	//	            eLog.d(TAG, "[downloadFile] progress: " + totalBytesFromDataInputStream+"/"+totalSize);
	//			    
	//			    // Here you can call the method which updates progress
	//			    // be sure to wrap it so UI-updates are done on the main thread!
	//			    int percent = (int) (100 * totalBytesFromDataInputStream / totalSize);
	//				if(lastPercent!=percent){
	//					lastPercent = percent;
	//					if(progressListener!=null)progressListener.onProgressChanged(inputStream, null, percent);
	//					eLog.d(TAG, "[downloadFile] percent:" + percent);
	//				}
	//
	//
	//				if(fdi.isStopDownload())
	//					break;
	//	        }                                  
	//			
	//	        eLog.d(TAG, "[downloadFile] totalBytesFromDataInputStream:" + totalBytesFromDataInputStream);
	//	        wr.flush();
	//	        wr.close();
	//
	//	        responseCode = con.getResponseCode();
	//
	//	        eLog.d(TAG, "[downloadFile] Response Code : " + responseCode);
	//
	//	        BufferedReader in = new BufferedReader(
	//	                new InputStreamReader(con.getInputStream()));
	//	        String inputLine;
	//	        StringBuffer response = new StringBuffer();
	//
	//	        while ((inputLine = in.readLine()) != null) {
	//	            response.append(inputLine);
	//	        }
	//	        in.close();
	//
	//	        responseString = response.toString();
	//	        
	////	        eLog.d(TAG, "[downloadFile] isUploadCompleted:" + isUploadCompleted);
	////	        eLog.d(TAG, "[downloadFile] isInterrupted:" + isInterrupted);
	//	    } catch (Exception e) {
	//	        e.printStackTrace();            
	////	        eLog.d(TAG, "[downloadFile] isInterrupted:" + isInterrupted);
	////	        eLog.d(TAG, "[downloadFile] totalBytesFromDataInputStream:" + totalBytesFromDataInputStream);
	//	    }
	//	    
	//	    
	//		fdi.setRange(totalBytesFromDataInputStream);
	//	    
	//	    
	//	    
	//		if(!fdi.isStopDownload()){
	//
	//			if(responseCode == GoogleRespCode.ERROR_AUTHORIZATION){
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return downloadFile(fdi, progressListener);
	//				}
	//			}else if(responseCode >= GoogleRespCode.ERROR){
	//				
	//				
	//				if(ASI.isInternetOn(c)){
	//					retryCount++;
	//
	//					try {
	//						Thread.sleep(retryCount*200);
	//					} catch (InterruptedException e) {
	//						// TODO Auto-generated catch block
	//						e.printStackTrace();
	//					}
	//
	//					return downloadFile(fdi, progressListener);
	//				}else{
	//					
	//					return responseCode;
	//				}
	//				
	//			}
	//			
	//			
	//			retryCount = 0;
	//			isAttempt=false;
	//
	//
	//			eLog.v(TAG, "RESPONSE = "+responseString);
	//			
	//			if(JavaUtils.isJSONValid(responseString)){
	//
	//				File file = getFileFromJson(responseString);
	//				
	//				if(progressListener!=null)progressListener.onFinish();
	//				
	//				return responseCode;
	//			}
	//		}
	//
	//		eLog.d(TAG, "[downloadFile] ---");
	//
	//		return responseCode;
	//	}
































	//*********************************************************************
	//*		GOOGLE DRIVE: API TRANSFER METHODS
	//*********************************************************************

	public synchronized GoogleResponse uploadFileDefault(FileUploadInfo fui, final UniversalProgressListener progressListener) {
		try {
			// File's binary content
			FileInputStream fileInputStream = new FileInputStream(fui.getFileContent());
			if(fileInputStream!=null){
				String parentId = fui.getParentCloudId();

				final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
				InputStreamContent mediaContent = new InputStreamContent(fui.getFileMime(), bufferedInputStream);
				mediaContent.setLength(fui.getFileContent().length());
				mediaContent.setRetrySupported(true);

				// File's metadata.
				File body = new File();
				body.setTitle(fui.getFileContent().getName());
				body.setMimeType(fui.getFileMime());

				if(parentId!=null){
					ParentReference parent = new ParentReference();
					parent.setId(parentId);
					body.setParents(Arrays.asList(parent)); 
				}


				Drive.Files.Insert request = drive.files().insert(body, mediaContent);

				MediaHttpUploaderProgressListener listener = new MediaHttpUploaderProgressListener() {

					@Override
					public void progressChanged(MediaHttpUploader arg0) throws IOException {
						switch (arg0.getUploadState()) {
						case INITIATION_STARTED:
							if(progressListener!=null)progressListener.onPreStart(bufferedInputStream,null);
							break;

						case INITIATION_COMPLETE:
							if(progressListener!=null)progressListener.onStart(bufferedInputStream,null);
							break;

						case MEDIA_IN_PROGRESS:
							int progress = JavaUtils.getIntegerValueOfDouble(arg0.getProgress()*100, 0);
							if(progressListener!=null)progressListener.onProgressChanged(bufferedInputStream, null, progress);
							break;

						case MEDIA_COMPLETE:
							if(progressListener!=null)progressListener.onFinish();
							break;

						default:
							break;
						}
					}
				};

				int chunkSize = Upload.MINIMUM_CHUNK_SIZE;

				MediaHttpUploader uploader = request.getMediaHttpUploader();
				uploader.setDirectUploadEnabled(false);
				uploader.setChunkSize(chunkSize); 
				uploader.setProgressListener(listener);
				File file = request.execute();
				if (file != null) {
					eLog.i(TAG, "FILE "+body.getTitle()+" UPLOADED!");
					isAttempt=false;
					return new GoogleResponse(file, null);
				}
			}


		} catch (GoogleJsonResponseException e) {
			GoogleJsonError error = e.getDetails();
			eLog.e(TAG, "FILE "+fui.getFileContent().getName()+" NOT UPLOADED!");
			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));


			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
				if(refreshAccessToken() && !isAttempt){
					isAttempt=true;
					return uploadFile(fui, progressListener);
				}
			}else{
				return new GoogleResponse(null, error.getCode());
			}

		} catch (IOException e) {
			eLog.e(TAG,"An error occurred: " + e.getLocalizedMessage());
			//			if(e.getLocalizedMessage()!=null && e.getLocalizedMessage().contains("unexpected end of stream"))
			//				return new GoogleResponse(null, GoogleRespCode.RETRY);
		}

		return null;
	}

	public int downloadFileDefault(FileDownloadInfo fdi, final UniversalProgressListener progressListener) {

		String fileCloudId = fdi.getFileDownloadId();

		GoogleResponse response = getDriveFile(fileCloudId);
		if(response!=null && response.result!=null){
			try {
				Drive.Files.Get request = drive.files().get(fileCloudId);
				final FileOutputStream fileOutputStream = new FileOutputStream(fdi.getFileContent());

				MediaHttpDownloaderProgressListener listener = new MediaHttpDownloaderProgressListener() {

					@Override
					public void progressChanged(MediaHttpDownloader arg0) throws IOException {
						switch (arg0.getDownloadState()) {
						case MEDIA_IN_PROGRESS:
							int progress = JavaUtils.getIntegerValueOfDouble(arg0.getProgress()*100, 0);
							if(progressListener!=null)progressListener.onProgressChanged(null, fileOutputStream, progress);
							break;

						case MEDIA_COMPLETE:
							if(progressListener!=null)progressListener.onFinish();
							break;

						default:
							break;
						}
					}
				};

				MediaHttpDownloader downloader =
						new MediaHttpDownloader(builder.getTransport(), drive.getRequestFactory().getInitializer());
				downloader.setDirectDownloadEnabled(false);
				int chunkSize = MediaHttpDownloader.MAXIMUM_CHUNK_SIZE/100;
				downloader.setChunkSize(chunkSize);
				downloader.setProgressListener(listener);
				downloader.download(new GenericUrl(request.execute().getDownloadUrl()), fileOutputStream);
				isAttempt=false;
				return GoogleRespCode.SUCCESS;
			} catch (GoogleJsonResponseException e) {
				GoogleJsonError error = e.getDetails();
				eLog.e(TAG,"Download error Code: "+error.getCode());
				eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));

				if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
					if(refreshAccessToken() && !isAttempt){
						isAttempt=true;
						return downloadFileDefault(fdi, progressListener);
					}
				}
				return error.getCode();
			} catch (IOException e) {
				eLog.e(TAG,"An error occurred: " + e);
				if(fdi.getFileContent().exists()){
					fdi.getFileContent().delete();
				}
			} catch (NullPointerException e) {
				if(fdi.getFileContent().exists()){
					fdi.getFileContent().delete();
				}
			}
		}else if(response!=null && response.errorCode!=null){
			if(fdi.getFileContent().exists()){
				fdi.getFileContent().delete();
			}
			return response.errorCode;
		}

		return GoogleRespCode.ERROR;
	}

	public GoogleResponse download(String downloadUrl) {
		if (downloadUrl != null && downloadUrl.length() > 0) {
			try {
				HttpResponse resp = drive.getRequestFactory().buildGetRequest(new GenericUrl(downloadUrl)).execute();
				eLog.i(TAG, "FILE DONWLOADED FROM URL "+downloadUrl);

				return new GoogleResponse(resp.getContent(), null);
			} catch (GoogleJsonResponseException e) {
				GoogleJsonError error = e.getDetails();
				eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
				eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));

				if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
					if(refreshAccessToken() && !isAttempt){
						isAttempt=true;
						return download(downloadUrl);
					}
				}
				return new GoogleResponse(null, error.getCode());
			} catch (IOException e) {
				eLog.e(TAG,"An error occurred: " + e);
			}
		}
		return null;
	}



























	//	//*********************************************************************
	//	//*		GOOGLE DRIVE: EDIT FILE
	//	//*********************************************************************
	//
	//	public int editFile(String cloudId, File file){
	//
	//		try {
	//			Update update = drive.files().update(cloudId, file);
	//			if(update!=null){
	//				eLog.i(TAG, "FILE "+file.getTitle()+" EDITED");
	//				isAttempt=false;
	//				return GoogleRespCode.SUCCESS;
	//			}else
	//				eLog.e(TAG, "NOT EDITED");
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG, "FILE "+file.getTitle()+"NOT EDITED");
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return editFile(cloudId, file);
	//				}else
	//					return error.getCode();
	//			}else
	//				return error.getCode();
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//
	//		return GoogleRespCode.FAILED;
	//	}
	//
	//
	//
	//	public int removeFile(String driveFileId){
	//
	//		try {
	//			if(driveFileId!=null){
	//				Void delete = drive.files().delete(driveFileId).execute();
	//				if(delete!=null){ 
	//					eLog.i(TAG, "REMOVED? "+delete.toString());
	//					isAttempt=false;
	//					return GoogleRespCode.SUCCESS;
	//				}else{
	//					eLog.e(TAG, "NOT REMOVED");
	//				}
	//			}
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return removeFile(driveFileId);
	//				}else
	//					return error.getCode();
	//			}else if(error.getCode() == GoogleRespCode.ERROR_FILE_NOT_FOUND){
	//				eLog.i(TAG, "FILE TO REMOVE NOT FOUND");
	//				return error.getCode();
	//			}else
	//				return error.getCode();
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//
	//		return GoogleRespCode.FAILED;
	//	}


	//*********************************************************************
	//*		GOOGLE DRIVE: SHARE
	//*********************************************************************

	//	/**
	//	 * Insert a new permission.
	//	 *
	//	 * @param fileId ID of the file to insert permission for.
	//	 * @param value User or group e-mail address, domain name or {@code null}
	//	         "default" type.
	//	 * @param type The value "user", "group", "domain" or "default".
	//	 * @param role The value "owner", "writer" or "reader".
	//	 * @return The inserted permission if successful, {@code null} otherwise.
	//	 */
	//	public int insertPermission(String fileId, String value, String type, String role, boolean sendNotificationEmail) {
	//		Permission newPermission = new Permission();
	//		newPermission.setValue(value);
	//		newPermission.setType(type);
	//		newPermission.setRole(role);
	//		newPermission.setWithLink(sendNotificationEmail);
	//		try {
	//			Permission permission = drive.permissions().insert(fileId, newPermission).setSendNotificationEmails(false).execute();
	//			if(permission!=null){
	//				eLog.i(TAG, "FILE ID "+fileId+" SHARED");
	//				return GoogleRespCode.SUCCESS;
	//			}else{
	//				eLog.e(TAG, "FILE ID "+fileId+" NOT SHARED");
	//			}
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG, "FILE ID "+fileId+" NOT SHARED");
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return insertPermission(fileId, value, type, role, sendNotificationEmail);
	//				}else
	//					return error.getCode();
	//			}else
	//				return error.getCode();
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//		return GoogleRespCode.FAILED;
	//	}
	//
	//
	//
	//	public GoogleResponse retrievePermissions(String fileId) {
	//		try {
	//
	//			PermissionList permissions = drive.permissions().list(fileId).execute();
	//			return new GoogleResponse(permissions.getItems(), null);
	//
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION)
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return retrievePermissions(fileId);
	//				}
	//			return new GoogleResponse(null, error.getCode());
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//
	//		return null;
	//	}
	//
	//
	//
	//	public int updatePermission(String fileId, String permissionId, String newRole) {
	//		try {
	//
	//			// First retrieve the permission from the API.
	//			Permission permission = drive.permissions().get(fileId, permissionId).execute();
	//			permission.setRole(newRole);
	//			Permission newPermission = drive.permissions().update(fileId, permissionId, permission).execute();
	//			if(newPermission!=null)
	//				return GoogleRespCode.SUCCESS;
	//
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return updatePermission(fileId, permissionId, newRole);
	//				}else
	//					return error.getCode();
	//			}else
	//				return error.getCode();
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//		return GoogleRespCode.FAILED;
	//	}





	//*********************************************************************
	//*		GOOGLE DRIVE: COPY & MOVE
	//*********************************************************************


	//	/**
	//	 * Copy an existing file.
	//	 *
	//	 * @param service Drive API service instance.
	//	 * @param originFileId ID of the origin file to copy.
	//	 * @param copyTitle Title of the copy.
	//	 * @return The copied file if successful, {@code null} otherwise.
	//	 */
	//	public GoogleResponse copyFile(String originFileId, String destinationFolderId, String copyTitle) {
	//		File copiedFile = new File();
	//
	//		if(copyTitle!=null)
	//			copiedFile.setTitle(copyTitle);
	//
	//		if(destinationFolderId!=null){
	//			ParentReference parent = new ParentReference();
	//			parent.setId(destinationFolderId);
	//			copiedFile.setParents(Arrays.asList(parent)); 
	//		}
	//
	//		try {
	//			File cfile = drive.files().copy(originFileId, copiedFile).execute();
	//			eLog.i(TAG, "FILE ID "+originFileId+" COPIED");
	//			return new GoogleResponse(cfile,null);
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG, "FILE ID "+originFileId+" NOT COPIED");
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return copyFile(originFileId, destinationFolderId, copyTitle);
	//				}
	//			}else{
	//				return new GoogleResponse(null, error.getCode());
	//			}
	//
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//		return null;
	//	}
	//
	//
	//	public GoogleResponse moveFile(String originFileId, String destinationFolderId, String moveTitle) {
	//		File movedFile = new File();
	//
	//		if(moveTitle!=null)
	//			movedFile.setTitle(moveTitle);
	//
	//		if(destinationFolderId!=null){
	//			ParentReference parent = new ParentReference();
	//			parent.setId(destinationFolderId);
	//			movedFile.setParents(Arrays.asList(parent)); 
	//		}
	//
	//		try {
	//			File mfile = drive.files().patch(originFileId, movedFile).execute();
	//			eLog.i(TAG, "FILE ID "+originFileId+" MOVED");
	//			return new GoogleResponse(mfile,null);
	//		} catch (GoogleJsonResponseException e) {
	//			GoogleJsonError error = e.getDetails();
	//			eLog.e(TAG, "FILE ID "+originFileId+" NOT MOVED");
	//			eLog.e(TAG,"Code: "+(error!=null? (""+error.getCode()):""));
	//			eLog.e(TAG,"Message:\n"+(error!=null? (""+error.getMessage()):""));
	//
	//
	//			if(error==null || error.getCode() == GoogleRespCode.ERROR_AUTHORIZATION){
	//				if(refreshAccessToken() && !isAttempt){
	//					isAttempt=true;
	//					return moveFile(originFileId, destinationFolderId, moveTitle);
	//				}
	//			}else{
	//				return new GoogleResponse(null, error.getCode());
	//			}
	//
	//
	//		} catch (IOException e) {
	//			eLog.e(TAG,"An error occurred: " + e);
	//		}
	//		return null;
	//	}


}
