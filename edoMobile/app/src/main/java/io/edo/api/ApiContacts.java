package io.edo.api;

import android.content.Context;

import com.paolobriganti.android.ASI;
import com.paolobriganti.android.Contact;
import com.paolobriganti.android.ContactsManager;
import com.paolobriganti.utils.JavaUtils;

import java.util.ArrayList;

import io.edo.api.google.GoogleContactsEdo;
import io.edo.db.local.LocalDBManager;
import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LService;
import io.edo.db.local.beans.LServiceDAO;
import io.edo.db.web.beans.ContactExtendedInfos;

public class ApiContacts {

	public static final int LIMIT_SUGGESTED_CONTACTS = 10;


	public synchronized static ArrayList<LContact> getContactsFromService(Context c, LService service){
		ArrayList<LContact> wcontacts = new ArrayList<LContact>();
		if(ASI.isInternetOn(c)){
			if(service.getName().equals(LService.SERVICE_NAME_GOOGLE)){
				wcontacts.addAll(GoogleContactsEdo.getAllContacts(c, service));
			}
		}
		return wcontacts;
	}

	public synchronized static ArrayList<LContact> getContactsFromAllServices(Context c, ArrayList<LService> services){
		ArrayList<LContact> wcontacts = new ArrayList<LContact>();
		if(ASI.isInternetOn(c)){

			if(services==null){
				LServiceDAO ldao = new LServiceDAO(c);
				services = ldao.getList();
			}

			for(int i=0; i<services.size(); i++){
				wcontacts.addAll(getContactsFromService(c, services.get(i)));
			}
		}
		return wcontacts;
	}

	public synchronized static ArrayList<LContact> getDeviceContacts(Context c){
		ArrayList<Contact> contacts = ContactsManager.getAllContactsWithSingleEmail(c);

		ArrayList<LContact> wcontacts = new ArrayList<LContact>();
		for(Contact con:contacts){
			if(con.getEmails()!=null && con.getEmails().size()>0){
				for(String email:con.getEmails()){
					LContact contact = new LContact(con, email);
					wcontacts.add(contact);
				}
			}
		}
		return wcontacts;
	}


	//	public synchronized static ArrayList<LContact> getAndSaveContactsFromEmails(Context c, LocalDBManager db, LService service, final int maxResult, boolean downloadContactPhoto){
	//
	//		ArrayList<LContact> lcontacts = new ArrayList<LContact>();
	//		if(ASI.isInternetOn(c)){
	//			if(service.getName().equals(LService.SERVICE_NAME_GOOGLE)){
	//				lcontacts.addAll(GoogleGmailEdo.getAndSaveContactsFromEmails(c, db, service, maxResult, downloadContactPhoto));
	//			}
	//		}
	//		return lcontacts;
	//	}













	//*********************************************************************
	//*		PHOTO
	//*********************************************************************
	public static String getContactPhotoWebLink(Context c, LocalDBManager db, LContact contact){
		if(contact!=null){


			//Get photo from Service
			ContactExtendedInfos ei = contact.getExtendedInfos();
			if(ei!=null && ei.serviceId!=null){
				LService service = new LServiceDAO(c).get(ei.serviceId);
				if(service!=null){
					String link = downloadContactPhotoFromService(c, db, service, contact);
					if(JavaUtils.isNotEmpty(link)){
						return link;
					}
				}
			}

			//Get photo from Google Plus
//			if(contact.getPrincipalEmailAddress()!=null) {
//				GooglePlusContactInfo infos = GoogleContactsEdo.getInfoFromGooglePlus(c, contact.getPrincipalEmailAddress());
//				if (infos != null) {
//					String link = infos.getPhotoLink();
//					if (JavaUtils.isNotEmpty(link)) {
//						return link;
//					}
//				}
//			}

		}

		return null;
	}



	public synchronized static String downloadContactPhotoFromService(Context c, LocalDBManager db, LService service, LContact contact){

		if(ASI.isInternetOn(c)){
			if(service.getName().equals(LService.SERVICE_NAME_GOOGLE))
				return GoogleContactsEdo.getContactPhotoLink(c, service, contact);
		}
		return null;
	}


}
