package io.edo.api.linkparser;

import android.content.Context;

import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.JavaUtils;
import com.paolobriganti.utils.WebUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;

import io.edo.db.global.beans.ServiceDAO;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.LService;
import io.edo.db.web.beans.WEntity;
import io.edo.db.web.beans.WThumbnailLink;
import io.edo.db.web.request.EdoRequest;
import io.edo.db.web.request.HeaderParams;
import io.edo.db.web.request.HttpRequest;
import io.edo.edomobile.ThisApp;
import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

/**
 * Created by PaoloBriganti on 16/05/15.
 */
public class LinkParser {



    public static final String REQUEST_TYPE_GET = "get";
    public static final String REQUEST_TYPE_POST = "post";
    public static final String REQUEST_TYPE_DELETE = "delete";
    public static final String REQUEST_TYPE_PUT = "put";



    public static String send(Context c, String type, String url, String body){
        String stringResponse = null;

        eLog.d("E_D_O_SERVICE", type.toUpperCase()+": "+url);
        eLog.w("E_D_O_SERVICE", "BODY \n"+body);

        HttpResponse response = execute(c, type, url, body);

        if(!hasErrors(response)){

            try {
                if(response.getEntity()!=null){
                    stringResponse = EntityUtils.toString(response.getEntity());

                    eLog.w("E_D_O_SERVICE", "SERVER RESPONSE " + stringResponse);
                }else{
                    eLog.w("E_D_O_SERVICE", "SERVER RESPONSE NULL");

                }

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }else if(isForbidden(c, response)){
            eLog.e("E_D_O_SERVICE", "ERROR FORBIDDEN");
            if(refreshToken(c)){
                eLog.i("E_D_O_SERVICE", "TOKEN REFRESHED");
                return send(c,type,url,body);
            }
        }

        return stringResponse;
    }

    public static HttpResponse execute(Context c, String type, String url, String body){
        ArrayList<HeaderParams> headerParams = getHeaderParams(c);

        HttpResponse response = null;

        if(ASI.isInternetOn(c) && headerParams!=null){

            if(JavaUtils.isNotEmpty(type) && type.equals(REQUEST_TYPE_GET)){
                response = HttpRequest.sendGet(url, headerParams);
            }else if(JavaUtils.isNotEmpty(type) && type.equals(REQUEST_TYPE_POST)){
                response = HttpRequest.sendPost(url, headerParams, null, body);
            }else if(JavaUtils.isNotEmpty(type) && type.equals(REQUEST_TYPE_DELETE)){
                response = HttpRequest.sendDelete(url, headerParams);
            }else if(JavaUtils.isNotEmpty(type) && type.equals(REQUEST_TYPE_PUT)){
                response = HttpRequest.sendPut(url, headerParams, null, body);
            }

        }

        return response;
    }

    public static ArrayList<HeaderParams> getHeaderParams(Context c){
        ThisApp app = ThisApp.getInstance(c);
        LService service = getService(c);
        if(service!=null && JavaUtils.isNotEmpty(service.getAccessToken())){
            ArrayList<io.edo.db.web.request.HeaderParams> params = new ArrayList<io.edo.db.web.request.HeaderParams>();
            params.add(new io.edo.db.web.request.HeaderParams("Authorization", service.getAccessToken()));
            return params;
        }
        return null;
    }

    public static LService getService(Context c){
        ServiceDAO serviceDAO = new ServiceDAO(c);
        ThisApp app = ThisApp.getInstance(c);
        LService service = serviceDAO.getLDAO().getServiceByName(LService.SERVICE_NAME_LINK_PARSER);
        if(service!=null && JavaUtils.isNotEmpty(service.getAccessToken())){
            return service;
        }else{
            service = serviceDAO.downloadLinkParserService();
            if(service!=null){
                return service;
            }
        }
        return null;
    }

    public static boolean refreshToken(Context c){
        return new ServiceDAO(c).downloadLinkParserService() != null;
    }

    public static boolean hasErrors(HttpResponse response){

        if(response!=null){
            int statusCode = response.getStatusLine().getStatusCode();

            eLog.w("E_D_O_SERVICE", "RESPONSE_STATUS_CODE "+statusCode);

            return statusCode >= HttpStatus.SC_MULTIPLE_CHOICES;

        }


        return true;
    }


    public static boolean isForbidden(Context c, HttpResponse response){
        if(response==null)
            return false;


        int statusCode = response.getStatusLine().getStatusCode();

        eLog.w("E_D_O_SERVICE", "CHECK RESPONSE_STATUS_CODE "+statusCode);

        return statusCode == HttpStatus.SC_FORBIDDEN || statusCode == HttpStatus.SC_UNAUTHORIZED;


    }

    public static LFile getThumbnailAndNameOfWebLink(Context context, String link){

        if(link!=null){

            String targetURL = EdoRequest.URL_LINK_THUMBNAIL+"?url="+link;

            String response = send(context, REQUEST_TYPE_GET, targetURL, null);
            if(!EdoRequest.hasEdoErrors(response)){
                WThumbnailLink entity = WThumbnailLink.fromJson(response);


                if(entity!=null){

                    LFile file = LFile.newLinkFile(null,entity.title,link,ThisApp.getInstance(context).getEdoUserId());
                    file.setThumbnail(entity.preview);


                    if(WebUtils.isYoutubeUrl(link) || link.contains(Constants.HOST_NAME_YOUTUBE_APP)){
                        String thumbLink = WebUtils.getYoutubeVideoThumbnailUrl(link);

                        if(JavaUtils.isEmpty(thumbLink) && link.contains(Constants.HOST_NAME_YOUTUBE_APP)){
                            String[] urlComp = link.split("/");
                            if(urlComp!=null && urlComp.length>0)
                                thumbLink = WebUtils.getYoutubeVideoThumbnailUrlByYoutubeId(urlComp[urlComp.length-1]);
                        }
                        if(thumbLink!=null){
                            file.setThumbnail(thumbLink);
                        }

                    }

                    return file;
                }
            }
        }
        return null;
    }

    public static WEntity getContactInfo(Context context, String email){

        if(email!=null){

            String targetURL = EdoRequest.URL_LINK_CONTACT_THUMB+"?e="+email;

            String response = send(context, REQUEST_TYPE_GET, targetURL, null);
            if(!EdoRequest.hasEdoErrors(response)){
                return WEntity.fromJson(response);
            }
        }
        return null;
    }

}
