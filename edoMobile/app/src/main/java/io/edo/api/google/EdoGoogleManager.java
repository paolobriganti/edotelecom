package io.edo.api.google;

import android.content.Context;

import com.paolobriganti.android.AUI;

import io.edo.db.global.beans.ServiceDAO;
import io.edo.db.local.beans.LService;
import io.edo.db.web.request.EdoAuthManager;
import io.edo.db.web.request.EdoRequest;
import io.edo.utilities.eLog;

public class EdoGoogleManager {
	public final static String APPLICATION_NAME="edo.io";
	public final static String TAG="GOOGLE_API";


	//*********************************************************************
	//*		AUTHORIZATION TOKEN
	//*********************************************************************
	private final static int MAX_ATTEMPTS = 3;
	public static LService refreshAccessToken(Context c, LService localService){
		int attempts = 0;
		if(attempts!=MAX_ATTEMPTS && localService!=null){
			while(attempts!=MAX_ATTEMPTS){
				final long sleep = attempts*500;

				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ServiceDAO serviceDAO = new ServiceDAO(c);
				LService srv = serviceDAO.updateGoogleAccessToken(localService.getId());
				
				eLog.e(TAG," REFRESH ACCESS TOKEN "+attempts+" ATTEMPTS --> "+srv);
				
				if(srv!=null){					
					return srv;
				}
				attempts++;
			}
			EdoAuthManager.logout(c);
		}
		return null;
	}


	public static boolean requestAuth(Context context, String userId){
		String url = EdoRequest.HOST_PREFIX + EdoRequest.PATH_SERVICE_GOOGLE + EdoRequest.QUERY_SEPARATOR + "id=" + userId;
		return AUI.openBrowser(context, url);
	}



}
