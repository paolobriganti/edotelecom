package io.edo.api.google;

import com.google.gson.Gson;
import com.paolobriganti.utils.JavaUtils;

import io.edo.utilities.Constants;
import io.edo.utilities.eLog;

public class GooglePlusContactInfo {
	
	

	String nameSurname; 
	String f; 
	String d; 
	String b; 
	String c; 
	String a; 
	String l; 
	String h; 
	String i;

	public String getNameSurname() {
		return nameSurname;
	}

	public void setNameSurname(String nameSurname) {
		this.nameSurname = nameSurname;
	}
	
	public String getEmail() {
		return f;
	}

	public void setEmail(String f) {
		this.f = f;
	}

	public String getInfo() {
		return d;
	}

	public void setInfo(String d) {
		this.d = d;
	}

	public String getPhotoLink() {
		if(JavaUtils.isNotEmpty(b) && !b.contains("http"))
			b = "http:"+b;
		return b;
	}

	public void setPhotoLink(String b) {
		this.b = b;
	}

	public String getCity() {
		return c;
	}

	public void setCity(String c) {
		this.c = c;
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getL() {
		return l;
	}

	public void setL(String l) {
		this.l = l;
	}

	public String getH() {
		return h;
	}

	public void setH(String h) {
		this.h = h;
	}

	public String getI() {
		return i;
	}

	public void setI(String i) {
		this.i = i;
	}

	public String toJson(){
		return new Gson().toJson(this);
	}
	
	public static GooglePlusContactInfo fromJson(String json){
		
		try{
			return new Gson().fromJson(json, GooglePlusContactInfo.class);
		}catch (com.google.gson.JsonSyntaxException e){
			eLog.e(Constants.TAG_JSON_SYNTAX_EXCEPTION, "GooglePlusContactInfo:\n"+e.getMessage());
			eLog.w(Constants.TAG_JSON_SYNTAX_EXCEPTION, "GooglePlusContactInfo JSON:\n"+json);
			return null;
		}
	}
}
