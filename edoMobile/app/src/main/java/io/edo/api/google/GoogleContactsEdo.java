package io.edo.api.google;

import android.content.Context;

import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.extensions.Email;
import com.paolobriganti.android.ASI;
import com.paolobriganti.utils.JavaUtils;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import io.edo.db.local.beans.LContact;
import io.edo.db.local.beans.LService;
import io.edo.db.web.request.HttpRequest;



public class GoogleContactsEdo {

	public static ArrayList<LContact> getAllContacts(Context context, LService localService){
		ArrayList<LContact> wcontacts = new ArrayList<LContact>();
		GoogleContacts gContacts = new GoogleContacts(context, localService);
		List<ContactEntry> contactsEntries = gContacts.getContacts();
		if(contactsEntries!=null && contactsEntries.size()>0)
			for(int i=0; i<contactsEntries.size(); i++){
				List<Email> emails = contactsEntries.get(i).getEmailAddresses();
				if(emails!=null && emails.size()>0){
					for(Email email:emails){
						LContact contact = new LContact(localService.getId(), contactsEntries.get(i), email);
						wcontacts.add(contact);
					}
				}
			}
		return wcontacts;
	}



//	public static boolean downloadContactPhoto(Context c, LocalDBManager db, LContact contact, LService service, GoogleContacts gContacts, ContactEntry contactEntry){
//		if(gContacts==null)
//			gContacts = new GoogleContacts(c,db, service);
//
//		if(contactEntry==null && gContacts!=null && contact!=null && contact.getExtendedInfoJson()!=null)
//			contactEntry = gContacts.getContactEntry(contact.getExtendedInfos().cloudId);
//
//		if(contactEntry!=null){
//			File folder = ContactPhoto.getPhotoFolder(c);
//			folder.mkdirs();
//			String photoPath = folder.toString()+File.separator+contact.getId();
//			return gContacts.downloadPhoto(contactEntry, photoPath);
//		}
//		return false;
//	}

	public static String getContactPhotoLink(Context c, LService service, LContact contact){
		GoogleContacts gContacts = new GoogleContacts(c, service);

		if(gContacts!=null && contact!=null && contact.getExtendedInfosJson()!=null){
			ContactEntry contactEntry = gContacts.getContactEntry(contact.getExtendedInfos().cloudId);
			if(contactEntry!=null){
				return contactEntry.getContactPhotoLink().getHref();
			}
		}

		return null;
	}







	//*********************************************************************
	//*		PHOTO FROM GOOGLE PLUS
	//*********************************************************************
	public static GooglePlusContactInfo getInfoFromGooglePlus(Context c, String googleEmail){
		int PREFIX = 19;

		String url = "https://plus.google.com/complete/search?client=es-people-picker&authuser=0&xhr=f&q="+googleEmail;
		HttpResponse resp = HttpRequest.sendGet(url, null);
		String response = null;
		try {
			response = EntityUtils.toString(resp.getEntity());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(JavaUtils.isNotEmpty(response) && response.length()>PREFIX){
			String jsonString = response.substring(PREFIX, response.length() - 1);


			try {
				JSONArray array1 = new JSONArray(jsonString);
				JSONArray array2 = new JSONArray(array1.getString(1));
				JSONArray array3 = new JSONArray(array2.getString(0));
				if(array3!=null && array3.length()>3){
					GooglePlusContactInfo infos = GooglePlusContactInfo.fromJson(array3.getString(3));
					if(infos!=null)
						infos.setNameSurname(array3.getString(0));
					return infos;
				}

			} catch (JSONException e) {
			}
		}


		return null;
	}

	public static LContact getContactFromGooglePlus(final Context c, final String emailAddress){

		if(ASI.isInternetOn(c)) {

			GooglePlusContactInfo infos = getInfoFromGooglePlus(c, emailAddress);

			if (infos != null) {
				LContact contact = new LContact();

				contact.setThumbnail(infos.getPhotoLink());

				contact.setName(infos.getNameSurname());

				contact.setPrincipalEmailAddress(infos.getEmail());

				return contact;
			}

		}
		return null;
	}
}
