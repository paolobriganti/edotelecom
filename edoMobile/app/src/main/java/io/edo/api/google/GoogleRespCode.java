package io.edo.api.google;


public class GoogleRespCode{
	public static final int SUCCESS = 200;
	public static final int SUCCESS_CHUCK = 308;
	public static final int ERROR_BAD_REQUEST = 400;
	public static final int ERROR_AUTHORIZATION = 401;
	public static final int ERROR_FILE_NOT_FOUND = 404;
	public static final int ERROR = 500;
	public static final int ERROR_SERVICE_NOT_AVAILABLE = 503;
	public static final int ERROR_TIMEOUT = 599;
}
