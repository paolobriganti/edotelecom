package io.edo.api;


import com.paolobriganti.utils.FileInfo;

import io.edo.db.local.beans.LActivity;

public class FileUploadInfo{

	public LActivity a;
	private String filePath;
	private String fileMime;
	
	private java.io.File fileContent;
	
	private String parentCloudId;
	private String uploadId;
	private long range = 0;
	
	private boolean stopUpload = false;
    
	public FileUploadInfo(String filePath) {
		super();
		this.filePath = filePath;
		this.fileMime = FileInfo.getMime(filePath);
		
		try{
			this.fileContent = new java.io.File(filePath);
		}catch(NullPointerException e){
			
		}
		
		
	}
    
    
    
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileMime() {
		return fileMime;
	}

	public void setFileMime(String fileMime) {
		this.fileMime = fileMime;
	}

	public java.io.File getFileContent() {
		return fileContent;
	}

	public void setFileContent(java.io.File fileContent) {
		this.fileContent = fileContent;
	}
	
	public String getUploadId() {
		return uploadId;
	}
	
	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}

	public long getRange() {
		return range;
	}

	public void setRange(long range) {
		this.range = range;
	}



	public String getParentCloudId() {
		return parentCloudId;
	}



	public void setParentCloudId(String parentCloudId) {
		this.parentCloudId = parentCloudId;
	}



	public boolean isStopUpload() {
		return stopUpload;
	}

	public void setStopUpload(boolean stopUpload) {
		this.stopUpload = stopUpload;
	}
}
