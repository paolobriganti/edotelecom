package io.edo.api;

import java.io.Serializable;

import io.edo.db.local.beans.LActivity;
import io.edo.db.local.beans.LFile;
import io.edo.db.local.beans.support.EdoContainer;
import io.edo.db.local.beans.support.LSpace;

public class Download  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public EdoContainer container;
	
	public LSpace tab;
	public String folderPath;
	public LFile file;
	public boolean isRunning=false;

	public LActivity a;

	public Download() {}

	public Download(EdoContainer container, LSpace tab, String folderPath, LFile file) {
		super();
		this.container = container;
		this.tab = tab;
		this.folderPath = folderPath;
		this.file = file;
	}



}
