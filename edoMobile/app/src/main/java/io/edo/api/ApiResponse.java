package io.edo.api;

import io.edo.api.google.GoogleResponse;

public class ApiResponse {
	public Object result;
	public Integer errorCode;
	
	
	public ApiResponse(Object result, Integer errorCode) {
		super();
		this.result = result;
		this.errorCode = errorCode;
	}

	public ApiResponse(Object result, GoogleResponse response) {
		super();
		this.result = result;
		this.errorCode = getApiResponseCodeFromGoogleResponse(response!=null?response.errorCode:null);
	}
	
	public static Integer getApiResponseCodeFromGoogleResponse(Integer code){
		//Because google has the same error codes
		return code;
	}
}
